<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfileParamsValidators extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_params', function(Blueprint $table)
		{
                        DB::table('profile_params')
                            ->where('code', '=', 'chest')
                            ->update(array('validation_rule' => 'required|integer|between:' . \Acme\Models\Profile\ProfileParams::MIN_CHEST . ',' . \Acme\Models\Profile\ProfileParams::MAX_CHEST));
                        
                        DB::table('profile_params')
                            ->where('code', '=', 'waist')
                            ->update(array('validation_rule' => 'required|integer|between:' . \Acme\Models\Profile\ProfileParams::MIN_WAIST . ',' . \Acme\Models\Profile\ProfileParams::MAX_WAIST));
                        
                        DB::table('profile_params')
                            ->where('code', '=', 'breast')
                            ->update(array('validation_rule' => 'required|integer|between:' . \Acme\Models\Profile\ProfileParams::MIN_BREAST . ',' . \Acme\Models\Profile\ProfileParams::MAX_BREAST));
                        
                        DB::table('profile_params')
                            ->where('code', '=', 'cloth')
                            ->update(array('validation_rule' => 'required|integer|between:' . \Acme\Models\Profile\ProfileParams::MIN_CLOTH . ',' . \Acme\Models\Profile\ProfileParams::MAX_CLOTH));
                        
                        DB::table('profile_params')
                            ->where('code', '=', 'shoes')
                            ->update(array('validation_rule' => 'required|integer|between:' . \Acme\Models\Profile\ProfileParams::MIN_SHOES . ',' . \Acme\Models\Profile\ProfileParams::MAX_SHOES));
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_params', function(Blueprint $table)
		{
			//
		});
	}

}
