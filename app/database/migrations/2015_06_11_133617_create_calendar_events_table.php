<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendar_events', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('events_id')->unsigned()->index();
            $table->tinyInteger('status');
            $table->timestamp('repeat_start');
            $table->timestamp('repeat_end');
            $table->timestamp('repeat_interval');
            $table->timestamp('repeat_year');
            $table->timestamp('repeat_month');
            $table->timestamp('repeat_day');
            $table->timestamp('repeat_week');
            $table->timestamp('repeat_weekday');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('events_id')->references('id')->on('events')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendar_events');
	}

}
