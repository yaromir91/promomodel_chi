<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGroupsInvites extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('groups_invites');
		Schema::create('groups_invites', function(Blueprint $table){
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();;
			$table->integer('group_id')->unsigned()->index();
			$table->string('token')->nullable();
			$table->unique(['user_id', 'group_id']);
			$table->enum('status', ['active', 'not_active'])->default('not_active');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\Schema::dropIfExists('groups_invites');
	}

}
