<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateEventInviteAddAcceptManagerStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('event_invite', function(Blueprint $table)
		{
			$table->tinyInteger('manager_status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('event_invite', function(Blueprint $table)
		{
			$table->tinyInteger('manager_status');
		});
	}

}
