<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUserChats extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_chats', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
			$table->char('chat_code', 8);
			$table->unique('chat_code');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_chats');
	}

}
