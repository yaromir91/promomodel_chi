<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteUnusedFieldsUserReserves extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_reserves', function(Blueprint $table)
		{
                    $table->dropColumn('repeat_start');
                    $table->dropColumn('repeat_duration');
                    $table->dropColumn('repeat_week');
                    $table->dropColumn('repeat_dayli');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_reserves', function(Blueprint $table)
		{
                    $table->timestamp('repeat_start');
                    $table->timestamp('repeat_duration');
                    $table->string('repeat_week');
                    $table->smallInteger('repeat_dayli');
		});
	}

}
