<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProfessionsRateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_professions_rate', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned()->index();
            $table->integer('professions_id')->unsigned()->index();
            $table->foreign('user_id')->references('user_id')->on('user_profile')->onDelete('cascade');
            $table->foreign('professions_id')->references('id')->on('professions')->onDelete('cascade');
            $table->double('rate_h', 12, 2);
            $table->double('rate_d', 12, 2);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_professions_rate');
	}

}
