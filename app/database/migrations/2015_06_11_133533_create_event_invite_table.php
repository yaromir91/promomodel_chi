<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventInviteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_invite', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
            $table->integer('events_id')->unsigned()->index();
            $table->integer('users_id')->unsigned()->index();
            $table->integer('professions_id')->unsigned()->index();
            $table->tinyInteger('status');
            $table->foreign('events_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('professions_id')->references('id')->on('professions')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_invite');
	}

}
