<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationOwnerUserId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notifications', function(Blueprint $table)
		{
            $table->integer('owner_user_id')->unsigned()->index();
            $table->foreign('owner_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->dropColumn('subject');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notifications', function(Blueprint $table)
		{
            $table->string('subject');
            $table->dropColumn('owner_user_id');
		});
	}

}
