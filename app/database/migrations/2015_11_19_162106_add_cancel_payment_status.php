<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCancelPaymentStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('events', function(Blueprint $table)
		{
			//
			DB::statement("ALTER TABLE `prm_events` CHANGE `paiment_status` `paiment_status` enum('new','hold','paid','cancel')");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('events', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE `prm_events` CHANGE `paiment_status` `paiment_status` enum('new','hold','paid')");
		});
	}

}
