<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateValidationRulesField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_params', function(Blueprint $table)
		{
			DB::table('profile_params')
				->where('name', '=', 'Размер груди')
				->update(array('validation_rule' => 'required'));

			DB::table('profile_params')
				->where('name', '=', 'Цвет волос')
				->update(array('validation_rule' => 'required'));

			DB::table('profile_params')
				->where('name', '=', 'Длина волос')
				->update(array('validation_rule' => 'required'));

			DB::table('profile_params')
				->where('name', '=', 'Цвет глаз')
				->update(array('validation_rule' => 'required'));

			DB::table('profile_params')
				->where('name', '=', 'Тип внешности')
				->update(array('validation_rule' => 'required'));

			DB::table('profile_params')
				->where('name', '=', 'Опыт работы')
				->update(array('validation_rule' => 'required'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_params', function(Blueprint $table)
		{
			//
		});
	}

}
