<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUserChatMessages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_chat_messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_sender_id')->unsigned()->index();
			$table->integer('user_receiver_id')->unsigned()->index();
			$table->text('body');
			$table->tinyInteger('is_read')->default(0);
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
			$table->foreign('user_sender_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('user_receiver_id')->references('id')->on('users')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_chat_messages');
	}

}
