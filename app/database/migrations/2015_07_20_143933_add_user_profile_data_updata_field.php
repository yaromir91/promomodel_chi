<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUserProfileDataUpdataField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("
                ALTER TABLE `prm_user_profile_data`
                CHANGE `int_value` `int_value` int(11) NULL AFTER `params_id`,
                CHANGE `string_value` `string_value` varchar(255) COLLATE 'utf8_unicode_ci' NULL AFTER `int_value`,
                CHANGE `text_value` `text_value` text COLLATE 'utf8_unicode_ci' NULL AFTER `string_value`,
                CHANGE `bool_value` `bool_value` tinyint(4) NULL AFTER `text_value`
        ");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement("
            ALTER TABLE `prm_user_profile_data`
            CHANGE `int_value` `int_value` int(11) NOT NULL AFTER `params_id`,
            CHANGE `string_value` `string_value` varchar(255) COLLATE 'utf8_unicode_ci' NOT NULL AFTER `int_value`,
            CHANGE `text_value` `text_value` text COLLATE 'utf8_unicode_ci' NOT NULL AFTER `string_value`,
            CHANGE `bool_value` `bool_value` tinyint(4) NOT NULL AFTER `text_value`
        ");
	}

}
