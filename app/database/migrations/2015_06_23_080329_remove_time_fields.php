    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTimeFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('events', function(Blueprint $table)
		{
            $table->dropColumn('date_start');
            $table->dropColumn('date_end');
            $table->dropColumn('time_start');
            $table->dropColumn('time_end');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('events', function(Blueprint $table)
		{
            $table->time('time_start');
            $table->time('time_end');
            $table->date('date_start');
            $table->date('date_end');
		});
	}

}
