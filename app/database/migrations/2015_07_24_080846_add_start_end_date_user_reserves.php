<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartEndDateUserReserves extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_reserves', function(Blueprint $table)
		{
            $table->timestamp('reserve_start');
            $table->timestamp('reserve_end');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_reserves', function(Blueprint $table)
		{
            $table->dropColumn('reserve_start');
            $table->dropColumn('reserve_end');
		});
	}

}
