<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateUserApplicantsPaimentsAddProfessionId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_applicants_payments', function(Blueprint $table)
		{
			$table->integer('profession_id')->unsigned()->index();
			$table->foreign('profession_id')->references('id')->on('professions')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_applicants_payments', function(Blueprint $table)
		{
			$table->dropColumn('profession_id');
		});
	}

}
