<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfessionsEventsRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('professions_events_rates', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
            $table->integer('events_id')->unsigned()->index();
            $table->integer('professions_id')->unsigned()->index();
            $table->integer('count');
            $table->double('rate_h', 12, 2);
            $table->double('rate_d', 12, 2);
            $table->foreign('events_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('professions_id')->references('id')->on('professions')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('professions_events_rates');
	}

}
