<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProfileDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_profile_data', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned()->index();
            $table->integer('params_id')->unsigned()->index();
            $table->integer('int_value');
            $table->string('string_value');
            $table->text('text_value');
            $table->tinyInteger('bool_value');
            $table->foreign('user_id')->references('user_id')->on('user_profile')->onDelete('cascade');
            $table->foreign('params_id')->references('id')->on('profile_params')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_profile_data');
	}

}
