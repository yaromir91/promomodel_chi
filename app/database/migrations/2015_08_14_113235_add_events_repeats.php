<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventsRepeats extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('events', function(Blueprint $table)
		{
            $table->smallInteger('repeat_status');
            $table->timestamp('repeat_end');
            $table->string('repeat_years');
            $table->string('repeat_month');
            $table->string('repeat_weekday');
            $table->string('repeat_day');
            $table->string('repeat_text');
            $table->string('repeat_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('events', function(Blueprint $table)
        {
            $table->dropColumn('repeat_status');
            $table->dropColumn('repeat_end');
            $table->dropColumn('repeat_years');
            $table->dropColumn('repeat_month');
            $table->dropColumn('repeat_weekday');
            $table->dropColumn('repeat_day');
            $table->dropColumn('repeat_text');
            $table->dropColumn('repeat_type');
        });
	}

}
