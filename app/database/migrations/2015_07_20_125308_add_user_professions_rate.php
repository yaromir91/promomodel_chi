<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUserProfessionsRate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("
          ALTER TABLE `prm_user_professions_rate`
          CHANGE `rate_h` `rate_h` double(12,2) NULL AFTER `professions_id`,
          CHANGE `rate_d` `rate_d` double(12,2) NULL AFTER `rate_h`
        ");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement("
          ALTER TABLE `prm_user_professions_rate`
          CHANGE `rate_h` `rate_h` double(12,2) NOT NULL AFTER `professions_id`,
          CHANGE `rate_d` `rate_d` double(12,2) NOT NULL AFTER `rate_h`,
        ");
	}

}
