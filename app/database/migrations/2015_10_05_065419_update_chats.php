<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateChats extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `prm_user_chat_messages` DROP FOREIGN KEY `user_chat_messages_chat_id_foreign`");
		DB::statement("ALTER TABLE `prm_user_chat_messages` DROP FOREIGN KEY `user_chat_messages_user_receiver_id_foreign`");
		DB::statement("ALTER TABLE `prm_user_chat_messages` DROP INDEX `user_chat_messages_user_receiver_id_index`, DROP INDEX `user_chat_messages_chat_id_index`");
		DB::statement("ALTER TABLE `prm_user_chat_messages` DROP `user_receiver_id`, DROP `chat_id`");
		DB::statement("ALTER TABLE `prm_user_chats` DROP INDEX `user_chats_chat_code_unique`");
		DB::statement("ALTER TABLE `prm_user_chats` DROP `chat_code`");
		DB::statement("ALTER TABLE `prm_user_chats` DROP `id`");


		Schema::table('user_chats', function(Blueprint $table)
		{
			$table->integer('user_sender_id')->unsigned()->index();
			$table->integer('user_receiver_id')->unsigned()->index();
			$table->string('code', 100);
			$table->primary('code');
			$table->foreign('user_sender_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('user_receiver_id')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::table('user_chat_messages', function(Blueprint $table)
		{
			$table->string('chat_code', 100);
			$table->foreign('chat_code')->references('code')->on('user_chats')->onDelete('cascade');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_chats', function(Blueprint $table)
		{
			$table->dropColumn('user_sender_id');
			$table->dropColumn('user_receiver_id');
		});
	}

}
