<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUpserProfileTableAddVkLink extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('user_profile', function(Blueprint $table)
            {
                $table->string('vk_link', 255);
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('user_profile', function(Blueprint $table)
            {
                $table->dropColumn('vk_link');
            });
	}

}
