<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfileParamsChinaLng extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profile_params', function(Blueprint $table)
		{       
                        $id = DB::table('profile_params')->where('name', '=', 'Знание языков')->pluck('id');
                        
                        $lng = [
                            'parent_id'         => $id,
                            'name'              => 'китайский',
                            'type'              => 4,
                            'num'               => 1,
                            'validation_rule'   => 'required',
                            'code'              => '0'
                        ];

                        DB::table('profile_params')->insert($lng);
        
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profile_params', function(Blueprint $table)
		{
			//
		});
	}

}
