<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesProfileParamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roles_profile_params', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
            $table->integer('roles_id')->unsigned()->index();
            $table->integer('profile_params_id')->unsigned()->index();
            $table->foreign('roles_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('profile_params_id')->references('id')->on('profile_params')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles_profile_params');
	}

}
