<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUserProfileOrderedParam extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profile_params', function(Blueprint $table)
            {
                $table->integer('num')->unsigned()->default(1);
            });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profile_params', function(Blueprint $table)
            {
                $table->dropColumn('num');
            });
	}

}
