<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRepeatFlag extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_reserves', function(Blueprint $table)
		{
            $table->smallInteger('repeat_status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_reserves', function(Blueprint $table)
		{
            $table->dropColumn('repeat_status');
		});
	}

}
