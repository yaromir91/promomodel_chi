<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateRatingTableAddTeamType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `prm_ratings` CHANGE `type` `type` enum('event','user','photo','post','team') COLLATE 'utf8_unicode_ci' NOT NULL AFTER `rating`");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement("ALTER TABLE `prm_ratings` CHANGE `type` `type` enum('event','user','photo','post') COLLATE 'utf8_unicode_ci' NOT NULL AFTER `rating`");
	}

}
