<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateEventInviteProfIdSetAsNull extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `prm_event_invite` CHANGE `professions_id` `professions_id` int(10) unsigned NULL AFTER `users_id`");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement("ALTER TABLE `prm_event_invite` CHANGE `professions_id` `professions_id` int(10) unsigned NOT NULL AFTER `users_id`");
	}

}
