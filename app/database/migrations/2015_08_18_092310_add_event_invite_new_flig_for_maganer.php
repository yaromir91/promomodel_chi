<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddEventInviteNewFligForMaganer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('event_invite', function(Blueprint $table)
		{
			$table->tinyInteger('is_manager');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('event_invite', function(Blueprint $table)
		{
			$table->dropColumn('is_manager');
		});
	}

}
