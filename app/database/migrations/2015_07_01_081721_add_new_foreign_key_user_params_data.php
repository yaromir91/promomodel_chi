<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNewForeignKeyUserParamsData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('user_profile_data', function(Blueprint $table)
            {
                $table->integer('param_id_val')->unsigned()->index()->nullable();
                $table->foreign('param_id_val')->references('id')->on('profile_params')->onDelete('cascade')->nullable();
            });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('user_profile_data', function(Blueprint $table)
            {
                $table->dropForeign('user_profile_data_param_id_val_foreign');
                $table->dropColumn('param_id_val');
            });
	}

}
