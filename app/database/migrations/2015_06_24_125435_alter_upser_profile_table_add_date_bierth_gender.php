<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUpserProfileTableAddDateBierthGender extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('user_profile', function(Blueprint $table)
            {
                $table->date('date_birth');
                $table->enum('gender', array('m', 'f'));
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('user_profile', function(Blueprint $table)
            {
                $table->dropColumn('date_birth');
                $table->dropColumn('gender');
            });
	}

}
