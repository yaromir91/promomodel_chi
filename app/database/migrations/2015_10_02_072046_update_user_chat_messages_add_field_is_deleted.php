<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateUserChatMessagesAddFieldIsDeleted extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_chat_messages', function(Blueprint $table)
		{
			$table->dropColumn('is_read');
			$table->tinyInteger('is_m_read')->default(0);
			$table->tinyInteger('is_deleted')->default(0);
			$table->integer('chat_id')->unsigned()->index();
			$table->foreign('chat_id')->references('id')->on('user_chats')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_chat_messages', function(Blueprint $table)
		{
			$table->dropColumn('is_deleted');
			$table->dropColumn('chat_id');
		});
	}

}
