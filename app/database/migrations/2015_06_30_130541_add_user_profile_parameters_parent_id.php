<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUserProfileParametersParentId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("ALTER TABLE prm_profile_params CHANGE parent_id parent_id int(10) unsigned NULL AFTER id");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profile_params', function(Blueprint $table)
            {
                $table->renameColumn('parent_id','parent_id');
            });
	}

}
