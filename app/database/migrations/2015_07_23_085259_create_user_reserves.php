<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReserves extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_reserves', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id')->unsigned()->index();    // создатель
            $table->string('title');                            // название
            $table->text('description');                        // описание
            $table->timestamp('repeat_start');                  // начало повторения
            $table->timestamp('repeat_end');                    // конец повторения
            $table->timestamp('repeat_duration');               // интервал повторения
            $table->string('repeat_years');                     // в какие года повторять, например, в 2015 и 2017
            $table->string('repeat_month');                     // в какой месяц повторять, например каждый 5 месяц(май) или 5,6,? месяцы
            $table->string('repeat_week');                      // в какую по счету неделю повторять
            $table->string('repeat_weekday');                   // в какой день недели повторять
            $table->string('repeat_day');                       // в какой день повторять
            $table->smallInteger('repeat_dayli');               // повторять каждый день, null or 1
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_reserves');
	}

}
