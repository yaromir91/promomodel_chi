<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamePhotoshow extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('events_type', function(Blueprint $table)
		{
			DB::table('events_type')
                            ->where('title', '=', 'Фотовыставка')
                            ->update(array('title' => 'Выставка'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('events_type', function(Blueprint $table)
		{
			DB::table('events_type')
                            ->where('title', '=', 'Выставка')
                            ->update(array('title' => 'фотовыставка'));
		});
	}

}
