<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProfileParamsUpdateLanguageFieldTableSeeder extends Seeder {

	public function run()
	{
        DB::table('profile_params')
            ->where('name', '=', 'Знание языков')
            ->update(array('type' => 6));
	}

}