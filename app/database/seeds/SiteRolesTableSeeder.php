<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SiteRolesTableSeeder extends Seeder {

	public function run()
	{
		$siteRoles = [
            ['name' => 'Agent'],
            ['name' => 'Applicant'],
            ['name' => 'ApplicantManager'],
            ['name' => 'Organizer'],
        ];

        DB::table('roles')->insert($siteRoles);
	}

}