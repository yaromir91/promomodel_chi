<?php



class UpdateParamProfileTypesTableSeeder extends Seeder {

	public function run()
	{
        DB::table('profile_params')
            ->where('name', 'Рост')
            ->update(array('type' => 2));

        DB::table('profile_params')
            ->where('name', 'Грудь')
            ->update(array('type' => 2));

        DB::table('profile_params')
            ->where('name', 'Талия')
            ->update(array('type' => 2));

        DB::table('profile_params')
            ->where('name', 'Бедра')
            ->update(array('type' => 2));

        $growth = DB::table('profile_params')->where('name', 'Рост')->first();

        if ($growth) {
            DB::table('profile_params')->where('parent_id', $growth->id)->delete();
        }


        $breast = DB::table('profile_params')->where('name', 'Грудь')->first();

        if ($breast) {
            DB::table('profile_params')->where('parent_id', $breast->id)->delete();
        }

        $thalia = DB::table('profile_params')->where('name', 'Талия')->first();

        if ($thalia) {
            DB::table('profile_params')->where('parent_id', $thalia->id)->delete();
        }

        $hips = DB::table('profile_params')->where('name', 'Бедра')->first();

        if ($hips) {
            DB::table('profile_params')->where('parent_id', $hips->id)->delete();
        }
	}

}