<?php

class ProfileAdditionParamsTypeFaceTableSeeder extends Seeder {

	public function run()
	{
        // Ethnicity
        $ethnicity = DB::table('profile_params')->where('name', 'Тип внешности')->first();

        if ($ethnicity) {
            $ethnicityArr = [
                ['name' => 'европейский', 'type' => 4, 'parent_id' => $ethnicity->id],
                ['name' => 'азиатский',   'type' => 4, 'parent_id' => $ethnicity->id],
                ['name' => 'мулат',       'type' => 4, 'parent_id' => $ethnicity->id],
            ];

            DB::table('profile_params')->insert($ethnicityArr);
        }
	}

}