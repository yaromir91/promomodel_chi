<?php


class ProfileAdditionParamsTableSeeder extends Seeder {

	public function run()
	{
        DB::table('profile_params')
            ->where('name', 'Грудь')
            ->update(array('num' => 2));

        DB::table('profile_params')
            ->where('name', 'Талия')
            ->update(array('num' => 3));

        DB::table('profile_params')
            ->where('name', 'Бедра')
            ->update(array('num' => 4));

        DB::table('profile_params')
            ->where('name', 'Размер груди')
            ->update(array('num' => 5));

        DB::table('profile_params')
            ->where('name', 'Размер одежды')
            ->update(array('num' => 6));

        DB::table('profile_params')
            ->where('name', 'Размер обуви')
            ->update(array('num' => 7));

        DB::table('profile_params')
            ->where('name', 'Цвет глаз')
            ->update(array('num' => 8));

        DB::table('profile_params')
            ->where('name', 'Цвет волос')
            ->update(array('num' => 9));

        DB::table('profile_params')
            ->where('name', 'Длина волос')
            ->update(array('num' => 10));

        DB::table('profile_params')
            ->where('name', 'Тип внешности')
            ->update(array('num' => 11));

        DB::table('profile_params')
            ->where('name', 'Знание языков')
            ->update(array('num' => 12));

        DB::table('profile_params')
            ->where('name', 'Youtube link')
            ->update(array('num' => 13));

        DB::table('profile_params')
            ->where('name', 'Опыт работы')
            ->update(array('num' => 14));
    }

}