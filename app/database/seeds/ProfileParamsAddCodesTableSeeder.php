<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProfileParamsAddCodesTableSeeder extends Seeder {

	public function run()
	{
        $height_id = DB::table('profile_params')->where('name', '=', 'Рост')->pluck('id');
        $chest_id = DB::table('profile_params')->where('name', '=', 'Грудь')->pluck('id');
        $waist_id = DB::table('profile_params')->where('name', '=', 'Талия')->pluck('id');
        $breast_id = DB::table('profile_params')->where('name', '=', 'Бедра')->pluck('id');
        $cloth_id = DB::table('profile_params')->where('name', '=', 'Размер одежды')->pluck('id');
        $shoes_id = DB::table('profile_params')->where('name', '=', 'Размер обуви')->pluck('id');


        DB::table('profile_params')
            ->where('id', $height_id)
            ->update(array('code' => 'height'));
        DB::table('profile_params')
            ->where('id', $chest_id)
            ->update(array('code' => 'chest'));
        DB::table('profile_params')
            ->where('id', $waist_id)
            ->update(array('code' => 'waist'));
        DB::table('profile_params')
            ->where('id', $breast_id)
            ->update(array('code' => 'breast'));
        DB::table('profile_params')
            ->where('id', $cloth_id)
            ->update(array('code' => 'cloth'));
        DB::table('profile_params')
            ->where('id', $shoes_id)
            ->update(array('code' => 'shoes'));
	}

}