<?php


class RolesCodeFieldTableSeeder extends Seeder {

	public function run()
	{
        DB::table('roles')
            ->where('name', 'admin')
            ->update(array('code' => 1));

        DB::table('roles')
            ->where('name', 'comment')
            ->update(array('code' => 2));

        DB::table('roles')
            ->where('name', 'Agent')
            ->update(array('code' => 3));

        DB::table('roles')
            ->where('name', 'Applicant')
            ->update(array('code' => 4));

        DB::table('roles')
            ->where('name', 'ApplicantManager')
            ->update(array('code' => 5));

        DB::table('roles')
            ->where('name', 'Organizer')
            ->update(array('code' => 6));
	}

}