<?php

class DatabaseSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        // Add calls to Seeders here
        $this->call('UsersTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('PermissionsTableSeeder');
        $this->call('EventsTypeTableSeeder');
        $this->call('ProfessionsTableSeeder');
        $this->call('SiteRolesTableSeeder');
        $this->call('ProfileParamsTableSeeder');
        $this->call('ProfileAdditionParamsTableSeeder');
        $this->call('ProfileAdditionOrderedParamTableSeeder');
        $this->call('ProfileAdditionParamsTypeFaceTableSeeder');
        $this->call('RolesProfileParamsTableSeeder');
        $this->call('UpdateParamProfileTypesTableSeeder');
        $this->call('UpdateParamProfileValidationTableSeeder');
        $this->call('FixInTypeProfileParamsTableSeeder');
		$this->call('RolesCodeFieldTableSeeder');
		$this->call('ProfileParamsFixChestWaistBreastTableSeeder');
		$this->call('ProfileParamsFixHeightTableSeeder');
		$this->call('ProfileParamsFixShoesClothTableSeeder');
		$this->call('ProfileParamsSearchTypeTableSeeder');
		$this->call('ProfileParamsAddCodesTableSeeder');
		$this->call('ProfileParamsUpdateLanguageFieldTableSeeder');
		$this->call('ValidationRulesProfileParamsUpdatesTableSeeder');
		$this->call('RolesProfileParamsAplicationManagerTableSeeder');
    }

}