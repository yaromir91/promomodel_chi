<?php

class RolesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();

        $adminRole = new Acme\Models\Role;
        $adminRole->name = 'admin';
        $adminRole->save();

        $commentRole = new Acme\Models\Role;
        $commentRole->name = 'comment';
        $commentRole->save();

        $user = Acme\Models\User::where('username','=','admin')->first();
        $user->attachRole( $adminRole );

        $user = Acme\Models\User::where('username','=','user')->first();
        $user->attachRole( $commentRole );
    }

}
