<?php



class ValidationRulesProfileParamsUpdatesTableSeeder extends Seeder {

	public function run()
	{
        // Рост
        $max = \Acme\Models\Profile\ProfileParams::MAX_HEIGHT;
        $min = \Acme\Models\Profile\ProfileParams::MIN_HEIGHT;

        $height_id = DB::table('profile_params')->where('name', '=', 'Рост')->pluck('id');

        DB::table('profile_params')
            ->where('id', $height_id)
            ->update(array('type' => 2, 'validation_rule' => 'integer|between:' . $min . ',' . $max));

        // Грудь Талия Бедра
        $chest_max = \Acme\Models\Profile\ProfileParams::MAX_CHEST;
        $chest_min = \Acme\Models\Profile\ProfileParams::MIN_CHEST;
        $waist_max = \Acme\Models\Profile\ProfileParams::MAX_WAIST;
        $waist_min = \Acme\Models\Profile\ProfileParams::MIN_WAIST;
        $breast_max = \Acme\Models\Profile\ProfileParams::MAX_BREAST;
        $breast_min = \Acme\Models\Profile\ProfileParams::MIN_BREAST;

        $chest_id = DB::table('profile_params')->where('name', '=', 'Грудь')->pluck('id');
        $waist_id = DB::table('profile_params')->where('name', '=', 'Талия')->pluck('id');
        $breast_id = DB::table('profile_params')->where('name', '=', 'Бедра')->pluck('id');


        DB::table('profile_params')
            ->where('id', $chest_id)
            ->update(array('type' => 2, 'validation_rule' => 'integer|between:' . $chest_min . ',' . $chest_max));
        DB::table('profile_params')
            ->where('id', $waist_id)
            ->update(array('type' => 2, 'validation_rule' => 'integer|between:' . $waist_min . ',' . $waist_max));
        DB::table('profile_params')
            ->where('id', $breast_id)
            ->update(array('type' => 2, 'validation_rule' => 'integer|between:' . $breast_min . ',' . $breast_max));

        DB::table('profile_params')
            ->where('name', 'Размер груди')
            ->update(array('validation_rule' => ''));

        DB::table('profile_params')
            ->where('name', 'Цвет глаз')
            ->update(array('validation_rule' => ''));

        DB::table('profile_params')
            ->where('name', 'Цвет волос')
            ->update(array('validation_rule' => ''));

        DB::table('profile_params')
            ->where('name', 'Длина волос')
            ->update(array('validation_rule' => ''));

        DB::table('profile_params')
            ->where('name', 'Тип внешности')
            ->update(array('validation_rule' => ''));

        DB::table('profile_params')
            ->where('name', 'Знание языков')
            ->update(array('validation_rule' => ''));

        DB::table('profile_params')
            ->where('name', 'Youtube link')
            ->update(array('validation_rule' => 'url'));

        DB::table('profile_params')
            ->where('name', 'Опыт работы')
            ->update(array('validation_rule' => ''));

        // shoes and cloth size
        $cloth_max = \Acme\Models\Profile\ProfileParams::MAX_CLOTH;
        $cloth_min = \Acme\Models\Profile\ProfileParams::MIN_CLOTH;
        $shoes_max = \Acme\Models\Profile\ProfileParams::MAX_SHOES;
        $shoes_min = \Acme\Models\Profile\ProfileParams::MIN_SHOES;

        $cloth_id = DB::table('profile_params')->where('name', '=', 'Размер одежды')->pluck('id');
        $shoes_id = DB::table('profile_params')->where('name', '=', 'Размер обуви')->pluck('id');


        DB::table('profile_params')
            ->where('id', $cloth_id)
            ->update(array('type' => 2, 'validation_rule' => 'integer|between:' . $cloth_min . ',' . $cloth_max));
        DB::table('profile_params')
            ->where('id', $shoes_id)
            ->update(array('type' => 2, 'validation_rule' => 'integer|between:' . $shoes_min . ',' . $shoes_max));
	}

}