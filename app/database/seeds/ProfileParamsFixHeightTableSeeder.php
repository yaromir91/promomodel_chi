<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProfileParamsFixHeightTableSeeder extends Seeder {

	public function run()
	{
        $max = \Acme\Models\Profile\ProfileParams::MAX_HEIGHT;
        $min = \Acme\Models\Profile\ProfileParams::MIN_HEIGHT;

        $height_id = DB::table('profile_params')->where('name', '=', 'Рост')->pluck('id');

        DB::table('profile_params')
            ->where('parent_id', $height_id)
            ->delete();
        DB::table('profile_params')
            ->where('id', $height_id)
            ->update(array('type' => 2, 'validation_rule' => 'required|integer|between:' . $min . ',' . $max));
	}

}