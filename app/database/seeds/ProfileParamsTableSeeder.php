<?php

// Composer: "fzaninotto/faker": "v1.3.0"

class ProfileParamsTableSeeder extends Seeder {

	public function run()
	{
        $siteRoles = [
            ['name' => 'Рост',          'type' => 1, 'parent_id' => null],
            ['name' => 'Грудь',         'type' => 1, 'parent_id' => null],
            ['name' => 'Талия',         'type' => 1, 'parent_id' => null],
            ['name' => 'Бедра',         'type' => 1, 'parent_id' => null],
            ['name' => 'Размер груди',  'type' => 1, 'parent_id' => null],
            ['name' => 'Размер одежды', 'type' => 1, 'parent_id' => null],
            ['name' => 'Размер обуви',  'type' => 1, 'parent_id' => null],
            ['name' => 'Цвет глаз',     'type' => 1, 'parent_id' => null],
            ['name' => 'Цвет волос',    'type' => 1, 'parent_id' => null],
            ['name' => 'Длина волос',   'type' => 1, 'parent_id' => null],
            ['name' => 'Тип внешности', 'type' => 1, 'parent_id' => null],
            ['name' => 'Знание языков', 'type' => 1, 'parent_id' => null],
            ['name' => 'Youtube link',  'type' => 2, 'parent_id' => null],
            ['name' => 'Опыт работы',   'type' => 3, 'parent_id' => null],
        ];

        DB::table('profile_params')->insert($siteRoles);
	}

}