<?php


class ProfileAdditionOrderedParamTableSeeder extends Seeder {

	public function run()
	{
        // grown
        $growth = DB::table('profile_params')->where('name', 'Рост')->first();

        if ($growth) {
            $growthArr = [
                ['name' => '<150',     'type' => 4, 'parent_id' => $growth->id],
                ['name' => '150-159',  'type' => 4, 'parent_id' => $growth->id],
                ['name' => '160-169',  'type' => 4, 'parent_id' => $growth->id],
                ['name' => '170-179',  'type' => 4, 'parent_id' => $growth->id],
                ['name' => '>180-189', 'type' => 4, 'parent_id' => $growth->id],
                ['name' => '>190',     'type' => 4, 'parent_id' => $growth->id],
            ];

            DB::table('profile_params')->insert($growthArr);
        }

        // breast
        $breast = DB::table('profile_params')->where('name', 'Грудь')->first();

        if ($breast) {
            $breastArr = [
                ['name' => '<80',   'type' => 4, 'parent_id' => $breast->id],
                ['name' => '80-89', 'type' => 4, 'parent_id' => $breast->id],
                ['name' => '90-99', 'type' => 4, 'parent_id' => $breast->id],
                ['name' => '>100',  'type' => 4, 'parent_id' => $breast->id],
            ];

            DB::table('profile_params')->insert($breastArr);
        }

        // Thalia
        $thalia = DB::table('profile_params')->where('name', 'Талия')->first();

        if ($thalia) {
            $thaliaArr = [
                ['name' => '<50',   'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '50-54', 'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '55-59', 'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '60-64', 'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '65-69', 'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '70-74', 'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '75-79', 'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '80-84', 'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '85-89', 'type' => 4, 'parent_id' => $thalia->id],
                ['name' => '>90',   'type' => 4, 'parent_id' => $thalia->id],
            ];

            DB::table('profile_params')->insert($thaliaArr);
        }

        // Hips
        $hips = DB::table('profile_params')->where('name', 'Бедра')->first();

        if ($hips) {
            $hipsArr = [
                ['name' => '<80', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '80',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '82',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '84',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '86',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '88',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '90',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '94',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '96',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '98',  'type' => 4, 'parent_id' => $hips->id],
                ['name' => '100', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '102', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '104', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '106', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '108', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '110', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '112', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '114', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '116', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '118', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '120', 'type' => 4, 'parent_id' => $hips->id],
                ['name' => '>120', 'type' => 4, 'parent_id' => $hips->id],
            ];

            DB::table('profile_params')->insert($hipsArr);
        }

        // Breast Size
        $breastSize = DB::table('profile_params')->where('name', 'Размер груди')->first();

        if ($breastSize) {
            $breastSizeArr = [
                ['name' => '<1', 'type' => 4, 'parent_id' => $breastSize->id],
                ['name' => '1',  'type' => 4, 'parent_id' => $breastSize->id],
                ['name' => '2',  'type' => 4, 'parent_id' => $breastSize->id],
                ['name' => '3',  'type' => 4, 'parent_id' => $breastSize->id],
                ['name' => '4',  'type' => 4, 'parent_id' => $breastSize->id],
                ['name' => '5',  'type' => 4, 'parent_id' => $breastSize->id],
                ['name' => '>5', 'type' => 4, 'parent_id' => $breastSize->id],
            ];

            DB::table('profile_params')->insert($breastSizeArr);
        }

        // clothing size
        $clothingSize = DB::table('profile_params')->where('name', 'Размер одежды')->first();

        if ($clothingSize) {
            $clothingSizeArr = [
                ['name' => '34-36', 'type' => 4, 'parent_id' => $clothingSize->id],
                ['name' => '38-40',  'type' => 4, 'parent_id' => $clothingSize->id],
                ['name' => '42-44',  'type' => 4, 'parent_id' => $clothingSize->id],
                ['name' => '46-48',  'type' => 4, 'parent_id' => $clothingSize->id],
                ['name' => '50-52',  'type' => 4, 'parent_id' => $clothingSize->id],
                ['name' => '>52',    'type' => 4, 'parent_id' => $clothingSize->id],
            ];

            DB::table('profile_params')->insert($clothingSizeArr);
        }

        // shoe size
        $shoeSize = DB::table('profile_params')->where('name', 'Размер обуви')->first();

        if ($shoeSize) {
            $shoeSizeArr = [
                ['name' => '34',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '35',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '36',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '37',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '38',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '39',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '40',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '41',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '42',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '43',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '44',  'type' => 4, 'parent_id' => $shoeSize->id],
                ['name' => '>44', 'type' => 4, 'parent_id' => $shoeSize->id],
            ];

            DB::table('profile_params')->insert($shoeSizeArr);
        }

        // eye color
        $eyeColor = DB::table('profile_params')->where('name', 'Цвет глаз')->first();

        if ($eyeColor) {
            $eyeColorArr = [
                ['name' => 'серые',   'type' => 4, 'parent_id' => $eyeColor->id],
                ['name' => 'карие',   'type' => 4, 'parent_id' => $eyeColor->id],
                ['name' => 'зеленые', 'type' => 4, 'parent_id' => $eyeColor->id],
                ['name' => 'голубые', 'type' => 4, 'parent_id' => $eyeColor->id],
            ];

            DB::table('profile_params')->insert($eyeColorArr);
        }

        // hair color
        $hairColor = DB::table('profile_params')->where('name', 'Цвет волос')->first();

        if ($hairColor) {
            $hairColorArr = [
                ['name' => 'черные', 'type' => 4, 'parent_id' => $hairColor->id],
                ['name' => 'шатен',  'type' => 4, 'parent_id' => $hairColor->id],
                ['name' => 'русые',  'type' => 4, 'parent_id' => $hairColor->id],
                ['name' => 'рыжие',  'type' => 4, 'parent_id' => $hairColor->id],
                ['name' => 'блонд',  'type' => 4, 'parent_id' => $hairColor->id],
            ];

            DB::table('profile_params')->insert($hairColorArr);
        }

        // hair length
        $hairLength = DB::table('profile_params')->where('name', 'Длина волос')->first();

        if ($hairLength) {
            $hairColorArr = [
                ['name' => 'короткие', 'type' => 4, 'parent_id' => $hairLength->id],
                ['name' => 'до плеч', 'type' => 4, 'parent_id' => $hairLength->id],
                ['name' => 'длинные',  'type' => 4, 'parent_id' => $hairLength->id],
            ];

            DB::table('profile_params')->insert($hairColorArr);
        }

        // hair length
        $hairLength = DB::table('profile_params')->where('name', 'Длина волос')->first();

        if ($hairLength) {
            $hairColorArr = [
                ['name' => 'короткие', 'type' => 4, 'parent_id' => $hairLength->id],
                ['name' => 'до плеч', 'type' => 4, 'parent_id' => $hairLength->id],
                ['name' => 'длинные',  'type' => 4, 'parent_id' => $hairLength->id],
            ];

            DB::table('profile_params')->insert($hairColorArr);
        }

        // languages
        $languages = DB::table('profile_params')->where('name', 'Знание языков')->first();

        if ($languages) {
            $languagesArr = [
                ['name' => 'английский',  'type' => 4, 'parent_id' => $languages->id],
                ['name' => 'немецкий',    'type' => 4, 'parent_id' => $languages->id],
                ['name' => 'французcкий',  'type' => 4, 'parent_id' => $languages->id],
                ['name' => 'итальянский', 'type' => 4, 'parent_id' => $languages->id],
                ['name' => 'испанский',   'type' => 4, 'parent_id' => $languages->id],
            ];

            DB::table('profile_params')->insert($languagesArr);
        }
	}

}