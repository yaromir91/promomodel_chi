<?php



class FixInTypeProfileParamsTableSeeder extends Seeder {

	public function run()
	{
        DB::table('profile_params')
            ->where('name', 'Youtube link')
            ->update(array('type' => 5));
	}

}