<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class EventsTypeTableSeeder extends Seeder {

	public function run()
	{
        DB::table('events_type')->delete();


        $events_types = array(
            array(
                'title'      => 'Промо акция'
            ),
            array(
                'title'      => 'RP мероприятие'
            ),
            array(
                'title'      => 'Фотовыставка'
            ),
            array(
                'title'      => 'Автошоу'
            ),
            array(
                'title'      => 'Презентация'
            )
        );

        DB::table('events_type')->insert( $events_types );
	}

}