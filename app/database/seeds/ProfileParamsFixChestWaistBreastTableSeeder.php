<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProfileParamsFixChestWaistBreastTableSeeder extends Seeder {

	public function run()
	{
        $chest_max = \Acme\Models\Profile\ProfileParams::MAX_CHEST;
        $chest_min = \Acme\Models\Profile\ProfileParams::MIN_CHEST;
        $waist_max = \Acme\Models\Profile\ProfileParams::MAX_WAIST;
        $waist_min = \Acme\Models\Profile\ProfileParams::MIN_WAIST;
        $breast_max = \Acme\Models\Profile\ProfileParams::MAX_BREAST;
        $breast_min = \Acme\Models\Profile\ProfileParams::MIN_BREAST;

        $chest_id = DB::table('profile_params')->where('name', '=', 'Грудь')->pluck('id');
        $waist_id = DB::table('profile_params')->where('name', '=', 'Талия')->pluck('id');
        $breast_id = DB::table('profile_params')->where('name', '=', 'Бедра')->pluck('id');

        DB::table('profile_params')
            ->where('parent_id', $chest_id) // delete chest parent fields
            ->delete();
        DB::table('profile_params')
            ->where('parent_id', $waist_id) // delete waist parent fields
            ->delete();
        DB::table('profile_params')
            ->where('parent_id', $breast_id) // delete breast parent fields
            ->delete();

        DB::table('profile_params')
            ->where('id', $chest_id)
            ->update(array('type' => 2, 'validation_rule' => 'required|integer|between:' . $chest_min . ',' . $chest_max));
        DB::table('profile_params')
            ->where('id', $waist_id)
            ->update(array('type' => 2, 'validation_rule' => 'required|integer|between:' . $waist_min . ',' . $waist_max));
        DB::table('profile_params')
            ->where('id', $breast_id)
            ->update(array('type' => 2, 'validation_rule' => 'required|integer|between:' . $breast_min . ',' . $breast_max));
	}

}