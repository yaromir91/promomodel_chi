<?php


class UpdateParamProfileValidationTableSeeder extends Seeder {

	public function run()
	{
        DB::table('profile_params')
            ->where('name', 'Рост')
            ->update(array('validation_rule' => 'required|integer'));

        DB::table('profile_params')
            ->where('name', 'Грудь')
            ->update(array('validation_rule' => 'required|integer'));

        DB::table('profile_params')
            ->where('name', 'Талия')
            ->update(array('validation_rule' => 'required|integer'));

        DB::table('profile_params')
            ->where('name', 'Бедра')
            ->update(array('validation_rule' => 'required|integer'));

        DB::table('profile_params')
            ->where('name', 'Youtube link')
            ->update(array('validation_rule' => 'required|url'));
	}

}