<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProfessionsTableSeeder extends Seeder {

	public function run()
	{
        DB::table('professions')->delete();


        $professions = array(
            array(
                'title'      => 'Промо-модель'
            ),
            array(
                'title'      => 'Промоутер'
            ),
            array(
                'title'      => 'Аниматор'
            ),
            array(
                'title'      => 'Фото-модель'
            ),
            array(
                'title'      => 'Бодиарт-модель'
            )
        );

        DB::table('professions')->insert( $professions );
	}

}