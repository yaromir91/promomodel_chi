<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProfileParamsFixShoesClothTableSeeder extends Seeder {

	public function run()
	{
        $cloth_max = \Acme\Models\Profile\ProfileParams::MAX_CLOTH;
        $cloth_min = \Acme\Models\Profile\ProfileParams::MIN_CLOTH;
        $shoes_max = \Acme\Models\Profile\ProfileParams::MAX_SHOES;
        $shoes_min = \Acme\Models\Profile\ProfileParams::MIN_SHOES;

        $cloth_id = DB::table('profile_params')->where('name', '=', 'Размер одежды')->pluck('id');
        $shoes_id = DB::table('profile_params')->where('name', '=', 'Размер обуви')->pluck('id');

        DB::table('profile_params')
            ->where('parent_id', $cloth_id) // delete cloth parent fields
            ->delete();
        DB::table('profile_params')
            ->where('parent_id', $shoes_id) // delete shoes parent fields
            ->delete();

        DB::table('profile_params')
            ->where('id', $cloth_id)
            ->update(array('type' => 2, 'validation_rule' => 'required|integer|between:' . $cloth_min . ',' . $cloth_max));
        DB::table('profile_params')
            ->where('id', $shoes_id)
            ->update(array('type' => 2, 'validation_rule' => 'required|integer|between:' . $shoes_min . ',' . $shoes_max));
	}

}