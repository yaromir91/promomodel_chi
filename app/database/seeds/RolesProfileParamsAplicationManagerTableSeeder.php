<?php



class RolesProfileParamsAplicationManagerTableSeeder extends Seeder {

	public function run()
	{
        $applicantRole = DB::table('roles')->where('name', 'ApplicantManager')->first();
        $params        = DB::table('profile_params')->get();

        if ($applicantRole && $params) {
            foreach ($params as $p) {
                $roleParam = [
                    ['profile_params_id' => $p->id, 'roles_id' => $applicantRole->id],
                ];

                DB::table('roles_profile_params')->insert($roleParam);
            }
        }
	}

}