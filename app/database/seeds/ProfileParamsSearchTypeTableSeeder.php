<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProfileParamsSearchTypeTableSeeder extends Seeder {

    /**
     *  search_type
     * 1: range ползунок
     * 2: чекбоксы
     */
	public function run()
	{
        DB::table('profile_params')
            ->where('name', 'Рост')
            ->update(array('search_type' => 1));
        DB::table('profile_params')
            ->where('name', 'Грудь')
            ->update(array('search_type' => 1));
        DB::table('profile_params')
            ->where('name', 'Талия')
            ->update(array('search_type' => 1));
        DB::table('profile_params')
            ->where('name', 'Бедра')
            ->update(array('search_type' => 1));
        DB::table('profile_params')
            ->where('name', 'Размер груди')
            ->update(array('search_type' => 2));
        DB::table('profile_params')
            ->where('name', 'Размер одежды')
            ->update(array('search_type' => 1));
        DB::table('profile_params')
            ->where('name', 'Размер обуви')
            ->update(array('search_type' => 1));
        DB::table('profile_params')
            ->where('name', 'Цвет глаз')
            ->update(array('search_type' => 2));
        DB::table('profile_params')
            ->where('name', 'Цвет волос')
            ->update(array('search_type' => 2));
        DB::table('profile_params')
            ->where('name', 'Длина волос')
            ->update(array('search_type' => 2));
        DB::table('profile_params')
            ->where('name', 'Тип внешности')
            ->update(array('search_type' => 2));
        DB::table('profile_params')
            ->where('name', 'Знание языков')
            ->update(array('search_type' => 2));
	}

}