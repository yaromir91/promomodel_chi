<?php

return array(
    'title'         => 'Соискатели',
    'main_info'     => 'Информация',
    'search'        => 'Поиск Соискателей',
    'search_result' => 'Результат поиска Соискателей',
    'online_users'      => 'Пользователи Online',
    'title_public_user' => 'Пользователи',
    'auth_error_search' => 'Поиск доступен только авторизованным пользователям'
);