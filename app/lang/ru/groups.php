<?php

return array(
    'title' => 'Группы',
    'tab_all'    => 'Все группы',
    'tab_subscribe_group'    => 'Мои группы',
    // default field
    'title_title' => 'Название',
    'title_posts'   => '',
    'title_description' => 'Описание',
    'title_photo' => 'Фото',
    'title_status' => 'Статус',
    // Notices for group
    'title_create_group' => 'Создать группу',
    'search' => 'Поиск участника',
    'search_result' => 'Поиск не дал результатов',
    'title_show_group' => 'Группа',
    'title_groups_data' => 'Дата',
    'title_groups_profession' => 'Профессия',
    'title_groups_need' => 'Требуется',
    'title_groups_accepted' => 'Подтверждено',
    'title_groups_h_rate' => 'Почасовая ставка',
    'title_groups_d_rate' => 'Дневная ставка',
    'title_groups_details' => 'Детали',
    'title_groups_start_date' => 'Дата начала',
    'title_groups_end_date' => 'Дата окончания',
    'title_groups_place' => 'Мето проведения',
    'title_groups_owner' => 'Организатор',
    'title_edit_group' => 'Редактировать группу',
    'group_success_update' => 'Группа обновлена',
    'group_success_add' => 'Гуппа добавлена',
    'group_error_update' => 'Не удалось обновить группу',
    'group_not_access' => 'У вас нет прав на редактирование',
    'title_groups_type' => 'Тип группы',
    'delete_confirmation' => 'Вы уверены, что хотите удалить эту группу? Внимание! Группа будет удалена вместе со всей сопутствующей информацией.',
    'group_user_error_delete' => 'Этот пользователь не является создателем группы',
    'group_success_delete' => 'Группа удалена',
    'group_comment_success_delete' => 'Комментарий удален',
    'title_group_invite' => 'Пригласить пользователей',
    'title_group_group' => 'Группы',
    'title_group_search' => 'Поиск кандидатов',
    'role_error' => 'Вы не можете создавать группы',
    // Subscribe
    'subscribe' => 'Вы уверены, что хотите вступить в эту группу?',
    'unsubscribe' => 'Вы уверены, что хотите покинуть эту группу?',
    'subscribe_unsubscribe' => 'Вы уверены, что хотите удалить участника из этой группы?',
    'subscribe_success' => 'Вы являетесь участником этой группы',
    'subscribe_unsubscribe_success' => 'Участник успешно удален из группы',
    'subscribers_title' => 'Участники',
    'subscribe_role_error' => 'Вы не можете выйти из группы',
    //Validate
    'exists_subscribe' => 'Вы уже являетесь участником другой группы',
    'exists_group'     => 'Вы уже имеете группу',
    'exists_user_invite'     => 'Вы уже отправили приглашение данному пользователю',
    'group_deleted_description' => 'Все пользователи текущей группы будут удалены',
    'group_deleted' => 'Группа удалена',

    //Comments
    'title_comments' => 'коментарии',
    'comment_role_error' => 'У вас не хватает прав',
    'confirm-delete-comment' => 'Вы действительно хотите удалить комментарий?',

    //Members
    'title_members' => 'Участники',

    // Invite
    'invite' => 'Пригласить',
    'invite_not_active' => 'Не потвержденно',
    'invite_description' => 'Вы уверены, что хотите пригласить ?',
    //// Mail
    'invite_mail_title' => 'Приглашение в группу',
    'invite_mail_title_refuse' => 'Отказ от приглашение',
    'invite_mail_title_accept' => 'Согласие на приглашение',
    'invite_mail_success' => 'Для того что бы подтвердить перейдите по ',
    'invite_mail_refuse' => ', что бы отказаться перейдите по вот этой ',
    'invite_mail_show_user' => 'Что бы узнать кто отказался от приглашения перейдите по ',
    'invite_mail_token_expires' => 'Сcылка не активна',
    // End Invite
    'tab_my_invitation'         => 'Мои приглашения',
    'title_groups_list'         => 'Приглашения',
    'invite_accept_question'    => 'Вы действительно хотите, принять это приглашение?',
    'group_members' => 'Участники группы',
    'delete_comment_confirm' => 'Вы уверены, что хотите удалить этот комментарий?',
);