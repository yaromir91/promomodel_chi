<?php

return [
    'user_chat'    => 'Пользовательский чат',
    'your_message' => 'Ваше сообщение',
    'me'           => 'Я',
    'delete'       => 'удалить',
    'delete_msg'   => 'Удалить сообщение?',
    'me_msg'       => 'Я',
    'history'      => 'история',
    'show_history' => 'показать историю',
    'hide_history' => 'скрыть историю',
    'user_chats'   => 'Диалоги',
    'unread'       => 'непрочитанных сообщений',
    'empty_list'   => 'Диалогов нет',
    'show_else'    => 'показать еще',
    'send'         => 'Отправить',
];