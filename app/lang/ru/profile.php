<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Site Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Username'          => 'Ник',
    'Email'             => 'Email',
    'Password'          => 'Пароль',
    'NewPassword'       => 'Новый пароль',
    'OldPassword'       => 'Старый пароль',
    'Password Confirm'  => 'Подтверждение пароля',
    'Full name'         => 'Настоящее имя',
    'Gender'            => 'Пол',
    'Date Birth'        => 'Дата рождения',
    'Phone'             => 'Номер телефона',
    'VK link'           => 'ссылка на VK',
    'LastName'          => 'Фамилия',
    'FirstName'         => 'Имя',
    'phone'             => 'Телефон',
    'vk_link'           => 'Ссылка в VK',
    'Gender'            => 'Пол',
    'Male'              => 'муж',
    'Female'            => 'жен',
    'DateBirth'         => 'Дата рождения',
    'payment_count'     => 'Номер счета',

    'profile_was_successfully_saved' => 'Профиль сохранен',
    'password_was_successfully_saved'=> 'Профиль обновлен',

    'eventtypes'         => 'Тип мероприятия',
    'professions_rates'  => 'Рейты',
    'rate'               => 'ставка',
    'rate_h'             => 'Почасовая ставка',
    'rate_d'             => 'Дневная ставка',
    'is_ready_manager'   => 'Готов(а) быть менеджером',
    'account_type'       => 'Тип аккаунта',
    'PasswordConfirm'    => 'Подтверждение пароля',
    'title'              => 'Профиль',

    'main'               => 'Основные',
    'additional'         => 'Дополнительные',
    'password'           => 'Пароль',
    'old_password_not_correct'      => 'Старый пароль не правильный',
    'error_in_password_update'      => 'Не удалось обновить пароль',
    'Avatar'             => 'Аватар',
    'public'             => 'Публичный профиль?',
    'profile_settings'   => 'Настройки профиля',
    'choose_role_pass'   => 'Выберите роль',

    'age'                => 'Возраст',
    'click_here'         => 'нажмите здесь',
    'my_applicants'      => 'Мои соискатели',
    'my_messages'        => 'Сообщения',
);
