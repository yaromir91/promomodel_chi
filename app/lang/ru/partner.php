<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Site Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'message'           => array(
        'register_partner_succes'       => 'Операция успешно выполнена Для подтверждения Вашего аккаунта перейдите на Вашу почту и нажмите на ссылку в письме. После ожидайте активации администратором',
    ),

    'title' => 'Наши партнеры',
    'menu_partner' => 'Наши партнеры',
    'date' => 'Дата публикации',
    'details' => 'Детали',
    'url' => 'Сайт',
    'email' => 'Email',
    'phone' => 'Телефон',
    'adress' => 'Адрес',
    'description' => 'Описание',
    'name' => 'Название'
);