<?php

return array(
    'title' => 'Отчет',
    'my_title' => 'Мои отчеты',
    'all_reports' => 'Все отчеты',
    'title_list_popular_report' => 'Популярные отчеты',
    'menu_report' => 'Отчеты',
    'tab_all' => 'Все отчеты',
    'tab_my' => 'Мои отчеты',
    'tab_rate' => 'По рейту',
    'my_all' => 'Все',
    'tab_popular' => 'Популярные отчеты',
    'tab_arhive' => 'Архивные отчеты',
    'tab_tag' => 'Тэги',
    'title_create_report' => 'Создать новый отчет',
    'title_show_report' => 'Отчет',
    'form_events' => 'Мероприятия',
    'form_event' => 'Мероприятие',
    'form_title' => 'Название',
    'form_content' => 'Содержание',
    'form_meta_title' => 'Meta title',
    'form_meta_description' => 'Meta description',
    'form_meta_keywords' => 'Meta keywords',
    'form_tags' => 'Тэги',
    'messages' => [
        'create' => array(
            'error'   => 'Не удалось создать отчет, попробуйте еще раз.',
            'success' => 'Отчет создан.'
        ),

        'update' => array(
            'error'   => 'Не удалось обновить отчет, попробуйте еще раз.',
            'success' => 'Отчет обновлен.'
        ),

        'delete' => array(
            'error'   => 'При удалении отчета возникла проблема. Попробуйте еще раз.',
            'success' => 'Отчет удален.'
        ),
    ],

    'comment' => [
        'validation_error'  => 'Заполните необходимые поля',
        'save_success'      => 'Комментарий добавлен'
        ],
    'event_report' => 'Отчеты мероприятия',
    'choose_event'  => 'Выберите мероприятие',
    'tab_my_reports'    => 'Мои отчеты',
    'filter_all'    => 'Все',
    'filter_published'    => 'Опубликованные',
    'filter_archive'    => 'Архивные',
    'form_status'   => 'Черновик',
    'confirm_delete_msg'   => 'Вы уверены, вы хотите удалить этот отчет?',
    'comment_upprove'   => 'опубликовать',
    'comment_edit'   => 'редактировать',
    'comment_delete'   => 'удалить',
    'image' => 'Картинка',
    'title_edit_report' => 'Редактирование отчета',
    'separate_words'    => 'Разделите слова запятыми',
    'description'       => 'Описание',
    'details'           => 'Детали',
    'publeshed'         => 'Опубликовано',
    'add_comment'       => 'Добавить комментарий',
    'title_photo'       => 'Картинка',
    'status'            => 'Статус отчета',
    'author'            => 'Автор',
);