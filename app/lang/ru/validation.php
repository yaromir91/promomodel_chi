<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"         => "Необходимо принять.",
	"active_url"       => "Недействительный URL адрес.",
//	"after"            => ":attribute должна быть после :date.",
    'after'            => 'Дата должна быть после :date.',
	"alpha"            => ":attribute может содержать только буквы.",
	"alpha_dash"       => "Данное поле может содержать только буквы, числа, тире и нижнее подчеркивание.",
	"alpha_num"        => "Данное поле может содержать только буквы и числа.",
	"before"           => ":attribute должна быть до :date.",
	"between"          => array(
		"numeric" => ":attribute должно быть между :min - :max.",
		"file"    => ":attribute должно быть между :min - :max kb.",
		"string"  => ":attribute должно быть между :min - :max знаками.",
	),
	"confirmed"        => ":attribute подтверждение не совпадает.",
	"date"             => ":attribute недействительная дата.",
	"date_format"      => "Не соответствует формату :format.",
	"different"        => "Должен быть другим.",
	"digits"           => "Должно быть от 1 до 9.",
	"digits_between"   => ":attribute должно быть между :min и :max.",
	"email"            => ":attribute недействительный формат.",
	"exists"           => "Выбранный формат недействителен.",
	"image"            => "Должна быть картинка.",
	"in"               => "Выбранное недействительно.",
	"integer"          => "Должен быть цифрой.",
	"ip"               => "Должен быть правильным IP адресом.",
	"max"              => array(
		"numeric" => "Не должно быть больше чем :max.",
		"file"    => "Не должно быть больше чем :max kb.",
		"string"  => "Не должно быть больше чем :max символов.",
	),
	"mimes"            => "Файл должен быть типа: :values.",
	"min"              => array(
		"numeric" => "Не должно быть меньше чем :min.",
		"file"    => "Не должно быть меньше чем :min kb.",
		"string"  => "Не должно быть меньше чем :min символов.",
	),
	"not_in"           => "Выбранное недействительно.",
	"numeric"          => "Должен быть цифрой.",
	"regex"            => "Неправильный формат.",
	"required"         => "Данное поле обязательно для заполнения.",
	"required_if"      => "Данное поле обязательно для заполнения когда :other есть :value.",
	"required_with"    => "Данное поле обязательно для заполнения когда присуствует :values.",
	"required_without" => "Данное поле обязательно для заполнения когда нет :values.",
	"same"             => "Должно соответствовать.",
	"size"             => array(
		"numeric" => "Должно быть :size.",
		"file"    => "Должно быть :size kb.",
		"string"  => "Должно быть :size символов.",
	),
	"unique"           => "Данный :attribute уже занят.",
	"url"              => "Данный :attribute формат не верен.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
		'report_date' 	=> 'Дата отчета',
		'date_end' 		=> 'Дата начала',
		'date_start' 	=> 'Дата окончания',
	),

	'custom' => array(
		'date_end' => array(
			'after' => 'Дата окончания должна быть после даты начала',
		),
		'report_date' => array(
			'before'    => 'Дата отчета должна быть до даты начала',
            'after'     => 'Не верная дата отчета со списком согласившихся соискателей'
		),
	),

);
