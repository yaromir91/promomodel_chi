<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Пароль должен быть не меньше 6 символов и совпадать с подтверждением.",

	"user"     => "Пользователя с таким e-mail адресом не найдено.",

	"token"    => "Токен для изменения пароля не верный.",

	"sent"     => "Напоминание пароля отправлено!",

	"notice_of_profile"     => "Для использования всех возможностей сайта заполните, пожалуйста,",
);