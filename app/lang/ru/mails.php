<?php

return array(
    'thank_for_registering.'                => 'Благодарим за регистрацию.',
    'new_generated_password'                => 'Пароль создан.',
    'you_can_change_password'               => 'При необходимости, вы можете поменять пароль в настройках.',
    'you_invited_to_event'                  => 'Вы были приглашены на участие в мероприятии',
    'you_invited_to_agent'                  => 'Вы были приглашены на участие в мероприятии',
    'you_was_invited_agent'                 => 'Вы были приглашены стать сотрудником агента.',
    'you_was_invited_new_event'             => 'Вы были приглашены на мероприятие.',
    'you_was_invited_as_manager_new_event'  => 'Вы были приглашены как менеджер на мероприятие.',
    'event_cancel_message'                  => 'Организатор %s просит отменить оплаченное мероприятие.',
    'you_invited_to_be_manager_event'       => 'Вы были приглашены как менеджер на мероприятие.',
    'you_can_view_invitations'              => 'Вы можетет просмотреть свои приглашения на странице',
    'your_profession'                       => 'Ваша профессия',
    'report_invitation_event'               => 'Отчет о приглашениях на мероприятие',

    'these_candidates_accepted_invitation'   => 'Эти кандидаты приняли Ваше приглашение на мероприятие',
    'any_candidates_not_accepted_invitation' => 'Ни одни из кандидатов не принял Ваше приглашение на мероприятие',

    'manager'    => 'менеждер',
    'applicant'  => 'соискатель',
    'click_here' => 'Нажмите сюда',

    'user_rejected_invitation_event'        => 'Пользователь %s отклонил Ваше приглашение на это мероприятие',
    'user_accepted_invitation_event'        => 'Пользователь %s принял Ваше приглашение на это мероприятие',
    'you_can_find_other_applicants_on_page' => 'Вы можете найти другого соискателя на этой странице',
    'link'                    => 'ссылка',
    'as_a_manager'  => 'как участник',
    'on_profession' => 'на профессию',

    'you_deleted_from_team' => 'Вы были удалены из команды этого мероприятия',
    'dear' => 'Уважаемый',
    'event_deleted' => 'Событие удалено',
    'this_event_deleted' => 'Это мероприятие было удалено',
    'you_was_deleted' => 'Вы были удалены с сайта',
    'link'                    => 'ссылке',
    'created_by'                    => 'создано',
    'the_event'                    => 'Мероприятие',


    'new_user_password' => array(
        'subject'       => 'Подтверждение регистрации',
        'greetings'     => 'Здравствуйте, :name',
        'body_password' => 'Ваш пароль :password',
        'body'          => 'Пожалуйста, перейдите по указанной ниже ссылке для подтверждения регистрации.',
        'farewell'      => 'С уважением,',
    ),
    'invite_mail_title' => 'Приглашение в группу',
    'invite_mail_title_refuse' => 'Отказ от приглашение',
    'invite_mail_title_accept' => 'Согласие на приглашение',

);
