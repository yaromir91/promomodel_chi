<?php

return array(

    'sLengthMenu'  => 'Показать _MENU_ записей',
    'sProcessing'  => 'Подождите...',
    'sZeroRecords' => 'Записи отсутствуют.',

    'paginate' => [
        'previous' => 'Пред.',
        'next' => 'След.',
    ]
);