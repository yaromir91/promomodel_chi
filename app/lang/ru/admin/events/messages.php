<?php

return array(

	'delete' => array(
		'error'   => 'Произошла ошибка при удалении, повторите пожалуйста',
		'success' => 'Мероприятие было успешно удалено'
	)

);
