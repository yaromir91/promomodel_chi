<?php

return array(
    'payments'                  => 'Управление платежами',
    'users_list'                => 'Список пользователей',
    'paid'                      => 'Оплачено',
    'not_paid'                  => 'Не оплачено',
    'pay'                       => 'оплачено',
    'not_pay'                   => 'не оплачено',
    'payments_confirm'          => 'Подтверждение оплаты',
    'payments_cancel'           => 'Отмена оплаты',
    'payment_color_end'         => 'Участники мероприятия ожидают оплату',
    'payment_color_complete'    => 'Участники мероприятия получили оплату',
);