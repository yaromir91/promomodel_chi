<?php

return array(

	'user_management'    => 'Управление пользователем',
	'user_update'        => 'Обновить пользователя',
	'user_delete'        => 'Удалить пользователя',
	'create_a_new_user'  => 'Создать нового пользователя',
	'General'            => 'Основные',
    'additional'         => 'Дополнительные',
    'Avatar'             => 'Аватар',
    'password'           => 'Пароль',
    'profile_params'     => 'Параметры профиля',
    'user_activate'      => 'Активировать пользователя'

);
