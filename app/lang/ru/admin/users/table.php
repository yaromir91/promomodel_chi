<?php

return array(

	'first_name' => 'Имя',
	'last_name'  => 'Фамилия',
	'user_id'    => '# пользователя',
	'username'   => 'Имя пользователя',
	'email'      => 'Email',
	'groups'     => 'Группы',
	'roles'      => 'Роли',
	'activated'  => 'Активировано',
	'created_at' => 'Дата создания',

);
