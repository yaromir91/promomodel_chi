<?php

return array(

    'partners' => 'Партнеры',
    'partners_list' => 'Список партнеров',
    'create' => 'Добавить партнера',
    'delete' => 'Удалить партнера',
    'update' => 'Редактировать партнера'
);