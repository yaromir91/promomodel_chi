<?php

return [
    'logo'        => 'Логотип',
    'description' => 'Описание',
    'adress' => 'Адрес',
    'active' => 'Статус',
    'active_yes' => 'Активно',
    'active_no' => 'Пасивно'
];