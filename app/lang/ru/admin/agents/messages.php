<?php

return array(

	'already_exists'    => 'Такой пользователь уже существует!',
	'does_not_exist'    => 'Такого пользователя нет.',
	'login_required'    => 'Поле login обязательное',
	'password_required' => 'Поле password обязательное.',
	'password_does_not_match' => 'Неправильный формат пароля.',

    'activate' => array(
        'error'      => 'произошла ошибка при активации агента',
        'success'    => 'Агент был успешно активирован.'
    ),
    'deactivate' => array(
        'error'      => 'произошла ошибка при деактивации агента',
        'success'    => 'Агент был успешно деактивирован.'
    )

);
