<?php

return array(
    'reports'           => 'Отчеты',
    'reports_list'      => 'Список отчетов',
    'update_reports'    => 'Редактирование отчета',
    'comments_list'     => 'Список комментариев',
    'edit_comment'      => 'Редактирование комментария',
    'delete_comment'    => 'Удалить комментарий',
    'create_comment'    => 'Добавить комментарий',
    'delete_reports'    => 'Удалить отчет',
    'create_report'     => 'Создать отчет'
);