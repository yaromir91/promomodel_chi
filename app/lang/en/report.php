<?php

return array(
    'title' => 'Report',
    'all_reports' => 'All reports',
    'title_list_popular_report' => 'Popular reports',
    'menu_report' => 'Reports',
    'tab_all' => 'All reports',
    'tab_my' => 'My reports',
    'tab_rate' => 'By rating',
    'my_all' => 'All',
    'tab_popular' => 'Popular reports',
    'tab_arhive' => 'Archive reports',
    'tab_tag' => 'Tags',
    'title_create_report' => 'Create new report',
    'title_show_report' => 'Report',
    'form_events' => 'Events',
    'form_event' => 'Event',
    'form_title' => 'Title',
    'form_content' => 'Content',
    'form_meta_title' => 'Meta title',
    'form_meta_description' => 'Meta description',
    'form_meta_keywords' => 'Meta keywords',
    'form_tags' => 'Tags',
    'messages' => [
        'create' => array(
            'error'   => 'Failed to create a report, try again.',
            'success' => 'The report is created.'
        ),

        'update' => array(
            'error'   => 'Failed to update report, please try again.',
            'success' => 'The report is updated.'
        ),

        'delete' => array(
            'error'   => 'When you delete a report a problem. Try again.',
            'success' => 'The report has been deleted.'
        ),
    ],

    'comment' => [
        'validation_error'  => 'Fill in the fields',
        'save_success'      => 'Comment added'
        ],
    'event_report' => 'Reports of the event',
    'choose_event'  => 'Choose event',
    'tab_my_reports'    => 'My reports',
    'filter_all'    => 'All',
    'filter_published'    => 'Published',
    'filter_archive'    => 'Archive',
    'form_status'   => 'Draft',
    'confirm_delete_msg '   => 'Are you sure you want to delete this report?',
    'comment_upprove'   => 'upprove',
    'comment_edit'   => 'edit',
    'comment_delete'   => 'delete',
    'image' => 'Image',
    'title_edit_report' => 'Editing the report',
    'separate_words'    => 'Separate words with commas',
    'description'       => 'Description',
    'details'           => 'Details',
    'publeshed'         => 'Publeshed',
    'add_comment'       => 'Add a comment',
    'title_photo'       => 'Photo',
    'status'            => 'Report status',
    'author'            => 'Author',
);