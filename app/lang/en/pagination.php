<?php

return array(

    'sLengthMenu'  => 'Show _MENU_ records',
    'sProcessing'  => 'Wait...',
    'sZeroRecords' => 'Records missing.',

    'paginate' => [
        'previous' => 'Prev.',
        'next' => 'Next.',
    ]
);