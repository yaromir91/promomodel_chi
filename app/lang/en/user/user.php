<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| User Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'register'              => 'Register',
	'login'                 => 'Login',
	'login_first'           => 'Login first',
	'account'               => 'Account',
	'forgot_password'       => 'Forgot Password',
	'settings'              => 'Settings',
	'profile'               => 'Profile',
	'events'                => 'Events',
	'users'                 => 'Users',
	'user_account_is_not_confirmed' => 'User Account is not confirmed.',
	'user_account_updated'          => 'User Account updated.',
	'user_account_created'          => 'User Account created.',
	'confirm_your_account'          => 'To confirm your account, go to your mail :email, and click on the link in the email.',
        'login_title'           => 'Login to your account',
        'login_welcome'         => 'Welcome',
        'login_sigin_title'     => 'Please login',

	'my_photos'		    => 'My Photos',
	'my_albums'		    => 'My Albums',
	'my_groups'		    => 'My Groups',
	'invitations'	    => 'Invitations',
	'posts'			    => 'Messages',
	'notification'	    => 'Notification',
	'my_reports'	    => 'My Reports',
	'my_events'	        => 'My Events',
        'my_notifications'  => 'My Notifications',
        'zizaco_user_name'  => 'Nickname',
        'forgot_password_title' => 'Password recovery',
        'filter_all' => 'All',
        'filter_online' => 'Online',
        'filter_search' => 'Search',
    'message_01' => 'Select Account Type (The applicant - if you are looking for a job, The employer - if you are looking for people)',
    'message_02' => 'Only letters, numbers, dashes and underscores',
    'message_03' => 'The password must consist of 6-16 characters, You can use uppercase and lowercase letters (A-Z, a-z), numbers (0-9).',
    'join' => 'Join',
    'username_e_mail' => 'Nickname or email',
);