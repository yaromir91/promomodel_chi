<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Site Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Username'          => 'Nickname',
    'Email'             => 'Email',
    'Password'          => 'Password',
    'NewPassword'       => 'New password',
    'OldPassword'       => 'Old password',
    'Password Confirm'  => 'Password confirm',
    'Full name'         => 'Full name',
    'Gender'            => 'Gender',
    'Date Birth'        => 'Date birth',
    'Phone'             => 'Phone',
    'VK link'           => 'VK link',
    'LastName'          => 'Last Name',
    'FirstName'         => 'First Name',
    'phone'             => 'Phone',
    'vk_link'           => 'VK link',
    'Gender'            => 'Gender',
    'Male'              => 'Male',
    'Female'            => 'Female',
    'DateBirth'         => 'DateBirth',
    'payment_count'     => 'Payment number',

    'profile_was_successfully_saved' => 'The profile was successfully saved',
    'password_was_successfully_saved'=> 'The password was successfully updated',

    'eventtypes'         => 'Event Types',
    'professions_rates'  => 'Professions Rates',
    'rate'               => 'rate',
    'rate_h'             => 'hourly rate',
    'rate_d'             => 'daily rate',
    'is_ready_manager'   => 'Are you ready to be a manager?',
    'account_type'       => 'Type of account',
    'PasswordConfirm'    => 'Password Confirm',
    'title'              => 'Profile',

    'main'               => 'Main',
    'additional'         => 'Additional',
    'password'           => 'Password',
    'old_password_not_correct'      => 'Old password not correct',
    'error_in_password_update'      => 'Error in password update',
    'Avatar'             => 'Avatar',
    'public'             => 'Is a public profile?',
    'profile_settings'   => 'Profile settings',
    'choose_role_pass'   => 'Choose your role',

    'age'                => 'Age',
    'click_here'         => 'click here',
    'my_applicants'      => 'My job seekers',
    'my_messages'        => 'Messages',
);
