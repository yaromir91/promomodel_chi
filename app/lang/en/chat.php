<?php

return [
    'user_chat'    => 'User chat',
    'your_message' => 'Your message',
    'me'           => 'Me',
    'delete'       => 'delete',
    'delete_msg'   => 'Delete message?',
    'me_msg'       => 'Me',
    'history'      => 'history',
    'show_history' => 'show history',
    'hide_history' => 'hide history',
    'user_chats'   => 'Dialogues',
    'unread'       => 'unread messages',
    'empty_list'   => 'No dialogue',
    'show_else'    => 'show more',
    'send'         => 'Send',
];