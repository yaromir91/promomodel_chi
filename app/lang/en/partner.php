<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Site Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'message'           => array(
        'register_partner_succes'       => 'The operation was successfully performed to confirm your account, go to your post and click on the link in the email. After activation expect administrator',
    ),

    'title' => 'Our partners',
    'menu_partner' => 'Our partners',
    'date' => 'Publishing date',
    'details' => 'Details',
    'url' => 'Site',
    'email' => 'Email',
    'phone' => 'Phone',
    'adress' => 'Adress',
    'description' => 'Description',
    'name' => 'Title'
);