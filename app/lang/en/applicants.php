<?php

return array(
    'title'         => 'Applicants',
    'main_info'     => 'Information',
    'search'        => 'Search Applicants',
    'search_result' => 'Showing Applicants',
    'online_users'      => 'Users Online',
    'title_public_user' => 'Users',
    'auth_error_search' => 'Search is available only to authorized users'
);