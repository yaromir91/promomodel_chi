<?php

return array(
    'title' => 'Groups',
    'tab_all'    => 'All groups',
    'tab_subscribe_group'    => 'My groups',
    // default field
    'title_title' => 'Title',
    'title_posts'   => '',
    'title_description' => 'Description',
    'title_photo' => 'Photo',
    'title_status' => 'Status',
    // Notices for group
    'title_create_group' => 'Create group',
    'search' => 'User search',
    'search_result' => 'The search has not given any results',
    'title_show_group' => 'Group',
    'title_groups_data' => 'Date',
    'title_groups_profession' => 'Profession',
    'title_groups_need' => 'Required',
    'title_groups_accepted' => 'Confirmed',
    'title_groups_h_rate' => 'Hourly rate',
    'title_groups_d_rate' => 'Daily rate',
    'title_groups_details' => 'Details',
    'title_groups_start_date' => 'Start date',
    'title_groups_end_date' => 'End date',
    'title_groups_place' => 'Location',
    'title_groups_owner' => 'Organizer',
    'title_edit_group' => 'Edit group',
    'group_success_update' => 'Group updated',
    'group_success_add' => 'Group added',
    'group_error_update' => 'Failed to update group',
    'group_not_access' => 'You have no rights to edit',
    'title_groups_type' => 'Type of group',
    'delete_confirmation' => 'Are you sure you want to delete this group? Attention! Group will be deleted along with all the associated information.',
    'group_user_error_delete' => 'This user is the creator of the group',
    'group_success_delete' => 'Group removed',
    'group_comment_success_delete' => 'Comment has been deleted',
    'title_group_invite' => 'Invite users',
    'title_group_group' => 'Groups',
    'title_group_search' => 'Search candidates',
    'role_error' => 'You can not create groups',
    // Subscribe
    'subscribe' => 'Are you sure you want to join this group?',
    'unsubscribe' => 'Are you sure you want to leave this group?',
    'subscribe_unsubscribe' => 'Are you sure you want to remove a member from the group?',
    'subscribe_success' => 'You are a member of this group',
    'subscribe_unsubscribe_success' => 'Member successfully removed from the group',
    'subscribers_title' => 'Participants',
    'subscribe_role_error' => 'You can not leave the group',
    //Validate
    'exists_subscribe' => 'You are already a member of another group',
    'exists_group'     => 'You already have a group',
    'exists_user_invite'     => 'You have already sent an invitation to this user',
    'group_deleted_description' => 'All users of the current group will be deleted',
    'group_deleted' => 'Group removed',

    //Comments
    'title_comments' => 'Comments',
    'comment_role_error' => 'You do not have permission',
    'confirm-delete-comment' => 'Are you sure you want to delete the comment?',

    //Members
    'title_members' => 'Members',

    // Invite
    'invite' => 'Invite',
    'invite_not_active' => 'Not confirmed',
    'invite_description' => 'Are you sure you want to invite?',
    //// Mail
    'invite_mail_title' => 'The invitation to the group',
    'invite_mail_title_refuse' => 'Rejection of invitation',
    'invite_mail_title_accept' => 'Consent to the invitation',
    'invite_mail_success' => 'To confirm click on ',
    'invite_mail_refuse' => ', refusing to go to this here ',
    'invite_mail_show_user' => 'To find out who declined an invitation to go to ',
    'invite_mail_token_expires' => 'Link not active',
    // End Invite
    'tab_my_invitation'         => 'My invitations',
    'title_groups_list'         => 'Invitations',
    'invite_accept_question'    => 'Are you sure you want to accept this invitation?',
    'group_members' => 'Group members',
    'delete_comment_confirm' => 'Are you sure you want to delete this comment?',
);