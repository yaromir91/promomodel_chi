<?php

return array(
    'groups'           => 'Группы',
    'groups_list'      => 'Список групп',
    'update_group'     => 'обновить группы',
    'add_group'        => 'Добавить группу',
    'delete_group'     => 'Удалить группу',
    'create_group'     => 'Создать группу',
    'group_details'    => 'Детали группы',
    'invite'           => 'Пригласить',
    'search_result'    => 'Результат поиска',
    'invitations'      => 'Приглашения',
    'delete_group'     => 'Удалить группу'
);