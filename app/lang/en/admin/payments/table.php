<?php

return array(
    'title'                 => 'Мероприятие',
    'date'                  => 'Дата окончания',
    'total_payment_status'  => 'Статус',
    'paiment_status'        => 'Статус оплаты',
    'total_price'           => 'Стоимость',
    'user_name'             => 'Пользователь',
    'user_price'            => 'Сумма',
    'user_code'             => 'Счет',
    'user_status'           => 'Статус'
);