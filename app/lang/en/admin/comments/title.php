<?php

return array(

	'comment_management'    => 'Управление коментариями',
	'comment_update'        => 'Обновить комментарий',
	'comment_delete'        => 'Удалить комментарий',
	'create_a_new_comment'  => 'Создать новый комментарий',

);