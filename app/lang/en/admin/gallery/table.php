<?php

return array(
    'title'            => 'Название',
    'created_at'       => 'Дата создания',
    'preview'          => 'Предпросмотр',
    'user'             => 'Пользователь',
    'image'            => 'Картинка',
);