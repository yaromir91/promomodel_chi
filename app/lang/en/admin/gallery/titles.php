<?php

return array(
    'gallery'            => 'Галереи',
    'gallery_list'       => 'Список галерей',
    'update_gallery'     => 'Редактировать галерею',
    'add_gallery'        => 'Добавить галерею',
    'edit_image'         => 'Редактировать картинку',
    'delete_gallery'     => 'Удалить картинку',
    'add_images'         => 'Добавить картинку'
);