<?php

return array(

    'user_management'           => 'Агенты',
    'name'                      => 'Имя',
    'email'                     => 'Email',
    'status'                    => 'Статус',
    'agent_activate'            => 'Активация агента',
    'agent_deactivate'          => 'Деактивация агента'
);
