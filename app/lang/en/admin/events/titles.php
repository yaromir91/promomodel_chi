<?php

return array(
    'events'             => 'Мероприятия',
    'event_types'        => 'Типы мероприятия',
    'event_titles_list'  => 'Список типов меропрития',
    'edit_event_types'   => 'Редактировать тип мероприятия',
    'create_event_types' => 'Создать новый тип мероприятия',
    'delete_event_type'  => 'Удалить тип мероприятия',
    'delete_event'       => 'Удалить мероприятие',
    'edit_success'       => 'Успешно обновлено.',
    'create_success'     => 'Успешно создано.',
    'events_list'        => 'Список мероприятий',
    'event_create'       => 'Создать мероприятие',
    'event_details'      => 'Детали мероприятия',
    'search'             => 'Поиск',
    'search_results'     => 'Поиск результатов',
    'team'               => 'Команда',
    'username'           => 'Имя пользователя',
    'status_in_team'     => 'Статус в команде',
    'status_manager'     => 'Статус менеджера',
    'profession'         => 'Профессия',
    'edit_event'         => 'Редактирование мероприятия',
    'delete_event'       => 'Удаление мероприятия'
);