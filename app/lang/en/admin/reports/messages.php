<?php

return array(
    'edit_error'        => 'Поле контент, не может быть пустым.',
    'delete'            => array(
        'success'           => 'Удалено успешно'
    ),
    'save'              => array(
        'success'           => 'Сохранено успешно'
    )
);