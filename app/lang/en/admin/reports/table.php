<?php

return array(
    'title'            => 'Название',
    'created_at'       => 'Дата создания',
    'content'          => 'Контент',
    'user'             => 'Пользователь',
    'image'            => 'Картинка',
    'status'           => 'Статус',
    'event'            => 'Мероприятие',
    'username'         => 'Пользователь',
    'comments'         => 'Комментарий',
    'update'           => 'Обновить'
);