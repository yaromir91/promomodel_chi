<?php

return array(
    'update_params_title' => 'Параметры обновления',
    'General'             => 'Основные',
    'child_elements'      => 'Под-параметры',
    'name'                => 'Название',
    'name_en'             => 'Название по английски',
    'type'                => 'Тип',
    'new_profile_params'  => 'Новые параметры',
    'param_delete'        => 'Удалить параметры',
);