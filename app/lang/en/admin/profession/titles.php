<?php

return array(
    'professions'             => 'Профессии',
    'create_a_new_profession' => 'Добавить новую профессию',
    'edit_profession'         => 'Редактировать профессию',
    'delete_profession'       => 'Удалить профессию',
);