<?php

return array(
    
    'thank_for_registering.'                => 'Thank you for registering.',
    'new_generated_password'                => 'For you was generated password',
    'you_can_change_password'               => 'If necessary, you can change it in the settings.',
    'you_invited_to_event'                  => 'You are invited to participate in the event.',
    'you_was_invited_new_event'             => 'Dear %s, you was invited on new event.',
    'you_was_invited_as_manager_new_event'  => 'Dear %s, you was invited as manager on new event.',
    'you_invited_to_be_manager_event'       => 'Dear %s, you was invited as manager on new event.',
    'you_can_view_invitations'              => 'You can view your invitations on the page',
    'your_profession'                       => 'Your profession',
    'report_invitation_event'               => 'A report about invitation on event',

    'these_candidates_accepted_invitation'   => 'These candidates accepted your invitation on event',
    'any_candidates_not_accepted_invitation' => 'Any candidates didn\'t accept your invitation on event',

    'manager'    => 'manager',
    'applicant'  => 'applicant',
    'click_here' => 'Click here',

    'user_rejected_invitation_event'        => 'User %s rejected your invitation on the event',
    'user_accepted_invitation_event'        => 'User %s accepted your invitation on the event',
    'you_can_find_other_applicants_on_page' => 'You can find other applicants on this page',

    'as_a_manager'  => 'as a manager',
    'on_profession' => 'on profession',

    'you_deleted_from_team' => 'You were deleted from team of the event',
    'dear' => 'Dear',
    'event_deleted' => 'Event was deleted',
    'this_event_deleted' => 'This event was deleted',

    'you_was_deleted' => 'You have been removed from the site',
    'link'                    => 'link',
    'created_by'                    => 'created',
    'the_event'                    => 'Event',

    'link'                    => 'link',
    'event_cancel_message'                  => 'Organizer %s asks to cancel the event paid.',
    'you_invited_to_agent'                  => 'You have been invited to attend the event',
    'you_was_invited_agent'                 => 'Dear %s, You have been invited to become a member of the agent.',

    'new_user_password' => array(
        'subject'       => 'Confirmation of registration',
        'greetings'     => 'Hello, :name',
        'body_password' => 'Your password :password',
        'body'          => 'Please click on the link below to confirm your registration.',
        'farewell'      => 'Best regards,',
    ),
    'invite_mail_title' => 'The invitation to the group',
    'invite_mail_title_refuse' => 'Rejection of invitation',
    'invite_mail_title_accept' => 'Consent to the invitation',

);
