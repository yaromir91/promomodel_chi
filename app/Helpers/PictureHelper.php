<?php

namespace Acme\Helpers;

/**
 * Class PictureHelper
 * @package Acme\Helpers
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class PictureHelper
{

    /**
     * @param $src
     * @param $dst
     * @param $data
     * @param null $msg
     * @param int $newWidth
     * @param int $newHieght
     * @param int $quality
     *
     * @return bool
     */
    static public function crop($src, $dst, $data, &$msg = null, $newWidth = 0, $newHieght = 0, $quality = 100) {

        $type   = false;
        $srcImg = false;

        if (!empty($src)) {
            $type      = exif_imagetype($src);
            $extension = image_type_to_extension($type);
            $size      = getimagesize($src);
        }

        if (!empty($src) && !empty($dst) && !empty($data) && !empty($type) && !empty($size)) {

            switch ($type) {
                case IMAGETYPE_GIF:
                    $srcImg  = imagecreatefromgif($src);
                    $picfunc = 'imagegif';
                    break;

                case IMAGETYPE_JPEG:
                    $srcImg  = imagecreatefromjpeg($src);
                    $picfunc = 'imagejpeg';
                    break;

                case IMAGETYPE_PNG:
                    $q        = 9/100;
                    $quality *= $q;
                    $quality  = ceil($quality);
                    $srcImg   = imagecreatefrompng($src);
                    $picfunc  = 'imagejpeg';
                    break;
            }

            if (!$srcImg) {
                $msg = "Failed to read the image file";
                return false;
            }

            $sizeW    = $size[0];
            $sizeH    = $size[1];
            $srcImgW  = $sizeW;
            $srcImgH  = $sizeH;
            $degrees  = $data->rotate;

            if (is_numeric($degrees) && $degrees != 0) {
                $newImg = imagerotate( $srcImg, -$degrees, imagecolorallocatealpha($srcImg, 240, 240, 240, 127) );

                imagedestroy($srcImg);
                $srcImg = $newImg;

                $deg = abs($degrees) % 180;
                $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;

                $srcImgW = $sizeW * cos($arc) + $sizeH * sin($arc);
                $srcImgH = $sizeW * sin($arc) + $sizeH * cos($arc);

                $srcImgW -= 1;
                $srcImgH -= 1;
            }

            $tmpImgW = $data->width;
            $tmpImgH = $data->height;
            $dstImgH = $newWidth;
            $dstImgH = $newHieght;

            $srcX    = $data->x;
            $srcY    = $data->y;

            if ($srcX <= -$tmpImgW || $srcX > $srcImgW) {
                $srcX = $srcW = $dstX = $dstW = 0;
            } else if ($srcX <= 0) {
                $dstX = -$srcX;
                $srcX = 0;
                $srcW = $dstW = min($srcImgW, $tmpImgW + $srcX);
            } else if ($srcX <= $srcImgW) {
                $dstX = 0;
                $srcW = $dstW = min($tmpImgW, $srcImgW - $srcX);
            }

            if ($srcW <= 0 || $srcY <= -$tmpImgH || $srcY > $srcImgH) {
                $srcY = $srcH = $dstY = $dstH = 0;
            } else if ($srcY <= 0) {
                $dstY = -$srcY;
                $srcY = 0;
                $srcH = $dstH = min($srcImgH, $tmpImgH + $srcY);
            } else if ($srcY <= $srcImgH) {
                $dstY = 0;
                $srcH = $dstH = min($tmpImgH, $srcImgH - $srcY);
            }

            $ratio = $tmpImgW / $dstImgH;
            $dstX /= $ratio;
            $dstY /= $ratio;
            $dstW /= $ratio;
            $dstH /= $ratio;

            $dstImg = imagecreatetruecolor($dstImgH, $dstImgH);

            imagefill($dstImg, 0, 0, imagecolorallocatealpha($dstImg, 250, 250, 250, 127));
            imagesavealpha($dstImg, true);

            $result = imagecopyresampled($dstImg, $srcImg, $dstX, $dstY, $srcX, $srcY, $dstW, $dstH, $srcW, $srcH);

            if ($result) {
                if (function_exists($picfunc)) {
                    if (!$picfunc($dstImg, $dst, $quality)) {
                        $msg = "Failed to save the cropped image file";dd($msg);
                        return false;
                    }
                }

            } else {
                $msg = "Failed to crop the image file";dd($msg);
                return false;
            }

            imagedestroy($srcImg);
            imagedestroy($dstImg);

            return true;
        }
    }

    /**
     * @param $src
     * @param $out
     * @param $width
     * @param $height
     * @param int $color
     * @param int $quality
     *
     * @return bool
     */
    public static function img_resize_max_if_big($src, $out, $width, $height, $color = 0xFFFFFF, $quality = 100)
    {
        if (!file_exists($src)) {
            return false;
        }

        $size = getimagesize($src);
        if(($size[0]==$width and $size[1]==$height) or($size[0]<=$width and $size[1]<=$height)){return true;}

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
        if($format=='png'){$quality=9;}

        $picfunc = 'imagecreatefrom'.$format;

        $gor = $width  / $size[0];
        $ver = $height / $size[1];

        if ($height == 0) {
            $ver = $gor;
            $height  = $ver * $size[1];
        }
        elseif ($width == 0) {
            $gor = $ver;
            $width   = $gor * $size[0];
        }

        $ratio   = min($gor, $ver);
        if ($gor == $ratio)
            $use_gor = true;
        else
            $use_gor = false;

        $new_width   = $use_gor  ? $width  : floor($size[0] * $ratio);
        $new_height  = !$use_gor ? $height : floor($size[1] * $ratio);
        $new_left    = $use_gor  ? 0 : floor(($width - $new_width)   / 2);
        $new_top     = !$use_gor ? 0 : floor(($height - $new_height) / 2);

        $picsrc  = $picfunc($src);
        $picout = imagecreatetruecolor($width, $height);

        imagefill($picout, 0, 0, $color);
        imagecopyresampled($picout, $picsrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);

        $func_image = 'image'.$format;
        $func_image($picout, $out, $quality);

        imagedestroy($picsrc);
        imagedestroy($picout);

        return true;
    }

    /**
     * @param $src
     * @param $out
     * @param $width
     * @param $height
     * @param int $color
     * @param int $quality
     * @param bool|false $is_transparency
     * @return bool
     */
    public static function img_resize_if_big($src, $out, $width, $height, $color = 0xFFFFFF, $quality = 100,$is_transparency=false)
    {
        if (!file_exists($src)) {
            return false;
        }

        if($size = getimagesize($src))
        {
            if(($size[0]==$width and $size[1]==$height) or($size[0]<=$width and $size[1]<=$height)){return true;}
            $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
            if($format=='png'){$quality=9;}

            $picfunc = 'imagecreatefrom'.$format;


            $gor = $width  / $size[0];
            $ver = $height / $size[1];

            if ($height == 0) {
                $ver = $gor;
                $height  = $ver * $size[1];
            }
            elseif ($width == 0) {
                $gor = $ver;
                $width   = $gor * $size[0];
            }

            $ratio   = min($gor, $ver);
            if ($gor == $ratio)
                $use_gor = true;
            else
                $use_gor = false;

            $new_width   = $use_gor  ? $width  : floor($size[0] * $ratio);
            $new_height  = !$use_gor ? $height : floor($size[1] * $ratio);
            $new_left    = $use_gor  ? 0 : floor(($width - $new_width)   / 2);
            $new_top     = !$use_gor ? 0 : floor(($height - $new_height) / 2);

            $picsrc  = $picfunc($src);
            $picout = imagecreatetruecolor($width, $height);

            if($is_transparency){
                imageAlphaBlending($picout, false);
                imageSaveAlpha($picout, true);
            }
            else{
                imagefill($picout, 0, 0, $color);
            }

            imagecopyresampled($picout, $picsrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);

            $func_image = 'image'.$format;
            $func_image($picout, $out, $quality);

            imagedestroy($picsrc);
            imagedestroy($picout);

            return true;
        }
        return false;
    }


    /**
     * @param $src
     * @param $out
     * @param $width
     * @param $height
     * @param int $color
     * @param int $quality
     * @param bool $is_transparency
     *
     * @return bool
     */
    public static function img_resize($src, $out, $width, $height, $color = 0xFFFFFF, $quality = 100,$is_transparency=false)
    {
        if (!file_exists($src)) {
            return false;
        }


        if($size = getimagesize($src))
        {
            if($size[0]==$width and $size[1]==$height){return true;}

            $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
            if($format=='png'){$quality=9;}


            $picfunc = 'imagecreatefrom'.$format;


            $gor = $width  / $size[0];

            $ver = $height / $size[1];


            if ($height == 0) {
                $ver = $gor;
                $height  = $ver * $size[1];
            }

            elseif ($width == 0) {
                $gor = $ver;
                $width   = $gor * $size[0];
            }


            $ratio   = min($gor, $ver);

            if ($gor == $ratio)
                $use_gor = true;
            else
                $use_gor = false;

            $new_width   = $use_gor  ? $width  : floor($size[0] * $ratio);
            $new_height  = !$use_gor ? $height : floor($size[1] * $ratio);
            $new_left    = $use_gor  ? 0 : floor(($width - $new_width)   / 2);
            $new_top     = !$use_gor ? 0 : floor(($height - $new_height) / 2);

            $picsrc  = $picfunc($src);
            $picout = imagecreatetruecolor($width, $height);

            if($is_transparency){
                imageAlphaBlending($picout, false);
                imageSaveAlpha($picout, true);
            }
            else{
                imagefill($picout, 0, 0, $color);
            }

            imagecopyresampled($picout, $picsrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);


            $func_image = 'image'.$format;
            $func_image($picout, $out, $quality);

            imagedestroy($picsrc);
            imagedestroy($picout);

            return true;
        }
        return false;
    }


    /**
     * @param $src
     * @param $out
     * @param $width
     * @param $height
     * @param int $color
     * @param int $quality
     *
     * @return bool
     */
    public static function img_resize_max($src, $out, $width, $height, $color = 0xFFFFFF, $quality = 100)
    {
        if (!file_exists($src)) {
            return false;
        }

        $size = getimagesize($src);
        if($size[0]==$width and $size[1]==$height){return true;}

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
        if($format=='png'){$quality=9;}

        $picfunc = 'imagecreatefrom'.$format;

        $gor = $width  / $size[0];

        $ver = $height / $size[1];


        if ($height == 0) {
            $ver = $gor;
            $height  = $ver * $size[1];
        }

        elseif ($width == 0) {
            $gor = $ver;
            $width   = $gor * $size[0];
        }


        $ratio   = max($gor, $ver);

        if ($gor == $ratio)
            $use_gor = true;
        else
            $use_gor = false;

        $new_width   = $use_gor  ? $width  : floor($size[0] * $ratio);
        $new_height  = !$use_gor ? $height : floor($size[1] * $ratio);
        $new_left    = $use_gor  ? 0 : floor(($width - $new_width)   / 2);
        $new_top     = !$use_gor ? 0 : floor(($height - $new_height) / 2);

        $picsrc  = $picfunc($src);
        $picout = imagecreatetruecolor($width, $height);

        imagefill($picout, 0, 0, $color);

        imagecopyresampled($picout, $picsrc, 0, 0, $new_top, $new_left, $new_width, $new_height, $size[0], $size[1]);


        $func_image = 'image'.$format;
        $func_image($picout, $out, $quality);

        imagedestroy($picsrc);
        imagedestroy($picout);

        return true;
    }
}