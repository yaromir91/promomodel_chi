<?php

namespace Acme\Helpers;

/**
 * Class MainHelper
 *
 * @package Acme\Helpers
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class MainHelper
{

    /**
     * @param string  $dir
     * @param integer $mode
     *
     * @param bool $recursive
     */
    static public function createDirIfNotExist($dir, $mode, $recursive = true)
    {
        if (!is_dir($dir)) {
            $oldMask = umask(0);
            \File::makeDirectory($dir, $mode, $recursive);
            umask($oldMask);
        }
    }

    /**
     * @param $checkedDate
     * @param int $days
     *
     * @return bool
     */
    static public function checkDatesDiff($checkedDate, $days = 0)
    {
        if (!empty($checkedDate)) {
            $now            = new \DateTime("now");
            $eventStartDate = new \DateTime($checkedDate);
            $interval       = $now->diff($eventStartDate);

            if ($interval->invert == 1) {
                return false;
            } else {
                if ($interval->days > $days) {

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param int $codeLength
     *
     * @return string
     */
    static public function generateRandomCode($codeLength = 8)
    {
        return substr( md5(rand()), 0, $codeLength);
    }

    /**
     * @param $id1
     * @param $id2
     *
     * @return string
     */
    static public function generateRandCodeUniq($id1, $id2)
    {
        return md5(min($id1, $id2) . "|" . max($id1, $id2));
    }
}