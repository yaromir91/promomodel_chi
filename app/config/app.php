<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => true,

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => 'http://promomodel.local',

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'Europe/Moscow',

	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'ru',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, long string, otherwise these encrypted values will not
	| be safe. Make sure to change it before deploying any application!
	|
	*/

	'key' => 'dKYOXmVhVp4YBP6vbWvw8o7e0tiHLytK',

	'cipher' => MCRYPT_RIJNDAEL_128,	

	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

    'providers' => array(
        /* Laravel Base Providers */
        'Illuminate\Foundation\Providers\ArtisanServiceProvider',
        'Illuminate\Auth\AuthServiceProvider',
        'Illuminate\Cache\CacheServiceProvider',
        'Illuminate\Session\CommandsServiceProvider',
        'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
        'Illuminate\Routing\ControllerServiceProvider',
        'Illuminate\Cookie\CookieServiceProvider',
        'Illuminate\Database\DatabaseServiceProvider',
        'Illuminate\Encryption\EncryptionServiceProvider',
        'Illuminate\Filesystem\FilesystemServiceProvider',
        'Illuminate\Hashing\HashServiceProvider',
        'Illuminate\Html\HtmlServiceProvider',
        'Illuminate\Log\LogServiceProvider',
        'Illuminate\Mail\MailServiceProvider',
        'Illuminate\Database\MigrationServiceProvider',
        'Illuminate\Pagination\PaginationServiceProvider',
        'Illuminate\Queue\QueueServiceProvider',
        'Illuminate\Redis\RedisServiceProvider',
        'Illuminate\Remote\RemoteServiceProvider',
        'Illuminate\Auth\Reminders\ReminderServiceProvider',
        'Illuminate\Database\SeedServiceProvider',
        'Illuminate\Session\SessionServiceProvider',
        'Illuminate\Translation\TranslationServiceProvider',
        'Illuminate\Validation\ValidationServiceProvider',
        'Illuminate\View\ViewServiceProvider',
        'Illuminate\Workbench\WorkbenchServiceProvider',
        'Artdarek\OAuth\OAuthServiceProvider',
		
        /* Additional Providers */
        'Zizaco\Confide\ServiceProvider', // Confide Provider
        'Zizaco\Entrust\EntrustServiceProvider', // Entrust Provider for roles
        'Bllim\Datatables\DatatablesServiceProvider', // Datatables
		'Acme\Providers\Search\PMSearchServiceProvider', //Search candidate
		'Acme\Providers\Search\InviteToGroupServiceProvider', //Invite to a group candidate
		'Acme\Providers\SearchApplicantServiceProvider', // Build params for filters
		'Acme\Validators\ProfileProfValidationServiceProvider', // Custom Validate
		//'Acme\Validators\PromoModValidatorsServiceProvider', // Custom Validate
		'Acme\Providers\ReserveServiceProvider', // Build params for filters
        'Acme\Providers\CalendarServiceProvider', // Build params for calendar plugin

        /* Uncomment for use in development */
        'Way\Generators\GeneratorsServiceProvider', // Generators
        'Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider', // IDE Helpers

        /* Test generator */
        'Davispeixoto\TestGenerator\TestGeneratorServiceProvider',

        /* Cyr to Lat */
        'Ivanlemeshev\Laravel4CyrillicSlug\SlugServiceProvider',

        'Intervention\Image\ImageServiceProvider',

		/* Debug Panel  */
//		'Barryvdh\Debugbar\ServiceProvider',

		'BrainSocket\BrainSocketServiceProvider',
	),

    /*
    |--------------------------------------------------------------------------
    | Service Provider Manifest
    |--------------------------------------------------------------------------
    |
    | The service provider manifest is used by Laravel to lazy load service
    | providers which are not needed for each request, as well to keep a
    | list of all of the services. Here, you may set its storage spot.
    |
    */

    'manifest' => storage_path() . '/meta',

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => array(
        /* Laravel Base Aliases */
		'App'             => 'Illuminate\Support\Facades\App',
		'Artisan'         => 'Illuminate\Support\Facades\Artisan',
		'Auth'            => 'Illuminate\Support\Facades\Auth',
		'Blade'           => 'Illuminate\Support\Facades\Blade',
		'Cache'           => 'Illuminate\Support\Facades\Cache',
		'ClassLoader'     => 'Illuminate\Support\ClassLoader',
		'Config'          => 'Illuminate\Support\Facades\Config',
		'Controller'      => 'Illuminate\Routing\Controller',
		'Cookie'          => 'Illuminate\Support\Facades\Cookie',
		'Crypt'           => 'Illuminate\Support\Facades\Crypt',
		'DB'              => 'Illuminate\Support\Facades\DB',
		'Eloquent'        => 'Illuminate\Database\Eloquent\Model',
		'Event'           => 'Illuminate\Support\Facades\Event',
		'File'            => 'Illuminate\Support\Facades\File',
		'Form'            => 'Illuminate\Support\Facades\Form',
		'Hash'            => 'Illuminate\Support\Facades\Hash',
		'HTML'            => 'Illuminate\Support\Facades\HTML',
		'Input'           => 'Illuminate\Support\Facades\Input',
		'Lang'            => 'Illuminate\Support\Facades\Lang',
		'Log'             => 'Illuminate\Support\Facades\Log',
		'Mail'            => 'Illuminate\Support\Facades\Mail',
		'Paginator'       => 'Illuminate\Support\Facades\Paginator',
		'Password'        => 'Illuminate\Support\Facades\Password',
		'Queue'           => 'Illuminate\Support\Facades\Queue',
		'Redirect'        => 'Illuminate\Support\Facades\Redirect',
		'Redis'           => 'Illuminate\Support\Facades\Redis',
		'Request'         => 'Illuminate\Support\Facades\Request',
		'Response'        => 'Illuminate\Support\Facades\Response',
		'Route'           => 'Illuminate\Support\Facades\Route',
		'Schema'          => 'Illuminate\Support\Facades\Schema',
		'Seeder'          => 'Illuminate\Database\Seeder',
		'Session'         => 'Illuminate\Support\Facades\Session',
		'SoftDeletingTrait' => 'Illuminate\Database\Eloquent\SoftDeletingTrait',
		'SSH'             => 'Illuminate\Support\Facades\SSH',
		'Str'             => 'Illuminate\Support\Str',
		'URL'             => 'Illuminate\Support\Facades\URL',
		'Validator'       => 'Illuminate\Support\Facades\Validator',
		'View'            => 'Illuminate\Support\Facades\View',
        'OAuth'           => 'Artdarek\OAuth\Facade\OAuth',

        /* Additional Aliases */
        'Confide'         => 'Zizaco\Confide\Facade', // Confide Alias
        'Entrust'         => 'Zizaco\Entrust\EntrustFacade', // Entrust Alias
        'String'          => 'Andrew13\Helpers\String', // String
        'Carbon'          => 'Carbon\Carbon', // Carbon
        'Datatables'      => 'Bllim\Datatables\Datatables', // DataTables

        /* Cyr to Lat */
        'Slug' => 'Ivanlemeshev\Laravel4CyrillicSlug\Facades\Slug',

        'Image' => 'Intervention\Image\Facades\Image',

		/* Debug panel */
//		'Debugbar'   => 'Barryvdh\Debugbar\Facade',

		'BrainSocket'     => 'BrainSocket\BrainSocketFacade',

	),

    'available_language' => array('en', 'pt', 'es'),

    'image_dir' => array(
        'profile'           => 'upload/profile/',
        'events'            => 'upload/events/',
        'events_small'      => 'upload/events/small/',
        'events_preview'    => 'upload/events/preview/',
        'groups'            => 'upload/groups/',
        'groups_small'      => 'upload/groups/small/',
        'groups_preview'    => 'upload/groups/preview/',
        'album'             => 'upload/album/',
        'album_slider'      => 'upload/album/slider/',
        'comment'           => 'upload/comment/',
        'comments_small'    => 'upload/comment/small/',
        'comments_preview'  => 'upload/comment/preview/',
        'posts'             => 'upload/posts/',
        'posts_small'       => 'upload/posts/small/',
        'posts_preview'     => 'upload/posts/preview/',
        'stub'              => 'upload/stub.jpg',
        'partners'          => 'upload/partners/',
        'partners_small'    => 'upload/partners/small/',
        'partners_preview'  => 'upload/partners/preview/',
    ),

    'galleries_pagination' 				=> '15',
	'groups_pagination' 				=> '6',
    'groups_pagination_list'            => '12',
	'group_comments_pagination'	 		=> '10',
	'album_no_image' 					=> '<img class="content-image" src="/assets/img/noimg/360_330.png" />',
	'album_no_image_260_180' 			=> '<img src="/assets/img/noimg/260x180.png" alt="">',
	'album_no_image_260_180_with_class' => '<img class="content-image" src="/assets/img/noimg/260x180.png" alt="">',
	'album_no_image_260_180_main' 		=> '<img class="main-image" src="/assets/img/noimg/260x180.png" alt="">',
	'album_no_image_no_class' 			=> '<img src="/assets/img/noimg/360_330.png" />',
	'album_no_image_content_262_262'    => '<img class="content-image" src="/assets/img/noimg/262x262.png" />',
	'album_profile_image_262_262'       => '<img class="profile-image" src="http:/assets/img/noimg/262x262.png" />',
	'album_no_big_1000_400'             => '<img src="/assets/img/noimg/1000x400.png" />',
	'album_550_500'                     => '<img src="/assets/img/noimg/550x500.png" />',
	'main_pic_resize_width' 			=> 262,
	'main_pic_resize_height' 			=> 262,
	'gall_slider_width' 			    => 1000,
	'gall_slider_height' 			    => 400,
	'preview_width' 			        => 550,
	'preview_height' 			        => 500,
	'quality_picture' 			        => 50,

	'count_post_home_page' => [
		'events' 	=> 8,
		'users' 	=> 8,
		'reports' 	=> 8,
	],

	'allowed_extension' => [
		'jpeg' => 1,
		'jpg'  => 1,
		'png'  => 1,
	],

    'calendar' => array(
        'textColor'             => '#FAFFFF',
        'reserve_color'         => '#CDCDCD',
        'event_color'           => '#f777b9',
        'accepted_event_color'  => '#71D779',
        'public_event_color'    => '#add8e6'
    ),
	'payment_color' => array(
		'end' => 'orangered',
		'complete' => '#00dd1c'
	),

	'payment_percent_service' => '10',

    'redis' => array(

        'cluster' => false,

        'default' => array(
            'host'     => '127.0.0.1',
            'port'     => 6699,
            'database' => 0,
            'password' => 'WFVQ37BxTgZoXe0B'
        ),
    ),

    'payments' => array(
        'scId'              => '41719', // test:529604 work:41719
        'shopId'            => '107938',
        'ShopPassword'      => '1020312Qq',
        'checkOrder'        => 'events/checkOrder',
        'testCheckOrder'    => 'events/testCheckOrder',
        'paymentAviso'      => 'events/paymentAviso',
        'testPaymentAviso'  => 'events/testPaymentAviso',
        'action'            => 'https://money.yandex.ru/eshop.xml',
        'testAction'        => 'https://demomoney.yandex.ru/eshop.xml',
        '_pswd'             => 'g356arh3',
        '_key_path'         => '/var/www/promomodel/cert/private.key',
        '_cer_path'         => '/var/www/promomodel/cert/107938.cer',
        'payment_url'       => 'https://penelope.yamoney.ru:443/webservice/mws/api/',
        'demo_payment_url'  => 'https://penelope-demo.yamoney.ru:8083/webservice/mws/api/'
        
    ),

    'social' => array(
        'links'       => array(
            'twitter'           => 'https://twitter.com/PromoGangWorld',
            'facebook'          => 'https://www.facebook.com/promogang/',
            'google'            => 'https://plus.google.com/+PromogangWorld?rel=author',
        )
    ),
    'admin_email'				=> 'support@promogang.ru',
);
