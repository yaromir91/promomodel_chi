<?php
namespace Acme\Models;
/**
 * Class AssignedRoles
 * @property $id
 */
class AssignedRoles extends \Eloquent
{

    /**
     * @var array
     */
    protected $guarded = array();

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'assigned_roles';

    /**
     * @var array
     */
    public static $rules = array();

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }
}