<?php

namespace Acme\Models;

/**
 *
 * Class EventComments
 *
 * @package Acme\Models
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class Partners extends \Eloquent {
    protected $fillable = [];
        
    /**
     * @var string
     */
    protected $table = 'partners';
    
    const ACTIVE_YES = 1;
    const ACTIVE_NO = 0;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

        /**
     * @return string
     */
    public function getTemporarySmallFile()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->logo) {

            return asset(\Config::get('app.image_dir.partners_small') . $this->logo);
        } else {

            return asset(\Config::get('app.image_dir.partners_small') . $thePic);
        }
    }

    /**
     * @return mixed
     */
    public function getTemporarySmallFileName()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->logo) {

            return $this->logo;
        } else {

            return $thePic;
        }
    }
        /**
     * @param $data
     *
     * @return bool
     */
    public function saveInfo($data)
    {
        $destinationPath          = \Config::get('app.image_dir.partners');
        $destinationSmallPath     = \Config::get('app.image_dir.partners_small');
        $destinationPrevPath      = \Config::get('app.image_dir.partners_preview');

        try {
            $this->title             = isset($data['title']) ? $data['title'] : '';
            $this->description       = isset($data['description']) ? $data['description'] : '';
            $this->url               = isset($data['url']) ? $data['url'] : '';
            $this->email             = isset($data['email']) ? $data['email'] : '';
            $this->phone             = isset($data['phone']) ? $data['phone'] : '';
            $this->adress            = isset($data['adress']) ? $data['adress'] : '';
            $this->active            = isset($data['active']) ? $data['active'] : '';

            $oldImage                = $this->logo;

            if($data['new_picture']) {

                if (trim($data['new_picture']) != $oldImage) {
                    \File::delete($destinationPath . '/' . $oldImage);
                    \File::delete($destinationSmallPath . '/' . $oldImage);
                    \File::delete($destinationPrevPath . '/' . $oldImage);
                }
                $this->logo       = $data['new_picture'];
            }

            if (isset($data['logo']) && is_object($data['logo'])) {
                $file                = $data['logo'];
                $this->logo         = $data['logo']->getClientOriginalName();

                $file->move($destinationPath, $file->getClientOriginalName());

                if ($oldImage != $this->logo) {
                    $this->deletePreview($oldImage);
                }
            }

            if($this->save()) {

                return true;
            }
        } catch (\Exception $e) {

            return false;
        }

        return false;
    }
    
        /**
     * @param bool $img
     */
    protected function deletePreview($img = false)
    {
        $destinationPath     = \Config::get('app.image_dir.partners');

        if ($img !== false) {
            @unlink($destinationPath . $img);
        } elseif ($this->logo) {
            @unlink($destinationPath . $this->logo);
        }
    }
    
    
    
    /**
     * @return bool|null
     * @throws \Exception
     */
    public function delete()
    {
        $destinationPath      = \Config::get('app.image_dir.partners') . $this->logo;
        $destinationSmallPath = \Config::get('app.image_dir.partners_small') . $this->logo;
        $destinationPrevPath  = \Config::get('app.image_dir.partners_preview') . $this->logo;

        if (\File::exists($destinationPath)) {
            \File::delete($destinationPath);
            \File::delete($destinationSmallPath);
            \File::delete($destinationPrevPath);
        }

        return parent::delete();
    }

    /**
     * Get the date the post was created.
     *
     * @param \Carbon|null $date
     * @return string
     */
    public function date($date=null)
    {
        if(is_null($date)) {
            $date = $this->updated_at;
        }

        return $date->toDateString();
//        return \String::date($date);
    }
}