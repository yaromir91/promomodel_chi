<?php

namespace Acme\Models;

/**
 * Class Groups
 * @package Acme\Models
 *
 * @property integer $id
 * @property string $comment_id
 * @property string $file
 */
class GroupCommentImages extends \Eloquent
{
    /**
     * @var string
     */
    protected $table = 'group_comment_images';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }
    
}