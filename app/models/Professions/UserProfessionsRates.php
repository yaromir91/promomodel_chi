<?php

namespace Acme\Models\Professions;

/**
 * Class ProfessionsEventsRates
 *
 * @property $user_id
 * @property $professions_id
 * @property $rate_h
 * @property $rate_d
 *
 * @package Acme\Models\Professions
 */
class UserProfessionsRates extends \Eloquent
{
    /**
     * @var string
     */
    protected $table = 'user_professions_rate';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }
}