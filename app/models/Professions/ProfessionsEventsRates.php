<?php

namespace Acme\Models\Professions;

/**
 * Class ProfessionsEventsRates
 *
 * @property $events_id
 * @property $professions_id
 * @property $count
 * @property $rate_h
 * @property $rate_d
 *
 * @package Acme\Models\Professions
 */
class ProfessionsEventsRates extends \Eloquent
{

    protected $table = 'professions_events_rates';

    public $timestamps = false;

    protected $fillable = ['events_id', 'professions_id', 'count', 'rate_h', 'rate_d'];

    public function events_id()
    {
        return $this->belongsTo('events', 'id');
    }

    public function professions_id()
    {
        return $this->belongsTo('Acme\Models\Professions\Professions', 'id');
    }

    public function count(){
        return $this->count;
    }

    public function rate_h(){
        return $this->rate_h;
    }

    public function rate_d(){
        return $this->rate_d;
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }
}
