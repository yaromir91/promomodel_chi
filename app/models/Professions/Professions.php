<?php

namespace Acme\Models\Professions;

/**
 * Class Professions
 *
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $invites
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $payments
 *
 * @package Acme\Models\Professions
 */
class Professions extends \Eloquent
{

    protected $table = 'professions';

    /**
     * @var bool
     */
    public $timestamps = false;


    /**
     * @return $this
     */
    public function profiles()
    {
        return $this->belongsToMany('Acme\Models\Profile\UserProfile', 'user_professions_rate', 'professions_id', 'user_id')->withPivot('rate_h', 'rate_d');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invites()
    {
        return $this->hasMany('Acme\Models\Events\EventInvite', 'professions_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany('Acme\Models\Payments\UserApplicantsPayments', 'profession_id', 'id');
    }

}