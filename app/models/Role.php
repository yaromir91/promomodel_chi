<?php

namespace Acme\Models;

use Zizaco\Entrust\EntrustRole;

/**
 * Class Role
 *
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany profileparams
 *
 * @package Acme\Models
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class Role extends EntrustRole
{

    const ADMIN_USER             = 1;
    const COMMENT_USER           = 2;
    const AGENT_USER             = 3;
    const APPLICANT_USER         = 4;
    const APPLICANT_MANAGER_USER = 5;
    const ORGANIZER_USER         = 6;


    /**
     * @param $code
     *
     * @return string
     */
    static public function getRolesTitle($code)
    {
        $translates = [
            self::ORGANIZER_USER         => \Lang::get('roles.organizer'),
            self::ADMIN_USER             => \Lang::get('roles.admin'),
            self::AGENT_USER             => \Lang::get('roles.agent'),
            self::APPLICANT_USER         => \Lang::get('roles.applicant'),
            self::APPLICANT_MANAGER_USER => \Lang::get('roles.applicant_manager'),
        ];

        if (isset($translates[$code])) {

            return $translates[$code];
        }

        return '';
    }

    /**
     * Provide an array of strings that map to valid roles.
     * @param array $roles
     * @return stdClass
     */
    public function validateRoles( array $roles )
    {
        $user = \Confide::user();
        $roleValidation = new \stdClass();
        foreach( $roles as $role )
        {
            $roleValidation->$role = ( empty($user) ? false : $user->hasRole($role) );
        }
        return $roleValidation;
    }

    /**
     * @return mixed
     */
    public function getProfileParamsForCurrentRole()
    {

        return $this->profileparams->filter(function($param)
            {
                return empty($param->parent_id);
            })->sort(function($a, $b)
                {
                    $a = $a->num;
                    $b = $b->num;
                    if ($a === $b) {
                        return 0;
                    }
                    return ($a > $b) ? 1 : -1;
                });
    }

    /**
     * @return mixed
     */
    public function getSearchParamsForCurrentRole()
    {
        return $this->profileparams->filter(function($param)
        {
            return ($param->search_type != 0);
        })->sort(function($a, $b)
                {
                    $a = $a->num;
                    $b = $b->num;
                    if ($a === $b) {
                        return 0;
                    }
                    return ($a > $b) ? 1 : -1;
                });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function profileparams()
    {
        return $this->belongsToMany('Acme\Models\Profile\ProfileParams', 'roles_profile_params', 'roles_id', 'profile_params_id');
    }

    /**
     * @return mixed
     */
    public static function getApplicantRole()
    {
        return self::where('code', '=', self::APPLICANT_USER)->first();
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }
}