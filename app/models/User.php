<?php

namespace Acme\Models;

use Acme\Models\Profile\UserProfileData;
use Illuminate\Database\Eloquent\Collection;
use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;
use Carbon\Carbon;

use Acme\Models\UserProvider;
use Acme\Models\Profile\UserProfile;
use Acme\Models\Repositories\RoleRepository;
use Acme\Models\Repositories\EventInviteRepository;
use Acme\Models\Repositories\RatingsRepository;
use Acme\Helpers\PictureHelper;

/**
 * @property \Illuminate\Database\Eloquent\Relations\HasOne  $social_provider
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $galleries
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $eventinvites
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $sentMessages
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $receiveMessages
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $payments
 *
 * @property object assignedRoles
 */
class User extends \Eloquent implements ConfideUserInterface {

    use ConfideUser, HasRole;

    protected $table = 'users';

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;

    const CONFIRM_STATUS_NOT    = 0; // email не подтвержден
    const CONFIRM_STATUS_YES    = 1; // email подтвержден
    const CONFIRM_STATUS_ACTIVE = 2; // агент активирован администратором

    /**
     * @var null
     */
    private $specUser = null;

    /**
     * @var null
     */
    private $mainRoleID   = null;

    /**
     * @var null
     */
    private $mainRoleCode = null;

    /**
     * @var null
     */
    public $gender        = null;

    /**
     * @var null
     */
    public $accountType   = null;

    /**
     * @var
     */
    public $firstName;

    /**
     * @var
     */
    public $lastName;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany('Acme\Models\Groups', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sentMessages()
    {
        return $this->hasMany('Acme\Models\Chat\UserChatMessages', 'user_sender_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function receiveMessages()
    {
        return $this->hasMany('Acme\Models\Chat\UserChatMessages', 'user_receiver_id', 'id');
    }

    /**
     * @return $this
     */
    public function userGroup()
    {
        return $this->belongsToMany('Acme\Models\Groups', 'users_group', 'user_id', 'groups_id')
            ->withPivot('status');
    }

    public function groupInvitation()
    {
        return $this->belongsToMany('Acme\Models\Groups', 'groups_invites', 'user_id', 'groups_id')
            ->withPivot('status', 'token');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany('Acme\Models\Payments\UserApplicantsPayments', 'user_id', 'id');
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();

        $array['gender']      = $this->gender;
        $array['accountType'] = $this->accountType;
        $array['firstName']   = $this->firstName;
        $array['lastName']    = $this->lastName;

        return $array;
    }

    /**
     * Get user by username
     * @param $username
     * @return mixed
     */
    public function getUserByUsername( $username )
    {
        return $this->where('username', '=', $username)->first();
    }

    /**
     * Get user by id
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getUserById($id)
    {
        return $this->where('id', '=', $id)->first();
    }

    /**
     * Find the user and check whether they are confirmed
     *
     * @param array $identity an array with identities to check (eg. ['username' => 'test'])
     * @return boolean
     */
    public function isConfirmed($identity) {
        $user = \Confide::getUserByEmailOrUsername($identity);
        return ($user && $user->confirmed);
    }

    /**
     * Find the user and check whether they are active Agent
     *
     * @param array $identity an array with identities to check (eg. ['username' => 'test'])
     * @return boolean
     */
    public function isConfirmedAgent($user_id) {
        $user = $this->getUserById($user_id);
        return ($user && $user->confirmed == self::CONFIRM_STATUS_ACTIVE);
    }

    /**
     * Get the date the user was created.
     *
     * @return string
     */
    public function joined()
    {
        return \String::date(Carbon::createFromFormat('Y-n-j G:i:s', $this->created_at));
    }

    /**
     * Save roles inputted from multiselect
     * @param $inputRoles
     */
    final public function saveRoles($inputRoles)
    {
        if(! empty($inputRoles)) {
            $this->roles()->sync($inputRoles);
        } else {
            $this->roles()->detach();
        }
    }

    /**
     * @param $role
     */
    final public function saveRole($role)
    {
        try {
            $this->roles()->detach();
            $this->roles()->attach($role);
        } catch (\Exception $e) {

        }
    }

    /**
     * Returns user's current role ids only.
     * @return array|bool
     */
    public function currentRoleIds()
    {
        $roles   = $this->roles;
        $roleIds = false;
        if( !empty( $roles ) ) {
            $roleIds = array();
            foreach( $roles as $role )
            {
                $roleIds[] = $role->id;
            }
        }
        return $roleIds;
    }


    /**
     * @param integer $roleId Role ID
     *
     * @return bool
     */
    public function checkRole($roleId)
    {
        $roles = $this->currentRoleIds();

        if (in_array($roleId, $roles)) {

            return true;
        }

        return false;
    }

    /**
     * TODO:закончить
     *
     * @return null|object
     */
    public function getSpecUser()
    {
        if (!is_object($this->specUser)) {


        }

        return $this->specUser;
    }

    /**
     * TODO: обсудить возможность принадледности к нескольким ролям
     *
     * @return mixed|null
     */
    public function getMainUserRole()
    {
        if (empty($this->mainRoleID)) {
            $roles = $this->currentRoleIds();

            if (count($roles)) {
                $this->mainRoleID = reset($roles);
            }
        }

        return $this->mainRoleID;
    }

    /**
     * TODO: обсудить возможность принадледности к нескольким ролям
     *
     * @return mixed|null
     */
    public function getMainUserRoleCode()
    {
        if (empty($this->mainRoleCode)) {
            $role = $this->getCurrentUserRole();

            if ($role) {
                $this->mainRoleCode = $role->code;
            }
        }

        return $this->mainRoleCode;
    }

    /**
     * @return null
     */
    public function getCurrentUserRole()
    {
        if ($this->roles) {

            return $this->roles->first();
        }

        return null;
    }

    /**
     * Redirect after auth.
     * If ifValid is set to true it will redirect a logged in user.
     * @param $redirect
     * @param bool $ifValid
     * @return mixed
     */
    public static function checkAuthAndRedirect($redirect, $ifValid=false)
    {
        // Get the user information
        $user = \Auth::user();
        $redirectTo = false;

        if(empty($user->id) && ! $ifValid) // Not logged in redirect, set session.
        {
            \Session::put('loginRedirect', $redirect);
            $redirectTo = \Redirect::to('user/login')
                ->with( 'notice', \Lang::get('user/user.login_first') );
        }
        elseif(!empty($user->id) && $ifValid) // Valid user, we want to redirect.
        {
            $redirectTo = \Redirect::to($redirect);
        }

        return array($user, $redirectTo);
    }

    public function currentUser()
    {
        return \Confide::user();
    }


    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function social_provider()
    {
        return $this->hasOne('Acme\Models\UserProvider');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function galleries()
    {
        return $this->hasMany('Acme\Models\Gallery', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne('Acme\Models\Profile\UserProfile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventinvites()
    {
        return $this->hasMany('Acme\Models\Events\EventInvite', 'users_id', 'id');
    }


    /**
     * @return mixed
     */
    public function getNewInvitation()
    {
        return $this->eventinvites->filter(function($invite)
        {
            return ($invite->status == \Acme\Models\Events\EventInvite::INVITE_PROF_STATUS_NEW);
        })->sort(function($a, $b)
        {
            $a = $a->id;
            $b = $b->id;
            if ($a === $b) {
                return 0;
            }
            return ($a < $b) ? 1 : -1;
        });
    }


    /**
     * @param $eventId
     *
     * @return bool
     */
    public function isSentEventInvited($eventId, &$invitation = null)
    {
        $isPresent = EventInviteRepository::findInviteForUser($eventId, $this->id, \Acme\Models\Events\EventInvite::INVITE_PROF_STATUS_REJECT);

        if (!empty($isPresent)) {

            $invitation = $isPresent;

            return true;
        }

        return false;
    }

    /**
     * @return UserProfile|bool
     */
    public function getOrCreateProfile()
    {
        if (!($this->profile instanceof UserProfile)) {
            $profile          = new UserProfile();
            $profile->user_id = $this->id;
            $profile->gender  = UserProfile::DEFAULT_GENDER;

            if ($profile->save()) {
                return $profile;
            }
        } else {

            return $this->profile;
        }

        return false;
    }

    /**
     * @param array $dataIds
     * @param bool $withClear
     */
    public function attachEventTypes($dataIds = [], $withClear = true)
    {
        if ($withClear) {
            $this->eventstype()->detach();
        }

        if (!empty($dataIds)) {
            foreach($dataIds as $id) {
                $this->eventstype()->attach($id);
            }
        }
    }

    /**
     * @param array $dataIds
     * @param array $dataRateH
     * @param array $dataRateD
     * @param bool $withClear
     */
    public function attachProfessionsToProfile($dataIds = [], $dataRateH = [], $dataRateD = [], $withClear = true)
    {
        $profile        = $this->profile;

        if ($profile instanceof \Acme\Models\Profile\UserProfile) {
            $profile->attachProfessions($dataIds, $dataRateH, $dataRateD, $withClear);
        }
    }


    /**
     * @param $params
     * @param $types
     */
    public function fillProfileParams($params, $types = [])
    {
        $profile        = $this->profile;

        if ($profile instanceof \Acme\Models\Profile\UserProfile) {
            $profile->fillProfileParams($params, $types);
        }
    }

    /**
     * @param $providerID
     * @param $provider
     *
     * @return bool
     */
    public function addSocialProvider($providerID, $provider)
    {
       if (!($this->social_provider instanceof UserProvider)) {
           $socialProvider = new UserProvider();

           $socialProvider->user_id     = $this->id;
           $socialProvider->provider_id = $providerID;
           $socialProvider->provider    = $provider;

           if ($socialProvider->save()) {

               return true;
           }

           return false;
       }

        return true;
    }

    /**
     * Save General User Information
     *
     * @param $data
     *
     * @return bool
     */
    public function saveGeneralInfo($data)
    {
        $profile             = $this->profile;
        $this->username      = $data['username'];
        $this->email         = $data['email'];
        $this->confirmed     = isset($data['confirm']) ? $data['confirm'] : 1;
        $this->payment_count = isset($data['payment_count']) ? $data['payment_count'] : "";

        $destinationPath = \Config::get('app.image_dir.profile');

        if ($this->save()) {
            if (
                (
                    ($this->getMainUserRoleCode()==\Acme\Models\Role::APPLICANT_USER) ||
                    ($this->getMainUserRoleCode()==\Acme\Models\Role::ORGANIZER_USER) ||
                    ($this->getMainUserRoleCode()==\Acme\Models\Role::APPLICANT_MANAGER_USER)
                ) &&
                ($profile instanceof \Acme\Models\Profile\UserProfile)
            ) {

                $oldPicture                 = $profile->avatar;

                $profile->last_name         = $data['last_name'];
                $profile->first_name        = $data['first_name'];
                $profile->gender            = $data['gender'];
                $profile->phone             = $data['phone'];
                $profile->vk_link           = isset($data['vk_link']) ? $data['vk_link'] : null;
                $profile->is_ready_manager  = isset($data['is_ready_manager']) ? 1 : 0;
                $profile->public            = isset($data['public']) ? 1 : 0;
                $profile->date_birth        = date('Y-m-d', strtotime($data['date_birth']));

                if(!empty($data['avatar'])) {
                    $profile->avatar        = $data['avatar']->getClientOriginalName();
                }

                if($data['new_picture']) {
                    if (trim($data['new_picture']) != $oldPicture) {
                        \File::delete($destinationPath . '/' . $oldPicture);
                    }
                    $profile->avatar       = $data['new_picture'];
                }

                if(
                    ($this->getMainUserRoleCode()==\Acme\Models\Role::APPLICANT_USER) ||
                    ($this->getMainUserRoleCode()==\Acme\Models\Role::APPLICANT_MANAGER_USER)
                ) {
                    if(isset($data['is_ready_manager'])){
                        $this->saveRole(RoleRepository::getRoleByCode(Role::APPLICANT_MANAGER_USER));
                    } else {
                        $this->saveRole(RoleRepository::getRoleByCode(Role::APPLICANT_USER));
                    }
                }


                // upload image
                $file = isset($data['avatar']) ? $data['avatar'] : false;
                if($file)
                {
                    $fileName        = $destinationPath.$file->getClientOriginalName();

                    if($profile->avatar) {
                        \File::delete($destinationPath . '/' . $profile->avatar);
                        $profile->avatar = $file->getClientOriginalName();
                    }
                    $file->move($destinationPath, $file->getClientOriginalName());

                    $img = \Image::make($fileName);
                    $img->crop(\Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'));
                    $img->save($fileName, \Config::get('app.quality_picture'));
                }

                $profile->save();
            }

            return true;
        }

        return false;
    }


    /**
     * @param array $data
     * @param bool|false $withCrop
     *
     * @return bool|mixed|string
     */
    public function saveAvatar($data = [], $withCrop = false)
    {
        $profile    = $this->getOrCreateProfile();
        $file       = isset($data['avatar']) ? $data['avatar'] : false;
        $avatarData = isset($data['avatar_data']) ? json_decode(stripslashes($data['avatar_data'])) : null;

        if($file && ($profile instanceof \Acme\Models\Profile\UserProfile))
        {
            $extensionList = \Config::get('app.allowed_extension');
            $extension     = $file->getClientOriginalExtension();

            if (isset($extensionList[$extension]) && ($extensionList[$extension] ==1)) {
                $destinationPath = \Config::get('app.image_dir.profile');
                $fileName        = \Str::random(20) . '.' . $extension;

                if($profile->avatar) {
                    \File::delete($destinationPath . $profile->avatar);
                }

                $profile->avatar = $fileName;

                $file->move($destinationPath, $fileName);

                $filePathName = $destinationPath . $fileName;

                if ($withCrop && !empty($avatarData)) {
                    $error      = '';
                    $cropResult = PictureHelper::crop($filePathName, $filePathName, $avatarData, $error, 262, 262);
                }

                if ($profile->save()) {

                    return $profile->avatar;
                }
            }
        }

        return false;
    }

    public function savePassword($data)
    {
        $profile              = $this->profile;
        $password             = $data['new_password'];
        $passwordConfirmation = $data['password_confirmation'];

        if ($password) {
            $this->password              = $password;
            $this->password_confirmation = $passwordConfirmation;
        }

        if ($this->save()) {
            if (
                (
                    ($this->getMainUserRoleCode()==\Acme\Models\Role::APPLICANT_USER) ||
                    ($this->getMainUserRoleCode()==\Acme\Models\Role::APPLICANT_MANAGER_USER)
                ) &&
                ($profile instanceof \Acme\Models\Profile\UserProfile)
            ) {
                $profile->save();
            }

            return true;
        }

        return false;
    }

    /**
     * @return mixed|string
     */
    public function getUserName()
    {
        $profile = $this->profile;

        if ($profile instanceof UserProfile) {

            return $profile->getFullName();
        } else {

            return $this->username;
        }
    }

    /**
     * @param bool|false $isImg
     *
     * @return bool|string
     */
    public function getUserAvatar($isImg = false)
    {
        $profile = $this->profile;

        if (($profile instanceof UserProfile) && $profile->avatar) {

            if ($isImg) {

                return '<img class="content-image" src="' . asset(\Config::get('app.image_dir.profile') . $profile->avatar) . '">';
            } else {

                return asset(\Config::get('app.image_dir.profile') . $profile->avatar);
            }
        }

        return false;
    }

    /**
     *
     */
    public function checkDataProfile()
    {
        $res = false;
        $profile = $this->profileData;
        if($profile instanceof Collection){
            /* @var UserProfileData $profile*/
            if($profile->count() > 0){
                $res = true;
            }
        }
        return $res;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function eventstype()
    {
        return $this->belongsToMany('Acme\Models\Events\EventsType', 'users_events_type', 'user_id', 'event_type_id');
    }

    public function events()
    {
        return $this->hasMany('Acme\Models\Events\Events', 'user_id', 'id');
    }

    public function profileData()
    {
        return $this->hasMany('Acme\Models\Profile\UserProfileData', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function assignedRoles()
    {
        return $this->hasOne('Acme\Models\AssignedRoles', 'user_id');
    }


    /**
     * @return float
     */
    public function getUserRating()
    {
        return RatingsRepository::getAvgRating(\Acme\Models\Ratings::RATING_TYPE_USER, $this->id);
    }

    /**
     * Set confirm field to CONFIRM_STATUS_ACTIVE
     */
    public function agentActivate()
    {
        $this->confirmed = self::CONFIRM_STATUS_ACTIVE;
        $this->save();
    }
    
    /**
     * Set confirm field to CONFIRM_STATUS_YES
     */
    public function agentDeactivate()
    {
        $this->confirmed = self::CONFIRM_STATUS_YES;
        $this->save();
    }

    /**
     * @return bool
     */
    public function checkAgentStatus()
    {
        return ($this->confirmed == self::CONFIRM_STATUS_ACTIVE);
    }


    /**
     * @param null $callback
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete($callback = null)
    {
        if (isset($callback)){
            if (is_callable($callback)){
                call_user_func($callback, $this);
            }
        }

        $this->profile()->delete();

        return parent::delete();
    }
}
