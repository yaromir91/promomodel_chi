<?php

namespace Acme\Models;

use Acme\Helpers\MainHelper;

/**
 * Class Groups
 * @package Acme\Models
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $status
 */
class Groups extends \Eloquent
{
    /**
     * @var string
     */
    protected $table = 'groups';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function users()
    {
        return $this->hasOne('Acme\Models\User', 'id', 'user_id');
    }

    /**
     * @return $this
     */
    public function userInGroup()
    {
        return $this->belongsToMany('Acme\Models\User', 'users_group', 'groups_id', 'user_id')
            ->withPivot('status');
    }

    /**
     * @return $this
     */
    public function userInvitation()
    {
        return $this->belongsToMany('Acme\Models\User', 'groups_invites', 'group_id', 'user_id')
            ->withPivot('status', 'token');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userProfile()
    {
        return $this->hasOne('Acme\Models\Profile\UserProfile', 'user_id', 'user_id');
    }

    /**
     * Get the group's comments.
     *
     * @return array
     */
    public function comments()
    {
        return $this->hasMany('Acme\Models\GroupComments');
    }
    /**
     * @param $data
     *
     * @return array
     */
    public function saveGeneralInfo($data)
    {
        $result = ['status' => 0, 'error' => null];

        try {

            $oldPicture         = $this->image;
            if(isset($data['user_id'])) {
                $this->user_id = $data['user_id'];
            }
//            $this->user_id      = isset($data['user_id']) ? $data['user_id'] : 0;
            $this->title        = isset($data['title']) ? $data['title'] : '';
            $this->description  = isset($data['description']) ? $data['description'] : '';

            $destinationPath        = \Config::get('app.image_dir.groups');
            $destinationSmallPath   = \Config::get('app.image_dir.groups_small');
            $destinationPrevPath    = \Config::get('app.image_dir.groups_preview');
            $fileName               = '';
            $file                   = false;

            if($data['new_picture']) {
                if (trim($data['new_picture']) != $oldPicture) {
                    \File::delete($destinationPath . '/' . $oldPicture);
                    \File::delete($destinationSmallPath . '/' . $oldPicture);
                    \File::delete($destinationPrevPath . '/' . $oldPicture);
                }

                $this->image       = $data['new_picture'];
            }

            if($data['image']) {
                $file        = $data['image'];
                $fileName    = (str_random(20) . '.' . explode('/', $file->getMimeType())[1]);
                $this->image = $fileName;
            }

            if ($this->save()) {

                if($file)
                {
                    MainHelper::createDirIfNotExist($destinationPath, 0777, true);

                    if($oldPicture) {
                        \File::delete($destinationPath . $oldPicture);
                    }
                    $file->move($destinationPath, $fileName);
                }

                return $result;
            }
        } catch (\Exception $e) {
            return ['status' => 1, 'error' => $e->getMessage()];
        }
    }


    /**
     * @param null $callback
     * @return bool|null
     * @throws \Exception
     */
    public function delete($callback = null)
    {
        $inGroup = $this->userInGroup;

        if (isset($callback)){
            if (is_callable($callback)){
                if (count($inGroup)) {
                        call_user_func($callback, $this, $inGroup);
                    }
            }
        }

        $destinationPath      = \Config::get('app.image_dir.groups') . $this->image;
        $destinationSmallPath = \Config::get('app.image_dir.groups_small') . $this->image;
        $destinationPrevPath  = \Config::get('app.image_dir.groups_preview') . $this->image;

        if (\File::exists($destinationPath)) {
            \File::delete($destinationPath);
            \File::delete($destinationSmallPath);
            \File::delete($destinationPrevPath);
        }

        $this->userInvitation()->detach();
        $this->userInGroup()->detach();

        return parent::delete();
    }


    /**
     * @return string
     */
    public function getTemporarySmallFile()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->image) {

            return asset(\Config::get('app.image_dir.groups_small') . $this->image);
        } else {

            return asset(\Config::get('app.image_dir.groups_small') . $thePic);
        }
    }

    /**
     * @return mixed
     */
    public function getTemporarySmallFileName()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->image) {

            return $this->image;
        } else {

            return $thePic;
        }
    }
}