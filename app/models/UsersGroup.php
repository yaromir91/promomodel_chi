<?php
namespace Acme\Models;

/**
 * Class UsersGroup
 * @package Acme\Models
 *
 * @property integer $id
 * @property integer $groups_id
 * @property integer $user_id
 * @property string $status
 */
class UsersGroup extends \Eloquent
{
    /**
     * @var string
     */
    protected $table = 'users_group';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function users()
    {
        return $this->hasOne('Acme\Models\User', 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasOne('Acme\Models\Groups', 'id', 'groups_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne('Acme\Models\Profile\UserProfile', 'user_id', 'user_id');
    }

    /**
     * @return bool
     */
    public function checkAgent()
    {
        $check = false;
        if ($this->users instanceof \Acme\Models\User) {
            $code = $this->users->getMainUserRoleCode();
            if($code == Role::AGENT_USER){
                $check = true;
            }

        }

        return $check;
    }
}