<?php

namespace Acme\Models;

use Acme\Models\Repositories\TagsRepository;
use Acme\Models\Repositories\GalleryImagesRepository;
use Acme\Models\Tags;
use Acme\Helpers\MainHelper;

class Gallery extends \Eloquent {
	protected $fillable = [];
    protected $table    = 'galleries';

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    public function user()
    {
        return $this->belongsTo('Acme\Models\User', 'user_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany('Acme\Models\Tags', 'galleries_tags', 'gallery_id', 'tag_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('Acme\Models\GalleryImages', 'gallery_id', 'id');
    }

    /**
     * @param $tags
     */
    public function saveTags($tags)
    {
        try {
            $this->tags()->detach();

            if ($tags) {
                $dataTags = explode(',', $tags);

                if (count($dataTags)) {
                    foreach($dataTags as $oneTag) {
                        $presentTag = Tags::Name($oneTag)->first();

                        if (!$presentTag) {
                            $presentTag       = new Tags;
                            $presentTag->name = $oneTag;
                            $presentTag->save();
                        }

                        $this->tags()->attach($presentTag->id);
                    }
                }

            }
        } catch (\Exception $e) {

        }
    }

    /**
     * @return bool|null
     * @throws \Exception
     */
    public function delete()
    {
        $destinationPath   = \Config::get('app.image_dir.album') . $this->user_id . '/' . $this->preview;
        $destSliderPic     = \Config::get('app.image_dir.album_slider') . $this->preview;



        if (\File::exists($destinationPath)) {
            \File::delete($destinationPath);
        }

        if (\File::exists($destSliderPic)) {
            \File::delete($destSliderPic);
        }

        TagsRepository::clearGalleriesTags($this->id);
        GalleryImagesRepository::deleteImageById($this->id);

        return parent::delete();
    }


    /**
     * Save general info about gallery
     *
     * @param $data
     *
     * @return array
     */
    public function saveGeneralInfo($data)
    {
        $result = ['status' => 1, 'error' => null];

        try {

            $oldPicture    = $this->preview;
            $oldUser       = $this->user_id;

            if (isset($data['user_id'])) {
                $this->user_id = $data['user_id'];
            }

            $this->title   = isset($data['title']) ? $data['title'] : '';

            $oldDestPath       = \Config::get('app.image_dir.album') . $oldUser . '/';
            $destinationPath   = \Config::get('app.image_dir.album') . $this->user_id . '/';

            if($data['new_picture']) {

                if (trim($data['new_picture']) != $oldPicture) {
                    if ($oldDestPath != $destinationPath) {
                        \File::delete($oldDestPath . '/' . $oldPicture);
                    } else {
                        \File::delete($destinationPath . '/' . $oldPicture);
                    }
                }

                $this->preview       = $data['new_picture'];
            }

            if($data['preview']) {
                $this->preview        = $data['preview']->getClientOriginalName();
            }

            if ($this->save()) {

                $file = $data['preview'];
                if($file)
                {
                    $fileName        = $destinationPath . $file->getClientOriginalName();

                    MainHelper::createDirIfNotExist($destinationPath, 0777, true);

                    if($oldPicture) {
                        \File::delete($oldDestPath . $oldPicture);
                    }
                    $file->move($destinationPath, $file->getClientOriginalName());

                    $img = \Image::make($fileName);
                    $img->crop(360, 330);
                    $img->save($fileName, \Config::get('app.quality_picture'));
                } elseif (!isset($data['new_picture'])) {
                    if ($oldPicture && $oldDestPath != $destinationPath) {

                        MainHelper::createDirIfNotExist($oldDestPath, 0777, true);
                        MainHelper::createDirIfNotExist($destinationPath, 0777, true);

                        @rename($oldDestPath . $oldPicture, $destinationPath . $oldPicture);
                    }
                }

                return $result;
            }
        } catch (\Exception $e) {

            return ['status' => 1, 'error' => $e->getMessage()];
        }
    }


    /**
     * @return string
     */
    public function getTemporarySmallFile()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->preview) {

            return asset(\Config::get('app.image_dir.album') . $this->user_id . '/' . $this->preview);
        } else {

            return asset(\Config::get('app.image_dir.album') . $this->user_id . '/'  . $thePic);
        }
    }

    /**
     * @return mixed
     */
    public function getTemporarySmallFileName()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->preview) {

            return $this->preview;
        } else {

            return $thePic;
        }
    }

}