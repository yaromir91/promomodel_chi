<?php

namespace Acme\Models\Payments;

/**
 * Class UserApplicantsPayments
 * @package Acme\Models\Payments
 *
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $event
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $user
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $profession
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class UserApplicantsPayments extends \Eloquent
{

    const STATUS_PAID     = 1;
    const STATUS_NOT_PAID = 0;

    protected $table = 'user_applicants_payments';

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('Acme\Models\Events\Events', 'event_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Acme\Models\User', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profession()
    {
        return $this->belongsTo('Acme\Models\Professions\Professions', 'profession_id', 'id');
    }
}