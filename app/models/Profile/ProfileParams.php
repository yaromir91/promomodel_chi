<?php

namespace Acme\Models\Profile;

/**
 * Class ProfileParams
 *
 * @package Acme\Models\Profile
 *
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany roles
 * @property \Illuminate\Database\Eloquent\Relations\HasMany       children
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo     parent
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany user_params
 *
 * @property
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ProfileParams extends \Eloquent
{

    const SELECT_TYPE     = 1;
    const INPUT_INT_TYPE  = 2;
    const TEXT_TYPE       = 3;
    const SELECTABLE_TYPE = 4;
    const STRING_TYPE     = 5;
    const P_CHECKBOX_TYPE = 6;

    const SLIDER_TYPE    = 1; // тип поля в форме подбора, слайдер
    const CHECKBOX_TYPE  = 2; // тип поля в форме подбора, чекбоксы


    // TODO: В будущем эти параметры будут браться из БД
    const MIN_HEIGHT = 120; //рост в см
    const MAX_HEIGHT = 260;
    const STEP_HEIGHT = 1;
    const PIP_STEP_HEIGHT = 10;

    const MIN_CHEST = 70; // грудь в см
    const MAX_CHEST = 120;
    const STEP_CHEST = 1;
    const PIP_STEP_CHEST = 2;

    const MIN_WAIST = 50; // пояс
    const MAX_WAIST = 80;
    const STEP_WAIST = 1;
    const PIP_STEP_WAIST = 2;

    const MIN_BREAST = 80; // бедра
    const MAX_BREAST = 110;
    const STEP_BREAST = 1;
    const PIP_STEP_BREAST = 2;

    const MIN_CLOTH = 40; // размер одежды
    const MAX_CLOTH = 50;
    const STEP_CLOTH = 2;
    const PIP_STEP_CLOTH = 1;

    const MIN_SHOES = 35; // размер обуви
    const MAX_SHOES = 46;
    const STEP_SHOES = 1;
    const PIP_STEP_SHOES = 1;

    public $timestamps = false;
    
    // TODO: нужно придумать более изящную модель маппинга
    public static $relationship = [ //соотношение констант к code параметров
        'height' => [self::MAX_HEIGHT, self::MIN_HEIGHT, self::STEP_HEIGHT, self::PIP_STEP_HEIGHT],
        'chest' => [self::MAX_CHEST, self::MIN_CHEST, self::STEP_CHEST, self::PIP_STEP_CHEST],
        'waist' => [self::MAX_WAIST, self::MIN_WAIST, self::STEP_WAIST, self::PIP_STEP_WAIST],
        'breast' => [self::MAX_BREAST, self::MIN_BREAST, self::STEP_BREAST, self::PIP_STEP_BREAST],
        'cloth' => [self::MAX_CLOTH, self::MIN_CLOTH, self::STEP_CLOTH, self::PIP_STEP_CLOTH],
        'shoes' => [self::MAX_SHOES, self::MIN_SHOES, self::STEP_SHOES, self::PIP_STEP_SHOES],
    ];

    /**
     * @var string
     */
    protected $defaultEditTemplate = 'site.profile.params.empty';

    /**
     * @var string
     */
    protected $defaultViewTemplate = 'site.applicants.params.empty';

    /**
     * @var string
     */
    static protected $defaultSaveField = 'string_value';

    /**
     * @var array
     */
    protected $templatesEditHTML = [
                        self::SELECT_TYPE     => 'site.profile.params.select',
                        self::INPUT_INT_TYPE  => 'site.profile.params.input',
                        self::STRING_TYPE     => 'site.profile.params.input',
                        self::TEXT_TYPE       => 'site.profile.params.text',
                        self::SELECTABLE_TYPE => 'site.profile.params.selectable',
                        self::P_CHECKBOX_TYPE => 'site.profile.params.checkbox',
    ];

    /**
     * @var array
     */
    protected $templatesViewHTML = [
                        self::SELECT_TYPE     => 'site.applicants.params.select',
                        self::INPUT_INT_TYPE  => 'site.applicants.params.input',
                        self::STRING_TYPE     => 'site.applicants.params.input',
                        self::TEXT_TYPE       => 'site.applicants.params.text',
                        self::SELECTABLE_TYPE => 'site.applicants.params.selectable',
                        self::P_CHECKBOX_TYPE => 'site.applicants.params.checkbox',
    ];

    /**
     * @var array
     */
    protected $templatesSearchHTML = [
        self::SLIDER_TYPE     => 'site.events.search.slider',
        self::CHECKBOX_TYPE  => 'site.events.search.checkbox',
    ];

    /**
     * @var array
     */
    static protected $savedFieldByType = [
                        self::SELECT_TYPE     => 'param_id_val',
                        self::INPUT_INT_TYPE  => 'int_value',
                        self::STRING_TYPE     => 'string_value',
                        self::TEXT_TYPE       => 'text_value',
                        self::P_CHECKBOX_TYPE => 'param_id_val',
    ];

    /**
     * @var array
     */
    static protected $typeNames = [
        self::SELECT_TYPE     => 'Выбор',
        self::INPUT_INT_TYPE  => 'Число',
        self::STRING_TYPE     => 'Текстовое поле',
        self::TEXT_TYPE       => 'Текс',
        self::P_CHECKBOX_TYPE => 'Да/Нет',
        self::SELECTABLE_TYPE => 'Элемент выбора',
    ];


    /**
     * @param $isParent
     *
     * @return array
     */
    static public function getAllTypeNames($isParent = false)
    {
        if ($isParent) {
            return [
                self::SELECTABLE_TYPE => 'Элемент выбора'
            ];
        } else {
            return [
                self::SELECT_TYPE     => 'Выбор',
                self::INPUT_INT_TYPE  => 'Число',
                self::STRING_TYPE     => 'Текстовое поле',
                self::TEXT_TYPE       => 'Текс',
                self::P_CHECKBOX_TYPE => 'Да/Нет'
            ];
        }
    }


    /**
     * @return array
     */
    static public function getTypesForSelect()
    {
        return [
            self::SELECT_TYPE     => self::SELECT_TYPE,
            self::P_CHECKBOX_TYPE => self::P_CHECKBOX_TYPE
        ];
    }

    /**
     * @param $type
     *
     * @return string
     */
    static public function getTypeNames($type)
    {
        if (isset(self::$typeNames[$type])) {

            return trim(self::$typeNames[$type]);
        }

        return '';
    }

    /**
     * @param $type
     *
     * @return string
     */
    static public function getSavedFieldByType($type)
    {
        if (isset(self::$savedFieldByType[$type])) {

            return trim(self::$savedFieldByType[$type]);
        }

        return self::$defaultSaveField;
    }

    /**
     * Get HTML template for display edit parameters
     *
     * @return string
     */
    public function getTemplateByType()
    {
         if (isset($this->templatesEditHTML[$this->type])) {

             return $this->templatesEditHTML[$this->type];
         }

        return $this->defaultEditTemplate;
    }

    /**
     * @return string
     */
    public function getViewTemplateByType()
    {
        if (isset($this->templatesViewHTML[$this->type])) {

            return $this->templatesViewHTML[$this->type];
        }

        return $this->defaultViewTemplate;
    }

    /**
     * Get HTML template for display search parameters
     *
     * @return string
     */
    public function getSearchTemplateByType()
    {
        if (isset($this->templatesSearchHTML[$this->search_type])) {

            return $this->templatesSearchHTML[$this->search_type];
        }

        return $this->defaultEditTemplate;
    }

    public function getTranslatedParamTitle($locale)
    {
        switch($locale) {
            case 'ru' :
                $result = $this->name;
                break;
            case 'en' :
                $result = $this->name_en;
                break;
        }

        return $result;
    }

    /**
     * @param $id
     * @param bool $isName
     *
     * @return ProfileParams|bool|mixed
     */
    static public function getSelectedResult($id, $isName = false)
    {
        $param = self::find($id);

        if ($param instanceof ProfileParams) {
            $locale = \App::getLocale();
            if($isName) {
                switch($locale) {
                    case 'en' :
                        return $param->name_en;
                        break;
                    case 'ru' :
                        return $param->name;
                        break;
                }
            } else {
                return $param;
            }
        }

        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user_params()
    {
        return $this->belongsToMany('Acme\Models\Profile\UserProfile', 'user_profile_data', 'params_id', 'user_id')
            ->withPivot('int_value', 'string_value', 'text_value', 'bool_value', 'param_id_val');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('Acme\Models\Profile\ProfileParams', 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('Acme\Models\Role', 'roles_profile_params', 'profile_params_id', 'roles_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('Acme\Models\Profile\ProfileParams', 'parent_id');
    }

}