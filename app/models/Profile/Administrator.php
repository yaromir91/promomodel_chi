<?php

namespace Acme\Models\Profile;

use Acme\Models\User;

/**
 * Class Administrator
 * @package Acme\Models\Profile
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class Administrator extends User
{

}