<?php

namespace Acme\Models\Profile;

use Acme\Models\User;
use Acme\Models\Role;
use Acme\Models\Profile\ProfileParams;

/**
 * Class UserProfile
 *
 * @property \Illuminate\Database\Eloquent\Relations\HasOne        user
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany params
 *
 * @package Acme\Models\Profile
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class UserProfileData extends \Eloquent
{

    /**
     * @var string
     */
    protected $table = 'user_profile_data';


    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return array
     */


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function params()
    {
        return $this->belongsToMany('Acme\Models\Profile\ProfileParams', 'user_profile_data', 'user_id', 'params_id')
                    ->withPivot('int_value', 'string_value', 'text_value', 'bool_value', 'param_id_val');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('Acme\Models\User', 'foreign_key', 'local_key');
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

}