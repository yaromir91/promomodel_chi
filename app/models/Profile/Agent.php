<?php

namespace Acme\Models\Profile;

use Acme\Models\User;

/**
 * Class Agent
 * @package Acme\Models\Profile
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class Agent extends User
{

}