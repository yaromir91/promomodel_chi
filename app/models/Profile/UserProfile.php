<?php

namespace Acme\Models\Profile;

use Acme\Models\User;
use Acme\Models\Role;
use Acme\Models\Profile\ProfileParams;

/**
 * Class UserProfile
 *
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany professions
 * @property \Illuminate\Database\Eloquent\Relations\HasOne        user
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany params
 *
 * @package Acme\Models\Profile
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class UserProfile extends \Eloquent
{

    const DEFAULT_GENDER = 'm';

    const MALE_GENDER    = 'm';
    const FEMALE_GENDER  = 'f';

    /**
     * @var string
     */
    protected $table = 'user_profile';

    /**
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return array
     */
    static public function getAvailableRolesForReg()
    {

        return [Role::APPLICANT_USER, Role::ORGANIZER_USER];
    }

    /**
     * @return array
     */
    static public function getAvailableRolesForAgentReg()
    {

        return [Role::APPLICANT_USER, Role::APPLICANT_MANAGER_USER];
    }

    /**
     * @return array
     */
    static public function getAvailableRolesForAdminReg()
    {

        return [Role::APPLICANT_USER, Role::ORGANIZER_USER, Role::APPLICANT_MANAGER_USER, Role::AGENT_USER];
    }

    /**
     * @return array
     */
    static public function getGenderTitles()
    {
        return [
            self::FEMALE_GENDER => \Lang::get('profile.Female'),
            self::MALE_GENDER   => \Lang::get('profile.Male')
        ];
    }


    /**
     * @param array $dataIds
     * @param array $dataRateH
     * @param array $dataRateD
     * @param bool $withClear
     */
    public function attachProfessions($dataIds = [], $dataRateH = [], $dataRateD = [], $withClear = true)
    {
        if ($withClear) {
            $this->professions()->detach();
        }

        if (!empty($dataIds) && (!empty($dataRateH) || !empty($dataRateD))) {
            foreach($dataIds as $id) {
                $dRD = isset($dataRateD[$id]) ? $dataRateD[$id] : 0;
                $dRH = isset($dataRateH[$id]) ? $dataRateH[$id] : 0;
                $this->professions()->attach($id, ['rate_h' => $dRH, 'rate_d' => $dRD]);
            }
        }
    }


    /**
     * @return int|string
     */
    public function getAge()
    {
        $age = '';

        if (date_default_timezone_get()) {
            $tz = date_default_timezone_get();
        } else {
            $tz = 'UTC';
        }

        if (!empty($this->date_birth)) {
            $tzo   = new \DateTimeZone($tz);
            $ageObj = \DateTime::createFromFormat('Y-m-d', $this->date_birth, $tzo);
            if ($ageObj instanceof \DateTime) {
                $age = $ageObj->diff(new \DateTime('now', $tzo))->y;
            }
        }

        return $age;
    }


    /**
     * @param $gender
     *
     * @return string
     */
    static public function getUserGender($gender)
    {
        $titles = self::getGenderTitles();

        if (!empty($gender)) {
           if (isset($titles[$gender])) {

               return $titles[$gender];
           }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        $fullName = '';

        if (!empty($this->last_name)) {
            $fullName .= $this->last_name;
        }

        if (!empty($this->first_name)) {
            $fullName .= ($fullName != '') ? (' ' . $this->first_name) : $this->first_name;
        }

        return $fullName;
    }

    /**
     * @param array $paramsData
     * @param array $typesArr
     *
     * @param bool $withClear
     */
    public function fillProfileParams($paramsData, $typesArr = [], $withClear = true)
    {
        if ($withClear) {
            $this->params()->detach();
        }

        if (!empty($paramsData) && !empty($typesArr)) {
            foreach ($paramsData as $k => $v) {
                if (isset($typesArr[$k])) {

                    if(is_array($v)) {
                        foreach($v as $a) {
                            $dataToSave = [
                                ProfileParams::getSavedFieldByType($typesArr[$k]) => $a,
                            ];

                            $this->params()->attach($k, $dataToSave);
                        }
                    } else {

                        if (!empty($v)) {
                            $dataToSave = [
                                ProfileParams::getSavedFieldByType($typesArr[$k]) => $v,
                            ];

                            $this->params()->attach($k, $dataToSave);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param ProfileParams $param
     *
     * @return null
     */
    public function getParamValue(ProfileParams $param)
    {
        if ($this->params->contains($param->id))
        {
            $field = ProfileParams::getSavedFieldByType($param->type);

            return $this->params->find($param->id)->pivot->$field;
        }

        return null;
    }

    /**
     * @param $paraID
     * @param $checkedID
     * @param bool $isGetObj
     *
     * @return array|bool|null|static
     */
    public function checkValueCheckbox($paraID, $checkedID, $isGetObj = false)
    {
        $param = \DB::table('user_profile_data')
            ->where('user_id',      '=', $this->user_id)
            ->where('params_id',    '=', $paraID)
            ->where('param_id_val', '=', $checkedID)
            ->get();

        if ($isGetObj) {

            return $param;
        } else {

            return !empty($param);
        }


        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function params()
    {
        return $this->belongsToMany('Acme\Models\Profile\ProfileParams', 'user_profile_data', 'user_id', 'params_id')
                    ->withPivot('int_value', 'string_value', 'text_value', 'bool_value', 'param_id_val');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('Acme\Models\User', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function professions()
    {
        return $this->belongsToMany('Acme\Models\Professions\Professions', 'user_professions_rate', 'user_id', 'professions_id')->withPivot('rate_h', 'rate_d');
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return string
     */
    public function getTemporaryFile()
    {
        $thePic = \Input::old('new_picture');

        if ($thePic) {

            return asset(\Config::get('app.image_dir.profile') . $thePic);
        } elseif ($this->avatar) {

            return asset(\Config::get('app.image_dir.profile') . $this->avatar);
        }

        return '';
    }

    /**
     * @return mixed
     */
    public function getTemporaryFileName()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->avatar) {

            return $this->avatar;
        } else {

            return $thePic;
        }
    }

    /**
     * @param null $callback
     * @return bool|null
     * @throws \Exception
     */
    public function delete($callback = null)
    {
        if (isset($callback)){
            if (is_callable($callback)){
                call_user_func($callback, $this);
            }
        }

        $destinationPath = \Config::get('app.image_dir.profile') . $this->avatar;

        if (\File::exists($destinationPath)) {
            \File::delete($destinationPath);
        }

        return parent::delete();
    }
}