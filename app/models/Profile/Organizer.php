<?php

namespace Acme\Models\Profile;

use Acme\Models\User;

/**
 * Class Organizer
 * @package Acme\Models\Profile
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class Organizer extends User
{

}