<?php

namespace Acme\Models\Repositories;

use Acme\Models\Profile\UserProfile;
use Acme\Models\Role;

/**
 * Class GalleryRepository
 *
 * @package Acme\Models\Repositories
 *
 *  @author Sergey Bespalov
 */
class GalleryRepository
{
    static protected $filter_org           = 'org';
    static protected $filter_app           = 'app';

    /**
     * @param $code
     *
     * @return bool
     */
    static public function getPublicGalleries()
    {
        $galleries = \DB::table('galleries')
            ->join('user_profile', 'user_profile.user_id', '=', 'galleries.user_id')
            ->where('user_profile.public', '=', 1)
            ->get();

        return $galleries;
    }

    /**
     * @param      $user_id
     * @param bool $list
     *
     * @return mixed
     */
    static public function getUserGalleries($user_id, $list = false)
    {
        if($list) {
            $galleries = \DB::table('galleries')
                ->join('user_profile', 'user_profile.user_id', '=', 'galleries.user_id')
                ->where('galleries.user_id', '=', $user_id)
                ->lists('title', 'id');
        } else {
            $galleries = \DB::table('galleries')
                ->join('user_profile', 'user_profile.user_id', '=', 'galleries.user_id')
                ->where('galleries.user_id', '=', $user_id)
                ->get();
        }

        return $galleries;
    }

    /**
     * @param $filter
     *
     * @return mixed
     */
    static public function getPublicFilterGalleries($filter)
    {
        $galleries = \DB::table('galleries')
            ->select('galleries.*', 'user_profile.*')
            ->leftJoin('user_profile', 'user_profile.user_id', '=', 'galleries.user_id')
            ->leftJoin('assigned_roles', 'assigned_roles.user_id', '=', 'galleries.user_id')
            ->leftJoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
            ->where('user_profile.public', '=', 1);

        if($filter == self::$filter_org) {
            $galleries->where('roles.code', '=', Role::ORGANIZER_USER);
        }else if($filter == self::$filter_app) {
            $galleries->where('roles.code', '=', Role::APPLICANT_USER)->orWhere('roles.code', '=', Role::APPLICANT_MANAGER_USER);
        } else {
            return false;
        }

        return $galleries->get();
    }
}