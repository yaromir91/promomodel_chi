<?php

namespace Acme\Models\Repositories;

use Acme\Models\Profile\UserProfile;
use Acme\Models\Role;
use Acme\Helpers\PictureHelper;
use Acme\Models\Gallery;
use Acme\Models\GalleryImages;

/**
 * Class GalleryImagesRepository
 *
 * @package Acme\Models\Repositories
 *
 *  @author Sergey Bespalov
 */
class GalleryImagesRepository
{


    /**
     * @param $userId
     * @param $count
     *
     * @return array|static[]
     */
    static public function getImageForPreview($userId, $count)
    {
        if ($count > 0) {
            $galleriesTable    = \DB::getTablePrefix().Gallery::getTableName();
            $galleriesImgTable = \DB::getTablePrefix().GalleryImages::getTableName();

            $images = \DB::table('gallery_images')
                ->join('galleries', 'galleries.id', '=', 'gallery_images.gallery_id')
                ->where('galleries.user_id', '=', $userId)
                ->whereRaw('RAND() < (select
                            ((1 / COUNT(*)) * 10)
                        from
                            '.$galleriesImgTable.' as gi
                                left join
                            '.$galleriesTable.' as g ON g.id = gi.gallery_id
                        where
                            g.user_id = '.$userId.')')
                ->select('gallery_images.id', 'gallery_images.gallery_id', 'gallery_images.file', 'galleries.title', 'galleries.user_id')
                ->get();

            return $images;
        }
    }

    /**
     * @param $album_id
     *
     * @return array|static
     */
    static public function getImagesOfAlbum($album_id)
    {
        $images = \DB::table('gallery_images')
            ->join('galleries', 'galleries.id', '=', 'gallery_images.gallery_id')
            ->where('gallery_images.gallery_id', '=', $album_id)
            ->select('gallery_images.id', 'gallery_images.gallery_id', 'gallery_images.file', 'galleries.title', 'galleries.user_id')
            ->get();

        return $images;
    }

    static public function getImagesOfUser($user_id, $limit = false)
    {
        $query = \DB::table('gallery_images')
            ->join('galleries', 'galleries.id', '=', 'gallery_images.gallery_id')
            ->where('galleries.user_id', '=', $user_id)
            ->select('gallery_images.id', 'gallery_images.gallery_id', 'gallery_images.file', 'galleries.title');

        if ($limit) {
            $query->limit($limit);
        }

        return $query->get();
    }

    static public function deleteImageById($id)
    {
        $image = \DB::table('gallery_images')
            ->where('id', '=', $id)
            ->delete();

        return $image;
    }

    /**
     * @param $name
     *
     * @return int
     */
    static public function deleteImageByName($name)
    {
        return \DB::table('gallery_images')
            ->where('file', '=', $name)
            ->delete();
    }

    /**
     * @param $gallery
     */
    static public function deleteAllGalleryImages($gallery)
    {
       if ($gallery instanceof \Acme\Models\Gallery) {

           $destinationPath = \Config::get('app.image_dir.album') . $gallery->user_id;

           if (file_exists($destinationPath)) {
               $images = $gallery->images;

               if (count($images)) {
                    foreach ($images as $img) {
                        \File::delete($destinationPath . '/' . $img->file);
                        $img->delete();
                    }
               }
           }
       }
    }

    /**
     * @param $userId
     *
     * @return int
     */
    static public function getImageCountByUser($userId)
    {
        $imagesCount = \DB::table('gallery_images')
            ->join('galleries', 'galleries.id', '=', 'gallery_images.gallery_id')
            ->where('galleries.user_id', '=', $userId)
            ->whereNotNull('gallery_images.id')
            ->count();

        return $imagesCount;
    }

    /**
     * @param $userId
     *
     * @return int
     */
    static public function getGalleryPreviewCountByUser($userId)
    {
        $imagesCount = \DB::table('galleries')
            ->whereRaw('preview != ""')
            ->where('user_id', '=', $userId)
            ->count();

        return $imagesCount;
    }

    /**
     * @param $userId
     * @param int $limit
     *
     * @return array|static[]
     */
    static public function getUserAlbums($userId, $limit = 3)
    {
        $albums = \DB::table('galleries')
            ->where('user_id', '=', $userId)
            ->limit($limit)
            ->get();

        return $albums;
    }

    /**
     * updateAllGalleryPreview
     */
    public static function updateAllGalleryPreview()
    {
        $albums = \DB::table('galleries')
            ->whereNotNull('preview')
            ->get();

        $path           = public_path();
        $destSliderPath = \Config::get('app.image_dir.album_slider');

        if (!is_dir($destSliderPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destSliderPath, 0777, true);
            umask($oldmask);
        }

        if (count($albums)) {
            foreach ($albums as $img) {
                $destinationPath = $path . '/' . \Config::get('app.image_dir.album') .$img->user_id;
                $prevImg         = $path . '/' . $destSliderPath . $img->preview;


                if (!is_dir($destinationPath)) {
                    $oldmask = umask(0);
                    \File::makeDirectory($destinationPath, 0777, true);
                    umask($oldmask);
                }

                $origImg   = $destinationPath . '/' . $img->preview;

                if (\File::exists($origImg)) {
                    if (!\File::exists($prevImg)) {
                        copy($origImg, $prevImg);
                        PictureHelper::img_resize($prevImg, $prevImg, \Config::get('app.gall_slider_width'), \Config::get('app.gall_slider_height'), 0xF0F0F0, 60);

                    }
                }
            }
        }
    }

    /**
     * @param int $offset
     * @param bool|false $limit
     */
    public static function updateAllPicGallery($offset = 0, $limit = false)
    {
        $query = \DB::table('gallery_images')
            ->join('galleries', 'galleries.id', '=', 'gallery_images.gallery_id')
            ->select('gallery_images.file', 'galleries.user_id');

        if ($limit) {
            $query->offset($offset)
                ->limit($limit);
        }

        $images = $query->get();
        $path   = public_path();

        if (count($images)) {
            foreach ($images as $img) {
                $origImg   = $path . '/' . \Config::get('app.image_dir.album') . $img->user_id . '/' . $img->file;
                $smallImg  = $path . '/' . \Config::get('app.image_dir.album') . $img->user_id .'/small/' . $img->file;
                $prevImg   = $path . '/' . \Config::get('app.image_dir.album') . $img->user_id .'/preview/' . $img->file;

                $smallPath = $path . '/' . \Config::get('app.image_dir.album') . $img->user_id .'/small/';
                $prevPath  = $path . '/' . \Config::get('app.image_dir.album') . $img->user_id .'/preview/';

                if (\File::exists($origImg)) {
                    if (!\File::exists($smallImg)) {

                        if (!file_exists($smallPath)) {
                            \File::makeDirectory($smallPath, 0755, true);
                        }

                        copy($origImg, $smallImg);
                        PictureHelper::img_resize_if_big($origImg, $smallImg, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'), $color = 0xF0F0F0, 60);
                    }

                    //if (!\File::exists($prevImg)) {

                        if (!file_exists($prevPath)) {
                            \File::makeDirectory($prevPath, 0755, true);
                        }

                        copy($origImg, $prevImg);
                       PictureHelper::img_resize_if_big($prevImg, $prevImg, \Config::get('app.gall_slider_width'), \Config::get('app.gall_slider_height'), $color = 0xF0F0F0, 60);
                       /* $img = \Image::make($prevImg);
                        $img->fit(\Config::get('app.gall_slider_width'), \Config::get('app.gall_slider_height'), null, 'top');
                        $img->save($prevImg);*/
                   // }
                }
            }
        }
    }
}