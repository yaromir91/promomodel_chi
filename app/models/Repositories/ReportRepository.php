<?php

namespace Acme\Models\Repositories;

use Acme\Models\Post;
use Acme\Models\Ratings;

/**
 * Class ReportRepository
 * @package Acme\Models\Repositories
 */
class ReportRepository
{

    /**
     * @param int $limit
     * @return \Paginator
     */
    public static function getPopular($limit = 5) {

        $post = new Post();
        $ratings_table = with(new Ratings)->getTable();
        $ratings_table = \DB::getTablePrefix() . $ratings_table;

        $reports = $post->where('posts.status', 0)
                        ->where('posts.created_at', '>', date('Y-m-d H:m:s', time() - 31536000))
                        ->orderBy('rating', 'DESC')
                        ->select('posts.*',
                            \DB::raw( '(SELECT AVG( ' . $ratings_table . '.rating ) FROM ' . $ratings_table . ' WHERE ' . $ratings_table . '.object_id = prm_posts.id AND ' . $ratings_table . '.type = "post" ) AS rating' ))
                        ->limit($limit)
                        ->get();

        return $reports;
    }

    /**
     * @param $user_id
     * @param bool|false $paginate
     * @param bool|false $limit
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator|static[]
     */
    public static function getPersoanlReports($user_id, $paginate = false, $limit = false)
    {
        $post = new Post();
        $ratings_table = with(new Ratings)->getTable();
        $ratings_table = \DB::getTablePrefix() . $ratings_table;

        $reports = $post->
        where('posts.user_id', $user_id)->
        where('posts.created_at', '>', date('Y-m-d H:m:s', time() - 31536000))->
        orderBy('created_at', 'DESC')->
        select('posts.*', \DB::raw( '(SELECT AVG( ' . $ratings_table . '.rating ) FROM ' . $ratings_table . ' WHERE ' . $ratings_table . '.object_id = prm_posts.id AND ' . $ratings_table . '.type = "post" ) AS rating' ));

        if ($limit) {
            $reports->limit($limit);
        }

        if($paginate) {
            $result = $reports->paginate($paginate);
        } else {
            $result = $reports->get();
        }


        return $result;
    }

}