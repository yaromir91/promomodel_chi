<?php

namespace Acme\Models\Repositories;

use Acme\Models\Events\EventsType;

/**
 * Class EventsTypeRepository
 *
 * @package Acme\Models\Repositories
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class EventsTypeRepository
{

    /**
     * @param bool $isSelect
     *
     * @return array|static
     */
    static public function getEvensTypes($isSelect = false) {


        $eventsType = \DB::table('events_type')
            ->orderBy('title', 'desc')
            ->get();

        if ($isSelect) {

            $result = [];

            if (count($eventsType)) {
                foreach ($eventsType as $et) {
                    $result[$et->id] = $et->title;
                }
            }

            return $result;
        }

        return $eventsType;
    }

    static public function getEventTypes()
    {
        $event_types_obj = EventsType::all();
        foreach($event_types_obj as $obj)
        {
            $event_types[$obj->id] = $obj->title;
        }

        return $event_types;
    }
}