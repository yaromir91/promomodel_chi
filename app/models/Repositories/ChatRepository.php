<?php

namespace Acme\Models\Repositories;

use Acme\Helpers\MainHelper;
use Acme\Models\Chat\UserChats;
use Acme\Models\Chat\UserChatMessages;

/**
 * Class ChatRepository
 * @package Acme\Models\Repositories
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ChatRepository
{


    /**
     * @param $userId
     * @return array|static[]
     */
    public static function getUserChats($userId)
    {
        $chatMsgable    = \DB::getTablePrefix().UserChatMessages::getTableName();

        return \DB::table('user_chats')
            ->select([
                'user_chats.code',
                'user_chats.user_sender_id',
                'user_chats.user_receiver_id',
                \DB::raw('SUM('.$chatMsgable.'.is_m_read = '.UserChatMessages::READ_ON.') AS read_count'),
                \DB::raw('SUM('.$chatMsgable.'.is_m_read = '.UserChatMessages::READ_OFF.' and '.$chatMsgable.'.user_sender_id != '.$userId.') AS uread_count'),
                \DB::raw('count('.$chatMsgable.'.id) as all_count'),
                \DB::raw('p1.last_name as last_name_u1'),
                \DB::raw('p1.first_name as first_name_u1'),
                \DB::raw('p2.last_name as last_name_u2'),
                \DB::raw('p2.first_name as first_name_u2')
            ])
            ->leftJoin('user_chat_messages', function(\Illuminate\Database\Query\JoinClause $join) use($userId)
            {
                $join->on('user_chat_messages.chat_code', '=', 'user_chats.code')
                    ->where('user_chat_messages.is_deleted', '=', UserChatMessages::DELETED_MSG_OFF);
            })
            ->join('users as u1', 'user_chats.user_sender_id', '=', \DB::raw('u1.id'))
            ->join('users as u2', 'user_chats.user_receiver_id', '=', \DB::raw('u2.id'))
            ->join('user_profile as p1', \DB::raw('p1.user_id'), '=', \DB::raw('u1.id'))
            ->join('user_profile as p2', \DB::raw('p2.user_id'), '=', \DB::raw('u2.id'))
            ->where(function($query) use($userId)  {
                $query->where('user_chats.user_sender_id', '=', $userId);
                $query->orWhere('user_chats.user_receiver_id', '=', $userId);
            })
            ->groupBy('user_chats.code')
            ->having('all_count', '>', 0)
            ->orderBy('user_chats.created_at', 'desc')
            ->get();
    }

    /**
     * @param $userId1
     * @param $userId2
     *
     * @return string
     */
    static public function getUsersChatCode($userId1, $userId2)
    {
        if ($userId1 == $userId2) {

            return null;
        }

        $theChat = \DB::table('user_chats')
            ->select(['user_chats.code', 'user_chats.user_sender_id', 'user_chats.user_receiver_id'])
            ->where(function($query) use($userId1, $userId2)  {
                $query->where('user_chats.user_sender_id', '=', $userId1);
                $query->where('user_chats.user_receiver_id', '=', $userId2);
            })
            ->orWhere(function($query) use ($userId1, $userId2){
                $query->where('user_chats.user_receiver_id', '=', $userId1);
                $query->where('user_chats.user_sender_id', '=', $userId2);
            })
            ->limit(1)
            ->first();

        if (!empty($theChat)) {

            return $theChat->code;
        } else {

            return self::generateChatCode($userId1, $userId2);
        }
    }

    /**
     * Generate random Chat code
     *
     * @return string
     */
    static private function generateChatCode($userId1, $userId2)
    {
        $code    = MainHelper::generateRandCodeUniq($userId1, $userId2);
        $theChat = self::getChatByCode($code);

        if ($theChat instanceof UserChats) {

            return self::generateChatCode($userId1, $userId2);
        } else {

            try {
                $theChat                   = new UserChats();
                $theChat->code             = $code;
                $theChat->user_sender_id   = $userId1;
                $theChat->user_receiver_id = $userId2;
                $theChat->save();

                return $code;
            } catch (\Exception  $e) {

                return null;
            }
        }
    }

    /**
     * @param $userId
     *
     * @return int
     */
    public static function getUserNotReadMessage($userId)
    {
        return  \DB::table('user_chats')
            ->join('user_chat_messages', 'user_chat_messages.chat_code', '=', 'user_chats.code')
            ->where(function($query) use($userId)  {
                $query->where('user_chats.user_sender_id', '=', $userId);
                $query->orWhere('user_chats.user_receiver_id', '=', $userId);
            })
            ->where('user_chat_messages.user_sender_id', '!=', $userId)
            ->where('user_chat_messages.is_deleted', '=', UserChatMessages::DELETED_MSG_OFF)
            ->where('user_chat_messages.is_m_read', '=', UserChatMessages::READ_OFF)
            ->count();
    }

    /**
     * @param $code
     * @return mixed|static
     */
    static public function getChatByCode($code)
    {
        return UserChats::where('code', '=', $code)
            ->limit(1)
            ->first();
    }
}