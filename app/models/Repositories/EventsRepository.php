<?php

namespace Acme\Models\Repositories;

use Acme\Models\Events\Events;
use Acme\Models\Events\EventInvite;
use Acme\Models\User;
use Acme\Models\Role;
use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Acme\Helpers\PictureHelper;
use Illuminate\Support\Facades\Lang;

/**
 * Class EventsRepository
 *
 * @package Acme\Models\Repositories
 */
class EventsRepository
{

    public static function getPaimentStatusTitle($status)
    {
        $titles = [
            Events::PAIMENT_STATUS_NEW      => \Lang::get('events.paiment_status_title_new'),
            Events::PAIMENT_STATUS_HOLD     => \Lang::get('events.paiment_status_title_hold'),
            Events::PAIMENT_HOLD_ACCEPT     => \Lang::get('events.paiment_status_title_accept_hold'),
            Events::PAIMENT_HOLD_CANCEL     => \Lang::get('events.paiment_status_title_cancel_hold'),
            Events::PAIMENT_STATUS_PAID     => \Lang::get('events.paiment_status_title_paid'),
            Events::PAIMENT_STATUS_CANCEL   => \Lang::get('events.paiment_status_title_cancel'),
        ];

        if($status && isset($titles[$status])) {
            return $titles[$status];
        } else {
            return false;
        }
    }

    public static function getFuturePublicEvents($object = false, $limit = 1, $offset = 0) {
        $events = Events::where('permission_view', '=',1)
            ->where('date_end', '>', new \DateTime('today'))
            ->where('paiment_status', '=', Events::PAIMENT_STATUS_HOLD)
            ->orderBy('date_start', 'desc')
            ;
        if($object){
            return $events->orderByRaw('RAND()')->limit($limit)->get();
        }

        return $events->get();
    }

    public static function getPastPublicEvents() {
        $events = Events::where('permission_view', '=',1)
            ->where('date_end', '<', new \DateTime('today'))
            ->where('paiment_status', '=', Events::PAIMENT_STATUS_HOLD)
            ->orderBy('date_start', 'desc')
            ->get();

        return $events;
    }

    /**
     * @param User $user
     * @param int $status
     * @param bool|false $inTeamStatus
     * @param bool|false $limit
     * @return array|static[]
     */
    public static function getMyEvents(User $user, $status = EventInvite::INVITE_PROF_STATUS_ACCEPT, $inTeamStatus = false, $limit = false) {

        $events   = [];
        $codeRole = $user->getMainUserRoleCode();

        if ($codeRole == Role::APPLICANT_USER || $codeRole == Role::APPLICANT_MANAGER_USER) {

            $query = Events::join('event_invite', 'event_invite.events_id', '=', 'events.id')
                ->where('event_invite.users_id', '=', $user->id)
                ->where('event_invite.status', '=', $status)
                ->select(
                    'events.id',
                    'events.type_id',
                    'events.users_id',
                    'events.title',
                    'events.date_start',
                    'events.date_end',
                    'events.image',
                    'event_invite.is_manager',
                    'event_invite.manager_status',
                    'events.description',
                    'events.repeat_status',
                    'events.repeat_years',
                    'events.repeat_month',
                    'events.repeat_weekday',
                    'events.repeat_day',
                    'events.repeat_end'
                );

            if ($inTeamStatus) {
                if (is_array($inTeamStatus)) {
                    $query->where(function($query) use($inTeamStatus)  {
                        foreach ($inTeamStatus as $sv) {
                            $query->orWhere('event_invite.in_team', '=', $sv);
                        }
                    });
                } else {
                    $query->where('event_invite.in_team', '=', $inTeamStatus);
                }
            }

            if ($limit) {
                $query->limit($limit);
            }

            $events = $query->get();

        } elseif ($codeRole == Role::ORGANIZER_USER) {

            $query = Events::where('users_id', '=', $user->id)
                ;
            if ($limit) {
                $query->limit($limit);
            }

            $events = $query->get();
        }

        return $events;
    }

    /**
     *
     * @return array
     */
    public static function getMyEventsForReports($ids = false) {

        $user = \Auth::user();
        if(!empty($user->id))
        {
            if($ids) {
                $events = \DB::table('events')
                    ->where('users_id', '=', $user->id)
                    ->lists('id');
            } else {
                $events = \DB::table('events')
                    ->where('users_id', '=', $user->id)
                    ->lists('title', 'id');
            }
        }
        else
        {
            $events = array();
        }

        return $events;
    }

    public static function getEventForReport($report_id)
    {
        $event = \DB::table('events')
            ->where('id', '=', $report_id)
            ->first();

        return $event;
    }
    /**
     * @return array|static[]
     */
    public static function getEventsForReport()
    {
        $events = \DB::table('events')
            ->whereRaw('report_date=CURDATE()')
            ->select(['id'])
            ->get();

        return $events;
    }

    /**
     * @param $user_id
     *
     * @return mixed
     */
    public static function getAcceptedEvents($user_id) {

        $events = \DB::table('event_invite')
            ->join('events', 'events.id', '=', 'event_invite.events_id')
            ->where('event_invite.users_id', '=', $user_id)
            ->where('event_invite.status', '=', EventInvite::INVITE_PROF_STATUS_ACCEPT)
            ->get();

        return $events;
    }

    /**
     * @param int $offset
     * @param bool|false $limit
     */
    public static function updateAllPreview($offset = 0, $limit = false)
    {
        $query = \DB::table('events')
            ->select('events.image');

        if ($limit) {
            $query->offset($offset)
                ->limit($limit);
        }

        $images = $query->get();
        $path   = public_path();

        if (count($images)) {
            foreach ($images as $img) {
                $origImg  =  $path . '/' .\Config::get('app.image_dir.events') . $img->image;
                $smallImg =  $path . '/' .\Config::get('app.image_dir.events_small') . $img->image;
                $previImg =  $path . '/' .\Config::get('app.image_dir.events_preview') . $img->image;

                if (\File::exists($origImg)) {
                    if (!\File::exists($smallImg)) {
                        copy($origImg, $smallImg);
                        PictureHelper::img_resize_if_big($origImg, $smallImg, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'), 0xF0F0F0,  \Config::get('app.quality_picture'));
                    }
                    if (!\File::exists($previImg)) {
                        copy($origImg, $previImg);
                        $img = \Image::make($origImg);
                        $img->fit(\Config::get('app.preview_width'), \Config::get('app.preview_height'));
                        $img->save($previImg,  \Config::get('app.quality_picture'));
                        //PictureHelper::img_resize_if_big($origImg, $previImg, \Config::get('app.preview_width'), \Config::get('app.preview_height'), 0xF0F0F0, 60);
                    }
                }
            }
        }
    }
}