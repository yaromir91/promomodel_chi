<?php

namespace Acme\Models\Repositories;

use Acme\Models\Events\EventInvite;
use Acme\Models\Events\Events;
use Acme\Models\User;
use Acme\Models\Role;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EventInviteRepository
 * @package Acme\Models\Repositories
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class EventInviteRepository
{

    /**
     * @param User $user
     * @param bool|false $limit
     *
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    static public function getUserInvitation(User $user, $limit = false)
    {
        $invitation   = [];
        $userCodeRole = $user->getMainUserRoleCode();

        if ($userCodeRole == Role::APPLICANT_MANAGER_USER || $userCodeRole == Role::APPLICANT_USER) {

            $query = EventInvite::where('users_id', '=', $user->id);

            if ($limit) {
                $query->limit($limit);
            }

            $invitation = $query->get();

        } elseif ($userCodeRole == Role::ORGANIZER_USER) {

            $query = EventInvite::join('events', 'events.id', '=', 'event_invite.events_id')
                ->where('events.users_id', '=', $user->id)
                ->select(
                    'event_invite.id',
                    'event_invite.events_id',
                    'event_invite.users_id',
                    'event_invite.professions_id',
                    'event_invite.status',
                    'event_invite.is_manager',
                    'event_invite.manager_status',
                    'event_invite.manager_status',
                    'event_invite.in_team',
                    'event_invite.manager_in_team'
                )
                ;

            if ($limit) {
                $query->limit($limit);
            }

            $invitation = $query->get();
        }

        return $invitation;
    }

    /**
     * @param $eventID
     * @param $userId
     * @param $notStatus
     *
     * @return mixed|static
     */
    static public function findInviteForUser($eventID, $userId, $notStatus)
    {

        return \DB::table('event_invite')
            ->leftJoin('professions', 'event_invite.professions_id', '=', 'professions.id')
            ->where('events_id', '=',  $eventID)
            ->where('users_id',  '=',  $userId)
            ->where('status',    '!=', $notStatus)
            ->select(
                    'event_invite.events_id', 
                    'event_invite.users_id', 
                    'event_invite.professions_id', 
                    'event_invite.status',
                    'event_invite.is_manager',
                    'event_invite.id',
                    'event_invite.in_team',
                    'event_invite.manager_status',
                    'event_invite.manager_in_team',
                    'professions.title'
                    )
            ->first();
    }

    /**
     * @param $eventID
     * @param $userId
     *
     * @return mixed|static
     */
    static public function checkUserInTeam($eventID, $userId)
    {

        return \DB::table('event_invite')
            ->where('events_id', '=',  $eventID)
            ->where('users_id',  '=',  $userId)
            ->where('in_team',   '!=', EventInvite::IS_OUT_TEAM)
            ->first();
    }
    
    /**
     * @param $eventID
     * @param $userId
     *
     * @return mixed|static
     */
    static public function getUserInTeamEventsId($userId, $only_ids = false)
    {
        
        $events_ids = \DB::table('event_invite')
            ->where('users_id',  '=',  $userId)
            ->where('in_team',   '!=', EventInvite::IS_OUT_TEAM)
            ->lists('events_id');
        
        $owner_events_id = \DB::table('events')
            ->where('users_id',  '=',  $userId)
            ->lists('id');
        
        $data = array_merge($events_ids, $owner_events_id);

        if(!empty($data))
        {

            if($only_ids) {
                $events = $data;
            } else {
                $events = \DB::table('events')
                    ->whereIn('id', $data)
                    ->lists('title', 'id');
            }
        }
        else
        {
            $events = array();
        }
        
        return $events;
    }

    /**
     * @param $eventID
     * @param bool|false $status
     * @param bool|true $inTeam
     * @param bool|true $withReserve
     *
     * @return array|static[]
     */
    static public function getEventTeam($eventID, $status = false, $inTeam = true, $withReserve = true)
    {

        $query = \DB::table('professions_events_rates')
                    ->select(
                            'professions_events_rates.events_id',
                            'professions_events_rates.professions_id',
                            'professions_events_rates.count',
                            'event_invite.in_team',
                            'event_invite.manager_in_team',
                            'event_invite.is_manager',
                            'event_invite.status',
                            'event_invite.manager_status',
                            'event_invite.users_id'
                        )
                    ->leftJoin('event_invite', function(\Illuminate\Database\Query\JoinClause $join) use ($eventID)
                    {
                        $join->on('event_invite.professions_id', '=', 'professions_events_rates.professions_id')
                            ->on('event_invite.events_id', '=', 'professions_events_rates.events_id');
                    })
                    ->where('professions_events_rates.events_id', '=', $eventID);

        if ($status) {

            if (is_array($status)) {
                $query->where(function($query) use($status)  {
                    foreach ($status as $sv) {
                        $query->orWhere('event_invite.status', '=', $sv);
                    }
                });
            } else {
                $query->where('event_invite.status', '=', $status);
            }
        }

        if ($inTeam) {
            if ($withReserve) {
                $query->where(function($query)
                {
                    $query->orWhere('event_invite.in_team', '=', EventInvite::IS_IN_TEAM)
                          ->orWhere('event_invite.in_team', '=', EventInvite::IS_IN_RESERVE);
                });
            } else {
                $query->where('event_invite.in_team', '=', EventInvite::IS_IN_TEAM);
            }
        }

        return $query->get();
    }
    
        /**
     * @param $eventID
     * @param $status
     *
     * @return array|static[]
     */
    static public function getEventTeamForManagerCheck($eventID, $status)
    {

        $query = \DB::table('professions_events_rates')
                    ->select(
                            'professions_events_rates.events_id',
                            'professions_events_rates.professions_id',
                            'professions_events_rates.count',
                            'event_invite.in_team',
                            'event_invite.manager_in_team',
                            'event_invite.is_manager',
                            'event_invite.status',
                            'event_invite.manager_status',
                            'event_invite.users_id'
                        )
                    ->leftJoin('event_invite', function(\Illuminate\Database\Query\JoinClause $join) use ($eventID)
                    {
                        $join->on('event_invite.professions_id', '=', 'professions_events_rates.professions_id')
                            ->on('event_invite.events_id', '=', 'professions_events_rates.events_id');
                    })
                    ->where('professions_events_rates.events_id', '=', $eventID);


        $query->where('event_invite.status', '=', $status);


        $query->where('event_invite.in_team', '=', EventInvite::IS_IN_TEAM);

        return $query->get();
    }

    /**
     * @param Events $event
     * @param EventInvite $invite
     */
    public static function sendLettersAboutDeleteEventToMembers(Events $event, EventInvite $invite)
    {
        $teamMember   = $invite->user;
        $siteUrl      = \Config::get('app.url');

        if ($teamMember instanceof User) {

            \Queue::push('\Acme\Queues\SendAboutDeleteEventToMembersMessageQueue', [
                'url'              => $siteUrl,
                'email'            => $teamMember->email,
                'username'         => $teamMember->getUserName(),
                'teamMember'       => $teamMember->getUserName(),
                'eventTitle'       => $event->title
            ], 'emails');
        }
    }
}