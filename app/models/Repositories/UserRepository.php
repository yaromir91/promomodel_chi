<?php

namespace Acme\Models\Repositories;

use Acme\Models\Professions\UserProfessionsRates;
use Acme\Models\User;
use Acme\Models\Profile\UserProfile;
use Acme\Models\Role;
use Acme\Models\UsersAgentInvitations;
use Acme\Models\UsersGroup;

/**
 * Class UserRepository
 *
 * This service abstracts some interactions that occurs between Confide and
 * the Database.
 */
class UserRepository
{

	/**
	 * @param $code
	 * @return User|null
	 */
	public function getUserByConfirmCode($code) {
		return User::where('confirmation_code', '=', $code)->first();
	}

	/**
	 * Signup a new account with the given parameters
	 *
	 * @param  array   $input Array containing 'username', 'email' and 'password'.
	 * @param  integer $isConfilm
	 *
	 * @return  User User object that may or may not be saved successfully. Check the id to make sure.
	 */
	public function signup($input, $isConfilm = 0, $agent_id = 0)
	{
        $user = new User;

        $user->agent_id  = $agent_id;
        $user->username  = array_get($input, 'username');
        $user->email     = array_get($input, 'email');
        $user->password  = array_get($input, 'password');
        $user->confirmed = $isConfilm;

        $user->gender      = array_get($input, 'gender');
        $user->firstName   = array_get($input, 'first_name');
        $user->lastName    = array_get($input, 'last_name');
        $user->accountType = array_get($input, 'account_type');

        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        $user->password_confirmation = array_get($input, 'password_confirmation');

        // Generate a random confirmation code
        $user->confirmation_code     = md5(uniqid(mt_rand(), true));

        // Save if valid. Password field will be hashed before save
        $res = $this->save($user);

        return $user;
	}

    /**
     * @param User $user
     *
     * @return bool
     */
    public function addProfileRoleToUser(User $user)
    {
        try {
            $profile        = $user->profile;

            if (!($profile instanceof UserProfile)) {
                $profile          = new UserProfile;
                $profile->user_id = $user->id;
            }

            if ($user->lastName) {
                $profile->last_name = $user->lastName;
            }

            if ($user->firstName) {
                $profile->first_name = $user->firstName;
            }

            if ($user->gender) {
                $profile->gender = $user->gender;
            }
            
            // выставляем по умолчание профиль публичным
            $profile->public = 1;

            $resultProfile = $profile->save();

            if ($user->accountType) {
                $user->roles()->detach();
                $user->roles()->attach($user->accountType);
            }

            return $resultProfile;

        } catch (\Exception $e) {

            return false;
        }
    }

	/**
	 * Attempts to login with the given credentials.
	 *
	 * @param  array $input Array containing the credentials (email/username and password)
	 *
	 * @return  boolean Success?
	 */
	public function login($input)
	{
		if (! isset($input['password'])) {
			$input['password'] = null;
		}

		return \Confide::logAttempt($input, \Config::get('confide::signup_confirm'));
	}

	/**
	 * Checks if the credentials has been throttled by too
	 * much failed login attempts
	 *
	 * @param  array $credentials Array containing the credentials (email/username and password)
	 *
	 * @return  boolean Is throttled
	 */
	public function isThrottled($input)
	{
		return \Confide::isThrottled($input);
	}

	/**
	 * Checks if the given credentials correponds to a user that exists but
	 * is not confirmed
	 *
	 * @param  array $credentials Array containing the credentials (email/username and password)
	 *
	 * @return  boolean Exists and is not confirmed?
	 */
	public function existsButNotConfirmed($input)
	{
		$user = \Confide::getUserByEmailOrUsername($input);

		if ($user) {
			$correctPassword = \Hash::check(
				isset($input['password']) ? $input['password'] : false,
				$user->password
			);

			return (! $user->confirmed && $correctPassword);
		}
	}

	/**
	 * Resets a password of a user. The $input['token'] will tell which user.
	 *
	 * @param  array $input Array containing 'token', 'password' and 'password_confirmation' keys.
	 *
	 * @return  boolean Success
	 */
	public function resetPassword($input)
	{
		$result = false;
		$user   = \Confide::userByResetPasswordToken($input['token']);

		if ($user) {
			$user->password              = $input['password'];
			$user->password_confirmation = $input['password_confirmation'];
			$result = $this->save($user);
		}

		// If result is positive, destroy token
		if ($result) {
			\Confide::destroyForgotPasswordToken($input['token']);
		}

		return $result;
	}

	/**
	 * Simply saves the given instance
	 *
	 * @param  User $instance
	 *
	 * @return  boolean Success
	 */
	public function save(User $instance)
	{
		return $instance->save();
	}

    public function addFbAvatar($user, $filename)
    {
        return \DB::table('user_profile')
            ->where('user_id', $user->id)
            ->update(array('avatar' => $filename));

    }

    public function setFbStatus($user, $status)
    {
        return \DB::table('user_profile')
            ->where('user_id', $user->id)
            ->update(array('fb_status' => $status));
    }


    /**
     * @param $status
     * @param bool $toArray
     * @param bool $userRole
     *
     * @return array|static
     */
    static public function getUsersByStatus($status, $toArray = false, $userRole = false)
    {
        $result = [];

        $query  = \DB::table('users');
        if (is_array($status)) {
            $query->whereIn('users.confirmed', $status);
        } else {
            $query->where('users.confirmed', '=', $status);
        }

        $query->select('users.id','users.username');

        if ($userRole) {
            $query->join('assigned_roles', 'assigned_roles.user_id', '=', 'users.id');
            $query->join('roles',          'assigned_roles.role_id', '=', 'roles.id');

            if (is_array($userRole)) {
                $query->where(function($query) use($userRole)  {
                    foreach ($userRole as $sv) {
                        $query->orWhere('roles.code', '=', $sv);
                    }
                });
            } else {
                $query->where('roles.code', '=', $userRole);
            }
        }

        $users  = $query->get();

        if ($toArray) {
           if (count($users)) {
               foreach($users as $us) {
                   $result[$us->id] = $us->username;
               }

               return $result;
           }
        }

        return $users;
    }

    /**
     * @param $status
     * @param bool $toArray
     * @param bool $userRole
     *
     * @return array|static
     */
    static public function getUsersOfGroupOwners($status, $toArray = false, $userRole = false)
    {
        $result = [];

        $query  = \DB::table('users');
        if (is_array($status)) {
            $query->whereIn('users.confirmed', $status);
        } else {
            $query->where('users.confirmed', '=', $status);
        }

        $query->select('users.id','users.username');

        if ($userRole) {
            $query->join('assigned_roles', 'assigned_roles.user_id', '=', 'users.id');
            $query->join('roles',          'assigned_roles.role_id', '=', 'roles.id');



            if (is_array($userRole)) {
                $query->where(function($query) use($userRole)  {
                    foreach ($userRole as $sv) {
                        $query->orWhere('roles.code', '=', $sv);
                    }
                });
            } else {
                $query->where('roles.code', '=', $userRole);
            }
        }

        $query->leftJoin('users_group', 'users_group.user_id', '=', 'users.id');
        $query->whereRaw('prm_users_group.id IS NULL');

        $users  = $query->get();

        if ($toArray) {
            if (count($users)) {
                foreach($users as $us) {
                    $result[$us->id] = $us->username;
                }

                return $result;
            }
        }

        return $users;
    }

    public static function getPublicUsers($rand = false, $limit = 5)
    {
        if($rand){
            return \DB::table('user_profile')
                ->where('public', 1)
                ->orderByRaw('RAND()')
                ->limit($limit)
                ->get();
        }

        return \DB::table('user_profile')
            ->where('public', 1)
            ->get();

    }

    public static function getAgentUsers($id)
    {
        $result = \DB::table('users')
            ->leftJoin('user_profile', 'user_profile.user_id', '=', 'users.id')
            ->leftJoin('users_agent_invitations', 'users_agent_invitations.user_id', '=', 'users.id')
            ->where('users_agent_invitations.agent_id', $id)
            ->select(
                'users.id as user_id',
                'user_profile.last_name',
                'user_profile.first_name',
                'user_profile.phone',
                'user_profile.date_birth',
                'user_profile.gender',
                'user_profile.is_ready_manager',
                'user_profile.avatar',
                'users_agent_invitations.invite_status'
            );


        return $result->get();
    }
    
    /**
     * @return float
     */
    public function getUserRating()
    {
        return RatingsRepository::getAvgRating(\Acme\Models\Ratings::RATING_TYPE_USER, $this->id);
    }

    /**
     * @param User $user
     */
    public static function sendLettersAboutDeleteUser(User $user)
    {
        $siteUrl      = \Config::get('app.url');

        \Queue::push('\Acme\Queues\SendLetterAboutDeleteUserQueue', [
            'url'              => $siteUrl,
            'email'            => $user->email,
            'username'         => $user->getUserName()
        ], 'emails');
    }

    static public function checkReloginStatus($user, $agent)
    {
        $result = false;

        $status = UsersAgentInvitationsRepository::checkUserInvitationStatus($user, $agent);

        if (($user instanceof User) && ($agent instanceof User)) {

            $user_code = $user->getMainUserRoleCode(); // TODO: где проверка на объект?
            $agent_code = $agent->getMainUserRoleCode(); // TODO: где проверка на объект?

            if(
                $agent &&
                ($user_code == Role::APPLICANT_USER || $user_code == Role::APPLICANT_MANAGER_USER) && // пользователь которого смотрим соискатель или менеджер
                $agent_code == Role::AGENT_USER && // текущий пользователь агент
                $agent->confirmed == User::CONFIRM_STATUS_ACTIVE && // текущий пользователь подтвержденный агент
                $user->agent_id == $agent->id && // пользователь которого смотрим сотрудник агента
                $status->invite_status == UsersAgentInvitations::STATUS_ACCEPTED // пользователь которого смотрим согласился быть сотрудником
            ) {
                $result = true;
            }
        }


        return $result;
    }
}
