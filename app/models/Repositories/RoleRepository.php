<?php

namespace Acme\Models\Repositories;

use Acme\Models\Profile\UserProfile;
use Acme\Models\Role;

/**
 * Class RoleRepository
 *
 * @package Acme\Models\Repositories
 *
 *  @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class RoleRepository
{

    /**
     * @param $code
     *
     * @return bool
     */
    static public function getRoleByCode($code)
    {
        $role = \DB::table('roles')
            ->select('id')
            ->where('code', $code)
            ->first();

        if ($role) {

            return $role->id;
        }

        return false;
    }


    /**
     * Get available roles for registration
     *
     * @param array $rolesForReg
     *
     * @return array
     */
    static public function getAvailableRolesForRegistration($rolesForReg = []) {

        try {
            $rolesForReg = empty($rolesForReg) ? UserProfile::getAvailableRolesForReg() : $rolesForReg;

            if (count($rolesForReg)) {
                $roles = \DB::table('roles')
                    ->select('id', 'code')
                    ->whereIn('code', $rolesForReg)
                    ->get();

                if (count($roles)) {
                    $result = [];

                    foreach ($roles as $r) {
                        $result[$r->id] = Role::getRolesTitle($r->code);
                    }

                    return $result;
                }
            }
        } catch (\Exception $e) {

           return [];
        }

        return [];
    }

    /**
     * @param $code
     * @return \Acme\Models\User|null
     */
    public static function getUsersByRoleCode(array $code) {

        $result = \DB::table('assigned_roles')
            ->select(
                'assigned_roles.user_id',
                'user_profile.last_name',
                'user_profile.first_name',
                'user_profile.phone',
                'user_profile.date_birth',
                'user_profile.gender',
                'user_profile.is_ready_manager',
                'user_profile.avatar'
            )
            ->join('roles', 'roles.id', '=', 'assigned_roles.role_id')
            ->join('user_profile', 'user_profile.user_id', '=', 'assigned_roles.user_id')
            ->where('user_profile.public', 1)
            ->whereIn('roles.code', $code)
            ->get();

        return $result;
    }

    /**
     * @param $code
     * @return \Acme\Models\User|null
     */
    public static function getAgents() {

        $result = \DB::table('assigned_roles')
            ->select(
                'assigned_roles.user_id',
                'user_profile.last_name',
                'user_profile.first_name',
                'user_profile.phone',
                'user_profile.date_birth',
                'user_profile.gender',
                'user_profile.is_ready_manager',
                'user_profile.avatar'
            )
            ->leftJoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
            ->leftJoin('user_profile', 'user_profile.user_id', '=', 'assigned_roles.user_id')
            ->leftJoin('users', 'users.id', '=', 'assigned_roles.user_id')
            ->where('users.confirmed', 2)
            ->where('roles.code', Role::AGENT_USER)
            ->get();

        return $result;
    }
}