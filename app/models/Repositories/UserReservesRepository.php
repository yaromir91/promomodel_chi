<?php

namespace Acme\Models\Repositories;

use Acme\Models\User;


class UserReservesRepository {

    static public function getUserReserves($user_id)
    {
        $reserves = \DB::table('user_reserves')
            ->where('user_id', '=', $user_id)
            ->get();

        return $reserves;
    }
}