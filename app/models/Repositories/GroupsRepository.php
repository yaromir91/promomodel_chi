<?php

namespace Acme\Models\Repositories;

use Acme\Models\Groups;
use Acme\Models\GroupsInvites;
use Acme\Models\User;
use Acme\Helpers\PictureHelper;

/**
 * Class GroupsRepository
 * @package Acme\Models\Repositories
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class GroupsRepository
{


    /**
     * @param Groups $group
     * @param array  $users
     */
    public static function sendLettersAboutDeleteGroup(Groups $group, $users)
    {
        $siteUrl      = \Config::get('app.url');

        \Mail::send('emails.groups.delete-group', [
            'group' => $group->title,
        ], function ($message) use ($group, $users) {

            $message->from(\Config::get('mail.from.address'), \Config::get('mail.from.name'))
                ->subject(\Lang::get('groups.group_deleted'));

            foreach ($users as $user) {
                if($user->user_id !== \Auth::id() ){
                    $message = $message->to($user->email);
                }
            }

        });
    }

    /**
     * @param int $offset
     * @param bool|false $limit
     */
    public static function updateAllPreview($offset = 0, $limit = false)
    {
        $query = \DB::table('groups')
            ->select('groups.image');

        if ($limit) {
            $query->offset($offset)
                ->limit($limit);
        }

        $images = $query->get();
        $path   = public_path();

        if (count($images)) {
            foreach ($images as $img) {
                $origImg  =  $path . '/' .\Config::get('app.image_dir.groups') . $img->image;
                $smallImg =  $path . '/' .\Config::get('app.image_dir.groups_small') . $img->image;
                $previImg =  $path . '/' .\Config::get('app.image_dir.groups_preview') . $img->image;

                if (\File::exists($origImg)) {
                    if (!\File::exists($smallImg)) {
                        copy($origImg, $smallImg);
                        PictureHelper::img_resize_if_big($origImg, $smallImg, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'), $color = 0xF0F0F0, 60);
                    }

                    if (!\File::exists($previImg)) {
                        copy($origImg, $previImg);
                        $img = \Image::make($origImg);
                        $img->fit(\Config::get('app.preview_width'), \Config::get('app.preview_height'));
                        $img->save($previImg, \Config::get('app.quality_picture'));
                    }
                }
            }
        }
    }

    public static function getUserInvitation($user_id = false)
    {
        if(!$user_id) {
            return false;
        }

        $invitations = \DB::table('groups_invites')
            ->where('groups_invites.user_id', '=', $user_id)
            ->where('groups_invites.status', '=',  GroupsInvites::STATUS_NOT_ACTIVE)
            ->leftJoin('groups', 'groups.id', '=', 'groups_invites.group_id')
            ->leftJoin('users', 'users.id', '=', 'groups_invites.user_id')
            ->select('groups_invites.id', 'groups_invites.token', 'groups.title', 'groups.image', 'users.username', 'groups_invites.group_id')
            ->get();

        return $invitations;
    }

    public static function getUserInvitationResult($result = false, $group = false)
    {
        if(!$result || !$group) {
            return false;
        }
        foreach($result as $r) {
            $list = \DB::table('groups_invites')->where('user_id', '=', $r->user_id)->lists('group_id', 'user_id');
            $data[$r->user_id] = (!in_array($group->id, $list));
        }

        return $data;
    }

    public static function getUserInvitationRefuse($user_id = false, $group_id = false)
    {
        if(!$user_id || !$group_id) {
            return false;
        }

        return \DB::table('groups_invites')
            ->where('user_id', '=', $user_id)
            ->where('group_id', '!=', $group_id)
            ->get();
    }
}