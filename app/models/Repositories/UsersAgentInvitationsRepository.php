<?php

namespace Acme\Models\Repositories;

use Acme\Models\Profile\UserProfile;
use Acme\Models\User;
use Acme\Models\UsersAgentInvitations;

/**
 * Class RoleRepository
 *
 * @package Acme\Models\Repositories
 *
 *  @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class UsersAgentInvitationsRepository
{

    const SEND      = 'send';
    const REJECTED  = 'rejected';
    const ACCEPTED  = 'accepted';


    /**
     * @param $code
     *
     * @return string
     */
    static public function getStatusTitle($code)
    {
        $translates = [
            self::SEND         => \Lang::get('agents.send'),
            self::REJECTED     => \Lang::get('agents.rejected'),
            self::ACCEPTED     => \Lang::get('agents.accepted'),
        ];

        if (isset($translates[$code])) {

            return $translates[$code];
        }

        return '';
    }

    /**
     * @param $user
     * @param $agent
     *
     * @return mixed
     */
    static public function checkUserInvitationStatus($user, $agent)
    {
        // TODO: нету проверки на $user, $agent - ошибка в скрине
        $query = \DB::table('users_agent_invitations')
            ->select('invite_status');

        if ($user instanceof User) {
            $query->where('user_id', $user->id);
        }

        if ($agent instanceof User) {
            $query->where('agent_id', $agent->id);
        }

        $status = $query->first();

        return $status;
    }

    /**
     * @param $user
     * @param $agent
     */
    static public function sendInvitation($user, $agent)
    {
        $invitation = new UsersAgentInvitations();

        $invitation->user_id        = $user->id;
        $invitation->agent_id       = $agent->id;
        $invitation->invite_status  = UsersAgentInvitations::STATUS_SEND;
        $invitation->created_at     = date('Y-m-d H:i:s');

        $invitation->save();
    }

    static public function getUserInvitations($user)
    {
        $invitations = \DB::table('users_agent_invitations')
            ->leftJoin('users', 'users.id', '=', 'users_agent_invitations.agent_id')
            ->leftJoin('user_profile', 'user_profile.user_id', '=', 'users_agent_invitations.agent_id')
            ->where('users_agent_invitations.user_id', $user->id)
            ->select('users.id', 'user_profile.first_name', 'users_agent_invitations.created_at', 'users_agent_invitations.invite_status')
            ->get();

        return $invitations;
    }

    static public function setRejectStatus($user, $agent)
    {
        \DB::table('users_agent_invitations')
            ->where('user_id', $user->id)
            ->where('agent_id', $agent->id)
            ->update(array('invite_status' => UsersAgentInvitations::STATUS_REJECTED));
    }


    static public function setAcceptStatus($user, $agent)
    {
        \DB::table('users_agent_invitations')
            ->where('user_id', $user->id)
            ->update(array('invite_status' => UsersAgentInvitations::STATUS_REJECTED));

        \DB::table('users')
            ->where('id', $user->id)
            ->update(array('agent_id' => $agent->id));

        \DB::table('users_agent_invitations')
            ->where('user_id', $user->id)
            ->where('agent_id', $agent->id)
            ->update(array('invite_status' => UsersAgentInvitations::STATUS_ACCEPTED));
    }

    static public function checkAgentForUser($user)
    {
        $query = \DB::table('users')
            ->select('agent_id');

        if ($user instanceof User) {
            $query->where('user_id', $user->id);
        }

        $status = $query->first();

        return $status;
    }
}