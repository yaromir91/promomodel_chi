<?php

namespace Acme\Models\Repositories;


use Acme\Models\Events\Events;
use Acme\Models\Events\EventInvite;

/**
 * Class ProfessionsRepository
 *
 * @package Acme\Models\Repositories
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ProfessionsRepository
{

    /**
     * @return mixed
     */
    public static function getAllProfessions() {

        return \DB::table('professions')
            ->orderBy('title', 'desc')
            ->get();
    }


    /**
     * @param $eventID
     * @param bool $isArray
     *
     * @return array|static
     */
    public static function getEventsRates($eventID, $isArray = false)
    {
        $eventProfeRates = \DB::table('professions_events_rates')
            ->select(array('professions_events_rates.*'))
            ->where('events_id', '=', $eventID)
            ->get();

        if ($isArray) {

            $result = [];

            foreach($eventProfeRates as $rates)
            {
                $result[$rates->professions_id] = $rates;
            }

            return $result;
        }

        return $eventProfeRates;
    }

    /**
     * @param Events $event
     */
    public static function getProfessionsRatesWithPlaces(Events $event)
    {
        $eventInviteTale    = \DB::getTablePrefix().EventInvite::getTableName();
        $eventResInviteTale = \DB::getTablePrefix().'event_invite_res';

        return \DB::table('professions_events_rates')
            ->leftJoin('event_invite', function(\Illuminate\Database\Query\JoinClause $join)
            {
                $join->on('event_invite.professions_id', '=', 'professions_events_rates.professions_id')
                    ->on('event_invite.events_id', '=', 'professions_events_rates.events_id');
                $join->where('event_invite.status', '=', EventInvite::INVITE_PROF_STATUS_ACCEPT);
                $join->where('event_invite.in_team', '=', EventInvite::IS_IN_TEAM);
            })
            ->leftJoin('event_invite as '.\DB::getTablePrefix().'event_invite_res', function(\Illuminate\Database\Query\JoinClause $join)
            {
                $join->on('event_invite_res.professions_id', '=', 'professions_events_rates.professions_id')
                    ->on('event_invite_res.events_id', '=', 'professions_events_rates.events_id');
                $join->where('event_invite_res.status', '=', EventInvite::INVITE_PROF_STATUS_ACCEPT);
                $join->where('event_invite_res.in_team', '=', EventInvite::IS_IN_RESERVE);
            })
            ->leftJoin('professions', 'professions_events_rates.professions_id', '=', 'professions.id')
            ->select(
                'professions_events_rates.events_id',
                'professions_events_rates.professions_id',
                'professions_events_rates.count',
                'professions_events_rates.rate_h',
                'professions_events_rates.rate_d',
                \DB::raw('count(distinct '.$eventInviteTale.'.id) as accepted_count'),
                \DB::raw('count(distinct '.$eventResInviteTale.'.id) as reserved_count'),
                'professions.title'
            )
            ->where('professions_events_rates.events_id', '=', $event->id)
            ->groupBy('professions_events_rates.professions_id')
            ->get()
        ;
    }
}