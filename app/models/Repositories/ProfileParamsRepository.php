<?php

namespace Acme\Models\Repositories;

use Acme\Models\Profile\ProfileParams;

/**
 * Class ProfileParamsRepository
 *
 * @package Acme\Models\Repositories
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ProfileParamsRepository
{

    /**
     * Get main parameters
     * @return mixed
     */
    public static function getMainParams() {

        return \DB::table('profile_params')
            ->orderBy('num', 'asc')
            ->whereNull('parent_id')
            ->get();
    }

    public static function getParrentParamsForSearch() {

        $parrents = \DB::table('profile_params')
            ->orderBy('num', 'asc')
            ->whereNotNull('parent_id')
            ->get();

        foreach($parrents as $parrent) {
            $result[$parrent->parent_id][] = $parrent;
        }

        return $result;
    }

    public static function getParrentParams($id) {

        $locale = \App::getLocale();

        switch($locale) {
            case 'en' :
                $field = 'name_en';
                break;
            case 'ru' :
                $field = 'name';
                break;
        }
        $result = \DB::table('profile_params')
            ->orderBy('num', 'asc')
            ->where('parent_id', '=', $id)
            ->lists($field, 'id');

        return $result;
    }
}