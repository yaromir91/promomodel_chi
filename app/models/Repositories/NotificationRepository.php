<?php

namespace Acme\Models\Repositories;

use Acme\Models\Events\Events;
use Acme\Models\GroupsInvites;
use Acme\Models\Notifications;
use Acme\Models\User;

/**
 * Class NotificationRepository
 * @package Acme\Models\Repositories
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class NotificationRepository
{

    /**
     * @param $user // id того, кто отправил уведомление
     * @param $owner // id того, кому адресовано уведомление
     * @param $body // текст
     * @param $type //enum тип 'event_invite','group_invite','message','event','group','report'
     */
    public static function addNewNotification($user_id, $owner_id, $body = '', $type)
    {
        if($user_id && $owner_id) {
            $user_obj = new User();

            $user = $user_obj->getUserById($user_id);
            $owner = $user_obj->getUserById($owner_id);

            if(is_object($user) && is_object($owner)) {
                $notification = new Notifications();

                $notification->user_id          = $user->id; // кто прислал
                $notification->owner_user_id    = $owner->id; // кому прислал
                $notification->body             = $body;
                $notification->is_read          = Notifications::IS_READ_OFF;
                $notification->type             = $type;

                $notification->save();

                if($user->agent_id && !$owner->hasRole('Agent')) {
                    $notification = new Notifications();

                    $notification->user_id          = $user->id; // кто прислал
                    $notification->owner_user_id    = $user->agent_id; // кому прислал
                    $notification->body             = $body;
                    $notification->is_read          = Notifications::IS_READ_OFF;
                    $notification->type             = $type;

                    $notification->save();
                }
            }
        }
    }

    public static function getUserNotifications($user_id)
    {
        $result = '';
        if($user_id) {
            $result = \DB::table('notifications')
                ->leftJoin('users', 'users.id', '=', 'notifications.user_id')
                ->where('notifications.owner_user_id', '=', $user_id)
                ->select(
                    'notifications.updated_at',
                    'notifications.type',
                    'notifications.body',
                    'notifications.is_read',
                    'users.username',
                    'notifications.user_id',
                    'notifications.id'
                )
                ->orderBy('notifications.updated_at', 'desc')
                ->get();
        }
        

        return $result;
    }

    public static function getTypeTitle($type)
    {
        $result = array(
            Notifications::TYPE_EVENT_INVITE    => \Lang::get('notifications.msg_17'),
            Notifications::TYPE_GROUP_INVITE    => \Lang::get('notifications.msg_18'),
            Notifications::TYPE_MESSAGE         => \Lang::get('notifications.msg_19'),
            Notifications::TYPE_EVENT           => \Lang::get('notifications.msg_20'),
            Notifications::TYPE_GROUP           => \Lang::get('notifications.msg_21'),
            Notifications::TYPE_REPORT          => \Lang::get('notifications.msg_22')
        );

        if(isset($result[$type])) {
            return $result[$type];
        } else {
            return false;
        }
    }

    public static function getTypeHref($type, $user_id)
    {
        $href = '#';
        if($type && $user_id) {
            switch($type){
                case Notifications::TYPE_EVENT_INVITE:
                    $event = Events::where('users_id', '=', $user_id)->first();
                    if($event) {
                        $href = \URL::to('events/show/' . $event->id);
                    }
                    break;
                case Notifications::TYPE_GROUP_INVITE:
                    $group = GroupsInvites::where('user_id', '=', $user_id)->first();
                    if($group) {
                        $href = \URL::to('groups/show/' . $group->group_id);
                    }
                    break;
                case Notifications::TYPE_MESSAGE:
                    break;
                case Notifications::TYPE_EVENT:
                    $event = Events::where('users_id', '=', $user_id)->first();
                    if($event) {
                        $href = \URL::to('events/show/' . $event->id);
                    }
                    break;
                case Notifications::TYPE_GROUP:
                    $group = GroupsInvites::where('user_id', '=', $user_id)->first();
                    if($group) {
                        $href = \URL::to('groups/show/' . $group->group_id);
                    }
                    break;
                case Notifications::TYPE_REPORT:
                    break;
            }
        }

        return $href;
    }


}