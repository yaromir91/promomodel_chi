<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 17.07.15
 * Time: 11:16
 */

 namespace Acme\Models\Repositories;

 /**
  * Class TagsRepository
  * @package Acme\Models\Repositories
  * @author Sergey Bespalov
  */
 class TagsRepository
 {
     /**
      * @return mixed
      */
     public static function getTags() {

         $tags = \DB::table('tags')
             ->select('tags.id', 'tags.name')
             ->get();

         return $tags;
     }

     /**
      * @param $word
      * @param bool $isArray
      *
      * @return array|static
      */
     public static function searchTags($word, $isArray = true)
     {
         $tags = \DB::table('tags')
             ->select('tags.name')
             ->where('tags.name', 'like', '%'.$word.'%')
             ->get();

         if ($isArray) {
             $result = [];

             if (count($tags)) {
                 foreach ($tags as $tg) {
                     $result[] = $tg->name;
                 }
             }

             return $result;
         }

         return $tags;
     }

     /**
      * Return list of of tags name/id
      * @param $id
      *
      * @return mixed
      */
     public static function getCurrentGalleriesTags($id) {

         $tags = \DB::table('galleries_tags')
             ->distinct()
             ->join('tags', 'tags.id', '=', 'galleries_tags.tag_id')
             ->where('galleries_tags.gallery_id', '=', $id)
             ->lists('name');

         return $tags;
     }

     public static function clearGalleriesTags($id) {

         $tags = \DB::table('galleries_tags')
             ->where('galleries_tags.gallery_id', '=', $id)
             ->delete();

         return $tags;
     }

     /**
      * Return list of of tags name/id
      * @param $id
      *
      * @return mixed
      */
     public static function getCurrentPostTags($id) {

         $tags = \DB::table('posts_tags')
             ->distinct()
             ->join('tags', 'tags.id', '=', 'posts_tags.tag_id')
             ->where('posts_tags.post_id', '=', $id)
             ->lists('name');

         return $tags;
     }

     public static function clearPostTags($id) {

         $tags = \DB::table('posts_tags')
             ->where('posts_tags.post_id', '=', $id)
             ->delete();

         return $tags;
     }
 }
