<?php

namespace Acme\Models\Repositories;

use Acme\Models\Ratings;


/**
 *
 * Class RatingsRepository
 *
 * @package Acme\Models\Repositories
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class RatingsRepository {

    public static function getGalleryRating($gallery_id)
    {
        $table = with(new Ratings)->getTable();
        $result = [];

        $ratings = \DB::table('gallery_images')
            ->join('ratings', 'ratings.object_id', '=', 'gallery_images.id')
            ->where('gallery_images.gallery_id', '=', $gallery_id)
            ->where('ratings.type', '=', \Acme\Models\Ratings::RATING_TYPE_PHOTO)
            ->groupBy( 'gallery_images.id' )
            ->select('gallery_images.id', \DB::raw( 'AVG( ' . \DB::getTablePrefix() . $table . '.rating ) AS rating' ))
            ->get();

        foreach($ratings as $rating) {
            $result[$rating->id] = round($rating->rating, 1);
        }

        return $result;
    }

    public static function getReportRating($report_id)
    {
        $table = with(new Ratings)->getTable();
        $result = [];

        $ratings = \DB::table('posts')
            ->join('ratings', 'ratings.object_id', '=', 'posts.id')
            ->where('posts.id', '=', $report_id)
            ->where('ratings.type', '=', \Acme\Models\Ratings::RATING_TYPE_POST)
            ->groupBy( 'posts.id' )
            ->select('posts.id', \DB::raw( 'AVG( ' . \DB::getTablePrefix() . $table . '.rating ) AS rating' ))
            ->get();

        foreach($ratings as $rating) {
            $result[$rating->id] = round($rating->rating, 1);
        }

        return $result;
    }

    public static function getUserRating($user_id)
    {
        $table = with(new Ratings)->getTable();
        $result = [];

        $ratings = \DB::table('users')
            ->join('ratings', 'ratings.object_id', '=', 'users.id')
            ->where('users.id', '=', $user_id)
            ->where('ratings.type', '=', \Acme\Models\Ratings::RATING_TYPE_USER)
            ->groupBy( 'users.id' )
            ->select('users.id', \DB::raw( 'AVG( ' . \DB::getTablePrefix() . $table . '.rating ) AS rating' ))
            ->get();

        foreach($ratings as $rating) {
            $result[$rating->id] = round($rating->rating, 1);
        }

        return $result;
    }

    /**
     * @param $type
     * @param $objectId
     *
     * @return float
     */
    public static function getAvgRating($type, $objectId)
    {
        return round(Ratings::whereRaw('object_id = ? and type = "'.$type.'"', array($objectId))->avg('rating'), 1);
    }

    /**
     * @param $type
     * @param $userId
     * @param $objectId
     * @param $rating
     */
    public static function updateOrCreateRatingObject($type, $userId, $objectId, $rating)
    {
        $rate = Ratings::whereRaw('user_id = ? and object_id = ? and type = "'.$type.'"', array($userId, $objectId))->first();

        if (!($rate instanceof Ratings)) {
            $rate            = new Ratings();
            $rate->user_id   = $userId;
            $rate->object_id = $objectId;
            $rate->type      = $type;
        }

        $rate->rating    = $rating;
        $rate->save();
    }

}