<?php

namespace Acme\Models\Repositories;

use Acme\Models\Payments\UserApplicantsPayments;

/**
 * Class PaymentsRepository
 * @package Acme\Models\Repositories
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class PaymentsRepository
{

    /**
     * @param $eventId
     * @param $userId
     * @param $professionId
     * @param int $status
     *
     * @return bool
     *
     * @throws \Exception
     */
    public static function deletePayments($eventId, $userId, $professionId, $status = UserApplicantsPayments::STATUS_NOT_PAID)
    {
        $payment = self::checkExistPayments($eventId, $userId, $professionId, $status);

        if (!empty($payment)) {
            \DB::table('user_applicants_payments')->where('user_applicants_payments.id', '=', $payment->id)->delete();

            return true;
        }

        return false;
    }

    /**
     * @param $eventId
     * @param $userId
     * @param $professionId
     * @param $price
     * @param int $status
     *
     * @return bool
     */
    public static function addNewPayments($eventId, $userId, $professionId, $price, $status = UserApplicantsPayments::STATUS_NOT_PAID)
    {
        $payment = self::checkExistPayments($eventId, $userId, $professionId, $status);

        if (empty($payment)) {
            $payment = new UserApplicantsPayments();
            $payment->event_id      = $eventId;
            $payment->user_id       = $userId;
            $payment->status        = $status;
            $payment->price         = $price;
            $payment->profession_id = $professionId;

            return $payment->save();
        }

        return false;
    }

    /**
     * @param $eventId
     * @param $userId
     * @param $professionId
     * @param int $status
     *
     * @return mixed|static
     */
    public static function checkExistPayments($eventId, $userId, $professionId, $status = UserApplicantsPayments::STATUS_NOT_PAID)
    {
         return \DB::table('user_applicants_payments')
            ->where('user_applicants_payments.event_id',      '=', $eventId)
            ->where('user_applicants_payments.user_id',       '=', $userId)
            ->where('user_applicants_payments.profession_id', '=', $professionId)
            ->where('user_applicants_payments.status',        '=', $status)
            ->first();
    }

    public static function getEventPaymentStatus($eventId)
    {
        return \DB::table('user_applicants_payments')
            ->where('event_id', '=', $eventId)
            ->where('status', '=', UserApplicantsPayments::STATUS_NOT_PAID)
            ->count();
    }
}