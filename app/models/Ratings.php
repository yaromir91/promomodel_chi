<?php

namespace Acme\Models;

/**
 *
 * Class Ratings
 *
 * @package Acme\Models
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class Ratings extends \Eloquent {

    const RATING_TYPE_EVENT = 'event';
    const RATING_TYPE_USER  = 'user';
    const RATING_TYPE_PHOTO = 'photo';
    const RATING_TYPE_POST  = 'post';
    const RATING_TYPE_TEAM  = 'team';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var string
     */
    protected $table = 'ratings';

    /**
     * @var bool
     */
    public $timestamps = false;


}