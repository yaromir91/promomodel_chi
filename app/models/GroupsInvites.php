<?php

namespace Acme\Models;

/**
 * Class GroupsInvites
 * @package Acme\Models
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $group_id
 * @property string $token string
 * @property string $status ['active','not_active']
 */
class GroupsInvites extends \Eloquent
{

    const STATUS_ACTIVE = 'active';
    const STATUS_NOT_ACTIVE = 'not_active';
    /**
     * @var string
     */
    protected $table = 'groups_invites';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * TODO on the future
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('Acme\Models\User', 'user_id', 'user_id');
    }

    /**
     * TODO on the future
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function group()
    {
        return $this->hasOne('Acme\Models\Groups', 'id', 'group_id');
    }

}