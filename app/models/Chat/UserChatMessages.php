<?php

namespace Acme\Models\Chat;

/**
 * Class UserChatMessages
 * @package Acme\Models\Chat
 *
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $userSender
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $chat
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class UserChatMessages extends \Eloquent
{

    const DELETED_MSG_ON  = 1;
    const DELETED_MSG_OFF = 0;

    const READ_ON  = 1;
    const READ_OFF = 0;

    protected $table = 'user_chat_messages';

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userSender()
    {
        return $this->belongsTo('Acme\Models\User', 'user_sender_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chat()
    {
        return $this->belongsTo('Acme\Models\Chat\UserChats', 'chat_code', 'code');
    }
}