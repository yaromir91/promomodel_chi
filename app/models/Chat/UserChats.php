<?php

namespace Acme\Models\Chat;


use Acme\Models\User;
use Acme\Models\Chat\UserChatMessages;

/**
 * Class UserChats
 * @package Acme\Models\Chat
 *
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $messages
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $userSender
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $userReceiver
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class UserChats extends \Eloquent
{

    const CHAT_CODE_LENGTH  = 8;
    const MESSAGES_PER_PAGE = 10;

    protected $table = 'user_chats';

    protected $primaryKey = 'code';

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userSender()
    {
        return $this->belongsTo('Acme\Models\User', 'user_sender_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userReceiver()
    {
        return $this->belongsTo('Acme\Models\User', 'user_receiver_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('Acme\Models\Chat\UserChatMessages', 'chat_code', 'code');
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    public function isAllowToTheChat($userId)
    {
        return (
            $this->user_sender_id == $userId || $this->user_receiver_id == $userId
        );
    }

    /**
     * @param $userId
     * @param bool|false $isObj
     * @return bool|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getReceiverUserId($userId, $isObj = false)
    {
        if ($isObj) {
            return (($this->user_sender_id == $userId) ? $this->userReceiver : (($this->user_receiver_id == $userId) ? $this->userSender : false));
        } else {
            return (($this->user_sender_id == $userId) ? $this->user_receiver_id : (($this->user_receiver_id == $userId) ? $this->user_sender_id : false));
        }
    }

    /**
     * @param $userId
     *
     * @return string
     */
    public function getUserNames($userId)
    {
        $user1  = $this->userSender;
        $user2  = $this->userReceiver;
        $result = ['name1' => '', 'name2' => ''];

        if (($user1 instanceof User) && ($user2 instanceof User)) {
            if ($this->user_sender_id == $userId) {

                $result['name1'] = $user1->getUserName();
                $result['name2'] = $user2->getUserName();
            } elseif ($this->user_receiver_id == $userId) {

                $result['name1'] = $user2->getUserName();
                $result['name2'] = $user1->getUserName();
            }
        }

        return $result;
    }


    /**
     * @param $userId
     * @param $message
     * @param null $msg
     * @return bool
     */
    public function postNewMessage($userId, $message, &$msg = null)
    {
        try {
            $msg                 = new UserChatMessages();
            $msg->chat_code      = $this->code;
            $msg->user_sender_id = $userId;
            $msg->body           = $message;
            $msg->save();

            return true;
        } catch (\Exception $e) {

            return false;
        }
    }

    /**
     * @return int
     */
    public function getCountOfMessages()
    {
        return UserChatMessages::where('user_chat_messages.chat_code', '=', $this->code)
            ->where('user_chat_messages.is_deleted', '=', UserChatMessages::DELETED_MSG_OFF)->count();
    }

    /**
     * @param int $page
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCurrentMessages($page = 0)
    {

        $offset = $page*self::MESSAGES_PER_PAGE;
        $limit  = self::MESSAGES_PER_PAGE;

        $query = UserChatMessages::where('user_chat_messages.chat_code', '=', $this->code)
            ->where('user_chat_messages.is_deleted', '=', UserChatMessages::DELETED_MSG_OFF)
            ->orderBy('user_chat_messages.created_at', 'desc')
            ->offset($offset)
            ->limit($limit)
        ;

        return $query->get();
    }
}