<?php

namespace Acme\Models;

/**
 * Class Notifications
 * @package Acme\Models
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class Notifications extends \Eloquent
{

    const IS_READ_OFF = 0;
    const IS_READ_ON  = 1;

    const TYPE_EVENT_INVITE     = 'event_invite';
    const TYPE_GROUP_INVITE     = 'group_invite';
    const TYPE_MESSAGE          = 'message';
    const TYPE_EVENT            = 'event';
    const TYPE_GROUP            = 'group';
    const TYPE_REPORT           = 'report';

    //
    // TODO Remove after testing
    //
    const EVENT_NOT_ACCEPT          = 'Соискатель отказался от приглашения на участие в мероприятии';
    const EVENT_ACCEPT              = 'Соискатель принял приглашение на участие в мероприятии';
    const EVENT_NOT_ACCEPT_MANAGER  = 'Соискатель отказался от приглашения быть менеджером';
    const EVENT_ACCEPT_MANAGER      = 'Соискатель принял приглашение быть менеджером';
    const EVENT_RESERVE_ACCEPT      = 'Соискатель принял приглашение на участие в мероприятии запасным участником';
    const EVENT_RESERVE_ACCEPT_MANAGER = 'Соискатель принял приглашение быть запасными менеджером';

    const APPLICANT_BECAME_MEMBER               = 'Получено уведомление на участие в';
    const APPLICANT_BECAME_MANAGER              = 'Получено уведомление на участие в';

    const APPLICANT_GROUP_REFUSE                = 'Соискатель отказался от приглашения вступить в';
    const APPLICANT_GROUP_ACCEPT                = 'Соискатель принял приглашение вступить в';
    const APPLICANT_GROUP_LEAVE                 = 'Соискатель покинул группу';
    const CREATOR_GROUP_LEAVE                   = 'Создатель покинул группу и группа удалена';

    const GROUP_DELETE                 = 'Группа была удалена';
    const EVENT_EDIT                   = 'было отредактировано';
    const EVENT_DELETE                 = 'Мероприятие было удалено';

}