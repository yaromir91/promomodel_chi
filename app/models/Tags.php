<?php

namespace Acme\Models;

class Tags extends \Eloquent {
	protected $fillable = [];

    public $timestamps = false;

    public function scopeName($query, $name)
    {
        return $query->where('name', '=', $name);
    }

    public function gallery()
    {
        return $this->belongsToMany('Acme\Models\Gallery', 'galleries_tags', 'tag_id', 'gallery_id');
    }
}