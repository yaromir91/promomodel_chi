<?php

namespace Acme\Models;

class GalleryImages extends \Eloquent {

	protected $fillable = [];
    protected $table    = 'gallery_images';
    public $timestamps  = false;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo('Acme\Models\Gallery', 'gallery_id', 'id');
    }
}