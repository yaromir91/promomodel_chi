<?php

namespace Acme\Models;


/**
 *
 * Class UsersAgentInvitations
 *
 * @package Acme\Models
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class UsersAgentInvitations extends \Eloquent {

    /**
     * @var array
     */
    protected $fillable = [];
    /**
     * @var string
     */
    protected $table = 'users_agent_invitations';

    /**
     * @var bool
     */
    public $timestamps = false;

    const STATUS_SEND       = 'send';
    const STATUS_REJECTED   = 'rejected';
    const STATUS_ACCEPTED   = 'accepted';
}