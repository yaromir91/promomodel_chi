<?php

namespace Acme\Models\Events;

/**
 * Class EventsType
 *
 * @package Acme\Models\Events
 */
class EventsStatusOwner extends \Eloquent {

    /**
     * @var string
     */
    protected $table = 'events_status_owner';


    public static function getTableName()
    {
        return with(new static)->table;
    }
}