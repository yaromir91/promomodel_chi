<?php

namespace Acme\Models\Events;

/**
 * Class EventInvite
 * @package Acme\Models\Events
 *
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $event
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $user
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $professions
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class EventInvite extends \Eloquent
{


    // status
    const INVITE_PROF_STATUS_NEW       = 1;
    const INVITE_PROF_STATUS_ACCEPT    = 2;
    const INVITE_PROF_STATUS_REJECT    = 3;

    // manager_status
    const INVITE_MANAGER_STATUS_NEW       = 1;
    const INVITE_MANAGER_STATUS_ACCEPT    = 2;
    const INVITE_MANAGER_STATUS_REJECT    = 3;

    // in_team
    const IS_IN_RESERVE = 2;
    const IS_IN_TEAM    = 1;
    const IS_OUT_TEAM   = 0;

    // manager_in_team
    const IS_MANAGER_IN_RESERVE = 2;
    const IS_MANAGER_IN_TEAM    = 1;
    const IS_MANAGER_OUT_TEAM   = 0;

    // is_manager
    const MANAGER_ON  = 1;
    const MANAGER_OFF = 0;

    protected $table = 'event_invite';

    public $timestamps = false;


    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('Acme\Models\Events\Events', 'events_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Acme\Models\User', 'users_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function professions()
    {
        return $this->belongsTo('Acme\Models\Professions\Professions', 'professions_id', 'id');
    }

    /**
     * @return array
     */
    protected static function getAllStatusTitles()
    {
        return [
            self::INVITE_PROF_STATUS_NEW       => \Lang::get('events.invitation_sent'),
            self::INVITE_PROF_STATUS_ACCEPT    => \Lang::get('events.invitation_prof_accepted'),
            self::INVITE_PROF_STATUS_REJECT    => \Lang::get('events.invitation_rejected'),
        ];
    }

    /**
     * @return array
     */
    protected static function getAllManagerStatusTitles()
    {
        return [
            self::INVITE_MANAGER_STATUS_NEW    => \Lang::get('events.invitation_sent'),
            self::INVITE_MANAGER_STATUS_ACCEPT => \Lang::get('events.invitation_manager_accepted'),
            self::INVITE_MANAGER_STATUS_REJECT => \Lang::get('events.invitation_manager_rejected'),
        ];
    }

    /**
     * @return array
     */
    protected static function getAllTeamStatusTitles()
    {
        return [
            self::IS_IN_TEAM    => \Lang::get('events.in_team'),
            self::IS_IN_RESERVE => \Lang::get('events.team_reserve'),
            self::IS_OUT_TEAM   => \Lang::get('events.out_team')
        ];
    }

    /**
 * @return array
 */
    protected static function getManagerTeamStatusTitles()
    {
        return [
            self::IS_MANAGER_IN_TEAM    => \Lang::get('events.in_team'),
            self::IS_MANAGER_IN_RESERVE => \Lang::get('events.team_reserve'),
            self::IS_MANAGER_OUT_TEAM   => \Lang::get('events.out_team')
        ];
    }

    /**
     * @return string
     */
    public static function getCurrentManagerTeamStatusTitle($inTeam)
    {
        $teamStatusTitles = self::getManagerTeamStatusTitles();

        if (isset($teamStatusTitles[$inTeam])) {

            return $teamStatusTitles[$inTeam];
        }

        return '';
    }

    /**
     * @return string
     */
    public static function getCurrentTeamStatusTitle($inTeam)
    {
        $teamStatusTitles = self::getAllTeamStatusTitles();

        if (isset($teamStatusTitles[$inTeam])) {

            return $teamStatusTitles[$inTeam];
        }

        return '';
    }

    /**
     * @return string
     */
    public static function getCurrentManagerStatusTitle($managerStatus)
    {
        $statusTitles = self::getAllManagerStatusTitles();

        if (isset($statusTitles[$managerStatus])) {

            return $statusTitles[$managerStatus];
        }

        return '';
    }

    /**
     * @return string
     */
    public static function getCurrentStatusTitle($status)
    {
         $statusTitles = self::getAllStatusTitles();

        if (isset($statusTitles[$status])) {

            return $statusTitles[$status];
        }

        return '';
    }

    /**
     * @param bool|false $isAccept
     * @param bool|false $isManager
     */
    public function setStatusWithData($isAccept, $isManager = false)
    {
        if ($isAccept) {
            if ($isManager) {
                $this->status         = self::INVITE_PROF_STATUS_ACCEPT;
                $this->manager_status = self::INVITE_MANAGER_STATUS_ACCEPT;
            } else {
                $this->status = self::INVITE_PROF_STATUS_ACCEPT;
            }
        } else {
            if ($isManager) {
                $this->manager_status = self::INVITE_MANAGER_STATUS_REJECT;
            } else {
                $this->status         = self::INVITE_PROF_STATUS_REJECT;
                $this->manager_status = self::INVITE_MANAGER_STATUS_REJECT;
            }
        }
    }

    /**
     * @return mixed|string
     */
    public function getProfessionName()
    {
        $profession = $this->professions;

        if ($profession instanceof \Acme\Models\Professions\Professions) {

            return $profession->title;
        }

        return '';
    }

}