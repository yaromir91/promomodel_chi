<?php

namespace Acme\Models\Events;

/**
 * Class EventsType
 *
 * @package Acme\Models\Events
 */
class EventsType extends \Eloquent {

    /**
     * @var string
     */
    protected $table = 'events_type';

    public $timestamps = false;

    /**
     * @return mixed
     */
    public function title(){
        return $this->title;
    }
}