<?php

namespace Acme\Models\Events;

use Acme\Models\Professions\ProfessionsEventsRates;
use Acme\Models\User;
use Acme\Models\Repositories\EventInviteRepository;
use Acme\Models\Repositories\RatingsRepository;
use Acme\Models\Events\EventInvite;
use Acme\Models\Notifications;
use Acme\Models\Repositories\NotificationRepository;
use Acme\Models\Repositories\PaymentsRepository;

/**
 * Class Events
 *
 * @property $professionsRates
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $invites
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $payments
 *
 * @package Acme\Models\Events
 */
class Events extends \Eloquent {


    private $ratioTeamProf = [];

    private $completeTeam  = [];

    const PRESENCE_OWNER_TRUE = 1;
    const PRESENCE_OWNER_FALSE = 0;

    const PAIMENT_STATUS_NEW    = 'new';
    const PAIMENT_STATUS_HOLD   = 'hold';
    const PAIMENT_HOLD_ACCEPT   = 'hold_accept';
    const PAIMENT_HOLD_CANCEL   = 'hold_cancel';
    const PAIMENT_STATUS_PAID   = 'paid';
    const PAIMENT_STATUS_CANCEL = 'cancel';

    const REPEAT_STATUS_ON  = 1;
    const REPEAT_STATUS_OFF = 0;

    const REPEATED_DAILY   = 'daily';
    const REPEATED_WEEKLY  = 'weekly';
    const REPEATED_MONTHLY = 'monthly';
    const REPEATED_YEARLY  = 'yearly';
    
    const STATUS_YA_SUCCESS = 'success';
    const STATUS_YA_FAILED  = 'failed';
    
    const REJECT_SEARCH_LIMIT = 3;

    /**
     * Get the event's author.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('Acme\Models\User', 'users_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function eventStatusOwner()
    {
        return $this->hasOne('Acme\Models\Events\EventsStatusOwner', 'id', 'event_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany('Acme\Models\Payments\UserApplicantsPayments', 'event_id', 'id');
    }

    /**
     * Get the event's type.
     *
     * @return User
     */
    public function eventType()
    {
        return $this->belongsTo('Acme\Models\Events\EventsType', 'type_id', 'id');
    }

    /**
     * @return \Acme\Models\Professions\ProfessionsEventsRates object
     */
    public function professionsRates()
    {
        return $this->belongsToMany('Acme\Models\Professions\Professions', 'professions_events_rates', 'events_id', 'professions_id')
            ->withPivot('count', 'rate_h', 'rate_d');
    }


    /**
     * Get the post's comments.
     *
     * @return array
     */
    public function comments()
    {
        return $this->hasMany('Acme\Models\EventComments');
    }


    /**
     * boot
     */
    public static function boot()
    {
        parent::boot();

        static::created(function($event)
        {
            //$event->getTotalPrice(true, true);
        });

        static::updated(function($event)
        {
            /*if ($event->paiment_status != 1) {
                $event->getTotalPrice(true, true);
            }*/
        });
    }

    /**
     * @param bool|false $isRefreshPrice
     * @param bool|false $isUpdate
     *
     * @return int|mixed
     */
    public function getTotalPrice($isRefreshPrice = false, $isUpdate = false)
    {
        if ($isRefreshPrice) {
            $totalTime     = $this->getTotalEventTime();

            $repeatedCount = 0;
            $totalPrice    = 0;
            $profs         = $this->professionsRates;

            if (count($profs)) {
                foreach ($profs as $ps) {
                    $totalPrice += (
                        $ps->pivot->count * $ps->pivot->rate_d * $totalTime['days'] +
                        $ps->pivot->count * $ps->pivot->rate_h * $totalTime['hours']
                    );
                }
            }



            if ($this->repeat_status == self::REPEAT_STATUS_ON) {
                $repeatedCount += $this->getRepeatedCount();
                $totalPrice = $totalPrice*$repeatedCount;
            }

            if ($isUpdate) {
                $this->total_price = $totalPrice;
                $this->save();
            }

            return $totalPrice;
        } else {

            return $this->total_price;
        }
    }

    /**
     * @return int|mixed
     */
    public function getTotalPriceWithServicePercent()
    {
        $percent    = \Config::get('app.payment_percent_service');
        $totalPrice = $this->getTotalPrice(true);

        return ($totalPrice + ceil($totalPrice*($percent/100)));
    }


    /**
     * @param $professionId
     * @return int
     */
    public function getTotalPriceOneProfession($professionId)
    {
        $repeatedCount = 0;
        $totalPrice    = 0;
        $profRate      = $this->getOneProfessionRate($professionId);
        $totalTime     = $this->getTotalEventTime();

        if ($profRate instanceof \Acme\Models\Professions\Professions) {
            $totalPrice += (
                $profRate->pivot->count * $profRate->pivot->rate_d * $totalTime['days'] +
                $profRate->pivot->count * $profRate->pivot->rate_h * $totalTime['hours']
            );

            if ($this->repeat_status == self::REPEAT_STATUS_ON) {
                $repeatedCount += $this->getRepeatedCount();
                $totalPrice = $totalPrice*$repeatedCount;
            }
        }

        return $totalPrice;
    }

    /**
     * @param $professionId
     * @return int
     */
    public function getPriceOneProfession($professionId)
    {
        $repeatedCount = 0;
        $totalPrice    = 0;
        $profRate      = $this->getOneProfessionRate($professionId);
        $totalTime     = $this->getTotalEventTime();

        if ($profRate instanceof \Acme\Models\Professions\Professions) {
            $totalPrice += (
                $profRate->pivot->rate_d * $totalTime['days'] +
                $profRate->pivot->rate_h * $totalTime['hours']
            );

            if ($this->repeat_status == self::REPEAT_STATUS_ON) {
                $repeatedCount += $this->getRepeatedCount();
                $totalPrice = $totalPrice*$repeatedCount;
            }
        }

        return $totalPrice;
    }

    /**
     * @return float|int
     */
    public function getRepeatedCount()
    {
        $repeatedCount = 0;

        if (!empty($this->date_start) && !empty($this->repeat_end)) {
            $dateStart = new \DateTime($this->date_start);
            $repeatEnd = new \DateTime($this->repeat_end);

            $interval  = $dateStart->diff($repeatEnd);
            $days      = $interval->days;

            if ($days > 0 && ($interval->invert != 1)) {

                if ($this->repeat_type == self::REPEATED_DAILY) {
                    $repeatedCount = ceil($days / $this->repeat_day);
                } elseif ($this->repeat_type == self::REPEATED_MONTHLY) {
                    $repeatedCount = ceil($days / 30 / $this->repeat_month);
                } elseif ($this->repeat_type == self::REPEATED_WEEKLY && !empty($this->repeat_weekday)) {
                    $rW = explode(',',$this->repeat_weekday);
                    if (!empty($rW)) {
                        $repeatedCount = ceil($days/7)*count($rW);
                    }
                } elseif ($this->repeat_type == self::REPEATED_YEARLY) {
                    $repeatedCount = ceil($days / 365 / $this->repeat_years);
                }
            }
        }

        return $repeatedCount;
    }

    /**
     * @return mixed
     */
    public function getTotalEventTime()
    {
        $result['days']  = 0;
        $result['hours'] = 0;
        $result['months'] = 0;

        if (!empty($this->date_start) && !empty($this->date_end)) {
            $dateStart = new \DateTime($this->date_start);
            $dateEnd   = new \DateTime($this->date_end);

            $interval  = $dateEnd->diff($dateStart);

            $result['days']  = $interval->d;
            $result['hours'] = $interval->h;
            $result['months'] = $interval->m;
        }

        return $result;
    }

    /**
     * @param array $data
     * @param bool $withClear
     */
    public function attachProfessions($data = [], $withClear = true)
    {
        if ($withClear) {
            $this->professionsRates()->detach();
        }

        if (!empty($data)) {
            foreach($data as $prof) {
                if (isset($prof['checked'])) {
                    $dRD = isset($prof['rate_d']) ? $prof['rate_d'] : null;
                    $dRH = isset($prof['rate_h']) ? $prof['rate_h'] : null;
                    $cnt = isset($prof['count']) ? $prof['count'] : null;
                    $this->professionsRates()->attach($prof['profession'], ['count' => $cnt, 'rate_h' => $dRH, 'rate_d' => $dRD]);
                }
            }
        }
    }

    public function addProfessionsToEvents($data = [], $withClear = true)
    {
        if ($withClear) {
            $this->professionsRates()->detach();
        }

        if (!empty($data)) {

            $professions = $data['profession'];
            $counts = $data['count'];
            $prof_rate_h = $data['prof_rate_h'];
            $prof_rate_d = $data['prof_rate_d'];

            foreach($professions as $profession)
            {
                ProfessionsEventsRates::create(
                    array(
                        'events_id'     => $this->id,
                        'professions_id'=> $profession,
                        'count'         => $counts[$profession],
                        'rate_h'        => $prof_rate_h[$profession],
                        'rate_d'        => $prof_rate_d[$profession],
                    )
                );
            }
        }
    }


    /**
     * @param bool $img
     */
    protected function deleteEventPreview($img = false)
    {
        $destinationPath     = \Config::get('app.image_dir.events');

        if ($img !== false) {
            @unlink($destinationPath . $img);
        } elseif ($this->image) {
            @unlink($destinationPath . $this->image);
        }
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function saveGeneralInfo($data)
    {
        $destinationPath          = \Config::get('app.image_dir.events');
        $destinationSmallPath     = \Config::get('app.image_dir.events_small');
        $destinationPrevPath      = \Config::get('app.image_dir.events_preview');

        try {
            $this->users_id          = isset($data['users_id']) ? $data['users_id'] : '';
            $this->type_id           = isset($data['type']) ? $data['type'] : '';

            $this->title             = isset($data['title']) ? $data['title'] : '';
            $this->description       = isset($data['description']) ? $data['description'] : '';
            $this->location          = isset($data['location']) ? $data['location'] : '';
            $this->permission_view   = isset($data['permission_view']) ? $data['permission_view'] : '';
            $this->permission_invite = isset($data['permission_invite']) ? $data['permission_invite'] : '';
            $this->status            = isset($data['status']) ? $data['status'] : '';
            $this->casting           = isset($data['casting']) ? $data['casting'] : '';
            $this->manager_num       = isset($data['manager_num']) ? $data['manager_num'] : '';
            $this->date_start        = isset($data['date_start']) ? date('Y-m-d H:i', strtotime($data['date_start'])) : '';
            $this->date_end          = isset($data['date_end']) ? date('Y-m-d H:i', strtotime($data['date_end'])) : '';
            $this->report_date       = isset($data['report_date']) ? date('Y-m-d H:i', strtotime($data['report_date'])) : '';

            $oldImage                = $this->image;

            if($data['new_picture']) {

                if (trim($data['new_picture']) != $oldImage) {
                    \File::delete($destinationPath . '/' . $oldImage);
                    \File::delete($destinationSmallPath . '/' . $oldImage);
                    \File::delete($destinationPrevPath . '/' . $oldImage);
                }
                $this->image       = $data['new_picture'];
            }

            if (isset($data['image']) && is_object($data['image'])) {
                $file                = $data['image'];
                $this->image         = $data['image']->getClientOriginalName();

                $file->move($destinationPath, $file->getClientOriginalName());

                if ($oldImage != $this->image) {
                    $this->deleteEventPreview($oldImage);
                }
            }

            if($this->save()) {

                return true;
            }
        } catch (\Exception $e) {

            return false;
        }

        return false;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invites()
    {
        return $this->hasMany('Acme\Models\Events\EventInvite', 'events_id', 'id');
    }

    /**
     * @param integer $userId User ID
     *
     * @return bool
     */
    public function checkUserInvites($userId)
    {
        $invites = EventInvite::where('events_id', '=', $this->id)->where('users_id',  '=', $userId)->first();

        if ($invites instanceof EventInvite) {

            return $invites;
        }

        return false;
    }


    /**
     * @param $data
     *
     * @return \Acme\Models\Events\EventInvite|bool
     */
    public function updateOrCreateInvite($data)
    {
        try {
            $userId        = isset($data['user_id']) ? $data['user_id'] : null;
            $professionsId = isset($data['professions_id']) ? $data['professions_id'] : null;
            $isManager     = isset($data['is_manager']) ? 1 : 0;

            $invite        = $this->checkUserInvites($userId);

            if (!($invite instanceof EventInvite)) {

                $invite            = new EventInvite();

                $invite->events_id = $this->id;
                $invite->users_id  = $userId;
            }


            $invite->professions_id = $professionsId;
            $invite->is_manager     = $isManager;
            $invite->status         = EventInvite::INVITE_PROF_STATUS_NEW;
            $invite->manager_status = EventInvite::INVITE_MANAGER_STATUS_NEW;

            if ($invite->save()) {

                return $invite;
            }

            return false;

        } catch (\Exception $e) {

            return false;
        }
    }

    /**
     * @param $professionId
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getOneProfessionRate($professionId)
    {
        return $this->professionsRates()->where('professions_events_rates.professions_id', $professionId)->first();
    }

    /**
     * @return mixed
     */
    public function getActiveInvitation()
    {
        return $this->invites->filter(function($param)
        {
            return (
                $param->status         == EventInvite::INVITE_PROF_STATUS_ACCEPT
            );
        });
    }

    /**
     * @param \Acme\Models\Events\EventInvite $invitation
     */
    public function addUserToTeam(EventInvite $invitation, &$msg)
    {
        $teamRatio = $this->getProfTeamRatio();

        if ($invitation->status == EventInvite::INVITE_PROF_STATUS_REJECT) {

            $invitation->in_team = EventInvite::IS_OUT_TEAM;

        } elseif (
            $invitation->status         == EventInvite::INVITE_PROF_STATUS_ACCEPT
        ) {

            $inTeam        = $invitation->in_team;
            $managerInTeam = EventInvite::IS_MANAGER_OUT_TEAM;

            if (count($teamRatio) && ($this->id == $invitation->events_id)) {
                $places = $this->getTeamPlace($teamRatio, $invitation->professions_id);

                if (count($places)) {
                    foreach ($places as $p) {
                        $tmpArr = $teamRatio[$p];

                        if ($tmpArr->in_team == EventInvite::IS_OUT_TEAM) {
                            if ($tmpArr->users_id == $invitation->users_id){

                                $isPlace = $this->isAllowToAddToTeam($invitation->professions_id);

                                if ($isPlace) {
                                    $inTeam = EventInvite::IS_IN_TEAM;
                                } else {
                                    $inTeam = EventInvite::IS_IN_RESERVE;
                                }
                                break;
                            } elseif ($tmpArr->users_id === null) {
                                $inTeam = EventInvite::IS_IN_TEAM;
                                break;
                            }
                        }
                    }
                }
            }

            if ($invitation->is_manager == 1 && $invitation->manager_status == EventInvite::INVITE_MANAGER_STATUS_ACCEPT) {

                if ($this->isManagerComplete()) {
                    $managerInTeam = EventInvite::IS_MANAGER_IN_RESERVE;
                } else {
                    $managerInTeam = EventInvite::IS_MANAGER_IN_TEAM;
                }
            }

            $invitation->in_team         = $inTeam;
            $invitation->manager_in_team = $managerInTeam;
        }

        if ($inTeam == EventInvite::IS_IN_TEAM) {
            $msg = \Lang::get('events.you_become_team_members_event') . ' ' . $this->title;
            if($invitation->is_manager) {
                $notification = \Lang::get('notifications.msg_01') . ' <a target="_blank" href="' . \URL::to('events/show/' . $this->id) . '">' . \Lang::get('notifications.msg_02') . '</a>';
                NotificationRepository::addNewNotification($invitation->users_id, $this->user->id, $notification . ' ' . \Lang::get('notifications.msg_08'), Notifications::TYPE_EVENT_INVITE);
                NotificationRepository::addNewNotification($invitation->users_id, $this->user->id, $notification, Notifications::TYPE_EVENT_INVITE);
            } else {
                $notification = \Lang::get('notifications.msg_01') . ' <a target="_blank" href="' . \URL::to('events/show/' . $this->id) . '">' . \Lang::get('notifications.msg_02') . '</a>';
                NotificationRepository::addNewNotification($invitation->users_id, $this->user->id, $notification, Notifications::TYPE_EVENT_INVITE);
            }

            PaymentsRepository::addNewPayments($this->id, $invitation->users_id, $invitation->professions_id, $this->getPriceOneProfession($invitation->professions_id));

        } elseif ($inTeam == EventInvite::IS_IN_RESERVE) {
            $notification = \Lang::get('notifications.msg_01') . ' <a target="_blank" href="' . \URL::to('events/show/' . $this->id) . '">' . \Lang::get('notifications.msg_02') . '</a> ' . \Lang::get('notifications.msg_04');
            $msg = \Lang::get('events.you_become_reserve_team_members_event') . ' ' . $this->title;
            NotificationRepository::addNewNotification($this->user->id, $invitation->users_id, ($invitation->is_manager) ? \Lang::get('notifications.msg_01') . ' <a target="_blank" href="' . \URL::to('events/show/' . $this->id) . '">' . \Lang::get('notifications.msg_02') . '</a> ' . \Lang::get('notifications.msg_03') : \Lang::get('notifications.msg_01') . ' <a target="_blank" href="' . \URL::to('events/show/' . $this->id) . '">' . \Lang::get('notifications.msg_02') . '</a> ' . \Lang::get('notifications.msg_04'), Notifications::TYPE_EVENT_INVITE);
            NotificationRepository::addNewNotification($invitation->users_id, $this->user->id, ($invitation->is_manager) ? \Lang::get('notifications.msg_01') . ' <a target="_blank" href="' . \URL::to('events/show/' . $this->id) . '">' . \Lang::get('notifications.msg_02') . '</a> ' . \Lang::get('notifications.msg_03') : \Lang::get('notifications.msg_01') . ' <a target="_blank" href="' . \URL::to('events/show/' . $this->id) . '">' . \Lang::get('notifications.msg_02') . '</a> ' . \Lang::get('notifications.msg_04'), Notifications::TYPE_EVENT_INVITE);
            NotificationRepository::addNewNotification($invitation->users_id, $this->user->id, $notification, Notifications::TYPE_EVENT_INVITE);
        }

        $invitation->save();
    }

    /**
     * Get team places for some professions
     *
     * @param array   $team
     * @param integer $professionId
     *
     * @return array
     */
    private function getTeamPlace($team, $professionId)
    {
        $keys = [];
        if (count($team)) {
            foreach ($team as $k=>$v) {
                if ($v->professions_id == $professionId) {
                    $keys[] = $k;
                }
            }
        }

        return $keys;

    }


    /**
     * @return bool
     */
    public function checkIsTeamComplete()
    {
        $team              = $this->getCompleteTeam();
        $isManagerComplete = $this->isManagerComplete();

        if (count($team) && $isManagerComplete) {

            return true;
        }

        return false;
    }

    /**
     * @param bool|true $isRefresh
     *
     * @return array|static[]
     */
    public function getCompleteTeam($isRefresh = true, $is_team_check = false)
    {
        $isNeedRefresh = ($isRefresh) ? true : (empty($this->completeTeam) ? true : false);

        if ($isNeedRefresh) {

            if($is_team_check) {
                $this->completeTeam = EventInviteRepository::getEventTeam($this->id, EventInvite::INVITE_PROF_STATUS_ACCEPT, true, false);
            } else {
                $this->completeTeam = EventInviteRepository::getEventTeam($this->id, EventInvite::INVITE_PROF_STATUS_ACCEPT);
            }

            return $this->completeTeam;
        } else {

            return $this->completeTeam;
        }
    }
    
    /**
     * @param bool|true $isRefresh
     *
     * @return array|static[]
     */
    public function getCompleteTeamM()
    {

        $this->completeTeam = EventInviteRepository::getEventTeamForManagerCheck($this->id, EventInvite::INVITE_PROF_STATUS_ACCEPT);

        if($this->isManagerComplete() && $this->completeTeam) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return bool
     */
    public function isManagerComplete()
    {
        $acceptedManagers = $this->getTeamManager();

        return (count($acceptedManagers) == $this->manager_num);
    }

    /**
     * @return int
     */
    public function getCompletedCountManager()
    {
        return count($this->getTeamManager());
    }

    /**
     * @return mixed
     */
    public function getTeamManager()
    {
        return $this->invites->filter(function($param)
        {
            return (
                    $param->is_manager == EventInvite::MANAGER_ON &&
                    $param->manager_in_team == EventInvite::IS_MANAGER_IN_TEAM &&
                    $param->manager_status == EventInvite::INVITE_MANAGER_STATUS_ACCEPT
            );
        });
    }

    /**
     * @return int
     */
    public function getReservedCountManager()
    {
        return count($this->getReservedTeamManager());
    }

    /**
     * @return mixed
     */
    public function getReservedTeamManager()
    {
        return $this->invites->filter(function($param)
        {
            return (
                $param->is_manager == EventInvite::MANAGER_ON &&
                $param->manager_in_team == EventInvite::IS_MANAGER_IN_RESERVE &&
                $param->manager_status == EventInvite::INVITE_MANAGER_STATUS_ACCEPT
            );
        });
    }

    /**
     * @param $professionId
     * @return mixed
     */
    public function getPresentUserOnProfession($professionId)
    {
        return $this->invites->filter(function($param) use ($professionId)
        {
            return (
                $param->professions_id == $professionId &&
                $param->in_team        == EventInvite::IS_IN_TEAM &&
                $param->status         == EventInvite::INVITE_PROF_STATUS_ACCEPT
            );
        });
    }

    /**
     * @param $professionId
     * @return bool
     */
    public function isAllowToAddManagerToTeam($users_id, $in_t = false)
    {
        if($in_t) {
            $result = $this->invites->filter(function($param) use ($users_id)
            {
                return (
                    $param->users_id        == $users_id &&
                    ($param->in_team         == EventInvite::IS_IN_RESERVE || $param->in_team == EventInvite::IS_IN_TEAM) &&
                    $param->manager_in_team == EventInvite::IS_MANAGER_IN_RESERVE &&
                    $param->manager_status  == EventInvite::INVITE_MANAGER_STATUS_ACCEPT &&
                    $param->status          == EventInvite::INVITE_PROF_STATUS_ACCEPT
                );
            });
        } else {
            $result = $this->invites->filter(function($param) use ($users_id)
            {
                return (
                    $param->users_id        == $users_id &&
                    $param->in_team         == EventInvite::IS_IN_RESERVE &&
                    $param->manager_in_team == EventInvite::IS_MANAGER_IN_RESERVE &&
                    $param->manager_status  == EventInvite::INVITE_MANAGER_STATUS_ACCEPT &&
                    $param->status          == EventInvite::INVITE_PROF_STATUS_ACCEPT
                );
            });
        }
        return count($result);
    }
    /**
     * @param bool|true $isRefresh
     *
     * @return array|static[]
     */
    public function getProfTeamRatio($isRefresh = true)
    {
        $isNeedRefresh = ($isRefresh) ? true : (empty($this->ratioTeamProf) ? true : false);

        if ($isNeedRefresh) {

            $this->ratioTeamProf = EventInviteRepository::getEventTeam($this->id, false, false, false);
            return $this->ratioTeamProf;
        } else {

            return $this->ratioTeamProf;
        }
    }

    /**
     * @param $professionId
     * @return bool
     */
    public function isAllowToAddToTeam($professionId)
    {
        $needProfCount = 0;

        if ($this->professionsRates->contains($professionId))
        {
            $needProfCount = $this->professionsRates->find($professionId)->pivot->count;
        }

        $presentProf = $this->getPresentUserOnProfession($professionId);

        if ($needProfCount > count($presentProf)) {

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        return $this->invites->filter(function($param)
        {
            return (
                (
                    $param->status == EventInvite::INVITE_PROF_STATUS_ACCEPT
                )
                &&
                (
                    $param->in_team == EventInvite::IS_IN_TEAM ||
                    $param->in_team == EventInvite::IS_IN_RESERVE
                )
            );
        });
    }

    /**
     * @return mixed
     */
    public function getMainTeam()
    {
        return $this->invites->filter(function($param)
        {
            return (
                (
                    $param->status == EventInvite::INVITE_PROF_STATUS_ACCEPT
                )
                &&
                (
                    $param->in_team == EventInvite::IS_IN_TEAM
                )
            );
        });
    }

    /**
     * @return mixed
     */
    public function getReservedTeam()
    {
        return $this->invites->filter(function($param)
        {
            return (
                (
                    $param->status == EventInvite::INVITE_PROF_STATUS_ACCEPT
                )
                &&
                (
                    $param->in_team         == EventInvite::IS_IN_RESERVE ||
                    $param->manager_in_team == EventInvite::IS_MANAGER_IN_RESERVE
                )
            );
        });
    }

    /**
     * @return mixed
     */
    public function getReservedTeamByProfession($professionsId)
    {
        return $this->invites->filter(function ($param) use ($professionsId)
        {
            return (
                (
                    $param->status == EventInvite::INVITE_PROF_STATUS_ACCEPT &&
                    $param->professions_id = $professionsId
                )
                &&
                (
                    $param->in_team         == EventInvite::IS_IN_RESERVE ||
                    $param->manager_in_team == EventInvite::IS_MANAGER_IN_RESERVE
                )
            );
        });
    }


    /**
     * @param \Acme\Models\Events\EventInvite $deletedInvite
     */
    public function getFromReservedInTeam(EventInvite $deletedInvite)
    {
        $reserved = $this->getReservedTeamByProfession($deletedInvite->professions_id);

        if (count($reserved)) {
            $first = $reserved->first();

            if ($first instanceof EventInvite) {
                $first->in_team = EventInvite::IS_IN_TEAM;

                if (
                    $first->is_manager == EventInvite::MANAGER_ON &&
                    $first->manager_status == EventInvite::INVITE_MANAGER_STATUS_ACCEPT &&
                    $deletedInvite->is_manager == EventInvite::MANAGER_ON &&
                    $deletedInvite->manager_status == EventInvite::INVITE_MANAGER_STATUS_ACCEPT
                ) {
                    $first->manager_in_team = EventInvite::IS_MANAGER_IN_TEAM;
                }
                PaymentsRepository::addNewPayments($this->id, $first->users_id, $first->professions_id, $this->getTotalPriceOneProfession($first->professions_id));
                $first->save();
            }
        }
    }


    /**
     * Send a report to organizer about invitation
     */
    public function sendInvitationReport()
    {
        $invites = $this->getActiveInvitation();
        $user    = $this->user;
        $siteUrl = \Config::get('app.url');

        if (count($invites) && ($user instanceof User)) {
            \Mail::send('emails.invites.report', ['invitations' => $invites, 'event' => $this, 'url' => $siteUrl], function($message) use ($user)
            {
                $subject = \Lang::get('mails.report_invitation_event') . ' ' . $this->title;
                $message->to($user->email, $user->getUserName())->subject($subject);
            });
        }
    }

    /**
     * @param \Acme\Models\Events\EventInvite $invitation
     * @param bool|false $isManager
     */
    public function sendToOrganizerAcceptInvitationReport(EventInvite $invitation, $isManager = false)
    {
        $user           = $this->user;
        $invitationUser = $invitation->user;
        $siteUrl        = \Config::get('app.url');

        if (($user instanceof User) && ($invitationUser instanceof User)) {

            \Queue::push('\Acme\Queues\SendToOrganizerAcceptInvitationReport', [
                'eventId'          => $this->id,
                'url'              => $siteUrl,
                'userName'         => $invitationUser->getUserName(),
                'eventTitle'       => $this->title,
                'organizeName'     => $user->getUserName(),
                'isManager'        => $isManager,
                'organizeEmail'    => $user->email
            ], 'emails');
        }
    }


    /**
     * @param \Acme\Models\Events\EventInvite $invitation
     * @param bool|false $isManager
     */
    public function sendToOrganizerRejectedReport(EventInvite $invitation, $isManager = false)
    {
        $user         = $this->user;
        $rejectedUser = $invitation->user;
        $siteUrl      = \Config::get('app.url');
        $reject_send  = true;
        $event        = $this;
        
        $result = \App::make('PMSearchServiceProvider', compact('reject_send', 'event'));

        if (($user instanceof User) && ($rejectedUser instanceof User)) {

            NotificationRepository::addNewNotification($rejectedUser->id, $user->id, ($isManager) ? \Lang::get('notifications.msg_06') : \Lang::get('notifications.msg_05') . ' <a target="_blank" href="' . \URL::to('events/show/' . $this->id) . '">' . \Lang::get('notifications.msg_02') . '</a>', Notifications::TYPE_EVENT_INVITE);

            \Queue::push('\Acme\Queues\SendReportInviteRejectMessageQueue', [
                'eventId'          => $this->id,
                'url'              => $siteUrl,
                'rejectedUserName' => $rejectedUser->getUserName(),
                'eventTitle'       => $this->title,
                'organizeName'     => $user->getUserName(),
                'isManager'        => $isManager,
                'organizeEmail'    => $user->email,
                'result'           => $result
            ], 'emails');
        }
    }

    /**
     * @return bool
     */
    public function checkAvailableToAccept()
    {
        $dateStart = $this->date_start;

        if (!empty($dateStart)) {
            $now            = new \DateTime("now");
            $eventStartDate = new \DateTime($dateStart);
            $interval       = $now->diff($eventStartDate);

            if ($interval->invert == 1) {
                return false;
            } else {
                if ($interval->days > 0) {

                    return true;
                }
            }
        }

        return false;
    }


    /**
     * @param null $callback
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete($callback = null)
    {
        $invitation = $this->invites;

        if (isset($callback)){
            if (is_callable($callback)){
                if (count($invitation)) {
                    foreach($invitation as $inv) {
                        call_user_func($callback, $this, $inv);
                    }
                }
            }
        }

        $destinationPath      = \Config::get('app.image_dir.events') . $this->image;
        $destinationSmallPath = \Config::get('app.image_dir.events_small') . $this->image;
        $destinationPrevPath  = \Config::get('app.image_dir.events_preview') . $this->image;

        if (\File::exists($destinationPath)) {
            \File::delete($destinationPath);
            \File::delete($destinationSmallPath);
            \File::delete($destinationPrevPath);
        }

        $this->professionsRates()->detach();
        $this->invites()->delete();

        return parent::delete();
    }

    /**
     * @return float
     */
    public function getEventRating()
    {
        return RatingsRepository::getAvgRating(\Acme\Models\Ratings::RATING_TYPE_EVENT, $this->id);
    }

    /**
     * @return float
     */
    public function getEventTeamRating()
    {
        foreach($this->getMainTeam() as $user_invite) {
            $sum[] = User::find($user_invite->users_id)->getUserRating();
        }

        $avg = array_sum($sum)/count($sum);

        return $avg;
    }

    /**
     * @return string
     */
    public function getTemporarySmallFile()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->image) {

            return asset(\Config::get('app.image_dir.events_small') . $this->image);
        } else {

            return asset(\Config::get('app.image_dir.events_small') . $thePic);
        }
    }

    /**
     * @return mixed
     */
    public function getTemporarySmallFileName()
    {
        $thePic = \Input::old('new_picture');
        if (!$thePic && $this->image) {

            return $this->image;
        } else {

            return $thePic;
        }
    }
}