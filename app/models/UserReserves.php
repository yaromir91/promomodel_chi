<?php

namespace Acme\Models;


/**
 *
 * Class UserReserves
 *
 * @package Acme\Models
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class UserReserves extends \Eloquent {
    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $timestamps = false;
}