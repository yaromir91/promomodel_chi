<?php

namespace Acme\Models;


/**
 *
 * Class EventComments
 *
 * @package Acme\Models
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class EventComments extends \Eloquent
{
    /**
     * @var string
     */
    protected $table = 'event_comments';

    /**
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new static)->table;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function users()
    {
        return $this->hasOne('Acme\Models\User', 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function events()
    {
        return $this->hasOne('Acme\Models\Events', 'id', 'events_id');
    }

    /**
     * Get the comment's author.
     *
     * @return User
     */
    public function author()
    {
        return $this->belongsTo('Acme\Models\User', 'user_id');
    }

    /**
     * Get the comment's content.
     *
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * Get the date the post was created.
     *
     * @param \Carbon|null $date
     * @return string
     */
    public function date($date=null)
    {
        if(is_null($date)) {
            $date = $this->updated_at;
        }

        return $date->toDateTimeString();
//        return \String::date($date);
    }

    public function delete()
    {

        $destinationPath      = \Config::get('app.image_dir.comment') . $this->id . '/' . $this->image;

        if (\File::exists($destinationPath)) {
            \File::delete($destinationPath);
        }

        return parent::delete();
    }
}