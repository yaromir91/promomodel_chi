<?php

namespace Acme\Models;

use Illuminate\Support\Facades\URL;

class Post extends \Eloquent {

    const REPORT_STATUS_ACTIVE  = 0;
    const REPORT_STATUS_DRAFT   = 1;

    /**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	public function delete()
	{
		$this->comments()->delete();

		$destinationPath      = \Config::get('app.image_dir.posts') . $this->image;
		$destinationSmallPath = \Config::get('app.image_dir.posts_small') . $this->image;
		$destinationPrevPath  = \Config::get('app.image_dir.posts_preview') . $this->image;

		if (\File::exists($destinationPath)) {
			\File::delete($destinationPath);
			\File::delete($destinationSmallPath);
			\File::delete($destinationPrevPath);
		}

		return parent::delete();
	}

	/**
	 * Returns a formatted post content entry,
	 * this ensures that line breaks are returned.
	 *
	 * @return string
	 */
	public function content()
	{
		return nl2br($this->content);
	}

	/**
	 * Get the post's author.
	 *
	 * @return User
	 */
	public function author()
	{
		return $this->belongsTo('Acme\Models\User', 'user_id');
	}

	/**
	 * Get the post's meta_description.
	 *
	 * @return string
	 */
	public function meta_description()
	{
		return $this->meta_description;
	}

	/**
	 * Get the post's meta_keywords.
	 *
	 * @return string
	 */
	public function meta_keywords()
	{
		return $this->meta_keywords;
	}

	/**
	 * Get the post's comments.
	 *
	 * @return array
	 */
	public function comments()
	{
		return $this->hasMany('Acme\Models\Comment');
	}

    /**
     * Get the date the post was created.
     *
     * @param \Carbon|null $date
     * @return string
     */
    public function date($date=null)
    {
        if(is_null($date)) {
            $date = $this->created_at;
        }

        return $date->toDateString();
//        return \String::date($date);
    }

	/**
	 * Get the URL to the post.
	 *
	 * @return string
	 */
	public function url()
	{
		return Url::to('report/show/' . $this->slug);
	}

	/**
	 * Returns the date of the blog post creation,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function created_at()
	{
		return $this->date($this->created_at);
	}

	/**
	 * Returns the date of the blog post last update,
	 * on a good and more readable format :)
	 *
	 * @return string
	 */
	public function updated_at()
	{
        return $this->date($this->updated_at);
	}

    public function tags()
    {
        return $this->belongsToMany('Acme\Models\Tags', 'posts_tags', 'post_id', 'tag_id');
    }



	/**
	 * @return string
	 */
	public function getTemporarySmallFile()
	{
		$thePic = \Input::old('new_picture');
		if (!$thePic && $this->image) {

			return asset(\Config::get('app.image_dir.posts_small') . $this->image);
		} elseif ($thePic) {
			return asset(\Config::get('app.image_dir.posts_small') . $thePic);
		}

		return '';
	}

	/**
	 * @return mixed
	 */
	public function getTemporarySmallFileName()
	{
		$thePic = \Input::old('new_picture');
		if (!$thePic && $this->image) {

			return $this->image;
		} else {

			return $thePic;
		}
	}
        
        public function status()
	{
            if($this->created_at <= date('Y-m-d H:m:s', time() - 31536000)) {
                return 'Архивное';
            }
            
            $data = [
                self::REPORT_STATUS_ACTIVE  => \Lang::get('messages.publeshed'),
                self::REPORT_STATUS_DRAFT   => \Lang::get('messages.draft'),
            ];
            return $data[$this->status];
	}
}
