<?php

namespace Acme\Validators;

use Illuminate\Validation\Validator;

/**
 * Class PromoModelArrayValidator
 * @package Acme\Validators
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class PromoModelArrayValidator extends Validator
{

    /**
     * @var array
     */
    private $_customMessages = array(
        "eitherorrequired"   => "Select any parameters."
    );

    /**
     * @param \Symfony\Component\Translation\TranslatorInterface $translator
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     */
    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
        parent::__construct( $translator, $data, $rules, $messages, $customAttributes );

        $this->_setCustomStuff();
    }

    /**
     * Setup any customizations etc
     *
     * @return void
     */
    protected function _setCustomStuff() {
        $this->setCustomMessages( $this->_customMessages );
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return int
     */
    public function validateEitherorrequired($attribute, $value, $parameters)
    {
        if (!empty($parameters)) {
            foreach ($parameters as $param) {
                if (isset($this->data[$param])) {

                    return true;
                }
            }
        }

        return false;
    }
}