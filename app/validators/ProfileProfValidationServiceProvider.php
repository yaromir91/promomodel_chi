<?php

namespace Acme\Validators;

use Illuminate\Support\ServiceProvider;

/**
 * Class ProfileProfValidationServiceProvider
 *
 * @package Acme\Validators
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ProfileProfValidationServiceProvider extends ServiceProvider
{

    public function register(){}

    public function boot()
    {
        $this->app->validator->resolver(function($translator, $data, $rules, $messages)
            {
                return new ProfileProfArrayValidator($translator, $data, $rules, $messages);
            });
    }
}