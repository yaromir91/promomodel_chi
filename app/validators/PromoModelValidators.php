<?php

namespace Acme\Validators;

use Validator;

/**
 * Class PromoModelValidators
 * @package Acme\Validators
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class PromoModelValidators
{

    /**
     * @param array $input  Data
     *
     * @return \Illuminate\Validation\Validator
     */
    static public function getEventInvitationValidator($input)
    {
        $rules = [
            //'user_id'        => 'required|eitherorrequired:is_manager,professions_id'
            'user_id'        => 'required',
            'professions_id' => 'required',
        ];

        return Validator::make($input, $rules);
    }

    /**
     * @return array
     */
    static public function getEventsValidationMessages()
    {
        // 'email.required' => 'We need to know your e-mail address!',
        return [
            'report_date.before' => '',
            'date_end.after'     => '',
            'date_start',
        ];
    }
}