<?php

namespace Acme\Validators\Profile;

use Acme\Models\Profile\UserProfile;
use Validator;
use Acme\Models\Role;

/**
 * Class ProfileValidators
 *
 * @package Acme\Validators\Profile
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ProfileValidators
{


    /**
     * @param array $input  Data
     * @param null  $userID User ID
     *
     * @return \Illuminate\Validation\Validator
     */
    static public function getApplicantGeneralFormValidator($input, $userID = null)
    {
        $profile = false;
        if($userID) {
            $profile = UserProfile::where('user_id', '=', $userID)->first();
        }

        $rules = [
            'username'              => 'required|min:3',
            'email'                 => ($userID) ? 'required|email|unique:users,email,'.$userID.',id' : 'required|email|unique:users',
            'last_name'             => 'required|min:2',
            'first_name'            => 'required|min:2',
            'payment_count'         => 'required|min:2',
            'phone'                 => 'phone',
            'gender'                => 'required|in:f,m',
            'vk_link'               => 'url',
            'date_birth'            => 'required|date',
            'new_picture'           => (!$profile || !$profile->avatar) ? 'required' : '',
            'avatar'                => 'image'
        ];

        return Validator::make($input, $rules);
    }

    /**
     * @param $input
     * @param null|integer $userID
     * @param null|integer $role
     *
     * @return \Illuminate\Validation\Validator
     */
    static public function getAdminApplicantGeneralFormValidator($input, $userID = null, $role = null)
    {
        $profile = false;
        if($userID) {
            $profile = UserProfile::where('user_id', '=', $userID)->first();
        }

        if ($role == Role::APPLICANT_USER || $role == Role::APPLICANT_MANAGER_USER) {
            $rules = [
                'username' => 'required|min:3',
                'email' => ($userID) ? 'required|email|unique:users,email,' . $userID . ',id' : 'required|email|unique:users',
                'last_name' => 'required|min:2',
                'first_name' => 'required|min:2',
                'payment_count' => 'required|min:2',
                'phone' => 'phone',
                'gender' => 'required|in:f,m',
                'vk_link' => 'url',
                'date_birth' => 'date',
                'roles' => 'required',
                'new_picture' => (!$profile || !$profile->avatar) ? 'required' : '',
                'avatar' => 'image'
            ];
        } else if($role == Role::ORGANIZER_USER) {
            $rules = [
                'username' => 'required|min:3',
                'email' => ($userID) ? 'required|email|unique:users,email,' . $userID . ',id' : 'required|email|unique:users',
                'last_name' => 'required|min:2',
                'first_name' => 'required|min:2',
                'phone' => 'phone',
                'gender' => 'required|in:f,m',
                'date_birth' => 'date',
                'roles' => 'required',
                'new_picture' => (!$profile || !$profile->avatar) ? 'required' : '',
                'avatar' => 'image'
            ];
        } else {
            $rules = [
                'username'              => 'required|min:3',
                'roles'                 => 'required',
                'email'                 => ($userID) ? 'required|email|unique:users,email,'.$userID.',id' : 'required|email|unique:users',
            ];
        }

        return Validator::make($input, $rules);
    }


    /**
     * @param array $input  Data
     * @param null  $userID User ID
     *
     * @return \Illuminate\Validation\Validator
     */
    static public function getApplicantAdditionFormValidator($input, $userID = null)
    {
        $rules = [
            'eventtypes'            => 'required',
            //'professions'           => 'required|array|profileprof:prof_rate_h,prof_rate_d',
            //'profile_params'        => 'required|array|profileparams:valid_ruleArr',
//            'professions'           => 'profileprof:prof_rate_h,prof_rate_d',
            'professions'           => 'required',
            'profile_params'        => 'profileparams:valid_ruleArr',
        ];

        return Validator::make($input, $rules);
    }

    /**
     * @param array $input  Data
     * @param null  $userID User ID
     *
     * @return \Illuminate\Validation\Validator
     */
    static public function getPasswordFormValidator($input, $userID = null, $fb_status = false, $isWithoutOld = false)
    {
        if ($isWithoutOld) {
            $rules = [
                'new_password'              => 'required|between:4,8',
                'password_confirmation'     => 'required|between:4,8|same:new_password',
            ];
        } else {
            if($fb_status) {
                $rules = [
                    'new_password'              => 'required|between:4,8',
                    'password_confirmation'     => 'required|between:4,8|same:new_password',
                ];
            } else {
                $rules = [
                    'old_password'              => 'required|between:4,8',
                    'new_password'              => 'required|between:4,8',
                    'password_confirmation'     => 'required|between:4,8|same:new_password',
                ];
            }
        }

        return Validator::make($input, $rules);
    }


    /**
     * @param array $input  Data
     * @param null  $userID User ID
     *
     * @return \Illuminate\Validation\Validator
     */
    static public function getOrganizerGeneralFormValidator($input, $userID = null)
    {
        $profile = false;
        if($userID) {
            $profile = UserProfile::where('user_id', '=', $userID)->first();
        }

        $rules = [
            'username'              => 'required|min:3',
            'email'                 => ($userID) ? 'required|email|unique:users,email,'.$userID.',id' : 'required|email|unique:users',
            'last_name'             => 'required|min:2',
            'first_name'            => 'required|min:2',
            'phone'                 => 'phone',
            'gender'                => 'required|in:f,m',
            'date_birth'            => 'date',
            'new_picture'           => (!$profile || !$profile->avatar) ? 'required' : '',
            'avatar'                => 'image'
        ];

        return Validator::make($input, $rules);
    }

    /**
     * @param array $input  Data
     *
     * @return \Illuminate\Validation\Validator
     */
    static public function getFbPasswordFormValidator($input)
    {
        $rules = [
            'password'              => 'required|between:4,8',
            'password_confirmation' => 'required|between:4,8|same:password',
        ];

        return Validator::make($input, $rules);
    }
}