<?php

namespace Acme\Validators;


use  Zizaco\Confide\UserValidator;

/**
 * Class RegistrationUserValidation
 *
 * @package Acme\Validators
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class RegistrationUserValidation extends UserValidator
{

    public $rules = [
        'create' => [
            'username'    => 'required|alpha_dash',
            'email'       => 'required|email',
            'password'    => array('required', 'regex:/(?=^.{6,16}$)([a-zA-Z0-9])*$/'),
            'password_confirmation' => 'required|same:password',
            'lastName'    => 'required',
            'firstName'   => 'required',
            'gender'      => 'required|in:f,m',
            'accountType' => 'required',
        ],
        'update' => [
            'username' => 'required|alpha_dash',
            'email'    => 'required|email',
            'password' => 'required|min:4',
        ]
    ];
}