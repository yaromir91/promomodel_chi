<?php

namespace Acme\Validators;

use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Validator;
use Acme\Models\User;
use Acme\Models\Role;

/**
 * Class ProfileProfArrayValidator
 *
 * @package Acme\Validators
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ProfileProfArrayValidator extends Validator
{

    /**
     * @var array
     */
    private $_customMessages = array(
        "profileprof"      => "The selected :attribute must have hourly rate or daily rate.",
        "profileparams"    => "Пожалуйста, введите правильные данные.",
        "phone"            => "Введите коректный номер телефона.",
        "eitherorrequired" => "Выберите любые параметры.",
        "groupuser"        => "This user already has a group.This user can create only one group."
    );

    /**
     * @param \Symfony\Component\Translation\TranslatorInterface $translator
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     */
    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
        parent::__construct( $translator, $data, $rules, $messages, $customAttributes );

        $this->_setCustomStuff();
    }

    /**
     * Setup any customizations etc
     *
     * @return void
     */
    protected function _setCustomStuff() {
        $this->setCustomMessages( $this->_customMessages );
    }


    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    public function validateProfileparams($attribute, $value, $parameters)
    {
        $filledParam = array_filter($value);

        if (!empty($parameters)) {
            $validNameArr = $parameters[0];

            if (isset($this->data[$validNameArr])) {

                $validator = \Validator::make($value, $this->data[$validNameArr]);

                if ($validator->fails())
                {
                    $mssages = $validator->messages()->getMessages();
                
                    if (count($mssages)) {
                        foreach ($mssages as $k => $v) {
                            $msg = str_replace($k, \Lang::get('general.field'), reset($v));
                            $this->getMessageBag()->add($attribute . '_' . $k, $msg);
                        }
                    }

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    public function validateProfileprof($attribute, $value, $parameters)
    {
        if (count($parameters) && count($value)) {

            foreach ($value as $v) {
                $isValue = 0;
                foreach ($parameters as $p) {
                    $paramArr = array_filter($this->data[$p]);

                    if (!count($paramArr)) {
                        return false;
                    }

                    if (isset($paramArr[$v]) && is_numeric(trim($paramArr[$v]))) {
                        $isValue++;
                    }
                }

                if ($isValue == 0) {

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return int
     */
    public function validatePhone($attribute, $value, $parameters)
    {
        if (!empty($value)) {

            $pattern  = "/^\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}$/";

            return preg_match($pattern, $value, $matces);
        }

        return true;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return int
     */
    public function validateEitherorrequired($attribute, $value, $parameters)
    {
        if (!empty($parameters)) {
            foreach ($parameters as $param) {
                if (isset($this->data[$param])) {

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    public function validateGroupuser($attribute, $value, $parameters)
    {
        if (!empty($parameters)) {
            $groupId = reset($parameters);

            if (!isset($this->data[$groupId])) {
                $user = User::find($value);

                if ($user instanceof User) {
                    $userCodeRole = $user->getMainUserRoleCode();

                    if ($userCodeRole != Role::AGENT_USER) {

                        if (count($user->groups) > 0) {

                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
}