<?php

namespace Acme\Validators;

use Illuminate\Support\ServiceProvider;


/**
 * Class PromoModValidatorsServiceProvider
 * @package Acme\Validators
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class PromoModValidatorsServiceProvider extends ServiceProvider
{

    public function register(){}

    public function boot()
    {
        $this->app->validator->resolver(function($translator, $data, $rules, $messages)
        {
            return new PromoModelArrayValidator($translator, $data, $rules, $messages);
        });
    }
}