<?php

namespace Acme\Queues;

/**
 * Class SendAboutDeleteTeamMemberMessageQueue
 * @package Acme\Queues
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class SendAboutDeleteTeamMemberMessageQueue
{

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        if (
            isset($data['email']) &&
            isset($data['username']) &&
            isset($data['eventId']) &&
            isset($data['teamMember']) &&
            isset($data['eventTitle'])
        ) {
            \Mail::send('emails.invites.delete_team_member', $data, function($message) use ($data)
            {
                $message->to($data['email'], $data['username'])->subject(\Lang::get('mails.you_deleted_from_team'));
            });
        }
    }
}