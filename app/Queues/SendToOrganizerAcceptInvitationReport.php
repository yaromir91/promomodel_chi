<?php

namespace Acme\Queues;

/**
 * Class SendToOrganizerAcceptInvitationReport
 * @package Acme\Queues
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class SendToOrganizerAcceptInvitationReport
{

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        if (isset($data['eventId']) &&
            isset($data['url']) &&
            isset($data['userName']) &&
            isset($data['eventTitle']) &&
            isset($data['organizeName']) &&
            isset($data['isManager']) &&
            isset($data['organizeEmail'])
        ) {

            \Mail::send('emails.invites.user_invitation_accept_report', [
                'eventId'    => $data['eventId'],
                'siteUrl'    => $data['url'],
                'userName'   => $data['userName'],
                'isManager'  => $data['isManager'],
                'eventTitle' => $data['eventTitle']
            ], function($message) use ($data)
            {
                $subject = \Lang::get('mails.report_invitation_event') . ' ' . $data['eventTitle'];
                $message->to($data['organizeEmail'], $data['organizeName'])->subject($subject);
            });
        }
    }
}