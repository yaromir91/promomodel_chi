<?php

namespace Acme\Queues;

/**
 * Class SendReportInviteRejectMessageQueue
 * @package Acme\Queues
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class SendReportInviteRejectMessageQueue
{

    /**
     * @param $job+
     * @param $data
     */
    public function fire($job, $data)
    {
        if (isset($data['eventId']) &&
            isset($data['url']) &&
            isset($data['rejectedUserName']) &&
            isset($data['eventTitle']) &&
            isset($data['organizeName']) &&
            isset($data['isManager']) &&
            isset($data['organizeEmail'])
        ) {
            \Mail::send('emails.invites.user_rejected_report', [
                'eventId'          => $data['eventId'],
                'siteUrl'          => $data['url'],
                'isManager'        => $data['isManager'],
                'rejectedUserName' => $data['rejectedUserName'],
                'result'           => $data['result'],
            ], function($message) use ($data)
            {
                $subject = \Lang::get('mails.report_invitation_event') . ' ' . $data['eventTitle'];
                $message->to($data['organizeEmail'], $data['organizeName'])->subject($subject);
            });
        }
    }
}