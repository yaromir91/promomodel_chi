<?php

namespace Acme\Queues;

/**
 * Class SendManagerInvitationQueue
 * @package Acme\Queues
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class SendManagerInvitationQueue
{

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        if (isset($data['email']) && isset($data['username'])) {
            \Mail::send('emails.invites.new_manager_invitation', $data, function($message) use ($data)
            {
                $message->to($data['email'], $data['username'])->subject(sprintf(\Lang::get('mails.you_was_invited_as_manager_new_event'), $data['username']));
            });
        }
    }
}