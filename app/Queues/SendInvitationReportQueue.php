<?php

namespace Acme\Queues;

use Acme\Models\Events\Events;

/**
 * Class SendInvitationReportQueue
 * @package Acme\Queues
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class SendInvitationReportQueue
{

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        if (isset($data['eventId'])) {
            $event = Events::find($data['eventId']);

            if ($event instanceof Events) {
                $event->sendInvitationReport();
            }
        }
    }
}