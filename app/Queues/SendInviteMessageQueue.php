<?php

namespace Acme\Queues;

/**
 * Class SendInviteMessageQueue
 *
 * @package Acme\Queues
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class SendInviteMessageQueue
{

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        if (isset($data['email']) && isset($data['username'])) {
            \Mail::send('emails.invites.new_invite', $data, function($message) use ($data)
            {
                $message->to($data['email'], $data['username'])->subject(\Lang::get('mails.you_invited_to_event'));
            });
        }
    }
}