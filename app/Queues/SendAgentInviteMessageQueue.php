<?php

namespace Acme\Queues;

/**
 *
 * Class SendAgentInviteMessageQueue
 *
 * @package Acme\Queues
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class SendAgentInviteMessageQueue
{

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        if (isset($data['email']) && isset($data['username'])) {
            \Mail::send('emails.invites.agent_invite', $data, function($message) use ($data)
            {
                $message->to($data['email'], $data['username'])->subject(\Lang::get('mails.you_invited_to_agent'));
            });
        }
    }
}