<?php

namespace Acme\Queues;

/**
 * Class SendLetterAboutDeleteUserQueue
 * @package Acme\Queues
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class SendLetterAboutDeleteUserQueue
{

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        if (isset($data['email']) && isset($data['username'])) {
            \Mail::send('emails.user.deleted_user', $data, function($message) use ($data)
            {
                $message->to($data['email'], $data['username'])->subject(\Lang::get('mails.you_was_deleted'));
            });
        }
    }
}