<?php

namespace Acme\Queues;

/**
 * Class SendAboutDeleteEventToMembersMessageQueue
 * @package Acme\Queues
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class SendAboutDeleteEventToMembersMessageQueue
{

    public function fire($job, $data)
    {
        if (
            isset($data['email']) &&
            isset($data['username']) &&
            isset($data['teamMember']) &&
            isset($data['eventTitle'])
        ) {
            \Mail::send('emails.invites.delete_event_team_member', $data, function($message) use ($data)
            {
                $message->to($data['email'], $data['username'])->subject(\Lang::get('mails.event_deleted'));
            });
        }
    }
}