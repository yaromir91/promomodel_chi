<?php

namespace Acme\Queues;


/**
 *
 * Class SendAdminToCancelQueue
 *
 * @package Acme\Queues
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class SendAdminToCancelQueue
{

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        if (isset($data['email']) && isset($data['organizeName'])) {
            \Mail::send('emails.admin.cancel_event', $data, function($message) use ($data)
            {
                $message->to($data['email'], $data['organizeName'])->subject(sprintf(\Lang::get('mails.event_cancel_message'), $data['organizeName']));
            });
        }
    }
}