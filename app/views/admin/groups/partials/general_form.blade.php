{{-- Edit Param Form --}}
<form class="form-horizontal avatar-form" enctype="multipart/form-data" method="post" action="@if (isset($group)){{ URL::to('admin/groups/' . $group->id . '/edit') }} @else {{ URL::to('admin/groups/create') }} @endif" autocomplete="off">

    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->

    <div class="form-group {{ $errors->has('title') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="title">{{ Lang::get('admin/groups/table.title') }}</label>
            <input class="form-control" maxlength="70" type="text" name="title" id="title" value="{{ Input::old('title', isset($group) ? $group->title : null) }}" />
            {{ $errors->first('title', '<span class="help-block">:message</span>') }}
        </div>
    </div>

    <div class="form-group {{ $errors->has('user_id') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="user_id">{{ Lang::get('admin/groups/form.user') }}</label>
            {{ Form::select('user_id', $usersList, Input::old('user_id', isset($group) ? $group->user_id : null), array('class' => 'form-control', 'id' => 'user_id')) }}
            {{ $errors->first('user_id', '<span class="help-block">:message</span>') }}
        </div>
    </div>

    <div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="description">{{ Lang::get('admin/groups/form.description') }}</label>
            {{ Form::textarea('description', Input::old('description', isset($group) ? $group->description : null), array('class' => 'form-control', 'id' => 'description')) }}
            {{ $errors->first('description', '<span class="help-block">:message</span>') }}
        </div>
    </div>

    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
        <div class="col-md-12">
            {{ Form::label('image', Lang::get('admin/groups/form.title_photo'), array('class' => 'control-label')) }}

            <?php

            if (isset($group)) {
                $tmpSmallPicFile = $group->getTemporarySmallFile();
                $tmpSmallPicName = $group->getTemporarySmallFileName();
            } else {
                $tmpSmallPicFile = (Input::old('new_picture', '')) ? asset(\Config::get('app.image_dir.groups_small') . Input::old('new_picture', '')) : '';
                $tmpSmallPicName = Input::old('new_picture', '');
            }

            ?>

            @include('site.profile.applicant.user_avatar', [
                 'fileFieldName'   => 'image',
                 'actionUrlUpload' => '/groups/upload',
                 'origImage'       => $tmpSmallPicFile ,
                 'origImageName'   => $tmpSmallPicName
             ])
            {{ $errors->first('image') }}
        </div>
    </div>

    @if(isset($group))
        <input type="hidden" name="id" value="{{$group->id}}" />
    @endif


<div class="form-group">
    <div class="col-md-12">
        <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
        <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
        <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
    </div>
</div>

</form>