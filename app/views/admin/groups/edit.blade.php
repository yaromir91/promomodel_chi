@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')


<ul class="nav nav-tabs">
    <li class="@if ($tab=='1')active @endif"><a href="#tab-general" data-toggle="tab">{{ Lang::get('admin/groups/titles.group_details') }}</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane @if ($tab=='1')active @endif" id="tab-general">
        @include('admin.groups.partials.general_form')
    </div>
</div>

@stop
