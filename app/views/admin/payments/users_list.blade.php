@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="col-md-2">{{ Lang::get('admin/payments/table.user_name') }}</th>
            <th class="col-md-2">{{ Lang::get('admin/payments/table.user_price') }}</th>
            <th class="col-md-2">{{ Lang::get('admin/payments/table.user_status') }}</th>
            <th class="col-md-3">{{ Lang::get('admin/payments/table.user_code') }}</th>
            <th class="col-md-3">{{ Lang::get('table.actions') }}</th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr class="odd">
                    <td>
                        {{ $user->username }}
                    </td>
                    <td>
                        {{ $user->price }}
                    </td>
                    <td id="status_{{ $user->id }}">
                        {{ $user->status ? Lang::get('admin/payments/titles.paid') : Lang::get('admin/payments/titles.not_paid') }}
                    </td>
                    <td>
                        {{ $user->payment_count }}
                    </td>
                    <td>
                        <input @if($user->status) disabled checked @endif class="changeStatus radio_{{ $user->id }}" id="{{ $user->id }}" type="radio"  name="pay" value="" />{{ Lang::get('admin/payments/titles.pay') }}<br />
                        <input @if($user->status) disabled @else checked @endif class="radio_{{ $user->id }}" type="radio" name="pay" value="" />{{ Lang::get('admin/payments/titles.not_pay') }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div id="_token" token="{{ csrf_token() }}"></div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.changeStatus').click(function(){
                var id = $(this).attr('id');
                $.ajax({
                    url: "{{ URL::to('admin/payments/paid') }}/" + id,
                    dataType: 'json',
                    data: {
                        _token: $('#_token').attr('token'),
                        id:     id
                    },
                    cache: false,
                    type: 'POST',
                    success: function (result) {
                        if(result.success) {
                            $('.radio_' + id).attr('disabled', true);
                            $('#' + id).attr('checked', true);
                        } else {
                            console.log('result: ', result);
                        }
                    }
                });
            });
        });
    </script>
@stop