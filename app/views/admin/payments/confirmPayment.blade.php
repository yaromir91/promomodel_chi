@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

    <div class="form-group">
        <div class="controls">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button id="Submit" type="submit" class="btn btn-danger">{{Lang::get('button.ok')}}</button>
        </div>
    </div>

    <div id="_token" token="{{ csrf_token() }}"></div>

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#Submit').click(function(){
                $.ajax({
                    url: '{{ URL::to('admin/payments/confirm') }}',
                    method: "POST",
                    data: {
                        requestDT: '{{ $date }}',
                        orderId: '{{ $event->id }}',
                        amount: '{{ $event->getTotalPriceWithServicePercent() }}',
                        currency: '{{ $event->currency }}',
                        _token: $('#_token').attr('token')
                    },
                    success: function( data ) {
                        console.log('data: ', data);
                        $('.close_popup').click();
                    },
                    error: function( data ) {
                        console.log('data: ', data);
                        $('.close_popup').click();
                    }
                });
            });
        });
    </script>
@stop

@stop