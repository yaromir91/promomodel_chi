@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')

    <div class="page-header">
        <h3>
            {{ $title }}
        </h3>
    </div>
    <div class="pull-right" style="margin-bottom: 15px;">
        <div class="color-block">
            <span style="background-color: {{ \Config::get('app.payment_color.end') }}" class="payment_color"></span>&nbsp;-&nbsp;{{Lang::get('admin/payments/titles.payment_color_end')}}
        </div>
        <div class="color-block">
            <span style="background-color: {{ \Config::get('app.payment_color.complete') }}" class="payment_color"></span>&nbsp;-&nbsp;{{Lang::get('admin/payments/titles.payment_color_complete')}}
        </div>
    </div>

    <div class="pull-left">
        <table id="payments" class="table table-striped table-hover">
            <thead>
            <tr>
                <th class="col-md-2">{{ Lang::get('admin/payments/table.title') }}</th>
                <th class="col-md-1">{{ Lang::get('admin/payments/table.date') }}</th>
                <th class="col-md-4">{{ Lang::get('admin/payments/table.paiment_status') }}</th>
                <th class="col-md-1">{{ Lang::get('admin/payments/table.total_price') }}</th>
                <th class="col-md-3">{{ Lang::get('admin/payments/table.total_payment_status') }}</th>
                <th class="col-md-1">{{ Lang::get('table.actions') }}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

@stop


{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        var oTable;
        $(document).ready(function() {
            oTable = $('#payments').dataTable( {
                "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sUrl": "/assets/Russian.json"
                },
                "bProcessing": true,
                "bServerSide": true,
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false },
                    { "bSortable": true },
                    { "bSortable": false },
                ],
                "sAjaxSource": "{{ URL::to('admin/payments/data') }}",
                "fnDrawCallback": function ( oSettings ) {
                    $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
                }
            });
        });
    </script>
@stop