<form class="form-horizontal" method="post" action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/edit') }}@endif" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->

<?php $oldInputT1 = Input::old(); $isOldTab1 = isset($oldInputT1['mainparams']) ? true : false; ?>

    <!-- Password -->
    <div class="form-group {{ $errors->has('new_password') ? 'error' : '' }}">
        <label class="col-md-2 control-label" for="new_password">Password</label>
        <div class="col-md-10">
            <input class="form-control" type="password" name="new_password" id="new_password" value="" />
            {{ $errors->first('new_password', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ password -->

    <!-- Password Confirm -->
    <div class="form-group {{ $errors->has('password_confirmation') ? 'error' : '' }}">
        <label class="col-md-2 control-label" for="password_confirmation">Password Confirm</label>
        <div class="col-md-10">
            <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="" />
            {{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ password confirm -->

    <input type="hidden" name="set_password" value="1" />

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
            <button type="submit" class="btn btn-success">OK</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>