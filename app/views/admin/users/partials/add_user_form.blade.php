<form method="POST" action="@if (isset($user)){{ URL::to('admin/users/create') }}@endif" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ Session::getToken() }}">
    <fieldset>
        <div class="form-group">
            <label for="account_type">{{ Lang::get('profile.account_type') }}</label>
            {{ Form::select('account_type', Acme\Models\Repositories\RoleRepository::getAvailableRolesForRegistration(Acme\Models\Profile\UserProfile::getAvailableRolesForAdminReg()), Input::old('account_type'), array('class' => 'form-control', 'id' => 'account_type'))}}
        </div>
        <div class="form-group">
            <label for="username">{{ Lang::get('confide::confide.username') }}</label>
            <input class="form-control" placeholder="{{ Lang::get('confide::confide.username') }}" type="text" name="username" id="username" value="{{ Input::old('username') }}">
        </div>
        <!-- Activation Status -->
        <div class="form-group {{ $errors->has('activated') || $errors->has('confirm') ? 'error' : '' }}">
            <label for="confirm">{{ Lang::get('admin/users/title.user_activate') }}?</label>
                <select class="form-control" name="confirm" id="confirm">
                    <option value="1"{{ (Input::old('confirm', 0) === 1 ? ' selected="selected"' : '') }}>{{ Lang::get('general.yes') }}</option>
                    <option value="0"{{ (Input::old('confirm', 0) === 0 ? ' selected="selected"' : '') }}>{{ Lang::get('general.no') }}</option>
                </select>
                {{ $errors->first('confirm', '<span class="help-inline">:message</span>') }}
        </div>
        <!-- ./ activation status -->
        <div class="form-group">
            <label for="last_name">{{ Lang::get('profile.LastName') }}</label>
            <input class="form-control" placeholder="{{ Lang::get('profile.LastName') }}" type="text" name="last_name" id="last_name" value="{{ Input::old('last_name') }}">
        </div>
        <div class="form-group">
            <label for="first_name">{{ Lang::get('profile.FirstName') }}</label>
            <input class="form-control" placeholder="{{ Lang::get('profile.FirstName') }}" type="text" name="first_name" id="first_name" value="{{ Input::old('first_name') }}">
        </div>
        <div class="form-group">
            <label for="gender">{{ Lang::get('profile.Gender') }}</label>
            {{ Form::select('gender', array('m' => Lang::get('profile.Male'), 'f' => Lang::get('profile.Female')), Input::old('gender'), array('class' => 'form-control', 'id' => 'gender'))}}
        </div>
        <div class="form-group">
            <label for="email">{{ Lang::get('confide::confide.e_mail') }}</label>
            <input class="form-control" placeholder="{{ Lang::get('confide::confide.e_mail') }}" type="text" name="email" id="email" value="{{ Input::old('email') }}">
        </div>
        <div class="form-group">
            <label for="password">{{ Lang::get('confide::confide.password') }}</label>
            <input class="form-control" placeholder="{{ Lang::get('confide::confide.password') }}" type="password" name="password" id="password">
        </div>
        <div class="form-group">
            <label for="password_confirmation">{{ Lang::get('confide::confide.password_confirmation') }}</label>
            <input class="form-control" placeholder="{{ Lang::get('confide::confide.password_confirmation') }}" type="password" name="password_confirmation" id="password_confirmation">
        </div>

        @if (Session::get('error'))
        <div class="alert alert-error alert-danger">
            @if (is_array(Session::get('error')))
            {{ head(Session::get('error')) }}
            @endif
        </div>
        @endif

        @if (Session::get('notice'))
        <div class="alert">{{ Session::get('notice') }}</div>
        @endif

        <div class="form-actions form-group">
            <button type="submit" class="btn btn-primary">{{ Lang::get('confide::confide.signup.submit') }}</button>
        </div>

    </fieldset>
</form>
