@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
  @include('admin.users.partials.add_user_form')
@stop