<form class="form-horizontal avatar-form" method="post" enctype="multipart/form-data" action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/edit') }}@endif" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->

    <?php $oldInputT1 = Input::old(); $isOldTab1 = isset($oldInputT1['mainparams']) ? true : false; ?>

    <!-- username -->
    <div class="form-group {{ $errors->has('username') ? 'error' : '' }}">
        <label class="col-md-2 control-label" for="username">{{Lang::get('profile.Username')}}</label>
        <div class="col-md-10">
            <input class="form-control" type="text" name="username" id="username" value="{{ Input::old('username', isset($user) ? $user->username : null) }}" />
            {{ $errors->first('username', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ username -->

    <!-- Email -->
    <div class="form-group {{ $errors->has('email') ? 'error' : '' }}">
        <label class="col-md-2 control-label" for="email">{{Lang::get('profile.Email')}}</label>
        <div class="col-md-10">
            <input class="form-control" type="text" name="email" id="email" value="{{ Input::old('email', isset($user) ? $user->email : null) }}" />
            {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ email -->

    <!-- Activation Status -->
    <div class="form-group {{ $errors->has('activated') || $errors->has('confirm') ? 'error' : '' }}">
        <label class="col-md-2 control-label" for="confirm">{{ Lang::get('admin/users/title.user_activate') }}?</label>
        <div class="col-md-6">
            @if ($mode == 'create')
            <select class="form-control" name="confirm" id="confirm">
                <option value="1"{{ (Input::old('confirm', 0) === 1 ? ' selected="selected"' : '') }}>{{ Lang::get('general.yes') }}</option>
                <option value="0"{{ (Input::old('confirm', 0) === 0 ? ' selected="selected"' : '') }}>{{ Lang::get('general.no') }}</option>
            </select>
            @else
            <select class="form-control" {{ ($user->id === Confide::user()->id ? ' disabled="disabled"' : '') }} name="confirm" id="confirm">
            <option value="1"{{ ($user->confirmed ? ' selected="selected"' : '') }}>{{ Lang::get('general.yes') }}</option>
            <option value="0"{{ ( ! $user->confirmed ? ' selected="selected"' : '') }}>{{ Lang::get('general.no') }}</option>
            </select>
            @endif
            {{ $errors->first('confirm', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ activation status -->

    <!-- Groups -->
    <div class="form-group {{ $errors->has('roles') ? 'error' : '' }}">
        <label class="col-md-2 control-label" for="roles">Roles</label>
        <div class="col-md-6">
            <select class="form-control" name="roles" id="roles">
                <option></option>
                @foreach ($roles as $role)
                @if ($mode == 'create')
                <option value="{{ $role->id }}"{{ ( in_array($role->id, $selectedRoles) ? ' selected="selected"' : '') }}>{{ $role->name }}</option>
                @else
                <option value="{{ $role->id }}"{{ ( array_search($role->id, $user->currentRoleIds()) !== false && array_search($role->id, $user->currentRoleIds()) >= 0 ? ' selected="selected"' : '') }}>{{ $role->name }}</option>
                @endif
                @endforeach
            </select>

            <span class="help-block">
                Select a group to assign to the user, remember that a user takes on the permissions of the group they are assigned.
            </span>
            {{ $errors->first('roles') }}
        </div>
    </div>
    <!-- ./ groups -->

    @if(($profile instanceof Acme\Models\Profile\UserProfile))

    <!-- Avatar -->
    <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
        {{ Form::label('avatar', Lang::get('admin/users/title.Avatar'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            @include('site.profile.applicant.user_avatar', [
                'fileFieldName'   => 'avatar',
                'origImage'       => $profile->getTemporaryFile(),
                'origImageName'   => $profile->getTemporaryFileName(),
                'actionUrlUpload' => '/profile/upload'
            ])
            <div id="_token" token="{{ csrf_token() }}"></div>
            {{ $errors->first('avatar') }}
        </div>
    </div>
    <!-- ./ avatar -->

    <!-- Last name -->
    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
        {{ Form::label('last_name', Lang::get('profile.LastName'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            {{ Form::text('last_name', Input::old('last_name', $profile->last_name), array('class' => 'form-control', 'id' => 'last_name')) }}
            {{ $errors->first('last_name') }}
        </div>
    </div>
    <!-- ./ Last name -->

    <!-- First name -->
    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
        {{ Form::label('first_name', Lang::get('profile.FirstName'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            {{ Form::text('first_name', Input::old('first_name', $profile->first_name), array('class' => 'form-control', 'id' => 'first_name')) }}
            {{ $errors->first('first_name') }}
        </div>
    </div>
    <!-- ./ First name -->

    <!-- Gender -->
    <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
        {{ Form::label('gender', Lang::get('profile.Gender'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            {{ Form::select('gender', array('m' => Lang::get('profile.Male'), 'f' => Lang::get('profile.Female')), Input::old('gender', $profile->gender), array('class' => 'form-control', 'id' => 'gender'))}}
            {{ $errors->first('gender') }}
        </div>
    </div>
    <!-- ./ Gender -->

    <!-- Payment count -->
    <div class="form-group {{ $errors->has('payment_count') ? 'has-error' : '' }}">
            {{ Form::label('payment_count', Lang::get('profile.payment_count'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            {{ Form::text('payment_count', Input::old('payment_count', $user->payment_count), array('class' => 'form-control', 'id' => 'payment_count')) }}
            {{ $errors->first('payment_count') }}
        </div>
    </div>
    <!-- ./ Payment count -->

    <!-- Date -->
    <div class="form-group {{ $errors->has('date_birth') ? 'has-error' : '' }}">
        {{ Form::label('date_birth', Lang::get('profile.DateBirth'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            <div class='input-group'>
                <input type="text" id="date_birth" value="@if($profile->date_birth != '0000-00-00') {{Input::old('date_birth', $profile->date_birth)}} @endif" name="date_birth" class="onlydatepicker form-control">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            {{ $errors->first('date_birth') }}
        </div>
    </div>
    <!-- ./ date -->

    <!-- Phone -->
    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
        {{ Form::label('phone', Lang::get('profile.phone'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            {{ Form::text('phone', Input::old('phone', $profile->phone), array('class' => 'form-control phone', 'id' => 'phone')) }}
            {{ $errors->first('phone') }}
        </div>
    </div>
    <!-- ./ Phone -->

    <!-- VK link -->
    <div class="form-group {{ $errors->has('vk_link') ? 'has-error' : '' }}">
        {{ Form::label('vk_link', Lang::get('profile.vk_link'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            {{ Form::text('vk_link', Input::old('vk_link', $profile->vk_link), array('class' => 'form-control', 'id' => 'vk_link')) }}
            {{ $errors->first('vk_link') }}
        </div>
    </div>
    <!-- ./ VK link -->

    <!-- Is Ready Manager -->
    <div class="form-group {{ $errors->has('is_ready_manager') ? 'has-error' : '' }}">
        {{ Form::label('is_ready_manager', Lang::get('profile.is_ready_manager'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            <?php
            $isCheckd = $isOldTab1 ? (isset($oldInputT1['is_ready_manager']) ? true : false) :  (($profile->is_ready_manager) ? true: false);
            ?>
            <input tabindex="1" type="checkbox" @if ($isCheckd) checked="checked" @endif id="is_ready_manager" value="{{$isCheckd}}" name="is_ready_manager" />
            {{ $errors->first('is_ready_manager') }}
        </div>
    </div>
    <!-- ./ Is Ready Manager -->

    <!-- Is public profile -->
    <div class="form-group {{ $errors->has('public') ? 'has-error' : '' }}">
        {{ Form::label('public', Lang::get('profile.public'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-md-10">
            <?php
            $isPublic = $isOldTab1 ? (isset($oldInputT1['public']) ? true : false) :  (($profile->public) ? true: false);
            ?>
            <input tabindex="1" type="checkbox" @if ($isPublic) checked="checked" @endif id="public" value="{{$isPublic}}" name="public" />
            {{ $errors->first('public') }}
        </div>
    </div>
    <!-- ./ Is public profile -->

    <input type="hidden" name="mainparams" value="1" />

    @endif

    <input type="hidden" name="mainparams" value="1" />

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
            <button type="submit" class="btn btn-success">OK</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>