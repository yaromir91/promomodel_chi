@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	<!-- Tabs -->
    <ul class="nav nav-tabs">
        <li class="@if ($tab=='1')active @endif"><a href="#tab-general" data-toggle="tab">{{ Lang::get('admin/users/title.General') }}</a></li>
        <li class="@if ($tab=='2')active @endif"><a href="#tab-profileparams" data-toggle="tab">{{ Lang::get('admin/users/title.additional') }}</a></li>
        <li class="@if ($tab=='3')active @endif"><a href="#tab-password" data-toggle="tab">{{ Lang::get('admin/users/title.password') }}</a></li>
    </ul>
	<!-- ./ tabs -->


		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane @if ($tab=='1')active @endif" id="tab-general">
                @include('admin.users.applicant.profile_params')
			</div>
			<!-- ./ general tab -->

            <!-- Additional tab -->
            <div class="tab-pane @if ($tab=='2')active @endif" id="tab-profileparams">
                    @include('admin.users.applicant.profileparams')
            </div>
            <!-- ./ Additional tab -->

            <!-- Password tab -->
            <div class="tab-pane @if ($tab=='3')active @endif" id="tab-password">
                    @include('admin.users.partials.password')
            </div>
            <!-- ./ Password tab -->

		</div>
		<!-- ./ tabs content -->

@stop
