<form class="form-horizontal" method="post"  action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/edit') }}@endif" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->
<!-- Event Types -->
<?php $oldInput = Input::old(); $isOldTab2 = isset($oldInput['additionparams']) ? true : false; ?>
<div class="form-group {{ $errors->has('eventtypes') ? 'has-error' : '' }}">
    {{ Form::label('eventtypes', Lang::get('profile.eventtypes'), array('class' => 'col-md-2 control-label')) }}
    <div class="col-md-10">
        @if (count($eventsType))
        @foreach ($eventsType as $type)
        <div class="checkbox">
            <?php
            $checkedET = false;
            if ($user->eventstype->contains($type->id)){
                $checkedET = true;
            }
            $checkedET = $isOldTab2 ? Input::old('eventtypes.'.$type->id, false) : $checkedET;
            ?>
            <label>
                <input tabindex="1" type="checkbox" @if ($checkedET) checked="checked" @endif name="eventtypes[{{ $type->id }}]" value="{{ $type->id }}">
                {{ $type->title }}
            </label>
        </div>
        @endforeach
        @endif
        {{ $errors->first('eventtypes') }}
    </div>
</div>
<!-- ./ Event Types -->


<!-- Professions -->
<div class="form-group {{ $errors->has('professions') ? 'has-error' : '' }}">
    {{ Form::label('professions', Lang::get('profile.professions_rates'), array('class' => 'col-md-2 control-label')) }}
    <div class="col-md-10">
        @if (count($professions))
        @foreach ($professions as $prof)
        <div class="checkbox">
            <?php
            $prh = '';
            $prd = '';
            if ($profile->professions->contains($prof->id))
            {
                $prh = $profile->professions->find($prof->id)->pivot->rate_h;
                $prd = $profile->professions->find($prof->id)->pivot->rate_d;
            }

            $prh = trim(Input::old('prof_rate_h.'.$prof->id, $prh));
            $prd = trim(Input::old('prof_rate_d.'.$prof->id, $prd));

            $checked = false;
            if ($profile->professions->contains($prof->id)){
                $checked = true;
            }
            $checked = $isOldTab2 ? Input::old('professions.'.$prof->id, false) : $checked;
            ?>
            <label>
                <input tabindex="1" type="checkbox" @if ($checked) checked="checked" @endif name="professions[{{ $prof->id }}]" value="{{ $prof->id }}">
                {{ $prof->title }}&nbsp;&nbsp;{{Lang::get('profile.rate')}}:
            </label>
            <input tabindex="2" type="text" name="prof_rate_h[{{ $prof->id }}]" value="{{$prh}}" placeholder="{{Lang::get('profile.rate_h')}}" />
            <input tabindex="3" type="text" name="prof_rate_d[{{ $prof->id }}]" value="{{$prd}}" placeholder="{{Lang::get('profile.rate_d')}}" />

        </div>
        @endforeach
        @endif
        {{ $errors->first('professions') }}
    </div>
</div>
<!-- ./ Professions -->


<!-- Profile params -->
@if (count($mainParams))
<p class="bg-danger">{{ $errors->first('profile_params') }}</p>
@foreach ($mainParams as $mParam)
<div class="form-group {{ $errors->has('profile_params') ? 'has-error' : '' }}">
    {{ Form::label('profile_params', $mParam->name, ['class' => 'col-md-2 control-label']) }}
    <div class="col-md-10">
        <?php $value = Input::old('profile_params.'.$mParam->id, $profile->getParamValue($mParam)); ?>
        @include($mParam->getTemplateByType(), ['param' => $mParam, 'name' => 'profile_params', 'value' => $value])
        <input type="hidden" name="valid_ruleArr[{{$mParam->id}}]" value="{{$mParam->validation_rule}}" />
        <input type="hidden" name="param_type[{{$mParam->id}}]" value="{{$mParam->type}}" />
        {{ $errors->first('profile_params_'.$mParam->id) }}
    </div>
</div>
@endforeach
@endif
<!-- ./ Profile params -->


<input type="hidden" name="additionparams" value="1" />

<!-- Form Actions -->
<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        {{--<button type="button" class="btn btn-default">{{Lang::get('button.cancel')}}</button>--}}
        {{--<button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>--}}
        <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
    </div>
</div>
<!-- ./ form actions -->
{{ Form::close() }}