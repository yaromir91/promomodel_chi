@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
    {{-- Delete Partner Form --}}
    <form id="deleteForm" class="form-horizontal" method="post" action="@if (isset($partner)){{ URL::to('admin/partners/' . $partner->id . '/delete') }}@endif" autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="id" value="{{ $partner->id }}" />
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="controls">
                <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
                <button type="submit" class="btn btn-danger ">{{Lang::get('button.delete')}}</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop