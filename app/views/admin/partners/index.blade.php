@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	{{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
            <h3>
                {{ $title }}
                
                <div class="pull-right">
                    <a href="{{ URL::to('admin/partners/create') }}" class="btn btn-small btn-info iframe">
                        <span class="glyphicon glyphicon-plus-sign"></span> {{ Lang::get('button.create') }}
                    </a>
                </div>
            </h3>
	</div>

	<table id="comments" class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-3">{{ Lang::get('admin/partners/table.title') }}</th>
				<th class="col-md-3">{{ Lang::get('admin/partners/table.url') }}</th>
				<th class="col-md-2">{{ Lang::get('admin/partners/table.email') }}</th>
				<th class="col-md-2">{{ Lang::get('admin/partners/table.active') }}</th>
                                <th class="col-md-2">{{ Lang::get('admin/partners/table.updated_at') }}</th>
				<th class="col-md-2">{{ Lang::get('table.actions') }}</th>
			</tr>
		</thead>
	</table>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#comments').dataTable( {
//				"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sUrl": "/assets/Russian.json"
				},
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{{ URL::to('admin/partners/data') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop