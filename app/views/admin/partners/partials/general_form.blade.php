{{-- Edit Param Form --}}
<form class="form-horizontal avatar-form" enctype="multipart/form-data" method="post" action="@if (isset($partners)){{ URL::to('admin/partners/' . $partners->id . '/edit') }} @else {{ URL::to('admin/partners/create') }} @endif" autocomplete="off">

    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->

    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="title">{{ Lang::get('admin/partners/table.title') }}</label>
            <input class="form-control" maxlength="70" type="text" name="title" id="title" value="{{ Input::old('title', isset($partners) ? $partners->title : null) }}" />
            {{ $errors->first('title', '<span class="help-block">:message</span>') }}
        </div>
    </div>

    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="description">{{ Lang::get('admin/partners/form.description') }}</label>
            {{ Form::textarea('description', Input::old('description', isset($partners) ? $partners->description : null), array('class' => 'form-control', 'id' => 'description')) }}
            {{ $errors->first('description', '<span class="help-block">:message</span>') }}
        </div>
    </div>

    <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
        <div class="col-md-12">
            {{ Form::label('logo', Lang::get('admin/partners/form.logo'), array('class' => 'control-label')) }}

            <?php

            if (isset($partners)) {
                $tmpSmallPicFile = $partners->getTemporarySmallFile();
                $tmpSmallPicName = $partners->getTemporarySmallFileName();
            } else {
                $tmpSmallPicFile = (Input::old('new_picture', '')) ? asset(\Config::get('app.image_dir.partners_small') . Input::old('new_picture', '')) : '';
                $tmpSmallPicName = Input::old('new_picture', '');
            }
            
            ?>

            @include('site.profile.applicant.user_avatar', [
                 'fileFieldName'   => 'logo',
                 'actionUrlUpload' => '/partner/upload',
                 'origImage'       => $tmpSmallPicFile ,
                 'origImageName'   => $tmpSmallPicName
             ])
            {{ $errors->first('logo') }}
        </div>
    </div>
    
    <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="url">{{ Lang::get('admin/partners/table.url') }}</label>
            <input class="form-control" maxlength="70" type="text" name="url" id="title" value="{{ Input::old('url', isset($partners) ? $partners->url : null) }}" />
            {{ $errors->first('url', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    
    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="email">{{ Lang::get('admin/partners/table.email') }}</label>
            <input class="form-control" maxlength="70" type="text" name="email" id="title" value="{{ Input::old('email', isset($partners) ? $partners->email : null) }}" />
            {{ $errors->first('email', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    
    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="phone">{{ Lang::get('admin/partners/table.phone') }}</label>
            <input class="form-control" maxlength="70" type="text" name="phone" id="title" value="{{ Input::old('phone', isset($partners) ? $partners->phone : null) }}" />
            {{ $errors->first('phone', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    
    <div class="form-group {{ $errors->has('adress') ? 'has-error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="adress">{{ Lang::get('admin/partners/form.adress') }}</label>
            {{ Form::textarea('adress', Input::old('adress', isset($partners) ? $partners->adress : null), array('class' => 'form-control', 'id' => 'adress')) }}
            {{ $errors->first('adress', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    
<!-- Active -->
<div class="form-group {{ $errors->has('casting') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3 col-sm-4">
        {{ Form::label('casting', Lang::get('admin/partners/form.active'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6 col-sm-8">
        <?php
        $oldInput = Input::old();
        $active1 = isset($oldInput['active']) ? ($oldInput['active']=='1') : (isset($partners) ? ($partners->active == '1') : true);
        $active0 = isset($oldInput['active']) ? ($oldInput['active']=='0') : (isset($partners) ? ($partners->active == '0') : false);
        ?>
        <div class="radio">
            <label>
                {{ Form::radio('active', '1', $active1) }}
                {{ Lang::get('admin/partners/form.active_yes') }}
            </label>
        </div>
        <div class="radio">
            <label>
                {{ Form::radio('active', '0', $active0) }}
                {{ Lang::get('admin/partners/form.active_no') }}
            </label>
        </div>
        {{ $errors->first('active', '<span class="help-block">:message</span>') }}
    </div>
</div>
<!-- ./ active -->

    @if(isset($partners))
        <input type="hidden" name="id" value="{{$partners->id}}" />
    @endif


<div class="form-group">
    <div class="col-md-12">
        <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
        <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
        <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
    </div>
</div>

</form>