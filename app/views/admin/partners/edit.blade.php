@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

<!-- Tabs Content -->
<div class="tab-content">
    <div class="tab-pane active" id="tab-general">
        @include('admin.partners.partials.general_form')
    </div>
</div>
<!-- ./ tabs content -->

@stop