@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
<!-- Tabs -->
<ul class="nav nav-tabs">
    <li class="@if ($tab=='1')active @endif"><a href="#tab-general" data-toggle="tab">{{ Lang::get('admin/params/titles.General') }}</a></li>
</ul>
<!-- ./ tabs -->


<!-- Tabs Content -->
<div class="tab-content">
    <!-- General tab -->
    <div class="tab-pane @if ($tab=='1')active @endif" id="tab-general">
        @include('admin.params.partials.new_param_form')
    </div>
    <!-- ./ general tab -->

</div>
<!-- ./ tabs content -->

@stop