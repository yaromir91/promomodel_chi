{{-- Edit Param Form --}}
<form class="form-horizontal" method="post" action="{{ URL::to('admin/params/create') }}" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->

    <!-- Post Title -->
    <div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="name">{{ Lang::get('admin/params/titles.name') }}</label>
            <input class="form-control" type="text" name="name" id="name" value="{{ Input::old('name') }}" />
            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ post title -->

    <!-- Post Title -->
    <div class="form-group {{ $errors->has('name_en') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="name">{{ Lang::get('admin/params/titles.name_en') }}</label>
            <input class="form-control" type="text" name="name_en" id="name_en" value="{{ Input::old('name_en') }}" />
            {{ $errors->first('name_en', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ post title -->

    <!-- Content -->
    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="type">{{ Lang::get('admin/params/titles.type') }}</label>
            {{ Form::select('type', Acme\Models\Profile\ProfileParams::getAllTypeNames(($parent && $parent->id) ? $parent->id : null), Input::old('type'), array('class' => 'form-control', 'id' => 'type'))}}
            {{ $errors->first('type', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ content -->

    @if (isset($mode) && ($mode='create') && ($parent && $parent->id))
    <input type="hidden" name="parent_id" value="{{$parent->id}}" />
    @endif

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-12">
            <button class="btn btn-default  btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
            <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>