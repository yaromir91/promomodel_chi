<div class="page-header">
    <h3>
        {{ $title }}

        <div class="pull-right">
            <a href="{{ URL::to('admin/params/create/'.$param->id) }}" class="btn btn-small btn-info iframe"><span class="glyphicon glyphicon-plus-sign"></span> Создать</a>
        </div>
    </h3>
</div>

<table id="params-child" class="table table-striped table-hover">
    <thead>
    <tr>
        <th class="col-md-2">ID</th>
        <th class="col-md-4">{{ Lang::get('admin/params/titles.name') }}</th>
        <th class="col-md-4">{{ Lang::get('admin/params/titles.name_en') }}</th>
        <th class="col-md-2">Тип</th>
        <th class="col-md-2">{{ Lang::get('table.actions') }}</th>
    </tr>
    </thead>
    <tbody class="table-params-data">
    </tbody>
</table>

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable = $('#params-child').dataTable( {
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sUrl": "/assets/Russian.json"
            },
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "{{ URL::to('admin/params/'.$param->id.'/datachild') }}",
            "fnDrawCallback": function ( oSettings ) {
                $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
            }
        });
    });
</script>
@stop