{{-- Edit Param Form --}}
<form class="form-horizontal" method="post" action="@if (isset($param)){{ URL::to('admin/params/' . $param->id . '/edit') }}@endif" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->

    <!-- Post Title -->
    <div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="name">{{ Lang::get('admin/params/titles.name') }}</label>
            <input class="form-control" type="text" name="name" id="name" value="{{ Input::old('name', isset($param) ? $param->name : null) }}" />
            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ post title -->
    <!-- Post Title -->
    <div class="form-group {{ $errors->has('name_en') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="name_en">{{ Lang::get('admin/params/titles.name_en') }}</label>
            <input class="form-control" type="text" name="name_en" id="name_en" value="{{ Input::old('name_en', isset($param) ? $param->name_en : null) }}" />
            {{ $errors->first('name_en', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ post title -->

    <!-- Content -->
    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="type">{{ Lang::get('admin/params/titles.type') }}</label>
            {{ Form::select('type', Acme\Models\Profile\ProfileParams::getAllTypeNames($param->parent_id), Input::old('type', isset($param) ? $param->type : null), array('class' => 'form-control params-types', 'id' => 'type'))}}
            {{ $errors->first('type', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ content -->

    {{ Form::select('type_to_select', Acme\Models\Profile\ProfileParams::getTypesForSelect(), '', array('class' => 'hidden params-type-to-select'))}}

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-12">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
            <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>