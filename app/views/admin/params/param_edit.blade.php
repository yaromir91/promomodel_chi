@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	<!-- Tabs -->
    <ul class="nav nav-tabs">
        <li class="@if ($tab=='1')active @endif"><a href="#tab-general" data-toggle="tab">{{ Lang::get('admin/params/titles.General') }}</a></li>
        <li class="childrens-el @if ($tab=='2')active @endif @if ($param && !in_array($param->type, Acme\Models\Profile\ProfileParams::getTypesForSelect()))hidden @endif"><a href="#tab-childrens" data-toggle="tab">{{ Lang::get('admin/params/titles.child_elements') }}</a></li>
    </ul>
	<!-- ./ tabs -->


		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane @if ($tab=='1')active @endif" id="tab-general">
                @include('admin.params.partials.general_form')
			</div>
			<!-- ./ general tab -->

			<!-- Child Elements tab -->
            <div class="tab-pane @if ($tab=='2')active @endif" id="tab-childrens">
                @include('admin.params.partials.child_elements_form')
            </div>
			<!-- ./ Child Elements tab -->
		</div>
		<!-- ./ tabs content -->

@stop
