@if (isset($comments))
    <div class="page-header clearfix">
        <h3 class="col-md-10">
            {{ $title_comments }}
        </h3>
        <div class="pull-right">
            <a href="{{ URL::to('admin/reports/comments/create/' . $report->id) }}" class="btn btn-small btn-info iframe">
                </span> {{ Lang::get('button.create') }}
            </a>
        </div>
    </div>

    <table id="report-comments" class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="col-md-4">{{ Lang::get('admin/reports/table.username') }}</th>
            <th class="col-md-4">{{ Lang::get('admin/reports/table.comments') }}</th>
            <th class="col-md-4">{{ Lang::get('admin/reports/table.update') }}</th>
            <th class="col-md-2">{{ Lang::get('table.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endif

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable = $('#report-comments').dataTable( {
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sUrl": "/assets/Russian.json"
            },
            "bProcessing": true,
            "bServerSide": true,
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false }
            ],
            "sAjaxSource": "{{ URL::to('admin/reports/comments/' . $report->id) }}",
            "fnDrawCallback": function ( oSettings ) {
                $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
            }
        });
    });
</script>
@stop
