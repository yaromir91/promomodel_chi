{{-- Edit Param Form --}}
{{ Form::open(array('url' => 'admin/reports/edit/' . $report->id, 'method' => 'post', 'autocomplete' => 'off', 'files' => 'true', 'class' => 'form-horizontal avatar-form')) }}
<!-- General tab -->
<div class="tab-pane active" id="tab-general">
    <!-- events -->
    <div class="form-group {{ $errors->has('events') ? 'has-error' : '' }}">
        {{ Form::label('events', Lang::get('report.form_event'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-lg-5">
            {{ Form::text('events', $event->title, array('class' => 'form-control', 'id' => 'events', 'disabled')) }}
            {{ $errors->first('events') }}
        </div>
    </div>
    <!-- ./ events -->
    <!-- Title -->
    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
        {{ Form::label('title', Lang::get('report.form_title'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-lg-5">
            {{ Form::text('title', (Input::old('title')) ? Input::old('title') : $report->title, array('class' => 'form-control', 'id' => 'title')) }}
            {{ $errors->first('title') }}
        </div>
    </div>
    <!-- ./ Title -->
    <!-- Description -->
    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
        {{ Form::label('content', Lang::get('report.form_content'), array('class' => 'col-lg-2 control-label')) }}
        <div class="col-lg-5">
            {{ Form::textarea('content', (Input::old('content')) ? Input::old('content') : $report->content, array('class' => 'form-control', 'id' => 'content')) }}
            {{ $errors->first('content') }}
        </div>
    </div>
    <!-- ./ Description -->

    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
        {{ Form::label('image', Lang::get('report.image'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-lg-5">

            @include('site.profile.applicant.user_avatar', [
                 'fileFieldName'   => 'image',
                 'actionUrlUpload' => '/report/upload',
                 'origImage'       => $report->getTemporarySmallFile() ,
                 'origImageName'   => $report->getTemporarySmallFileName()
             ])

            {{ $errors->first('image') }}

        </div>
    </div>

    <!-- Tags -->
    <!-- ./ Tags -->
    <!-- Status -->
    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
        {{ Form::label('status', Lang::get('report.form_status'), array('class' => 'col-lg-2 control-label')) }}
        <div class="col-lg-5">
            {{ Form::checkbox('status', Input::old('status'), ($report->status)?true:false, array('class' => 'form-control', 'id' => 'status')) }}
            {{ $errors->first('status') }}
        </div>
    </div>
    <!-- ./ Status -->
    <!-- Form Actions -->
    <div class="form-group">
        <div class="collg-offset-2 col-lg-5">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->
    {{ Form::close() }}
</div>