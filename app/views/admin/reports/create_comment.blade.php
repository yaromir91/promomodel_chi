@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

    {{ Form::open(array('url' => 'admin/reports/comments/create', 'method' => 'post', 'autocomplete' => 'off', 'class' => 'form-horizontal')) }}

    {{ Form::hidden('post_id',  $report->id) }}
    <!-- Title -->
    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
        {{ Form::label('title', Lang::get('admin/reports/form.title_content'), array('class' => 'col-md-2 control-label')) }}
        <div class="col-lg-4">
            {{ Form::textarea('content', (Input::old('content')) ? Input::old('content') : '', array('id' => 'content', 'cols' => 80, 'rows' => 6)) }}
            {{ $errors->first('content') }}
        </div>
    </div>
    <!-- ./ Title -->

    <!-- Form Actions -->
    <div class="form-group">
        <div class="collg-offset-2 col-lg-5">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->

    {{ Form::close() }}


@stop