@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

    {{ Form::open(array('url' => 'admin/reports/delete/' . $report->id, 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'deleteForm')) }}

        {{ Form::hidden('id',  $report->id) }}

        <!-- Form Actions -->
    <div class="form-group">
        <div class="controls">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="submit" class="btn btn-danger">{{Lang::get('button.delete')}}</button>
        </div>
    </div>
        <!-- ./ form actions -->
    {{ Form::close() }}
@stop