@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

<!-- Tabs Content -->
<div class="content">


    <!-- Tabs -->
    <ul class="nav nav-tabs">
        <li class="@if ($tab=='1')active @endif">
            <a href="#tab-general" data-toggle="tab">{{ Lang::get('admin/reports/form.report_details') }}</a>
        </li>
        @if (isset($comments))
            <li class="childrens-el @if ($tab=='2')active @endif">
                <a href="#tab-childrens" data-toggle="tab">{{ Lang::get('admin/reports/form.comments') }}</a>
            </li>
        @endif
    </ul>
    <!-- ./ tabs -->


    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane @if ($tab=='1')active @endif" id="tab-general">
            @include('admin.reports.partials.general_form')
        </div>
        <!-- ./ general tab -->

        @if (isset($comments))
            <!-- Child Elements tab -->
            <div class="tab-pane @if ($tab=='2')active @endif" id="tab-childrens">
                @include('admin.reports.partials.comments_form')
            </div>
            <!-- ./ Child Elements tab -->
        @endif
    </div>
    <!-- ./ tabs content -->

@stop