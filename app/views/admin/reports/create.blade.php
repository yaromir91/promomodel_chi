@extends('admin.layouts.modal')


{{-- Content --}}
@section('content')
        <!-- General tab -->
  <div class="tab-pane active" id="tab-general">
    {{-- Create User Form --}}
    {{ Form::open(array('url' => 'admin/reports/create', 'method' => 'post', 'autocomplete' => 'off', 'files' => 'true', 'class' => 'form-horizontal avatar-form')) }}


        <!-- events -->
        <div class="form-group {{ $errors->has('events') ? 'has-error' : '' }}">
            {{ Form::label('events', Lang::get('report.form_events'), array('class' => 'col-md-2 control-label')) }}
            <div class="col-lg-5">
                {{ Form::select('events', $events, '', array('class' => 'form-control', 'id' => 'events')) }}
                {{ $errors->first('events') }}
            </div>
        </div>
        <!-- ./ events -->
        <!-- Title -->
        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
            {{ Form::label('title', Lang::get('report.form_title'), array('class' => 'col-md-2 control-label')) }}
            <div class="col-lg-5">
                {{ Form::text('title', Input::old('title'), array('class' => 'form-control', 'id' => 'title')) }}
                {{ $errors->first('title') }}
            </div>
        </div>
        <!-- ./ Title -->
        <!-- Description -->
        <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
            {{ Form::label('content', Lang::get('report.form_content'), array('class' => 'col-lg-2 control-label')) }}
            <div class="col-lg-5">
                {{ Form::textarea('content', Input::old('content'), array('class' => 'form-control', 'id' => 'content')) }}
                {{ $errors->first('content') }}
            </div>
        </div>
        <!-- ./ Description -->
        <!-- Image -->
        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
            {{ Form::label('image', Lang::get('report.image'), array('class' => 'col-md-2 control-label')) }}
            <div class="col-lg-5">


                <?php

                    $tmpSmallPicFile = (Input::old('new_picture', '')) ? asset(\Config::get('app.image_dir.posts_small') . Input::old('new_picture', '')) : '';
                    $tmpSmallPicName = Input::old('new_picture', '');

                ?>

                @include('site.profile.applicant.user_avatar', [
                     'fileFieldName'   => 'image',
                     'actionUrlUpload' => '/report/upload',
                     'origImage'       => $tmpSmallPicFile ,
                     'origImageName'   => $tmpSmallPicName
                 ])
                {{ $errors->first('image') }}
            </div>
        </div>
        <!-- ./ Image -->
        <!-- Tags -->
        <!-- ./ Tags -->
        <!-- Status -->
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            {{ Form::label('status', Lang::get('report.form_status'), array('class' => 'col-lg-2 control-label')) }}
            <div class="col-lg-5">
                {{ Form::checkbox('status', Input::old('status'), true, array('class' => 'form-control', 'id' => 'status')) }}
                {{ $errors->first('status') }}
            </div>
        </div>
        <!-- ./ Status -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="collg-offset-2 col-lg-5">
                <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
                <button type="submit" class="btn btn-success">{{Lang::get('button.add')}}</button>
            </div>
        </div>
        <!-- ./ form actions -->
        {{ Form::close() }}
</div>
@stop
