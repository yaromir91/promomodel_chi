@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
    <h3>
        {{ $title }}

        <div class="pull-right">
            <a href="{{ URL::to('admin/reports/create') }}" class="btn btn-small btn-info iframe">
                </span> {{ Lang::get('button.create') }}
            </a>
        </div>
    </h3>
</div>

<table id="report" class="table table-striped table-hover">
    <thead>
    <tr>
        <th class="col-md-2">{{ Lang::get('admin/reports/table.title') }}</th>
        <th class="col-md-2">{{ Lang::get('admin/reports/table.user') }}</th>
        <th class="col-md-2">{{ Lang::get('admin/reports/table.event') }}</th>
        <th class="col-md-2">{{ Lang::get('admin/reports/table.image') }}</th>
        <th class="col-md-2">{{ Lang::get('admin/reports/table.status') }}</th>
        <th class="col-md-2">{{ Lang::get('admin/reports/table.created_at') }}</th>
        <th class="col-md-2">{{ Lang::get('table.actions') }}</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop


{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable = $('#report').dataTable( {
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sUrl": "/assets/Russian.json"
            },
            "bProcessing": true,
            "bServerSide": true,
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }
            ],
            "sAjaxSource": "{{ URL::to('admin/reports/data') }}",
            "fnDrawCallback": function ( oSettings ) {
                $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
            }
        });
    });
</script>
@stop