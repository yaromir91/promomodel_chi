@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')

    <div class="page-header">
        <h3>
            {{ $title }}
        </h3>
    </div>

    <table id="agents" class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="col-md-4">{{ Lang::get('admin/agents/title.name') }}</th>
            <th class="col-md-4">{{ Lang::get('admin/agents/title.email') }}</th>
            <th class="col-md-4">{{ Lang::get('admin/agents/title.status') }}</th>
            <th class="col-md-2">{{ Lang::get('table.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@stop


{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        var oTable;
        $(document).ready(function() {
            oTable = $('#agents').dataTable( {
                "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sUrl": "/assets/Russian.json"
                },
                "bProcessing": true,
                "bServerSide": true,
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": true },
                    { "bSortable": false }
                ],
                "sAjaxSource": "{{ URL::to('admin/agents/data') }}",
                "fnDrawCallback": function ( oSettings ) {
                    $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
                }
            });
        });
    </script>
@stop