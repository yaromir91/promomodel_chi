@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

        {{ Form::open(array('class' => 'form-horizontal', 'id' => 'activateForm', 'autocomplete' => 'off', 'url' => URL::to('admin/agents/' . $user->id . '/activate'))) }}

        <input type="hidden" name="id" value="{{ $user->id }}" />


        <!-- Form Actions -->
        <div class="form-group">
            <div class="controls">
                <button class="btn btn-default btn-cancel close_popup">Отмена</button>
                <button type="submit" class="btn btn-danger">Активировать</button>
            </div>
        </div>
        <!-- ./ form actions -->
    {{ Form::close() }}
@stop