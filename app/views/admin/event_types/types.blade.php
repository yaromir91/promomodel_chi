@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
    <h3>
        {{ $title }}

        <div class="pull-right">
            <a href="{{ URL::to('admin/events-type/create') }}" class="btn btn-small btn-info iframe">
                <span class="glyphicon glyphicon-plus-sign"></span> {{ Lang::get('button.create') }}</a>
        </div>
    </h3>
</div>

<table id="event-types" class="table table-striped table-hover">
    <thead>
    <tr>
        <th class="col-md-4">{{ Lang::get('general.name') }}</th>
        <th class="col-md-4">{{ Lang::get('general.name_en') }}</th>
        <th class="col-md-2">{{ Lang::get('table.actions') }}</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop


{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable = $('#event-types').dataTable( {
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sUrl": "/assets/Russian.json"
            },
            "bProcessing": true,
            "bServerSide": true,
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": false },
                { "bSortable": false }
            ],
            "sAjaxSource": "{{ URL::to('admin/events-type/data') }}",
            "fnDrawCallback": function ( oSettings ) {
                $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
            }
        });
    });
</script>
@stop