<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>
		@section('title')
			Administration
		@show
	</title>

	<meta name="keywords" content="@yield('keywords')" />
	<meta name="author" content="@yield('author')" />
	<!-- Google will often use this as its description of your page/site. Make it good. -->
	<meta name="description" content="@yield('description')" />

	<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="">

	<!-- Dublin Core Metadata : http://dublincore.org/ -->
	<meta name="DC.title" content="Project Name">
	<meta name="DC.subject" content="@yield('description')">
	<meta name="DC.creator" content="@yield('author')">

	<!--  Mobile Viewport Fix -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- This is the traditional favicon.
	 - size: 16x16 or 32x32
	 - transparency is OK
	 - see wikipedia for info on browser support: http://mky.be/favicon/ -->
	<link rel="shortcut icon" href="{{ asset('assets/ico/favicon.png') }}">

	<!-- iOS favicons. -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">

	<!-- CSS -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/prettify.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/bootstrap-wysihtml5.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/datatables-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colorbox.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/admin/style.css')}}">

	<style>
	body {
		padding: 60px 0;
	}
	</style>

	@yield('styles')

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body>
	<!-- Container -->
	<div class="container">
		<!-- Navbar -->
		<div class="navbar navbar-default navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav">
                            <li class="dropdown{{ (Request::is('admin/users*') || Request::is('admin/roles*') || Request::is('admin/params*') || Request::is('admin/profession*') || Request::is('admin/agents*') || Request::is('admin/partners*') ? ' active' : '') }}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin/users') }}">
                                    <span class="glyphicon glyphicon-user"></span> Пользователи <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{{ (Request::is('admin/users*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/users') }}"><span class="glyphicon glyphicon-user"></span> Пользователи</a></li>
                                    <li{{ (Request::is('admin/roles*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/roles') }}"><span class="glyphicon glyphicon-exclamation-sign"></span> Роли</a></li>
                                    <li{{ (Request::is('admin/params*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/params') }}"><span class="glyphicon glyphicon-th-large"></span> Параметры</a></li>
                                    <li{{ (Request::is('admin/profession*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/profession') }}"><span class="glyphicon glyphicon-briefcase"></span> Профессии</a></li>
                                    <li{{ (Request::is('admin/agents*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/agents') }}"><span class="glyphicon glyphicon-user"></span> Агенты</a></li>
                                    <li{{ (Request::is('admin/partners*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/partners') }}"><span class="glyphicon glyphicon-user"></span> {{ Lang::get('admin/partners/title.partners') }}</a></li>
                                </ul>
                            </li>
                            <li class="dropdown{{ (Request::is('admin/events*') || Request::is('admin/events-type/*') || Request::is('admin/payments*') ? ' active' : '') }}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin/events-type') }}">
                                    <span class="glyphicon glyphicon-th-large"></span> {{ Lang::get('admin/events/titles.events') }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{{ (Request::is('admin/events/?*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/events') }}"><span class="glyphicon glyphicon-tag"></span> {{ Lang::get('admin/events/titles.events_list') }}</a></li>
                                    <li{{ (Request::is('admin/events-type*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/events-type') }}"><span class="glyphicon glyphicon-th-list"></span> {{ Lang::get('admin/events/titles.event_types') }}</a></li>
                                    <li{{ (Request::is('admin/payments*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/payments') }}"><span class="glyphicon glyphicon-th-list"></span> {{ Lang::get('admin/payments/titles.payments') }}</a></li>
                                </ul>
                            </li>
                            <li class="dropdown{{ (Request::is('admin/gallery*') ? ' active' : '') }}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin/gallery') }}">
                                    <span class="glyphicon glyphicon-camera"></span> {{ Lang::get('admin/gallery/titles.gallery') }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{{ (Request::is('admin/gallery*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/gallery') }}"><span class="glyphicon glyphicon-th-list"></span> {{ Lang::get('admin/gallery/titles.gallery') }}</a></li>
                                </ul>
                            </li>
                            <li class="dropdown{{ (Request::is('admin/reports*') ? ' active' : '') }}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin/reports') }}">
                                    <span class="glyphicon glyphicon-list-alt"></span> {{ Lang::get('admin/reports/titles.reports') }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{{ (Request::is('admin/reports*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/reports') }}"><span class="glyphicon glyphicon-th-list"></span> {{ Lang::get('admin/reports/titles.reports') }}</a></li>
                                </ul>
                            </li>
                            <li class="dropdown{{ (Request::is('admin/groups*') ? ' active' : '') }}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin/groups') }}">
                                    <span class="glyphicon glyphicon-list-alt"></span> {{ Lang::get('admin/groups/titles.groups') }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li{{ (Request::is('admin/groups*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/groups') }}"><span class="glyphicon glyphicon-th-list"></span> {{ Lang::get('admin/groups/titles.groups') }}</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav pull-right">
                            <li><a href="{{ URL::to('/') }}">Вернуться на главную</a></li>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="glyphicon glyphicon-user"></span> {{ Auth::user()->username }}	<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to('user/logout') }}"><span class="glyphicon glyphicon-share"></span> Выйти</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
            </div>
		</div>
		<!-- ./ navbar -->

		<!-- Notifications -->
		@include('notifications')
		<!-- ./ notifications -->

		<!-- Content -->
		@yield('content')
		<!-- ./ content -->

		<!-- Footer -->
		<footer class="clearfix">
			@yield('footer')
		</footer>
		<!-- ./ Footer -->

	</div>
	<!-- ./ container -->

	<!-- Javascripts -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/wysihtml5/wysihtml5-0.3.0.js')}}"></script>
    <script src="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables-bootstrap.js')}}"></script>
    <script src="{{asset('assets/js/datatables.fnReloadAjax.js')}}"></script>
    <script src="{{asset('assets/js/jquery.colorbox.js')}}"></script>
    <script src="{{asset('assets/js/prettify.js')}}"></script>

    @if(Request::is('admin/params*'))
    <script type="text/javascript" src="{{ asset('assets/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/admin/params_list.js') }}"></script>
    @endif

    <script type="text/javascript">
    	$('.wysihtml5').wysihtml5();
        $(prettyPrint);
    </script>

    @yield('scripts')

</body>

</html>
