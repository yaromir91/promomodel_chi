@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')


{{-- Delete Post Form --}}
<form id="deleteForm" class="form-horizontal" method="post" action="@if (isset($profession)){{ URL::to('admin/profession/' . $profession->id . '/delete') }}@endif" autocomplete="off">

    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="id" value="{{ $profession->id }}" />
    <!-- ./ csrf token -->

    <!-- Form Actions -->
    <div class="form-group">
        <div class="controls">
            {{Lang::get('admin/profession/titles.delete_profession')}}?
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="submit" class="btn btn-danger">{{Lang::get('button.delete')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>
@stop