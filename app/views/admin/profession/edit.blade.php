@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')


{{-- Create Profession Form --}}
<form class="form-horizontal" method="post" action="{{ URL::to('admin/profession/'.$profession->id.'/edit') }}" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- Tab General -->
        <div class="tab-pane active" id="tab-general">
            <!-- Name -->
            <div class="form-group {{ $errors->has('title') ? 'error' : '' }}">
                <label class="col-md-2 control-label" for="title">{{ Lang::get('general.name') }}</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="title" id="title" value="{{ Input::old('title', $profession->title) }}" />
                    {{ $errors->first('title', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <div class="form-group {{ $errors->has('title_en') ? 'error' : '' }}">
                <label class="col-md-2 control-label" for="title_en">{{ Lang::get('general.name_en') }}</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" name="title_en" id="title_en" value="{{ Input::old('title_en', $profession->title_en) }}" />
                    {{ $errors->first('title_en', '<span class="help-inline">:message</span>') }}
                </div>
            </div>
            <!-- ./ name -->
        </div>
        <!-- ./ tab general -->

    </div>
    <!-- ./ tabs content -->

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
            <button type="submit" class="btn btn-success">{{Lang::get('button.update')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>
@stop
