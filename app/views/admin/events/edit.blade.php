@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')


<!-- Tabs -->
<ul class="nav nav-tabs">
    <li class="@if ($tab=='1')active @endif"><a href="#tab-general" data-toggle="tab">{{ Lang::get('admin/events/titles.event_details') }}</a></li>
    @if (isset($event))
        <li class="childrens-el @if ($tab=='2')active @endif"><a href="#tab-childrens" data-toggle="tab">{{ Lang::get('admin/events/titles.search') }}</a></li>
            @if (isset($isSearch) && $isSearch)
                <li class="childrens-el @if ($tab=='3')active @endif"><a href="#tab-seached" data-toggle="tab">{{ Lang::get('admin/events/titles.search_results') }}</a></li>
            @endif
        <li class="childrens-el"><a href="#tab-event-team" data-toggle="tab">{{ Lang::get('admin/events/titles.team') }}</a></li>
    @endif
</ul>
<!-- ./ tabs -->

<!-- Tabs Content -->
<div class="tab-content">
    <!-- General tab -->
    <div class="tab-pane @if ($tab=='1')active @endif" id="tab-general">
        @include('admin.events.partials.general_form')
    </div>
    <!-- ./ general tab -->

    @if (isset($event))
    <!-- Child Elements tab -->
    <div class="tab-pane @if ($tab=='2')active @endif" id="tab-childrens">
        @include('admin.events.partials.search')
    </div>
    <!-- ./ Child Elements tab -->

    @if (isset($isSearch) && $isSearch)
        <!-- Search results tab -->
        <div class="tab-pane @if ($tab=='3')active @endif" id="tab-seached">
            @include('admin.events.partials.search_results')
        </div>
        <!-- ./ Search results tab -->
    @endif

    <div class="tab-pane" id="tab-event-team">
        @include('admin.events.partials.event_team')
    </div>

    @endif
</div>
<!-- ./ tabs content -->

@stop
