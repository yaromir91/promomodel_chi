@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

<form id="deleteForm" class="form-horizontal" method="post" action="@if (isset($event)){{ URL::to('admin/events/' . $event->id . '/delete') }}@endif" autocomplete="off">


    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="id" value="{{ $event->id }}" />

    <div class="form-group">
        <div class="controls">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="submit" class="btn btn-danger">{{Lang::get('button.delete')}}</button>
        </div>
    </div>
</form>
@stop