{{ Form::open(array('url' => 'admin/events/' . $event->id .'/edit', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'candidatSearchForm')) }}

<p class="bg-danger">{{ $errors->first('search_params') }}</p>
@foreach ($params as $mParam)
<div class="form-group {{ $errors->has('search_params') ? 'has-error' : '' }}">
    {{ Form::label('search_params', $mParam->getTranslatedParamTitle(App::getLocale()), ['class' => 'col-md-2 control-label']) }}
    <div class="col-md-10">
        @include($mParam->getSearchTemplateByType(), ['param' => $mParam, 'name' => 'search_params', 'value' => $ranges[$mParam->id]])
        {{ $errors->first('profile_params_'.$mParam->id) }}
    </div>
</div>
@endforeach

<input type="hidden" name="events_search" value="1" />

<!-- Form Actions -->
<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        <button type="submit" class="btn btn-submit">{{ Lang::get('button.search') }}</button>
    </div>
</div>
<!-- ./ form actions -->
{{ Form::close() }}