<div class="page-header">
    <h3>
        {{ $title }}
    </h3>
</div>

<table id="team-member" class="table table-striped table-hover">
    <thead>
    <tr>
        <th class="col-md-2">{{ Lang::get('admin/events/titles.username') }}</th>
        <th class="col-md-4">{{ Lang::get('admin/events/titles.profession') }}</th>
        <th class="col-md-4">{{ Lang::get('admin/events/titles.status_in_team') }}</th>
        <th class="col-md-2">{{ Lang::get('admin/events/titles.status_manager') }}</th>
    </tr>
    </thead>
    <tbody class="table-params-data">
    </tbody>
</table>

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        var oTable;
        $(document).ready(function() {
            oTable = $('#team-member').dataTable( {
                "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sUrl": "/assets/Russian.json"
                },
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "{{ URL::to('admin/events/'.$event->id.'/team') }}",
                "fnDrawCallback": function ( oSettings ) {
                    $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
                }
            });
        });
    </script>
@stop