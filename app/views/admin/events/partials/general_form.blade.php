{{-- Create Event Form --}}
<form class="form-horizontal avatar-form" enctype="multipart/form-data" method="post" action="@if (isset($event)){{ URL::to('admin/events/'.$event->id.'/edit') }} @else {{ URL::to('admin/events/create') }} @endif" autocomplete="off">
<!-- CSRF Token -->
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<!-- ./ csrf token -->

<?php $oldInput = Input::old(); ?>
    <!-- Title -->
    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
        <div class="col-lg-3 col-md-3 col-sm-4">
            {{ Form::label('title', Lang::get('events.title_title'), array('class' => 'control-label')) }}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::text('title', Input::old('title', isset($event) ? $event->title : ""), array('class' => 'form-control', 'id' => 'title')) }}
            {{ $errors->first('title', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ title -->
    <!-- Users -->
    <div class="form-group {{ $errors->has('users_id') ? 'has-error' : '' }}">
        <div class="col-lg-3 col-md-3 col-sm-4">
            {{ Form::label('users_id', Lang::get('events.users_id'), array('class' => 'control-label')) }}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::select('users_id', $usersList, Input::old('users_id', isset($event) ? $event->users_id : null), array('class' => 'form-control', 'id' => 'users_id', (isset($event) && count($event->invites))?'disabled':'')) }}
            {{ $errors->first('users_id', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ Users -->

    <!-- Description -->
    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
        <div class="col-lg-3 col-md-3 col-sm-4">
            {{ Form::label('description', Lang::get('events.title_description'), array('class' => 'control-label')) }}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::textarea('description', Input::old('description', isset($event) ? $event->description : ""), array('class' => 'form-control', 'maxlength' => '570', 'id' => 'description', 'rows' => '5')) }}
            {{ $errors->first('description', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ description -->
    <!-- Image -->
    <div class="form-group {{ $errors->has('new_picture') ? 'has-error' : '' }}">
        <div class="col-lg-3">
            {{ Form::label('new_picture', Lang::get('events.title_photo'), array('class' => 'control-label')) }}
        </div>
        <div class="col-lg-6">

            <?php

            if (isset($event)) {
                $tmpSmallPicFile = $event->getTemporarySmallFile();
                $tmpSmallPicName = $event->getTemporarySmallFileName();
            } else {
                $tmpSmallPicFile = (Input::old('new_picture', '')) ? asset(\Config::get('app.image_dir.events_small') . Input::old('new_picture', '')) : '';
                $tmpSmallPicName = Input::old('new_picture', '');
            }

            ?>

            @include('site.profile.applicant.user_avatar', [
                'fileFieldName'   => 'image',
                'actionUrlUpload' => '/events/upload',
                'origImage'       => $tmpSmallPicFile,
                'origImageName'   => $tmpSmallPicName
            ])
            {{ $errors->first('new_picture', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ image -->

    <!-- Location -->
    <div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
        <div class="col-lg-3 col-md-3 col-sm-4">
            {{ Form::label('location', Lang::get('events.title_event_place'), array('class' => 'control-label', )) }}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::text('location', Input::old('location', isset($event) ? $event->location : ""), array('class' => 'form-control', 'id' => 'location', (isset($event) && count($event->invites))?'disabled':'')) }}

            {{ $errors->first('location', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ location -->
    <!-- Permission view -->
    <div class="form-group {{ $errors->has('permission_view') ? 'has-error' : '' }}">
        <div class="col-lg-3 col-md-3 col-sm-4">
            {{ Form::label('permission_view', Lang::get('events.title_permission_view'), array('class' => 'control-label')) }}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8">
            <?php
            $permissionView1 = isset($oldInput['permission_view']) ? ($oldInput['permission_view']=='1') : (isset($event) ? ($event->permission_view == '1') : true);
            $permissionView0 = isset($oldInput['permission_view']) ? ($oldInput['permission_view']=='0') : (isset($event) ? ($event->permission_view == '0') : false);
            ?>
            <div class="radio">

                <label>
                    {{ Form::radio('permission_view', '1', $permissionView1, array((isset($event) && count($event->invites))?'disabled':'')) }}
                    {{ Lang::get('events.description_permission_invite_yes') }}
                </label>
            </div>
            <div class="radio">
                <label>
                    {{ Form::radio('permission_view', '0', $permissionView0, array((isset($event) && count($event->invites))?'disabled':'')) }}
                    {{ Lang::get('events.description_permission_invite_no') }}
                </label>
            </div>
            {{ $errors->first('permission_view', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ permission view -->
<!-- Permission invite -->

<!-- ./ permission invite -->
<!-- Status -->

<!-- ./ status -->
<!-- Casting -->
<div class="form-group {{ $errors->has('casting') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3 col-sm-4">
        {{ Form::label('casting', Lang::get('events.title_casting'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6 col-sm-8">
        <?php
        $casting1 = isset($oldInput['casting']) ? ($oldInput['casting']=='1') : (isset($event) ? ($event->casting == '1') : true);
        $casting0 = isset($oldInput['casting']) ? ($oldInput['casting']=='0') : (isset($event) ? ($event->casting == '0') : false);
        ?>
        <div class="radio">
            <label>
                {{ Form::radio('casting', '1', $casting1, array((isset($event) && count($event->invites))?'disabled':'')) }}
                {{ Lang::get('events.description_permission_invite_yes') }}
            </label>
        </div>
        <div class="radio">
            <label>
                {{ Form::radio('casting', '0', $casting0, array((isset($event) && count($event->invites))?'disabled':'')) }}
                {{ Lang::get('events.description_permission_invite_no') }}
            </label>
        </div>
        {{ $errors->first('casting', '<span class="help-block">:message</span>') }}
    </div>
</div>
<!-- ./ casting -->

<!-- Type -->
<div class="form-group {{ $errors->has('type_id') ? 'error' : '' }}">
    <div class="col-lg-3 col-md-3 col-sm-4">
        {{ Form::label('type', Lang::get('events.title_event_type'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6 col-sm-8">
        {{ Form::select('type', Acme\Models\Repositories\EventsTypeRepository::getEvensTypes(true), Input::old('type', isset($event) ? $event->type_id : ""), array((isset($event) && count($event->invites))?'disabled':'') ) }}
        {{ $errors->first('type', '<span class="help-block">:message</span>') }}
    </div>
</div>
<!-- ./ type -->


<!-- Profession -->
<div class="form-heading">
    <h3>{{ Lang::get('events.title_profession') }}</h3>
</div>
<div class="form-group {{ $errors->has('profession') ? 'has-error' : '' }}">
    @foreach ($professions as $profession)
        <?php



        $prh = (isset($profRates[$profession->id])) ? $profRates[$profession->id]->rate_h : '';
        $prd = (isset($profRates[$profession->id])) ? $profRates[$profession->id]->rate_d : '';
        $cnt = (isset($profRates[$profession->id])) ? $profRates[$profession->id]->count : '';

        $prh = trim(Input::old('prof_rate_h.'.$profession->id, $prh));
        $prd = trim(Input::old('prof_rate_d.'.$profession->id, $prd));
        $cnt = trim(Input::old('count.'.$profession->id, $cnt));

        $checked = isset($profRates[$profession->id]) ? true : false;
        if(\Session::get('profession')){
            \Session::forget('profession');
            $checked = false;
        }

        ?>
        <div class="row profession-row" id="prof_{{ $profession->id }}">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <label class="control-label" for="profession[{{ $profession->id }}]">{{ $profession->title }}</label>
                <input {{ (isset($event) && count($event->invites))?'disabled':'' }} @if ($checked) checked="checked" @endif name="profession[{{ $profession->id }}]" class="form-control prof_checked" type="checkbox" id="" value="{{ $profession->id }}" />
            </div>
            <div @if (!$checked) style="display: none" @endif>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label class="control-label" for="count[{{ $profession->id }}]">{{ Lang::get('events.count') }}</label>
                    <input {{ (isset($event) && count($event->invites))?'disabled':'' }} class="form-control numbersOnly" type="text" name="count[{{ $profession->id }}]" id="count" value="{{ $cnt }}" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label class="control-label" for="prof_rate_h[{{ $profession->id }}]">{{ Lang::get('events.hour_rate') }}</label>
                    <input {{ (isset($event) && count($event->invites))?'disabled':'' }} class="form-control numbersOnly" type="text" name="prof_rate_h[{{ $profession->id }}]" id="h_count" value="{{ $prh }}" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label class="control-label" for="prof_rate_d[{{ $profession->id }}]">{{ Lang::get('events.day_rate') }}</label>
                    <input {{ (isset($event) && count($event->invites))?'disabled':'' }} class="form-control numbersOnly" type="text" name="prof_rate_d[{{ $profession->id }}]" id="d_count" value="{{ $prd }}" />
                </div>
            </div>
        </div>
    @endforeach
    <div class="col-lg-8">
        {{ $errors->first('profession', '<span class="help-block">:message</span>') }}
    </div>
</div>
<!-- ./ profession -->

    <!-- Manager Number -->
    <div class="form-group {{ $errors->has('manager_num') ? 'has-error' : '' }}">
        <div class="col-lg-3 col-md-3 col-sm-4">
            {{ Form::label('manager_num', Lang::get('events.manager_num'), array('class' => 'control-label')) }}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::text('manager_num', Input::old('manager_num', isset($event) ? $event->manager_num : ""), array('class' => 'form-control numbersOnly', 'id' => 'manager_num', (isset($event) && count($event->invites))?'disabled':'')) }}
            {{ $errors->first('manager_num', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ Manager Number -->
    <!-- Date BEGIN -->
    <div class="form-group">
        <div class="col-lg-3 col-md-3 col-sm-4">
            {{ Form::label('date_start', Lang::get('events.title_date'), array('class' => 'control-label')) }}
        </div>
        <div class='col-lg-3 col-md-3 col-sm-4 {{ $errors->has('date_start') ? 'has-error' : '' }}'>
            <?php $dateStart = Input::old('date_start', (isset($event) && $event->date_start!='0000-00-00 00:00:00') ? date("Y-m-d H:i:s", strtotime($event->date_start)) : ''); ?>
            {{ Form::text('date_start', $dateStart, array('class' => 'datepicker form-control', 'id' => 'datetimepickerStart', (isset($event) && count($event->invites))?'disabled':'')) }}
            {{ $errors->first('date_start', '<span class="help-block">:message</span>') }}
        </div>
        <div class='col-lg-3 col-md-3 col-sm-4 {{ $errors->has('date_end') ? 'has-error' : '' }}'>
            <?php $dateEnd = Input::old('date_end', (isset($event) && $event->date_end!='0000-00-00 00:00:00') ? date("Y-m-d H:i:s", strtotime($event->date_end)) : ''); ?>
            {{ Form::text('date_end', $dateEnd, array('class' => 'datepicker form-control', 'id' => 'datetimepickerEnd', (isset($event) && count($event->invites))?'disabled':'')) }}
            {{ $errors->first('date_end', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- Date END -->
    <!-- Report Date BEGIN -->
    <div class="form-group {{ $errors->has('report_date') ? 'has-error' : '' }}">
        <div class="col-lg-3 col-md-3 col-sm-4">
            {{ Form::label('report_date', Lang::get('events.report_date'), array('class' => 'control-label', 'style' => 'text-align: left;')) }}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-8">
            <?php
            $dateReport = Input::old('report_date', (isset($event) && $event->report_date!='0000-00-00') ? date("Y-m-d", strtotime($event->report_date)) : '');
            ?>
            {{ Form::text('report_date', $dateReport, array('class' => 'form-control datepicker-onlydate datetimepicker-additional', 'id' => 'report_date', (isset($event) && count($event->invites))?'disabled':'')) }}
            {{ $errors->first('report_date', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- Report Date END -->


<input type="hidden" name="events_general_info" value="1" />

<!-- Form Actions -->
<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
        <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
        @if(isset($event))
            <button type="submit" class="btn btn-success">{{Lang::get('button.edit')}}</button>
        @else
            <button type="submit" class="btn btn-success">{{Lang::get('button.create')}}</button>
        @endif
    </div>
</div>
<!-- ./ form actions -->
</form>