<hr class="featurette-divider">
<div class="row">
    @if(!empty($result) && (count($result) > 0))
    @foreach ($result as $item)
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="well well-sm">
            <div class="row">
                @if($item->avatar)
                <div class="col-sm-6 col-md-4">
                    {{ HTML::image(Config::get('app.image_dir.profile') . $item->avatar, $item->avatar, ['class' => 'img-rounded img-responsive']) }}
                </div>
                @endif
                <div class="col-sm-6 col-md-8">
                    <h4> {{ $item->first_name}} {{ $item->last_name}} </h4>
                    <i class="glyphicon glyphicon-map-marker"></i><cite title="San Francisco, USA">Москва, Россия</cite>
                    <p>
                        @if($item->user->email)<i class="glyphicon glyphicon-envelope"></i> {{ $item->user->email }} @endif
                        @if($item->vk_link )<br /><i class="glyphicon glyphicon-globe"></i><a href="{{ $item->vk_link }}"> {{ $item->vk_link }}</a> @endif
                        @if($item->date_birth)<br /><i class="glyphicon glyphicon-gift"></i> {{ $item->date_birth }} @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @else
    <h2>{{ Lang::get('events.user_not_found') }}</h2>
    @endif
</div>