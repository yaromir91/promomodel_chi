@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
    <h3>
        {{ $title }}

        <div class="pull-right">
            <a href="{{ URL::to('admin/gallery/create') }}" class="btn btn-small btn-info iframe">
                <span class="glyphicon glyphicon-plus-sign"></span> {{ Lang::get('button.create') }}</a>
        </div>
    </h3>
</div>

<table id="gallery" class="table table-striped table-hover">
    <thead>
    <tr>
        <th class="col-md-4">{{ Lang::get('admin/gallery/table.preview') }}</th>
        <th class="col-md-4">{{ Lang::get('admin/gallery/table.user') }}</th>
        <th class="col-md-4">{{ Lang::get('admin/gallery/table.title') }}</th>
        <th class="col-md-4">{{ Lang::get('admin/gallery/table.created_at') }}</th>
        <th class="col-md-2">{{ Lang::get('table.actions') }}</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop


{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable = $('#gallery').dataTable( {
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sUrl": "/assets/Russian.json"
            },
            "bProcessing": true,
            "bServerSide": true,
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }
            ],
            "sAjaxSource": "{{ URL::to('admin/gallery/data') }}",
            "fnDrawCallback": function ( oSettings ) {
                $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
            }
        });
    });
</script>
@stop