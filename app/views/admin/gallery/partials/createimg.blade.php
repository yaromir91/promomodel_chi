@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

        <!-- File -->
        <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
            {{ Form::label('file', Lang::get('gallery.field_file_title'), array('class' => 'col-md-2 control-label')) }}
            <div class="col-md-10" id="imageUploadBlock">
                <div id="fileuploader" token="{{ csrf_token() }}">{{ Lang::get('button.add') }}</div>
            </div>
        </div>
        <!-- ./ file -->

        <input type="hidden" id="gallery_id" value="{{$gallery->id}}" class="gallery-id-hdn" />


@stop