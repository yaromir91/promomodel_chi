{{-- Edit Param Form --}}
<form class="form-horizontal avatar-form" enctype="multipart/form-data" method="post" action="@if (isset($gallery)){{ URL::to('admin/gallery/' . $gallery->id . '/edit') }} @else {{ URL::to('admin/gallery/create') }} @endif" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- ./ csrf token -->

    <!-- Post Title -->
    <div class="form-group {{ $errors->has('title') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="title">{{ Lang::get('admin/gallery/table.title') }}</label>
            <input class="form-control" maxlength="70" type="text" name="title" id="title" value="{{ Input::old('title', isset($gallery) ? $gallery->title : null) }}" />
            {{ $errors->first('title', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ post title -->

    <!-- Post User -->
    <div class="form-group {{ $errors->has('user_id') ? 'error' : '' }}">
        <div class="col-md-12">
            <label class="control-label" for="user_id">{{ Lang::get('admin/gallery/form.user') }}</label>

            <?php
                $attributes = [
                        'class'             => 'form-control addition-param',
                        'data-param-name'   => 'user_id',
                        'id'                => 'user_id'
                ];

                if (isset($gallery)) {
                    $attributes['disabled'] = 'disabled';
                }
            ?>

            {{ Form::select('user_id', $usersList, Input::old('user_id', isset($gallery) ? $gallery->user_id : null), $attributes) }}
            {{ $errors->first('user_id', '<span class="help-block">:message</span>') }}
        </div>
    </div>
    <!-- ./ post title -->


    <!-- Hash tag -->
    <!-- ./ hash tag -->

    <!-- Preview -->
    <div class="form-group {{ $errors->has('preview') ? 'has-error' : '' }}">
        <div class="col-md-12">
            {{ Form::label('preview', Lang::get('gallery.Preview'), array('class' => 'control-label')) }}

            <?php

            if (isset($gallery)) {
                $tmpSmallPicFile = $gallery->getTemporarySmallFile();
                $tmpSmallPicName = $gallery->getTemporarySmallFileName();
            } else {
                $tmpSmallPicFile = (Input::old('new_picture', '') && Input::old('user_id', '')) ? asset(\Config::get('app.image_dir.album') . Input::old('user_id', '') . '/' . Input::old('new_picture', '')) : '';
                $tmpSmallPicName = Input::old('new_picture', '');
            }

            ?>

            @include('site.profile.applicant.user_avatar', [
                 'fileFieldName'   => 'preview',
                 'actionUrlUpload' => '/gallery/upload-preview',
                 'origImage'       => $tmpSmallPicFile ,
                 'origImageName'   => $tmpSmallPicName
             ])
            {{ $errors->first('preview') }}
        </div>
    </div>
    <!-- ./ Preview -->

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-12">
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>
            <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>