@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

<form id="deleteForm" class="form-horizontal" method="post" action="@if (isset($gallery)){{ URL::to('admin/gallery/' . $gallery->id . '/delete') }}@endif" autocomplete="off">

    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="id" value="{{ $gallery->id }}" />
    <!-- <input type="hidden" name="_method" value="DELETE" /> -->
    <!-- ./ csrf token -->

    <!-- Form Actions -->
    <div class="form-group">
        <div class="controls">
            <div>{{Lang::get('admin/gallery/titles.delete_gallery')}}?</div>
            <button class="btn btn-default btn-cancel close_popup">{{Lang::get('button.cancel')}}</button>
            <button type="submit" class="btn btn-danger">{{Lang::get('button.delete')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->
</form>
@stop