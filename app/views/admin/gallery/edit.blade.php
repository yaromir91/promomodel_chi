@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

<!-- Tabs -->
<ul class="nav nav-tabs">
    <li class="@if ($tab=='1')active @endif"><a href="#tab-general" data-toggle="tab">{{ Lang::get('admin/gallery/form.gallery_details') }}</a></li>
    @if (isset($gallery))
    <li class="childrens-el @if ($tab=='2')active @endif"><a href="#tab-childrens" data-toggle="tab">{{ Lang::get('admin/gallery/form.images') }}</a></li>
    @endif
</ul>
<!-- ./ tabs -->


<!-- Tabs Content -->
<div class="tab-content">
    <!-- General tab -->
    <div class="tab-pane @if ($tab=='1')active @endif" id="tab-general">
        @include('admin.gallery.partials.general_form')
    </div>
    <!-- ./ general tab -->

    @if (isset($gallery))
    <!-- Child Elements tab -->
    <div class="tab-pane @if ($tab=='2')active @endif" id="tab-childrens">
        @include('admin.gallery.partials.images_form')
    </div>
    <!-- ./ Child Elements tab -->
    @endif
</div>
<!-- ./ tabs content -->

@stop