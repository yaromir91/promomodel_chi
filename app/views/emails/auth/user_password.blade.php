<h1>{{ Lang::get('mails.new_user_password.subject') }}</h1>

<p>{{ Lang::get('mails.new_user_password.greetings', array('name' => $user['username'])) }},</p>

<p>{{ Lang::get('mails.new_user_password.body_password', array('password' => $password)) }}</p>

@if(!$user['confirmed'])
    <p>{{ Lang::get('mails.new_user_password.body') }}</p>
    <a href='{{ URL::to("user/confirm/{$user['confirmation_code']}") }}'>
        {{ URL::to("user/confirm/{$user['confirmation_code']}") }}
    </a>
@endif
