<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>{{Lang::get('mails.thank_for_registering')}}</h2>

<div>
    {{Lang::get('mails.new_generated_password')}} {{$password}}
    {{Lang::get('mails.you_can_change_password')}}
</div>
</body>
</html>