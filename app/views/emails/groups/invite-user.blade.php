<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">

	</head>
	<body>
		<h2>{{ Lang::get('groups.invite_mail_title') }}</h2>
		<div>
			{{ Lang::get('groups.title_show_group') }} : <a href="{{URL::route('show_group', $group_id)}}" target="_blank">{{ $group_name }}</a>
			<hr>
			{{ Lang::get('groups.invite_mail_success') }} <a href="{{ URL::route('invite_user_get_success', $token) }}">{{ Lang::get('mails.link') }}</a>
			{{ Lang::get('groups.invite_mail_refuse') }} <a href="{{ URL::route('invite_user_get_refuse', $token) }}">{{ Lang::get('mails.link') }}</a>
		</div>
	</body>
</html>