<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">

	</head>
	<body>
		<h2>{{ Lang::get('groups.group_deleted') }}</h2>

		<div>
			{{ Lang::get('groups.title_show_group') }} : {{ $group }}
			<hr>
			{{ Lang::get('groups.group_deleted_description') }}
		</div>
	</body>
</html>