<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>{{ Lang::get('groups.invite_mail_title_accept') }}</h2>
		<div>
			{{ Lang::get('groups.title_show_group') }} : <a href="{{URL::route('show_group', $group_id)}}" target="_blank">{{ $group_name }}</a>
			<hr>
			{{ Lang::get('groups.invite_mail_show_user') }} <a href="{{ URL::route('view_user_profile', $user_id) }}">{{ Lang::get('mails.link') }}</a>
		</div>
	</body>
</html>