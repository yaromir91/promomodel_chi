{{sprintf(\Lang::get('mails.you_was_invited_new_event'), $email)}}
<br />
{{\Lang::get('mails.your_profession')}}: {{$profession}}
<br />
{{\Lang::get('mails.the_event')}} <a href="{{URL::route('show_events', $eventId)}}">{{$eventTitle}}</a> {{\Lang::get('mails.created_by')}} <a href="{{URL::route('view_user_profile', $organizeId)}}">{{$organizeName}}</a>
<br />
{{\Lang::get('mails.you_can_view_invitations')}} <a href="{{URL::route('event_my_invitation')}}"></a>