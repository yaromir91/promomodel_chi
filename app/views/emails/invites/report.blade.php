@if(count($invitations))
    <p>{{\Lang::get('mails.these_candidates_accepted_invitation')}} "{{$event->title}}".</p>
    <ul>
        @foreach($invitations as $inv)
            <li>
                <a href="{{URL::route('view_user_profile', $inv->users_id)}}">{{$inv->user->getUserName()}}</a>
                -
                @if($inv->is_manager==1)
                    {{\Lang::get('mails.manager')}} / {{$inv->professions->title}}
                @else
                    {{\Lang::get('mails.applicant')}} / {{$inv->professions->title}}
                @endif
            </li>
        @endforeach
    </ul>
@else
    <p>{{\Lang::get('mails.any_candidates_not_accepted_invitation')}} "{{$event->title}}".</p>
@endif
