<p>{{sprintf(\Lang::get('mails.user_rejected_invitation_event'), $rejectedUserName)}}</p>
<p>{{\Lang::get('mails.you_can_find_other_applicants_on_page')}} <a href="{{URL::route('get_search_event_users', $eventId)}}">{{\Lang::get('mails.click_here')}}</a></p>

@if($result)
    <p>Список возможных кандидатов</p>
    <ul>
        @foreach ($result as $item)
            <li>
                <a target="_blank" href="{{URL::route('view_user_profile', $item['user_id'])}}">
                    <h4> {{ $item['first_name']}} {{ $item['last_name']}} </h4>
                </a>
            </li>
        @endforeach
    </ul>
@endif
@if($isManager)
    {{\Lang::get('mails.as_a_manager')}}.
@else
    {{\Lang::get('mails.on_profession')}}.
@endif