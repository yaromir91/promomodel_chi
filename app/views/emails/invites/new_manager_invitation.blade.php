{{sprintf(\Lang::get('mails.you_was_invited_as_manager_new_event'), $email)}}
<br />
The event <a href="{{URL::route('show_events', $eventId)}}">{{$eventTitle}}</a> created by <a href="{{URL::route('view_user_profile', $organizeId)}}">{{$organizeName}}</a>
<br />
{{\Lang::get('mails.you_can_view_invitations')}} <a href="{{URL::route('event_my_invitation')}}"></a>