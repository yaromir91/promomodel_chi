<div>
    {{sprintf(\Lang::get('mails.user_accepted_invitation_event'), $userName)}} ]
    <a href="{{$siteUrl}}{{URL::route('show_events', $eventId)}}">{{$eventTitle}}</a>
    @if($isManager)
        {{\Lang::get('mails.as_a_manager')}} и {{\Lang::get('mails.on_profession')}}.
    @else
        {{\Lang::get('mails.on_profession')}}.
    @endif
</div>