@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ String::title($post->title) }} ::
@parent
@stop

{{-- Update the Meta Title --}}
@section('meta_title')
@parent

@stop

{{-- Update the Meta Description --}}
@section('meta_description'){{ $post->meta_description() }}@stop

{{-- Content --}}
@section('content')
<h3>{{ $post->title }}</h3>

<p>{{ $post->content() }}</p>

<div>
	<span class="badge badge-info">Posted {{ $post->date() }}</span>
</div>

<hr />

<a id="comments"></a>
<h4>{{ $comments->count() }} {{ \Illuminate\Support\Pluralizer::plural('Comment', $comments->count()) }}</h4>

@if ($comments->count())
@foreach ($comments as $comment)
<div class="row">
	<div class="col-md-1">
		<img class="thumbnail" src="//placehold.it/60x60" alt="">
	</div>
	<div class="col-md-11">
		<div class="row">
			<div class="col-md-11">
				<span class="muted">{{ $comment->author->username }}</span>
				&bull;
				{{ $comment->date() }}
			</div>

			<div class="col-md-11">
				<hr />
			</div>

			<div class="col-md-11">
				{{ nl2br(e($comment->content())) }}
			</div>
		</div>
	</div>
</div>
<hr />
@endforeach
@else
<hr />
@endif

{{ $commentForm }}

@stop
