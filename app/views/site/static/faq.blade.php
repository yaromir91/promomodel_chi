@extends('site.layouts.default')
{{-- Web site Title --}}
@section('title')
    {{ Lang::get('general.home_page') }} ::
    @parent
@stop

<div class="container">
    <div class="page-header clearfix">
        <h1>помощь по сайту</h1>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <ul id="faq-accordion">
                <li>
                    <i class="faq-icon faq-icon-question"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#general"
                       aria-expanded="true" aria-controls="general">
                        Общие вопросы
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse in" id="general">
                        <li><a href="#q1">Как стать агентом?</a></li>
                        <li><a href="#q2">Какие возможности у агента?</a></li>
                        <li><a href="#q3">Как стать партнером?</a></li>
                        <li><a href="#q4">Как стать Соискателем-менеджером?</a></li>
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-event"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#event"
                       aria-controls="event">
                        Создание мероприятия
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="event">
                        <li><a href="#q5">Как стать агентом? 2</a></li>
                        <li><a href="#q5">Какие возможности у агента? 2</a></li>
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-eventReport"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#eventReport"
                       aria-controls="eventReport">
                        Создание отчета мероприятия
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="eventReport">
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-chat"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#chat"
                       aria-controls="chat">
                        Чат между пользователями
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="chat">
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-group"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#group"
                       aria-controls="group">
                        Создание группы
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="group">
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-profile"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#profile"
                       aria-controls="profile">
                        Заполнение профиля
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="profile">
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-calendar"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#calendar"
                       aria-controls="calendar">
                        Календарь
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="calendar">
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-auth"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#auth"
                       aria-controls="auth">
                        Авторизация
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="auth">
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-reg"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#reg"
                       aria-controls="reg">
                        Регистрация
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="reg">
                    </ul>
                </li>
                <li>
                    <i class="faq-icon faq-icon-album"></i>
                    <a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#album"
                       aria-controls="album">
                        Создание альбома
                        <i class="faq-icon faq-icon-arrow"></i>
                    </a>
                    <ul class="collapse" id="album">
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="questions">
                <div class="question" id="q1">
                    <h3>Как стать агентом?</h3>
                    <p>Для того, чтобы стать Агентом, зайдите в раздел «Агенты»
                        <a href="https://www.promogang.ru/agents">https://www.promogang.ru/agents</a> и нажмите справа на
                        кнопку «Стать агентом». После заполнения всех
                        полей нажмите на кнопку «Стать агентом». На вашу электронную почту будет выслано
                        письмо−подтверждение, перейдите по ссылке указанной в письме.</p>
                    <p>После рассмотрения и подтверждения вашей заявки администратором, вы становитесь Агентом.</p>
                </div>
                <div class="question" id="q2">
                    <h3>Какие возможности у агента?</h3>
                    <p>Для того, чтобы стать Агентом, зайдите в раздел «Агенты»
                        <a href="https://www.promogang.ru/agents">https://www.promogang.ru/agents</a> и нажмите справа на
                        кнопку «Стать агентом». После заполнения всех
                        полей нажмите на кнопку «Стать агентом». На вашу электронную почту будет выслано
                        письмо−подтверждение, перейдите по ссылке указанной в письме.</p>
                    <p>После рассмотрения и подтверждения вашей заявки администратором, вы становитесь Агентом.</p>
                </div>
                <div class="question" id="q3">
                    <h3>Как стать партнером?</h3>
                    <p>Для того, чтобы стать Агентом, зайдите в раздел «Агенты»
                        <a href="https://www.promogang.ru/agents">https://www.promogang.ru/agents</a> и нажмите справа на
                        кнопку «Стать агентом». После заполнения всех
                        полей нажмите на кнопку «Стать агентом». На вашу электронную почту будет выслано
                        письмо−подтверждение, перейдите по ссылке указанной в письме.</p>
                    <p>После рассмотрения и подтверждения вашей заявки администратором, вы становитесь Агентом.</p>
                </div>
                <div class="question" id="q4">
                    <h3>Как стать Соискателем-менеджером?</h3>
                    <p>Для того, чтобы стать Агентом, зайдите в раздел «Агенты»
                        <a href="https://www.promogang.ru/agents">https://www.promogang.ru/agents</a> и нажмите справа на
                        кнопку «Стать агентом». После заполнения всех
                        полей нажмите на кнопку «Стать агентом». На вашу электронную почту будет выслано
                        письмо−подтверждение, перейдите по ссылке указанной в письме.</p>
                    <p>После рассмотрения и подтверждения вашей заявки администратором, вы становитесь Агентом.</p>
                </div>
                <div class="question" id="q5">
                    <h3>Как стать агентом? 2</h3>
                    <p>Для того, чтобы стать Агентом, зайдите в раздел «Агенты»
                        <a href="https://www.promogang.ru/agents">https://www.promogang.ru/agents</a> и нажмите справа на
                        кнопку «Стать агентом». После заполнения всех
                        полей нажмите на кнопку «Стать агентом». На вашу электронную почту будет выслано
                        письмо−подтверждение, перейдите по ссылке указанной в письме.</p>
                    <p>После рассмотрения и подтверждения вашей заявки администратором, вы становитесь Агентом.</p>
                </div>
                <div class="question" id="q6">
                    <h3>Какие возможности у агента? 2</h3>
                    <p>Для того, чтобы стать Агентом, зайдите в раздел «Агенты»
                        <a href="https://www.promogang.ru/agents">https://www.promogang.ru/agents</a> и нажмите справа на
                        кнопку «Стать агентом». После заполнения всех
                        полей нажмите на кнопку «Стать агентом». На вашу электронную почту будет выслано
                        письмо−подтверждение, перейдите по ссылке указанной в письме.</p>
                    <p>После рассмотрения и подтверждения вашей заявки администратором, вы становитесь Агентом.</p>
                </div>
            </div>
        </div>
    </div>
</div>