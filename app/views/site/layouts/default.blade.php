<!DOCTYPE html>
<html lang="ru">
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
        <meta http-equiv="Content-Encoding" content="gzip" />
        <meta http-equiv="cache-control" content="max-age=3600, public, must-revalidate" />
        <meta http-equiv="expires" content="3600" />
		<title>
			@section('title')
                {{ Lang::get('general.title') }}
			@show
		</title>
		@section('meta_keywords')
		<meta name="keywords" content="promo model" />
		@show
		@section('meta_author')
		<meta name="author" content="CHISW" />
		@show

        <meta name="description" content="@yield('meta_description')" />

		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-param" content="_csrf">
        <meta name="csrf-token" content="{{ csrf_token() }}">


		<!-- CSS
		================================================== -->
        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/static.css')}}">

        @if(Request::is('events*') || Request::is('applicants*') || Request::is('groups*'))
            <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/jquery-ui-slider-pips.css')}}">
        @endif

        @if(Request::is('groups*') || Request::is('profile'))
            <link rel="stylesheet" href="{{asset('assets/css/uploadfile.css')}}" rel="stylesheet">
        @endif

        @if(Request::is('events*') || Request::is('profile'))
            <link rel="stylesheet" href="{{asset('assets/css/jquery.datetimepicker.css')}}">
        @endif

        @if(Request::is('calendar*'))
            <link rel="stylesheet" href="{{asset('assets/fullcalendar/fullcalendar.min.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/jquery.datetimepicker.css')}}">
            {{--<link rel="stylesheet" href="{{asset('assets/fullcalendar/fullcalendar.print.css')}}">--}}
        @endif

        @if(Request::is('gallery*'))
            <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/jquery.tagit.css')}}">

            <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
            <link rel="stylesheet" href="{{asset('assets/css/uploadfile.css')}}" rel="stylesheet">
            <link rel="stylesheet" href="{{asset('assets/css/fancybox/jquery.fancybox.css')}}" type="text/css" media="screen" />
        @endif

        @if(Request::is('gallery*') || Request::is('events*') || Request::is('profile*') || Request::is('report*'))
            <link rel="stylesheet" href="{{asset('assets/css/star-rating.min.css')}}" rel="stylesheet">
        @endif

        @if(Request::is('applicants*') || Request::is('organizers'))
            <link rel="stylesheet" href="{{asset('assets/css/applicant/style.css')}}" rel="stylesheet">
        @endif

        @if(Request::is('profile*') || Request::is('gallery*') || Request::is('events*') || Request::is('groups*') || Request::is('report*'))
            <link rel="stylesheet" href="{{asset('assets/css/cropper.min.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/cropper_main.css')}}">
        @endif

        @if(Request::is('profile*'))
            <link rel="stylesheet" href="{{asset('assets/css/jssor_slider.css')}}">
        @endif

		<!-- Favicons
		================================================== -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" href="{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">
		<link rel="shortcut icon" sizes="32x32" href="{{ asset('assets/ico/favicon.png') }}">

        <script type="application/ld+json">
            {
                "@context" : "http://schema.org",
                "@type" : "Organization",
                "name" : "Promogang",
                "url" : "https://www.promogang.ru",
                "sameAs" : [
                    "https://twitter.com/PromoGangWorld"
                ]
            }
        </script>

    </head>

	<body>
		<!-- To make sticky footer need to wrap in a div -->
		<div id="wrap">
		<!-- Navbar -->
            <div class="navbar navbar-fixed-top">
                 <div class="container-fluid">
                     <div class="navbar-content">
                         <div class="navbar-header">
                             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                 <span class="sr-only">Toggle navigation</span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                             </button>
                             <a class="navbar-brand" href="{{ URL::to('') }}">Promogang</a>
                         </div>
                         <div class="collapse navbar-collapse navbar-ex1-collapse">
                             <ul class="nav navbar-nav left-navigation">
                                {{-- <li {{ (Request::is('/') ? ' class="active"' : '') }}><a href="{{ URL::to('') }}">{{ Lang::get('general.menu_main') }}</a></li>--}}
                                 @if (Auth::check() && !Auth::user()->hasRole('admin'))
                                 <li {{ (Request::is('profile/show') ? ' class="active"' : '') }}><a href="{{ URL::to('profile/show') }}">{{ Lang::get('general.menu_my_page') }}</a></li>
                                 @endif
                                 <li {{ (Request::is('organizers*') ? ' class="active"' : '') }}><a href="{{ URL::to('organizers') }}">{{ Lang::get('general.menu_organizers') }}</a></li>
                                 <li {{ (Request::is('applicants*') ? ' class="active"' : '') }}><a href="{{ URL::to('applicants') }}">{{ Lang::get('general.menu_applicants') }}</a></li>
                                 @if (!Auth::check() || (Auth::check() && !Auth::user()->hasRole('Organizer')))
                                    <li {{ (Request::is('agents*') ? ' class="active"' : '') }}><a href="{{ URL::to('agents') }}">{{ Lang::get('general.menu_agents') }}</a></li>
                                 @endif
                                 <li {{ (Request::is('gallery*') ? ' class="active"' : '') }}><a href="{{ URL::to('gallery') }}">{{ Lang::get('general.menu_gallery') }}</a></li>
                                 <li {{ (Request::is('report*') ? ' class="active"' : '') }}><a href="{{ URL::to('report') }}">{{ Lang::get('general.menu_report') }}</a></li>
                                 <li {{ (Request::is('events*') ? ' class="active"' : '') }}><a href="{{ URL::to('events') }}">{{ Lang::get('general.menu_events') }}</a></li>
                                 <li {{ (Request::is('groups*') ? ' class="active"' : '') }}><a href="{{ URL::to('groups') }}">{{ Lang::get('general.menu_groups') }}</a></li>
                                 @if (Auth::check() && !Auth::user()->hasRole('admin') && !Auth::user()->hasRole('Agent'))
                                    <li {{ (Request::is('calendar*') ? ' class="active"' : '') }}><a href="{{ URL::to('calendar') }}">{{ Lang::get('general.menu_calendar') }}</a></li>
                                 @endif
                                <li {{ (Request::is('partner*') ? ' class="active"' : '') }}><a href="{{ URL::to('partner') }}">{{ Lang::get('general.menu_partner') }}</a></li>
                             </ul>
                             <ul class="nav navbar-nav navbar-right sign-menu">
                                 @if (Auth::check())
                                     <li>
                                         <a href="{{URL::route('chat_list')}}">
                                             {{ Lang::get('general.messages') }}
                                             @if($user_un_read_message > 0)
                                                 <span> {{$user_un_read_message}}</span>
                                             @endif
                                         </a>
                                     </li>
                                 @if (Auth::user()->hasRole('admin'))
                                 <li><a href="{{ URL::to('admin/users') }}"><!--{{ Lang::get('general.menu_admin_panel') }}--><i class="fa fa-cog"></i></a></li>
                                 @endif
                                 <li><span>@if($user_role_for_header){{ $user_role_for_header }}:@endif {{ Auth::user()->username }}</span></li>
                                 <li><a href="{{ URL::to('user/logout') }}">{{ Lang::get('general.menu_logout') }}</a></li>
                                 @else
                                 <li {{ (Request::is('user/login') ? ' class="active"' : '') }}><a href="{{ URL::to('user/login') }}">{{ Lang::get('general.menu_login') }}</a></li>
                                 <!--<li>{{ link_to_route('language.select', 'Ru', array('ru')) }}</li>-->
                                 <!--<li>{{ link_to_route('language.select', 'En', array('en')) }}</li>-->
                                 <li {{ (Request::is('fb/login') ? ' class="active"' : '') }}><a href="{{ URL::to('fb/login') }}"><!--{{ Lang::get('general.menu_facebook_login') }}--><i class="fa fa-facebook-official"></i></a></li>
                                 <li {{ (Request::is('user/create') ? ' class="active"' : '') }}><a href="{{ URL::to('user/create') }}">{{ Lang::get('general.menu_sign_up') }}</a></li>
                                 @endif
                             </ul>
                             <!-- ./ nav-collapse -->
                         </div>
                     </div>
                </div>
            </div>
            <a href="#" class="mobile-navigationBtn">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-bars fa-stack-1x fa-inverse"></i>
                </span>
            </a>
            <div class="mobile-navigation">
                <ul>
                    @if (Auth::check() && !Auth::user()->hasRole('admin'))
                        <li {{ (Request::is('profile/show') ? ' class="active"' : '') }}><a href="{{ URL::to('profile/show') }}">{{ Lang::get('general.menu_my_page') }}</a></li>
                    @endif
                    <li {{ (Request::is('organizers*') ? ' class="active"' : '') }}><a href="{{ URL::to('organizers') }}">{{ Lang::get('general.menu_organizers') }}</a></li>
                    <li {{ (Request::is('applicants*') ? ' class="active"' : '') }}><a href="{{ URL::to('applicants') }}">{{ Lang::get('general.menu_applicants') }}</a></li>
                    @if (!Auth::check() || (Auth::check() && !Auth::user()->hasRole('Organizer')))
                        <li {{ (Request::is('agents*') ? ' class="active"' : '') }}><a href="{{ URL::to('agents') }}">{{ Lang::get('general.menu_agents') }}</a></li>
                    @endif
                    <li {{ (Request::is('gallery*') ? ' class="active"' : '') }}><a href="{{ URL::to('gallery') }}">{{ Lang::get('general.menu_gallery') }}</a></li>
                    <li {{ (Request::is('report*') ? ' class="active"' : '') }}><a href="{{ URL::to('report') }}">{{ Lang::get('general.menu_report') }}</a></li>
                    <li {{ (Request::is('events*') ? ' class="active"' : '') }}><a href="{{ URL::to('events') }}">{{ Lang::get('general.menu_events') }}</a></li>
                    <li {{ (Request::is('groups*') ? ' class="active"' : '') }}><a href="{{ URL::to('groups') }}">{{ Lang::get('general.menu_groups') }}</a></li>
                    @if (Auth::check() && !Auth::user()->hasRole('admin') && !Auth::user()->hasRole('Agent'))
                        <li {{ (Request::is('calendar*') ? ' class="active"' : '') }}><a href="{{ URL::to('calendar') }}">{{ Lang::get('general.menu_calendar') }}</a></li>
                    @endif
                    <li {{ (Request::is('partner*') ? ' class="active"' : '') }}><a href="{{ URL::to('partner') }}">{{ Lang::get('general.menu_partner') }}</a></li>
                    @if (Auth::check())
                        <li>
                            <a href="{{URL::route('chat_list')}}">
                                {{ Lang::get('general.messages') }}
                                @if($user_un_read_message > 0)
                                    <span> {{$user_un_read_message}}</span>
                                @endif
                            </a>
                        </li>
                        @if (Auth::user()->hasRole('admin'))
                            <li><a href="{{ URL::to('admin/users') }}"><!--{{ Lang::get('general.menu_admin_panel') }}--><i class="fa fa-cog"></i></a></li>
                        @endif
                        <li><span>@if($user_role_for_header){{ $user_role_for_header }}:@endif {{ Auth::user()->username }}</span></li>
                        <li><a href="{{ URL::to('user/logout') }}">{{ Lang::get('general.menu_logout') }}</a></li>
                    @else
                        <li {{ (Request::is('user/login') ? ' class="active"' : '') }}><a href="{{ URL::to('user/login') }}">{{ Lang::get('general.menu_login') }}</a></li>
                        <!--<li>{{ link_to_route('language.select', 'Ru', array('ru')) }}</li>-->
                        <!--<li>{{ link_to_route('language.select', 'En', array('en')) }}</li>-->
                        <li {{ (Request::is('fb/login') ? ' class="active"' : '') }}><a href="{{ URL::to('fb/login') }}"><!--{{ Lang::get('general.menu_facebook_login') }}--><i class="fa fa-facebook-official"></i></a></li>
                        <li {{ (Request::is('user/create') ? ' class="active"' : '') }}><a href="{{ URL::to('user/create') }}">{{ Lang::get('general.menu_sign_up') }}</a></li>
                    @endif
                </ul>
            </div>
		<!-- ./ navbar -->

            <div class="helper-section">
                <div class="helper-logo">
                    <a href="">
                        <img src="{{asset('assets/img/question-symbol.png')}}" alt="">
                    </a>
                </div>
                <div class="helper-container">
                    <p>Нужна помощь?<br>
                        Инструкция на странице<br>
                        <a href="{{ URL::to('static/faq') }}">"Помощь по сайту"</a>
                    </p>
                </div>
            </div>

            <div class="language-section">
                <ul>
                    @if(\Session::get('lang') == null || \Session::get('lang') == 'ru')
                        <li class="inner_language">
                            <a href="{{ URL::to('language/ru') }}" class="active">Ru<i class="fa fa-chevron-down"></i></a>
                        </li>
                        <li class="inner_language">
                            <a href="{{ URL::to('language/en') }}">En<i class="fa"></i></a>
                        </li>
                    @else
                        <li class="inner_language">
                            <a href="{{ URL::to('language/en') }}" class="active">En<i class="fa fa-chevron-down"></i></a>
                        </li>
                        <li class="inner_language">
                            <a href="{{ URL::to('language/ru') }}">Ru<i class="fa"></i></a>
                        </li>
                    @endif

                </ul>
            </div>
		<!-- Container -->
		<div class="container">
			<!-- Notifications -->
			@include('notifications')
			<!-- ./ notifications -->

			<!-- Content -->
			@yield('content')
			<!-- ./ content -->
		</div>
		<!-- ./ container -->
        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <ul>
                            <li><a class="twitter" target="_blank" href="{{ \Config::get('app.social.links.twitter'); }}"><i class="fa fa-2x fa-twitter-square"></i></a></li>
                            <li><a href="{{ \Config::get('app.social.links.facebook'); }}"><i class="fa fa-2x fa-facebook-square"></i></a></li>
                            <li><a target="_blank" rel="publisher" href="{{ \Config::get('app.social.links.google'); }}"><i class="fa fa-2x fa-google-plus-square"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-info-block">
                            <p class="footer-info-header">
                                {{ Lang::get('general.phones') }}:
                            </p>
                            <p class="footer-basic-info">
                                <a href="tel:+70562244444">+7(056) 224 44 44</a>,&nbsp;<a href="tel:+70562244444">+7(056) 224 44 44</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-info-block">
                            <p class="footer-info-header">
                                {{ Lang::get('general.email') }}:
                            </p>
                            <p class="footer-basic-info">
                                <a href="mailto:{{ \Config::get('app.admin_email'); }}">{{ \Config::get('app.admin_email'); }}</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-info-block">
                            <p class="footer-info-header">
                                {{ Lang::get('general.office_address') }}:
                            </p>
                            <p class="footer-basic-info">
                                <a href="mailto:{{ \Config::get('app.admin_email'); }}">{{ \Config::get('app.admin_email'); }}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- the following div is needed to make a sticky footer -->
		<div id="push"></div>
		</div>
		<!-- ./wrap -->





        <style>
            @section('styles')
            @show
        </style>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="{{asset('assets/js/html5.js')}}"></script>
        <![endif]-->

        <script>
            var base_url = '{{ URL::to('') }}/';
            var translate_upload = '{{ Lang::get('button.upload') }}';
            var translate_reject = '{{ Lang::get('button.reject') }}';
            var translate_delete = '{{ Lang::get('button.delete') }}';
            var current_lang = '{{ \App::getLocale() }}';
        </script>

		<!-- Javascripts
		================================================== -->
        <script src="{{asset('assets/js/jquery.min_v3.js')}}"></script>
        <script src="{{asset('bootstrap/js/bootstrap.min_v3.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/faq.js') }}"></script>
        <script type="text/javascript" src="{{asset('assets/js/mobileNavigation.js')}}"></script>
        @if(Request::is('user/forgot'))
            <script type="text/javascript" src="{{ asset('assets/js/user.js') }}"></script>
        @endif
        @if(Request::is('events*'))
            <script type="text/javascript" src="{{ asset('assets/js/events.js') }}"></script>
        @endif
        @if(Request::is('events/team/*'))
            <script type="text/javascript" src="{{ asset('assets/js/team.js') }}"></script>
        @endif
        @if(Request::is('events*') || Request::is('applicants*') || Request::is('groups*'))
            <script type="text/javascript" src="{{ asset('assets/js/jquery-ui.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/jquery-ui-slider-pips.js') }}"></script>
        @endif
        @if(Request::is('groups*'))
            <script type="text/javascript" src="{{ asset('assets/js/groups.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/jquery.uploadfile.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/applicant/applicants.js') }}"></script>
        @endif
        @if(Request::is('events*') || Request::is('profile') || Request::is('calendar*'))
            <script src="{{asset('assets/js/moment.min.js')}}"></script>
            <script src="{{asset('assets/js/jquery.mask.js')}}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/jquery.datetimepicker.js') }}"></script>
            <!--<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>-->
            <script type="text/javascript" src="{{ asset('assets/js/custom_calendar.js') }}"></script>
        @endif

        @if(Request::is('calendar'))
            <script type="text/javascript" src="{{ asset('assets/fullcalendar/lib/moment.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/fullcalendar/fullcalendar.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/fullcalendar/lang/ru.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/calendar_main.js') }}"></script>
        @endif

        @if(Request::is('gallery*'))
            <script type="text/javascript" src="{{ asset('assets/js/jquery-ui.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/tag-it.min.js') }}"></script>

            <script src="{{ asset('assets/js/jquery.uploadfile.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/fancybox/jquery.fancybox.pack.js') }}"></script>

            <script type="text/javascript" src="{{ asset('assets/js/star-rating.min.js') }}"></script>

            <script type="text/javascript" src="{{ asset('assets/js/gallery.js') }}"></script>
        @endif

        @if(Request::is('applicants*') || Request::is('organizers') || Request::is('agents'))
            <script type="text/javascript" src="{{ asset('assets/js/applicant/applicants.js') }}"></script>
        @endif

        @if(Request::is('profile/view/*') || Request::is('agents/*'))
            <script type="text/javascript" src="{{ asset('assets/js/agent.js') }}"></script>
        @endif

        @if(Request::is('profile') || Request::is('gallery*') || Request::is('events*') || Request::is('groups*') || Request::is('report*'))
            <script type="text/javascript" src="{{ asset('assets/js/cropper.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/pics_cropping.js') }}"></script>
        @endif

        @if(Request::is('report*') || Request::is('events*') || Request::is('profile*'))
            <script type="text/javascript" src="{{ asset('assets/js/star-rating.min.js') }}"></script>
        @endif
        @if(Request::is('report*'))
            <script type="text/javascript" src="{{ asset('assets/js/report.js') }}"></script>
        @endif
        @if(Request::is('profile/view*'))
            <script type="text/javascript" src="{{ asset('assets/js/slider/jssor.slider.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/slider/profile_gallery_slider.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/profile.js') }}"></script>
        @endif
        @if(Request::is('profile'))
            <script type="text/javascript" src="{{ asset('assets/js/profile.js') }}"></script>
        @endif
        @if(Request::is('profile/show'))
            <script type="text/javascript" src="{{ asset('assets/js/profile_mobile.js') }}"></script>
        @endif
        @if(Request::is('notifications*'))
            <script type="text/javascript" src="{{ asset('assets/js/notifications.js') }}"></script>
        @endif

        @if(Request::is('chat*'))
            <script src="https://cdn.socket.io/socket.io-1.3.7.js"></script>
            <script type="text/javascript" src="{{ asset('assets/js/socket/user_chat.js') }}"></script>
        @endif

        {{--<script type="text/javascript" src="{{{ asset('assets/js/cropbox.min.js') }}}"></script>--}}
        {{--<script type="text/javascript" src="{{{ asset('assets/js/crope-script.js') }}}"></script>--}}
        {{--<script type="text/javascript" src="{{{ asset('assets/js/img_cropper.js') }}}"></script>--}}
        @yield('scripts')

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72003733-1', 'auto');
            ga('send', 'pageview');

        </script>

    </body>
</html>
