@extends('site.layouts.default')
{{-- Web site Title --}}
@section('title')
{{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header clearfix">
    <h1>
        {{ $title }}
    </h1>
</div>
{{-- Create User Form --}}
{{ Form::open(array('url' => 'applicants/search', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'candidatSearchForm')) }}

<p class="bg-danger">{{ $errors->first('search_params') }}</p>
@foreach ($params as $mParam)
<div class="form-group {{ $errors->has('search_params') ? 'has-error' : '' }}">
    {{ Form::label('search_params', $mParam->getTranslatedParamTitle(App::getLocale()), ['class' => 'col-md-2 control-label']) }}
    <div class="col-lg-10 col-md-10 search-params-block">
        @include($mParam->getSearchTemplateByType(), ['param' => $mParam, 'name' => 'search_params', 'value' => $ranges[$mParam->id]])
        {{ $errors->first('profile_params_'.$mParam->id) }}
    </div>
</div>
@endforeach


<!-- Form Actions -->
<div class="form-group">
    <div class="col-lg-12">
        <button type="submit" class="btn btn-submit">{{ Lang::get('button.search') }}</button>
    </div>
</div>
<!-- ./ form actions -->
{{ Form::close() }}
@stop