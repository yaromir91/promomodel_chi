<div class="row">
    <div class="col-lg-12">
        <h3 class="no-border">{{ Lang::get('applicants.main_info') }}</h3>
        <ul>
            <li>
                <p>{{ Lang::get('profile.Username') }}:<span>&nbsp;{{$user->username}}</span></p>
            </li>
            @if(Auth::user() && !Auth::user()->hasRole('Applicant'))
                <li>
                    <p>{{ Lang::get('profile.Email') }}:<span>&nbsp;{{$user->email}}</span></p>
                </li>
            @endif
            @if ($profile instanceof Acme\Models\Profile\UserProfile)
            <li>
                <p>{{ Lang::get('profile.Full name') }}:<span>&nbsp;{{$profile->getFullName()}}</span></p>
            </li>
            <li>
                <p>{{ Lang::get('profile.Gender') }}:<span>&nbsp;{{Acme\Models\Profile\UserProfile::getUserGender($profile->gender)}}</span></p>
            </li>

            @if($profile->getAge())
            <li>
                <p>{{ Lang::get('profile.age') }}:<span>&nbsp;{{$profile->getAge()}}</span></p>
            </li>
            @endif

            @if($profile->phone && Auth::user() && !Auth::user()->hasRole('Applicant'))
                <li>
                    <p>{{ Lang::get('profile.phone') }}:<span>&nbsp;{{$profile->phone}}</span></p>
                </li>
            @endif

            @if($profile->vk_link && Auth::user() && !Auth::user()->hasRole('Applicant'))
                <li>
                    <p>{{ Lang::get('profile.vk_link') }}:<span>&nbsp; @if($profile->vk_link)
                <a target="_blank" href="{{$profile->vk_link}}">{{ Lang::get('profile.click_here') }}</a>
                @endif</span></p>
                </li>
            @endif

            @endif
            @if($applicant_code)
                <li>
                    <input data-rating-type="<?php echo(Acme\Models\Ratings::RATING_TYPE_USER);?>" class="g_rating" @if(!Auth::user() || $rate_read_only) data-readonly="true" @endif value="{{$user->getUserRating()}}" data-id="{{ $user->id }}" >
                    <div id="_token" token="{{ csrf_token() }}"></div>
                </li>
            @endif

            @include('site.applicants.partials.eventstypeinfo')
            @include('site.applicants.partials.profinfo')
            @include('site.applicants.partials.addition_params_info')
        </ul>
    </div>
</div>



