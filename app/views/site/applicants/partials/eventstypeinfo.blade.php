@if (count($user->eventstype))
<h3 class="no-border">{{ Lang::get('profile.eventtypes') }}</h3>
    @foreach ($eventsType as $type)
    @if ($user->eventstype->contains($type->id))
    <p>
        @if(App::getLocale() == 'en')
            {{ $type->title_en }}
        @elseif(App::getLocale() == 'ru')
            {{ $type->title }}
        @endif
    </p>
    @endif
    @endforeach
@endif