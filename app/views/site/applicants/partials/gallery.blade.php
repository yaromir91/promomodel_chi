@if($imagesCount>0)
<div class="slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php $i=1; ?>
            @foreach ($gallery as $g)
            <?php $images = $g->images; ?>
            @if(count($images))
            @foreach ($images as $img)
            <div class="item @if($i==1) active @endif">
                <img class="content-image" src="{{ asset(\Config::get('app.image_dir.album') . $g->user_id . '/small/' . $img->file) }}"  />
            </div>
            <?php $i++; ?>
            @endforeach
            @endif
            @endforeach
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">{{ Lang::get('gallery.Previous') }}</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">{{ Lang::get('gallery.Next') }}</span>
        </a>
    </div>
</div>
@else
    @if($profile->avatar)
        <img src="{{ asset(\Config::get('app.image_dir.profile') . $profile->avatar) }}">
    @endif
@endif