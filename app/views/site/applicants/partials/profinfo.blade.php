@if (is_object($profile) && count($profile->professions) && count($professions))
<h3 class="no-border">{{ Lang::get('profile.professions_rates') }}</h3>

    @foreach ($professions as $prof)

    @if ($profile->professions->contains($prof->id))
    <p>
        @if(App::getLocale() == 'en')
            {{ $prof->title_en }}:
        @elseif(App::getLocale() == 'ru')
            {{ $prof->title }}:
        @endif
        <span>&nbsp;{{Lang::get('profile.rate_h')}}: {{$profile->professions->find($prof->id)->pivot->rate_h}}</span>
        <span>&nbsp;{{Lang::get('profile.rate_d')}}: {{$profile->professions->find($prof->id)->pivot->rate_d}}</span>
    </p>
    @endif

    @endforeach

@endif