@if (count($mainParams) && is_object($profile) && count($profile->professions))
<h3 class="no-border">{{ Lang::get('profile.additional') }}</h3>
    @foreach ($mainParams as $mParam)
        @if($profile->params->contains($mParam->id))
            <p>
                @if(App::getLocale() == 'en')
                    {{ $mParam->name_en }}:
                @elseif(App::getLocale() == 'ru')
                    {{ $mParam->name }}:
                @endif<span>
                @include($mParam->getViewTemplateByType(), ['param' => $mParam, 'profile' => $profile, 'value' => $profile->getParamValue($mParam)])
            </span></p>
        @endif
    @endforeach
@endif