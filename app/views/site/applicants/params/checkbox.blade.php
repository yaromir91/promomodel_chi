@if ($param instanceof Acme\Models\Profile\ProfileParams)

    @if (count($param->children))
        @foreach($param->children as $pchild)
            <?php
            $savedVal = $profile->checkValueCheckbox($param->id, $pchild->id);
            $valueC   = Input::old('profile_params.'.$pchild->id, $savedVal);
            ?>
            @if ($valueC)
                {{ $pchild->name }}
            @endif
        @endforeach
    @endif
@endif