@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('user/user.events') }} ::
    @parent
@stop

{{-- Search User --}}
@section('content')
    <hr class="featurette-divider">
    <div class="row">

        <input type="hidden" id="_token" name="csrf_token" value="{{ csrf_token() }}" />
        @if(!empty($result) && (count($result) > 0))

            @foreach ($result as $item)

                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="well well-sm">
                        <div class="row">
                            @if($item->avatar)
                                <div class="col-sm-6 col-md-4">
                                    <a target="_blank" href="{{URL::route('view_user_profile', [$item->user_id])}}">
                                        {{ HTML::image(Config::get('app.image_dir.profile') . $item->avatar, $item->avatar, ['class' => 'img-rounded img-responsive']) }}
                                    </a>
                                </div>
                                @else
                                <div class="col-sm-6 col-md-4">
                                    <a target="_blank" href="{{URL::route('view_user_profile', [$item->user_id])}}">
                                        {{ HTML::image('//placehold.it/360x330?text=no photo', $item->avatar, ['class' => 'img-rounded img-responsive']) }}
                                    </a>
                                </div>
                            @endif
                            <div class="col-sm-6 col-md-8">
                                <a target="_blank" href="{{URL::route('view_user_profile', $item->user_id)}}">
                                    <h4> {{ $item->first_name}} {{ $item->last_name}} </h4>
                                </a>
                                <!-- <i class="glyphicon glyphicon-map-marker"></i><cite title="San Francisco, USA"></cite> -->
                                <p>
                                    @if($item->email)<i class="glyphicon glyphicon-envelope"></i> {{ $item->email }} @endif
                                    @if($item->vk_link )<br /><i class="glyphicon glyphicon-globe"></i><a href="{{ $item->vk_link }}"> {{ $item->vk_link }}</a> @endif
                                    @if($item->date_birth)<br /><i class="glyphicon glyphicon-gift"></i> {{ $item->date_birth }} @endif
                                </p>

                                @if($item->status !== 'not_active' || $userInvitation[$item->user_id])
                                    <div class="invite-the-user">
                                        <input type="hidden" value="{{$item->user_id}}" name="user_id"/>
                                        <input class="btn btn-submit" type="submit" data-toggle="modal"
                                               data-target="#ajaxResponse" data-user="{{$item->user_id}}"
                                               value="{{ Lang::get('groups.invite') }}"/>
                                        <button class="btn btn-warning hidden" id="{{ $item->user_id }}" disabled>{{ Lang::get('groups.invite_not_active') }}</button>
                                    </div>
                                @else
                                    <div class="invite-the-user">
                                        <button class="btn btn-warning" disabled>{{ Lang::get('groups.invite_not_active') }}</button>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <h2>{{ Lang::get('events.user_not_found') }}</h2>
        @endif
    </div>


    <div id="ajaxResponse" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p>{{ Lang::get('groups.invite_description') }}</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="invite_user" value="{{ URL::route('invite_user_post') }}">
                    <input type="hidden" name="user_id" value="" />
                    <input type="hidden" name="group_id" value="{{ $group->id }}" />
                    <button type="button" class="btn btn-submit" data-dismiss="modal" data-res="not">{{ Lang::get('button.close') }}</button>
                    <button type="button" class="btn btn-submit" data-res="yes">{{ Lang::get('groups.invite') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop