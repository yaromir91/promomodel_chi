@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('groups.title') }} ::
    @parent
@stop


@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('groups.title_members')}}</h1>
    </div>
    <div class="pull-right">
        <a href="{{ URL::route('show_group', $group->id) }}"
           class="btn btn-submit">{{ Lang::get('button.back') }}</a>
    </div>
    <div class="row group-members">
        @if(!empty($members))
            @foreach ($members as $member)
                @if($member->users->getMainUserRoleCode() != \Acme\Models\Role::AGENT_USER)
                    <div class="col-lg-3 col-md-3 col-sm-4 item">
                        <div class="photo-album group-album">
                            <a href="{{ URL::route('view_user_profile' , $member->user_id) }}">
                                <img alt="Generic placeholder image"
                                     src="@if($member->profile ) {{ asset(\Config::get('app.image_dir.profile') . $member->profile->avatar) }}  @endif"
                                     class="content-image">
                            </a>
                            @if(Auth::id() == $group->users->id && $member->user_id !== Auth::id())
                                <div class="deleteUser">

                                    <a href="#" role="button"
                                       data-href="{{ URL::route('group_unsubscribe' , [$member->groups_id, $member->user_id]) }}"
                                       data-toggle="modal" data-target="#confirm-unsubscribe"
                                       >{{ Lang::get('button.delete') }}</a>
                                </div>


                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
    @if(!empty($members)) {{ $members->appends(Request::input())->links() }} @endif


    @if(Auth::check())
    <!-- Unsubscrbe -->
    <div class="modal fade" id="confirm-unsubscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ Lang::get('groups.subscribe_unsubscribe') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-submit"
                            data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a href="#" class="btn btn-submit btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>
    <!--  ./ Unsubscrbe -->
    @endif
@stop
