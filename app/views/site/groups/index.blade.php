@extends('site.layouts.default')

@section('title')
    {{ Lang::get('groups.title') }} ::
    @parent
@stop

@section('meta_description'){{ Lang::get('groups.title') }}@stop

<!-- Content -->
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('groups.title')}}</h1>
    </div>
    @if(Auth::user()->getMainUserRoleCode() == \Acme\Models\Role::APPLICANT_MANAGER_USER
    || Auth::user()->getMainUserRoleCode() == \Acme\Models\Role::AGENT_USER)
    <div class="pull-right">
        <a href="{{ URL::route('add_group') }}" class="btn btn-submit iframe">{{ Lang::get('button.create') }}</a>
    </div>
    @endif

     <!-- Tab navigation group -->
    @include('site.groups._partials._nav_tab')
    <!-- ./ Tab navigation group -->

    <!-- Tab groups -->
    <div class="tab-content group-content">
        @if(Route::currentRouteNamed('groups'))
            <!-- Tab All groups -->
            <div class="tab-pane active" >
                <div class="row">
                    @if(!empty($groups))
                        @foreach ($groups as $group)
                            <div class="col-lg-3 col-md-3 col-sm-4 item">
                                <div class="photo-album">
                                    <a href="{{ URL::route('show_group' , $group->id) }}"><img class="content-image" alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.groups_small') . $group->image) }}"></a>
                                </div>
                                <div class="photo-info">
                                    <div class="line-info">
                                        <span>{{ Lang::get('groups.title_title') }}:&nbsp;</span><a  href="{{ URL::route('show_group' , $group->id) }}">{{ $group->title }}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                @if(!empty($groups)) {{ $groups->appends(Request::input())->links() }} @endif
            </div>
            <!-- ./ Tab All groups -->
        @endif
        @if(Route::currentRouteNamed('my_group'))
            <!-- Tab My groups -->
            <div class="tab-pane active" >
                <div class="row">
                    @if(!empty($groups))
                        @foreach ($groups as $group)
                            <div class="col-lg-3 col-md-3 col-sm-4 item">
                                <div class="photo-album">
                                    <a href="{{ URL::route('show_group' , $group->groups->id) }}"><img class="content-image" alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.groups_small') . $group->groups->image) }}"></a>
                                </div>
                                <div class="photo-info">
                                    <div class="line-info">
                                        <span>{{ Lang::get('groups.title_title') }}:&nbsp;</span><a href="{{ URL::route('show_group' , $group->groups->id) }}">{{ $group->groups->title }}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- ./ Tab My groups -->
        @endif
    </div>
    <!-- ./ Tab groups -->
@stop
<!-- ./ Content -->