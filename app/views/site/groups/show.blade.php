@extends('site.layouts.default')

@section('title')
    {{ $title }} :: @parent
@stop

@section('meta_description'){{ Lang::get('groups.title') }} - {{ $group->title }}@stop

{{-- Content --}}
@section('content')
    <!-- Tabs Content -->
    <div class="page-header clearfix">
        <h1>{{ $group->title }}</h1>
    </div>
    <div class="pull-right">
        <a href="{{ URL::to('groups') }}"
           class="btn btn-submit">{{ Lang::get('button.back') }}</a>
    </div>
    {{-- Panel for edit group --}}
    @if(Auth::user()->id == $group->users->id)
    <div class="pull-right">
        <a href="{{ URL::route('invite_to_group' , $group->id) }}" class="btn btn-submit">
            {{ Lang::get('button.invite') }}
        </a>
        <a href="{{ URL::route('edit_group' , $group->id) }}" class="btn btn-submit">
            {{ Lang::get('button.edit') }}
        </a>
        <a href="#" class="btn btn-submit" data-toggle="modal" data-target="#confirm-delete-group">
            {{ Lang::get('button.delete') }}
        </a>
        {{--<a href="#" class="btn @if(is_null($currentSubscriber)) btn-submit @else btn-submit @endif" data-toggle="modal" data-target="#confirm-delete-group">--}}
            {{--{{ Lang::get('button.unsubscribe') }}--}}
        {{--</a>--}}
    </div>
    @else
    @if(!is_null($currentSubscriber))
    <div class="pull-right">
        <a href="#" class="btn btn-submit" data-toggle="modal" data-target="#subscribe">
            {{ Lang::get('button.unsubscribe') }}
        </a>
    </div>
    @endif
    @endif

    @include('site.groups._partials._nav_tab')

    <div class="content group-content">
        <!-- ./ Panel for edit group -->
        <!-- Members -->
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="group-photo">
                    @if($group->image)
                        <img alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.groups_preview') . $group->image) }}">
                    @endif
                </div>
                <div class="group-description">
                    <h3>{{ Lang::get('groups.title_description') }}</h3>
                    <p>{{ $group->description }}</p>
                </div>
                <div class="group-description">
                    @if($subscribers->count() > 1)
                    <h3 class="group-list-name"><a href="{{ URL::route('group_members', $group->id) }}">{{ Lang::get('groups.group_members') }}</a></h3>
                    <div class="col-lg-12 text-center">
                        <div class="row">
                            @foreach($subscribers as $subscriber)
                            @if(!$subscriber->checkAgent())
                            <div class="col-lg-4">
                                <a href="{{ URL::route('view_user_profile', $subscriber->user_id) }}"><img class="user-avatar-mini" src="@if(!empty($subscriber->profile->avatar)) {{ asset(Config::get('app.image_dir.profile') . $subscriber->profile->avatar)  }}@endif" alt=""></a>
                                <a href="{{ URL::route('view_user_profile', $subscriber->users->id) }}"><p class="user-name">{{ $subscriber->users->username }}</p></a>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h3 class="no-border">{{ Lang::get('groups.title_groups_details') }}</h3>
                <div class="row">
                    <div class="col-lg-4">
                       {{--<p>{{ Lang::get('groups.title_groups_type') }}:&nbsp;</p>--}}
                        <p>{{ Lang::get('groups.title_groups_owner') }}:&nbsp;</p>
                    </div>
                    <div class="col-lg-8">
                        {{--<p><b>{{ $group->title }}</b></p>--}}
                        <p>
                            <b>
                                <a href="{{ URL::to('profile/view/' . $group->users->id) }}">
                                    {{ $group->users->username }} {{ $group->users->id !== Auth::user()->id ? '' :'(Вы)' }}
                                </a>
                            </b>
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <!-- ./ Members -->
        <div id="_token" token="{{ csrf_token() }}"></div>
        <div class="row add-comment">
            <div class="col-lg-12">
                <h3>{{ Lang::get('report.add_comment') }}</h3>
                {{ Form::open(array('url' => 'groups/create/comment/' . $group->id, 'method' => 'post', 'files' => true, 'autocomplete' => 'off')) }}
                <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                    <div class="col-lg-10 col-md-10 col-sm-9">
                        {{ Form::textarea('comment', '', array('class' => 'form-control', 'id' => 'comment', 'rows' => '5')) }}
                        {{ $errors->first('comment') }}
                    </div>
                    <!-- Form Actions -->
                    <div class="col-lg-2 col-md-2 col-sm-3">
                        <button type="submit" class="btn btn-submit">{{Lang::get('button.add')}}</button>
                    </div>
                    <!-- ./ form actions -->
                </div>
                <!-- Image -->
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        {{ Form::file('image', array('id' => 'MainCommentImage')) }}
                        {{ $errors->first('image', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ image -->
                {{ Form::close() }}
            </div>
        </div>
        @if ($comments->count())
            @foreach ($comments as $comment)
                <div class="row" id="comment_{{ $comment->id }}">
                    <div class="col-lg-12 comment-text">
                        <h4>{{ $comment->author->username; }}</h4>
                        <p id="comment_content_block_{{ $comment->id }}">{{ $comment->content(); }}</p>
                        @if($comment->image)
                            <p><img class="img-thumbnail comment-img" src="{{ asset(\Config::get('app.image_dir.comment') . $comment->id . '/'  . $comment->image) }}" alt=""></p>
                        @endif
                        <span>{{ $comment->date(); }}</span>
                        @if($comment->user_id == $user->id || $is_admin)
                            <div class="pull-right">
                                <a href="#" data-toggle="modal" data-target="#comment-edit-form_{{ $comment->id }}">
                                    <span>{{ Lang::get('report.comment_edit') }} </span>
                                </a>|
                                <a href="#" data-href="{{ URL::to('groups/comment/delete/' . $comment->id) }}" data-id="{{ $comment->id }}" data-toggle="modal" data-target="#confirm-delete-comment">
                                    <span>{{ Lang::get('report.comment_delete') }}</span>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                {{-- COMMENT EDIT FORM MODAL--}}
                <div class="modal fade" id="comment-edit-form_{{ $comment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            {{ Form::open(array('url' => 'groups/edit/comment/' . $comment->id, 'method' => 'post', 'files' => true, 'autocomplete' => 'off')) }}
                                <div class="modal-body">
                                    <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                                        <div class="col-lg-12 col-md-10 col-sm-9">
                                            {{ Form::textarea('comment', strip_tags($comment->content()), array('class' => 'form-control', 'id' => 'comment_content_' . $comment->id, 'rows' => '5')) }}
                                            {{ $errors->first('comment') }}
                                        </div>
                                    </div>
                                    <!-- Image -->
                                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            {{ Form::file('image') }}
                                            {{ $errors->first('image', '<span class="help-block">:message</span>') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                                    <button type="submit" class="btn btn-submit">{{ Lang::get('button.update') }}</button>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
                {{-- COMMENT EDIT FORM MODAL--}}
            @endforeach
        @endif
    </div>


        <!-- ./ Comments -->
    <!-- Subscibe -->
    <div class="modal fade" id="subscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    @if(!is_null($currentSubscriber))
                        {{ Lang::get('groups.unsubscribe') }}
                    @else
                        {{ Lang::get('groups.subscribe') }}
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    @if(!is_null($currentSubscriber) && Auth::id() == $group->users->id)
                        <a href="{{ URL::route('group_unsubscribe' , $group->id) }}" class="btn btn-submitt btn-ok">{{ Lang::get('button.unsubscribe') }}</a>
                    @else
                        <a href="{{ URL::route('group_unsubscribe' , $group->id) }}" class="btn btn-submit btn-ok">{{ Lang::get('button.unsubscribe') }}</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- ./ Subscibe -->
    <!-- Delete -->
    <div class="modal fade" id="confirm-delete-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ Lang::get('groups.delete_confirmation') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a href="{{ URL::route('group_delete' , $group->id) }}" class="btn btn-submit btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ./ Delete -->

    @if(isset($subscribers))
        {{-- COMMENT DELETE ALERT MODAL--}}
        <div class="modal fade" id="confirm-delete-comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        {{ Lang::get('groups.delete_comment_confirm') }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                        <a class="btn btn-submit" id="btn_delete_comment">{{ Lang::get('button.delete') }}</a>
                    </div>
                </div>
            </div>
        </div>
        {{-- COMMENT DELETE ALERT MODAL--}}
    @endif
    <!-- ./ tabs content -->

@stop