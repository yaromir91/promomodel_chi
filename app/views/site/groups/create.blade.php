@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>
            {{ $title }}
        </h1>
    </div>
	{{-- Create Group Form --}}
    {{ Form::open(array('route' => 'add_group', 'method' => 'post', 'autocomplete' => 'off', 'class' => 'form-horizontal avatar-form', 'id' => 'EventForm', 'files'=> true)) }}

			{{-- General tab --}}
			<div class="tab-pane active" id="tab-general">
                {{-- Title --}}
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('title', Lang::get('groups.title_title'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::text('title', '', array('class' => 'form-control', 'id' => 'title')) }}
                        {{ $errors->first('title', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                {{-- ./ title --}}
                {{-- Description --}}
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('description', Lang::get('groups.title_description'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::textarea('description', '', array('class' => 'form-control', 'id' => 'description')) }}
                        {{ $errors->first('description', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                {{-- ./ description --}}
                {{-- Image --}}
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('image', Lang::get('groups.title_photo'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">

                        <?php
                            $tmpSmallPicFile = (Input::old('new_picture', '')) ? asset(\Config::get('app.image_dir.groups_small') . Input::old('new_picture', '')) : '';
                            $tmpSmallPicName = Input::old('new_picture', '');
                        ?>

                        @include('site.profile.applicant.user_avatar', [
                            'fileFieldName'   => 'image',
                            'actionUrlUpload' => '/groups/upload',
                            'origImage'       => $tmpSmallPicFile,
                            'origImageName'   => $tmpSmallPicName,
                        ])
                        {{ $errors->first('image', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                {{-- ./ image --}}
			{{-- ./ general tab --}}

            <!-- Form Actions -->
            <div class="form-group">
                <div class="col-lg-2 col-md-2 col-sm-4">
                    <button type="button" class="btn btn-submit" onclick="document.location.href='{{ URL::to('groups') }}'">{{ Lang::get('button.cancel') }}</button>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-8">
                    <button type="submit" class="btn btn-submit" >{{ Lang::get('button.create') }}</button>
                </div>
            </div>
        </div>

    <!-- ./ form actions -->
    {{ Form::close() }}
@stop
