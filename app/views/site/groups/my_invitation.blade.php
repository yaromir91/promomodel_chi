@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('user/user.groups') }} ::
    @parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('groups.title_groups_list') }}</h1>
    </div>

    @include('site.groups._partials._nav_tab')

    <div class="tab-content group-content">
        @if($isShowInvitation)
            <div class="tab-pane tab-user-invitation active" id="tab-invite">
                @include('site.groups._partials.user_invitations')
            </div>
        @endif
    </div>

@stop

    
