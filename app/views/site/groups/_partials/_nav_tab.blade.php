<div class="tab-navigation">
    <ul class="nav nav-tabs">
        <li{{Route::currentRouteNamed('groups') ? ' class="active"': ''}}>
            <a href="{{ URL::route('groups') }}" >{{ Lang::get('groups.tab_all') }}</a>
        </li>
        @if(Auth::check() && !Auth::user()->hasRole('admin') && !Auth::user()->hasRole('Organizer'))
            <li{{Route::currentRouteNamed('my_group') ? ' class="active"': ''}}>
                <a href="{{ URL::route('my_group') }}" >{{ Lang::get('groups.tab_subscribe_group') }}</a>
            </li>
            @if(!Auth::user()->hasRole('Agent'))
                <li{{Route::currentRouteNamed('group_my_invitation') ? ' class="active"': ''}}>
                    <a href="{{ URL::route('group_my_invitation') }}" >{{ Lang::get('groups.tab_my_invitation') }}</a>
                </li>
            @endif
        @endif
    </ul>
</div>