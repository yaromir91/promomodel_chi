@if(count($userInvitations))
    @foreach ($userInvitations as $invite)
        @if (is_object($invite))
            <div class="row margin-t-20 one-group-{{$invite->id}}">
                <div class="col-lg-4">
                    <img class="content-image" alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.groups_small') . $invite->image) }}">
                </div>
                <div class="col-lg-8">
                    <h2>{{ $invite->title }}</h2>
                    <p><a role="button" href="{{ URL::to('groups/show/' . $invite->group_id) }}" class="btn btn-submit">{{ Lang::get('button.view_details') }} »</a></p>

                        <p>
                            <a role="button" data-event-target=".one-event-{{$invite->id}}" data-target="#new-confirm-accept" data-href="{{ URL::route('invite_user_get_success', [$invite->token]) }}"  class="btn-send-invitation-answer btn btn-submit model accept-invitation">
                                {{ \Lang::get('button.accept') }}
                            </a>
                        </p>

                        <p><a role="button" data-event-target=".one-event-{{$invite->id}}" data-target="#new-confirm-not-accept" data-href="{{ URL::route('invite_user_get_refuse', [$invite->token]) }}" class="btn-send-invitation-answer btn btn-submit model accept-reject">{{ Lang::get('button.reject_all') }}</a></p>

                </div>
            </div>
        @endif
    @endforeach
@else
    <p class="invitations-message">{{ Lang::get('events.invitation_not_found') }}</p>
@endif

<div id="_token" token="{{ csrf_token() }}"></div>

<div id="new-confirm-accept" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn btn-submit" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ Lang::get('groups.invite_accept_question') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                <a role="button" data-target-answer="#new-invitation-answer" class="btn btn-primary send-accept">{{ Lang::get('button.yes') }}</a>
            </div>
        </div>
    </div>
</div>

<div id="new-confirm-not-accept" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn btn-submit" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ Lang::get('events.you_want_to_reject_invitation') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                <a role="button" data-target-answer="#new-invitation-answer" class="btn btn-primary send-accept">{{ Lang::get('button.yes') }}</a>
            </div>
        </div>
    </div>
</div>

<div id="new-invitation-answer" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
            </div>
        </div>
    </div>
</div>