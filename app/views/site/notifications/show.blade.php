@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ $title }}</h1>
    </div>

    <div class="pull-right">
        <a class="btn btn-submit" href="{{ URL::to('profile/show') }}">{{ Lang::get('button.back') }}</a>
    </div>

    <div class="content">
        <div class="row margin-t-70">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ \Lang::get('notifications.who_send') }}</th>
                                <th>{{ \Lang::get('notifications.data') }}</th>
                                <th>{{ \Lang::get('notifications.type') }}</th>
                                <th>{{ \Lang::get('notifications.text') }}</th>
                                <th>{{ \Lang::get('notifications.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notifications as $notification)
                                <tr @if(!$notification->is_read) class="notification_bold" @endif>
                                    <td>
                                        <a target="_blank" href="{{ URL::to('profile/view/' . $notification->user_id) }}">{{ $notification->username }}</a>
                                    </td>
                                    <td>{{ $notification->updated_at }}</td>
                                    <td>
                                        {{--<a target="_blank" href="{{ \Acme\Models\Repositories\NotificationRepository::getTypeHref($notification->type, $notification->user_id) }}">--}}
                                            {{ \Acme\Models\Repositories\NotificationRepository::getTypeTitle($notification->type) }}
                                        {{--</a>--}}
                                    </td>
                                    <td>{{ $notification->body }}</td>
                                    <td>
                                        @if(!$notification->is_read)
                                            <a href="{{ URL::route('notification_read', $notification->id) }}">{{ \Lang::get('notifications.read') }}   </a>
                                            <br />
                                        @endif
                                        <a href="#" data-href="{{ URL::route('notification_delete', $notification->id) }}" data-toggle="modal" data-target="#confirm-delete-notification">
                                            {{ Lang::get('button.delete') }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
{{-- NOTIFICATION DELETE ALERT MODAL--}}
<div class="modal fade" id="confirm-delete-notification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                <a class="btn btn-default btn-ok">{{ Lang::get('button.delete') }}</a>
            </div>
        </div>
    </div>
</div>
{{-- NOTIFICATION DELETE ALERT MODAL--}}