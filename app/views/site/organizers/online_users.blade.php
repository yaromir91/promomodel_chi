@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ Lang::get('organizers.online_users') }} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header clearfix">
    <h1>{{Lang::get('organizers.online_users')}}</h1>
</div>

<div class="filter-block pull-right">
    <a class="btn btn-submit iframe" href="{{ URL::to('organizers') }}">{{Lang::get('user/user.filter_all')}}</a>
    <a class="btn btn-submit iframe" href="{{ URL::route('show_online_organizers') }}">{{Lang::get('user/user.filter_online')}}</a>
</div>

<div class="content user-content">
    @if(count($result))
    <div class="row applicants-list">
        @foreach($result as $elem)
        <div class="col-lg-3 col-md-3 col-sm-4 one-applicant" data-id={{$elem->user_id}}>
            <div class="photo-album">
                @if($elem->avatar)
                    <img class="content-image" src="{{ asset(\Config::get('app.image_dir.profile') . $elem->avatar) }}">
                @else
                    {{\Config::get('app.album_no_image_no_class')}}
                @endif
            </div>
            <div class="photo-info">
                <span>{{Lang::get('user/user.zizaco_user_name')}}:&nbsp;</span>{{ $elem->first_name }} {{ $elem->last_name }} ({{ Acme\Models\Profile\UserProfile::getUserGender($elem->gender) }})
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>

@stop
