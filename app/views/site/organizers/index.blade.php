@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('organizers.title') }} ::
    @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('organizers.title')}}</h1>
    </div>


    <div class="filter-block pull-right">
        <a class="btn btn-submit iframe" href="{{ URL::to('organizers') }}">{{Lang::get('user/user.filter_all')}}</a>
        <a class="btn btn-submit iframe" href="{{ URL::route('show_online_organizers') }}">{{Lang::get('user/user.filter_online')}}</a>
    </div>

    <div class="content user-content">
        <div class="row applicants-list">
            @foreach($list as $k=>$elem)
                <div class="col-lg-3 col-md-3 col-sm-4 one-applicant" data-id={{$elem->user_id}}>
                    <div class="photo-album">
                        @if($elem->avatar)
                            <img class="content-image" src="{{ asset(\Config::get('app.image_dir.profile') . $elem->avatar) }}">
                        @else
                            <img class="content-image" src="{{ asset(\Config::get('app.image_dir.stub')) }}">
                        @endif
                    </div>
                    <div class="photo-info block-name">
                        {{ $elem->first_name }} {{ $elem->last_name }} ({{ Acme\Models\Profile\UserProfile::getUserGender($elem->gender) }})
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@stop
