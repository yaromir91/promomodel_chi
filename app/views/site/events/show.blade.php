@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

@section('meta_description'){{ $event->description }}@stop

{{-- Content --}}
@section('content')
    @if($paidStatus)
        <div class="alert alert-{{ $paidStatus['status'] }} alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>{{ $paidStatus['msg'] }}</h4>
        </div>
    @endif

    <div class="page-header clearfix">
        <h1>{{ $title }}</h1>
    </div>
    <!-- Tabs Content -->
    <div class="content event-content">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="event-photo">
                    <img  alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.events_preview') . $event->image) }}" class="photo-main">
                </div>
                <div class="event-description">
                    <h3>{{ Lang::get('events.title_description') }}</h3>
                    <p>{{ $event->description }}</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                @if($event->casting)<p><b>{{ Lang::get('events.title_casting') }}</b></p>@endif

                @if($member_prof)
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                            <p>{{ Lang::get('events.title_events_member_profession') }}:</p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                            <p><b>{{ $member_prof }}</b></p>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <p>{{ Lang::get('events.title_events_start_date') }}:</p>
                    </div>
                    <div class="col-lg-8 col-md-4 col-sm-6 col-xs-6">
                        <p><b>{{ $event->date_start }}</b></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <p>{{ Lang::get('events.title_events_end_date') }}:</p>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                        <p><b>{{ $event->date_end }}</b></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <p>{{ Lang::get('events.title_events_paid_status') }}:</p>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                        <p><b>{{ \Acme\Models\Repositories\EventsRepository::getPaimentStatusTitle($event->paiment_status) }}</b></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <p>{{ Lang::get('events.title_events_place') }}:</p>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                        <p><b>{{ $event->location }}</b></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <p>{{ Lang::get('events.title_events_type') }}:</p>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                        <p><b>{{ $event_type->title }}</b></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <p>{{ Lang::get('events.title_events_owner') }}:</p>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                        <p><b><a target="_blank" href="{{ URL::to('profile/view/' . $event->users_id) }}">{{ $user->username }}</a></b></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <p>{{ Lang::get('events.title_events_owner_status') }}:</p>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                        <p>
                            <b>{{ Lang::get('events.events_owner_status.' . $event->presence_owner)}}</b>
                            @if($current_user && $current_user->id == $event->users_id && strtotime($event->date_end) > time())
                                <a href="#" data-toggle="modal" data-target="#change_status_owner" data-href="{{ URL::route('event_change_status_owner' , $event->id) }}" class="btn btn-submit pull-right">
                                    {{ Lang::get('button.change_status') }}
                                </a>
                            @else
                                @if($event->presence_owner == 0)
                                    <a href="#" data-toggle="modal" data-target="#get_status_owner" class="btn btn-submit pull-right">
                                        {{ Lang::get('button.show_instruction') }}
                                    </a>
                                @endif
                            @endif
                        </p>
                    </div>
                </div>

                @if($current_user && $current_user->id == $event->users_id)
                    @if($event->paiment_status == \Acme\Models\Events\Events::PAIMENT_STATUS_NEW || $event->paiment_status == \Acme\Models\Events\Events::PAIMENT_STATUS_HOLD || $event->paiment_status == \Acme\Models\Events\Events::PAIMENT_HOLD_ACCEPT)
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <p>{{ Lang::get('events.event_price') }}:</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                                <p><b>{{ $event->getTotalPrice(true) }} {{ Lang::get('events.RUB') }}</b></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <p>{{ Lang::get('events.commission_website') }}:</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                                <p><b>{{ Config::get('app.payment_percent_service')}}%</b></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <p><b>{{ Lang::get('events.all_total_price') }}:</b></p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
                                <p><b>{{ $event->getTotalPriceWithServicePercent() }} {{ Lang::get('events.RUB') }}</b></p>
                            </div>
                        </div>
                    @endif
                @endif

                <div class="row button-setting">
                    @include($button_template)
                </div>
            </div>
        </div>
        <div id="_token" token="{{ csrf_token() }}"></div>
        <div class="row event-info">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                {{--<h3>{{ Lang::get('events.title_events_data') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>{{$event->date_start}} - {{$event->date_end}}</span></h3>--}}

                @if($profession)
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>{{ Lang::get('events.title_events_profession') }}</th>
                                    <th>{{ Lang::get('events.title_events_need') }}</th>
                                    <th>{{ Lang::get('events.title_events_accepted') }}</th>
                                    <th>{{ Lang::get('events.title_events_reserved') }}</th>
                                    <th>{{ Lang::get('events.title_events_h_rate') }}</th>
                                    <th>{{ Lang::get('events.title_events_d_rate') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($profession as $p)
                                    <tr>
                                        <td scope="row">{{ $p->title }}</td>
                                        <td>{{ $p->count }}</td>
                                        <td>{{ $p->accepted_count }}</td>
                                        <td>{{ $p->reserved_count }}</td>
                                        <td>{{ $p->rate_h }}</td>
                                        <td>{{ $p->rate_d }}</td>
                                    </tr>
                                @endforeach
                                @if($event->manager_num)
                                    <tr>
                                        <td scope="row">{{ Lang::get('events.managers') }}</td>
                                        <td>{{ $event->manager_num }}</td>
                                        <td>{{ $event->getCompletedCountManager() }}</td>
                                        <td>{{ $event->getReservedCountManager() }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                @else
                    {{ Lang::get('events.message_04') }}
                @endif
            </div>
        </div>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                {{--<div class="modal-header">--}}
                    {{--header--}}
                {{--</div>--}}
                <div class="modal-body">
                    {{ Lang::get('events.delete_confirmation') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a class="btn btn-default btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-cancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                {{--<div class="modal-header">--}}
                {{--header--}}
                {{--</div>--}}
                <div class="modal-body">
                    {{ Lang::get('events.cancel_confirmation') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a class="btn btn-default btn-ok">{{ Lang::get('button.ok') }}</a>
                </div>
            </div>
        </div>
    </div>
    @include('site.events._partials.change_status_owner', ['event_id' => $event->id])
    {{-- Check status for modal --}}
    @if($event->presence_owner == 0)
        @include('site.events._partials.get_status_owner', ['event_id' => $event->id])
    @endif
</div>
    <!-- ./ tabs content -->

@stop