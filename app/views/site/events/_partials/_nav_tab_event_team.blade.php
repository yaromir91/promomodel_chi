<div class="tab-navigation">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab-main-team" data-toggle="tab">{{ Lang::get('events.main_team') }}</a>
        </li>
        <li>
            <a href="#tab-reserved-team" data-toggle="tab">{{ Lang::get('events.reserved_team') }}</a>
        </li>
    </ul>
</div>
