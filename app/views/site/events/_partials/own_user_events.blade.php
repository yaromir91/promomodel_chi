@if(count($myPublicEvents))
    <div class="row event-content ">
        @foreach ($myPublicEvents as $index => $event)
            <div class="col-lg-3 col-md-3 col-sm-4 one-event-{{$event->id}} item">
                <div class="photo-album">
                    <a href="{{ URL::to('events/show/' . $event->id) }}">
                        <img class="content-image"  alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.events_small') . $event->image) }}">
                    </a>
                </div>
                <div class="photo-info photo-event-info">
                    <div class="line-info">
                        <span>{{ Lang::get('events.title_title') }}:&nbsp;</span><a href="{{ URL::to('events/show/' . $event->id) }}">{{ $event->title }}</a>
                    </div>
                    <div class="line-info">
                        <span>{{ Lang::get('events.title_events_start_date') }}:</span>{{ $event->date_start }}
                    </div>

                    @if($isShowInvitation)
                        @if(Acme\Helpers\MainHelper::checkDatesDiff($event->date_start))
                            @if($event->is_manager == 1 && $event->manager_status == Acme\Models\Events\EventInvite::INVITE_MANAGER_STATUS_NEW)
                                <div class="line-info"><a role="button" data-event-target=".one-event-{{$event->id}}" data-target="#confirm-accept" data-href="{{ URL::route('event_accept_set', [$event->id, 1, 1]) }}" class="btn-send-invitation-answer btn btn-submit model accept-invitation delAfter">{{ Lang::get('button.accept_manager') }}</a></div>
                            @endif

                            @if($event->is_manager == 1 && $event->manager_status == Acme\Models\Events\EventInvite::INVITE_MANAGER_STATUS_NEW)
                                <div class="line-info"><a role="button" data-event-target=".one-event-{{$event->id}}" data-target="#confirm-not-accept" data-href="{{ URL::route('event_accept_set', [$event->id, 0, 1]) }}" class="btn-send-invitation-answer btn btn-submit model accept-reject delAfter">{{ Lang::get('button.reject_manager') }}</a></div>
                            @endif

                            <div class="line-info"><a role="button" data-event-target=".one-event-{{$event->id}}" data-target="#confirm-not-accept" data-href="{{ URL::route('event_accept_set', [$event->id, 0, 0]) }}" class="btn-send-invitation-answer btn btn-submit model accept-reject">{{ Lang::get('button.reject_all') }}</a></div>
                        @endif
                    @endif
                </div>

            </div>

        @endforeach
    </div>
@else
    <p class="info-message">{{ Lang::get('events.events_not_found') }}</p>
@endif
<div id="_token" token="{{ csrf_token() }}"></div>
@if($isShowInvitation)
    <div id="confirm-accept" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ Lang::get('events.you_want_to_confirmation_manager_invitation') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                    <a role="button"   data-target-answer="#invitation-answer" class="btn btn-primary send-accept notDel">{{ Lang::get('button.yes') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div id="confirm-not-accept" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ Lang::get('events.you_want_to_reject_invitation') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                    <a role="button"   data-target-answer="#invitation-answer" class="btn btn-primary send-accept">{{ Lang::get('button.yes') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div id="invitation-answer" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                </div>
            </div>
        </div>
    </div>
@endif
