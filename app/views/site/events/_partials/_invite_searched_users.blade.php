<div class="invite-the-user">
    <?php $theInvitation = null;?>
    @if (!$item->user->isSentEventInvited($event->id, $theInvitation))
        @if ($item->is_ready_manager == 1 && $event->manager_num > 0)
            <div class="checkbox">
                <label for="invite_manager_{{$item->user_id}}">
                    <input type="checkbox" name="is_manager" id="invite_manager_{{$item->user_id}}" />
                    {{ Lang::get('events.Manager') }}
                </label>
            </div>
        @endif

        @if (count($item->professions))
            <h4>{{ Lang::get('events.Professions') }}</h4>
            @foreach($item->professions as $ip)
                @if(($event->professionsRates->contains($ip)))
                    <div class="radio">
                        <label for="invite_profession_{{$item->user_id}}_{{$ip->id}}">
                            <input type="radio" value="{{$ip->id}}" id="invite_profession_{{$item->user_id}}_{{$ip->id}}" name="professions_id" />
                            {{$ip->title}}
                        </label>
                    </div>
                @endif
            @endforeach
        @endif

        <input type="hidden" value="{{$item->user_id}}" name="user_id" />
    @elseif(is_object($theInvitation))
        @if($theInvitation->is_manager == 1)
            @if($theInvitation->manager_status == \Acme\Models\Events\EventInvite::INVITE_MANAGER_STATUS_ACCEPT)
                    <p class="bg-success"><span>{{ Lang::get('events.manager_status') }}</span>: {{ Lang::get('events.invitation_manager_accepted') }}</p>
            @elseif($theInvitation->manager_status == \Acme\Models\Events\EventInvite::INVITE_MANAGER_STATUS_NEW)
                    <p class="bg-warning"><span>{{ Lang::get('events.manager_status') }}</span>: {{ Lang::get('events.invitation_sent') }}</p>
            @endif
        @endif
        @if($theInvitation->status == \Acme\Models\Events\EventInvite::INVITE_PROF_STATUS_ACCEPT)
                <p class="bg-success"><span>{{ Lang::get('events.status') }}</span>: {{ Lang::get('events.invitation_prof_accepted') }}</p>
        @elseif($theInvitation->status == \Acme\Models\Events\EventInvite::INVITE_PROF_STATUS_NEW)
            <p class="bg-warning"><span>{{ Lang::get('events.status') }}</span>: {{ Lang::get('events.invitation_sent') }}</p>
        @endif
    @endif
</div>