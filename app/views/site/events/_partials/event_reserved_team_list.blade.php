<div class="event-team-users-block">
    @if(count($reservedTeam))
        @foreach ($reservedTeam as $inv)
            <?php $user = $inv->user; ?>
            @if($user instanceof Acme\Models\User)
                <?php $profile = $user->profile; ?>
                <div class="row team-member-{{$inv->id}}">
                    @if($profile instanceof Acme\Models\Profile\UserProfile)
                        <div class="col-lg-3 col-md-3 col-sm-4 item">
                            <div class="photo-album">
                                <a href="{{URL::route('view_user_profile', $user->id)}}">
                                    @if($profile->avatar)
                                    <img class="content-image" src="{{ asset(\Config::get('app.image_dir.profile') . $profile->avatar) }}">
                                    @else
                                        {{\Config::get('app.album_no_image')}}
                                    @endif
                                </a>
                            </div>
                        </div>
                    @endif
                    <div class="col-lg-9 col-md-9 col-sm-8">
                        <a href="{{URL::route('view_user_profile', $user->id)}}">
                            <h2>{{ $user->getUserName() }}</h2>
                        </a>
                        <p><span>{{\Lang::get('events.status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentStatusTitle($inv->status) }} - {{ $inv->getProfessionName() }}</p>
                        @if($inv->is_manager == 1)
                            <p><span>{{\Lang::get('events.manager_status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentManagerStatusTitle($inv->manager_status) }}</p>
                        @endif
                        <p><span>{{\Lang::get('events.team_status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentTeamStatusTitle($inv->in_team) }}</p>
                        <p><span>{{\Lang::get('events.team_manager_status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentManagerTeamStatusTitle($inv->manager_in_team) }}</p>

                        @if(time() < strtotime($event->report_date))
                            @if($event->users_id == $authUser->id)
                                <p>
                                    <a
                                            role="button"
                                            data-event-target=".team-member-{{$inv->id}}"
                                            data-target="#confirm-delete-team-member-reserved"
                                            data-href="{{ URL::route('event_delete_team_member', [$inv->id]) }}"
                                            class="btn-send-invitation-answer btn btn-default model delAfter"
                                    >{{ Lang::get('button.delete') }}</a>
                                </p>
                            @endif
                            @if($event->isAllowToAddToTeam($inv->professions_id) && $event->users_id == $authUser->id && $inv->in_team != Acme\Models\Events\EventInvite::IS_IN_TEAM)
                                <p>
                                    <a
                                        role="button"
                                        data-target="#confirm-move-team-member-main"
                                        data-href="{{ URL::route('event_move_team_member', [$inv->id]) }}"
                                        class="btn btn-default model"
                                        data-toggle="modal"
                                    >{{ Lang::get('button.move_to_main') }}</a>
                                </p>
                            @endif
                            @if($event->isAllowToAddManagerToTeam($inv->users_id, true) && $event->getCompleteTeamM() && $event->users_id == $authUser->id)
                                <p>
                                    <a
                                            role="button"
                                            data-target="#confirm-move-team-member-main"
                                            data-href="{{ URL::route('event_move_manager_member', [$inv->id]) }}"
                                            class="btn btn-default model"
                                            data-toggle="modal"
                                    >{{ Lang::get('button.move_to_main_m') }}</a>
                                </p>&nbsp; <span>{{ Lang::get('events.message_01') }}</span>
                                @endif
                        @endif
                        <!--<input class="g_rating" @if(!Auth::user() || $user->id == Auth::user()->id) data-readonly="true" @endif value="{{$user->getUserRating()}}" data-id="{{ $user->id }}" >-->
                    </div>
                </div>
            @endif
        @endforeach
    @else
        <p class="info-message">{{ Lang::get('events.team_is_empty') }}</p>
    @endif

    <div id="_token" token="{{ csrf_token() }}"></div>

    <div id="confirm-delete-team-member-reserved" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ Lang::get('events.you_want_to_delete_team_member') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                    <a role="button" data-target-answer="#delete-answer-reserved" class="btn btn-primary send-accept">{{ Lang::get('button.yes') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div id="confirm-move-team-member-main" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ Lang::get('events.you_want_to_move_team_member') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                    <a role="button" class="btn btn-primary btn-ok">{{ Lang::get('button.yes') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-answer-reserved" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                </div>
            </div>
        </div>
    </div>

</div>