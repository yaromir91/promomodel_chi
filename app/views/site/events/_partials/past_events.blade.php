@if(count($pastPublicEvents))
    <div class="row event-content">
        @foreach ($pastPublicEvents as $index => $event)
        <div class="col-lg-3 col-md-3 col-sm-4 item">
            <div class="photo-album">
                <a href="{{ URL::to('events/show/' . $event->id) }}">
                    <img class="content-image" alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.events_small') . $event->image) }}">
                </a>
            </div>
            <div class="photo-info">
                <div class="line-info">
                  <span>{{ Lang::get('events.title_title') }}:&nbsp;</span><a href="{{ URL::to('events/show/' . $event->id) }}">{{ $event->title }}</a>
                </div>
                <div class="line-info">
                    <span>{{ Lang::get('events.title_events_start_date') }}:&nbsp;</span>{{ $event->date_start }}
                </div>
<!--                <div class="line-info">
                    <input data-rating-type="<?php echo(Acme\Models\Ratings::RATING_TYPE_EVENT);?>" class="g_rating" @if(!Auth::user()) data-readonly="true" @endif value="{{$event->getEventRating()}}" data-id="{{ $event->id }}" >
                </div>-->
            </div>


        </div>
        @endforeach
    </div>
@else
    <p class="info-message">{{ Lang::get('events.events_not_found') }}</p>
@endif

<div id="_token" token="{{ csrf_token() }}"></div>