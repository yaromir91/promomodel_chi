<div class="modal fade" id="change_status_owner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2> {{ Lang::get('events.title_events_owner_status') }}</h2>
            </div>
            <div class="modal-body">
                {{ Form::open(['route' => 'event_change_status_owner', 'method' => 'POST'])}}
                {{ Form::token() }}
                <div class="form-group">
                    <label for="status" class="control-label">{{ Lang::get('events.status') }}</label>
                    <select class="form-control" id="status" name="status">
                        <option value="1">{{ Lang::get('events.events_owner_status.1') }}</option>
                        <option value="0">{{ Lang::get('events.events_owner_status.0') }}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="content" class="control-label">{{ Lang::get('events.title_description') }}</label>
                    <textarea class="form-control" name="content" id="content" cols="30" rows="10" readonly="readonly"></textarea>
                    <span class="help-block"></span>
                </div>
                <input type="hidden" name="event" value="{{ $event_id }}">
                <input type="hidden" name="get_content" value="{{ URL::route('event_show_status_owner') }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                <button type="button" class="btn btn-submit btn-ok">{{ Lang::get('button.update') }}</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<script>
    window.addEventListener("load",function(event) {
        console.log(1111);
        $('#change_status_owner')
                .on('shown.bs.modal', function (e) {
                    console.log(1111);
                    get_information(this);
                    var self = this;
                    $('select').off('change').on('change' ,function(e){
                        console.log(111);
                        if(e.target.selectedIndex == 0){
                            $(self).find('textarea').attr('readonly','readonly')
                        } else {
                            $(self).find('textarea').removeAttr('readonly')
                        }
                    });
                    $(this).find('button.btn-ok').off('click').on('click', function () {
                        var form = $(self).find('form'),
                            url = form.attr('action'),
                            data = {
                                id: $('[name="event"]').val(),
                                status: $('[name="status"]').val(),
                                content: $('[name="content"]').val(),
                                _token: $('[name="_token"]').val()
                            };

                        $.post(url, data)
                        .done(function (data) {
                            $(self).modal('hide');
                            location.reload()
                        }).error(function (error) {
                            var content = $('[name="content"]');
                            if(error.responseJSON.errors.content.length){
                                content.parent().addClass('has-error');
                                content.siblings('span.help-block').text(error.responseJSON.errors.content[0])
                            }
                            content.on('focus', function () {
                                $(this).parent().removeClass('has-error');
                                content.siblings('span.help-block').text('')
                            })
                        })
                    });

                })
                .on('hide.bs.modal', function () {

                });

        function get_information(modal){
            var url = $('[name="get_content"]').val(),
                data = {
                    event_id : $('[name="event"]').val(),
                    _token: $('[name="_token"]').val()
                };

            $.post(url, data)
            .done(function (data) {
                $('[name="status"]').val(0);
                $('[name="content"]').val(data.content);
                $(modal).find('textarea').removeAttr('readonly');
            }).error(function (error) {
                var content = $('[name="content"]');
                if(error.responseJSON.errors.content.length){
                    content.parent().addClass('has-error');
                    content.siblings('span.help-block').text(error.responseJSON.errors.content[0])
                }
                content.on('focus', function () {
                    $(this).parent().removeClass('has-error');
                    content.siblings('span.help-block').text('')
                })
            })
        }
    }, false);
</script>