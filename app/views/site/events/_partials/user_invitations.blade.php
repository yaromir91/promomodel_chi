@if(count($userInvitations))
        @foreach ($userInvitations as $invite)
            <?php $event = $invite->event; ?>
                    @if (is_object($event))
                        <div class="row margin-t-20 one-event-{{$event->id}}">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="event-photo">
                                    <img alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.events_small') . $event->image) }}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 ">
                                <h2>{{ $event->title }}</h2>
                                <p>{{ $event->description }}</p>
                                <p>{{ $event->date_start }}</p>
                                <div class="option-event"><a role="button" href="{{ URL::to('events/show/' . $event->id) }}" class="btn btn-submit">{{ Lang::get('button.view_details') }}</a></div>

                                @if ($event->checkAvailableToAccept())
                                    <div class="option-event">
                                        {{--Принять профессию--}}
                                        <a
                                            role="button"
                                            data-event-target=".one-event-{{$event->id}}"
                                            data-target="#new-confirm-accept"
                                            data-href="{{ URL::route('event_accept_set', [$event->id, 1, 0]) }}"
                                            class="btn-send-invitation-answer btn btn-submit model accept-invitation send-just"
                                        >
                                            {{sprintf(\Lang::get('button.accept_prof'), "\"<strong>".$invite->getProfessionName()."</strong>\"")}}
                                        </a>
                                    </div>
                                    @if($invite->is_manager == \Acme\Models\Events\EventInvite::MANAGER_ON && $invite->manager_status == \Acme\Models\Events\EventInvite::INVITE_MANAGER_STATUS_NEW)
                                        <div class="option-event manager_buttons">
                                            {{--Согласится быть менеджером--}}
                                            <a
                                                role="button"
                                                data-event-target=".one-event-{{$event->id}}"
                                                data-target="#new-confirm-accept"
                                                data-href="{{ URL::route('event_accept_set', [$event->id, 1, 1]) }}"
                                                class="btn-send-invitation-answer btn btn-submit model accept-invitation"
                                            >
                                                {{ Lang::get('button.accept_manager') }}
                                            </a>
                                        </div>
                                        <div class="option-event manager_buttons">
                                            {{--Отказаться быть менеджером--}}
                                            <a
                                                role="button"
                                                data-event-target=".one-event-{{$event->id}}"
                                                data-target="#confirm-not-accept-manager"
                                                data-href="{{ URL::route('event_accept_set', [$event->id, 0, 1]) }}"
                                                class="btn-send-invitation-answer btn btn-submit model accept-reject doNotDelete"
                                            >
                                                {{ Lang::get('button.reject_manager') }}
                                            </a>
                                        </div>
                                    @endif

                                    <div class="option-event">
                                        {{--Отказаться от приглашения--}}
                                        <a
                                            role="button"
                                            data-event-target=".one-event-{{$event->id}}"
                                            data-target="#new-confirm-not-accept"
                                            data-href="{{ URL::route('event_accept_set', [$event->id, 0, 0]) }}"
                                            class="btn-send-invitation-answer btn btn-submit model accept-reject"
                                        >
                                            {{ Lang::get('button.reject_all') }}
                                        </a>
                                    </div>


                                @endif
                            </div>
                        </div>
                @endif
            @endforeach
@else
    <p class="info-message">{{ Lang::get('events.invitation_not_found') }}</p>
@endif

<div id="_token" token="{{ csrf_token() }}"></div>

<div id="new-confirm-accept" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ Lang::get('events.you_want_to_accept_invitation_if_you_accept_manager_you_accept_profession') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                <a role="button" data-target-answer="#new-invitation-answer" class="btn btn-primary send-accept">{{ Lang::get('button.yes') }}</a>
            </div>
        </div>
    </div>
</div>

<div id="new-confirm-not-accept" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ Lang::get('events.you_want_to_reject_invitation') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                <a role="button" data-target-answer="#new-invitation-answer" class="btn btn-primary send-accept">{{ Lang::get('button.yes') }}</a>
            </div>
        </div>
    </div>
</div>

<div id="confirm-not-accept-manager" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ Lang::get('events.Confirmation') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ Lang::get('events.you_want_to_reject_manager_invitation') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
                <a role="button" data-target-answer="#new-invitation-answer" class="btn btn-primary send-accept">{{ Lang::get('button.yes') }}</a>
            </div>
        </div>
    </div>
</div>


<div id="new-invitation-answer" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.close') }}</button>
            </div>
        </div>
    </div>
</div>