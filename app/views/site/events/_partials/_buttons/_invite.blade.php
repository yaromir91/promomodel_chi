@if(time() < strtotime($event->date_start))
    <div class="col-lg-4 col-md-4 col-sm-6">
        <a href="{{ URL::to('events/' . $event->id . '/search') }}" class="btn btn-submit">
            {{ Lang::get('button.invite') }}
        </a>
    </div>
@endif