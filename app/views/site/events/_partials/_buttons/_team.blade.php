<div class="col-lg-4 col-md-4 col-sm-6">
    <a href="{{ URL::to('events/team/' . $event->id) }}" class="btn btn-submit">
        {{ Lang::get('button.team') }}
    </a>
</div>