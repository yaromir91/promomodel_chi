<div class="col-lg-4 col-md-4 col-sm-6">
    <a href="{{ URL::to('report/event/' . $event->id) }}" class="btn btn-submit">
        {{ Lang::get('button.report') }}
    </a>
</div>