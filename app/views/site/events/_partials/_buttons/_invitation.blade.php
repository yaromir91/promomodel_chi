<div class="col-lg-4 col-md-4 col-sm-6">
    <a href="{{ URL::to('events/invitation/' . $event->id) }}" class="btn btn-submit">
        {{ Lang::get('button.invitation') }}
    </a>
</div>