<div class="col-lg-4 col-md-4 col-sm-6">
    <form method="POST" action="{{ \Config::get('app.payments.action'); }}">
        <input type="hidden" name="scid" value="{{ \Config::get('app.payments.scId'); }}" required="">
        <input type="hidden" name="shopId" value="{{ \Config::get('app.payments.shopId'); }}" required="">
        <input type="hidden" name="sum" value="{{ $event->getTotalPriceWithServicePercent() }}" required="">
        <input type="hidden" name="shopSuccessURL" value="{{ URL::to('events/show/' . $event->id . '/?status=' . \Acme\Models\Events\Events::STATUS_YA_SUCCESS) }}" required="">
        <input type="hidden" name="shopFailURL" value="{{ URL::to('events/show/' . $event->id . '/?status=' . \Acme\Models\Events\Events::STATUS_YA_FAILED) }}" required="">
        <input type="hidden" name="customerNumber" value="{{ $event->id; }}" required="">
        <input type="hidden" name="eventId" value="{{ $event->id; }}" required="">

        <input type="hidden" name="paymentType" value="AC">
        <button class="btn btn-submit" type="submit">{{ Lang::get('button.paid') }}</button>
    </form>
</div>