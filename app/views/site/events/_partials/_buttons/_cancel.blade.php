<div class="col-lg-4 col-md-4 col-sm-6">
    <a href="#" data-href="{{ URL::to('events/cancel/' . $event->id) }}" class="btn btn-submit" data-toggle="modal" data-target="#confirm-cancel">
        {{ Lang::get('button.cancel') }}
    </a>
</div>