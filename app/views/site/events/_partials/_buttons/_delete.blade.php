<div class="col-lg-4 col-md-4 col-sm-6">
    <a href="#" data-href="{{ URL::to('events/delete/' . $event->id) }}" class="btn btn-submit" data-toggle="modal" data-target="#confirm-delete">
        {{ Lang::get('button.delete') }}
    </a>
</div>