@if(count($invitation))
    @foreach($invitation as $inv)
        <?php $user = $inv->user; ?>
        @if($user instanceof Acme\Models\User)
            <?php $profile = $user->profile; ?>
            <div class="row margin-t-20">
                @if($profile instanceof Acme\Models\Profile\UserProfile)
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <a href="{{URL::route('view_user_profile', $user->id)}}">
                            @if($profile->avatar)
                                <img src="{{ asset(\Config::get('app.image_dir.profile') . $profile->avatar) }}">
                            @else
                                {{\Config::get('app.album_no_image_no_class')}}
                            @endif
                        </a>
                    </div>
                @endif
                <div class="col-lg-9 col-md-9 col-sm-8">
                    <a href="{{URL::route('view_user_profile', $user->id)}}">
                        <h2>{{ $user->getUserName() }}</h2>
                    </a>
                    <p><span>{{\Lang::get('events.status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentStatusTitle($inv->status) }}</p>
                    @if($inv->is_manager == 1)
                        <p><span>{{\Lang::get('events.manager_status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentManagerStatusTitle($inv->manager_status) }}</p>
                    @endif
                    <p><span>{{\Lang::get('events.team_status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentTeamStatusTitle($inv->in_team) }}</p>

                    <p><span>{{\Lang::get('events.title_events_profession')}}</span>: {{ $inv->getProfessionName() }}</p>
                </div>
            </div>
        @endif
    @endforeach
@endif