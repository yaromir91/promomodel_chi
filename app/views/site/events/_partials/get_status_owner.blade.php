<div class="modal fade" id="get_status_owner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2> {{ Lang::get('events.title_events_instruction') }}</h2>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="content">{{ Lang::get('events.title_description') }}</label>
                    <textarea class="form-control" name="content" id="content" cols="30" rows="10" readonly="readonly"></textarea>
                    <input type="hidden" name="event" value="{{ $event_id }}">
                    {{ Form::token() }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-submit" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener("load",function(event) {
        var url = '{{ URL::route('event_show_status_owner') }}',
                data = {
                    event_id : $('[name="event"]').val(),
                    _token: $('[name="_token"]').val()
                };

        $.post(url, data)
        .done(function (data) {
            $('[name="status"]').val(0);
            $('[name="content"]').val(data.content);
        })
    },false);
</script>