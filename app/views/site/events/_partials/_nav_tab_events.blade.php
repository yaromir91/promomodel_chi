<div class="tab-navigation">
    <ul class="nav nav-tabs">
        <li @if($tab==1)class="active"@endif>
            <a href="{{ URL::to('events') }}">{{ Lang::get('events.tab_future') }}</a>
        </li>
        <li @if($tab==2)class="active"@endif>
            <a href="{{ URL::to('events/past') }}">{{ Lang::get('events.tab_past') }}</a>
        </li>
        @if(Auth::check() && !Auth::user()->hasRole('admin') && !Auth::user()->hasRole('Agent'))
            <li @if($tab==3)class="active"@endif>
                <a href="{{ URL::to('events/my') }}">{{ Lang::get('events.tab_my') }}</a>
            </li>
            @if($isShowInvitation)
            <li @if($tab==4)class="active"@endif>
                <a href="{{ URL::to('events/my-invitation') }}">{{ Lang::get('events.tab_invites') }}</a>
            </li>
            @endif
        @endif
    </ul>
</div>