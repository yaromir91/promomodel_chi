@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('events.event_invitation_list') }} ::
    @parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{$event->title}} - {{ Lang::get('events.event_invitation_list') }}</h1>
    </div>
    <div class="pull-right">
        <a class="btn btn-submit" href="{{URL::route('show_events', $event->id)}}">{{Lang::get('events.view_event')}}</a>
    </div>


    @include('site.events._partials.event_invitation_list')

@stop