@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{$event->title}} - {{ $title }}</h1>
    </div>

    <a class="btn btn-submit" href="{{URL::route('show_events', $event->id)}}">{{Lang::get('events.view_event')}}</a>
    <hr class="featurette-divider">

    @include('site.events._partials._nav_tab_event_team')

    <div class="tab-content team-content">
        <div class="tab-pane active" id="tab-main-team">
            @include('site.events._partials.event_team_list')
        </div>
        <div class="tab-pane" id="tab-reserved-team">
            @include('site.events._partials.event_reserved_team_list')
        </div>
        <div id="_token" token="{{ csrf_token() }}"></div>
        <div class="row add-comment">
            <div class="col-lg-12">
                <h3>{{ Lang::get('report.add_comment') }}</h3>
                {{ Form::open(array('url' => 'events/comment/create/' . $event->id, 'method' => 'post', 'files' => true, 'autocomplete' => 'off')) }}
                <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                    <div class="col-lg-10 col-md-10 col-sm-9">
                        {{ Form::textarea('comment', '', array('class' => 'form-control', 'id' => 'comment', 'rows' => '5')) }}
                        {{ $errors->first('comment') }}
                    </div>
                    <!-- Form Actions -->
                    <div class="col-lg-2 col-md-2 col-sm-3">
                        <button type="submit" class="btn btn-submit">{{Lang::get('button.add')}}</button>
                    </div>
                    <!-- ./ form actions -->
                </div>
                <!-- Image -->
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        {{ Form::file('image') }}
                        {{ $errors->first('image', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ image -->
                {{ Form::close() }}
            </div>
        </div>
        @if ($comments->count())
            @foreach ($comments as $comment)
                <div class="row" id="comment_{{ $comment->id }}">
                    <div class="col-lg-12 comment-text">
                        <h4>{{ $comment->author->username; }}</h4>
                        <p id="comment_content_block_{{ $comment->id }}">{{ $comment->content(); }}</p>
                        @if($comment->image)
                            <p><img class="img-thumbnail comment-img" src="{{ asset(\Config::get('app.image_dir.comment') . $comment->id . '/'  . $comment->image) }}" alt=""></p>
                        @endif
                        <span>{{ $comment->date(); }}</span>
                        @if($comment->user_id == $user->id || $is_admin)
                            <div class="pull-right">
                                <a href="#" data-toggle="modal" data-target="#comment-edit-form_{{ $comment->id }}">
                                    <span>{{ Lang::get('report.comment_edit') }} </span>
                                </a>|
                                <a href="#" data-href="{{ URL::to('events/comment/delete/' . $comment->id) }}" data-id="{{ $comment->id }}" data-toggle="modal" data-target="#confirm-delete-comment">
                                    <span>{{ Lang::get('report.comment_delete') }}</span>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                {{-- COMMENT EDIT FORM MODAL--}}
                <div class="modal fade" id="comment-edit-form_{{ $comment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                {{ Form::textarea('comment', $comment->content(), array('class' => 'form-control', 'id' => 'comment_content_' . $comment->id)) }}
                                {{ $errors->first('comment') }}
                                <div id="_token" token="{{ csrf_token() }}"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                                <a class="btn btn-default btn_update_comment" data-href="{{ URL::to('events/comment/edit/' . $comment->id) }}" data-id="{{ $comment->id }}">{{ Lang::get('button.update') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- COMMENT EDIT FORM MODAL--}}
            @endforeach
        @endif
    </div>

    {{-- COMMENT DELETE ALERT MODAL--}}
    <div class="modal fade" id="confirm-delete-comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a class="btn btn-default" id="btn_delete_comment">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>
    {{-- COMMENT DELETE ALERT MODAL--}}
@stop