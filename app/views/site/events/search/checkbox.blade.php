@if ($param instanceof Acme\Models\Profile\ProfileParams)
    @foreach($value as $key=>$val)
        <label class="custom-checkbox-inline">
            {{ Form::checkbox($name.'['.$param->id.'][data][]', $key, array()) }}
            {{ Form::hidden($name.'['.$param->id.'][type]', $param->search_type) }}
            {{ $val }}
        </label>
    @endforeach
@endif