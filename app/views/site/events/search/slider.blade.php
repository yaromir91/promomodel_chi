@if ($param instanceof Acme\Models\Profile\ProfileParams)
    <div class="slider desktop-search" id="{{ $param->id }}" data-min="{{ $value[0] }}" data-max="{{ $value[1] }}" data-step="{{ $value[2] }}" data-pip_step="{{ $value[3] }}" ></div>
    <div class="inputs-mobile mobile-search row">
        <div class="col md-6 col-sm-6 col-xs-6">
            <input type="number" class="number-min form-control" min="{{ $value[0] }}" max="{{ $value[1] }}" value="{{ $value[0] }}" placeholder="{{ $value[0] }}">
        </div>
        <div class="col md-6 col-sm-6 col-xs-6">
            <input type="number" class="number-max form-control" min="{{ $value[0] }}" max="{{ $value[1] }}"  value="{{ $value[1] }}" placeholder="{{ $value[1] }}">
        </div>
    </div>
    <script>
        window.onload = function() {
            function changeInputsForMobile() {
                $('.desktop-search').hide();
                $('.mobile-search').show();
            }
            function changeInputsForDesktop() {
                $('.desktop-search').show();
                $('.mobile-search').hide();
            }
            if(window.innerWidth < 768) {
                changeInputsForMobile();
            }
            else {
                changeInputsForDesktop();
            }
            $(window).on('resize', function() {
                if(window.innerWidth < 768) {
                    changeInputsForMobile()
                }
                else {
                    changeInputsForDesktop();
                }
            });
            var inputMinMobile = $('.mobile-search').find('.number-min'),
                inputMaxMobile = $('.mobile-search').find('.number-max');
            inputMinMobile.on('change', function(e) {
                e.preventDefault();
                var inputCurrentValue = $(this).val();
                var hiddenInput = $(this).closest('.search-params-block').find(".hidden-min");
                hiddenInput.attr('value', inputCurrentValue);
            });
            inputMaxMobile.on('change', function(e) {
                e.preventDefault();
                var inputCurrentValue = $(this).val();
                var hiddenInput = $(this).closest('.search-params-block').find(".hidden-max");
                hiddenInput.attr('value', inputCurrentValue);
            });
        }
    </script>
    {{ Form::hidden($name.'['.$param->id.'][data][]', '', array('id' => 'hidden0_' . $param->id, 'class' => 'hidden-min')) }}
    {{ Form::hidden($name.'['.$param->id.'][data][]', '', array('id' => 'hidden1_' . $param->id, 'class' => 'hidden-max')) }}
    {{ Form::hidden($name.'['.$param->id.'][type]', $param->search_type) }}
@endif