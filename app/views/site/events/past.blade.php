@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('user/user.events') }} ::
    @parent
@stop

@section('meta_description'){{ Lang::get('events.title_events_list') }}@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('events.title_events_list') }}</h1>
    </div>
    @if(Auth::user()->getMainUserRoleCode() == \Acme\Models\Role::ORGANIZER_USER)
    <div class="pull-right">
        <a href="{{ URL::to('events/create') }}" class="btn btn-submit iframe">{{ Lang::get('button.create') }}</a>
    </div>
    @endif
    @include('site.events._partials._nav_tab_events')

    <div class="tab-content">
        <div class="tab-pane active" id="tab-past">
            @include('site.events._partials.past_events')
        </div>
    </div>

@stop

    
