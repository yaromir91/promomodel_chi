@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>
            {{ $title }}
        </h1>
    </div>
    {{-- Create User Form --}}

    @if(Route::currentRouteNamed('invite_to_group'))
        {{ Form::open(array('route' => ['invite_to_group_post', isset($postParams) ? $postParams : null], 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'candidatSearchForm')) }}
        <div class="form-group">
            {{ Form::label('username', Lang::get('profile.Username'), ['class' => 'col-md-2 control-label']) }}
            <div class="col-md-5">
                {{ Form::input('text', 'search_params[username][data][]', null, ['class' => 'form-control']) }}
                {{ Form::input('hidden', 'search_params[username][type]', 3, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('first_name', Lang::get('profile.FirstName'), ['class' => 'col-md-2 control-label']) }}
            <div class="col-md-5">
                {{ Form::input('text', 'search_params[first_name][data][]', null, ['class' => 'form-control']) }}
                {{ Form::input('hidden', 'search_params[first_name][type]', 3, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('last_name', Lang::get('profile.LastName'), ['class' => 'col-md-2 control-label']) }}
            <div class="col-md-5">
                {{ Form::input('text', 'search_params[last_name][data][]', null, ['class' => 'form-control']) }}
                {{ Form::input('hidden', 'search_params[last_name][type]', 3, ['class' => 'form-control']) }}
            </div>
        </div>
    @else
        {{ Form::open(array('url' => 'events/' . $event->id .'/search', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'candidatSearchForm')) }}
    @endif


    {{ Form::input('hidden', 'search_params[public][data][]', 1, ['class' => 'form-control']) }}
    {{ Form::input('hidden', 'search_params[public][type]', 4, ['class' => 'form-control']) }}

    <p class="bg-danger">{{ $errors->first('search_params') }}</p>
    @foreach ($data['params'] as $mParam)
        <div class="form-group {{ $errors->has('search_params') ? 'has-error' : '' }}">
            {{ Form::label('search_params', $mParam->getTranslatedParamTitle(App::getLocale()), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                @include($mParam->getSearchTemplateByType(), ['param' => $mParam, 'name' => 'search_params', 'value' => $data['ranges'][$mParam->id]])
                {{ $errors->first('profile_params_'.$mParam->id) }}
            </div>
        </div>
    @endforeach

    <!-- Form Actions -->
    @if(Route::currentRouteNamed('invite_to_group'))
        <div class="form-group">
            <div class=" col-md-3">
                <button type="button" class="btn btn-submit" onclick="document.location.href='{{ URL::previous() }}'">{{ Lang::get('button.cancel') }}</button>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-submit">{{ Lang::get('button.search') }}</button>
            </div>
        </div>
    @else
    <div class="form-group">
        <div class="col-md-3">
            <button type="button" class="btn btn-submit" onclick="document.location.href='{{ URL::to('events/show/' . $event->id) }}'">{{ Lang::get('button.cancel') }}</button>
        </div>
        <div class="col-md-3">
            <button type="submit" class="btn btn-submit">{{ Lang::get('button.search') }}</button>
        </div>
    </div>
    @endif
    <!-- ./ form actions -->
    {{ Form::close() }}
@stop