@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>
            {{ $title }}
        </h1>
    </div>
	{{-- Create User Form --}}
    {{ Form::open(array('url' => 'events/create', 'method' => 'post', 'autocomplete' => 'off', 'class' => 'form-horizontal avatar-form', 'id' => 'EventForm', 'files'=> true)) }}

        {{ Form::hidden('search', '0', array('id' => 'searchField')) }}
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- username -->
                <!-- Title -->
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('title', Lang::get('events.title_title'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::text('title', '', array('class' => 'form-control', 'id' => 'title')) }}
                        {{ $errors->first('title', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ title -->
                <!-- Description -->
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('description', Lang::get('events.title_description'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::textarea('description', '', array('class' => 'form-control', 'maxlength' => '570', 'id' => 'description', 'rows' => '5')) }}
                        {{ $errors->first('description', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ description -->
                <!-- Image -->
                <div class="form-group {{ $errors->has('new_picture') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('image', Lang::get('events.title_photo'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">

                        <?php
                            
                            $tmpSmallPicFile = (Input::old('new_picture', '')) ? asset(\Config::get('app.image_dir.events_small') . Input::old('new_picture', '')) : '';
                            $tmpSmallPicName = Input::old('new_picture', '');

                        ?>

                        @include('site.profile.applicant.user_avatar', [
                            'fileFieldName'   => 'image',
                            'actionUrlUpload' => '/events/upload',
                            'origImage'       => $tmpSmallPicFile,
                            'origImageName'   => $tmpSmallPicName
                        ])
                        {{ $errors->first('new_picture', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ image -->
                <!-- Location -->
                <div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('location', Lang::get('events.title_event_place'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::text('location', '', array('class' => 'form-control', 'id' => 'location')) }}
                        {{ $errors->first('location', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ location -->
                <!-- Permission view -->
                <div class="form-group {{ $errors->has('permission_view') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('permission_view', Lang::get('events.title_permission_view'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        <div class="radio">
                            <label>
                                {{ Form::radio('permission_view', '1', true) }}
                                {{ Lang::get('events.description_permission_invite_yes') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                {{ Form::radio('permission_view', '0', true) }}
                                {{ Lang::get('events.description_permission_invite_no') }}
                            </label>
                        </div>
                        {{ $errors->first('permission_view', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ permission view -->
                <!-- Permission invite -->

                <!-- ./ permission invite -->
                <!-- Status -->

                <!-- ./ status -->
                <!-- Casting -->
                <div class="form-group {{ $errors->has('casting') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('casting', Lang::get('events.title_casting'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        <div class="radio">
                            <label>
                                {{ Form::radio('casting', '1', true) }}
                                {{ Lang::get('events.description_permission_invite_yes') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                {{ Form::radio('casting', '0', true) }}
                                {{ Lang::get('events.description_permission_invite_no') }}
                            </label>
                        </div>
                        {{ $errors->first('casting', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ casting -->
                <!-- Type -->
                <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('type', Lang::get('events.title_event_type'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::select('type', $event_types, '', array('class' => 'form-control')) }}
                        {{ $errors->first('type', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ type -->
                <!-- Profession -->
                <div class="form-heading">
                    <h3>{{ Lang::get('events.title_profession') }}</h3>
                </div>
                <div class="form-group {{ $errors->has('profession') ? 'has-error' : '' }}" {{ $errors->has('profession') ? 'style="border: solid 1px #a94442; padding-bottom: 10px;"' : '' }}>
                    @foreach ($professions as $profession)
                        <?php
                            $prh = '';
                            $prd = '';
                            $cnt = '';
                            $prh = trim(Input::old('prof_rate_h.'.$profession->id, $prh));
                            $prd = trim(Input::old('prof_rate_d.'.$profession->id, $prd));
                            $cnt = trim(Input::old('count.'.$profession->id, $cnt));

                            $checked = Input::old('profession.'.$profession->id, false);
                        ?>
                        <div class="row profession-row" id="prof_{{ $profession->id }}">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <label class="control-label" for="profession[{{ $profession->id }}]">
                                    @if(App::getLocale() == 'en')
                                        {{ $profession->title_en }}
                                    @elseif(App::getLocale() == 'ru')
                                        {{ $profession->title }}
                                    @endif
                                </label>
                                <input @if ($checked) checked="checked" @endif name="profession[{{ $profession->id }}]" class="form-control prof_checked" type="checkbox" id="" value="{{ $profession->id }}" />
                            </div>
                            <div @if (!$checked) style="display: none" @endif>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <label class="control-label" for="count[{{ $profession->id }}]">{{ Lang::get('events.count') }}</label>
                                    <input class="form-control numbersOnly" maxlength="6" type="text" name="count[{{ $profession->id }}]" id="count" value="{{ $cnt }}" />
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <label class="control-label" for="prof_rate_h[{{ $profession->id }}]">{{ Lang::get('events.hour_rate') }}</label>
                                    <input class="form-control numbersOnly" maxlength="6" type="text" name="prof_rate_h[{{ $profession->id }}]" id="h_count" value="{{ $prh }}" />
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <label class="control-label" for="prof_rate_d[{{ $profession->id }}]">{{ Lang::get('events.day_rate') }}</label>
                                    <input class="form-control numbersOnly" maxlength="6" type="text" name="prof_rate_d[{{ $profession->id }}]" id="d_count" value="{{ $prd }}" />
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @if($errors->has('profession'))
                    <div class="form-group has-error">
                        <div class="col-lg-3 col-md-3 col-sm-6"></div>
                        <div class="col-lg-8 col-md-8 col-sm-6">
                            {{ $errors->first('profession', '<span class="help-block">:message</span>') }}
                        </div>
                    </div>
                @endif
                <!-- ./ profession -->

                <!-- Manager Number -->
                <div class="form-group {{ $errors->has('manager_num') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('manager_num', Lang::get('events.manager_num'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::text('manager_num', '', array('class' => 'form-control numbersOnly', 'id' => 'manager_num')) }}
                        {{ $errors->first('manager_num', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ Manager Number -->
                <!-- Date BEGIN -->
                <div class="form-group">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('date_start', Lang::get('events.title_date'), array('class' => 'control-label')) }}
                    </div>
                    <div class='col-lg-3 col-md-3 col-sm-4 {{ $errors->has('date_start') ? 'has-error' : '' }}'>
                        {{ Form::text('date_start', Input::old('date_start', ''), array('class' => 'datepicker form-control', 'id' => 'datetimepickerStart')) }}
                        {{ $errors->first('date_start', '<span class="help-block">:message</span>') }}
                    </div>
                    <div class='col-lg-3 col-md-3 col-sm-4 {{ $errors->has('date_end') ? 'has-error' : '' }}'>
                        {{ Form::text('date_end', Input::old('date_end', ''), array('class' => 'datepicker form-control', 'id' => 'datetimepickerEnd')) }}
                        {{ $errors->first('date_end', '<span class="help-block">:message</span>') }}
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <span class="form-info">{{ Lang::get('events.msg_07') }}</span>
                    </div>
                </div>
                <!-- Date END -->
                <!-- Report Date BEGIN -->
                <div class="form-group {{ $errors->has('report_date') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('report_date', Lang::get('events.report_date'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-4">
                        {{ Form::text('report_date', Input::old('report_date', ''), array('class' => 'form-control datepicker-onlydate datetimepicker-additional', 'id' => 'report_date')) }}
                        {{ $errors->first('report_date', '<span class="help-block">:message</span>') }}
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <span class="form-info">{{ Lang::get('events.message_02') }}</span>
                    </div>
                </div>
                <!-- Report Date END -->

                <!-- Repeat -->
                <div class="form-group {{ $errors->has('repeat') ? 'has-error' : '' }}">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        {{ Form::label('repeat', Lang::get('events.title_repeat'), array('class' => 'control-label')) }}
                        <div class="checkbox">
                            <input name="repeat" type="checkbox" id="RepeatCheckbox" @if(Input::old('repeat')) checked @endif value="1">
                            <input type="hidden" name="repeat_after_date_title" value="{{ Lang::get('events.js_repeat_after_end_date') }}"/>
                        </div>
                        <a id="EditRepeatLink" @if(Input::old('repeat')) style="display: inline;" @endif href="#">{{ Lang::get('events.repeat_edit') }}</a>
                        <div class="repeat-data-error">
                            {{ Lang::get('events.error_repeat_empty_fields') }}
                        </div>
                        <div class="repeat-time-error">
                            {{ Lang::get('events.error_repeat_wrong_data') }}
                        </div>
                        <input type="text" value="{{ Input::old('show_repeat') }}" @if(Input::old('repeat')) style="display: inline;" @endif disabled id="EditRepeatValueInput" class="form-control">
                        <input id="ShowRepeat" type="hidden" name="show_repeat" value="{{ Input::old('show_repeat') }}" />
                    </div>
                </div>
                <!-- ./ repeat -->
                <!-- Form Actions -->
                <div class="form-group">
                    <div class="col-lg-2 col-md-2 col-sm-3">
                        <button type="button" class="btn btn-submit" onclick="document.location.href='{{ URL::to('events') }}'">{{ Lang::get('button.cancel') }}</button>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-3">
                        <button type="button" class="btn btn-submit" onclick="document.getElementById('searchField').value = 0; document.getElementById('EventForm').submit();">{{ Lang::get('button.create') }}</button>
                    </div>
                    {{--<div class="col-lg-2 col-md-2 col-sm-3">--}}
                        {{--<button type="button" class="btn btn-submit" onclick="document.getElementById('searchField').value = 1; document.getElementById('EventForm').submit();">{{ Lang::get('button.next') }}</button>--}}
                    {{--</div>--}}
                </div>
                <!-- ./ form actions -->
				<!-- ./ groups -->
			</div>
			<!-- ./ general tab -->



    <div class="modal repeat_popup" id="RepeatPopup" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="modal-header">
                    {{--<button type="button" class="close CloseRepeatPopup">&times;</button>--}}
                    {{--<h4 class="modal-title">Modal Header</h4>--}}
                </div>-->
                <div class="modal-body">
                    <div class="row repeat_row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            {{ Lang::get('events.repeat.repeat_event') }}
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-8">
                            <select name="reserve[repeat_type]" id="label-repeat" class="js-type-changer">
                                <option @if(Input::old('reserve')['repeat_type'] == 'daily' || Input::old('reserve')['repeat_type'] == null) selected="" @endif value="daily">{{ Lang::get('events.repeat.repeat_everyday') }}</option>
                                <option @if(Input::old('reserve')['repeat_type'] == 'weekly') selected="" @endif id="WeeklyOption" value="weekly">{{ Lang::get('events.repeat.repeat_everyweek') }}</option>
                                <option @if(Input::old('reserve')['repeat_type'] == 'monthly') selected="" @endif value="monthly">{{ Lang::get('events.repeat.repeat_everymonth') }}</option>
                                <option @if(Input::old('reserve')['repeat_type'] == 'yearly') selected="" @endif value="yearly">{{ Lang::get('events.repeat.repeat_everyyear') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row repeat-data" id="daily">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            {{ Lang::get('events.repeat.every') }}:
                            <select name="reserve[repeat_daily]" class="daily_select">
                                <option @if(Input::old('reserve')['repeat_daily'] == 1 || Input::old('reserve')['repeat_type'] == null) selected="" @endif value="1">1</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 2) selected="" @endif value="2">2</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 3) selected="" @endif value="3">3</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 4) selected="" @endif value="4">4</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 5) selected="" @endif value="5">5</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 6) selected="" @endif value="6">6</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 7) selected="" @endif value="7">7</option>
                            </select>
                            {{ Lang::get('events.repeat.day') }}
                        </div>
                        <div class="col-lg-12 last_repeat">
                            {{ Lang::get('events.repeat.last_repeat') }}: {{ Form::text('reserve[datetimepickerDaily]', Input::old('reserve')['datetimepickerDaily'], array('class' => 'form-control datepicker', 'id' => 'datetimepickerDaily')) }}
                        </div>
                    </div>
                    <div class="row repeat-data" id="weekly">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <?php
                            if(isset(Input::old('reserve')['weekly_repeat'])) {
                                $weekly_repeat = Input::old('reserve')['weekly_repeat'];
                            } else {
                                $weekly_repeat = false;
                            }
                            ?>
                            {{ Lang::get('events.repeat.every') }}:
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(1, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat1" type="checkbox" name="reserve[weekly_repeat][]" value="1">
                                {{ Lang::get('events.js_repeat_week_day_1') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(2, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat2" type="checkbox" name="reserve[weekly_repeat][]" value="2">
                                {{ Lang::get('events.js_repeat_week_day_2') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(3, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat3" type="checkbox" name="reserve[weekly_repeat][]" value="3">
                                {{ Lang::get('events.js_repeat_week_day_3') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(4, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat4" type="checkbox" name="reserve[weekly_repeat][]" value="4">
                                {{ Lang::get('events.js_repeat_week_day_4') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(5, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat5" type="checkbox" name="reserve[weekly_repeat][]" value="5">
                                {{ Lang::get('events.js_repeat_week_day_5') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(6, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat6" type="checkbox" name="reserve[weekly_repeat][]" value="6">
                                {{ Lang::get('events.js_repeat_week_day_6') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(7, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat7" type="checkbox" name="reserve[weekly_repeat][]" value="7">
                                {{ Lang::get('events.js_repeat_week_day_7') }}</label>

                        </div>
                        <div id="RepeatWeeklyError">
                            {{ Lang::get('events.repeat.error_need_at_last_one_week_day') }}
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 last_repeat">
                            {{ Lang::get('events.repeat.last_repeat') }}: {{ Form::text('reserve[datetimepickerWeekly]', Input::old('reserve')['datetimepickerWeekly'], array('class' => 'lastrepeat datepicker form-control', 'id' => 'datetimepickerWeekly')) }}
                        </div>
                    </div>
                    <div class="row repeat-data" id="monthly">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            {{ Lang::get('events.repeat.every') }}:
                            <select name="reserve[repeat_monthly]" class="monthly_select">
                                <option @if(Input::old('reserve')['repeat_monthly'] == 1) selected="" @endif selected="" value="1">1</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 2) selected="" @endif value="2">2</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 3) selected="" @endif value="3">3</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 4) selected="" @endif value="4">4</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 5) selected="" @endif value="5">5</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 6) selected="" @endif value="6">6</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 7) selected="" @endif value="7">7</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 8) selected="" @endif value="8">8</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 9) selected="" @endif value="9">9</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 10) selected="" @endif value="10">10</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 11) selected="" @endif value="11">11</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 12) selected="" @endif value="12">12</option>
                            </select>
                            {{ Lang::get('events.repeat.month') }}
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 last_repeat">
                            {{ Lang::get('events.repeat.last_repeat') }}: {{ Form::text('reserve[datetimepickerMonthly]', Input::old('reserve')['datetimepickerMonthly'], array('class' => 'lastrepeat datepicker form-control', 'id' => 'datetimepickerMonthly')) }}
                        </div>
                    </div>
                    <div class="row repeat-data" id="yearly">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            {{ Lang::get('events.repeat.every') }}:
                            <select name="reserve[repeat_yearly]" class="yearly_select">
                                <option @if(Input::old('reserve')['repeat_yearly'] == 1) selected="" @endif selected="" value="1">1</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 2) selected="" @endif value="2">2</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 3) selected="" @endif value="3">3</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 4) selected="" @endif value="4">4</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 5) selected="" @endif value="5">5</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 6) selected="" @endif value="6">6</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 7) selected="" @endif value="7">7</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 8) selected="" @endif value="8">8</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 9) selected="" @endif value="9">9</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 10) selected="" @endif value="10">10</option>
                            </select>
                            {{ Lang::get('events.repeat.year') }}
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 last_repeat">
                            {{ Lang::get('events.repeat.last_repeat') }}: {{ Form::text('reserve[datetimepickerYearly]', Input::old('reserve')['datetimepickerYearly'], array('class' => 'lastrepeat datepicker form-control', 'id' => 'datetimepickerYearly')) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<div class="pull-left">--}}
                    {{--Что получилось:--}}
                    {{--<br />--}}
                    {{--Повторять мероприятие каждый 4-й день--}}
                    {{--</div>--}}
                    <button type="button" id="AddRepeat" class="btn btn-submit">{{Lang::get('button.add')}}</button>
                    <button type="button" id="CloseRepeatPopup" class="btn btn-submit">{{Lang::get('button.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

    <script>
        var repeat_dictionary = {
            daily: [
                '{{ Lang::get("events.js_repeat_every_day") }}',
                '{{ Lang::get("events.js_repeat_every_", array('day' => 2)) }}',
                '{{ Lang::get("events.js_repeat_every_", array('day' => 3)) }}',
                '{{ Lang::get("events.js_repeat_every_", array('day' => 4)) }}',
                '{{ Lang::get("events.js_repeat_every_", array('day' => 5)) }}',
                '{{ Lang::get("events.js_repeat_every_", array('day' => 6)) }}',
                '{{ Lang::get("events.js_repeat_every_", array('day' => 7)) }}'
            ],
            weekly: [
                '{{ Lang::get("events.js_repeat_week_day_1") }}',
                '{{ Lang::get("events.js_repeat_week_day_2") }}',
                '{{ Lang::get("events.js_repeat_week_day_3") }}',
                '{{ Lang::get("events.js_repeat_week_day_4") }}',
                '{{ Lang::get("events.js_repeat_week_day_5") }}',
                '{{ Lang::get("events.js_repeat_week_day_6") }}',
                '{{ Lang::get("events.js_repeat_week_day_7") }}'
            ],
            monthly: [
                '{{ Lang::get("events.js_repeat_every_month") }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 2)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 3)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 4)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 5)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 6)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 7)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 8)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 9)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 10)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 11)) }}',
                '{{ Lang::get("events.js_repeat_every_month_", array('month' => 12)) }}'
            ],
            yearly: [
                '{{ Lang::get("events.js_repeat_every_year") }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 2)) }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 3)) }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 4)) }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 5)) }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 6)) }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 7)) }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 8)) }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 9)) }}',
                '{{ Lang::get("events.js_repeat_every_year_", array('month' => 10)) }}'
            ],
            before: '{{ Lang::get("events.js_repeat_before") }}',
            week: '{{ Lang::get("events.js_repeat_every_week") }}'
        };
    </script>

@stop
