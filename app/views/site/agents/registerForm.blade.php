@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ Lang::get('user/user.register') }} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header clearfix">
	<h1>{{ Lang::get('agents.become_agent') }}</h1>
</div>
<div class="welcome-box">
	<span>{{ Lang::get('agents.welcome') }}</span>
</div>

{{ Form::open(array('class' => 'form-horizontal', 'url' => URL::to('agents/register'))) }}
    {{--<fieldset>--}}
            {{ Form::hidden('account_type', Acme\Models\Repositories\RoleRepository::getRoleByCode(\Acme\Models\Role::AGENT_USER)) }}
            <!-- Username -->
            <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3">
                    {{ Form::label('username', Lang::get('user/user.zizaco_user_name'), array('class' => 'control-label')) }}
                </div>

                <div class="col-lg-6 col-md-6">
                    {{ Form::text('username', '', array('class' => 'form-control', 'id' => 'username', 'maxlength' => 20)) }}
                    {{ $errors->first('username', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <!-- ./ username -->
            <!-- Last name -->
            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3">
                    {{ Form::label('last_name', Lang::get('profile.LastName'), array('class' => 'control-label')) }}
                </div>

                <div class="col-lg-6 col-md-6">
                    {{ Form::text('last_name', '', array('class' => 'form-control', 'id' => 'last_name', 'maxlength' => 20)) }}
                    {{ $errors->first('last_name', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <!-- ./ last name -->
            <!-- First name -->
            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3">
                    {{ Form::label('first_name', Lang::get('profile.FirstName'), array('class' => 'control-label')) }}
                </div>

                <div class="col-lg-6 col-md-6">
                    {{ Form::text('first_name', '', array('class' => 'form-control', 'id' => 'first_name', 'maxlength' => 20)) }}
                    {{ $errors->first('first_name', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <!-- ./ first name -->
            <!-- Gender -->
            <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3">
                    {{ Form::label('gender', Lang::get('profile.Gender'), array('class' => 'control-label')) }}
                </div>

                <div class="col-lg-6 col-md-6">
                    {{ Form::select('gender', array('m' => Lang::get('profile.Male'), 'f' => Lang::get('profile.Female')), Input::old('gender'), array('class' => 'form-control', 'id' => 'gender'))}}
                    {{ $errors->first('gender', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <!-- ./ gender -->
            <!-- Email -->
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3">
                    {{ Form::label('email', Lang::get('confide::confide.e_mail'), array('class' => 'control-label')) }}
                </div>

                <div class="col-lg-6 col-md-6">
                    {{ Form::text('email', '', array('class' => 'form-control', 'id' => 'email')) }}
                    {{ $errors->first('email', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <!-- ./ email -->
            <!-- Password -->
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3">
                    {{ Form::label('password', Lang::get('confide::confide.password'), array('class' => 'control-label')) }}
                </div>

                <div class="col-lg-6 col-md-6">
                    {{ Form::password('password', array('class' => 'form-control', 'id' => 'password', 'maxlength' => 20)) }}
                    {{ $errors->first('password', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <!-- ./ password -->
            <!-- Password confirmation -->
            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3">
                    {{ Form::label('password_confirmation', Lang::get('confide::confide.password_confirmation'), array('class' => 'control-label')) }}
                </div>

                <div class="col-lg-6 col-md-6">
                    {{ Form::password('password_confirmation', array('class' => 'form-control', 'id' => 'password_confirmation', 'maxlength' => 20)) }}
                    {{ $errors->first('password_confirmation', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <!-- ./ password confirmation -->
            
            <div class="form-actions form-group">
                <div class="col-lg-4">
                    <button type="submit" class="btn btn-submit">{{ Lang::get('button.become_agent') }}</button>
                </div>
            </div>
    {{--</fieldset>--}}
{{ Form::close() }}
@stop