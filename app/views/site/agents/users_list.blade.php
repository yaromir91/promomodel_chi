@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('agents.title') }} ::
    @parent
@stop


{{-- Content --}}
@section('content')
    <div class="profile user-profile">
        <div class="page-header clearfix">
            <h1>{{Lang::get('agents.title')}}</h1>
        </div>

        <div class="pull-right">
            <a class="btn btn-submit" href="{{ URL::to('profile/show') }}">{{ Lang::get('button.back') }}</a>
            <a class="btn btn-submit" href="{{ URL::to('agents/add') }}">{{ Lang::get('button.add') }}</a>
        </div>

        <div class="content user-content">
            <div class="row applicants-list">
                @foreach($users as $k=>$elem)
                    <div class="col-lg-3 one-applicant" data-id={{$elem->user_id}}>
                        <div class="album-photo">
                            @if($elem->avatar)
                                <img class="content-image" src="{{ asset(\Config::get('app.image_dir.profile') . $elem->avatar) }}">
                            @else
                                {{\Config::get('app.album_no_image_content_262_262')}}
                            @endif
                        </div>
                        <div class="photo-info">
                            <span>{{ $elem->first_name }} {{ $elem->last_name }} ({{ $elem->gender }})</span>
                            <br />
                            <span>{{ Lang::get('agents.invite_status') }}: <span class="{{ $text_color[$elem->invite_status] }}">{{ ($elem->invite_status) ? \Acme\Models\Repositories\UsersAgentInvitationsRepository::getStatusTitle($elem->invite_status) : '' }}</span></span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@stop
