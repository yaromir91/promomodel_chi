@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('profile.title') }} ::
    @parent
@stop


{{-- Content --}}
@section('content')
    <div class="profile user-profile">
        <div class="page-header clearfix">
            <h1>{{ Lang::get('agents.Invitations') }}</h1>
        </div>
        <div class="pull-right">
            <a class="btn btn-submit" href="{{ URL::to('profile/show') }}">{{ Lang::get('button.back') }}</a>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>{{ Lang::get('agents.agent_title') }}</th>
                    <th>{{ Lang::get('agents.date_title') }}</th>
                    <th>{{ Lang::get('agents.status_title') }}</th>
                    <th>{{ Lang::get('agents.actions_title') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($agentsInvitations as $inv)
                    <tr>
                        <td>{{ $inv->first_name }}</td>
                        <td>{{ date('Y-m-d H:i', strtotime($inv->created_at)) }}</td>
                        <td>{{ \Acme\Models\Repositories\UsersAgentInvitationsRepository::getStatusTitle($inv->invite_status) }}</td>
                        <td>
                            @if($inv->invite_status == \Acme\Models\UsersAgentInvitations::STATUS_SEND)
                                <a href="#" data-href="{{ URL::to('agents/user/accept/' . $inv->id) }}" data-toggle="modal" data-target="#confirm-user-acept">{{ Lang::get('agents.accept') }}</a>&nbsp;|
                                <a href="#" data-href="{{ URL::to('agents/user/reject/' . $inv->id) }}" data-toggle="modal" data-target="#confirm-user-reject">{{ Lang::get('agents.reject') }}</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="modal fade" id="confirm-user-acept" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                            <a class="btn btn-default btn-ok">{{ Lang::get('button.accept') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-user-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                            <a class="btn btn-default btn-ok">{{ Lang::get('button.reject') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

