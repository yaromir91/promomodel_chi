@extends('site.layouts.default')
{{-- Web site Title --}}
@section('title')
{{ Lang::get('general.home_page') }} ::
@parent
@stop
@section('meta_description')Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei.@stop

{{-- Content --}}
@section('content')
<div class="main-page">
    {{--<div class="page-header clearfix">
        <h1>{{ Lang::get('general.main_page') }}</h1>
    </div>--}}
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if($role && !Auth::user()->checkDataProfile())
                <!-- Notice -->
                <div class="remind-block">
                    <p>{{ Lang::get('reminders.notice_of_profile') }} <a href="{{ URL::to('profile') }}">{{ Lang::get('profile.title') }}</a>.</p>
                </div>
                <!-- ./ Notice -->
            @endif
            <div class="main-banner">
                <img src="{{ asset('assets/img/main-banner.jpg') }}" alt="">
            </div>
            @if($events->count())
            <div class="event-list list">
                <h3>{{ Lang::get('events.title_future') }}</h3>
                <div class="row">
                    @foreach ($events as $index => $event)
                    <div class="col-lg-3 col-md-3 col-sm-4 item">
                        <a href="{{ URL::route('show_events' , $event->id) }}"><img class="content-image" alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.events_small') . $event->image) }}"></a>
                        <div class="photo-info">
                            <div class="line-info block-name"><a href="{{ URL::route('show_events' , $event->id) }}">{{ $event->title }}</a></div>
                            <div class="line-info block-info"><span>{{ Lang::get('general.date_start') }}:&nbsp;</span>{{ $event->date_start }}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            @if(count($publicUsers) )
            <div class="user-list list">
                <h3>{{ Lang::get('applicants.title_public_user') }}</h3>
                <div class="row">
                    @foreach($publicUsers as $user)
                    <div class="col-lg-3 col-md-3 col-sm-4 item">
                        <a href="{{ URL::route('view_user_profile' , $user->user_id) }}">
                            @if($user->avatar)
                            <img class="content-image" alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.profile') . $user->avatar) }}">
                            @else
                            {{ Config::get('app.album_no_image') }}
                            @endif
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            @if(!empty($reports) && $reports->count())
            <div class="report-list list">
                <h3>{{ Lang::get('report.title_list_popular_report') }}</h3>
                <div class="row">
                    @foreach($reports as $report)
                    <div class="col-lg-3 col-sm-4 item">
                        <a href="{{ URL::route('show_report' , $report->slug) }}">
                            @if($report->image)
                            <img class="content-image" alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.posts_small')  . $report->image) }}">
                            @else
                            {{ Config::get('app.album_no_image') }}
                            @endif
                        </a>
                        <div class="photo-info">
                            <div class="line-info block-name"><a href="{{ URL::route('show_report' , $report->slug) }}">{{ $report->title }}</a></div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
</div>





@stop
