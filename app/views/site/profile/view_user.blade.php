@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ Lang::get('applicants.title') }} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="profile show-profile">
    <div class="page-header clearfix">
        <h1>{{$user->username}}</h1>
    </div>
    @if (Auth::check() && $chatCode)
        <div class="pull-right">
            <a class="btn btn-submit" href="{{ URL::route('users_chat', $chatCode) }}">{{ Lang::get('button.write_message') }}</a>
        </div>
    @endif
    <div class="row">
        @if($imagesCount>0)
            <div class="col-lg-12 col-md-12 col-sm-12">@include('site.profile.partials.view_profile_gallery')</div>
        @else
            @if($profile->avatar)
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <img class="content-image"  src="{{ asset(\Config::get('app.image_dir.profile') . $profile->avatar) }}">
                </div>
            @else
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <img class="content-image"  src="{{ asset(\Config::get('app.image_dir.stub')) }}">
                </div>
            @endif
        @endif
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">@include('site.applicants.partials.main_info')</div>
        @if($user->agent_id == 0 && $show_agent_invite)
            <div class="col-lg-3 col-md-3">
                <a class="btn btn-submit" href="#" data-href="{{ URL::route('invite_user_to_agent', $user->id) }}" data-toggle="modal" data-target="#confirm-agent-invite">{{ Lang::get('agents.invite_user') }}</a>
            </div>
            <div class="modal fade" id="confirm-agent-invite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                            <a class="btn btn-default btn-ok">{{ Lang::get('button.invite') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if($relogin)
            <div class="col-lg-3 col-md-3">
                <a class="btn btn-default" href="#" data-href="{{ URL::route('relogin', $user->id) }}" data-toggle="modal" data-target="#confirm-relogin">{{ Lang::get('agents.relogin') }}</a>
            </div>
            <div class="modal fade" id="confirm-relogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                            <a class="btn btn-default btn-ok">{{ Lang::get('button.yes') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@stop