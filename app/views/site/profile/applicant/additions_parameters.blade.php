{{ Form::open(array('class' => 'form-horizontal', 'autocomplete' => 'off', 'url' => URL::to('profile/' . $data['user']->id . '/edit-applicant'))) }}
<!-- CSRF Token -->
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<!-- ./ csrf token -->

<!-- Event Types -->
<?php $oldInput = Input::old(); $isOldTab2 = isset($oldInput['additionparams']) ? true : false; ?>
<div class="form-group {{ $errors->has('eventtypes') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('eventtypes', Lang::get('profile.eventtypes'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-3 col-md-3" {{ $errors->has('eventtypes') ? 'style="border: solid 1px #a94442"' : '' }}>
        @if (count($data['eventsType']))
            @foreach ($data['eventsType'] as $type)
                <div class="checkbox">
                    <?php
                        $checkedET = false;
                        if ($data['user']->eventstype->contains($type->id)){
                            $checkedET = true;
                        }
                    $checkedET = $isOldTab2 ? Input::old('eventtypes.'.$type->id, false) : $checkedET;
                    ?>
                    <label>
                        <input tabindex="1" type="checkbox" @if ($checkedET) checked="checked" @endif name="eventtypes[{{ $type->id }}]" value="{{ $type->id }}">
                        @if(App::getLocale() == 'en')
                            {{ $type->title_en }}
                        @elseif(App::getLocale() == 'ru')
                            {{ $type->title }}
                        @endif
                    </label>
                </div>
            @endforeach
        @endif
    </div>
</div>
@if($errors->has('eventtypes'))
    <div class="form-group">
        <div class="col-lg-3 col-md-3"></div>
        <div class="col-lg-3 col-md-3">
            {{ $errors->first('eventtypes') }}
        </div>
    </div>
@endif
<!-- ./ Event Types -->


<!-- Professions -->
<div class="form-group {{ $errors->has('professions') ? 'has-error' : '' }}">
    <div class="col-lg-3">
        {{ Form::label('professions', Lang::get('profile.professions_rates'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-9" {{ $errors->has('professions') ? 'style="border: solid 1px #a94442"' : '' }}>
        @if (count($data['professions']))
        @foreach ($data['professions'] as $prof)
            <?php
                $prh = '';
                $prd = '';
                if ($data['profile']->professions->contains($prof->id))
                {
                    $prh = $data['profile']->professions->find($prof->id)->pivot->rate_h;
                    $prd = $data['profile']->professions->find($prof->id)->pivot->rate_d;
                }

                $prh = trim(Input::old('prof_rate_h.'.$prof->id, $prh));
                $prd = trim(Input::old('prof_rate_d.'.$prof->id, $prd));

                $checked = false;
                if ($data['profile']->professions->contains($prof->id)){
                    $checked = true;
                }
                $checked = $isOldTab2 ? Input::old('professions.'.$prof->id, false) : $checked;
            ?>
        <div class="row form-group">
            <div class="col-lg-1">
                <input id="{{$prof->id}}" class="prof_check" tabindex="1" type="checkbox" @if ($checked) checked="checked" @endif name="professions[{{ $prof->id }}]" value="{{ $prof->id }}">
            </div>
            <div class="col-lg-3">
                @if(App::getLocale() == 'en')
                    {{ $prof->title_en }}
                @elseif(App::getLocale() == 'ru')
                    {{ $prof->title }}
                @endif
                &nbsp;{{Lang::get('profile.rate')}}:
            </div>
            <div class="col-lg-4">
                <input id="prh_{{ $prof->id }}" tabindex="2" type="text" maxlength="8" name="prof_rate_h[{{ $prof->id }}]" @if (!$checked) disabled="disabled" @endif value="{{$prh}}" placeholder="{{Lang::get('profile.rate_h')}}" />
            </div>
            <div class="col-lg-4">
                <input id="prd_{{ $prof->id }}" tabindex="3" type="text" maxlength="8" name="prof_rate_d[{{ $prof->id }}]" @if (!$checked) disabled="disabled" @endif value="{{$prd}}" placeholder="{{Lang::get('profile.rate_d')}}" />
            </div>
        </div>
        @endforeach
        @endif
        
    </div>
</div>
@if($errors->has('professions'))
    <div class="form-group">
        <div class="col-lg-3 col-md-3"></div>
        <div class="col-lg-3 col-md-3">
            {{ $errors->first('professions') }}
        </div>
    </div>
@endif

<!-- ./ Professions -->


<!-- Profile params -->
@if (count($data['mainParams']))
    @foreach ($data['mainParams'] as $mParam)
    <div class="form-group {{ $errors->has('profile_params_'.$mParam->id) ? 'has-error' : '' }}">
        <div class="col-lg-3 col-md-3">
            {{ Form::label('profile_params', $mParam->getTranslatedParamTitle(App::getLocale()), ['class' => 'control-label']) }}
        </div>
        <div class="col-lg-6 col-md-6">
            <?php $value = Input::old('profile_params.'.$mParam->id, $data['profile']->getParamValue($mParam)); ?>
            @include($mParam->getTemplateByType(), ['param' => $mParam, 'name' => 'profile_params', 'value' => $value, 'profile' => $data['profile']])
            <input type="hidden" name="valid_ruleArr[{{$mParam->id}}]" value="{{$mParam->validation_rule}}" />
            <input type="hidden" name="param_type[{{$mParam->id}}]" value="{{$mParam->type}}" />
            {{ $errors->first('profile_params_'.$mParam->id) }}
        </div>
    </div>
    @endforeach
@endif
<!-- ./ Profile params -->


<input type="hidden" name="additionparams" value="1" />

<!-- Form Actions -->
<div class="form-group">
    <div class="col-lg-3">
        {{--<button type="button" class="btn btn-default">{{Lang::get('button.cancel')}}</button>--}}
        {{--<button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>--}}
        <button type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
    </div>
</div>
<!-- ./ form actions -->
{{ Form::close() }}