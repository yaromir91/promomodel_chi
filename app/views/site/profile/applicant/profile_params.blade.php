{{ Form::open(array('class' => 'form-horizontal avatar-form', 'autocomplete' => 'off', 'files' => 'true', 'url' => URL::to('profile/' . $data['user']->id . '/edit-applicant'))) }}
<!-- CSRF Token -->
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<!-- ./ csrf token -->
<?php $oldInputT1 = Input::old(); $isOldTab1 = isset($oldInputT1['mainparams']) ? true : false; ?>
<!-- username -->
<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('username', Lang::get('profile.Username'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::text('username', Input::old('username', $data['user']->username), array('class' => 'form-control', 'id' => 'username')) }}
        {{ $errors->first('username') }}
    </div>
</div>
<!-- ./ username -->

<!-- Email -->
<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('email', Lang::get('profile.Email'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::text('email', Input::old('email', $data['user']->email), array('class' => 'form-control', 'id' => 'email')) }}
        {{ $errors->first('email') }}
    </div>
</div>
<!-- ./ email -->

<!-- Avatar -->
<div class="form-group {{ ($errors->has('avatar') || $errors->has('new_picture')) ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('avatar', Lang::get('profile.Avatar'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        @include('site.profile.applicant.user_avatar', [
        'fileFieldName'   => 'avatar',
        'origImage'       => ($data['profile']->avatar) ? asset(\Config::get('app.image_dir.profile') . $data['profile']->avatar) : '',
        'actionUrlUpload' => '/profile/upload'
        ])
        <div id="_token" token="{{ csrf_token() }}"></div>
        {{ $errors->first('avatar') }}
    </div>
</div>
<!-- ./ avatar -->

<!-- Last name -->
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('last_name', Lang::get('profile.LastName'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::text('last_name', Input::old('last_name', $data['profile']->last_name), array('class' => 'form-control', 'id' => 'last_name')) }}
        {{ $errors->first('last_name') }}
    </div>
</div>
<!-- ./ Last name -->

<!-- First name -->
<div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('first_name', Lang::get('profile.FirstName'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::text('first_name', Input::old('first_name', $data['profile']->first_name), array('class' => 'form-control', 'id' => 'first_name')) }}
        {{ $errors->first('first_name') }}
    </div>
</div>
<!-- ./ First name -->

<!-- Payment count -->
<div class="form-group {{ $errors->has('payment_count') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('payment_count', Lang::get('profile.payment_count'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::text('payment_count', Input::old('payment_count', $data['user']->payment_count), array('class' => 'form-control', 'id' => 'payment_count')) }}
        {{ $errors->first('payment_count') }}
    </div>
</div>
<!-- ./ Payment count -->

<!-- Gender -->
<div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('gender', Lang::get('profile.Gender'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::select('gender', array('m' => Lang::get('profile.Male'), 'f' => Lang::get('profile.Female')), Input::old('gender', $data['profile']->gender), array('class' => 'form-control', 'id' => 'gender'))}}
        {{ $errors->first('gender') }}
    </div>
</div>
<!-- ./ Gender -->

<!-- Date -->
<div class="form-group {{ $errors->has('date_birth') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('date_birth', Lang::get('profile.DateBirth'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6">
        <input type="text" id="date_birth" value="@if($data['profile']->date_birth != '0000-00-00') {{Input::old('date_birth', $data['profile']->date_birth)}} @endif" name="date_birth" class="onlydatepicker form-control">
        {{ $errors->first('date_birth') }}
    </div>
</div>
<!-- ./ date -->

<!-- Phone -->
<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('phone', Lang::get('profile.phone'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::text('phone', Input::old('phone', $data['profile']->phone), array('class' => 'form-control phone', 'id' => 'phone')) }}
        {{ $errors->first('phone') }}
    </div>
</div>
<!-- ./ Phone -->

<!-- VK link -->
<div class="form-group {{ $errors->has('vk_link') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('vk_link', Lang::get('profile.vk_link'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::text('vk_link', Input::old('vk_link', $data['profile']->vk_link), array('class' => 'form-control', 'id' => 'vk_link')) }}
        {{ $errors->first('vk_link') }}
    </div>
</div>
<!-- ./ VK link -->

<!-- Is Ready Manager -->
<div class="form-group {{ $errors->has('is_ready_manager') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('is_ready_manager', Lang::get('profile.is_ready_manager'), array('class' => 'control-label')) }}
        <?php
            $isCheckd = $isOldTab1 ? (isset($oldInputT1['is_ready_manager']) ? true : false) :  (($data['profile']->is_ready_manager) ? true: false);
        ?>
        <input tabindex="1" type="checkbox" @if ($isCheckd) checked="checked" @endif id="is_ready_manager" value="{{$isCheckd}}" name="is_ready_manager" />
        {{ $errors->first('is_ready_manager') }}
    </div>
</div>
<!-- ./ Is Ready Manager -->

<!-- Is public profile -->
<div class="form-group {{ $errors->has('public') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('public', Lang::get('profile.public'), array('class' => 'control-label')) }}
        <?php
        $isPublic = $isOldTab1 ? (isset($oldInputT1['public']) ? true : false) :  (($data['profile']->public) ? true: false);
        ?>
        <input tabindex="1" type="checkbox" @if ($isPublic) checked="checked" @endif id="public" value="{{$isPublic}}" name="public" />
        {{ $errors->first('public') }}
    </div>
</div>
<!-- ./ Is public profile -->

<input type="hidden" name="mainparams" value="1" />

<!-- Form Actions -->
<div class="form-group">
    <div class="col-lg-2 col-md-2">
        {{--<button type="button" class="btn btn-default">{{Lang::get('button.cancel')}}</button>--}}
        {{--<button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>--}}
        <button type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
    </div>
</div>
<!-- ./ form actions -->
{{ Form::close() }}
