@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('user/user.profile') }} ::
    @parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('profile.profile_settings') }}</h1>
    </div>
    <!-- Tabs -->
    <div class="tab-navigation">
        <ul class="nav nav-tabs">
            <li class="@if ($data['tab']=='1')active @endif"><a href="#tab-general" data-toggle="tab">{{ Lang::get('profile.main') }}</a></li>
            <li class="@if ($data['tab']=='2')active @endif"><a href="#tab-permissions" data-toggle="tab">{{ Lang::get('profile.additional') }}</a></li>
            <li class="@if ($data['tab']=='3')active @endif"><a href="#tab-password" data-toggle="tab">{{ Lang::get('profile.password') }}</a></li>
        </ul>
    </div>
    <!-- ./ tabs -->

        <!-- Tabs Content -->
        <div class="tab-content form-content">
            <!-- General tab -->
            <div class="tab-pane @if ($data['tab']=='1')active @endif" id="tab-general">
                   @include('site.profile.applicant.profile_params')
            </div>
            <!-- ./ general tab -->

            <!-- Permissions tab -->
            <div class="tab-pane @if ($data['tab']=='2')active @endif" id="tab-permissions">
                <div class="form-group">
                    @include('site.profile.applicant.additions_parameters')
                </div>
            </div>
            <!-- ./ permissions tab -->

            <!-- Password tab -->
            <div class="tab-pane @if ($data['tab']=='3')active @endif" id="tab-password">
                <div class="form-group">
                    @include('site.profile.applicant.password')
                </div>
            </div>
            <!-- ./ permissions tab -->
        </div>
        <!-- ./ tabs content -->

    @if ($data['msg'])
        <div class="modal fade" id="MessageModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                        {{--<h4 id="mySmallModalLabel" class="modal-title">Small modal<a href="#mySmallModalLabel" class="anchorjs-link"><span class="anchorjs-icon"></span></a></h4>--}}
                    </div>
                    <div class="modal-body">
                        {{$data['msg']}}
                    </div>
                </div>
            </div>
        </div>
        {{-- Scripts --}}
        @section('scripts')
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#MessageModal').modal();
                });
            </script>
        @stop
    @endif
@stop
