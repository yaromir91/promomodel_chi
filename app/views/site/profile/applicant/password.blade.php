{{ Form::open(array('class' => 'form-horizontal', 'autocomplete' => 'off', 'url' => URL::to('profile/' . $data['user']->id . '/edit-applicant'))) }}
<!-- CSRF Token -->
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<!-- ./ csrf token -->
<?php $oldInputT2 = Input::old(); $isOldTab2 = isset($oldInputT2['password']) ? true : false; ?>

@if($data['profile']->fb_status != 1)
<!-- Old password -->
<div class="form-group {{ $errors->has('old_password') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('old_password', Lang::get('profile.OldPassword'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::password('old_password', array('class' => 'form-control', 'id' => 'old_password')) }}
        {{ $errors->first('old_password') }}
    </div>
</div>
<!-- ./ password -->
@endif

<!-- New password -->
<div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('password', Lang::get('profile.NewPassword'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::password('new_password', array('class' => 'form-control', 'id' => 'new_password')) }}
        {{ $errors->first('new_password') }}
    </div>
</div>
<!-- ./ password -->

<!-- Password Confirm -->
<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
    <div class="col-lg-3 col-md-3">
        {{ Form::label('password_confirmation', Lang::get('profile.PasswordConfirm'), array('class' => 'control-label')) }}
    </div>
    <div class="col-lg-6 col-md-6">
        {{ Form::password('password_confirmation', array('class' => 'form-control', 'id' => 'password_confirmation')) }}
        {{ $errors->first('password_confirmation') }}
    </div>
</div>
<!-- ./ password confirm -->

<input type="hidden" name="password" value="1" />

<!-- Form Actions -->
<div class="form-group">
    <div class="col-lg-3 col-md-3">
        {{--<button type="button" class="btn btn-default">{{Lang::get('button.cancel')}}</button>--}}
        {{--<button type="reset" class="btn btn-default">{{Lang::get('button.reset')}}</button>--}}
        <button type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
    </div>
</div>
<!-- ./ form actions -->
{{ Form::close() }}