<div id="crop-avatar">
    <div class="avatar-view @if(!$origImage) load-btn-picture btn_{{ App::getLocale() }} @endif" title="{{Lang::get('general.change_the_picture')}}">
        @if($origImage)
            <img width="150px" src="{{ $origImage }}">
        @endif
        <input name="new_picture" value="@if(isset($origImageName)){{ $origImageName }}@endif" type="hidden" class="the-new-picture" />
    </div>

    <input type="hidden" class="action_url_upload" value="{{$actionUrlUpload}}" />
    <!-- Cropping modal -->
    <div class="modal fade" id="picture-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="avatar-modal-label">{{Lang::get('general.change_picture')}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="avatar-body">

                            <!-- Upload image and data -->
                            <div class="avatar-upload">
                                <input type="hidden" class="avatar-src" name="avatar_src">
                                <input type="hidden" class="avatar-data" name="avatar_data">
                                <input type="file" class="avatar-input" id="avatarInput" name="{{$fileFieldName}}">
                            </div>

                            <!-- Crop and preview -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="avatar-wrapper hidden">
                                    </div>

                                    <div class="row avatar-btns hidden">
                                        <div class="col-md-9">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="{{Lang::get('general.rotate_left')}}">
                                                    <i class="fa fa-undo"></i>
                                                </button>
                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">15</button>
                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">30</button>
                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">45</button>
                                            </div>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="{{Lang::get('general.rotate_right')}}">
                                                    <i class="fa fa-repeat"></i>
                                                </button>
                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15</button>
                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30</button>
                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45</button>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" class="btn btn-primary btn-block avatar-save">OK</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
            </div>
        </div>
    </div><!-- /.modal -->

    <!-- Loading state -->
    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
</div>
