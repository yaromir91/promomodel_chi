@if($imagesCount>0)
        <div id="slider1_container" class="main-slider-container-user-profile">

            <div u="loading" class="loading-slading-main">
                <div class="loading-slading-inner1"></div>
                <div class="slider1_container_loading"></div>
            </div>

            <div u="slides" class="profile-alboms-list">

                @if(count($gallery))
                    @foreach ($gallery as $gal)
                        @if ($gal->preview)
                            <div>
                                <img u="image" src="{{ asset(\Config::get('app.image_dir.album_slider') . $gal->preview) }}" />
                                <div u=caption t="*" class="captionOrange curr-poss-img">
                                    <a class="slide-full-link" href="{{ URL::to('gallery/show/' . $gal->id) }}">{{ Lang::get('gallery.View') }}</a>
                                </div>
                                <img u="thumb" src="{{ asset(\Config::get('app.image_dir.album') . $gal->user_id . '/' . $gal->preview) }}" />
                            </div>
                        @endif
                    @endforeach
                @endif

                @if(count($previewImg))
                    @foreach ($previewImg as $gal)
                        @if ($gal->file)
                            <div>
                                <img u="image" src="{{ asset(\Config::get('app.image_dir.album') . $gal->user_id . '/preview/' . $gal->file) }}" />
                                <div u=caption t="*" class="captionOrange curr-poss-img">
                                    <a class="slide-full-link" href="{{ URL::to('gallery/show/' . $gal->gallery_id) }}">{{ Lang::get('gallery.View') }}</a>
                                </div>
                                <img u="thumb" src="{{ asset(\Config::get('app.image_dir.album') . $gal->user_id . '/small/' . $gal->file) }}" />
                            </div>
                        @endif
                    @endforeach
                @endif

            </div>

        <span u="arrowleft" class="jssora05l the-arrowleft"></span>

        <span u="arrowright" class="jssora05r the-arrowright"></span>

            <div u="thumbnavigator" class="jssort01 the-thumbnavigator">
                <div u="slides" class="thumb-list">
                    <div u="prototype" class="p">
                        <div class=w><div u="thumbnailtemplate" class="t"></div></div>
                        <div class=c></div>
                    </div>
                </div>
            </div>
        </div>
@endif