
<div class="col-lg-3 col-md-3 col-sm-4 main-info-menu">
    <div class="photo-album">
        @if($profile->avatar)
            <img src="{{ asset(\Config::get('app.image_dir.profile') . $profile->avatar) }}">
        @else
            <img src="{{ asset(\Config::get('app.image_dir.stub')) }}">
        @endif
    </div>
    <div class="photo-info menu">
        <ul>
            @if ($roleCode != \Acme\Models\Role::AGENT_USER)
                <li>
                    <a href="{{ URL::route('own_user_events') }}">{{ Lang::get('user/user.my_events') }}</a>
                </li>
            @endif
            <li><a href="{{ URL::route('report_personal_list') }}">{{ Lang::get('user/user.my_reports') }}</a></li>
            <li><a href="{{ URL::route('notifications') }}">{{ Lang::get('user/user.my_notifications') }}</a></li>
            @if ($roleCode==\Acme\Models\Role::APPLICANT_MANAGER_USER || $roleCode==\Acme\Models\Role::APPLICANT_USER)
                <li><a href="{{ URL::route('my_group') }}">{{ Lang::get('user/user.my_groups') }}</a></li>
                <li><a href="{{ URL::route('user_invitation_list') }}">{{ Lang::get('user/user.invitations') }}</a></li>
            @endif
            @if ($roleCode==\Acme\Models\Role::AGENT_USER)
                <li><a href="{{ URL::route('agent_applicants_list') }}">{{ Lang::get('profile.my_applicants') }}</a></li>
            @endif
            <li><a href="{{ URL::route('chat_list') }}">{{ Lang::get('profile.my_messages') }}</a></li>
        </ul>
    </div>

</div>
<div class="col-lg-9 col-md-9 col-sm-8">
    <h3 class="no-border">{{ Lang::get('applicants.main_info') }}</h3>
    <ul>
        <li>
            <p>{{ Lang::get('profile.Username') }}:<span>&nbsp;{{$user->username}}</span></p>
        </li>
        <li>
            <p>{{ Lang::get('profile.Email') }}:<span>&nbsp;{{$user->email}}</span></p>
        </li>
        @if ($profile instanceof Acme\Models\Profile\UserProfile)
            <li>
                <p>{{ Lang::get('profile.Full name') }}:<span>&nbsp;{{$profile->getFullName()}}</span></p>
            </li>
            <li>
                <p>{{ Lang::get('profile.Gender') }}:<span>&nbsp;{{Acme\Models\Profile\UserProfile::getUserGender($profile->gender)}}</span></p>
            </li>
            @if($profile->getAge())
                <li>
                    <p>{{ Lang::get('profile.age') }}:<span>&nbsp;{{$profile->getAge()}}</span></p>
                </li>
            @endif
            @if($profile->phone)
                <li>
                    <p>{{ Lang::get('profile.phone') }}:<span>&nbsp;{{$profile->phone}}</span></p>
                </li>
            @endif

            @if($profile->vk_link)
                <li>
                    <p>{{ Lang::get('profile.vk_link') }}:<span>&nbsp; @if($profile->vk_link)
                    <a target="_blank" href="{{$profile->vk_link}}">{{ Lang::get('profile.click_here') }}</a>
                    @endif</span></p>
                </li>
            @endif

        @endif

        @include('site.applicants.partials.eventstypeinfo')
        @include('site.applicants.partials.profinfo')
        @include('site.applicants.partials.addition_params_info')
    </ul>
    <div class="row">
        <div class="item-container">
            @include('site.profile.partials._user_images')
        </div>
    </div>
    <div class="row">
        <div class="item-container">
            @include('site.profile.partials._user_gallery')
        </div>
    </div>
    <div class="row">
        <div class="item-container">
            @include('site.profile.partials._users_events')
        </div>
    </div>
    <div class="row">
        <div class="item-container">
            @include('site.profile.partials._users_reports')
        </div>
    </div>
    <div class="row">
        <div class="item-container">
            @include('site.profile.partials._users_groups')
        </div>
    </div>


</div>


