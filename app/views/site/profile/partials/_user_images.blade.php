@if (count($images))
    <div class="col-lg-12 col-md-12 col-sm-12"><h3 class="no-border">
            <a href="{{ URL::route('gallery_personal_photo') }}">
                {{ Lang::get('user/user.my_photos') }}
            </a>
        </h3></div>
    @foreach($images as $elem)
        <div class="col-lg-3 col-md-3 col-sm-4 item">
            @if ($elem->file)
                <img class="profile-image" src="{{ asset(\Config::get('app.image_dir.album') . $user->id . '/small/' . $elem->file) }}">
            @else
                {{\Config::get('app.album_no_image')}}
            @endif
        </div>
    @endforeach
@endif