@if (count($reports))
    <div class="col-lg-12 col-md-12 col-sm-12">
        <h3 class="no-border"><a href="{{ URL::route('report_personal_list') }}">{{ Lang::get('report.my_title') }}</a></h3>
    </div>
    @foreach($reports as $report)
        <div class="col-lg-3 col-md-3 col-sm-4 item">
            <div class="photo-album">
                @if($report->image)
                <img class="profile-image" src="{{ asset(\Config::get('app.image_dir.posts_small') . $report->image) }}">
                @else
                {{\Config::get('app.album_no_image')}}
                @endif
            </div>
            <div class="photo-info">
                <a href="{{ $report->url() }}">{{ String::title($report->title) }}</a>
            </div>
        </div>
    @endforeach
@endif