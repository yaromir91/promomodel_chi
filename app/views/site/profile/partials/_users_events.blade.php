@if (count($myPublicEvents))
    <div class="col-lg-12 col-md-12 col-sm-12">
        <h3 class="no-border"><a href="{{ URL::route('own_user_events') }}">{{ Lang::get('events.my_events') }}</a></h3>
    </div>
    @foreach($myPublicEvents as $event)
        <div class="col-lg-3 col-md-3 col-sm-4 item">
            <div class="photo-album">
                @if ($event->image)
                <img class="profile-image" src="{{ asset(\Config::get('app.image_dir.events_small') . $event->image) }}">
                @else
                {{\Config::get('app.album_no_image')}}
                @endif
            </div>
            <div class="photo-info">
                <a href="{{ URL::to('events/show/' . $event->id) }}">{{ $event->title }}</a>
                <p>{{ $event->date_start }}</p>
            </div>
        </div>
    @endforeach
@endif