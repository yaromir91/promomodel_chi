@if (count($invitation))
    <div class="col-lg-12 col-md-12 col-sm-12">
        <h3 class="no-border">
            @if($isShowInvitLink)
                <a href="{{ URL::route('event_my_invitation') }}">{{ Lang::get('events.Invitations') }}</a>
            @else
                {{ Lang::get('events.Invitations') }}
            @endif
        </h3>
    </div>
    @foreach($invitation as $inv)
        <?php $event = $inv->event; $user = $inv->user; ?>
        @if(($event instanceof Acme\Models\Events\Events) && ($user instanceof Acme\Models\User))
            <div class="col-lg-3 col-md-3 col-sm-4 item">
                @if($isShowInvitLink)
                    <div class="photo-album">
                        @if ($event->image)
                        <img class="profile-image" src="{{ asset(\Config::get('app.image_dir.events') . $event->image) }}">
                        @else
                        {{\Config::get('app.album_no_image')}}
                        @endif
                    </div>
                    <div class="photo-info">
                        <a href="{{ URL::route('show_events', $event->id) }}">{{ $event->title }}</a>
                    </div>
                @else
                    <?php $profile = $user->profile; ?>
                    <div class="photo-album">
                        @if($profile instanceof Acme\Models\Profile\UserProfile)
                        @if ($profile->avatar)
                            <img class="profile-image" src="{{ asset(\Config::get('app.image_dir.profile') . $profile->avatar) }}">
                        @else
                        {{\Config::get('app.album_no_image')}}
                        @endif
                        @else
                        @if ($event->image)
                            <img class="profile-image" src="{{ asset(\Config::get('app.image_dir.events') . $event->image) }}">
                        @else
                        {{\Config::get('app.album_no_image')}}
                        @endif
                        @endif
                    </div>
                    <div class="photo-info">
                        <a href="{{ URL::route('show_events', $event->id) }}">{{ $event->title }}</a>
                        <a href="{{ URL::route('view_user_profile', $user->id) }}">{{ $user->getUserName()  }}</a>
                    </div>
                @endif

                <p><span>{{\Lang::get('events.status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentStatusTitle($inv->status) }}</p>
                @if($inv->is_manager == 1)
                    <p><span>{{\Lang::get('events.manager_status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentManagerStatusTitle($inv->manager_status) }}</p>
                @endif
                <p><span>{{\Lang::get('events.team_status')}}</span>: {{ Acme\Models\Events\EventInvite::getCurrentTeamStatusTitle($inv->in_team) }}</p>
                    <p><span>{{\Lang::get('events.title_events_profession')}}</span>: {{ $inv->getProfessionName() }}</p>
            </div>
        @endif
    @endforeach
@endif