@if (count($gallery))
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="no-border"><a href="{{ URL::route('gallery_personal_album') }}">{{ Lang::get('user/user.my_albums') }}</a></h3>
    </div>
    @foreach($gallery as $gal)
        <div class="col-lg-3 col-md-3 col-sm-4 item">
            <div class="photo-album">
                @if ($gal->preview)
                <img class="profile-image" src="{{ asset(\Config::get('app.image_dir.album') . $gal->user_id . '/' . $gal->preview) }}">
                @else
                {{\Config::get('app.album_profile_image_262_262')}}
                @endif
            </div>
            <div class="photo-info">
                <a href="{{ URL::to('gallery/personal/album/show/' . $gal->id) }}">{{ $gal->title }}</a>
            </div>
        </div>
    @endforeach
@endif