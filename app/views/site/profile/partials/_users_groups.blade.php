@if (count($groups))
    <div class="col-lg-12 col-md-12 col-sm-12">
        <h3 class="no-border"><a href="{{ URL::route('groups') }}">{{ Lang::get('user/user.my_groups') }}</a></h3>
    </div>
    @foreach($groups as $group)
        @if($group instanceof Acme\Models\Groups)
            <div class="col-lg-3 col-md-3 col-sm-4 item">
                <div class="photo-album">
                    @if ($group->image)
                    <img class="profile-image" src="{{ asset(\Config::get('app.image_dir.groups_small') . $group->image) }}">
                    @else
                    {{\Config::get('app.album_no_image')}}
                    @endif
                </div>
                <div class="photo-info">
                    <a href="{{ URL::route('show_group' , $group->id) }}">{{ $group->title }}</a>
                </div>
            </div>
        @endif
    @endforeach
@endif