@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('user/user.profile') }} ::
    @parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('profile.profile_settings') }}</h1>
    </div>
    <!-- Tabs -->
    <div class="tab-navigation clearfix">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab">{{ Lang::get('profile.main') }}</a></li>
        </ul>
    </div>
    <!-- ./ tabs -->

    {{-- Create Role Form --}}
    <form class="form-horizontal avatar-form" method="post" action="{{ URL::to('profile/' . $data['user']->id . '/edit') }}"  autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <!-- ./ csrf token -->

        <!-- Tabs Content -->
        <div class="tab-content">
            <!-- General tab -->
            <div class="tab-pane active" id="tab-general">
                <!-- username -->
                <div class="form-group {{ $errors->has('username') ? 'error' : '' }}">
                    <label class="col-md-2 control-label" for="username">{{Lang::get('profile.Username')}}</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="username" id="username" value="{{ Input::old('username', $data['user']->username) }}" />
                        {{ $errors->first('username', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ username -->

                <!-- Email -->
                <div class="form-group {{ $errors->has('email') ? 'error' : '' }}">
                    <label class="col-md-2 control-label" for="email">{{Lang::get('profile.Email')}}</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="email" id="email" value="{{ Input::old('email', $data['user']->email) }}" />
                        {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ email -->

                <!-- Avatar -->
                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    {{ Form::label('avatar', Lang::get('profile.Avatar'), array('class' => 'col-md-2 control-label')) }}
                    <div class="col-md-5">

                        @include('site.profile.applicant.user_avatar', [
                        'fileFieldName'   => 'avatar',
                        'origImage'       => ($data['profile']->avatar) ? asset(\Config::get('app.image_dir.profile') . $data['profile']->avatar) : '',
                        'actionUrlUpload' => '/profile/upload'
                        ])

                        <div id="_token" token="{{ csrf_token() }}"></div>

                        {{ $errors->first('avatar') }}
                    </div>
                </div>
                <!-- ./ avatar -->

                <!-- Password -->
                <div class="form-group {{ $errors->has('password') ? 'error' : '' }}">
                    <label class="col-md-2 control-label" for="password">{{Lang::get('profile.Password')}}</label>
                    <div class="col-md-10">
                        <input class="form-control" type="password" name="password" id="password" value="" />
                        {{ $errors->first('password', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ password -->

                <!-- Password Confirm -->
                <div class="form-group {{ $errors->has('password_confirmation') ? 'error' : '' }}">
                    <label class="col-md-2 control-label" for="password_confirmation">{{Lang::get('profile.Password Confirm')}}</label>
                    <div class="col-md-10">
                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="" />
                        {{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <!-- ./ password confirm -->
            </div>
            <!-- ./ general tab -->

        </div>
        <!-- ./ tabs content -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn btn-success">{{Lang::get('button.save')}}</button>
            </div>
        </div>
        <!-- ./ form actions -->
    {{ Form::close() }}
    @if ($data['msg'])
        <div class="modal fade" id="MessageModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                        {{--<h4 id="mySmallModalLabel" class="modal-title">Small modal<a href="#mySmallModalLabel" class="anchorjs-link"><span class="anchorjs-icon"></span></a></h4>--}}
                    </div>
                    <div class="modal-body">
                        {{$data['msg']}}
                    </div>
                </div>
            </div>
        </div>
        {{-- Scripts --}}
        @section('scripts')
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#MessageModal').modal();
                });
            </script>
        @stop
    @endif
@stop
