{{-- Create Role Form --}}
{{ Form::open(array('class' => 'form-horizontal avatar-form', 'autocomplete' => 'off', 'files' => 'true', 'url' => URL::to('profile/' . $data['user']->id . '/edit-organizer'))) }}
    <?php $oldInputT1 = Input::old(); $isOldTab1 = isset($oldInputT1['mainparams']) ? true : false; ?>
    <!-- username -->
    <div class="form-group {{ $errors->has('username') ? 'error' : '' }}">
        <label class="col-lg-3 col-md-3 col-sm-4 control-label" for="username">{{Lang::get('profile.Username')}}</label>
        <div class="col-lg-6 col-md-6 col-sm-8">
            <input class="form-control" type="text" name="username" id="username" value="{{ Input::old('username', $data['user']->username) }}" />
            {{ $errors->first('username', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ username -->

    <!-- Email -->
    <div class="form-group {{ $errors->has('email') ? 'error' : '' }}">
        <label class="col-lg-3 col-md-3 col-sm-4  control-label" for="email">{{Lang::get('profile.Email')}}</label>
        <div class="col-lg-6 col-md-6 col-sm-8">
            <input class="form-control" type="text" name="email" id="email" value="{{ Input::old('email', $data['user']->email) }}" />
            {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
        </div>
    </div>
    <!-- ./ email -->


    <!-- Avatar -->
    <div class="form-group {{ ($errors->has('avatar') || $errors->has('new_picture')) ? 'has-error' : '' }}">
        {{ Form::label('avatar', Lang::get('profile.Avatar'), array('class' => 'col-md-3 col-sm-4 control-label')) }}
        <div class="col-lg-6 col-md-6 col-sm-8">

            @include('site.profile.applicant.user_avatar', [
            'fileFieldName'   => 'avatar',
            'origImage'       => ($data['profile']->avatar) ? asset(\Config::get('app.image_dir.profile') . $data['profile']->avatar) : '',
            'actionUrlUpload' => '/profile/upload'
            ])

            <div id="_token" token="{{ csrf_token() }}"></div>
            @if($errors->has('avatar')){{ $errors->first('avatar') }}@endif
            @if($errors->has('new_picture')){{ $errors->first('new_picture') }}@endif
        </div>
    </div>
    <!-- ./ avatar -->

    <!-- Last name -->
    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
        {{ Form::label('last_name', Lang::get('profile.LastName'), array('class' => 'col-md-3 col-sm-4 control-label')) }}
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::text('last_name', Input::old('last_name', $data['profile']->last_name), array('class' => 'form-control', 'id' => 'last_name')) }}
            {{ $errors->first('last_name') }}
        </div>
    </div>
    <!-- ./ Last name -->

    <!-- First name -->
    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
        {{ Form::label('first_name', Lang::get('profile.FirstName'), array('class' => 'col-md-3 col-sm-4 control-label')) }}
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::text('first_name', Input::old('first_name', $data['profile']->first_name), array('class' => 'form-control', 'id' => 'first_name')) }}
            {{ $errors->first('first_name') }}
        </div>
    </div>
    <!-- ./ First name -->

    <!-- Gender -->
    <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
        {{ Form::label('gender', Lang::get('profile.Gender'), array('class' => 'col-md-3 col-sm-4 control-label')) }}
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::select('gender', array('m' => Lang::get('profile.Male'), 'f' => Lang::get('profile.Female')), Input::old('gender', $data['profile']->gender), array('class' => 'form-control', 'id' => 'gender'))}}
            {{ $errors->first('gender') }}
        </div>
    </div>
    <!-- ./ Gender -->

    <!-- Date -->
    <div class="form-group {{ $errors->has('date_birth') ? 'has-error' : '' }}">
        {{ Form::label('date_birth', Lang::get('profile.DateBirth'), array('class' => 'col-md-3 col-sm-4 control-label')) }}
        <div class="col-lg-6 col-md-6 col-sm-8">
            <div class='input-group'>

                <input type="text" id="date_birth" value="@if($data['profile']->date_birth != '0000-00-00') {{Input::old('date_birth', $data['profile']->date_birth)}} @endif" name="date_birth" class="onlydatepicker form-control">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
            </div>
            {{ $errors->first('date_birth') }}
        </div>
    </div>
    <!-- ./ date -->

    <!-- Phone -->
    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
        {{ Form::label('phone', Lang::get('profile.phone'), array('class' => 'col-md-3 col-sm-4 control-label')) }}
        <div class="col-lg-6 col-md-6 col-sm-8">
            {{ Form::text('phone', Input::old('phone', $data['profile']->phone), array('class' => 'form-control phone', 'id' => 'phone')) }}
            {{ $errors->first('phone') }}
        </div>
    </div>
    <!-- ./ Phone -->

    <!-- Is public profile -->
    <div class="form-group {{ $errors->has('public') ? 'has-error' : '' }}">
        {{ Form::label('public', Lang::get('profile.public'), array('class' => 'col-md-3 col-sm-4 control-label')) }}
        <div class="col-lg-6 col-md-6 col-sm-8">
            <?php
            $isPublic = $isOldTab1 ? (isset($oldInputT1['public']) ? true : false) :  (($data['profile']->public) ? true: false);
            ?>
            <input tabindex="1" type="checkbox" @if ($isPublic) checked="checked" @endif id="public" value="{{$isPublic}}" name="public" />
            {{ $errors->first('public') }}
        </div>
    </div>
    <!-- ./ Is public profile -->

    <input type="hidden" name="mainparams" value="1" />

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-lg-2 col-md-2 col-sm-3 ">
            {{--<button type="button" class="btn btn-submit">{{Lang::get('button.cancel')}}</button>--}}
            {{--<button type="reset" class="btn btn-submit">{{Lang::get('button.reset')}}</button>--}}
            <button type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
        </div>
    </div>
    <!-- ./ form actions -->
{{ Form::close() }}