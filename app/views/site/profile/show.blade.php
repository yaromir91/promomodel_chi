@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('profile.title') }} ::
    @parent
@stop


{{-- Content --}}
@section('content')
<div class="profile user-profile">
    <div class="page-header clearfix">
        <h1>{{ Lang::get('profile.title') }}</h1>
    </div>
    <div class="pull-right">
        <a class="btn btn-submit" href="{{ URL::route('view_user_profile', $user->id) }}">{{ Lang::get('button.look') }}</a>
        <a class="btn btn-submit" href="{{ URL::route('edit_profile') }}">{{ Lang::get('button.edit') }}</a>
    </div>
    <div class="row">
        @include('site.profile.partials.main_info')
    </div>
</div>

@stop
