@if ($param instanceof Acme\Models\Profile\ProfileParams)

    @if (count($param->children))
        @foreach($param->children as $pchild)
        <?php
            $savedVal = $profile->checkValueCheckbox($param->id, $pchild->id);
            $valueC   = Input::old('profile_params.'.$pchild->id, $savedVal);
        ?>
            <label class="custom-checkbox-inline">
                <input @if ($valueC) checked @endif type="checkbox" value="{{$pchild->id}}" name="profile_params[{{$param->id}}][]" />
                @if(App::getLocale() == 'en')
                    {{ $pchild->name_en }}
                @elseif(App::getLocale() == 'ru')
                    {{ $pchild->name }}
                @endif
            </label>
        @endforeach
    @endif
@endif