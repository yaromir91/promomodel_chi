@if ($param instanceof Acme\Models\Profile\ProfileParams)
<option @if ($value==$param->id) selected="selected" @endif value="{{$param->id}}">
    @if(App::getLocale() == 'en')
        {{ $pchild->name_en }}
    @elseif(App::getLocale() == 'ru')
        {{ $pchild->name }}
    @endif
</option>
@endif