@if ($param instanceof Acme\Models\Profile\ProfileParams)
    @if (count($param->children))
        <select name="{{$name.'['.$param->id.']'}}"  class="form-control">
            <option value="">{{Lang::get('general.select')}}</option>
            @foreach ($param->children as $pchild)
                @include($pchild->getTemplateByType(), ['param' => $pchild, 'name' => 'profile_params', 'value' => $value ])
            @endforeach
        </select>
    @endif
@endif