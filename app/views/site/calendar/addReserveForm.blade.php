@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} ::
    @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ $title }}</h1>
    </div>

{{ Form::open(array('class' => 'form-horizontal', 'autocomplete' => 'off', 'url' => URL::to('calendar/add-reserve'))) }}
    <div class="content">
        <!-- Title -->
        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
            <div class="col-lg-3">
                {{ Form::label('title', Lang::get('calendar.field_title'), array('class' => 'control-label')) }}
            </div>
            <div class="col-lg-6">
                {{ Form::text('title', Input::old('title', ''), array('class' => 'form-control', 'id' => 'title')) }}
                {{ $errors->first('title') }}
            </div>
        </div>
        <!-- ./ title -->

        <!-- Description -->
        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
            <div class="col-lg-3">
                {{ Form::label('description', Lang::get('events.title_description'), array('class' => 'control-label')) }}
            </div>
            <div class="col-lg-6">
                {{ Form::textarea('description', Input::old('description', ''), array('class' => 'form-control', 'id' => 'description')) }}
                {{ $errors->first('description', '<span class="help-block">:message</span>') }}
            </div>
        </div>
        <!-- ./ description -->

        <!-- Repeat start/end -->
        <div class="form-group {{ $errors->has('datetimepickerStart') ? 'has-error' : '' }}">
            <div class="col-lg-3">
                {{ Form::label('date', Lang::get('calendar.title_date_start'), array('class' => 'control-label')) }}
            </div>
            <div class='col-lg-6'>
                <div class='input-group date'>
                    {{ Form::text('datetimepickerStart', Input::old('datetimepickerStart', ''), array('class' => 'form-control datepicker', 'id' => 'datetimepickerStart')) }}
                    <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                {{ $errors->first('datetimepickerStart') }}
            </div>
        </div>
        <div class="form-group {{ $errors->has('datetimepickerEnd') ? 'has-error' : '' }}">
            <div class="col-lg-3">
                {{ Form::label('date', Lang::get('calendar.title_date_end'), array('class' => 'control-label')) }}
            </div>
            <div class="col-lg-6">
                <div class='input-group date'>
                    {{ Form::text('datetimepickerEnd', Input::old('datetimepickerEnd', ''), array('class' => 'form-control datepicker', 'id' => 'datetimepickerEnd')) }}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                {{ $errors->first('datetimepickerEnd') }}
            </div>
        </div>
        <!-- ./ repeat start/end -->

        <!-- Repeat -->
        <div class="form-group {{ $errors->has('repeat') ? 'error' : '' }}">
            <div class="col-lg-9">
                <input name="repeat" type="checkbox" id="RepeatCheckbox" @if(Input::old('repeat')) checked @endif value="1">
                {{ Form::label('description', Lang::get('calendar.title_repeat'), array('class' => 'control-label')) }}
                <a id="EditRepeatLink" @if(Input::old('repeat')) style="display: inline;" @endif href="#">{{ Lang::get('calendar.repeat_edit') }}</a>
                <div class="repeat-data-error">
                    {{ Lang::get('calendar.error_repeat_empty_fields') }}
                </div>
                <div class="repeat-time-error">
                    {{ Lang::get('calendar.error_repeat_wrong_data') }}
                </div>
                <input type="text" value="{{ Input::old('show_repeat') }}" @if(Input::old('repeat')) style="display: inline;" @endif disabled id="EditRepeatValueInput" class="form-control">
                <input id="ShowRepeat" type="hidden" name="show_repeat" value="{{ Input::old('show_repeat') }}" />
            </div>
        </div>
        <!-- ./ repeat -->


        {{--<button type="button" id="RepeatButton" class="btn btn-info btn-small">repeat</button>--}}
        <!-- Form Actions -->
        <div class="form-group">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <button type="button" class="btn btn-submit" onclick="window.location = '{{ URL::to('calendar') }}'">{{Lang::get('button.cancel')}}</button>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                {{--<button type="reset" class="btn btn-submit">{{Lang::get('button.reset')}}</button>--}}
                <button type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </div>

    <!-- Modal -->
    <div class="modal repeat_popup" id="RepeatPopup" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content calendar-modal">
                <div class="modal-header">
                    {{--<button type="button" class="close CloseRepeatPopup">&times;</button>--}}
                    {{--<h4 class="modal-title">Modal Header</h4>--}}
                </div>
                <div class="modal-body">
                    <div class="row repeat_row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <span>{{ Lang::get('calendar.reserve_repeat_event') }}</span>
                        </div>
                        <div class="col-lg-4 calendar-reserve-date">
                            <select name="reserve[repeat_type]" id="label-repeat" class="js-type-changer">
                                <option @if(Input::old('reserve')['repeat_type'] == 'daily' || Input::old('reserve')['repeat_type'] == null) selected="" @endif value="daily">{{ Lang::get('calendar.repeat.repeat_everyday') }}</option>
                                <option @if(Input::old('reserve')['repeat_type'] == 'weekly') selected="" @endif id="WeeklyOption" value="weekly">{{ Lang::get('calendar.repeat.repeat_everyweek') }}</option>
                                <option @if(Input::old('reserve')['repeat_type'] == 'monthly') selected="" @endif value="monthly">{{ Lang::get('calendar.repeat.repeat_everymonth') }}</option>
                                <option @if(Input::old('reserve')['repeat_type'] == 'yearly') selected="" @endif value="yearly">{{ Lang::get('calendar.repeat.repeat_everyyear') }}</option>
                            </select>
                        </div>

                    </div>
                    <div class="row repeat-data" id="daily">
                        <div class="col-lg-4 col-md-4 col-sm-4 calendar-reserve-date">
                            <span>{{ Lang::get('calendar.reserve_every') }}:</span>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 calendar-reserve-date">
                            <select name="reserve[repeat_daily]" class="daily_select">
                                <option @if(Input::old('reserve')['repeat_daily'] == 1 || Input::old('reserve')['repeat_type'] == null) selected="" @endif value="1">1</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 2) selected="" @endif value="2">2</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 3) selected="" @endif value="3">3</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 4) selected="" @endif value="4">4</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 5) selected="" @endif value="5">5</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 6) selected="" @endif value="6">6</option>
                                <option @if(Input::old('reserve')['repeat_daily'] == 7) selected="" @endif value="7">7</option>
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 calendar-reserve-date">{{ Lang::get('calendar.reserve_day') }}</div>
                        <div class="col lg-4 col-md-4 col-sm-4 calendar-reserve-date">{{ Lang::get('calendar.reserve_last_repeat') }}:</div>
                        <div class="col-lg-4 col-md-4 col-sm-4 calendar-reserve-date"">
                            {{ Form::text('reserve[datetimepickerDaily]', Input::old('reserve')['datetimepickerDaily'], array('class' => 'form-control datepicker', 'id' => 'datetimepickerDaily')) }}
                        </div>
                    </div>
                    <div class="row repeat-data" id="weekly">
                        <div>
                            <?php
                                if(isset(Input::old('reserve')['weekly_repeat'])) {
                                    $weekly_repeat = Input::old('reserve')['weekly_repeat'];
                                } else {
                                    $weekly_repeat = false;
                                }
                            ?>
                            {{ Lang::get('calendar.repeat.every') }}:
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(1, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat1" type="checkbox" name="reserve[weekly_repeat][]" value="1">
                                {{ Lang::get('calendar.js_repeat_week_day_1') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(2, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat2" type="checkbox" name="reserve[weekly_repeat][]" value="2">
                                {{ Lang::get('calendar.js_repeat_week_day_2') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(3, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat3" type="checkbox" name="reserve[weekly_repeat][]" value="3">
                                {{ Lang::get('calendar.js_repeat_week_day_3') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(4, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat4" type="checkbox" name="reserve[weekly_repeat][]" value="4">
                                {{ Lang::get('calendar.js_repeat_week_day_4') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(5, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat5" type="checkbox" name="reserve[weekly_repeat][]" value="5">
                                {{ Lang::get('calendar.js_repeat_week_day_5') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(6, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat6" type="checkbox" name="reserve[weekly_repeat][]" value="6">
                                {{ Lang::get('calendar.js_repeat_week_day_6') }}</label>
                            <label class="checkbox-inline">
                                <input @if($weekly_repeat && in_array(7, $weekly_repeat)) checked @endif class="weekly_repeat" id="WeeklyRepeat7" type="checkbox" name="reserve[weekly_repeat][]" value="7">
                                {{ Lang::get('calendar.js_repeat_week_day_7') }}</label>

                        </div>
                        <div id="RepeatWeeklyError">
                            {{ Lang::get('calendar.reserve_error_none_weekday') }}
                        </div>
                        <div class="last_repeat">
                            {{ Lang::get('calendar.reserve_last_repeat') }}: {{ Form::text('reserve[datetimepickerWeekly]', Input::old('reserve')['datetimepickerWeekly'], array('class' => 'form-control datepicker', 'id' => 'datetimepickerWeekly')) }}
                        </div>
                    </div>
                    <div class="row repeat-data" id="monthly">
                        <div>
                            {{ Lang::get('calendar.repeat.every') }}:
                            <select name="reserve[repeat_monthly]" class="monthly_select">
                                <option @if(Input::old('reserve')['repeat_monthly'] == 1) selected="" @endif selected="" value="1">1</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 2) selected="" @endif value="2">2</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 3) selected="" @endif value="3">3</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 4) selected="" @endif value="4">4</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 5) selected="" @endif value="5">5</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 6) selected="" @endif value="6">6</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 7) selected="" @endif value="7">7</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 8) selected="" @endif value="8">8</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 9) selected="" @endif value="9">9</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 10) selected="" @endif value="10">10</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 11) selected="" @endif value="11">11</option>
                                <option @if(Input::old('reserve')['repeat_monthly'] == 12) selected="" @endif value="12">12</option>
                            </select>
                            {{ Lang::get('calendar.reserve_month') }}
                        </div>
                        <div class="last_repeat">
                            {{ Lang::get('calendar.reserve_last_repeat') }}: {{ Form::text('reserve[datetimepickerMonthly]', Input::old('reserve')['datetimepickerMonthly'], array('class' => 'datepicker form-control', 'id' => 'datetimepickerMonthly')) }}
                        </div>
                    </div>
                    <div class="row repeat-data" id="yearly">
                        <div>
                            {{ Lang::get('calendar.repeat.every') }}:
                            <select name="reserve[repeat_yearly]" class="yearly_select">
                                <option @if(Input::old('reserve')['repeat_yearly'] == 1) selected="" @endif selected="" value="1">1</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 2) selected="" @endif value="2">2</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 3) selected="" @endif value="3">3</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 4) selected="" @endif value="4">4</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 5) selected="" @endif value="5">5</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 6) selected="" @endif value="6">6</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 7) selected="" @endif value="7">7</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 8) selected="" @endif value="8">8</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 9) selected="" @endif value="9">9</option>
                                <option @if(Input::old('reserve')['repeat_yearly'] == 10) selected="" @endif value="10">10</option>
                            </select>
                            {{ Lang::get('calendar.reserve_year') }}
                        </div>
                        <div class="last_repeat">
                            {{ Lang::get('calendar.reserve_last_repeat') }}: {{ Form::text('reserve[datetimepickerYearly]', Input::old('reserve')['datetimepickerYearly'], array('class' => 'datepicker form-control', 'id' => 'datetimepickerYearly')) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="AddRepeat" class="btn btn-submit">{{Lang::get('button.add')}}</button>
                    <button type="button" id="CloseRepeatPopup" class="btn btn-submit">{{Lang::get('button.close')}}</button>
                </div>
            </div>
        </div>
    </div>
{{ Form::close() }}

    <script>
        var repeat_dictionary = {
            daily: [
                '{{ Lang::get("calendar.js_repeat_every_day") }}',
                '{{ Lang::get("calendar.js_repeat_every_", array('day' => 2)) }}',
                '{{ Lang::get("calendar.js_repeat_every_", array('day' => 3)) }}',
                '{{ Lang::get("calendar.js_repeat_every_", array('day' => 4)) }}',
                '{{ Lang::get("calendar.js_repeat_every_", array('day' => 5)) }}',
                '{{ Lang::get("calendar.js_repeat_every_", array('day' => 6)) }}',
                '{{ Lang::get("calendar.js_repeat_every_", array('day' => 7)) }}'
            ],
            weekly: [
                '{{ Lang::get("calendar.js_repeat_week_day_1") }}',
                '{{ Lang::get("calendar.js_repeat_week_day_2") }}',
                '{{ Lang::get("calendar.js_repeat_week_day_3") }}',
                '{{ Lang::get("calendar.js_repeat_week_day_4") }}',
                '{{ Lang::get("calendar.js_repeat_week_day_5") }}',
                '{{ Lang::get("calendar.js_repeat_week_day_6") }}',
                '{{ Lang::get("calendar.js_repeat_week_day_7") }}'
            ],
            monthly: [
                '{{ Lang::get("calendar.js_repeat_every_month") }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 2)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 3)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 4)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 5)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 6)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 7)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 8)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 9)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 10)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 11)) }}',
                '{{ Lang::get("calendar.js_repeat_every_month_", array('month' => 12)) }}'
            ],
            yearly: [
                '{{ Lang::get("calendar.js_repeat_every_year") }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 2)) }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 3)) }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 4)) }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 5)) }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 6)) }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 7)) }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 8)) }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 9)) }}',
                '{{ Lang::get("calendar.js_repeat_every_year_", array('month' => 10)) }}'
            ],
            before: '{{ Lang::get("calendar.js_repeat_before") }}',
            week: '{{ Lang::get("calendar.js_repeat_every_week") }}'
        };
    </script>

@stop