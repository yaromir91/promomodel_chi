@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('calendar.title') }} ::
    @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('calendar.title')}}</h1>
    </div>

        <div class="controls-row clearfix">
            <div class="calendar-submit-button">
                <a href="{{ URL::to('calendar/add-reserve') }}" class="btn btn-submit">
                    {{ Lang::get('button.add_reserve') }}
                </a>
            </div>
            <div class="pull-right">
                @if($role == \Acme\Models\Role::APPLICANT_USER || $role == \Acme\Models\Role::APPLICANT_MANAGER_USER)
                    <div class="calendar-color-block"><span style="background-color: {{ \Config::get('app.calendar.accepted_event_color') }}" class="events_accepted_color"></span>&nbsp;-&nbsp;{{Lang::get('calendar.accepted_color')}}</div>
                    <div class="calendar-color-block"><span style="background-color: {{ \Config::get('app.calendar.public_event_color') }}" class="events_public_color"></span>&nbsp;-&nbsp;{{Lang::get('calendar.public_color')}}</div>
                @elseif($role == \Acme\Models\Role::ORGANIZER_USER)
                    <div class="calendar-color-block"><span style="background-color: {{ \Config::get('app.calendar.event_color') }}" class="events_color"></span>&nbsp;-&nbsp;{{Lang::get('calendar.events')}}</div>
                @endif
                <div class="calendar-color-block"><span style="background-color: {{ \Config::get('app.calendar.reserve_color') }}" class="reserves_color"></span>&nbsp;-&nbsp;{{Lang::get('calendar.reserves')}}</div>
            </div>
        </div>    
   
    <div>
        <input type="hidden" name="_token" id="Token" value="{{ csrf_token() }}" />
        <div id='calendar'></div>
    </div>

@stop
