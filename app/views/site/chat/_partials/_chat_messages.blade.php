@if(count($messages))
    @foreach($messages as $msg)
        <li>
            <div data-msg-id="{{$msg->id}}" @if($userId != $msg->user_sender_id) data-received-user="{{$userId}}" @endif class="alert @if($userId == $msg->user_sender_id)alert-success @elseif($msg->is_m_read == Acme\Models\Chat\UserChatMessages::READ_OFF) not-read-message @else alert-info @endif">
                @if($userId == $msg->user_sender_id)
                    <div class="del-user-msg"><a href="{{ URL::route('chat_del_msg', $msg->id) }}" data-msg="{{ Lang::get('chat.delete_msg') }}">{{ Lang::get('chat.delete') }}</a></div>
                @endif
                <img src="{{$msg->userSender->getUserAvatar()}}" style="width:50px;">
                @if($userId == $msg->user_sender_id)
                    {{ Lang::get('chat.me') }}
                @else
                    <a href="{{ URL::route('view_user_profile', $msg->user_sender_id) }}" target="_blank">
                        {{$receiveruser->getUserName()}}
                    </a>
                @endif: {{$msg->body}}
                <div class="msg-time">{{$msg->created_at}}</div>
            </div>
        </li>
    @endforeach
@endif