@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('chat.user_chats') }} ::
    @parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('chat.user_chats') }}</h1>
    </div>

    <div class="content">
        @if(count($chats))
            <ul>
                @foreach($chats as $cht)
                    <li>
                        <a href="{{URL::route('users_chat', $cht->code)}}">
                            {{$cht->last_name_u1}} {{$cht->first_name_u1}} -
                            {{$cht->last_name_u2}} {{$cht->first_name_u2}}

                            @if($cht->uread_count > 0)
                                <span class="red">({{ Lang::get('chat.unread') }} {{$cht->uread_count}})</span>
                            @endif
                        </a>
                    </li>
                @endforeach
            </ul>
        @else
            {{ Lang::get('chat.empty_list') }}
        @endif
    </div>
@stop
