@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('chat.user_chat') }} ::
    @parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('chat.user_chat') }}</h1>
    </div>

    <div class="content">
        <div class="messenger bg-white">
            <div class="chat-header text-white bg-gray-dark">
                {{$chatTitle['name1']}} <i class="fa fa-long-arrow-right"></i> {{$chatTitle['name2']}}
            </div>
            <div class="messenger-body open">
                <div class="chat-footer">
                    <div class="p-lr-10">
                        <textarea  rows="5" id="chat-message" class="input-light input-large brad chat-search" placeholder="{{ Lang::get('chat.your_message') }}..."></textarea>
                    </div>
                </div>

                <div>
                    <button class="btn btn-submit chat-send-message">
                        {{ Lang::get('chat.send') }}
                    </button>
                </div>

                <div class="chat-message-block-lk">
                    <ul class="chat-messages" id="chat-log">
                        @include('site.chat._partials._chat_messages')
                    </ul>
                    <div class="show-else-block  @if($countMsg<=count($messages)) hidden-block @endif">
                        <a class="btn btn-default show-else-messages">
                            {{ Lang::get('chat.show_else') }}
                        </a>
                    </div>
                </div>

            </div>


            <div id="_token" token="{{ csrf_token() }}"></div>
            <input type="hidden" class="current-user" data-msg-me="{{ Lang::get('chat.me_msg') }}" data-msg-delete="{{ Lang::get('chat.delete') }}" name="currentUser" value="{{$userId}}" />
            <input type="hidden" class="count2" name="count2" value="{{$countMsg}}" />
            <input type="hidden" name="delete_title" value="{{ Lang::get('chat.delete_msg') }}" />
            <input type="hidden" class="receiver-user" name="receiverUser" value="{{$chat->getReceiverUserId($userId)}}" />
            <input type="hidden" class="current-chat" name="currentChat" value="{{$chat->code}}" />
            <input type="hidden" class="current-user-avatar" name="currentUserAvatar" value="{{$currentuser->getUserAvatar()}}" />
            <input type="hidden" class="receiver-user-avatar" name="receiverUserAvatar" value="{{$receiveruser->getUserAvatar()}}" />
            <input type="hidden" class="receiver-user-name" name="receiverUserName" value="{{$receiveruser->getUserName()}}" />
        </div>
    </div>
@stop
