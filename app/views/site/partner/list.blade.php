@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('partner.title') }} ::
    @parent
@stop

@section('meta_description'){{ Lang::get('partner.menu_partner') }}@stop


{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('partner.menu_partner') }}</h1>
    </div>

    <!-- Tabs Content -->
    <div class="report-content">
        @if($partners)
            <div class="row">
                @foreach ($partners as $partner)
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="photo-album">
                            <a href="{{ URL::to('partner/' . $partner->id . '/show') }}">
                                @if($partner->logo)
                                <img class="content-image" src="{{ asset(\Config::get('app.image_dir.partners_small') . $partner->logo) }}" alt="">
                                @else
                                    {{\Config::get('app.album_no_image_260_180_with_class')}}
                                @endif
                            </a>
                        </div>
                        <div class="photo-info">
                            <div class="line-info">
                                <span>{{ Lang::get('partner.name') }}:&nbsp;</span><a href="{{ URL::to('partner/' . $partner->id . '/show') }}">{{ String::title($partner->title) }}</a>
                            </div>
                            <div class="line-info">
                                <span>{{ Lang::get('partner.date') }}:&nbsp;</span>{{ $partner->date() }}
                            </div>
                        </div>
                        <!-- Post Footer -->
                        <div id="_token" token="{{ csrf_token() }}"></div>
                        <!-- ./ post footer -->
                    </div>
                @endforeach
            </div>
        @endif
        {{ $partners->links() }}
    </div>
    <!-- ./ tabs content -->
@stop
