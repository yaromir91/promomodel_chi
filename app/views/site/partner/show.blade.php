@extends('site.layouts.default')

@section('title')
    {{ $title }} :: @parent
@stop

@section('meta_description'){{ Lang::get('partner.title') }} - {{ $partner->title }}@stop

{{-- Content --}}
@section('content')
    <!-- Tabs Content -->
    <div class="page-header clearfix">
        <h1>{{ $partner->title }}</h1>
    </div>
    <div class="pull-right">
        <a href="{{ URL::to('partner') }}"
           class="btn btn-submit">{{ Lang::get('button.back') }}</a>
    </div>

    <div class="content group-content">
        <!-- ./ Panel for edit group -->
        <!-- Members -->
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="group-photo">
                    @if($partner->logo)
                        <img alt="Generic placeholder image" src="{{ asset(\Config::get('app.image_dir.partners_preview') . $partner->logo) }}">
                    @else
                        <img src="{{ asset(\Config::get('app.image_dir.stub')) }}">
                    @endif
                </div>
                <div class="group-description">
                    <h3>{{ Lang::get('partner.description') }}</h3>
                    <p>{{ $partner->description }}</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h3 class="no-border">{{ Lang::get('partner.details') }}</h3>
                <div class="row">
                    <div class="col-lg-4">
                        <p>{{ Lang::get('partner.url') }}:</p>
                    </div>
                    <div class="col-lg-8">
                        <p><b><a target="_blank" href="{{ $partner->url }}">{{ $partner->url }}</a></b></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <p>{{ Lang::get('partner.email') }}:</p>
                    </div>
                    <div class="col-lg-8">
                        <p><b>{{ $partner->email }}</b></p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-4">
                        <p>{{ Lang::get('partner.phone') }}:</p>
                    </div>
                    <div class="col-lg-8">
                        <p><b>{{ $partner->phone }}</b></p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-4">
                        <p>{{ Lang::get('partner.adress') }}:</p>
                    </div>
                    <div class="col-lg-8">
                        <p><b>{{ $partner->adress }}</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop