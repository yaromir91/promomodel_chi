<form method="POST" class="form-horizontal" action="{{ URL::to('user') }}" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ Session::getToken() }}">
    {{--<fieldset>--}}
        <div class="content">
            <div class="form-heading clearfix">
                <h3>{{ Lang::get('site.sign_up') }}</h3>
            </div>
            <div class="form-group @if(isset(Session::get('err')['account_type'])) has-error @endif">
                <div class="col-lg-3">
                    <label class="control-label" for="account_type">{{ Lang::get('profile.account_type') }}</label>
                </div>
                <div class="col-lg-6">
                    {{ Form::select('account_type', Acme\Models\Repositories\RoleRepository::getAvailableRolesForRegistration(), Input::old('account_type'), array('class' => 'form-control', 'id' => 'account_type'))}}
                    @if(isset(Session::get('err')['account_type']))
                        <span class="help-block">{{ Session::get('err')['account_type'][0] }}</span>
                    @endif
                </div>
                <div class="col-lg-3">
                    <span class="form-info">{{ Lang::get('user/user.message_01') }}</span>
                </div>
            </div>
            <div class="form-group @if(isset(Session::get('err')['username'])) has-error @endif">
                <div class="col-lg-3">
                    <label class="control-label" for="username">{{ Lang::get('user/user.zizaco_user_name') }}</label>
                </div>
                <div class="col-lg-6">
                    <input class="form-control" placeholder="{{ Lang::get('user/user.zizaco_user_name') }}" type="text" name="username" id="username" value="{{ Input::old('username') }}">
                    @if(isset(Session::get('err')['username']))
                        <span class="help-block">{{ Session::get('err')['username'][0] }}</span>
                    @endif
                </div>
                <div class="col-lg-3">
                    <span class="form-info">{{ Lang::get('user/user.message_02') }}</span>
                </div>
            </div>
            <div class="form-group @if(isset(Session::get('err')['lastName'])) has-error @endif">
                <div class="col-lg-3">
                    <label class="control-label" for="last_name">{{ Lang::get('profile.LastName') }}</label>
                </div>
                <div class="col-lg-6">
                    <input class="form-control" placeholder="{{ Lang::get('profile.LastName') }}" type="text" name="last_name" id="last_name" value="{{ Input::old('last_name') }}">
                    @if(isset(Session::get('err')['lastName']))
                        <span class="help-block">{{ Session::get('err')['lastName'][0] }}</span>
                    @endif
                </div>
                <div class="col-lg-3">
                    <span class="form-info"></span>
                </div>
            </div>
            <div class="form-group @if(isset(Session::get('err')['firstName'])) has-error @endif">
                <div class="col-lg-3">
                    <label class="control-label" for="first_name">{{ Lang::get('profile.FirstName') }}</label>
                </div>
                <div class="col-lg-6">
                    <input class="form-control" placeholder="{{ Lang::get('profile.FirstName') }}" type="text" name="first_name" id="first_name" value="{{ Input::old('first_name') }}">
                    @if(isset(Session::get('err')['firstName']))
                        <span class="help-block">{{ Session::get('err')['firstName'][0] }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group @if(isset(Session::get('err')['gender'])) has-error @endif">
                <div class="col-lg-3">
                    <label class="control-label" for="gender">{{ Lang::get('profile.Gender') }}</label>
                </div>
                <div class="col-lg-6">
                    {{ Form::select('gender', array('m' => Lang::get('profile.Male'), 'f' => Lang::get('profile.Female')), Input::old('gender'), array('class' => 'form-control', 'id' => 'gender'))}}
                    @if(isset(Session::get('err')['gender']))
                        <span class="help-block">{{ Session::get('err')['gender'][0] }}</span>
                    @endif
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="form-group @if(isset(Session::get('err')['email'])) has-error @endif">
                <div class="col-lg-3">
                    <label class="control-label" for="email">{{ Lang::get('confide::confide.e_mail') }} </label>
                </div>
                <div class="col-lg-6">
                    <input class="form-control" placeholder="{{ Lang::get('confide::confide.e_mail') }}" type="text" name="email" id="email" value="{{ Input::old('email') }}">
                    @if(isset(Session::get('err')['email']))
                        <span class="help-block">{{ Session::get('err')['email'][0] }}</span>
                    @endif
                </div>
                <div class="col-lg-3">
                    <span class="form-info">{{ Lang::get('confide::confide.signup.confirmation_required') }}</span>
                </div>
            </div>
            <div class="form-group @if(isset(Session::get('err')['password'])) has-error @endif">
                <div class="col-lg-3">
                    <label class="control-label" for="password">{{ Lang::get('confide::confide.password') }}</label>
                </div>
                <div class="col-lg-6">
                    <input class="form-control" placeholder="{{ Lang::get('confide::confide.password') }}" type="password" name="password" id="password">
                    @if(isset(Session::get('err')['password']))
                        <span class="help-block">{{ Session::get('err')['password'][0] }}</span>
                    @endif
                </div>
                <div class="col-lg-3">
                    <span class="form-info">{{ Lang::get('user/user.message_03') }}</span>
                </div>
            </div>
            <div class="form-group @if(isset(Session::get('err')['password_confirmation'])) has-error @endif">
                <div class="col-lg-3">
                    <label class="control-label" for="password_confirmation">{{ Lang::get('confide::confide.password_confirmation') }}</label>
                </div>
                <div class="col-lg-6">
                    <input class="form-control" placeholder="{{ Lang::get('confide::confide.password_confirmation') }}" type="password" name="password_confirmation" id="password_confirmation">
                    @if(isset(Session::get('err')['password_confirmation']))
                        <span class="help-block">{{ Session::get('err')['password_confirmation'][0] }}</span>
                    @endif
                </div>
                <div class="col-lg-3"></div>
            </div>
            {{--@if (Session::get('error'))--}}
            {{--<div class="alert alert-error alert-danger">--}}
            {{--@if (is_array(Session::get('error')))--}}
            {{--{{ head(Session::get('error')) }}--}}
            {{--@endif--}}
            {{--</div>--}}
            {{--@endif--}}

            {{--@if (Session::get('notice'))--}}
            {{--<div class="alert">{{ Session::get('notice') }}</div>--}}
            {{--@endif--}}
            <div class="form-actions form-group">
                <div class="col-lg-4">
                    <button type="submit" class="btn btn-submit">{{ Lang::get('confide::confide.signup.submit') }}</button>
                </div>
            </div>

        </div>

    {{--</fieldset>--}}
</form>
