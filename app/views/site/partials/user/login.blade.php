<form class="form-horizontal" method="POST" action="{{ URL::to('user/login') }}" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-heading clearfix">
        <h3>{{ Lang::get('user/user.login_sigin_title') }}</h3>
    </div>
    <fieldset>
        <div class="form-group">
            <div class="col-lg-3">
                <label class="control-label" for="email">{{ Lang::get('user/user.username_e_mail') }}</label>
            </div>
            <div class="col-lg-6">
                <input class="form-control" tabindex="1" placeholder="{{ Lang::get('user/user.username_e_mail') }}" type="text" name="email" id="email" value="{{ Input::old('email') }}">
            </div>
            <div class="col-lg-3"></div>
        </div>
        <div class="form-group">
            <div class="col-lg-3">
                <label class="control-label" for="password">
                    {{ Lang::get('confide::confide.password') }}
                </label>
            </div>
            <div class="col-lg-6">
                <input class="form-control" tabindex="2" placeholder="{{ Lang::get('confide::confide.password') }}" type="password" name="password" id="password">
            </div>
            <div class="col-lg-3">

            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-3">
                <div class="submit-block">
                    <button tabindex="3" type="submit" class="btn btn-submit">{{ Lang::get('confide::confide.login.submit') }}</button>
                    <div class="option-block">
                        <input type="hidden" name="remember" value="0">
                        <input tabindex="4" type="checkbox" name="remember" id="remember" value="1">
                        <label for="remember">{{ Lang::get('confide::confide.login.remember') }}</label>
                    </div>
                    <p><a href="forgot">{{ Lang::get('confide::confide.login.forgot_password') }}</a></p>
                </div>
            </div>
            <div class="col-lg-6">
            </div>
        </div>

        @if ( Session::get('error') )
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif

        @if ( Session::get('notice') )
            <div class="alert">{{ Session::get('notice') }}</div>
        @endif

        <div class="form-group">
            <div class="col-md-10">

            </div>
        </div>
    </fieldset>
</form>
