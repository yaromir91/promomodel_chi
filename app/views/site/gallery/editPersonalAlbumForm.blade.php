@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('gallery.editTitle') . ' - ' . $data['gallery']->title }} ::
    @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('gallery.editTitle') . ' - ' . $data['gallery']->title}}</h1>
    </div>
    <!-- Tabs -->
        @include('site.gallery.nav_tabs')
    <!-- ./ tabs -->
    <div class="content form-content">
        {{ Form::open(array('class' => 'form-horizontal avatar-form', 'autocomplete' => 'off', 'files' => 'true', 'url' => URL::to('gallery/edit-album/' . $data['gallery']->id))) }}

        <!-- Title -->
        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
            <div class="col-md-3 col-lg-3 col-sm-4">
                {{ Form::label('title', Lang::get('gallery.field_title'), array('class' => 'control-label')) }}
            </div>
            <div class="col-lg-6 col-md-6 col-sm-8">
                {{ Form::text('title', $data['gallery']->title, array('class' => 'form-control', 'id' => 'title', 'maxlength' => 70)) }}
                {{ $errors->first('title') }}
            </div>
        </div>
        <!-- ./ title -->

        <!-- Hash tag -->
        {{--<div class="form-group {{ $errors->has('tag') ? 'has-error' : '' }}">--}}
            {{--<div class="col-md-3 col-lg- col-sm-4">--}}
                {{--{{ Form::label('tag', Lang::get('gallery.field_tag'), array('class' => 'control-label')) }}--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 col-md-6 col-sm-8">--}}
                {{--<ul id="singleFieldTags">--}}
                    {{--@foreach($data['currentTags'] as $ctag)--}}
                    {{--<li>{{ $ctag->name }}</li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
                {{--<input type="hidden" name="tag" id="mySingleField" value="{{ $data['currentTags'] }}">--}}
                {{--{{ Form::text('tag', '', array('class' => 'form-control', 'id' => 'tag')) }}--}}
                {{--{{ $errors->first('tag') }}--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- ./ hash tag -->

        <!-- Preview -->
        <div class="form-group {{ $errors->has('preview') ? 'has-error' : '' }}">
            <div class="col-lg-3 col-md-3 col-sm-4">
                {{ Form::label('preview', Lang::get('gallery.Preview'), array('class' => 'control-label')) }}
            </div>
            <div class="col-lg-6 col-md-6 col-sm-8">

                @include('site.profile.applicant.user_avatar', [
                'fileFieldName'   => 'preview',
                'actionUrlUpload' => '/gallery/upload-preview',
                'origImage'       => $data['gallery']->getTemporarySmallFile() ,
                'origImageName'   => $data['gallery']->getTemporarySmallFileName()
                ])
                {{ $errors->first('preview') }}
            </div>
        </div>
        <!-- ./ Preview -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="col-lg-2 col-sm-3">
                <button type="button" class="btn btn-submit" onclick="window.location = '{{ URL::to('gallery/personal/album/show/' . $data['gallery']->id) }}'">{{Lang::get('button.cancel')}}</button>
                {{--<button type="reset" class="btn btn-submit">{{Lang::get('button.reset')}}</button>--}}

            </div>
            <div class="col-lg-2 col-sm-3">
                <button type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
            </div>
        </div>
        <!-- ./ form actions -->
        {{ Form::close() }}
    </div>

@stop
