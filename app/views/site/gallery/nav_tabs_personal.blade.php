<div class="tab-navigation clearfix">
    <ul class="nav nav-tabs">

        <li class="active">
            <a href="{{ URL::route('user_albums', $data['user']->id) }}">{{ Lang::get('gallery.user_albums') }}</a>
        </li>

        <li>
            <a href="{{ URL::to('gallery') }}">{{ Lang::get('gallery.tab_all') }}</a>
        </li>

        @if($data['user_auth'])
            <li {{ (Request::is('gallery/personal/album*') ? ' class="active"' : '') }}>
                <a href="{{ URL::to('gallery/personal/album') }}">{{ Lang::get('gallery.my_albums') }}</a>
            </li>
            <li {{ (Request::is('gallery/personal/photo*') ? ' class="active"' : '') }}>
                <a href="{{ URL::to('gallery/personal/photo') }}">{{ Lang::get('gallery.my_photos') }}</a>
            </li>
        @elseif(Auth::check())
            <li {{ (Request::is('gallery/personal/album*') ? ' class="active"' : '') }}>
                <a href="{{ URL::to('gallery/personal/album') }}">{{ Lang::get('gallery.my_albums') }}</a>
            </li>
            <li {{ (Request::is('gallery/personal/photo*') ? ' class="active"' : '') }}>
                <a href="{{ URL::to('gallery/personal/photo') }}">{{ Lang::get('gallery.my_photos') }}</a>
            </li>
        @endif
    </ul>
</div>