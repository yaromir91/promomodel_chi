@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('gallery.add_image_title') }} ::
    @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('gallery.add_image_title')}}</h1>
    </div>
    <!-- Tabs -->
    @include('site.gallery.nav_tabs')
    <!-- ./ tabs -->
    <div class="content form-content">
        {{ Form::open(array('class' => 'form-horizontal', 'autocomplete' => 'off', 'files'=> true, 'url' => URL::to('gallery/add-photo/'))) }}

        {{ Form::hidden('gallery', ($data['gallery'])?$data['gallery']->id:'') }}
        <!-- Select -->
        <div class="form-group {{ $errors->has('album') ? 'has-error' : '' }}">
            <div class="col-md-3 col-lg-3 col-sm-4">
                {{ Form::label('album', Lang::get('gallery.field_select_album_title'), array('class' => 'control-label')) }}
            </div>
            <div class="col-md-6 col-lg-6 col-sm-8">
                {{ Form::select('album', $data['galeries'], ($data['gallery'])?$data['gallery']->id:'', array('class' => 'form-control', 'id' => 'album_choose')) }}
                {{ $errors->first('album') }}
            </div>
        </div>
        <!-- Select -->

        <!-- ALBUM -->
        <!-- Title -->
        <div class="album_container" @if($data['gallery']) style="display: none" @endif>
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <div class="col-lg-3 col-md-3 col-sm-4">
                    {{ Form::label('title', Lang::get('gallery.field_title'), array('class' => 'control-label')) }}
                </div>
                <div class="col-lg-6 col-md-6 col-sm-8">
                    {{ Form::text('title', '', array('class' => 'form-control', 'id' => 'title', 'maxlength' => 70)) }}
                    {{ $errors->first('title') }}
                </div>
            </div>
            <!-- ./ title -->

            <!-- Hash tag -->
<!--            <div class="form-group {{ $errors->has('tag') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3"></div>
                {{ Form::label('tag', Lang::get('gallery.field_tag'), array('class' => 'control-label')) }}
                <div class="col-lg-6 col-md-6">
                    <ul id="singleFieldTags"></ul>
                    <input type="hidden" name="tag" id="mySingleField" value="">
                    {{--                    {{ Form::text('tag', '', array('class' => 'form-control', 'id' => 'title')) }}--}}
                    {{ $errors->first('tag') }}
                </div>
            </div>-->
            <!-- ./ hash tag -->

            <!-- ./ ALBUM -->
        </div>

        <!-- File -->
        <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
            <div class="col-lg-3 col-md-3 col-sm-4">
                {{ Form::label('file', Lang::get('gallery.field_file_title'), array('class' => 'control-label')) }}
            </div>
            <div class="col-lg-6 col-md-6 col-sm-8" id="imageUploadBlock">
                <div id="fileuploader" token="{{ csrf_token() }}">{{ Lang::get('button.upload') }}</div>
            </div>
            <div class="HiddenImgInputs"></div>
        </div>
        <!-- ./ file -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="col-lg-2 col-md-2 col-sm-3">
                <button type="button" id="CancelSaveImage" class="btn btn-submit" data-url="{{ URL::to($data['back_link']) }}">{{Lang::get('button.cancel')}}</button>
                {{--<button type="reset" class="btn btn-submit">{{Lang::get('button.reset')}}</button>--}}
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3">
                <button id="SaveImage" type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
            </div>
        </div>
        <!-- ./ form actions -->
        {{ Form::close() }}
    </div>

@stop