@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('gallery.title') }}
    @parent
@stop

@section('meta_description'){{ Lang::get('gallery.title') }} - {{ $data['gallery']->title }}@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('gallery.title')}}</h1>
    </div>
        <!-- Tabs -->
            @include('site.gallery.nav_tabs')
        <!-- ./ tabs -->
    <div class="pull-right">
        @if($data['user_auth'])
            <a href="{{ URL::to('gallery/personal/album') }}" class="btn btn-submit">
                {{ Lang::get('button.back') }}
            </a>
            <a href="{{ URL::to('gallery/personal/album/edit/' . $data['gallery']->id) }}" class="btn btn-submit">
                {{ Lang::get('button.edit') }}
            </a>
            <a href="#" data-href="{{ URL::to('gallery/personal/album/delete/' . $data['gallery']->id) }}" data-toggle="modal" data-target="#confirm-delete" class="btn btn-submit">
                {{ Lang::get('button.delete') }}
            </a>
            <a href="{{ URL::to('gallery/personal/photo/add/' . $data['gallery']->id) }}" class="btn btn-submit">
                {{ Lang::get('gallery.add_image_button') }}
            </a>
            <a href="#" id="RemoveImages" data-href="#" data-toggle="modal" data-target="#confirm-img-delete" class="btn btn-submit">
                {{ Lang::get('gallery.remove_photos') }}
            </a>
        @endif
    </div>

    <div class="content-header">
    </div>

    <div class="content content-gallery">
        @if($data['images'])
        <div class="row">
            @foreach($data['images'] as $image)
            <div class="col-lg-3 col-md-3 col-sm-4 gallery-item">
                <div class="photo-album customImageList">
                    <a class="fancy_images" rel="gallery" href="{{ asset(\Config::get('app.image_dir.album') . $image->user_id . '/' . $image->file) }}">
                        <img class="content-image" src="{{ asset(\Config::get('app.image_dir.album') . $image->user_id . '/small/' . $image->file) }}" alt=""/>
                    </a>
                    <div class="checkboxCustom checkboxCustom-head hide">

                        <div class="makeDelete">
                            <input id="removeCheckbox" class="removeImageCheckbox" type="checkbox" value="{{ $image->id }}">
                            <label for="removeCheckbox" class="checkbox-label">{{ Lang::get('button.delete') }}</label>
                        </div>
                    </div>
                </div>
                <div class="photo-info">
                    <span>{{ Lang::get('gallery.field_title') }}:&nbsp;</span>{{ $data['gallery']->title }}
                </div>
            </div>
            @endforeach
            <div id="_token" token="{{ csrf_token() }}"></div>
        </div>
        @endif
    </div>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ Lang::get('gallery.confirm_delete_msg') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a class="btn btn-default btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-img-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ Lang::get('gallery.confirm_delete_msg') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a id="DeleteImages" class="btn btn-default btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>

@stop