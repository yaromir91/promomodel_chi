@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('gallery.title') }} ::
    @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('gallery.title')}}</h1>
    </div>
    <!-- Tabs -->
        @include('site.gallery.nav_tabs')
    <!-- ./ tabs -->
    <div class="button-photo-group clearfix">
        <div class="pull-right">
            <div class="btn-toolbar">
                <div class="btn-group">
                    <a href="#" id="RemoveImages" data-href="#" data-toggle="modal" data-target="#confirm-delete" class="btn btn-submit">
                        {{ Lang::get('gallery.remove_photos') }}
                    </a>
                </div>
                <div class="btn-group">
                    <a href="{{ URL::to('gallery/personal/photo/add/') }}" class="btn btn-submit">
                        {{ Lang::get('button.add_photo') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Tabs Content -->
    <div class="content">
        <div class="row">
            @foreach($data['photos'] as $image)
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <div class="gallery-item">
                        <div class="photo-album customImageList">
                            <a class="fancy_images" rel="gallery" href="{{ asset(\Config::get('app.image_dir.album') . $data['user']->id . '/' . $image->file) }}">
                                <img class="content-image" src="{{ asset(\Config::get('app.image_dir.album') . $data['user']->id . '/small/' . $image->file) }}" alt=""/>
                            </a>
                            <div class="checkboxCustom hide">
                                <div class="makeDelete">
                                    <input id="removeCheckbox" class="removeImageCheckbox" type="checkbox" value="{{ $image->id }}">
                                    <label for="removeCheckbox" class="checkbox-label">{{ Lang::get('button.delete') }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="photo-info">
                            <input class="g_rating" data-readonly="true" value="@if(isset($data['ratings'][$image->id])){{ $data['ratings'][$image->id] }}@endif" data-id="{{ $image->id }}" >
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div id="_token" token="{{ csrf_token() }}"></div>

    </div>
    <!-- ./ tabs content -->
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ Lang::get('gallery.confirm_delete_msg') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a id="DeleteImages" class="btn btn-default btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>


@stop