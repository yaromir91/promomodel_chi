@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('gallery.title') }} ::
    @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('gallery.title')}}</h1>
    </div>
    <!-- Tabs -->
            @include('site.gallery.nav_tabs')
    <!-- ./ tabs -->
<div class="button-photo-group clearfix">
    <div class="pull-right">
        <a href="{{ URL::to('gallery/personal/album/add') }}" class="btn btn-submit">
            {{ Lang::get('button.create_album') }}
        </a>
    </div>
</div>
    <!-- Tabs Content -->
    <div class="content">
        <div class="row">
            @foreach($data['galeries'] as $k=>$elem)
            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="gallery-item">
                    <div class="photo-album">
                        @if ($elem->preview)
                        <img class="content-image" src="{{ asset(\Config::get('app.image_dir.album') . $elem->user_id . '/' . $elem->preview) }}">
                        @else
                        {{\Config::get('app.album_no_image')}}
                        @endif
                    </div>
                    <div class="photo-info">
                        <div class="line-info"><span>{{Lang::get('gallery.album')}}:&nbsp;</span><a href="{{ URL::to('gallery/personal/album/show/' . $elem->id) }}">{{ $elem->title }}</a></div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- ./ tabs content -->


@stop