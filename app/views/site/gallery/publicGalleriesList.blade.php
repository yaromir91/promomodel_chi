@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('gallery.title') }} ::
    @parent
@stop

@section('meta_description'){{ Lang::get('gallery.title') }}@stop


{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('gallery.title')}}</h1>
    </div>
    <!-- Tabs -->
        @include('site.gallery.nav_tabs')
    <!-- ./ tabs -->

    <!-- Tabs Content -->
    <div class="content">
        <div class="content-header">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
                    <p>{{Lang::get('gallery.show')}}:</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                    <a class="btn btn-submit" href="{{ URL::to('gallery/filter/org') }}">{{Lang::get('gallery.filter_organizer')}}</a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                    <a class="btn btn-submit" href="{{ URL::to('gallery/filter/app') }}">{{Lang::get('gallery.filter_applicant')}}</a></div>
            </div>
        </div>
        <div class="row">
            @foreach($data['galeries'] as $k=>$elem)
            <div class="col-lg-3 col-sm-4">
                <div class="gallery-item">
                    <div class="photo-album">
                        @if ($elem->preview)
                        <img class="content-image" src="{{ asset(\Config::get('app.image_dir.album') . $elem->user_id . '/' . $elem->preview) }}">
                        @else
                            {{\Config::get('app.album_no_image')}}
                        @endif
                    </div>
                    <div class="photo-info">
                        <div class="line-info"><span>{{Lang::get('gallery.album')}}:&nbsp;</span><a href="{{ URL::to('gallery/show/' . $elem->id) }}">{{ $elem->title }}</a></div>
                        <div class="line-info"><span>{{Lang::get('gallery.user')}}:&nbsp;</span><a href="{{ URL::route('view_user_profile', $elem->user_id) }}">{{ $elem->first_name . ' ' . $elem->last_name }}</a></div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- ./ tabs content -->
@stop



