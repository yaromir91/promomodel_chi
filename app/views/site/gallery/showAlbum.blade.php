@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('gallery.title') }}
    @parent
@stop


@section('meta_description'){{ Lang::get('gallery.title') }} - {{ $data['gallery']->title }}@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('gallery.title')}}-{{ $data['gallery']->title }}</h1>
    </div>

    <a href="{{URL::route('view_user_profile', $data['albumOwner']->id)}}"><h2>{{$data['albumOwner']->getUserName()}}</h2></a>

    <div class="pull-right">
        <a href="{{ URL::to('gallery') }}" class="btn btn-submit">
            {{ Lang::get('button.back') }}
        </a>
        @if($data['user_auth'])
        <a href="{{ URL::to('gallery/personal/album/edit/' . $data['gallery']->id) }}" class="btn btn-submit">
            {{ Lang::get('button.edit') }}
        </a>
        <a href="#" data-href="{{ URL::to('gallery/personal/album/delete/' . $data['gallery']->id) }}" data-toggle="modal" data-target="#confirm-delete" class="btn btn-submit">
            {{ Lang::get('button.delete') }}
        </a>
        <a href="{{ URL::to('gallery/personal/photo/add/' . $data['gallery']->id) }}" class="btn btn-submit">
            {{ Lang::get('gallery.add_image_button') }}
        </a>
        <a href="#" id="RemoveImages" data-href="#" data-toggle="modal" data-target="#confirm-img-delete" class="btn btn-submit">
            {{ Lang::get('gallery.remove_photos') }}
        </a>
        @endif
    </div>

    <div class="tab-navigation clearfix">
        <ul class="nav nav-tabs">

            <li>
                <a href="{{ URL::route('user_albums', $data['albumOwner']->id) }}">{{ Lang::get('gallery.user_albums') }}</a>
            </li>

            <li>
                <a href="{{ URL::to('gallery') }}">{{ Lang::get('gallery.tab_all') }}</a>
            </li>

            @if($data['user_auth'])
                <li {{ (Request::is('gallery/personal/album*') ? ' class="active"' : '') }}>
                    <a href="{{ URL::to('gallery/personal/album') }}">{{ Lang::get('gallery.my_albums') }}</a>
                </li>
                <li {{ (Request::is('gallery/personal/photo*') ? ' class="active"' : '') }}>
                    <a href="{{ URL::to('gallery/personal/photo') }}">{{ Lang::get('gallery.my_photos') }}</a>
                </li>
            @elseif(Auth::check())
                <li {{ (Request::is('gallery/personal/album*') ? ' class="active"' : '') }}>
                    <a href="{{ URL::to('gallery/personal/album') }}">{{ Lang::get('gallery.my_albums') }}</a>
                </li>
                <li {{ (Request::is('gallery/personal/photo*') ? ' class="active"' : '') }}>
                    <a href="{{ URL::to('gallery/personal/photo') }}">{{ Lang::get('gallery.my_photos') }}</a>
                </li>
            @endif
        </ul>
    </div>

    <div class="content-header">
    </div>

    <div class="content">
        @if($data['images'])
        <div class="row">
            @foreach($data['images'] as $image)
                <div class="col-lg-3 col-md-3 col-sm-4 gallery-item">
                    <div class="photo-album customImageList">
                        <a class="fancy_images" rel="gallery" href="{{ asset(\Config::get('app.image_dir.album') . $image->user_id . '/' . $image->file) }}">
                            <img class="content-image" src="{{ asset(\Config::get('app.image_dir.album') . $image->user_id . '/small/' . $image->file) }}" alt=""/>
                        </a>
                        @if(is_object(Auth::user()))
                        @if($image->user_id == Auth::user()->id)
                        <div class="checkboxCustom checkboxCustom-head hide">
                            <div class="makeDelete">
                                <input id="removeCheckbox" class="removeImageCheckbox" type="checkbox" value="{{ $image->id }}">
                                <label class="checkbox-label" for="removeCheckbox">Delete</label>
                            </div>
                        </div>
                        @endif
                        @endif
                    </div>
                    <div class="photo-info">
                        <input class="g_rating" @if(!Auth::user() || $image->user_id == Auth::user()->id) data-readonly="true" @endif value="@if(isset($data['ratings'][$image->id])){{ $data['ratings'][$image->id] }}@endif" data-id="{{ $image->id }}" >
                    </div>
                </div>
            @endforeach
        </div>
        <div id="_token" token="{{ csrf_token() }}"></div>
        @endif
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ Lang::get('gallery.confirm_delete_msg') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a class="btn btn-default btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-img-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ Lang::get('gallery.confirm_delete_msg') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a id="DeleteImages" class="btn btn-default btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>

@stop