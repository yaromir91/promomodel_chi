@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('gallery.title') }} ::
    @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{Lang::get('gallery.title')}}</h1>
    </div>
    <!-- Tabs -->
        @include('site.gallery.nav_tabs')
    <!-- ./ tabs -->
    <div class="content form-content">
        {{ Form::open(array('class' => 'form-horizontal avatar-form', 'files' => 'true', 'autocomplete' => 'off', 'url' => URL::to('gallery/create-album'))) }}

            <!-- Title -->
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <div class="col-md-3 col-lg-3 col-sm-4">
                    {{ Form::label('title', Lang::get('gallery.field_title'), array('class' => 'control-label')) }}
                </div>

                <div class="col-lg-6 col-md-6 col-sm-8">
                    {{ Form::text('title', '', array('class' => 'form-control', 'id' => 'title', 'maxlength' => 70)) }}
                    {{ $errors->first('title', '<span class="help-block">:message</span>') }}
                </div>
            </div>
            <!-- ./ title -->

        <!-- Hash tag -->
        {{--<div class="form-group {{ $errors->has('tag') ? 'has-error' : '' }}">--}}
            {{--<div class="col-md-3 col-lg-3 col-sm-4">--}}
                {{--{{ Form::label('tag', Lang::get('gallery.field_tag'), array('class' => 'control-label')) }}--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 col-md-6 col-sm-8">--}}
                {{--<ul id="singleFieldTags"></ul>--}}
                {{--<input type="hidden" name="tag" id="mySingleField" value="">--}}
                {{--{{ Form::text('tag', '', array('class' => 'form-control', 'id' => 'tag')) }}--}}
                {{--{{ $errors->first('tag', '<span class="help-block">:message</span>') }}--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- ./ hash tag -->


        <!-- Preview -->
        <div class="form-group {{ $errors->has('preview') ? 'has-error' : '' }}">
            <div class="col-md-3 col-lg-3 col-sm-4">
                {{ Form::label('preview', Lang::get('gallery.Preview'), array('class' => 'control-label')) }}
            </div>
            <div class="col-lg-6 col-md-6 col-sm-8">

                <?php

                    $tmpSmallPicFile = (Input::old('new_picture', '')) ? asset(\Config::get('app.image_dir.album') . $user->id . '/' . Input::old('new_picture', '')) : '';
                    $tmpSmallPicName = Input::old('new_picture', '');

                ?>

                @include('site.profile.applicant.user_avatar', [
                'fileFieldName'   => 'preview',
                'actionUrlUpload' => '/gallery/upload-preview',
                'origImage'       => $tmpSmallPicFile,
                'origImageName'   => $tmpSmallPicName
                ])
                <div id="_token" token="{{ csrf_token() }}"></div>
                {{ $errors->first('preview') }}
            </div>
        </div>
        <!-- ./ Preview -->

            <!-- Form Actions -->
            <div class="form-group">
                <div class="col-lg-2 col-sm-3">
                    <button type="button" class="btn btn-submit" onclick="window.location = '{{ URL::to('gallery/personal/album') }}'">{{Lang::get('button.cancel')}}</button>
                </div>
                <div class="col-lg-2 col-sm-3">
                    <button type="submit" class="btn btn-submit">{{Lang::get('button.add')}}</button>
                </div>
            </div>
            <!-- ./ form actions -->
            {{ Form::close() }}
    </div>

@stop
