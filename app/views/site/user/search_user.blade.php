@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('user/user.events') }} ::
    @parent
@stop

{{-- Search User --}}
@section('content')
    <hr class="featurette-divider">
    <div class="row search-user">
        <input type="hidden" id="_token" name="csrf_token" value="{{ csrf_token() }}" />
        @if(!empty($result) && (count($result) > 0))
            <?php 
                $group_id = false; 
                $cnt = 0;
            ?>
            @foreach ($result as $item)
                <?php
                    $cnt++;
                    if($item->groups_id && $group_id != $item->groups_id) {
                        if($group_id) {
                            echo '</div>';
                        }
                        $div = '<div class="col-xs-12 col-sm-12 col-md-12 well group-search-block">';
                        $url_groups_id = URL::to('groups/show/' . $item->groups_id);
                        $title = '<h4 class="search-group-heading" >' . Lang::get("groups.title_show_group") . ' - <a href="' . $url_groups_id . '">' . $item->title . '<a/></h4>';
                        echo $div . $title;
                        $group_id = $item->groups_id;
                    }
                    if($group_id && !$item->groups_id) {
                        $group_id = false;
                        echo '</div>';
                    }
                ?>
                <div class="col-xs-6 col-sm-6 col-md-6 search-block">
                    <div class="well">
                        <div class="row">
                            {{ Form::open(array('url' => 'events/invite/'.$event->id, 'method' => 'post', 'class' => 'send-invite')) }}
                                @if($item->avatar)
                                <div class="col-sm-6 col-md-4">
                                    <a target="_blank" href="{{URL::route('view_user_profile', $item->user_id)}}">
                                        {{ HTML::image(Config::get('app.image_dir.profile') . $item->avatar, $item->avatar, ['class' => 'img-responsive']) }}
                                    </a>
                                    <?php $theInvitation1 = null;?>
                                    @if (!$item->user->isSentEventInvited($event->id, $theInvitation1))
                                        <input class="btn btn-submit" type="submit" value="{{ Lang::get('events.Invite') }}" />
                                    @endif
                                </div>
                                @endif
                                <div class="col-sm-4 col-md-4">
                                    <a target="_blank" href="{{URL::route('view_user_profile', $item->user_id)}}">
                                        <h4> {{ $item->first_name}} {{ $item->last_name}} </h4>
                                    </a>
                                    <i class="glyphicon glyphicon-map-marker"></i><cite title="San Francisco, USA">{{ Lang::get('general.message_01') }}</cite>
                                    <p>
                                        @if($item->user->email)<i class="glyphicon glyphicon-envelope"></i> <a
                                            href="mailto:{{$item->user->email }}">E-mail</a> @endif
                                        @if($item->vk_link )<br/><a class="vk-link" href="{{ $item->vk_link }}"><img class="vk-icon" src="{{ asset('assets/img/vkontakte.png') }}" alt=""/>&nbsp;Profile VK</a> @endif
                                        @if($item->date_birth)<br /><i class="glyphicon glyphicon-gift"></i> {{ $item->date_birth }} @endif
                                    </p>
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    @include('site.events._partials._invite_searched_users')
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
                <?php
                    if($group_id && count($result) == $cnt) {
                        echo '</div>';
                    }
                ?>
            @endforeach
        @else
            <h2>{{ Lang::get('events.user_not_found') }}</h2>
        @endif
    </div>


    <div id="ajaxResponse" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('events.Close') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop