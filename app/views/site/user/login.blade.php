@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ Lang::get('user/user.login') }} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header clearfix">
	<h1>{{ Lang::get('user/user.login_title') }}</h1>
</div>
<div class="welcome-box">
    <span>{{ Lang::get('user/user.login_welcome') }}!</span>
</div>
{{ Confide::makeLoginForm()->render() }}
@stop
