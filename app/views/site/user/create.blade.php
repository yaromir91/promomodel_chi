@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{ Lang::get('user/user.register') }} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header clearfix">
	<h1>{{ Lang::get('user/user.join') }}</h1>
</div>
<div class="welcome-box">
	<span>{{ Lang::get('user/user.login_welcome') }}!</span>
</div>
{{ Confide::makeSignupForm()->render() }}
@stop