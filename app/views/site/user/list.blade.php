@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('user/user.users') }} ::
    @parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h3>{{ Lang::get('user/user.users') }}</h3>
    </div>
    <table class="table table-hover">
        <tbody>
        @foreach ($usersList as $index => $user)
            <tr>
                <th scope="row">{{ $user->username }}</th>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop