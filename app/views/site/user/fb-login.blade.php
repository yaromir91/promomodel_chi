@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('profile.choose_role') }} ::
    @parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
    @parent
    body {
    background: #fff;
    }
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('profile.choose_role_pass') }}</h1>
    </div>
    {{ Form::open(array('class' => 'form-horizontal', 'autocomplete' => 'off', 'url' => URL::to('fb/choose-role'))) }}
        <fieldset>
            <div class="form-group">
                <label for="account_type">{{ Lang::get('profile.account_type') }}</label>
                {{ Form::select('account_type', Acme\Models\Repositories\RoleRepository::getAvailableRolesForRegistration(), Input::old('account_type'), array('class' => 'form-control', 'id' => 'account_type'))}}
            </div>

            <div class="form-actions form-group">
                <button type="submit" class="btn btn-default">{{ Lang::get('general.menu_facebook_login') }}</button>
            </div>

        </fieldset>
    {{ Form::close() }}
@stop