@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>
            {{ $title }}
        </h1>
    </div>
	{{-- Create User Form --}}
    {{ Form::open(array('url' => 'report/create', 'method' => 'post', 'autocomplete' => 'off', 'files' => 'true', 'class' => 'form-horizontal avatar-form')) }}

			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <!-- events -->
                <div class="form-group {{ $errors->has('events') ? 'has-error' : '' }}">
                    <div class="col-md-3 col-sm-4">
                        {{ Form::label('events', Lang::get('report.form_events'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-6 col-sm-8">
                        {{ Form::select('events', $data['events'], ($data['event']) ? $data['event']->id : '', array('class' => 'form-control', 'id' => 'events')) }}
                        {{ $errors->first('events') }}
                    </div>
                </div>
                <!-- ./ events -->
                <!-- Title -->
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <div class="col-md-3 col-sm-4">
                        {{ Form::label('title', Lang::get('report.form_title'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-6 col-sm-8">
                        {{ Form::text('title', Input::old('title'), array('class' => 'form-control', 'id' => 'title')) }}
                        {{ $errors->first('title') }}
                    </div>
                </div>
                <!-- ./ Title -->
                <!-- Description -->
                <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                    <div class="col-md-3 col-sm-4">
                        {{ Form::label('content', Lang::get('report.form_content'), array('class' => 'control-label')) }}
                    </div>

                    <div class="col-md-6 col-sm-8">
                        {{ Form::textarea('content', Input::old('content'), array('class' => 'form-control', 'id' => 'content')) }}
                        {{ $errors->first('content') }}
                    </div>
                </div>
                <!-- ./ Description -->
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <div class="col-md-3 col-sm-4">
                        {{ Form::label('image', Lang::get('report.image'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">

                        <?php

                        $tmpSmallPicFile = (Input::old('new_picture', '')) ? asset(\Config::get('app.image_dir.posts_small') . Input::old('new_picture', '')) : '';
                        $tmpSmallPicName = Input::old('new_picture', '');

                        ?>

                        @include('site.profile.applicant.user_avatar', [
                             'fileFieldName'   => 'image',
                             'actionUrlUpload' => '/report/upload',
                             'origImage'       => $tmpSmallPicFile ,
                             'origImageName'   => $tmpSmallPicName
                         ])
                        {{ $errors->first('image') }}
                    </div>
                </div>
                <!-- Tags -->
<!--                <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                    {{ Form::label('tags', Lang::get('report.form_tags'), array('class' => 'col-lg-2 control-label')) }}
                    <div class="col-lg-5">
                        {{ Form::text('tags', Input::old('tags'), array('class' => 'form-control', 'id' => 'tags')) }}
                        {{ Lang::get('report.separate_words') }}.
                        {{ $errors->first('tags') }}
                    </div>
                </div>-->
                <!-- ./ Tags -->
                <!-- Status -->
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <div class="col-md-3 col-sm-4">
                        {{ Form::label('status', Lang::get('report.form_status'), array('class' => 'control-label')) }}
                        <div class="checkbox">
                            {{ Form::checkbox('status', Input::old('status'), true, array('id' => 'status')) }}
                            {{ $errors->first('status') }}
                        </div>
                    </div>
                </div>
                <!-- ./ Status -->
                <!-- Meta title -->
                {{--<div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">--}}
                    {{--{{ Form::label('meta_title', Lang::get('report.form_meta_title'), array('class' => 'col-lg-2 control-label')) }}--}}
                    {{--<div class="col-lg-5">--}}
                        {{--{{ Form::text('meta_title', Input::old('meta_title'), array('class' => 'form-control', 'id' => 'meta_title')) }}--}}
                        {{--{{ $errors->first('meta_title') }}--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- ./ Meta title -->
                <!-- Meta description -->
                {{--<div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">--}}
                    {{--{{ Form::label('meta_description', Lang::get('report.form_meta_description'), array('class' => 'col-lg-2 control-label')) }}--}}
                    {{--<div class="col-lg-5">--}}
                        {{--{{ Form::text('meta_description', Input::old('meta_description'), array('class' => 'form-control', 'id' => 'meta_description')) }}--}}
                        {{--{{ $errors->first('meta_description') }}--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- ./ Meta description -->
                <!-- Meta keywords -->
                {{--<div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">--}}
                    {{--{{ Form::label('meta_keywords', Lang::get('report.form_meta_keywords'), array('class' => 'col-lg-2 control-label')) }}--}}
                    {{--<div class="col-lg-5">--}}
                        {{--{{ Form::text('meta_keywords', Input::old('meta_keywords'), array('class' => 'form-control', 'id' => 'meta_keywords')) }}--}}
                        {{--{{ $errors->first('meta_keywords') }}--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- ./ Meta title -->

		<!-- Form Actions -->
        <div class="form-group">
            <div class="col-md-3 col-sm-4">
                <button type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
            </div>
        </div>
        </div>
		<!-- ./ form actions -->
    {{ Form::close() }}
@stop
