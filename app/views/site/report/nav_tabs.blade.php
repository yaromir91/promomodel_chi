<div class="tab-navigation">
    <ul class="nav nav-tabs">
        <li @if(Request::is('report') || Request::is('report/show/*')) class="active" @endif >
            <a href="{{ URL::to('report') }}">{{ Lang::get('report.tab_all') }}</a>
        </li>
        @if(Auth::check() && !Auth::user()->hasRole('admin'))
        <li {{ (Request::is('report/personal*') ? ' class="active"' : '') }}>
            <a href="{{ URL::to('report/personal') }}">{{ Lang::get('report.tab_my_reports') }}</a>
        </li>
        @endif
        <li {{ (Request::is('report/popular') ? ' class="active"' : '') }}>
            <a href="{{ URL::to('report/popular') }}">{{ Lang::get('report.tab_popular') }}</a>
        </li>
        {{--<li {{ (Request::is('report/arhive') ? ' class="active"' : '') }}>--}}
        {{--<a href="{{ URL::to('report/arhive') }}">{{ Lang::get('report.tab_arhive') }}</a>--}}
        {{--</li>--}}
        {{--<li {{ (Request::is('report/tag*') ? ' class="active"' : '') }}>--}}
        {{--<a href="{{ URL::to('report/tag') }}">{{ Lang::get('report.tab_tag') }}</a>--}}
        {{--</li>--}}
    </ul>
</div>
