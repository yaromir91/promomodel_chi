@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('report.tab_my') }}
    @if(Request::is('report/personal/filter/all')) - {{ Lang::get('report.my_all') }} @endif
    @if(Request::is('report/personal/filter/published')) - {{ Lang::get('report.filter_published') }} @endif
    @if(Request::is('report/personal/filter/archive')) - {{ Lang::get('report.filter_archive') }} @endif
    ::
    @parent
@stop

@section('meta_description'){{ Lang::get('report.tab_my') }}@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('report.menu_report') }}</h1>
    </div>
@if($my_events)
<div class="pull-right">
    <a href="{{ URL::to('report/create') }}" class="btn btn-submit iframe">{{ Lang::get('button.create') }}</a>
</div>
@endif

    <!-- Tabs -->
    @include('site.report.nav_tabs')
    <!-- ./ tabs -->

    <!-- Tabs Content -->
    <div class="content report-content">
        <div class="content-header">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3">
                    <a class="btn btn-submit" @if(Request::is('report/personal') || Request::is('report/personal/filter/all')) class="active" @endif href="{{ URL::to('report/personal/filter/all') }}">{{Lang::get('report.filter_all')}}</a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3">
                    <a class="btn btn-submit" {{ (Request::is('report/personal/filter/published')) ? 'class="active"' : '' }} href="{{ URL::to('report/personal/filter/published') }}">&nbsp;{{Lang::get('report.filter_published')}}</a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3">
                    <a class="btn btn-submit" {{ (Request::is('report/personal/filter/archive')) ? 'class="active"' : '' }} href="{{ URL::to('report/personal/filter/archive') }}">&nbsp;{{Lang::get('report.filter_archive')}}</a>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($reports as $report)
                <div class="col-lg-3 col-md-3 col-sm-4 item">
                    <div class="album-photo">
                        <a href="{{ $report->url() }}">
                            @if($report->image)
                            <img class="content-image" src="{{ asset(\Config::get('app.image_dir.posts_small') . $report->image) }}" alt="">
                            @else
                                {{\Config::get('app.album_no_image_260_180_with_class')}}
                            @endif
                        </a>
                    </div>
                    <div class="photo-info">
                        <div class="line-info"><span>{{ Lang::get('report.form_title') }}:&nbsp;</span><a href="{{ $report->url() }}">{{ String::title($report->title) }}</a></div>
                        <div class="line-info"><span>{{ Lang::get('report.author') }}:&nbsp;</span>{{ $report->author->username }}</div>
                        <div class="line-info"><span>{{ Lang::get('general.date') }}:&nbsp;</span>{{ $report->date() }}</div>
                        <div class="line-info">{{--| <span class="glyphicon glyphicon-comment"></span> <a href="{{ $report->url() }}#comments">{{$report->comments()->count()}} {{ \Illuminate\Support\Pluralizer::plural('Comment', $report->comments()->count()) }}</a>--}}</div>
                        <div class="line-info"><input data-readonly="true" class="g_rating" value="{{ $report->rating }}"></div>
                    </div>
                    <div id="_token" token="{{ csrf_token() }}"></div>
                </div>
            @endforeach
        </div>


        {{ $reports->links() }}
    </div>
    <!-- ./ tabs content -->

@stop
