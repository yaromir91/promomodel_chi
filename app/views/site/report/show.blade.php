@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('report.title') }} - {{ $post->title }} ::
    @parent
@stop

@section('meta_description'){{ Lang::get('report.menu_report') }} - {{ $post->title }}@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('report.menu_report') }} - {{ $post->title }}</h1>
    </div>
    @if($user->id == $post->user_id)
    <div class="pull-right">
        <a href="{{ URL::to('report/edit/' . $post->id) }}" class="btn btn-submit">
            {{ Lang::get('button.edit') }}
        </a>
        <a href="#" data-href="{{ URL::route('report_delete_report', $post->id) }}" data-toggle="modal" data-target="#confirm-delete-report" class="btn btn-submit">
            {{ Lang::get('button.delete') }}
        </a>
    </div>
    @endif
    <!-- Tabs -->
    {{--@include('site.report.nav_tabs')--}}
    <!-- ./ tabs -->
    <!-- Tabs Content -->
    <div class="content report-content">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="report-photo">
                    @if($post->image)
                    <img class="main-image" src="{{ asset(\Config::get('app.image_dir.posts_preview')  . $post->image) }}" alt="">
                    @else
                        {{\Config::get('app.album_no_image_260_180_main')}}
                    @endif
                </div>
                <div class="report-description">
                    <h3>{{ Lang::get('report.description') }}</h3>
                    <p>{{ $post->content() }}</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h3 class="no-border">{{ Lang::get('report.details') }}</h3>
                <div class="row">
                    <div class="col-lg-4">
                        <p>{{ Lang::get('report.author') }}:</p>
                        <p>{{ Lang::get('report.publeshed') }}:</p>
                        <p>{{ Lang::get('report.status') }}:</p>
                        <p> <input @if(!Auth::user() || $post->user_id == Auth::user()->id) data-readonly="true" @endif class="g_rating" value="@if($rating){{ $rating[$post->id] }}@endif" data-id="{{ $post->id }}" ></p>
                    </div>
                    <div class="col-lg-8">
                        <p><b>@if (is_object($post->author)) {{ $post->author->username }} @endif</b></p>
                        <p><b>{{ $post->date() }}</b></p>
                        <p><b>{{ $post->status() }}</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="_token" token="{{ csrf_token() }}"></div>
        <div class="row add-comment">
            <div class="col-lg-12">
                <h3>{{ Lang::get('report.add_comment') }}</h3>
                {{ Form::open(array('url' => 'report/add_comment/' . $post->id, 'method' => 'post', 'files' => true, 'autocomplete' => 'off')) }}
                <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                    <div class="col-lg-10 col-md-10 col-sm-9">
                        {{ Form::textarea('comment', '', array('class' => 'form-control', 'id' => 'comment', 'rows' => '5')) }}
                        {{ $errors->first('comment') }}
                    </div>
                    <!-- Form Actions -->
                    <div class="col-lg-2 col-md-2 col-sm-3">
                        <button type="submit" class="btn btn-submit">{{Lang::get('button.add')}}</button>
                    </div>
                    <!-- ./ form actions -->
                </div>
                <!-- Image -->
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        {{ Form::file('image') }}
                        {{ $errors->first('image', '<span class="help-block">:message</span>') }}
                    </div>
                </div>
                <!-- ./ image -->
                {{ Form::close() }}
            </div>
        </div>
        @if ($comments->count())
        @foreach ($comments as $comment)
            <div class="row" id="comment_{{ $comment->id }}">
                <div class="col-lg-12 comment-text">
                    <h4>{{ $comment->author->username; }}</h4>
                    <p id="comment_content_block_{{ $comment->id }}">{{ $comment->content(); }}</p>
                    @if($comment->image)
                        <p><img class="img-thumbnail comment-img" src="{{ asset(\Config::get('app.image_dir.comment') . $comment->id . '/'  . $comment->image) }}" alt=""></p>
                    @endif
                    <span>{{ $comment->date(); }}</span>
                    @if($comment->user_id == $user->id || $is_admin)
                    <div class="pull-right">
                        <a href="#" data-toggle="modal" data-target="#comment-edit-form_{{ $comment->id }}">
                            <span>{{ Lang::get('report.comment_edit') }} </span>
                        </a>|
                        <a href="#" data-href="{{ URL::to('report/comment/delete/' . $comment->id) }}" data-id="{{ $comment->id }}" data-toggle="modal" data-target="#confirm-delete-comment">
                            <span>{{ Lang::get('report.comment_delete') }}</span>
                        </a>
                    </div>
                    @endif
                </div>
            </div>
            {{-- COMMENT EDIT FORM MODAL--}}
            <div class="modal fade" id="comment-edit-form_{{ $comment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            {{ Form::textarea('comment', $comment->content(), array('class' => 'form-control', 'id' => 'comment_content_' . $comment->id)) }}
                            {{ $errors->first('comment') }}
                            <div id="_token" token="{{ csrf_token() }}"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                            <a class="btn btn-default btn_update_comment" data-href="{{ URL::to('report/comment/edit/' . $comment->id) }}" data-id="{{ $comment->id }}">{{ Lang::get('button.update') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- COMMENT EDIT FORM MODAL--}}
        @endforeach
        @endif
        </div>
                    <!-- ./ tabs content -->


    {{-- POST DELETE ALERT MODAL--}}
    <div class="modal fade" id="confirm-delete-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ Lang::get('report.confirm_delete_msg') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a class="btn btn-default btn-ok">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>
    {{-- POST DELETE ALERT MODAL--}}

    {{-- COMMENT DELETE ALERT MODAL--}}
    <div class="modal fade" id="confirm-delete-comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('button.cancel') }}</button>
                    <a class="btn btn-default" id="btn_delete_comment">{{ Lang::get('button.delete') }}</a>
                </div>
            </div>
        </div>
    </div>
    {{-- COMMENT DELETE ALERT MODAL--}}

@stop

