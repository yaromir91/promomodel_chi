@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ Lang::get('report.all_reports') }} ::
    @parent
@stop

@section('meta_description'){{ Lang::get('report.all_reports') }}@stop


{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>{{ Lang::get('report.menu_report') }}</h1>
    </div>
    @if($my_events)
        <div class="pull-right">
            <a href="{{ URL::to('report/create') }}" class="btn btn-submit iframe">{{ Lang::get('button.create') }}</a>
        </div>
    @endif
    <!-- Tabs -->
    @include('site.report.nav_tabs')
    <!-- ./ tabs -->

    <!-- Tabs Content -->
    <div class="report-content">
        <div class="row">
            @foreach ($reports as $report)
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <div class="photo-album">
                        <a href="{{ $report->url() }}">
                            @if($report->image)
                            <img class="content-image" src="{{ asset(\Config::get('app.image_dir.posts_small') . $report->image) }}" alt="">
                            @else
                                {{\Config::get('app.album_no_image_260_180_with_class')}}
                            @endif
                        </a>
                    </div>
                    <div class="photo-info">
                        <div class="line-info">
                            <span>{{ Lang::get('report.form_title') }}:&nbsp;</span><a href="{{ $report->url() }}">{{ String::title($report->title) }}</a>
                        </div>
                        <div class="line-info">
                            <span>{{ Lang::get('report.author') }}:&nbsp;</span>{{ $report->author->username }}
                        </div>
                        <div class="line-info">
                            <span>{{ Lang::get('general.date') }}:&nbsp;</span>{{ $report->date() }}
                        </div>
                        <div class="line-info">
                            {{--| <span class="glyphicon glyphicon-comment"></span> <a href="{{ $report->url() }}#comments">{{$report->comments()->count()}} {{ \Illuminate\Support\Pluralizer::plural('Comment', $report->comments()->count()) }}</a>--}}
                        </div>
                        <div class="line-info">
                            <input @if(!Auth::user() || $report->user_id == Auth::user()->id) data-readonly="true" @endif class="g_rating" value="{{ $report->rating }}" data-id="{{ $report->id }}" >
                        </div>
                    </div>
                    <!-- Post Footer -->
                    <div id="_token" token="{{ csrf_token() }}"></div>
                    <!-- ./ post footer -->
                </div>
            @endforeach
        </div>
        {{ $reports->links() }}
    </div>
    <!-- ./ tabs content -->
@stop
