@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
    {{ $title }} :: @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <h1>
            {{ $title }}
        </h1>
    </div>
	{{-- Create User Form --}}
    {{ Form::open(array('url' => 'report/edit/' . $data['post']->id, 'method' => 'post', 'autocomplete' => 'off', 'files' => 'true', 'class' => 'form-horizontal avatar-form')) }}
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <!-- events -->
                <div class="form-group {{ $errors->has('events') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('events', Lang::get('report.form_events'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::select('events', $data['events'], (Input::old('events')) ? Input::old('events') : $data['post']->event_id, array('class' => 'form-control', 'id' => 'events')) }}
                        {{ $errors->first('events') }}
                    </div>
                </div>
                <!-- ./ events -->
                <!-- Title -->
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('title', Lang::get('report.form_title'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::text('title', (Input::old('title')) ? Input::old('title') : $data['post']->title, array('class' => 'form-control', 'id' => 'title')) }}
                        {{ $errors->first('title') }}
                    </div>
                </div>
                <!-- ./ Title -->
                <!-- Description -->
                <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('content', Lang::get('report.form_content'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        {{ Form::textarea('content', (Input::old('content')) ? Input::old('content') : $data['post']->content, array('class' => 'form-control', 'id' => 'content')) }}
                        {{ $errors->first('content') }}
                    </div>
                </div>
                <!-- ./ Description -->

                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('image', Lang::get('report.image'), array('class' => 'control-label')) }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8">
                        @include('site.profile.applicant.user_avatar', [
                             'fileFieldName'   => 'image',
                             'actionUrlUpload' => '/report/upload',
                             'origImage'       => $data['post']->getTemporarySmallFile() ,
                             'origImageName'   => $data['post']->getTemporarySmallFileName()
                         ])

                        {{ $errors->first('image') }}

                    </div>
                </div>

                <!-- Tags -->
<!--                <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                    {{ Form::label('tags', Lang::get('report.form_tags'), array('class' => 'col-lg-2 control-label')) }}
                    <div class="col-lg-5">
                        {{ Form::text('tags', (Input::old('tags')) ? Input::old('tags') : implode(", ", $data['tags']), array('class' => 'form-control', 'id' => 'tags')) }}
                        {{ Lang::get('report.separate_words') }}.
                        {{ $errors->first('tags') }}
                    </div>
                </div>-->
                <!-- ./ Tags -->
                <!-- Status -->
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        {{ Form::label('status', Lang::get('report.form_status'), array('class' => 'control-label')) }}
                        {{ Form::checkbox('status', Input::old('status'), ($data['post']->status)?true:false, array('id' => 'status')) }}
                        {{ $errors->first('status') }}
                    </div>
                </div>
                <!-- ./ Status -->
            </div>
		<!-- Form Actions -->
        <div class="form-group">
            <div class="col-lg-2 col-md-2 col-sm-3">
                <button type="button" class="btn btn-submit" onclick="window.location = '{{ URL::to('report/show/' . $data['post']->slug) }}'">{{Lang::get('button.cancel')}}</button>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3">
                <button type="submit" class="btn btn-submit">{{Lang::get('button.save')}}</button>
            </div>
        </div>
		<!-- ./ form actions -->
    {{ Form::close() }}
@stop
