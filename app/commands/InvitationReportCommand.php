<?php namespace Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Acme\Models\Repositories\EventsRepository;

class InvitationReportCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'invite:report';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Invitation Report.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$eventsForReport = EventsRepository::getEventsForReport();

		if (count($eventsForReport)) {
			foreach ($eventsForReport as $event) {
				\Queue::push('\Acme\Queues\SendInvitationReportQueue', [
					'eventId'        => $event->id
				], 'emails');
			}
		}
	}


}
