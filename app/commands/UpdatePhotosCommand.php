<?php

namespace Commands;

use Illuminate\Console\Command;
use Acme\Models\Repositories\GalleryImagesRepository;
use Acme\Models\Repositories\EventsRepository;
use Acme\Models\Repositories\GroupsRepository;

/**
 * Class UpadtePhotosCommand
 * @package Commands
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class UpdatePhotosCommand extends Command
{

    /**
     * @var string
     */
    protected $name = 'pictures:update';

    /**
     * @var string
     */
    protected $description = 'Update all pictures.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     */
    public function fire()
    {
        GalleryImagesRepository::updateAllPicGallery();
        GalleryImagesRepository::updateAllGalleryPreview();
        EventsRepository::updateAllPreview();
        GroupsRepository::updateAllPreview();
    }

}