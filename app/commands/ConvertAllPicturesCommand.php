<?php



namespace Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


ini_set('memory_limit', '500M');
/**
 * Class ConvertAllPicturesCommand
 * @package Commands
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ConvertAllPicturesCommand extends Command
{

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'pictures:convert';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Convert all pictures.';

	/**
	 * @var string
	 */
	private $uploadDir = 'upload';

	private $LogFile = 'conver_picters.log';

	/**
	 * ConvertAllPicturesCommand constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $startTime   = microtime(true);
        $picturePath = $this->argument('path');
        $isFile      = (bool) $this->option('is_file');
		$quality     = $this->option('qlty');


        if ($isFile) {
            $filname     = public_path() . '/' . $this->uploadDir . '/' . $picturePath;
            $this->convertPictures($filname, $quality);
            echo ("File convertes: " . $filname);
            echo "\n";
        } else {
            $path        = public_path() . '/' . $this->uploadDir . '/' . $picturePath;

            if (is_dir($path)) {
                $Directory   = new \RecursiveDirectoryIterator($path);
                $Iterator    = new \RecursiveIteratorIterator($Directory);
                $allImage    = new \RegexIterator($Iterator, '/^.+\.(jpg|png|jpeg)$/i', \RecursiveRegexIterator::GET_MATCH);

                foreach ($allImage  as $name => $object) {
                    $this->convertPictures($name, $quality);
                    echo ("File convertes: " . $name);
                    echo "\n";
                }
            }
        }

		$endTime = microtime(true);

		echo ("Running time: " . ($endTime - $startTime) . 's');
		echo "\n";
	}

    /**
     * @param \Exception $e
     */
    private function logError(\Exception $e)
    {
        \Log::useDailyFiles(storage_path().'/logs/'.$this->LogFile);
        \Log::error($e);

    }

    /**
     * @param $image
     * @param $quality
     */
	private function convertPictures($image, $quality)
	{
		try {
			exec("jpegoptim " . $image . " --strip-all --all-progressive");
			/*$img = \Image::make($image);
			$img->save($image,  $quality);
			$img = null;*/
		} catch (\Exception $e) {
            $this->logError($e);
		}

	}


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
				array('path', InputArgument::REQUIRED, 'Path of pictures location')
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
				array('qlty', null, InputOption::VALUE_REQUIRED, 'Quality of pictures'),
				array('is_file', null, InputOption::VALUE_OPTIONAL, 'File flag')
		);
	}

}
