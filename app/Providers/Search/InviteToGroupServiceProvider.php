<?php

namespace Acme\Providers\Search;

use Acme\Models\AssignedRoles;
use Acme\Models\GroupsInvites;
use Acme\Models\Profile\UserProfile;
use Acme\Models\Profile\UserProfileData;
use Acme\Models\Role;
use Acme\Models\User;
use Acme\Models\UsersGroup;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

/**
 * Class InviteToGroupServiceProvider
 * @package Acme\Providers\Search
 */
class InviteToGroupServiceProvider extends ServiceProvider
{

    /**
     * 1 - prm_user_profile_data.int_value
     * 2 - prm_user_profile_data.param_id_val
     * 3 - prm_users.username && prm_user_profile.last_name && prm_user_profile.first_name TODO This is search on the name
     * @var array
     */
    protected $typeToMethodMapping = [
        '1'     => 'searchOnIntValue',
        '2'     => 'searchOnParamIdVal',
        '3'     => 'searchOnNickFnLn',
        '4'     => 'setSomeProfileParams',
    ];

    /**
     * @var object
     */
    protected $group;
    /**
     * @var \Eloquent object
     */
    public $query;
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('InviteToGroupServiceProvider', function ($app, $data) {

            $this->group = $data['group'];

            return $this->buildWhere($data['search_params']);
        });

    }

    /**
     *
     * @param $conditions
     *
     * @return bool
     */
    public function buildWhere($conditions)
    {
        /* all collection with needed tables */
        $this->query = $this->buildJoin();

        foreach ($conditions as $type => $condition) {

            if(isset($condition['data']) && $condition['data'][0]) {
                $method = $this->typeToMethodMapping[$condition['type']];

                foreach ($condition as $key => $value) {

                    $this->query = $this->$method($type, $value, $this->query);

                }
            }
        }

        $user_profile = UserProfile::getTableName();
        $groups_invites = GroupsInvites::getTableName();

        $this->query->select([
            $user_profile . '.user_id',
            $user_profile . '.last_name',
            $user_profile . '.first_name',
            $user_profile . '.date_birth',
            $user_profile . '.vk_link',
            $user_profile . '.gender',
            $user_profile . '.avatar',
            User::getTableName() . '.email',
            User::getTableName() . '.username',
            $groups_invites . '.status'
        ]);
        $this->query->groupBy(UserProfile::getTableName() . '.user_id');
    
        return $this->query->get();
//        return $query->paginate(\Config::get('app.groups_pagination'));

    }

    /**
     * @param null $paramsId
     * @param null $value
     * @param null $query
     * @return array|null
     */
    protected function searchOnIntValue($paramsId = null, $value = null, $query)
    {
        if(!empty($value[0]) && !empty($value[1]) && !empty($paramsId)) {

            $aliasTable = 'upd' . $paramsId ;
            $query->join((UserProfileData::getTableName() . ' AS ' . \DB::getTablePrefix() . 'upd' . $paramsId),
                $aliasTable . '.user_id',
                '=',
                UserProfileData::getTableName() . '.user_id'
            );
            $query->whereNested(function(Builder $where) use ($aliasTable, $paramsId, $value, $query){
                $where->where([$aliasTable . '.params_id' => $paramsId]);
                $where->whereBetween($aliasTable . '.int_value', $value);

            })
            ;
            return $query;
        }
        return $query;
    }

    /**
     * @param null $paramsId
     * @param null $value
     * @param null $query
     * @return array|null
     */
    protected function searchOnParamIdVal($paramsId = null, $value = null, $query = null)
    {

        if(!empty($value[0]) && is_array($value) && !empty($paramsId)) {
            $aliasTable = 'upd' . $paramsId ;
            $query->join((UserProfileData::getTableName() . ' AS ' . \DB::getTablePrefix() . 'upd' . $paramsId),
                $aliasTable . '.user_id',
                '=',
                UserProfileData::getTableName() . '.user_id'
            );
            $query->whereNested(function(Builder $where) use ($aliasTable, $paramsId, $value, $query){
                $where->where([$aliasTable . '.params_id' => $paramsId]);
                $where->whereIn($aliasTable . '.param_id_val', $value);

            })
            ;
            return $query;
        }
        return $query;
    }

    /**
     *
     * Search users by nick, last_name and first_name
     * @param null $params
     * @param null $value
     * @param null $query
     * @return array|null|string
     */
    protected function searchOnNickFnLn($params = null, $value = null, $query = null)
    {
        if(!empty($value[0]) && is_array($value) && !empty($params)) {

            if($params == 'username'){
                $aliasTable = User::getTableName();
                $query->where($aliasTable . '.' . $params , 'LIKE' , '%' .$value[0]. '%' );

            } else {
                $aliasTable = UserProfile::getTableName();
                $query->where($aliasTable . '.' . $params , 'LIKE' , '%' .$value[0]. '%' );
            }

            return $query;
        }
        return $query;
    }

    /**
     * @param null $params
     * @param null $value
     * @param null $query
     *
     * @return null
     */
    protected function setSomeProfileParams($params = null, $value = null, $query = null)
    {
        if(!empty($value[0]) && is_array($value) && !empty($params)) {
            $aliasTable = UserProfile::getTableName();
            $query->where($aliasTable . '.' . $params , '=' , $value[0] );
        }

        return $query;
    }

    /**
     * Builder all needed table for search user
     */
    public function buildJoin()
    {

        $query = DB::table(User::getTableName());

        $query->join(UserProfileData::getTableName(),
            UserProfileData::getTableName() . '.user_id', '=',
            User::getTableName() . '.id', 'left');

        $query->join(AssignedRoles::getTableName(),
            AssignedRoles::getTableName() . '.user_id', '=',
            User::getTableName() . '.id');

        $query->join(Role::getTableName(),
            Role::getTableName() . '.id', '=',
            AssignedRoles::getTableName() . '.role_id');

        $query->join(UserProfile::getTableName(),
            UserProfile::getTableName() . '.user_id', '=',
            User::getTableName() . '.id');

        $query->join(GroupsInvites::getTableName(),
            GroupsInvites::getTableName() . '.user_id', '=',
            User::getTableName() . '.id', 'left');

        $query->join(UsersGroup::getTableName(),
            UsersGroup::getTableName() . '.user_id', '=',
            User::getTableName() . '.id', 'left');

        $query->whereNested(function($query){
            /** @var \Eloquent $query */
            $query->where([Role::getTableName() . '.code' => Role::APPLICANT_USER, ]);
            $query->orWhere([Role::getTableName() . '.code' => Role::APPLICANT_MANAGER_USER]);
            $query->whereRaw('IF(`' . \DB::getTablePrefix() . GroupsInvites::getTableName() . '`.`status` IS NOT NULL, `' . \DB::getTablePrefix() . GroupsInvites::getTableName() . '`.`status` <> "active", TRUE)');
        });



        $query->whereNested(function($query){
            /** @var \Eloquent $query */
            $query->whereNull(UsersGroup::getTableName() . '.user_id');
        });

        return $query;
    }

    /**
     * @param $result
     * @return array
     */
    private function convertResult($result){
        /* For WHERE IN () */
        $res = [];
        foreach ($result as $key => $id) {
            $res[] = $id['user_id'];

        }
        return $res;
    }
}