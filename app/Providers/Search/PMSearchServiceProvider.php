<?php

namespace Acme\Providers\Search;

use Acme\Models\AssignedRoles;
use Acme\Models\Events\Events;
use Acme\Models\Groups;
use Acme\Models\GroupsInvites;
use Acme\Models\Professions\ProfessionsEventsRates;
use Acme\Models\Professions\UserProfessionsRates;
use Acme\Models\Profile\UserProfile;
use Acme\Models\Profile\UserProfileData;
use Acme\Models\Repositories\UserRepository;
use Acme\Models\Role;
use Acme\Models\User;
use Acme\Models\UsersGroup;
use Illuminate\Support\ServiceProvider;

/**
 * Class PMSearchServiceProvider
 *
 * @package Acme\Providers\Search
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class PMSearchServiceProvider extends ServiceProvider
{

    /**
     * 1 - prm_user_profile_data.int_value
     * 2 - prm_user_profile_data.param_id_val
     * 3 - prm_users.username && prm_user_profile.last_name && prm_user_profile.first_name TODO This is search on the name
     * @var array
     */
    protected $typeToMethodMapping = [
        '1'     => 'searchOnIntValue',
        '2'     => 'searchOnParamIdVal',
        '3'     => 'searchOnNickFnLn',
    ];

    /**
     * @var object
     */
    protected $event;
    /**
     * @var \Eloquent object
     */
    public $query;
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PMSearchServiceProvider', function ($app, $data) {

            $this->event = $data['event'];
            
            if(isset($data['reject_send']) && $data['reject_send']) {
                $usersId = $this->buildJoin();
                if(is_array($usersId)){
                    return UserProfile::select(UserProfile::getTableName() . '.*')
                        ->whereIn(UserProfile::getTableName() . '.user_id', $usersId)
                        ->take(Events::REJECT_SEARCH_LIMIT)
                        ->get();
                }
                return $usersId;
            }

            return $this->buildWhere($data['search_params']);
        });
//
//        TODO: check it to drop!!!
//        $this->app->bind('PromoModelSearch', function($type, $paramId, $params) use ($this) {
//
//                return $this->buildWhere($type, $paramId, $params);
//        });
    }

    /**
     *
     * @param $conditions
     *
     * @return bool
     */
    public function buildWhere($conditions)
    {
        /* all collection with needed tables */
        $usersId = $this->buildJoin();
        /* init search on the params*/
        $this->query = new UserProfileData;

        foreach ($conditions as $type => $condition) {

            if (isset($this->typeToMethodMapping[$condition['type']])) {

                $method = $this->typeToMethodMapping[$condition['type']];

                foreach ($condition as $key => $value) {

                    $usersId = $this->$method($type, $value, $usersId, $condition['type']);

                }
            }
        }

        if(is_array($usersId)){
            return UserProfile::select(UserProfile::getTableName() . '.*', UsersGroup::getTableName() . '.groups_id', Groups::getTableName() . '.title')
                ->leftJoin(UsersGroup::getTableName(), UsersGroup::getTableName() . '.user_id', '=', UserProfile::getTableName() . '.user_id')
                ->leftJoin(Groups::getTableName(), Groups::getTableName() . '.id', '=', UsersGroup::getTableName() . '.groups_id')
                ->whereIn(UserProfile::getTableName() . '.user_id', $usersId)
                ->orderBy(UsersGroup::getTableName() . '.groups_id', 'desc')
                ->get();
//            return UserRepository::getUsersAndGroups($usersId);
//            return UserProfile::whereIn('user_id', $usersId)->get();
        }

        return $usersId;
    }

    /**
     * @param null $paramsId
     * @param null $value
     * @param null $usersId
     * @return array|null
     */
    protected function searchOnIntValue($paramsId = null, $value = null, $usersId = null)
    {
        if(!empty($value[0]) && !empty($value[1]) && !empty($paramsId)) {

            $aliasTable = 'upd' . $paramsId ;
            $this->query = $this->query->join((UserProfileData::getTableName() . ' AS ' . \DB::getTablePrefix() . 'upd' . $paramsId),
                $aliasTable . '.user_id',
                '=',
                UserProfileData::getTableName() . '.user_id'
            )
            ->whereNested(function(\Illuminate\Database\Query\Builder $where) use ($aliasTable, $paramsId, $value, $usersId){
                $where->where([$aliasTable . '.params_id' => $paramsId]);
                $where->whereBetween($aliasTable . '.int_value', $value);

            })
            ->whereIn($aliasTable . '.user_id', $usersId)
            ->groupBy($aliasTable . '.user_id')
            ;

            return $this->convertResult($this->query->get()->toArray());
        } else {
            return $usersId;
        }
    }

    /**
     * @param null $paramsId
     * @param null $value
     * @param null $usersId
     * @return array|null
     */
    protected function searchOnParamIdVal($paramsId = null, $value = null, $usersId = null)
    {
        if(!empty($value[0]) && is_array($value) && !empty($paramsId)) {

            $aliasTable = 'upd' . $paramsId ;
            $this->query = $this->query->join((UserProfileData::getTableName() . ' AS ' . \DB::getTablePrefix() . 'upd' . $paramsId),
                $aliasTable . '.user_id',
                '=',
                UserProfileData::getTableName() . '.user_id'
            )
            ->whereNested(function(\Illuminate\Database\Query\Builder $where) use ($aliasTable, $paramsId, $value, $usersId){
                $where->where([$aliasTable . '.params_id' => $paramsId]);
                $where->whereIn($aliasTable . '.param_id_val', $value);

            })
            ->whereIn($aliasTable . '.user_id', $usersId)
            ->groupBy($aliasTable . '.user_id')
            ;

            return $this->convertResult($this->query->get()->toArray());
        } else {
            return $usersId;
        }
    }

    /**
     * Builder all needed table for search user
     */
    protected function buildJoin()
    {
        /** @var \Eloquent $query */

        if ($this->event instanceof Events) {
            $query = $this->buildJoinEventsPresent();
        } elseif($this->event instanceof UsersGroup) {
            $query = $this->buildJoinUsersGroup();
        } else {
            $query = $this->buildJoinUser();
        }

        return $this->convertResult($query->get()->toArray());
    }

    /**
     * @return User
     */
    protected function buildJoinUser()
    {
        $query = new User();

        $query = $query->join(UserProfileData::getTableName(),
            UserProfileData::getTableName() . '.user_id', '=',
            User::getTableName() . '.id');

        $query = $query->join(AssignedRoles::getTableName(),
            AssignedRoles::getTableName() . '.user_id', '=',
            User::getTableName() . '.id');

        $query = $query->join(Role::getTableName(),
            Role::getTableName() . '.id', '=',
            AssignedRoles::getTableName() . '.role_id');

        $query = $query->join(UserProfile::getTableName(),
            UserProfile::getTableName() . '.user_id', '=',
            User::getTableName() . '.id');

        $query = $query->whereNested(function($query){
                    /** @var \Eloquent $query */
                    $query->where([Role::getTableName() . '.code' => Role::APPLICANT_USER])
                        ->orWhere([Role::getTableName() . '.code'  => Role::APPLICANT_MANAGER_USER]);
                });

        $query = $query->where(UserProfile::getTableName().'.public', '=', 1);

        $query = $query->select([
                UserProfile::getTableName() . '.user_id',
            ]);

        $query->groupBy(UserProfile::getTableName() . '.user_id');

        return $query;
    }

    public function buildJoinUsersGroup()
    {
        $query = new User();

        $query = $query->join(UserProfileData::getTableName(),
            UserProfileData::getTableName() . '.user_id', '=',
            User::getTableName() . '.id');

        $query = $query->join(AssignedRoles::getTableName(),
            AssignedRoles::getTableName() . '.user_id', '=',
            User::getTableName() . '.id');

        $query = $query->join(Role::getTableName(),
            Role::getTableName() . '.id', '=',
            AssignedRoles::getTableName() . '.role_id');

        $query = $query->join(UserProfile::getTableName(),
            UserProfile::getTableName() . '.user_id', '=',
            User::getTableName() . '.id');

        $query = $query->join(UsersGroup::getTableName(),
            UsersGroup::getTableName() . '.user_id', '=',
            User::getTableName() . '.id', 'left');

        $query = $query->whereNested(function($query){
            /** @var \Eloquent $query */
            $query->where([Role::getTableName() . '.code' => Role::APPLICANT_USER])
                ->orWhere([Role::getTableName() . '.code'  => Role::APPLICANT_MANAGER_USER]);
        });

        $query = $query->where(UserProfile::getTableName().'.public', '=', 1);

        $query = $query->select([
            UserProfile::getTableName() . '.user_id',
        ]);

        $query->groupBy(UserProfile::getTableName() . '.user_id');

        return $query;
    }

    /**
     * @return mixed
     */
    protected function buildJoinEventsPresent()
    {
        $query = new Events;
        $query = $query->join(ProfessionsEventsRates::getTableName(), 'id', '=', 'events_id');
        $query = $query->join(UserProfessionsRates::getTableName(),
            UserProfessionsRates::getTableName() . '.professions_id', '=',
            ProfessionsEventsRates::getTableName() . '.professions_id');
        $query = $query->join(UserProfileData::getTableName(),
            UserProfileData::getTableName() . '.user_id', '=',
            UserProfessionsRates::getTableName() . '.user_id');
        $query = $query->join(AssignedRoles::getTableName(),
            AssignedRoles::getTableName() . '.user_id', '=',
            UserProfessionsRates::getTableName() . '.user_id');
        $query = $query->join(Role::getTableName(),
            Role::getTableName() . '.id', '=',
            AssignedRoles::getTableName() . '.role_id');
        $query = $query->join(UserProfile::getTableName(),
            UserProfile::getTableName() . '.user_id', '=',
            UserProfessionsRates::getTableName() . '.user_id');
        $query = $query->where(['events_id' => $this->event->id])
            ->whereNested(function($query){
                    /** @var \Eloquent $query */
                    $query->whereRaw(\DB::getTablePrefix() . UserProfessionsRates::getTableName() . '.rate_h' . ' <= ' . \DB::getTablePrefix() . ProfessionsEventsRates::getTableName() . '.rate_h')
                        ->whereRaw(\DB::getTablePrefix() . UserProfessionsRates::getTableName() . '.rate_d' . ' <= ' . \DB::getTablePrefix() . ProfessionsEventsRates::getTableName() . '.rate_d')
                        ;
                })
            ->whereNested(function($query){
                    /** @var \Eloquent $query */
                    $query->where([Role::getTableName() . '.code' => Role::APPLICANT_USER])->orWhere([Role::getTableName() . '.code'  => Role::APPLICANT_MANAGER_USER]);
                });
        $query = $query->where(UserProfile::getTableName().'.public', '=', 1);
        $query = $query->select([
                UserProfile::getTableName() . '.user_id',
            ]);

        $query->groupBy(UserProfessionsRates::getTableName() . '.user_id');

        return $query;
    }

    /**
     * @param $result
     * @return array
     */
    private function convertResult($result){
        /* For WHERE IN () */
        $res = [];
        foreach ($result as $key => $id) {
            $res[] = $id['user_id'];

        }
        return $res;
    }
}