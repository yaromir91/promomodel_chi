<?php
/**
 * Created by PhpStorm.
 * User: Sergey Bespalov
 * Date: 25.07.15
 * Time: 11:56
 */

namespace Acme\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class SearchApplicantServiceProvider
 * @package Acme\Providers
 */
class ReserveServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $result = [];
    
    protected $reserve;


    const DAILY     = 'daily';
    const WEEKLY    = 'weekly';
    const MONTHLY   = 'monthly';
    const YEARLY    = 'yearly';


    public function register()
    {
        $this->app->bind('ReserveServiceProvider', function($app, $data)
        {
            $this->reserve = $data['reserve_input'];

            switch ($this->reserve['repeat_type']){
                case self::DAILY:
                    $this->dailyHandler();
                    break;
                case self::WEEKLY:
                    $this->weeklyHandler();
                    break;
                case self::MONTHLY:
                    $this->monthlyHandler();
                    break;
                case self::YEARLY:
                    $this->yearlyHandler();
                    break;
            }

            return $this->result;
        });
    }

    protected function dailyHandler()
    {
        $this->result['repeat_end'] = date('Y-m-d H:i', strtotime($this->reserve['datetimepickerDaily']));
        $this->result['repeat_day'] = $this->reserve['repeat_daily'];
    }

    protected function weeklyHandler()
    {
        $this->result['repeat_end'] = date('Y-m-d H:i', strtotime($this->reserve['datetimepickerWeekly']));
        $this->result['repeat_weekday'] = implode(",", $this->reserve['weekly_repeat']);
    }
    
    protected function monthlyHandler()
    {
        $this->result['repeat_end'] = date('Y-m-d H:i', strtotime($this->reserve['datetimepickerMonthly']));
        $this->result['repeat_month'] = $this->reserve['repeat_monthly'];
    }
    
    protected function yearlyHandler()
    {
        $this->result['repeat_end'] = date('Y-m-d H:i', strtotime($this->reserve['datetimepickerYearly']));
        $this->result['repeat_years'] = $this->reserve['repeat_yearly'];
    }
}