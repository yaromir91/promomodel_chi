<?php

namespace Acme\Providers;

use Illuminate\Support\ServiceProvider;
use \Acme\Models\Repositories\EventsRepository;
use \Acme\Models\Role;
use \Acme\Models\Repositories\UserReservesRepository;
use \Acme\Models\Events\EventInvite;

/**
 *
 * Class CalendarServiceProvider
 *
 * @package Acme\Providers
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class CalendarServiceProvider extends ServiceProvider {

    /**
     * @var array
     */
    protected $result = [];
    protected $user;


    public function register()
    {
        $this->app->bind('CalendarServiceProvider', function($app, $data)
        {
            $this->user = $data['user'];
            $role = $this->user->getMainUserRoleCode();

            switch ($role){
                case Role::ORGANIZER_USER:
                    $this->organizerHandler();
                    break;
                case Role::APPLICANT_USER:
                    $this->applicantHandler();
                    break;
                case Role::APPLICANT_MANAGER_USER:
                    $this->applicantHandler();
                    break;
            }

            return $this->result;
        });
    }

    public function organizerHandler()
    {
        $reserves = UserReservesRepository::getUserReserves($this->user->id);

        $events = EventsRepository::getMyEvents($this->user);

        foreach($events as $event)
        {
            if(isset($event->id)){
                if($event->repeat_status == 1) {
                    $this->result[] = array(
                        'repeat' =>         true,
                        'title' =>          $event->title,
                        'url' =>            'events/show/' . $event->id,
                        'start' =>          strtotime($event->date_start),
                        'end' =>            strtotime($event->date_end),
                        'textColor' =>      \Config::get('app.calendar.textColor'),
                        'color' =>          \Config::get('app.calendar.event_color'),
                        'repeat_years' =>   $event->repeat_years,
                        'repeat_month' =>   $event->repeat_month,
                        'repeat_weekday' => $event->repeat_weekday,
                        'repeat_day' =>     $event->repeat_day,
                        'repeat_end' =>     ($event->repeat_end != '0000-00-00 00:00:00') ? strtotime($event->repeat_end) : false
                    );
                } else {
                    $this->result[] = array(
                        'repeat' =>     false,
                        'title' =>      $event->title,
                        'url' =>        'events/show/' . $event->id,
                        'start' =>      strtotime($event->date_start),
                        'end' =>        strtotime($event->date_end),
                        'textColor' =>  \Config::get('app.calendar.textColor'),
                        'color' =>      \Config::get('app.calendar.event_color'),
                    );
                }
            }
        }

        if($reserves){
            foreach($reserves as $reserve) {
                if($reserve->repeat_status == 1) {
                    $this->result[] = array(
                        'repeat' =>         true,
                        'title' =>          $reserve->title,
                        'url' =>            'calendar/edit-reserve/' . $reserve->id,
                        'start' =>          strtotime($reserve->reserve_start),
                        'end' =>            strtotime($reserve->reserve_end),
                        'textColor' =>      \Config::get('app.calendar.textColor'),
                        'color' =>          \Config::get('app.calendar.reserve_color'),
                        'repeat_years' =>   $reserve->repeat_years,
                        'repeat_month' =>   $reserve->repeat_month,
                        'repeat_weekday' => $reserve->repeat_weekday,
                        'repeat_day' =>     $reserve->repeat_day,
                        'repeat_end' =>     ($reserve->repeat_end != '0000-00-00 00:00:00') ? strtotime($reserve->repeat_end) : false
                    );
                } else {
                    $this->result[] = array(
                        'repeat' =>     false,
                        'title' =>      $reserve->title,
                        'url' =>        'calendar/edit-reserve/' . $reserve->id,
                        'start' =>      strtotime($reserve->reserve_start),
                        'end' =>        strtotime($reserve->reserve_end),
                        'textColor' =>  \Config::get('app.calendar.textColor'),
                        'color' =>      \Config::get('app.calendar.reserve_color'),
                    );
                }
            }
        }
    }

    public function applicantHandler()
    {
        $reserves = UserReservesRepository::getUserReserves($this->user->id);

        $events = EventsRepository::getMyEvents($this->user, EventInvite::INVITE_PROF_STATUS_ACCEPT, EventInvite::IS_IN_TEAM);

        $eventsReserve = EventsRepository::getMyEvents($this->user, EventInvite::INVITE_PROF_STATUS_ACCEPT, EventInvite::IS_IN_RESERVE);

        foreach($eventsReserve as $event)
        {
            if(isset($event->id)){
                if($event->repeat_status == 1) {
                    $this->result[] = array(
                        'repeat' =>         true,
                        'title' =>          $event->title,
                        'url' =>            'events/show/' . $event->id,
                        'start' =>          strtotime($event->date_start),
                        'end' =>            strtotime($event->date_end),
                        'textColor' =>      \Config::get('app.calendar.textColor'),
                        'color' =>          \Config::get('app.calendar.public_event_color'),
                        'repeat_years' =>   $event->repeat_years,
                        'repeat_month' =>   $event->repeat_month,
                        'repeat_weekday' => $event->repeat_weekday,
                        'repeat_day' =>     $event->repeat_day,
                        'repeat_end' =>     ($event->repeat_end != '0000-00-00 00:00:00') ? strtotime($event->repeat_end) : false
                    );
                } else {
                    $this->result[] = array(
                        'repeat' =>     false,
                        'title' =>      $event->title,
                        'url' =>        'events/show/' . $event->id,
                        'start' =>      strtotime($event->date_start),
                        'end' =>        strtotime($event->date_end),
                        'textColor' =>  \Config::get('app.calendar.textColor'),
                        'color' =>      \Config::get('app.calendar.public_event_color'),
                    );
                }
            }
        }

        foreach($events as $event)
        {
            if(isset($event->id)){
                if($event->repeat_status == 1) {
                    $this->result[] = array(
                        'repeat' =>         true,
                        'title' =>          $event->title,
                        'url' =>            'events/show/' . $event->id,
                        'start' =>          strtotime($event->date_start),
                        'end' =>            strtotime($event->date_end),
                        'textColor' =>      \Config::get('app.calendar.textColor'),
                        'color' =>          \Config::get('app.calendar.accepted_event_color'),
                        'repeat_years' =>   $event->repeat_years,
                        'repeat_month' =>   $event->repeat_month,
                        'repeat_weekday' => $event->repeat_weekday,
                        'repeat_day' =>     $event->repeat_day,
                        'repeat_end' =>     ($event->repeat_end != '0000-00-00 00:00:00') ? strtotime($event->repeat_end) : false
                    );
                } else {
                    $this->result[] = array(
                        'repeat' =>     false,
                        'title' =>      $event->title,
                        'url' =>        'events/show/' . $event->id,
                        'start' =>      strtotime($event->date_start),
                        'end' =>        strtotime($event->date_end),
                        'textColor' =>  \Config::get('app.calendar.textColor'),
                        'color' =>      \Config::get('app.calendar.accepted_event_color'),
                    );
                }
            }
        }

        if($reserves){
            foreach($reserves as $reserve) {
                if($reserve->repeat_status == 1) {
                    $this->result[] = array(
                        'repeat' =>         true,
                        'title' =>          $reserve->title,
                        'url' =>            'calendar/edit-reserve/' . $reserve->id,
                        'start' =>          strtotime($reserve->reserve_start),
                        'end' =>            strtotime($reserve->reserve_end),
                        'textColor' =>      \Config::get('app.calendar.textColor'),
                        'color' =>          \Config::get('app.calendar.reserve_color'),
                        'repeat_years' =>   $reserve->repeat_years,
                        'repeat_month' =>   $reserve->repeat_month,
                        'repeat_weekday' => $reserve->repeat_weekday,
                        'repeat_day' =>     $reserve->repeat_day,
                        'repeat_end' =>     ($reserve->repeat_end != '0000-00-00 00:00:00') ? strtotime($reserve->repeat_end) : false
                    );
                } else {
                    $this->result[] = array(
                        'repeat' =>     false,
                        'title' =>      $reserve->title,
                        'url' =>        'calendar/edit-reserve/' . $reserve->id,
                        'start' =>      strtotime($reserve->reserve_start),
                        'end' =>        strtotime($reserve->reserve_end),
                        'textColor' =>  \Config::get('app.calendar.textColor'),
                        'color' =>      \Config::get('app.calendar.reserve_color'),
                    );
                }
            }
        }
    }
}