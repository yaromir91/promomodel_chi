<?php

namespace Acme\Providers;

use Acme\Models\Profile\ProfileParams;
use Acme\Models\Repositories\ProfileParamsRepository;
use Illuminate\Support\ServiceProvider;

/**
 *
 * Class SearchApplicantServiceProvider
 *
 * @package Acme\Providers
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class SearchApplicantServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $result = [];


    public function register()
    {
        $this->app->bind('SearchApplicantServiceProvider', function($app, $data)
        {
            foreach($data['params'] as $param){
                switch ($param->search_type){
                    case 1:
                        $this->sliderHandler($param);
                        break;
                    case 2:
                        $this->checkboxHandler($param);
                        break;
                }
            }
            return $this->result;
        });
    }

    /**
     * @param $param
     *
     * return array()
     */
    protected function sliderHandler($param)
    {
        $profile_param = ProfileParams::$relationship;
        $max = $profile_param[$param->code][0];
        $min = $profile_param[$param->code][1];
        $step = $profile_param[$param->code][2];
        $pip_step = $profile_param[$param->code][3];
        $this->result[$param->id] = [$min, $max, $step, $pip_step];
    }

    /**
     * @param $param
     *
     * return array()
     */
    protected function checkboxHandler($param)
    {
        $this->result[$param->id] = ProfileParamsRepository::getParrentParams($param->id);
    }
}