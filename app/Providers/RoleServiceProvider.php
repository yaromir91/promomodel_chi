<?php

namespace Acme\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RoleServiceProvider
 * @package Acme\Providers
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class RoleServiceProvider extends ServiceProvider
{

    public function register()
    {
    }
}