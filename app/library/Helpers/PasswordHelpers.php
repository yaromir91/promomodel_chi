<?php

namespace Acme\Helpers;

/**
 * Class PasswordHelpers
 *
 * @package Acme\Helpers
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class PasswordHelpers
{

    /**
     * @param int $length
     *
     * @return string
     */
    static public function generate_password($length = 8){

        $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
            '0123456789``-=~!@#$%^&*()_+,./<>?;:[]{}\|';

        $str = '';
        $max = strlen($chars) - 1;

        for ($i=0; $i < $length; $i++)
            $str .= $chars[mt_rand(0, $max)];

        return $str;
    }
}