<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user',         'Acme\Models\User');
Route::model('comment',      'Acme\Models\Comment');
//Route::model('post',         'Acme\Models\Post');
Route::model('role',         'Acme\Models\Role');
Route::model('param',        'Acme\Models\Profile\ProfileParams');
Route::model('event',        'Acme\Models\Events\Events');
Route::model('gallery',      'Acme\Models\Gallery');
Route::model('group',        'Acme\Models\Groups');
Route::model('profession',   'Acme\Models\Professions\Professions');
Route::model('eventtype',    'Acme\Models\Events\EventsType');
Route::model('event',        'Acme\Models\Events\Events');
Route::model('reserve',      'Acme\Models\UserReserves');
Route::model('gallery',      'Acme\Models\Gallery');
Route::model('image',        'Acme\Models\GalleryImages');
Route::model('groupcomment', 'Acme\Models\GroupComments');
Route::model('group',        'Acme\Models\Groups');
Route::model('report',       'Acme\Models\Post');
Route::model('invite',       'Acme\Models\Events\EventInvite');
Route::model('eventcomment', 'Acme\Models\EventComments');
Route::model('notification', 'Acme\Models\Notifications');
Route::model('chat',         'Acme\Models\Chat\UserChats');
Route::model('chatMes',      'Acme\Models\Chat\UserChatMessages');
Route::model('payment',      'Acme\Models\Payments\UserApplicantsPayments');
Route::model('partner',     'Acme\Models\Partners');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('event',   '[0-9]+');
Route::pattern('post',    '[0-9]+');
Route::pattern('user',    '[0-9]+');
Route::pattern('role',    '[0-9]+');
Route::pattern('token',   '[0-9a-z]+');
Route::pattern('status',  '[0-1]+');
Route::pattern('manager', '[0-1]+');
Route::pattern('invite',  '[0-9]+');
Route::pattern('report',  '[0-9]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    # Comment Management
    Route::get('comments/{comment}/edit',    'Acme\Controllers\Admin\AdminCommentsController@getEdit');
    Route::post('comments/{comment}/edit',   'Acme\Controllers\Admin\AdminCommentsController@postEdit');
    Route::get('comments/{comment}/delete',  'Acme\Controllers\Admin\AdminCommentsController@getDelete');
    Route::post('comments/{comment}/delete', 'Acme\Controllers\Admin\AdminCommentsController@postDelete');
    Route::controller('comments',            'Acme\Controllers\Admin\AdminCommentsController');


    # User Management
    Route::get('users/settings-list',  'Acme\Controllers\Admin\AdminUsersController@getSettingsList');
    Route::get('users/{user}/show',    'Acme\Controllers\Admin\AdminUsersController@getShow');
    Route::get('users/{user}/edit',    'Acme\Controllers\Admin\AdminUsersController@getEdit');
    Route::post('users/{user}/edit',   'Acme\Controllers\Admin\AdminUsersController@postEdit');
    Route::get('users/{user}/delete',  'Acme\Controllers\Admin\AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'Acme\Controllers\Admin\AdminUsersController@postDelete');
    Route::controller('users',         'Acme\Controllers\Admin\AdminUsersController');

    # Payments
    Route::get('payments',                  'Acme\Controllers\Admin\AdminPaymentsController@getList');
    Route::get('payments/{event}/users',    'Acme\Controllers\Admin\AdminPaymentsController@getUsers');
    Route::post('payments/paid/{payment}',  'Acme\Controllers\Admin\AdminPaymentsController@postAjaxSetPaid');
    Route::get('payments/{event}/confirm',  'Acme\Controllers\Admin\AdminPaymentsController@getConfirmPayment');
    Route::get('payments/{event}/cancel',   'Acme\Controllers\Admin\AdminPaymentsController@getCancelPayment');
    Route::post('payments/confirm',         'Acme\Controllers\Admin\AdminPaymentsController@sendConfirmRequest');
    Route::post('payments/cancel',          'Acme\Controllers\Admin\AdminPaymentsController@sendCancelRequest');
    Route::controller('payments',           'Acme\Controllers\Admin\AdminPaymentsController');

    # User Role Management
    Route::get('roles/{role}/show',    'Acme\Controllers\Admin\AdminRolesController@getShow');
    Route::get('roles/{role}/edit',    'Acme\Controllers\Admin\AdminRolesController@getEdit');
    Route::post('roles/{role}/edit',   'Acme\Controllers\Admin\AdminRolesController@postEdit');
    Route::get('roles/{role}/delete',  'Acme\Controllers\Admin\AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'Acme\Controllers\Admin\AdminRolesController@postDelete');
    Route::controller('roles',         'Acme\Controllers\Admin\AdminRolesController');

    # Params Management
    Route::get('params/setorder',          'Acme\Controllers\Admin\AdminProfileParamsController@getSetorder');
    Route::get('params/create/{param}',    'Acme\Controllers\Admin\AdminProfileParamsController@getCreate');
    Route::get('params/{param}/datachild', 'Acme\Controllers\Admin\AdminProfileParamsController@getDataChild');
    Route::get('params/{param}/show',      'Acme\Controllers\Admin\AdminProfileParamsController@getShow');
    Route::get('params/{param}/edit',      'Acme\Controllers\Admin\AdminProfileParamsController@getEdit');
    Route::post('params/{param}/edit',     'Acme\Controllers\Admin\AdminProfileParamsController@postEdit');
    Route::get('params/{param}/delete',    'Acme\Controllers\Admin\AdminProfileParamsController@getDelete');
    Route::post('params/{param}/delete',   'Acme\Controllers\Admin\AdminProfileParamsController@postDelete');
    Route::controller('params',            'Acme\Controllers\Admin\AdminProfileParamsController');

    # Professions Management
    Route::get('profession/{profession}/delete',  'Acme\Controllers\Admin\AdminProfessionController@getDelete');
    Route::post('profession/{profession}/delete', 'Acme\Controllers\Admin\AdminProfessionController@postDelete');
    Route::get('profession/{profession}/edit',    'Acme\Controllers\Admin\AdminProfessionController@getEdit');
    Route::post('profession/{profession}/edit',   'Acme\Controllers\Admin\AdminProfessionController@postEdit');
    Route::controller('profession',               'Acme\Controllers\Admin\AdminProfessionController');

    # Events types
    Route::get('events-type/{eventtype}/delete',  'Acme\Controllers\Admin\AdminEventTypesController@getDelete');
    Route::post('events-type/{eventtype}/delete', 'Acme\Controllers\Admin\AdminEventTypesController@postDelete');
    Route::get('events-type/{eventtype}/edit',    'Acme\Controllers\Admin\AdminEventTypesController@getEdit');
    Route::post('events-type/{eventtype}/edit',   'Acme\Controllers\Admin\AdminEventTypesController@postEdit');
    Route::controller('events-type',              'Acme\Controllers\Admin\AdminEventTypesController');

    # Events
    Route::get('events/{event}/delete',           'Acme\Controllers\Admin\AdminEventsController@getDelete');
    Route::post('events/{event}/delete',          'Acme\Controllers\Admin\AdminEventsController@postDelete');
    Route::get('events/{event}/edit',             'Acme\Controllers\Admin\AdminEventsController@getEdit');
    Route::get('events/{event}/team',             'Acme\Controllers\Admin\AdminEventsController@getTeamData');
    Route::post('events/{event}/edit',            'Acme\Controllers\Admin\AdminEventsController@postEdit');
    Route::controller('events',                   'Acme\Controllers\Admin\AdminEventsController');

    // Groups
    Route::get('groups/{group}/delete',           'Acme\Controllers\Admin\AdminGroupsController@getDelete');
    Route::post('groups/{group}/delete',          'Acme\Controllers\Admin\AdminGroupsController@postDelete');
    Route::get('groups/{group}/edit',             'Acme\Controllers\Admin\AdminGroupsController@getEdit');
    Route::post('groups/{group}/edit',            'Acme\Controllers\Admin\AdminGroupsController@postEdit');
    Route::controller('groups',                   'Acme\Controllers\Admin\AdminGroupsController');

    # Agent Management
    Route::get('agents/{user}/show',               'Acme\Controllers\Admin\AdminAgentsController@getShow');
    Route::get('agents/{user}/activate',           'Acme\Controllers\Admin\AdminAgentsController@getActivate');
    Route::post('agents/{user}/activate',          'Acme\Controllers\Admin\AdminAgentsController@postActivate');
    Route::get('agents/{user}/deactivate',         'Acme\Controllers\Admin\AdminAgentsController@getDeactivate');
    Route::post('agents/{user}/deactivate',        'Acme\Controllers\Admin\AdminAgentsController@postDeactivate');
    Route::get('agents/{user}/edit',               'Acme\Controllers\Admin\AdminUsersController@getEdit');
    Route::post('agents/{user}/edit',              'Acme\Controllers\Admin\AdminUsersController@postEdit');
    Route::get('agents/{user}/delete',             'Acme\Controllers\Admin\AdminUsersController@getDelete');
    Route::post('agents/{user}/delete',            'Acme\Controllers\Admin\AdminUsersController@postDelete');
    Route::controller('agents',                     'Acme\Controllers\Admin\AdminAgentsController');

    # Galleries
    Route::post('gallery/js-remove-photo/{gallery}', 'Acme\Controllers\Admin\AdminGalleryController@postJSRemoveImage');
    Route::post('gallery/js-add-photo/{gallery}',    'Acme\Controllers\Admin\AdminGalleryController@postJSAddImage');
    Route::get('gallery/searchtag',                  'Acme\Controllers\Admin\AdminGalleryController@getSearchtag');
    Route::get('gallery/{gallery}/dataimg',          'Acme\Controllers\Admin\AdminGalleryController@getDataimg');
    Route::get('gallery/{gallery}/addphotos',        'Acme\Controllers\Admin\AdminGalleryController@getCreateimg');
    Route::get('gallery/{gallery}/edit',             'Acme\Controllers\Admin\AdminGalleryController@getEdit');
    Route::post('gallery/{gallery}/edit',            'Acme\Controllers\Admin\AdminGalleryController@postEdit');

    Route::get('gallery/{gallery}/delete',           'Acme\Controllers\Admin\AdminGalleryController@getDelete');
    Route::post('gallery/{gallery}/delete',          'Acme\Controllers\Admin\AdminGalleryController@postDelete');

    Route::post('gallery/{image}/deleteimg',         'Acme\Controllers\Admin\AdminGalleryController@postDeleteImg');
    Route::get('gallery/{image}/deleteimg',          'Acme\Controllers\Admin\AdminGalleryController@getDeleteImg');

    Route::controller('gallery',                     'Acme\Controllers\Admin\AdminGalleryController');

    # Reports

    Route::get('reports/create',                            'Acme\Controllers\Admin\AdminReportsController@getCreate');
    Route::post('reports/create',                           'Acme\Controllers\Admin\AdminReportsController@postCreate');

    Route::get('reports/edit/{report}',                     'Acme\Controllers\Admin\AdminReportsController@getEdit');
    Route::post('reports/edit/{report}',                    'Acme\Controllers\Admin\AdminReportsController@postEdit');

    Route::get('reports/delete/{report}',                   'Acme\Controllers\Admin\AdminReportsController@getDelete');
    Route::post('reports/delete/{report}',                  'Acme\Controllers\Admin\AdminReportsController@postDelete');

    Route::get('reports/comments/{report}',                 'Acme\Controllers\Admin\AdminReportsController@getDataComments');

    Route::get('reports/comments/edit/{comment}',           'Acme\Controllers\Admin\AdminReportsController@getCommentEdit');
    Route::post('reports/comments/edit/{comment}',          'Acme\Controllers\Admin\AdminReportsController@postCommentEdit');

    Route::get('reports/comments/create/{report}',          'Acme\Controllers\Admin\AdminReportsController@getCommentCreate');
    Route::post('reports/comments/create',                  'Acme\Controllers\Admin\AdminReportsController@postCommentCreate');

    Route::get('reports/comments/delete/{comment}',         'Acme\Controllers\Admin\AdminReportsController@getCommentDelete');
    Route::post('reports/comments/delete/{comment}',        'Acme\Controllers\Admin\AdminReportsController@postCommentDelete');

    Route::controller('reports',                            'Acme\Controllers\Admin\AdminReportsController');
    
    // Partners
    Route::get('partners/{partner}/delete',    'Acme\Controllers\Admin\AdminPartnersController@getDelete');
    Route::post('partners/{partner}/delete',   'Acme\Controllers\Admin\AdminPartnersController@postDelete');
    Route::get('partners/{partner}/edit',      'Acme\Controllers\Admin\AdminPartnersController@getEdit');
    Route::post('partners/{partner}/edit',     'Acme\Controllers\Admin\AdminPartnersController@postEdit');
    Route::controller('partners',              'Acme\Controllers\Admin\AdminPartnersController');

    # Admin Dashboard
    Route::controller('/', 'Acme\Controllers\Admin\AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

// User reset routes
Route::get('user/reset/{token}',  'Acme\Controllers\User\UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'Acme\Controllers\User\UserController@postReset');
//:: User Account Routes ::
Route::post('user/{user}/edit',   'Acme\Controllers\User\UserController@postEdit');
//:: User Account Routes ::
Route::post('user/login',         array('uses' => 'Acme\Controllers\User\UserController@postLogin', 'as' => 'main_site_login'));
# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'Acme\Controllers\User\UserController');

//:: Application Routes ::

# Profile route
Route::get('profile/show',                   array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ProfileController@showProfile', 'as' => 'get_profile'));
Route::get('profile',                        array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ProfileController@showProfileData', 'as' => 'edit_profile'));
Route::post('profile/upload',                array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ProfileController@postUploadPhoto'));
Route::post('profile/{user}/edit',           'Acme\Controllers\User\ProfileController@postEdit');
Route::post('profile/{user}/edit-applicant', 'Acme\Controllers\User\ProfileController@postEditApplicant');
Route::post('profile/{user}/edit-organizer', 'Acme\Controllers\User\ProfileController@postEditOrganizer');
Route::get('profile/view/{user}',            array('uses' => 'Acme\Controllers\User\ProfileController@getView', 'as' => 'view_user_profile'));
Route::post('profile/rate',                  'Acme\Controllers\User\ProfileController@postRate');

# Users list
Route::get('users', array('uses' => 'Acme\Controllers\User\UserController@showUsers'));

# Filter for detect language
Route::when('contact-us','detectLang');

# Contact Us Static Page
Route::get('contact-us', function()
{
    // Return about us page
    return View::make('site/contact-us');
});

Route::get('chat/list',                             array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ChatController@getList', 'as' => 'chat_list'));
Route::post('chat/{chat}/get',                      array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ChatController@getData', 'as' => 'chat_get_msg'));
Route::post('chat/{chatMes}/set-read-msg',          array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ChatController@setRead', 'as' => 'msg-set-read'));
Route::post('chat/{chatMes}/delete',                array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ChatController@postDelete', 'as' => 'chat_del_msg'));
Route::post('chat/{chat}/send-message',             array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ChatController@postMessage', 'as' => 'post_chat_msg'));
Route::get('chat/{chat}',                           array('before' => 'auth', 'uses' => 'Acme\Controllers\User\ChatController@getIndex', 'as' => 'users_chat'));

# Events
/* ========================== EVENTS routes ========================== */
Route::get('events',                                    array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@index'));
Route::post('events/upload',                            array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@postUploadPreview'));
Route::get('events/past',                               array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@past'));
Route::get('events/my',                                 array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@myEvents', 'as' => 'own_user_events'));
Route::get('events/my-invitation',                      array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@myInvitation', 'as' => 'event_my_invitation'));
Route::get('events/create',                             array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@getCreate'));
Route::post('events/create',                            array('uses' => 'Acme\Controllers\EventsController@postCreate'));
Route::get('events/show/{event}',                       array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@getShow', 'as' => 'show_events'));
Route::get('events/edit/{event}',                       array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@getEdit'));
Route::post('events/edit/{event}',                      array('uses' => 'Acme\Controllers\EventsController@update'));
Route::get('events/{event}/search',                     array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@getSearch', 'as' => 'get_search_event_users'));
Route::post('events/{event}/search',                    array('uses' => 'Acme\Controllers\EventsController@postSearch'));
Route::get('events/delete/{event}',                     array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@delete'));
Route::get('events/cancel/{event}',                     array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@cancel'));
Route::get('events/invite/{event}',                     array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@invite'));
Route::post('events/invite/{event}',                    array('uses' => 'Acme\Controllers\EventsController@postInvite'));
Route::get('events/team/{event}',                       array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@team', 'as' => 'eventTeam'));
Route::get('events/team-member-delete/{invite}',        array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@deleteTeamMember', 'as' => 'event_delete_team_member'));
Route::get('events/team-member-move/{invite}',          array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@moveTeamMember', 'as' => 'event_move_team_member'));
Route::get('events/team-manager-move/{invite}',          array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@moveManagerMember', 'as' => 'event_move_manager_member'));
Route::post('events/rate',                              array('uses' => 'Acme\Controllers\EventsController@postAddRating'));
Route::post('events/rate_team',                         array('uses' => 'Acme\Controllers\EventsController@postAddTeamRating'));
Route::get('events/invitation/{event}',                 array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@invitation', 'as' => 'event_invitation_list'));
Route::get('events/accept/{event}/{status}/{manager}',  array('before' => 'auth', 'uses' => 'Acme\Controllers\EventsController@setAccept', 'as' => 'event_accept_set'));
Route::post('events/change/status/owner',               array('uses' => 'Acme\Controllers\EventsController@setStatusOwner', 'as' => 'event_change_status_owner'));
Route::post('events/show/status/owner',                 array('uses' => 'Acme\Controllers\EventsController@getStatusOwner', 'as' => 'event_show_status_owner'));

Route::post('events/comment/create/{event}',            array('uses' => 'Acme\Controllers\EventsController@getCreateComment', 'as' => 'event_create_comment'));
Route::post('events/comment/edit/{eventcomment}',       array('uses' => 'Acme\Controllers\EventsController@postCommentEdit')); //Ajax edit
Route::get('events/comment/delete/{eventcomment}',      array('uses' => 'Acme\Controllers\EventsController@getCommentDelete')); //Ajax delete

Route::any('events/checkOrder',                        array('uses' => 'Acme\Controllers\EventsController@checkOrder'));
Route::any('events/paymentAviso',                      array('uses' => 'Acme\Controllers\EventsController@paymentAviso'));

Route::any('events/testCheckOrder',                    array('uses' => 'Acme\Controllers\EventsController@checkOrder'));
Route::any('events/testPaymentAviso',                  array('uses' => 'Acme\Controllers\EventsController@paymentAviso'));
/* ========================== ./EVENTS routes ========================== */

# Organizers
Route::get('organizers/online',  array('uses' => 'Acme\Controllers\OrganizersController@getOnline', 'as' => 'show_online_organizers'));
Route::get('organizers',         array('uses' => 'Acme\Controllers\OrganizersController@index'));

# Applicants
Route::post('applicants/search', array('uses' => 'Acme\Controllers\ApplicantsController@postSearch' ,'as' => 'search_applicant'));
Route::get('applicants/search',  array('uses' => 'Acme\Controllers\ApplicantsController@getSearch', 'as' => 'show_current_applicant'));
Route::get('applicants/online',  array('uses' => 'Acme\Controllers\ApplicantsController@getOnline'));
Route::get('applicants',         array('uses' => 'Acme\Controllers\ApplicantsController@index'));


# Agents
Route::get('agents', array('uses' => 'Acme\Controllers\AgentsController@getIndex'));
Route::get('agents/register', array('uses' => 'Acme\Controllers\AgentsController@getRegisterForm'));
Route::post('agents/register', array('uses' => 'Acme\Controllers\AgentsController@postAddAgent'));
Route::get('agents/applicants/list', array('before' => 'auth', 'uses' => 'Acme\Controllers\AgentsController@getList' ,'as' => 'agent_applicants_list'));
Route::get('agents/invite/{user}', array('uses' => 'Acme\Controllers\AgentsController@getInvite' ,'as' => 'invite_user_to_agent'));
Route::get('agents/add', array('uses' => 'Acme\Controllers\AgentsController@getCreate'));
Route::post('agents/add', array('uses' => 'Acme\Controllers\AgentsController@postCreate'));
Route::get('agents/user/list', array('uses' => 'Acme\Controllers\AgentsController@getUserInvitationsList', 'as' => 'user_invitation_list'));
Route::get('agents/user/accept/{user}', array('uses' => 'Acme\Controllers\AgentsController@getUserInvitationAccept'));
Route::get('agents/user/reject/{user}', array('uses' => 'Acme\Controllers\AgentsController@getUserInvitationReject'));
Route::get('agents/user/relogin/{user}', array('uses' => 'Acme\Controllers\AgentsController@getRelogin', 'as' => 'relogin'));

# Gallery
/* ========================== GALLERY routes ========================== */
Route::get('gallery',                                     array('uses' => 'Acme\Controllers\GalleryController@getPublicGalleriesList'));
Route::get('gallery/show/{gallery}',                      array('uses' => 'Acme\Controllers\GalleryController@getShowAlbum'));

Route::post('gallery/upload-preview',                     array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@postUploadPreview'));
Route::get('gallery/filter/{filter}',                     array('uses' => 'Acme\Controllers\GalleryController@getPublicGalleriesListFilter'));
Route::get('gallery/personal/album',                      array('uses' => 'Acme\Controllers\GalleryController@getPersonalGalleriesList', 'as' => 'gallery_personal_album'));
Route::get('gallery/albums/{user}',                       array('uses' => 'Acme\Controllers\GalleryController@getAlbums', 'as' => 'user_albums'));
Route::get('gallery/personal/album/add',                  array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@getAddPersonalAlbum'));
Route::get('gallery/personal/album/show/{gallery}',       array('uses' => 'Acme\Controllers\GalleryController@getShowPersonalAlbum', 'as' => 'show_personal_album_gallery'));
Route::get('gallery/personal/album/edit/{gallery}',       array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@getEditPersonalAlbum'));
Route::get('gallery/personal/album/delete/{gallery}',     array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@getDeletePersonalAlbum'));

Route::get('gallery/personal/photo',                      array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@getPersonalPhotoList', 'as' => 'gallery_personal_photo'));
Route::get('gallery/personal/photo/add/{gallery?}',       array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@getAddPersonalPhoto'));


Route::get('gallery/searchtag',                           array('uses' => 'Acme\Controllers\GalleryController@getSearchtag'));

Route::post('gallery/edit-album/{gallery}',               array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@postEditAlbum'));
Route::post('gallery/create-album',                       array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@postCreateAlbum'));
Route::post('gallery/add-photo',                          array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@postAddImageToAlbum'));
Route::post('gallery/search-albums',                      array('before' => 'auth', 'uses' => 'Acme\Controllers\GalleryController@searchAlbums'));

Route::post('gallery/personal/photo/add/js-add-photo',    array('uses' => 'Acme\Controllers\GalleryController@postJSAddImage')); // js upload
Route::post('gallery/personal/photo/add/js-remove-photo', array('uses' => 'Acme\Controllers\GalleryController@postJSRemoveImage')); // js remove
Route::post('gallery/personal/photo/add/js-cancel',       array('uses' => 'Acme\Controllers\GalleryController@postJSDeleteCancel')); // js remove
Route::post('gallery/personal/delete_images',             array('uses' => 'Acme\Controllers\GalleryController@postJSDeleteImages')); // js remove
Route::post('gallery/personal/photo/rate',                array('uses' => 'Acme\Controllers\GalleryController@postAddRating')); // js add rate
/* ========================== ./GALLERY routes ========================== */


# Groups
Route::get('groups',                                array('uses' => 'Acme\Controllers\GroupsController@index', 'as' => 'groups'));
Route::post('groups/upload',                        array('before' => 'auth', 'uses' => 'Acme\Controllers\GroupsController@postUploadPreview'));
Route::get('groups/subscribed',                     array('uses' => 'Acme\Controllers\GroupsController@index', 'as' => 'my_group'));
Route::get('groups/create',                         array('uses' => 'Acme\Controllers\GroupsController@getCreate', 'as' => 'add_group'));
Route::post('groups/create',                        array('uses' => 'Acme\Controllers\GroupsController@getCreate', 'as' => 'add_group_post'));
Route::get('groups/edit/{group}',                   array('uses' => 'Acme\Controllers\GroupsController@getEdit', 'as' => 'edit_group'));
Route::post('groups/edit/{group}',                  array('uses' => 'Acme\Controllers\GroupsController@postEdit', 'as' => 'edit_group_post'));
Route::get('groups/show/{group}',                   array('uses' => 'Acme\Controllers\GroupsController@getShow', 'as' => 'show_group'));
Route::get('groups/delete/{group}',                 array('uses' => 'Acme\Controllers\GroupsController@destroy', 'as' => 'group_delete'));
Route::get('groups/subscribe/{group}',              array('uses' => 'Acme\Controllers\GroupsController@getSubscribe', 'as' => 'group_subscribe'));
Route::get('groups/unsubscribe/{group}/{user_id?}', array('uses' => 'Acme\Controllers\GroupsController@getUnsubscribe', 'as' => 'group_unsubscribe'));
Route::get('groups/{group}/invite',                 array('uses' => 'Acme\Controllers\GroupsController@getInviteToGroup', 'as' => 'invite_to_group'));
Route::post('groups/{group}/invite',                array('uses' => 'Acme\Controllers\GroupsController@getInviteToGroup', 'as' => 'invite_to_group_post'));
Route::get('groups/my-invitation',                  array('before' => 'auth', 'uses' => 'Acme\Controllers\GroupsController@myInvitation', 'as' => 'group_my_invitation'));
## Invite
Route::post('groups/invite',                        array('uses' => 'Acme\Controllers\GroupsController@getInviteUserToGroup', 'as' => 'invite_user_post'));
# Email Invite
Route::get('groups/invite/success/{token}',         array('uses' => 'Acme\Controllers\GroupsController@getInviteSuccess', 'as' => 'invite_user_get_success'))
    ->where('token', '[a-zA-Z0-9]+');
Route::get('groups/invite/refuse/{token}',          array('uses' => 'Acme\Controllers\GroupsController@getInviteRefuse', 'as' => 'invite_user_get_refuse'))
    ->where('token', '[a-zA-Z0-9]+');
## End invite

# Group Comments

Route::get('groups/comment/upprove/{groupcomment}',   array('before' => 'auth', 'uses' => 'Acme\Controllers\GroupCommentsController@getCommentUpprove')); //Ajax upprove
Route::post('groups/comment/edit/{groupcomment}',   array('before' => 'auth', 'uses' => 'Acme\Controllers\GroupCommentsController@postCommentEdit')); //Ajax edit
Route::get('groups/comment/delete/{groupcomment}',   array('before' => 'auth', 'uses' => 'Acme\Controllers\GroupCommentsController@getCommentDelete')); //Ajax delete

Route::post('groups/create/comment/{group}',               array('before' => 'auth', 'uses' => 'Acme\Controllers\GroupCommentsController@getCreateComment', 'as' => 'group_create_comment'));
Route::post('groups/edit/comment/{groupcomment}',               array('before' => 'auth', 'uses' => 'Acme\Controllers\GroupCommentsController@postEditComment', 'as' => 'group_edit_comment'));
//Route::get('groups/delete/comment/{groupcomment}/{group}', array('uses' => 'Acme\Controllers\GroupCommentsController@deleteComment', 'as' => 'group_delete_comment'));
//Route::post('groups/comment/js-add-photo',                 array('uses' => 'Acme\Controllers\GroupCommentsController@postJSAddImage', 'as' => 'js_comment_add_image'));
//Route::post('groups/comment/js-delete-photo',              array('uses' => 'Acme\Controllers\GroupCommentsController@postJSDeleteImages', 'as' => 'js_comment_delete_image'));
# Members
Route::get('groups/{group}/members/',                      array('before' => 'auth', 'uses' => 'Acme\Controllers\GroupsController@showMembers', 'as' => 'group_members'));
# End Groups


# Calendar
Route::get('calendar',                          array('before' => 'auth', 'uses' => 'Acme\Controllers\CalendarController@index'));
Route::get('calendar/add-reserve',              array('before' => 'auth', 'uses' => 'Acme\Controllers\CalendarController@getAddReserveForm'));
Route::post('calendar/add-reserve',             array('uses' => 'Acme\Controllers\CalendarController@postAddReserve'));
Route::post('calendar/getEvents',               array('uses' => 'Acme\Controllers\CalendarController@getEvents'));
//Route::get('calendar/show-reserve/{reserve}', array('uses' => 'Acme\Controllers\CalendarController@getShowReserve'));
Route::get('calendar/edit-reserve/{reserve}',   array('before' => 'auth', 'uses' => 'Acme\Controllers\CalendarController@getEditReserveForm'));
Route::get('calendar/delete-reserve/{reserve}', array('before' => 'auth', 'uses' => 'Acme\Controllers\CalendarController@getDeleteReserve'));
Route::post('calendar/edit-reserve/{reserve}',  array('uses' => 'Acme\Controllers\CalendarController@postEditReserve'));

# Report
Route::get('report',                 array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@index', 'as' => 'report_list'));
Route::post('report/upload',         array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@postUploadPreview'));
Route::post('report/comment_upload', array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@postUploadCommentPreview'));
Route::get('report/create/{event?}',          array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getCreate'));
Route::post('report/create',         array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@postCreate'));
//Route::get('report/show/{report}', array('uses' => 'Acme\Controllers\ReportController@show'));
Route::get('report/edit/{report}',   array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getEdit'));
Route::post('report/edit/{report}',  array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@postEdit'));
Route::get('report/event/{event}',   array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getEventReports'));
Route::get('report/personal',        array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getPersonalReports', 'as' => 'report_personal_list'));

Route::get('report/delete/{report}',            array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getDelete', 'as' => 'report_delete_report'));
Route::post('report/add_comment/{report}',      array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@reportAddComment'));
Route::get('report/comment/upprove/{comment}',  array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getCommentUpprove')); //Ajax upprove
Route::post('report/comment/edit/{comment}',    array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@postCommentEdit')); //Ajax edit
Route::get('report/comment/delete/{comment}',   array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getCommentDelete')); //Ajax delete

Route::get('report/popular',         array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getPopular'));
//Route::get('report/arhive',          array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getArhive'));
//Route::get('report/tag',             array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getTag'));

Route::get('report/personal/filter/all',            array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getPersonalReports'));
Route::get('report/personal/filter/published',      array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getPersonalPublished'));
Route::get('report/personal/filter/archive',        array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@getPersonalArchive'));

Route::post('report/rate',           array('before' => 'auth', 'uses' => 'Acme\Controllers\ReportController@postAddRating')); // js add rate


############## NOTIFICATION ##################
Route::get('notifications',                             array('before' => 'auth', 'uses' => 'Acme\Controllers\NotificationController@getMyNotifications', 'as' => 'notifications'));
Route::get('notifications/delete/{notification}',       array('before' => 'auth', 'uses' => 'Acme\Controllers\NotificationController@getDeleteNotifications', 'as' => 'notification_delete'));
Route::get('notifications/read/{notification}',         array('before' => 'auth', 'uses' => 'Acme\Controllers\NotificationController@postReadNotifications', 'as' => 'notification_read'));
############## /NOTIFICATION #################

/* ==========PARTNER=========== */
Route::get('partner', array('uses' => 'Acme\Controllers\PartnerController@getList'));
Route::get('partner/{partner}/show', array('uses' => 'Acme\Controllers\PartnerController@getShow'));
Route::post('partner/upload', array('before' => 'auth', 'uses' => 'Acme\Controllers\PartnerController@postUploadLogo'));
/* ==========PARTNER=========== */

############## LOCALIZATION ##################
Route::get('language/{lang}', array('as' => 'language.select', 'uses' => 'Acme\Controllers\LanguageController@select'));
############# ./LOCALIZATION #################

############## STATIC PAGES ##################
Route::get('static/{page?}', array('uses' => 'Acme\Controllers\StaticPageController@getIndex'));
############# ./STATIC PAGES #################

Route::get('search', array('before' => 'auth', 'uses' => 'Acme\Controllers\Search\SearchController@getIndex'));

# Index Page - Last route, no matches
Route::get('/', array('uses' => 'Acme\Controllers\HomeController@index'));

Route::get('fb/choose-role',  array('uses' => 'Acme\Controllers\User\UserController@chooseRole'));
Route::post('fb/choose-role', array('uses' => 'Acme\Controllers\User\UserController@postChooseRole'));
Route::get('fb/login',        array('uses' => 'Acme\Controllers\User\UserController@loginWithFacebook'));

# Posts - Second to last set, match slug
Route::get('report/show/{reportSlug}', array('uses' => 'Acme\Controllers\ReportController@show', 'as' => 'show_report'));

App::bind('confide.user_validator', 'Acme\Validators\RegistrationUserValidation');