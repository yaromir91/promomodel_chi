<?php

namespace Acme\Controllers;

use Acme\Models\Comment;
use Acme\Models\Post;
use Acme\Models\Repositories\ReportRepository;
use Acme\Models\Role;
use Acme\Models\Tags;
use Acme\Models\Events\Events;
use Acme\Models\Ratings;
use \Acme\Models\Repositories\EventsRepository;
use \Acme\Models\Repositories\TagsRepository;
use \Acme\Models\Repositories\RatingsRepository;
use Acme\Models\User;
use Acme\Helpers\PictureHelper;
use Acme\Models\Repositories\EventInviteRepository;

class ReportController extends \BaseController {

    /**
     * Post Model
     * @var Post
     */
    protected $post;

    protected $post_paginate = 12;

    /**
     * Inject the models.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        parent::__construct();
        $this->post = $post;
    }
	/**
	 * Display a listing of the resource.
	 * GET /report
	 *
	 * @return Response
	 */
    public function index()
	{

        $my_events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);

        $post = new Post();
        $ratings_table = with(new Ratings)->getTable();
        $ratings_table = \DB::getTablePrefix() . $ratings_table;

        $reports = $post->
            where('posts.status', 0)->
            where('posts.created_at', '>', date('Y-m-d H:m:s', time() - 31536000))->
            orderBy('created_at', 'DESC')->
            select('posts.*', \DB::raw( '(SELECT AVG( ' . $ratings_table . '.rating ) FROM ' . $ratings_table . ' WHERE ' . $ratings_table . '.object_id = prm_posts.id AND ' . $ratings_table . '.type = "post" ) AS rating' ))->
            paginate($this->post_paginate);

        return \View::make('site/report/index', compact('reports', 'my_events'));
    }

    /**
     * @param \Acme\Models\Events\Events $event
     *
     * @return mixed
     */
    public function getEventReports(Events $event)
    {

        $reports = $this->post->where('status', 0)->where('event_id', $event->id)->orderBy('created_at', 'DESC')->paginate($this->post_paginate);

        $access_to_create = false;
        
        if(\Auth::user()->id == $event->users_id){
            $access_to_create = true;
        }
        if(is_object(EventInviteRepository::checkUserInTeam($event->id, \Auth::user()->id))){
            $access_to_create = true;
        }

        return \View::make('site/report/event_reports', compact('reports', 'event', 'access_to_create'));
    }

	/**
	 * Show the form for creating a new resource.
	 * GET /report/create
	 *
	 * @return Response
	 */
    public function getCreate($event = false)
	{

        $title  = \Lang::get('report.title_create_report');
        $events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);

        $choose = [0 => \Lang::get('report.choose_event')];

        $data = array(
            'events'    => array_replace($choose, $events),
            'event'     => $event
        );


        return \View::make('site/report/create', compact('title', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     * POST /report
     *
     * @return Response
     */
    public function postCreate()
    {

        $events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id, true);

        $rules = array(
            'events'    => 'required|in:' . implode(',', $events),
            'title'     => 'required|min:3',
//            'tags'      => 'required|min:3',
            'content'   => 'required|min:3'
        );

        $data      = \Input::all();
        $validator = \Validator::make($data, $rules);

        $destinationPath        = \Config::get('app.image_dir.posts');
        $destinationSmallPath   = \Config::get('app.image_dir.posts_small');

        if ($validator->passes())
        {
            $name = '';
            if(isset($data['image'])) {
                $file = $data['image'];
                $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
            }

            $post = new Post();
            $post->user_id          = $this->user->id;
            $post->event_id         = \Input::get('events');
            $post->title            = \Input::get('title');
            $post->slug             = \Slug::make(\Input::get('title'));
            $post->content          = \Input::get('content');
            $post->image            = $name;
            $post->status           = (\Input::get('status'))?1:0;

            if($data['new_picture']) {
                $post->image       = $data['new_picture'];
            }

            if($post->save())
            {
                if(isset($data['image'])) {
                    $destinationPath = \Config::get('app.image_dir.posts') . $post->id;
                    if (!file_exists($destinationPath)) {
                        \File::makeDirectory($destinationPath, 0777, true);
                    }
                    $data['image']->move($destinationPath, $name);
                }

                return \Redirect::to('report/show/' . $post->slug)->with('success', \Lang::get('report.messages.create.success'));
            }

            return \Redirect::to('report/create')->withInput()->with('error', \Lang::get('report.messages.create.error'));
        }

        return \Redirect::to('report/create')->withInput()->withErrors($validator);
    }

	/**
	 * Display the specified resource.
	 * GET /report/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function show($reportSlug)
	{

        $this->notAuthRedirect();
        $post = $this->post->
                where('slug', '=', $reportSlug)->
//                where('posts.created_at', '>', date('Y-m-d H:m:s', time() - 31536000))->
                first();

        // Check if the blog post exists
        if (is_null($post)) {
            // If we ended up in here, it means that
            // a page or a blog post didn't exist.
            // So, this means that it is time for
            // 404 error page.
            return \App::abort(404);
        }
        $rating = RatingsRepository::getReportRating($post->id);

        // Get this post comments
        $comments = $post->comments()->orderBy('created_at', 'DESC')->get();

        $title = \Lang::get('report.title_show_report') . ' - ' . $post->title;

        $is_admin = ($this->user->getCurrentUserRole()->code == Role::ADMIN_USER);

        $user = $this->user;

        return \View::make('site/report/show', compact('title', 'post', 'comments', 'user', 'rating', 'is_admin'));
    }

    public function getEdit($post)
    {

        $title = \Lang::get('report.title_edit_report');
        $events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);

        $choose = [0 => \Lang::get('report.choose_event')];

        $tags = TagsRepository::getCurrentPostTags($post->id);

        $data = array(
            'events'    => array_replace($choose, $events),
            'post'      => $post,
            'tags'      => $tags
        );


        return \View::make('site/report/edit', compact('title', 'data'));
    }

    public function postEdit($post)
    {

        $events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id, true);

        $rules = array(
            'events'   => 'required|in:' . implode(',', $events),
            'title'    => 'required|min:3',
//            'tags'     => 'required|min:3',
            'content'  => 'required|min:3'
        );

        $data      = \Input::all();
        $validator = \Validator::make($data, $rules);

        $destinationPath        = \Config::get('app.image_dir.posts');
        $destinationSmallPath   = \Config::get('app.image_dir.posts_small');
        $destinationPrevPath    = \Config::get('app.image_dir.posts_preview');

        if ($validator->passes())
        {
            $name = $post->image;
            if(isset($data['image'])) {
                $file = $data['image'];
                $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
            }
            $old_image = $post->image;

            $post->user_id          = $this->user->id;
            $post->event_id         = \Input::get('events');
            $post->title            = \Input::get('title');
            $post->slug             = \Slug::make(\Input::get('title'));
            $post->content          = \Input::get('content');
            $post->image            = $name;
            $post->status           = (\Input::get('status'))?1:0;

            if($data['new_picture']) {
                if (trim($data['new_picture']) != $old_image) {
                    \File::delete($destinationPath . '/' . $old_image);
                    \File::delete($destinationSmallPath . '/' . $old_image);
                    \File::delete($destinationPrevPath . '/' . $old_image);
                }
                $post->image       = $data['new_picture'];
            }

            if($post->save())
            {
                if(isset($data['image'])) {
                    if (\File::exists($destinationPath)) {
                        \File::delete($destinationPath . $old_image);
                        \File::delete($destinationSmallPath . $old_image);
                        \File::delete($destinationPrevPath . $old_image);
                    }

                    if (!file_exists($destinationPath)) {
                        \File::makeDirectory($destinationPath, 0777, true);
                    }
                    $data['image']->move($destinationPath, $name);
                }

                return \Redirect::to('report/show/' . $post->slug)->with('success', \Lang::get('report.messages.update.success'));
            }

            return \Redirect::to('report/create')->withInput()->with('error', \Lang::get('report.messages.update.error'));
        }

        return \Redirect::to('report/create')->withInput()->withErrors($validator);
    }

    public function getPersonalReports()
    {

        $my_events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);

        $reports = ReportRepository::getPersoanlReports($this->user->id, $this->post_paginate);


        return \View::make('site/report/show_personal', compact('reports', 'my_events'));
    }

    public function getPopular()
    {

        $my_events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);

        $post = new Post();
        $ratings_table = with(new Ratings)->getTable();
        $ratings_table = \DB::getTablePrefix() . $ratings_table;

        $reports = $post->
            where('posts.status', 0)->
            where('posts.created_at', '>', date('Y-m-d H:m:s', time() - 31536000))->
            orderBy('rating', 'DESC')->
            select('posts.*', \DB::raw( '(SELECT AVG( ' . $ratings_table . '.rating ) FROM ' . $ratings_table . ' WHERE ' . $ratings_table . '.object_id = prm_posts.id AND ' . $ratings_table . '.type = "post" ) AS rating' ))->
            paginate($this->post_paginate);

        return \View::make('site/report/getPopular', compact('reports', 'my_events'));
    }

    public function getArhive()
    {

//        $my_events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);
//
//        $posts = $this->post->orderBy('created_at', 'DESC')->paginate($this->post_paginate);
//
//        return \View::make('site/report/getPopular', compact('posts', 'my_events'));
    }

    public function getTag()
    {

        $my_events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);

        $posts = $this->post->orderBy('created_at', 'DESC')->paginate($this->post_paginate);

        return \View::make('site/report/getPopular', compact('posts', 'my_events'));
    }


    public function getPersonalPublished()
    {

        $my_events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);

        $post = new Post();
        $ratings_table = with(new Ratings)->getTable();
        $ratings_table = \DB::getTablePrefix() . $ratings_table;

        $reports = $post->
            where('posts.user_id', $this->user->id)->
            where('posts.status', 0)->
            where('posts.created_at', '>', date('Y-m-d H:m:s', time() - 31536000))->
            orderBy('created_at', 'DESC')->
            select('posts.*', \DB::raw( '(SELECT AVG( ' . $ratings_table . '.rating ) FROM ' . $ratings_table . ' WHERE ' . $ratings_table . '.object_id = prm_posts.id AND ' . $ratings_table . '.type = "post" ) AS rating' ))->
            paginate($this->post_paginate);

        return \View::make('site/report/show_personal', compact('reports', 'my_events'));
    }

    public function getPersonalArchive()
    {

        $my_events = EventInviteRepository::getUserInTeamEventsId(\Auth::user()->id);

        $post = new Post();
        $ratings_table = with(new Ratings)->getTable();
        $ratings_table = \DB::getTablePrefix() . $ratings_table;

        $reports = $post->
            where('posts.user_id', $this->user->id)->
            where('posts.created_at', '<=', date('Y-m-d H:m:s', time() - 31536000))->
            orderBy('created_at', 'DESC')->
            select('posts.*', \DB::raw( '(SELECT AVG( ' . $ratings_table . '.rating ) FROM ' . $ratings_table . ' WHERE ' . $ratings_table . '.object_id = prm_posts.id AND ' . $ratings_table . '.type = "post" ) AS rating' ))->
            paginate($this->post_paginate);


        return \View::make('site/report/show_personal', compact('reports', 'my_events'));
    }

    public function postAddRating()
    {

        $object_rate = Ratings::whereRaw('user_id = ? and object_id = ? and type = "post"', array($this->user->id, \Input::get('id')))->first();

        if($object_rate) {
            $object_rate->user_id = $this->user->id;
            $object_rate->object_id = \Input::get('id');
            $object_rate->rating = \Input::get('rate');
            $object_rate->type = 'post';

            $object_rate->save();
        } else {
            $rate = new Ratings();

            $rate->user_id = $this->user->id;
            $rate->object_id = \Input::get('id');
            $rate->rating = \Input::get('rate');
            $rate->type = 'post';

            $rate->save();
        }

        $avg = round(Ratings::whereRaw('object_id = ? and type = "post"', array(\Input::get('id')))->avg('rating'), 1);

        return json_encode(array('avg' => $avg));
    }

    public function getDelete($report)
    {

        $report->delete();

        return \Redirect::to('report')->with('success', \Lang::get('report.messages.delete.success'));
    }

    public function reportAddComment($report)
    {

        $rules = array(
            'comment'   => 'required'
        );

        // Validate the inputs
        $validator = \Validator::make(\Input::all(), $rules);

        $data      = \Input::all();

        // Check if the form validates with success
        if ($validator->passes())
        {
            $comment = new Comment();

            $name = '';
            if(isset($data['image'])) {
                $file = $data['image'];
                $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
            }

            $comment->user_id   = $this->user->id;
            $comment->post_id   = $report->id;
            $comment->content   = nl2br($data['comment']);
            $comment->image     = $name;

            if($comment->save())
            {
                if(isset($data['image'])) {
                    $destinationPath = \Config::get('app.image_dir.comment') . $comment->id;
                    if (!file_exists($destinationPath)) {
                        \File::makeDirectory($destinationPath, 0777, true);
                    }
                    $data['image']->move($destinationPath, $name);
                }
                return \Redirect::to('report/show/' . $report->slug)->with('success', \Lang::get('report.comment.save_success'));
            }
            return \Redirect::to('report/show/' . $report->slug)->with('error', \Lang::get('report.comment.save_error'));
        }
        return \Redirect::to('report/show/' . $report->slug)->with('error', \Lang::get('report.comment.validation_error'));
    }

    public function getCommentDelete($comment)
    {

        $comment->delete();

        return json_encode('succes');
    }

    public function postCommentEdit($comment)
    {

        $comment->content = nl2br(\Input::get('content'));

        $comment->save();

        return json_encode('succes');
    }

    /**
     * upload event preview
     */
    public function postUploadPreview()
    {
        $data               = \Input::all();
        $fileName           = "";
        $fileSmallPathName  = "";

        $response = [
            'state'   => 404,
            'image'   => '',
            'result'  => null
        ];

        $destinationPath      = \Config::get('app.image_dir.posts');
        $destinationSmallPath = \Config::get('app.image_dir.posts_small');
        $destinationPrevPath  = \Config::get('app.image_dir.posts_preview');

        if (!is_dir($destinationPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationPath, 0777, true);
            umask($oldmask);
        }

        if (!is_dir($destinationSmallPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationSmallPath, 0777, true);
            umask($oldmask);
        }

        if (!is_dir($destinationPrevPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationPrevPath, 0777, true);
            umask($oldmask);
        }

        $file       = isset($data['image']) ? $data['image'] : false;
        $avatarData = isset($data['avatar_data']) ? json_decode(stripslashes($data['avatar_data'])) : null;

        try {
            if($file) {
                $extensionList = \Config::get('app.allowed_extension');
                $extension     = $file->getClientOriginalExtension();

                if (isset($extensionList[$extension]) && ($extensionList[$extension] ==1)) {
                    $fileName = \Str::random(20) . '.' . $extension;
                    $file->move($destinationPath, $fileName);

                    $filePathName        = $destinationPath . '/' . $fileName;
                    $fileSmallPathName   = $destinationSmallPath . '/' . $fileName;
                    $filePreviewPathName = $destinationPrevPath . '/' . $fileName;

                    copy($filePathName, $fileSmallPathName);
                    copy($filePathName, $filePreviewPathName);

                    $cropResult = PictureHelper::crop($filePathName, $fileSmallPathName, $avatarData, $error, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'), \Config::get('app.quality_picture'));

                    $img = \Image::make($filePathName);
                    $img->fit(\Config::get('app.preview_width'), \Config::get('app.preview_height'));
                    $img->save($filePreviewPathName, \Config::get('app.quality_picture'));
                }
            }

            if ($fileSmallPathName) {

                $response = [
                    'state'   => 200,
                    'image'   => $fileName,
                    'result'  => asset($fileSmallPathName)
                ];
            }

        } catch (\Exception $e) {
            $response = [
                'state'   => 404,
                'image'   => '',
                'result'  => null
            ];
        }


        echo json_encode($response);
        die();
    }
}