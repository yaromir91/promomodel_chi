<?php

namespace Acme\Controllers;


/**
 *
 * Class LanguageController
 *
 * @package Acme\Controllers
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class LanguageController extends \BaseController{

    /**
     * Set lang to Session
     *
     * @param $lang
     *
     * @return mixed
     */
    public function select($lang){
        \Session::put('lang', $lang);

        return \Redirect::to('/');
    }
}