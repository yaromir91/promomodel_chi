<?php

namespace Acme\Controllers;

use Acme\Models\Notifications;
use Acme\Models\Repositories\NotificationRepository;

/**
 * Class NotificationController
 * @package Acme\Controllers
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class NotificationController extends \BaseController
{
    public function getMyNotifications()
    {
        $title = \Lang::get('notifications.title');

        $this->notAuthRedirect();

        $notifications = NotificationRepository::getUserNotifications($this->user->id);

        return \View::make('site/notifications/show', compact(
            'title',
            'notifications'
        ));
    }

    public function getDeleteNotifications($notification)
    {
        if($notification instanceof Notifications) {

            $notification->delete();
            return \Redirect::to('notifications')->with('success', \Lang::get('notifications.success_delete'));
        } else {
            return \Redirect::to('notifications')->with('error', \Lang::get('notifications.bad_object'));
        }
    }

    public function postReadNotifications($notification)
    {

        if($notification instanceof Notifications) {
            $notification->is_read = Notifications::IS_READ_ON; // выставляем уведомлению статус прочитано
            $notification->save();
            return \Redirect::to('notifications');
        } else {
            return \Redirect::to('notifications')->with('error', \Lang::get('notifications.bad_object'));
        }
    }
}