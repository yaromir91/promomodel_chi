<?php

namespace Acme\Controllers;
use Acme\Models\Events\Events;
use Acme\Models\Post;
use Acme\Models\Repositories\EventsRepository;
use Acme\Models\Repositories\ReportRepository;
use Acme\Models\Repositories\UserRepository;
use Acme\Models\Role;

/**
 * Class HomeController
 * @package Acme\Controllers
 */
class HomeController  extends \BaseController
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $events      = EventsRepository::getFuturePublicEvents(true, \Config::get('app.count_post_home_page.events'));
        $publicUsers = UserRepository::getPublicUsers(true, \Config::get('app.count_post_home_page.users'));
        $reports     = ReportRepository::getPopular(\Config::get('app.count_post_home_page.reports'));

        $user = \Auth::user();

        if($user && ($user->getCurrentUserRole()->code == Role::APPLICANT_USER || $user->getCurrentUserRole()->code == Role::APPLICANT_MANAGER_USER)) {
            $role = true;
        } else {
            $role = false;
        }

        return \View::make('site.home.index', compact('events', 'publicUsers','reports', 'role'));
    }
}