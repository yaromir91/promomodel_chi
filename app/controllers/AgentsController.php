<?php

namespace Acme\Controllers;

use Acme\Models\Profile\UserProfile;
use Acme\Models\Repositories\RoleRepository;
use Acme\Models\Repositories\UsersAgentInvitationsRepository;
use Acme\Models\User;
use Acme\Models\Role;
use Acme\Models\Permission;

use Acme\Models\Repositories\UserRepository;
use Acme\Models\UsersAgentInvitations;


/**
 *
 * Class AgentsController
 *
 *
 * @property
 *
 * @author Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class AgentsController extends \BaseController
{
    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    function __construct(User $user, Permission $permission)
    {
        parent::__construct();

        $this->user       = $user;
        $this->permission = $permission;
        $this->userRepo   = new UserRepository();
    }

    /**
     * Display a agents list
     * GET /applicants
     *
     * @return Response
     */
    public function getIndex()
    {
        $list = \Acme\Models\Repositories\RoleRepository::getAgents();

        return \View::make('site/agents/index', compact('list'));
    }

    /**
     * Display a listing of the resource.
     * GET /applicants
     *
     * @return Response
     */
    public function getList()
    {
        $agent = \Auth::user();
        $users = UserRepository::getAgentUsers($agent->id);

        $text_color = [
            'null' => 'bg-info',
            UsersAgentInvitations::STATUS_ACCEPTED => 'bg-success',
            UsersAgentInvitations::STATUS_REJECTED => 'bg-danger',
            UsersAgentInvitations::STATUS_SEND => 'bg-info',
        ];

        return \View::make('site/agents/users_list', compact('users', 'text_color'));
    }

    public function getInvite($user)
    {

        $agent = \Auth::user();

        if(!$agent->checkAgentStatus()) {
            return \Redirect::to('applicants')->with('error', \Lang::get('agents.messages_not_agent'));
        }

        $userCodeRole = $user->getMainUserRoleCode();
        if($userCodeRole != Role::APPLICANT_MANAGER_USER && $userCodeRole != Role::APPLICANT_USER) {
            return \Redirect::to('applicants')->with('error', \Lang::get('agents.messages_not_applicant'));
        }

        $status = UsersAgentInvitationsRepository::checkUserInvitationStatus($user, $agent);

        if($status == null) {
            UsersAgentInvitationsRepository::sendInvitation($user, $agent);

            $profile        = $user->profile;
            $agentProfile   = $agent->profile;

            \Queue::push('\Acme\Queues\SendAgentInviteMessageQueue', [
                'email'        => $user->email,
                'username'     => $profile->getFullName(),
                'agentName'    => $agentProfile->getFullName(),
                'agentId'      => $agent->id,
            ], 'emails');
        }

        return \Redirect::to('applicants')->with('success', \Lang::get('agents.messages_invite_success'));
    }

    public function getUserInvitationsList()
    {
        $user = \Auth::user();
        $userCodeRole       = $user->getMainUserRoleCode();
        if($userCodeRole == Role::APPLICANT_MANAGER_USER || $userCodeRole == Role::APPLICANT_USER) {
            $agentsInvitations  = UsersAgentInvitationsRepository::getUserInvitations($user);

            return \View::make('site/agents/invitations', compact('agentsInvitations'));
        } else {
            return \Redirect::to('profile/show')->with('error', \Lang::get('agents.messages_not_applicant'));
        }

    }

    public function getUserInvitationAccept($agent){

        $user = \Auth::user();

        UsersAgentInvitationsRepository::setAcceptStatus($user, $agent);

        return \Redirect::route('user_invitation_list')->with('success', \Lang::get('agents.messages_accept_invite'));
    }


    public function getUserInvitationReject($agent){

        $user = \Auth::user();

        UsersAgentInvitationsRepository::setRejectStatus($user, $agent);


        return \Redirect::route('user_invitation_list')->with('success', \Lang::get('agents.messages_reject_invite'));
    }

    public function getRelogin($user)
    {
        $agent = \Auth::user();

        if(!UserRepository::checkReloginStatus($user, $agent)) {
            return \Redirect::to('/');
        }
        \Confide::logout();
        \Auth::login($user);
        return \Redirect::to('profile/show');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // All roles
        $roles = RoleRepository::getAvailableRolesForRegistration(UserProfile::getAvailableRolesForAgentReg());

        // Get all the available permissions
        $permissions = $this->permission->all();

        // Selected groups
        $selectedRoles = \Input::old('roles', array());

        // Selected permissions
        $selectedPermissions = \Input::old('permissions', array());

        // Title
        $title = \Lang::get('admin/users/title.create_a_new_user');

        // Mode
        $mode = 'create';

        // Show the page
        return \View::make('site/agents/add_form', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        $confirmed  = (\Input::get('confirm')) ? \Input::get('confirm') : 0;
        $user = \Auth::user();
        $this->user = $this->userRepo->signup(\Input::all(), $confirmed, $user->id);

        if ($this->user->id) {
            $invitation = new UsersAgentInvitations();

            $invitation->user_id        = $this->user->id;
            $invitation->agent_id       = $user->id;
            $invitation->invite_status  = UsersAgentInvitations::STATUS_ACCEPTED;

            $invitation->save();

            $profileSaved = $this->userRepo->addProfileRoleToUser($this->user);
            $this->user->saveRole(\Input::get('account_type', ''));

            if ($profileSaved) {
                if (\Config::get('confide::signup_email')) {
                    $user = $this->user;
                    \Mail::queueOn(
                        \Config::get('confide::email_queue'),
                        \Config::get('confide::email_account_confirmation'),
                        compact('user'),
                        function ($message) use ($user) {
                            $message
                                ->to($user->email, $user->username)
                                ->subject(\Lang::get('confide::confide.email.account_confirmation.subject'));
                        }
                    );
                }

                return \Redirect::to('agents/applicants/list')
                    ->with('success', \Lang::get('admin/users/messages.create.success'));
            }
        }  else {
            $error = $this->user->errors()->all();

            return \Redirect::to('agents/add')
                ->withInput(\Input::except('password'))
                ->with( 'error', $error );
        }

    }

    public function getRegisterForm()
    {
        return \View::make('site/agents/registerForm');
    }

    public function postAddAgent()
    {
        $rules = array(
            'username'                  => 'required|min:3|max:20',
            'last_name'                 => 'required|min:3|max:30',
            'first_name'                => 'required|min:3|max:30',
            'gender'                    => 'required',
            'email'                     => 'required|email',
            'password'                  => 'required|min:8',
            'password_confirmation'     => 'required|same:password'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $this->user = $this->userRepo->signup(\Input::all());
            if ($this->user->id) {
                $profileSaved = $this->userRepo->addProfileRoleToUser($this->user);
                $this->user->saveRole(\Input::get('account_type', ''));

                if ($profileSaved) {
                    if (\Config::get('confide::signup_email')) {
                        $user = $this->user;
                        \Mail::queueOn(
                            \Config::get('confide::email_queue'),
                            \Config::get('confide::email_account_confirmation'),
                            compact('user'),
                            function ($message) use ($user) {
                                $message
                                    ->to($user->email, $user->username)
                                    ->subject(\Lang::get('confide::confide.email.account_confirmation.subject'));
                            }
                        );
                    }
                }
            }
            return \Redirect::to('agents')->withSuccess(\Lang::get('agents.register_succes'));
        }

        return \Redirect::to('agents/register')->withInput()->withErrors($validator);
    }
}