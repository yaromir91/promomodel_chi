<?php

namespace Acme\Controllers;

use Acme\Models\Events\EventsStatusOwner;
use Acme\Models\Events\EventsType;
use Acme\Models\Events\Events;
use Acme\Models\Professions\Professions;
use Acme\Models\Professions\ProfessionsEventsRates;
use Acme\Models\Repositories\EventInviteRepository;
use Acme\Models\Repositories\UserRepository;
use Acme\Models\Role;
use Acme\Models\User;
use Acme\Models\Ratings;
use Acme\Models\Repositories\RatingsRepository;
use \Acme\Models\Repositories\EventsTypeRepository;
use \Acme\Models\Repositories\ProfessionsRepository;
use Acme\Validators\PromoModelValidators;
use Acme\Models\Events\EventInvite;
use Acme\Models\Repositories\EventsRepository;
use Acme\Helpers\PictureHelper;
use Acme\Models\EventComments;
use Acme\Models\Notifications;
use Acme\Models\Repositories\NotificationRepository;
use Acme\Models\Repositories\PaymentsRepository;

/**
 * Class EventsController
 */
class EventsController extends \BaseController {

    /**
     * Event Model
     * @var Event
     */
    protected $event;

    /**
     * Inject the models.
     * @param Events $event
     */
    public function __construct(Events $event)
    {
        parent::__construct();
        $this->event = $event;
    }

    /**
     * Display a listing of the resource.
     * GET /events
     *
     * @return Response
     */
    public function index()
    {
        $futurePublicEvents = EventsRepository::getFuturePublicEvents();

        $userCodeRole       = $this->user->getMainUserRoleCode();
        $isShowInvitation   = ($userCodeRole == Role::APPLICANT_MANAGER_USER || $userCodeRole == Role::APPLICANT_USER);
        $tab                = 1;
        
        return \View::make('site/events/index', compact(
            'futurePublicEvents',
            'tab',
            'isShowInvitation'
        ));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function myEvents()
    {
        $myPublicEvents     = EventsRepository::getMyEvents($this->user);

        $userCodeRole       = $this->user->getMainUserRoleCode();
        $isShowInvitation   = ($userCodeRole == Role::APPLICANT_MANAGER_USER || $userCodeRole == Role::APPLICANT_USER);

        $tab = 3;

        return \View::make('site/events/my_events', compact(
            'tab',
            'myPublicEvents',
            'isShowInvitation'
        ));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function myInvitation()
    {
        $userInvitations    = [];

        $userCodeRole       = $this->user->getMainUserRoleCode();
        $isShowInvitation   = ($userCodeRole == Role::APPLICANT_MANAGER_USER || $userCodeRole == Role::APPLICANT_USER);

        if ($isShowInvitation) {
            $userInvitations    = $this->user->getNewInvitation();
        }

        $tab = 4;

        return \View::make('site/events/my_invitation', compact(
            'tab',
            'userInvitations',
            'isShowInvitation'
        ));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function past()
    {
        $pastPublicEvents   = EventsRepository::getPastPublicEvents();

        $userCodeRole       = $this->user->getMainUserRoleCode();
        $isShowInvitation   = ($userCodeRole == Role::APPLICANT_MANAGER_USER || $userCodeRole == Role::APPLICANT_USER);
        $tab                = 2;

        return \View::make('site/events/past', compact(
            'pastPublicEvents',
            'tab',
            'isShowInvitation'
        ));
    }

    /**
     * @param Events $event
     * @return \Illuminate\View\View
     */
    public function invitation(Events $event)
    {
        $invitation = $event->invites;

        return \View::make('site/events/invitation', compact(
            'event',
            'invitation'
        ));
    }

	/**
	 * Show the form for creating a new resource.
	 * GET /events/create
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        if($this->user->getMainUserRoleCode() != 6) {
            return \Redirect::to('events')->with('error', \Lang::get('events.role_error'));
        }

        //TODO требуется код ревью
        $event_types = EventsType::lists('title', 'id');;

        $professions = Professions::all();

        $title = \Lang::get('events.title_create_event');  //'Создать мероприятие';
        return \View::make('site/events/create', compact('title', 'event_types', 'professions'));
	}

    /**
     * Save the form for creating a new resource.
     * POST /events/create
     *
     * @return Response
     */
    public function postCreate()
    {
        $beforeStart = date('Y-m-d H:i:s', strtotime(' +1 day'));

        $rules = array(
            'title'                 => 'required|min:3',
            'description'           => 'required|min:3|max:571',
            'new_picture'           => 'required',
            'profession'            => 'required|array|profileprof:prof_rate_h,prof_rate_d,count',
            'date_start'            => 'required|date|after:'.$beforeStart,
            'date_end'              => 'required|date|after:date_start',
            'report_date'           => 'required|date|before:date_start|after:'.date('Y-m-d H:i:s'),
            'location'              => 'required|min:3',
            'type'                  => 'required',
            'permission_view'       => 'required',
//            'permission_invite'     => 'required'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $data                           = \Input::all();
            $this->event->users_id          = $this->user->id;
            $this->event->type_id           = \Input::get('type');

            $this->event->title             = \Input::get('title');
            $this->event->description       = \Input::get('description');
            $this->event->image             = (\Input::file('image'))?\Input::file('image')->getClientOriginalName():'';
            $this->event->location          = \Input::get('location');
            $this->event->permission_view   = \Input::get('permission_view');
//            $this->event->permission_invite = \Input::get('permission_invite');
//            $this->event->status            = \Input::get('status');
            $this->event->casting           = \Input::get('casting');
            $this->event->manager_num       = \Input::get('manager_num');
            $this->event->date_start        = date('Y-m-d H:i', strtotime(\Input::get('date_start')));
            $this->event->date_end          = date('Y-m-d H:i', strtotime(\Input::get('date_end')));
            $this->event->report_date       = date('Y-m-d', strtotime(\Input::get('report_date')));

            $this->event->paiment_status    = Events::PAIMENT_STATUS_NEW;

            $destinationPath          = \Config::get('app.image_dir.events');
            if($data['new_picture']) {
                $this->event->image       = $data['new_picture'];
            }

            if(\Input::get('repeat') == '1') {
                $reserve_input = \Input::get('reserve');
                $repeats = \App::make('ReserveServiceProvider', compact('reserve_input'));

                foreach ($repeats as $key=>$repeat) {
                    $this->event->$key = $repeat;
                }
                $this->event->repeat_status = 1;
                $this->event->repeat_text = \Input::get('show_repeat');
                $this->event->repeat_type = \Input::get('reserve')['repeat_type'];
            } else {
                $this->event->repeat_status = 0;
            }

            if($this->event->save())
            {

                //TODO требуется код ревью
                // Insert needs professions
                $professions = \Input::get('profession');
                $counts = \Input::get('count');
                $prof_rate_h = \Input::get('prof_rate_h');
                $prof_rate_d = \Input::get('prof_rate_d');

                foreach($professions as $profession)
                {
                    ProfessionsEventsRates::create(array(
                        'events_id'     => $this->event->id,
                        'professions_id'=> $profession,
                        'count'         => $counts[$profession],
                        'rate_h'        => $prof_rate_h[$profession],
                        'rate_d'        => $prof_rate_d[$profession],
                    ));
                }

                $this->event->getTotalPrice(true, true);

                if(\Input::get('search') === '1')
                {
                    return \Redirect::to('events/' . $this->event->id . '/search')->with('success', \Lang::get('events.form_add_success'));
                }
                else
                {
                    return \Redirect::to('events/show/'.$this->event->id)->with('success', \Lang::get('events.form_add_success'));
                }
            }

            return \Redirect::to('events/create')->with('error', \Lang::get('events.form_add_error'));
        }

        return \Redirect::to('events/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     * GET events/show/{event}
     *
     * TODO требуется код-ревью запросов
     *
     * @param Events $event
     * @return Response
     */
	public function getShow(Events $event)
	{
        if(\Input::get('status')) {
            if(\Input::get('status') == \Acme\Models\Events\Events::STATUS_YA_SUCCESS){
                $paidStatus = ['status' => 'success',  'msg' => \Lang::get('events.msg_05')];
            } else if(\Input::get('status') == \Acme\Models\Events\Events::STATUS_YA_FAILED) {
                $paidStatus = ['status' => 'danger',  'msg' => \Lang::get('events.msg_06')];
            } else {
                $paidStatus = false;
            }
        } else {
            $paidStatus = false;
        }
        
        $title = \Lang::get('events.title_show_event') . ' - ' . $event->title;

        $profession = ProfessionsRepository::getProfessionsRatesWithPlaces($event);

        $event_type = $event->eventType;

        $user = $event->user;

        $current_user = \Auth::user();

        $current_user_check = EventInviteRepository::findInviteForUser($event->id, $current_user->id, EventInvite::INVITE_PROF_STATUS_REJECT);

        $button_template = 'site/events/_partials/_set_buttons/_default';
        
        $member_prof = false;

        if($event->users_id == $current_user->id) {
            if($event->paiment_status == Events::PAIMENT_STATUS_NEW) {
                $button_template = 'site/events/_partials/_set_buttons/_owner_new';
            } else if($event->paiment_status == Events::PAIMENT_STATUS_HOLD || $event->paiment_status == Events::PAIMENT_HOLD_ACCEPT) {
                $button_template = 'site/events/_partials/_set_buttons/_owner_hold';
            } else if($event->paiment_status == Events::PAIMENT_STATUS_PAID) {
                $button_template = 'site/events/_partials/_set_buttons/_owner_paid';
            } else if($event->paiment_status == Events::PAIMENT_STATUS_CANCEL || $event->paiment_status == Events::PAIMENT_HOLD_CANCEL) {
                $button_template = 'site/events/_partials/_set_buttons/_owner_cancel';
            }
        } else if($current_user_check) {
            $button_template = 'site/events/_partials/_set_buttons/_member';
            $member_prof = $current_user_check->title;
        }

        return \View::make('site/events/show', compact('title', 'event', 'profession', 'user', 'current_user', 'event_type', 'button_template', 'paidStatus', 'member_prof'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /events/{id}/edit
	 *
	 * @param  Events $event
	 * @return Response
	 */
    public function getEdit(Events $event)
    {
            if($event->paiment_status != Events::PAIMENT_STATUS_NEW) {
                return \Redirect::to('events/show/'.$event->id)->with('error', \Lang::get('events.event_paid_error'));
            }
            $event_types = EventsTypeRepository::getEventTypes();

            $professions = Professions::all();

            $event_professions = \DB::table('professions_events_rates')
                ->select(array('professions_events_rates.*'))
                ->where('events_id', '=', $event->id)
                ->get();

            foreach($event_professions as $event_profession)
            {
                $data_professions[$event_profession->professions_id] = $event_profession;
            }

            $title = \Lang::get('events.title_edit_event') . ' - ' . $event->title;  //'Создать мероприятие';

            return \View::make('site/events/edit', compact('title', 'event', 'event_types', 'professions', 'data_professions'));
    }

	/**
	 * Update the specified resource in storage.
	 * PUT /events/{id}
	 *
	 * @param  Events  $event
	 * @return Response
	 */
    public function update(Events $event)
    {
        if($event->paiment_status != Events::PAIMENT_STATUS_NEW) {
            return \Redirect::to('events/show/'.$event->id)->with('error', \Lang::get('events.event_paid_error'));
        }

        \Session::forget('profession');
        $rules = array(
            'title'                 => 'required|min:3',
            'description'           => 'required|min:3|max:571'
        );

        if(!count($event->invites)) {
            $beforeStart = date('Y-m-d H:i:s', strtotime(' +1 day'));
            $additional = array(
                'profession'            => 'required|array|profileprof:prof_rate_h,prof_rate_d,count',
                'date_start'            => 'required|date|after:'.$beforeStart,
                'date_end'              => 'required|date|after:date_start',
                'report_date'           => 'required|date|before:date_start|after:'.date('Y-m-d H:i:s'),
                'location'              => 'required|min:3',
                'type'                  => 'required',
                'permission_view'       => 'required',
//                'permission_invite'     => 'required'
            );
            $rules = array_merge($rules, $additional);
        }

        $data      = \Input::all();
        $validator = \Validator::make($data, $rules);


        if ($validator->passes())
        {
            \Session::forget('profession');

            // TODO: вот ЗАЧЕМ сто раз вызывать \Input::get
            $event->title             = \Input::get('title');
            $event->description       = \Input::get('description');

            $oldPicture               = $event->image;
            $destinationPath          = \Config::get('app.image_dir.events');
            $destSmallPath            = \Config::get('app.image_dir.events_small');
            $destPrevPath             = \Config::get('app.image_dir.events_preview');

            if($data['new_picture']) {
                if (trim($data['new_picture']) != $oldPicture) {
                    \File::delete($destinationPath . '/' . $oldPicture);
                    \File::delete($destSmallPath . '/' . $oldPicture);
                    \File::delete($destPrevPath . '/' . $oldPicture);
                }
                $event->image       = trim($data['new_picture']);
            }


            if(!count($event->invites)) {
                $event->type_id           = \Input::get('type');
                $event->location          = \Input::get('location');
                $event->permission_view   = \Input::get('permission_view');
//                $event->permission_invite = \Input::get('permission_invite');
//                $event->status            = \Input::get('status');
                $event->casting           = \Input::get('casting');
                $event->date_start        = date('Y-m-d H:i', strtotime(\Input::get('date_start')));
                $event->date_end          = date('Y-m-d H:i', strtotime(\Input::get('date_end')));
                $event->report_date       = date('Y-m-d', strtotime(\Input::get('report_date')));
                $event->manager_num       = \Input::get('manager_num');
                $event->repeat_end = '0000-00-00 00:00:00';
                $event->repeat_years = '';
                $event->repeat_month = '';
                $event->repeat_weekday = '';
                $event->repeat_day = '';
                $event->repeat_text = '';
                $event->repeat_type = '';

                if(\Input::get('repeat') == '1') {
                    $reserve_input = \Input::get('reserve');
                    $repeats = \App::make('ReserveServiceProvider', compact('reserve_input'));

                    foreach ($repeats as $key=>$repeat) {
                        $event->$key = $repeat;
                    }
                    $event->repeat_text = \Input::get('show_repeat');
                    $event->repeat_type = \Input::get('reserve')['repeat_type'];
                    $event->repeat_status = 1;
                }
                else {
                    $event->repeat_status = 0;
                }
            }

            if($event->save())
            {
                if(!count($event->invites)) {
                    $professions = \Input::get('profession');
                    $counts = \Input::get('count');
                    $prof_rate_h = \Input::get('prof_rate_h');
                    $prof_rate_d = \Input::get('prof_rate_d');

                    \DB::table('professions_events_rates')->where('events_id', '=', $event->id)->delete();

                    foreach($professions as $profession)
                    {
                        ProfessionsEventsRates::create(array(
                            'events_id'     => $event->id,
                            'professions_id'=> $profession,
                            'count'         => $counts[$profession],
                            'rate_h'        => $prof_rate_h[$profession],
                            'rate_d'        => $prof_rate_d[$profession],
                        ));
                    }

                    if ($event->paiment_status == Events::PAIMENT_STATUS_NEW) {
                        $event->getTotalPrice(true, true);
                    }
                }

                $team_users = EventInviteRepository::getEventTeam($event->id);

                foreach($team_users as $us) {
                    NotificationRepository::addNewNotification($event->users_id, $us->users_id,  '<a target="_blank" href="' . \URL::to('events/show/' . $event->id) . '">' . \Lang::get('notifications.msg_14') . '</a> "' . \Lang::get('notifications.msg_15'), Notifications::TYPE_EVENT);
                }

                return \Redirect::to('events/show/'.$event->id)->with('success', \Lang::get('events.event_success_update'));
            }

            return \Redirect::to('events/edit/'.$event->id)->with('error', \Lang::get('events.event_error_update'));
        }

        if(\Input::get('profession') == NULL) {
            \Session::put('profession', true);
        }
        // Form validation failed
        return \Redirect::to('events/edit/' . $event->id)->withInput()->withErrors($validator);
    }

	/**
	 * Remove the specified resource from storage.
	 * DELETE /events/{id}
	 *
	 * @param  Events  $event
	 * @return Response
	 */
    public function delete(Events $event)
    {
        if($event->paiment_status != Events::PAIMENT_STATUS_NEW) {
            return \Redirect::to('events/show/'.$event->id)->with('error', \Lang::get('events.event_paid_error'));
        }

        if($this->user->id != $event->users_id)
        {
            return \Redirect::to('events/show/' . $event->id)->with('error', \Lang::get('events.event_user_error_delete'));
        }

        $team_users = EventInviteRepository::getEventTeam($event->id);

        foreach($team_users as $us) {
            NotificationRepository::addNewNotification($event->users_id, $us->users_id, \Lang::get('notifications.msg_16'), Notifications::TYPE_EVENT);
        }

        $event->delete('\Acme\Models\Repositories\EventInviteRepository::sendLettersAboutDeleteEventToMembers');

        return \Redirect::to('events')->with('success', \Lang::get('events.event_success_delete'));
    }

    /**
     * Send message to admin
     * CANCEL /events/{id}
     *
     * @param  Events  $event
     * @return Response
     */
    public function cancel(Events $event)
    {

        if($this->user->id != $event->users_id)
        {
            return \Redirect::to('events/show/' . $event->id)->with('error', \Lang::get('events.event_user_error_delete'));
        }

        $team_users = EventInviteRepository::getEventTeam($event->id);

        foreach($team_users as $us) {
            NotificationRepository::addNewNotification($event->users_id, $us->users_id, \Lang::get('notifications.msg_16'), Notifications::TYPE_EVENT);
        }

        $event->paiment_status = Events::PAIMENT_STATUS_CANCEL;
        $event->save();

        \Queue::push('\Acme\Queues\SendAdminToCancelQueue', [
            'email'        => \Config::get('app.admin_email'),
            'eventId'      => $event->id,
            'organizeId'   => $event->users_id,
            'eventTitle'   => $event->title,
            'organizeName' => $this->user->profile->first_name,
        ], 'emails');

        return \Redirect::to('events/show/' . $event->id)->with('success', \Lang::get('events.event_success_cancel'));
    }

    /**
     * @param $event
     * @return mixed
     */
    public function invite(Events $event)
    {
        $title = \Lang::get('events.title_event_invite') . ' - ' . $event->title;

        return \View::make('site/events/invite', compact('title'));
    }


    /**
     * @param Events $event
     */
    public function postInvite(Events $event)
    {
        $data   = \Input::all();

        $result = [
            'title'   => \Lang::get('events.Error'),
            'status'  => 0,
            'message' => '',
            'info'    => '',
            'manager'    => false,
        ];

        $validator = PromoModelValidators::getEventInvitationValidator($data);

        if ($validator->passes()) {
            $invite      = $event->updateOrCreateInvite($data);

            if (isset($data['professions_id'])) {
                $professions = Professions::find($data['professions_id']);

                if ($professions instanceof Professions) {
                    $this->sendInviteMessage($data['user_id'], $event, $professions);

                    // регистрация уведомления соискателю, о приглашении на мероприятие соискателем
                    NotificationRepository::addNewNotification($event->users_id, $data['user_id'], \Lang::get('notifications.msg_07') . ' <a target="_blank" href="' . \URL::to('events/show/' . $event->id) . '">' . \Lang::get('notifications.msg_02') . '</a>', Notifications::TYPE_EVENT_INVITE);
                }
            }

            if (isset($data['is_manager'])) {
                $this->sendManagerInvitation($data['user_id'], $event);

                // регистрация уведомления соискателю, о приглашении на мероприятие менеджером
                NotificationRepository::addNewNotification($event->users_id, $data['user_id'], \Lang::get('notifications.msg_07') . ' <a target="_blank" href="' . \URL::to('events/show/' . $event->id) . '">' . \Lang::get('notifications.msg_02') . '</a> ' . \Lang::get('notifications.msg_08'), Notifications::TYPE_EVENT_INVITE);
            }

            $result['status']  = 1;
            $result['info']    = \Lang::get('events.invitation_sent');
            $result['title']   = \Lang::get('events.Successfull');
            $result['message'] = \Lang::get('events.invite_was_successfully_sent');
            if(isset($data['is_manager']) && $data['is_manager'] == 'on'){
                $result['manager']  = true;
            }
        } else {
            $result['message'] = \Lang::get('events.Select_any_prof_or_manager');
        }

        echo(json_encode($result));
        die();
    }


    /**
     * @param $userId
     * @param $event
     */
    private function sendManagerInvitation($userId, $event)
    {
        $user = User::find($userId);

        if ($user) {
            $profile   = $user->profile;
            $organizer = $event->user;

            \Queue::push('\Acme\Queues\SendManagerInvitationQueue', [
                'email'        => $user->email,
                'username'     => $profile->getFullName(),
                'eventId'      => $event->id,
                'organizeId'   => $event->users_id,
                'eventTitle'   => $event->title,
                'organizeName' => is_object($organizer) ? $organizer->getUserName() : '',
            ], 'emails');
        }
    }

    /**
     * @param $userId
     * @param $event
     * @param $professions
     */
    private function sendInviteMessage($userId, $event, $professions)
    {
        $user = User::find($userId);

        if ($user) {
            $profile   = $user->profile;
            $organizer = $event->user;

            if ($profile) {
                \Queue::push('\Acme\Queues\SendInviteMessageQueue', [
                    'email'        => $user->email,
                    'username'     => $profile->getFullName(),
                    'eventId'      => $event->id,
                    'organizeId'   => $event->users_id,
                    'eventTitle'   => $event->title,
                    'organizeName' => is_object($organizer) ? $organizer->getUserName() : '',
                    'profession'   => is_object($professions) ? $professions->title : '',
                ], 'emails');
            }

        }

    }

    /**
     * @param $event
     * @return mixed
     */
    public function team(Events $event)
    {
        $this->notAuthRedirect();

        $title        = \Lang::get('events.title_event_team');
        $mainTeam     = $event->getMainTeam();
        $reservedTeam = $event->getReservedTeam();
        $authUser     = $this->user;

        $comments = $event->comments()->orderBy('created_at', 'DESC')->get();
        $is_admin = ($this->user->getCurrentUserRole()->code == Role::ADMIN_USER);
        $user = $this->user;

        return \View::make('site/events/team', compact('title', 'mainTeam', 'reservedTeam', 'event', 'authUser', 'user', 'is_admin', 'comments'));
    }

    /**
     * @param Groups $group
     * @return $this
     */
    public function getCreateComment(Events $event)
    {
        $this->notAuthRedirect();

        $rules = array(
            'comment'   => 'required'
        );

        // Validate the inputs
        $validator = \Validator::make(\Input::all(), $rules);

        $data      = \Input::all();


        // Check if the form validates with success
        if ($validator->passes())
        {
            $comment = new EventComments();

            $name = '';
            if(isset($data['image']) && $data['image']) {
                $file = $data['image'];
                $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
            }

            $comment->user_id   = $this->user->id;
            $comment->events_id = $event->id;
            $comment->content   = nl2br($data['comment']);
            $comment->image     = $name;

            if($comment->save())
            {
                if(isset($data['image'])) {
                    $destinationPath = \Config::get('app.image_dir.comment') . $comment->id;
                    if (!file_exists($destinationPath)) {
                        \File::makeDirectory($destinationPath, 0777, true);
                    }
                    $data['image']->move($destinationPath, $name);
                }
                return \Redirect::to('events/team/' . $event->id)->with('success', \Lang::get('report.comment.save_success'));
            }
            return \Redirect::to('events/team/' . $event->id)->with('error', \Lang::get('report.comment.save_error'));
        }
        return \Redirect::to('events/team/' . $event->id)->with('error', \Lang::get('report.comment.validation_error'));
    }

    public function getCommentDelete($comment)
    {

        $this->notAuthRedirect();

        $comment->delete();

        return json_encode('succes');
    }

    public function postCommentEdit($comment)
    {
        $this->notAuthRedirect();

        $comment->content = nl2br(\Input::get('content'));

        $comment->save();

        return json_encode('succes');
    }

    /**
     * @param EventInvite $invite
     *
     * @return string
     */
    public function deleteTeamMember(EventInvite $invite)
    {
        $this->notAuthRedirect();

        $result = ['status' => 0, 'message' => ''];
        $event  = $invite->event;

        if ($event instanceof Events) {
            if ($event->users_id == $this->user->id) {

                $this->sendLettersAboutDeleteTeamMember($event, $invite);
                PaymentsRepository::deletePayments($event->id, $invite->users_id, $invite->professions_id);
                $invite->delete();

                $result['status']  = 1;
                $result['message'] = \Lang::get('events.team_member_deleted');
            } else {
                $result['message'] = \Lang::get('events.not_permission');
            }
        } else {
            $result['message'] = \Lang::get('events.event_not_found');
        }

        echo(json_encode($result));
        die();
    }

    public function moveTeamMember(EventInvite $invite)
    {
        $this->notAuthRedirect();

        $invite->in_team = EventInvite::IS_IN_TEAM;
        $invite->save();

        return \Redirect::to('events/team/'.$invite->events_id)->with('success', \Lang::get('events.move_team_member_success'));
    }

    public function moveManagerMember(EventInvite $invite)
    {
        $this->notAuthRedirect();

        $invite->in_team = EventInvite::IS_IN_TEAM;
        $invite->manager_in_team = EventInvite::IS_MANAGER_IN_TEAM;
        $invite->save();

        return \Redirect::to('events/team/'.$invite->events_id)->with('success', \Lang::get('events.move_manager_member_success'));
    }

    /**
     * @return string
     */
    public function postAddRating()
    {
        $this->notAuthRedirect();

        $objectId   = \Input::get('id', '');
        $rating     = \Input::get('rate', 0);
        $ratingType = \Input::get('type_rate', Ratings::RATING_TYPE_USER);

        RatingsRepository::updateOrCreateRatingObject($ratingType, $this->user->id, $objectId, $rating);

        $event = Events::find(\Input::get('event_id'));
        if($event) {
            $avg_team = $event->getEventTeamRating();
        } else {
            $avg_team = 0;
        }
        $avg        = RatingsRepository::getAvgRating($ratingType, $objectId);

        return json_encode(array('avg' => $avg, 'avg_team' => $avg_team));
    }

    public function postAddTeamRating()
    {
        $this->notAuthRedirect();

        $objectId   = \Input::get('id', '');
        $rating     = \Input::get('rate', 0);

        $event = Events::find($objectId);

        foreach($event->getMainTeam() as $user_invite) {
            RatingsRepository::updateOrCreateRatingObject(Ratings::RATING_TYPE_USER, $this->user->id, $user_invite->users_id, $rating);
        }

        return json_encode(array('avg' => $rating));
    }

    /**
     * @param Events $event
     * @return mixed
     */
    public function getSearch(Events $event)
    {
        $title  = \Lang::get('events.title_event_search') . ' - ' . $event->title;

        $role   = Role::getApplicantRole();
        $params = $role->getSearchParamsForCurrentRole();
        $ranges = \App::make('SearchApplicantServiceProvider', compact('params'));

        $data   = [
            'params' => $params,
            'ranges' => $ranges,
        ];

        return \View::make('site/events/search_form', compact('title', 'event', 'data'));
    }

    /**
     * @param Events $event
     * @return \Illuminate\View\View
     */
    public function postSearch(Events $event)
    {
        $search_params = \Input::all()['search_params'];

        if ($search_params) {
            $result = \App::make('PMSearchServiceProvider', compact('search_params', 'event'));
        }

        return \View::make('site/user/search_user', compact('result', 'event'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function setStatusOwner()
    {

        $event_id = \Input::get('id');
        $status = \Input::get('status');
        $rules = [
            'id' => 'required|numeric',
            'status' => 'required|in:' . Events::PRESENCE_OWNER_FALSE . ',' . Events::PRESENCE_OWNER_TRUE,
            'content' => 'string' . ($status == 0 ? '|required' : ''),
        ];

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->fails()) {
            return \Response::json(['errors' => $validator->getMessageBag()], 500);
        }

        if($status == 0){

            /** @var Events $event*/
            try
            {
                \DB::beginTransaction();

                $event = $this->event;
                $event = $event->find($event_id);

                if($event->users_id == \Auth::id()){

                    $event->presence_owner = $status;

                    $eventStatus = EventsStatusOwner::where(['event_id' => $event_id])
                                                    ->where(['user_id' => $event->users_id])
                                                    ->first();

                    $eventStatus = (is_null($eventStatus) ? new EventsStatusOwner : $eventStatus);

                    $eventStatus->user_id   = $event->users_id;
                    $eventStatus->content   = \Input::get('content');
                    $eventStatus->event_id  = $event->id;
                    $eventStatus->save();

                    $event->save();

                    \DB::commit();
                }

            }catch (\Exception $e){
                \DB::rollback();
            }

            return \Response::json();
        } else {

            $event = $this->event->find($event_id);
            $event->presence_owner = 1;
            $event->save();
            // clear information
            $eventStatus = EventsStatusOwner::where(['event_id' => $event_id])
                ->where(['user_id' => $event->users_id])
                ->first();
            if(!is_null($eventStatus)){
                $eventStatus->delete();
            }

            return \Response::json([], 200);
        }

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatusOwner()
    {
        $event_id = \Input::get('event_id');
        $validator = \Validator::make(\Input::all(),
            [
                'event_id' => 'required|numeric',
            ]);

        if ($validator->fails()) {
            return \Response::json(['errors' => $validator->getMessageBag()], 500);
        }

        $eventStatus = EventsStatusOwner::where(['event_id' => $event_id]);
        $eventStatus->orderBy('created_at', 'desc');
        $data = $eventStatus->first();

        return \Response::json($data);
    }


    /**
     * @param Events $event
     * @param $status
     * @param $manager
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setAccept(Events $event, $status, $manager)
    {
        $manager_flag  = false;
        $message       = \Lang::get('events.not_accepted_action');

        $invite = $event->checkUserInvites($this->user->id);

        if ($invite instanceof EventInvite) {

            $invite->setStatusWithData($status, $manager);
            $invite->save();

            $this->sendInvitationLetters($event, $invite, $status, $manager);

            if ($status) {
                $event->addUserToTeam($invite, $message);
                $message = $message . \Lang::get('events.warning');
            } else if(!$status && $manager) {
                $message = \Lang::get('events.you_not_accepted_manager_event') . ' ' . $event->title;
                $manager_flag = true;
            } else {
                $event->getFromReservedInTeam($invite);
                PaymentsRepository::deletePayments($event->id, $invite->users_id, $invite->professions_id);
                $message = \Lang::get('events.you_not_accepted_event') . ' ' . $event->title;
            }
        }

        return \Response::json([
            'message'        => $message,
            'manager'        => $manager_flag,
        ]);

//        echo(json_encode([
//            'message'        => $message,
//            'manager'        => $manager_flag,
//        ]));
//        die();
    }

    /**
     * @param Events      $event
     * @param EventInvite $invite
     * @param $status
     * @param $manager
     */
    private function sendInvitationLetters(Events $event, EventInvite $invite, $status, $manager)
    {
        if ($status) {
            $event->sendToOrganizerAcceptInvitationReport($invite, $manager);
        } else {
            $event->sendToOrganizerRejectedReport($invite, $manager);
        }
    }

    /**
     * @param Events $event
     * @param EventInvite $invite
     */
    private function sendLettersAboutDeleteTeamMember(Events $event, EventInvite $invite)
    {
        $user         = $this->user;
        $teamMember   = $invite->user;
        $siteUrl      = \Config::get('app.url');

        if (($user instanceof User) && ($teamMember instanceof User)) {

            \Queue::push('\Acme\Queues\SendAboutDeleteTeamMemberMessageQueue', [
                'email'            => $teamMember->email,
                'username'         => $teamMember->getUserName(),
                'eventId'          => $event->id,
                'url'              => $siteUrl,
                'teamMember'       => $teamMember->getUserName(),
                'eventTitle'       => $event->title
            ], 'emails');
        }
    }

    /**
     * upload event preview
     */
    public function postUploadPreview()
    {
        $data               = \Input::all();
        $user               = $this->user;
        $fileName           = "";
        $fileSmallPathName  = "";

        $response = [
            'state'   => 404,
            'image'   => '',
            'result'  => null
        ];

        $destinationPath      = \Config::get('app.image_dir.events');
        $destinationSmallPath = \Config::get('app.image_dir.events_small');
        $destinationPrevPath  = \Config::get('app.image_dir.events_preview');

        if (!is_dir($destinationPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationPath, 0777, true);
            umask($oldmask);
        }

        if (!is_dir($destinationSmallPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationSmallPath, 0777, true);
            umask($oldmask);
        }

        if (!is_dir($destinationPrevPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationPrevPath, 0777, true);
            umask($oldmask);
        }

        $file       = isset($data['image']) ? $data['image'] : false;
        $avatarData = isset($data['avatar_data']) ? json_decode(stripslashes($data['avatar_data'])) : null;

        try {

            if ($file) {

                $extensionList = \Config::get('app.allowed_extension');
                $extension     = $file->getClientOriginalExtension();

                if (isset($extensionList[$extension]) && ($extensionList[$extension] ==1)) {
                    $fileName = \Str::random(20).'.'.$extension;
                    $file->move($destinationPath, $fileName);

                    $filePathName = $destinationPath.$fileName;
                    $fileSmallPathName = $destinationSmallPath.$fileName;
                    $filePreviewPathName = $destinationPrevPath.$fileName;

                    copy($filePathName, $fileSmallPathName);
                    copy($filePathName, $filePreviewPathName);

                    PictureHelper::crop(
                        $filePathName,
                        $fileSmallPathName,
                        $avatarData,
                        $error,
                        \Config::get('app.main_pic_resize_width'),
                        \Config::get('app.main_pic_resize_height'),
                        \Config::get('app.quality_picture')
                    );

                    $img = \Image::make($filePathName);
                    $img->fit(\Config::get('app.preview_width'), \Config::get('app.preview_height'));
                    $img->save($filePreviewPathName, \Config::get('app.quality_picture'));
                }
            }

            if ($fileSmallPathName) {

                $response = [
                    'state' => 200,
                    'image' => $fileName,
                    'result' => asset($fileSmallPathName)
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'state'   => 404,
                'image'   => '',
                'result'  => null
            ];
        }

        echo json_encode($response);
        die();
    }

    //####################### PROD PAYMENT #######################
    public function checkOrder()
    {
        $message = '';
        if(
            isset($_POST['action']) &&
            isset($_POST['orderSumAmount']) &&
            isset($_POST['orderSumCurrencyPaycash']) &&
            isset($_POST['orderSumBankPaycash']) &&
            isset($_POST['invoiceId']) &&
            isset($_POST['customerNumber'])
        ) {
            $hash = md5($_POST['action'].';'.$_POST['orderSumAmount'].';'.$_POST['orderSumCurrencyPaycash'].';'.$_POST['orderSumBankPaycash'].';'.\Config::get('app.payments.shopId').';'.$_POST['invoiceId'].';'.$_POST['customerNumber'].';'.\Config::get('app.payments.ShopPassword'));
            if (strtolower($hash) != strtolower($_POST['md5'])){
                $message = 'Ошибка в проверке md5';
                $code = 1;
            }
            else {
                $code = 0;
            }
        } else {
            $code = 100;
            $message = 'Ошибка в POST параметрах';
        }


        if(isset($_POST['invoiceId'])) {
            $invoiceId = $_POST['invoiceId'];
        } else {
            $invoiceId = null;
            $message = 'Ошибка в POST параметрах';
        }

        if(isset($_POST['orderSumCurrencyPaycash'])) {
            $orderSumCurrencyPaycash = $_POST['orderSumCurrencyPaycash'];
        } else {
            $orderSumCurrencyPaycash = null;
            $message = 'Ошибка в POST параметрах';
        }

        $string = '<?xml version="1.0" encoding="UTF-8"?>';
        $string .= '<checkOrderResponse performedDatetime="'. date('c') .'" code="'.$code.'"'. ' invoiceId="'. $invoiceId .'" shopId="'. \Config::get('app.payments.shopId') .'" orderSumAmount="' . $orderSumCurrencyPaycash . '" message="' . $message . '"/>';
        return \Response::make($string, 200, ['Content-Type'=> 'application/xml']);
    }
    
    public function paymentAviso()
    {
        if(
            isset($_POST['action']) &&
            isset($_POST['orderSumAmount']) &&
            isset($_POST['orderSumCurrencyPaycash']) &&
            isset($_POST['orderSumBankPaycash']) &&
            isset($_POST['invoiceId']) &&
            isset($_POST['customerNumber'])
        ) {
            $hash = md5($_POST['action'].';'.$_POST['orderSumAmount'].';'.$_POST['orderSumCurrencyPaycash'].';'.$_POST['orderSumBankPaycash'].';'.\Config::get('app.payments.shopId').';'.$_POST['invoiceId'].';'.$_POST['customerNumber'].';'.\Config::get('app.payments.ShopPassword'));
            if (strtolower($hash) != strtolower($_POST['md5'])){
                $code = 1;
            }
            else {
                if(isset($_POST['eventId']) && $_POST['eventId']) {
                    $event = Events::find($_POST['eventId']);

                    $event->paiment_status = Events::PAIMENT_STATUS_HOLD;
                    $event->currency = $_POST['orderSumCurrencyPaycash'];
                    $event->Invoiceid = $_POST['invoiceId'];
                    $event->save();
                    $code = 0;
                } else {
                    $code = 100;
                }
            }
        } else {
            $code = 100;
        }

        if(isset($_POST['invoiceId'])) {
            $invoiceId = $_POST['invoiceId'];
        } else {
            $invoiceId = null;
        }

        $string = '<?xml version="1.0" encoding="UTF-8"?>';
        $string .= '<paymentAvisoResponse performedDatetime="'. date('c') .'" code="'.$code.'" invoiceId="'. $invoiceId .'" shopId="'. \Config::get('app.payments.shopId') .'"/>';
        return \Response::make($string, 200, ['Content-Type'=> 'application/xml']);
    }
    //####################### ./PROD PAYMENT #######################


    //####################### TEST PAYMENT #######################
    public function testCheckOrder()
    {
        $message = '';
        if(
            isset($_POST['action']) &&
            isset($_POST['orderSumAmount']) &&
            isset($_POST['orderSumCurrencyPaycash']) &&
            isset($_POST['orderSumBankPaycash']) &&
            isset($_POST['invoiceId']) &&
            isset($_POST['customerNumber'])
        ) {
            $hash = md5($_POST['action'].';'.$_POST['orderSumAmount'].';'.$_POST['orderSumCurrencyPaycash'].';'.$_POST['orderSumBankPaycash'].';'.\Config::get('app.payments.shopId').';'.$_POST['invoiceId'].';'.$_POST['customerNumber'].';'.\Config::get('app.payments.ShopPassword'));
            if (strtolower($hash) != strtolower($_POST['md5'])){
                $message = 'Ошибка в проверке md5';
                $code = 1;
            }
            else {
                if(isset($_POST['eventId']) && $_POST['eventId']) {
                    $event = Events::find($_POST['eventId']);

                    $event->paiment_status = Events::PAIMENT_STATUS_HOLD;
                    $event->save();
                    $code = 0;
                } else {
                    $code = 100;
                    $message = 'Ошибка в проверке мероприятия для оплаты';
                }

            }
        } else {
            $code = 100;
            $message = 'Ошибка в POST параметрах';
        }


        if(isset($_POST['invoiceId'])) {
            $invoiceId = $_POST['invoiceId'];
        } else {
            $invoiceId = null;
            $message = 'Ошибка в POST параметрах';
        }

        if(isset($_POST['orderSumCurrencyPaycash'])) {
            $orderSumCurrencyPaycash = $_POST['orderSumCurrencyPaycash'];
        } else {
            $orderSumCurrencyPaycash = null;
            $message = 'Ошибка в POST параметрах';
        }

        $string = '<?xml version="1.0" encoding="UTF-8"?>';
        $string .= '<checkOrderResponse performedDatetime="'. date('c') .'" code="'.$code.'"'. ' invoiceId="'. $invoiceId .'" shopId="'. \Config::get('app.payments.shopId') .'" orderSumAmount="' . $orderSumCurrencyPaycash . '" message="' . $message . '"/>';
        return \Response::make($string, 200, ['Content-Type'=> 'application/xml']);
    }
    
    public function testPaymentAviso()
    {
        if(
            isset($_POST['action']) &&
            isset($_POST['orderSumAmount']) &&
            isset($_POST['orderSumCurrencyPaycash']) &&
            isset($_POST['orderSumBankPaycash']) &&
            isset($_POST['invoiceId']) &&
            isset($_POST['customerNumber'])
        ) {
            $hash = md5($_POST['action'].';'.$_POST['orderSumAmount'].';'.$_POST['orderSumCurrencyPaycash'].';'.$_POST['orderSumBankPaycash'].';'.\Config::get('app.payments.shopId').';'.$_POST['invoiceId'].';'.$_POST['customerNumber'].';'.\Config::get('app.payments.ShopPassword'));
            if (strtolower($hash) != strtolower($_POST['md5'])){
                $code = 1;
            }
            else {
                if(isset($_POST['eventId']) && $_POST['eventId']) {
                    $event = Events::find($_POST['eventId']);

                    $event->paiment_status = Events::PAIMENT_STATUS_HOLD;
                    $event->currency = $_POST['orderSumCurrencyPaycash'];
                    $event->save();
                    $code = 0;
                } else {
                    $code = 100;
                }
            }
        } else {
            $code = 100;
        }

        if(isset($_POST['invoiceId'])) {
            $invoiceId = $_POST['invoiceId'];
        } else {
            $invoiceId = null;
        }

        $string = '<?xml version="1.0" encoding="UTF-8"?>';
        $string .= '<paymentAvisoResponse performedDatetime="'. date('c') .'" code="'.$code.'" invoiceId="'. $invoiceId .'" shopId="'. \Config::get('app.payments.shopId') .'"/>';
        return \Response::make($string, 200, ['Content-Type'=> 'application/xml']);

    }
    //####################### ./TEST PAYMENT #######################

}