<?php
namespace Acme\Controllers;
use Acme\Models\Comment;
use Acme\Models\GroupCommentImages;
use Acme\Models\GroupComments;
use Acme\Models\Groups;

/**
 * Class GroupCommentsController
 */
class GroupCommentsController extends \BaseController
{
    /**
     * @param Groups $group
     * @return $this
     */
    public function getCreateComment(Groups $group)
    {

        $rules = array(
            'comment'   => 'required'
        );

        // Validate the inputs
        $validator = \Validator::make(\Input::all(), $rules);

        $data      = \Input::all();


        // Check if the form validates with success
        if ($validator->passes())
        {
            $comment = new GroupComments();

            $name = '';
            if(isset($data['image']) && $data['image']) {
                $file = $data['image'];
                $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
            }

            $comment->user_id   = $this->user->id;
            $comment->groups_id = $group->id;
            $comment->content   = nl2br($data['comment']);
            $comment->image     = $name;

            if($comment->save())
            {
                if(isset($data['image'])) {
                    $destinationPath = \Config::get('app.image_dir.comment') . $comment->id;
                    if (!file_exists($destinationPath)) {
                        \File::makeDirectory($destinationPath, 0777, true);
                    }
                    $data['image']->move($destinationPath, $name);
                }
                return \Redirect::route('show_group', [$group->id, '#comments'])->with('success', \Lang::get('report.comment.save_success'));
            }
            return \Redirect::route('show_group', [$group->id, '#comments'])->with('error', \Lang::get('report.comment.save_error'));
        }
        return \Redirect::route('show_group', [$group->id, '#comments'])->with('error', \Lang::get('report.comment.validation_error'));
    }

    public function postEditComment(GroupComments $comment)
    {

        $rules = array(
            'comment'   => 'required'
        );

        // Validate the inputs
        $validator = \Validator::make(\Input::all(), $rules);

        $data = \Input::all();


        // Check if the form validates with success
        if ($validator->passes())
        {

            $comment->content   = nl2br($data['comment']);
            if(isset($data['image']) && $data['image']) {

                $image = \Config::get('app.image_dir.comment') . $comment->id . '/' . $comment->image;

                if (\File::exists($image)) {
                    \File::delete($image);
                }

                $file = $data['image'];
                $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
                $comment->image     = $name;
            }

            if($comment->save())
            {
                if(isset($data['image'])) {
                    $destinationPath = \Config::get('app.image_dir.comment') . $comment->id;
                    if (!file_exists($destinationPath)) {
                        \File::makeDirectory($destinationPath, 0777, true);
                    }
                    $data['image']->move($destinationPath, $name);
                }
                return \Redirect::route('show_group', [$comment->groups_id, '#comments'])->with('success', \Lang::get('report.comment.save_success'));
            }
            return \Redirect::route('show_group', [$comment->groups_id, '#comments'])->with('error', \Lang::get('report.comment.save_error'));
        }
        return \Redirect::route('show_group', [$comment->groups_id, '#comments'])->with('error', \Lang::get('report.comment.validation_error'));
    }

    public function getCommentDelete($comment)
    {

        $comment->delete();

        return json_encode('succes');
    }

    // TODO Unused method, delete after testing
//    public function postCommentEdit($comment)
//    {
//        $data = \Input::all();
//
//        $comment->content = nl2br($data['content']);
//
//        if(isset($data['image']) && $data['image']) {
//
//            $file = $data['image'];
//            $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
//
//            $destinationPath = \Config::get('app.image_dir.comment') . $comment->id;
//            if (!file_exists($destinationPath)) {
//                \File::makeDirectory($destinationPath, 0777, true);
//            }
//
//            $image = \Config::get('app.image_dir.comment') . $comment->id . '/' . $comment->image;
//
//            if (\File::exists($image)) {
//                \File::delete($image);
//            }
//
//            $data['image']->move($destinationPath, $name);
//            $comment->image = $name;
//        }
//
//        $comment->save();
//
//        return json_encode('succes');
//    }
}