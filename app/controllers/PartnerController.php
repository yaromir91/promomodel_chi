<?php

namespace Acme\Controllers;

use Acme\Helpers\PictureHelper;
/**
 *
 * Class GalleryController
 *
 *
 * @property
 *
 * @author Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class PartnerController extends \BaseController {


    /**
     * Inject the models.
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     *
     * @return mixed
     */
    public function getList()
    {
        $partners = \Acme\Models\Partners::orderBy('title', 'ASC')
            ->where('active', \Acme\Models\Partners::ACTIVE_YES)
            ->paginate(\Config::get('app.partners_pagination_list'));

        return \View::make('site/partner/list', compact('partners'));
    }

    public function getShow($partner)
    {
        $title = \Lang::get('partners.title');
        return \View::make('site/partner/show', compact('partner', 'title'));
    }
    
    /**
     * upload partners logo
     */
    public function postUploadLogo()
    {
        $data         		= \Input::all();
        $fileName     		= "";
        $fileSmallPathName	= "";

        $response = [
            'state'   => 404,
            'image'   => '',
            'result'  => null
        ];

        $destinationPath      = \Config::get('app.image_dir.partners');
        $destinationSmallPath = \Config::get('app.image_dir.partners_small');
        $destinationPrevPath  = \Config::get('app.image_dir.partners_preview');

        if (!is_dir($destinationPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationPath, 0777, true);
            umask($oldmask);
        }

        if (!is_dir($destinationSmallPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationSmallPath, 0777, true);
            umask($oldmask);
        }

        if (!is_dir($destinationPrevPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationPrevPath, 0777, true);
            umask($oldmask);
        }

        $file       = isset($data['logo']) ? $data['logo'] : false;
        $avatarData = isset($data['avatar_data']) ? json_decode(stripslashes($data['avatar_data'])) : null;

        try {
            if($file) {
                $extensionList = \Config::get('app.allowed_extension');
                $extension     = $file->getClientOriginalExtension();

                if (isset($extensionList[$extension]) && ($extensionList[$extension] ==1)) {
                    $fileName = \Str::random(20) . '.' . $extension;
                    $file->move($destinationPath, $fileName);

                    $filePathName        = $destinationPath . '/' . $fileName;
                    $fileSmallPathName   = $destinationSmallPath . '/' . $fileName;
                    $filePreviewPathName = $destinationPrevPath . '/' . $fileName;

                    copy($filePathName, $fileSmallPathName);
                    copy($filePathName, $filePreviewPathName);

                    $cropResult = PictureHelper::crop($filePathName, $fileSmallPathName, $avatarData, $error, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'), \Config::get('app.quality_picture'));

                    $img = \Image::make($filePathName);
                    $img->fit(\Config::get('app.preview_width'), \Config::get('app.preview_height'));
                    $img->save($filePreviewPathName, \Config::get('app.quality_picture'));
                }
            }

            if ($fileSmallPathName) {

                $response = [
                    'state'   => 200,
                    'image'   => $fileName,
                    'result'  => asset($fileSmallPathName)
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'state'   => 404,
                'image'   => '',
                'result'  => null
            ];
        }

        echo json_encode($response);
        die();
    }
}