<?php

namespace Acme\Controllers;

use Acme\Models\Role;
use Acme\Models\User;
use Acme\Models\Profile\UserProfile;

/**
 * Class ApplicantsController
 *
 * @package Acme\Models\Controllers
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ApplicantsController extends \BaseController {

    /**
     * @return \Illuminate\View\View
     */
    public function index()
	{
        $list = \Acme\Models\Repositories\RoleRepository::getUsersByRoleCode([Role::APPLICANT_USER, Role::APPLICANT_MANAGER_USER]);
        return \View::make('site/applicants/index', compact('list'));
	}


    /**
     * @return \Illuminate\View\View
     */
    public function getOnline()
    {
        $title  = \Lang::get('applicants.online_users');
        $result = $this->getOnlineUsers([Role::APPLICANT_USER, Role::APPLICANT_MANAGER_USER]);

        return \View::make('site/applicants/online_users', compact(
                'title',
                'result'
            ));
    }

    public function postSearch()
    {
        $event  = null;
        $data   = \Input::all();
        $result = null;
        $title  = \Lang::get('applicants.search_result');

        if (isset($data['search_params'])) {
            $search_params = $data['search_params'];
            $result        = \App::make('PMSearchServiceProvider', compact('search_params', 'event'));
        }

        return \View::make('site/applicants/search_results', compact(
                'title',
                'result'
            ));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getSearch()
    {
        if (!$this->isAuthorized()) {
            return \Redirect::to('applicants')->with('error', \Lang::get('applicants.auth_error_search'));
        }

        $role          = Role::getApplicantRole();
        $params        = $role->getSearchParamsForCurrentRole();
        $ranges        = \App::make('SearchApplicantServiceProvider', compact('params'));
        $title         = \Lang::get('applicants.search');

        return \View::make('site/applicants/search', compact(
                'params',
                'result',
                'title',
                'ranges'
            ));
    }

}