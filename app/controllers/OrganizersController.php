<?php

namespace Acme\Controllers;

use Acme\Models\Role;

/**
 * Class OrganizersController
 *
 * @package Acme\Models\Controllers
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class OrganizersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /organizers
	 *
	 * @return Response
	 */
	public function index()
	{
        $list = \Acme\Models\Repositories\RoleRepository::getUsersByRoleCode(array(Role::ORGANIZER_USER));
        return \View::make('site/organizers/index', compact('list'));
	}

    /**
     * @return \Illuminate\View\View
     */
    public function getOnline()
    {
        $title  = \Lang::get('organizers.online_users');
        $result = $this->getOnlineUsers([Role::ORGANIZER_USER]);

        return \View::make('site/organizers/online_users', compact(
                'title',
                'result'
            ));
    }


}