<?php

namespace Acme\Controllers\User;

use Acme\Models\Events\EventInvite;
use Acme\Models\Repositories\UsersAgentInvitationsRepository;
use Acme\Models\Repositories\ChatRepository;
use Acme\Models\User;
use Acme\Models\Role;

use Acme\Models\Repositories\ReportRepository;
use Acme\Models\Repositories\EventsTypeRepository;
use Acme\Models\Repositories\EventInviteRepository;
use Acme\Models\Repositories\ProfessionsRepository;
use Acme\Models\Repositories\EventsRepository;
use Acme\Models\Repositories\UserRepository;
use Acme\Models\Repositories\GalleryImagesRepository;
use Acme\Models\Ratings;
use \Acme\Models\Repositories\RatingsRepository;

use Acme\Helpers\MainHelper;
use Acme\Validators\Profile\ProfileValidators;


/**
 * Class ProfileController
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ProfileController extends \BaseController
{

    const PICTURES_IN_PREVIEW = 11;

    /**
     * @var array
     */
    private $profileForm = [
        Role::ADMIN_USER             => 'site/profile/index',
        Role::AGENT_USER             => 'site/profile/index',
        Role::APPLICANT_USER         => 'site/profile/applicant/form',
        Role::APPLICANT_MANAGER_USER => 'site/profile/applicant/form',
        Role::ORGANIZER_USER         => 'site/profile/organizer/form',
    ];

    /**
     * @var string
     */
    private $defaultTemplate = 'site/profile/index';


    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * Inject the models.
     * @param User $user
     * @param UserRepository $userRepo
     */
    public function __construct(User $user, UserRepository $userRepo)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showProfile() {

        $title = \Lang::get('profile.title');

        $user               = $this->user;

        $userCodeRole       = $this->user->getMainUserRoleCode();
        $isShowInvitLink    = ($userCodeRole == Role::APPLICANT_MANAGER_USER || $userCodeRole == Role::APPLICANT_USER);

        $profile        = $user->profile;
        $gallery        = GalleryImagesRepository::getUserAlbums($user->id, 4);
        $images         = GalleryImagesRepository::getImagesOfUser($user->id, 4);
        $role           = $user->getCurrentUserRole();
        $roleCode       = $user->getMainUserRoleCode();
        $eventsType     = EventsTypeRepository::getEvensTypes();
        $professions    = ProfessionsRepository::getAllProfessions();
        $mainParams     = ($role instanceof Role) ? $role->getProfileParamsForCurrentRole() : [];
        $myPublicEvents = EventsRepository::getMyEvents($this->user, EventInvite::INVITE_PROF_STATUS_ACCEPT, false, 4);
        $invitation     = EventInviteRepository::getUserInvitation($this->user, 4);
        $reports        = ReportRepository::getPersoanlReports($this->user->id, false, 4);
        $groups         = $this->user->groups;

        return \View::make('site/profile/show', compact(
            'title',
            'user',
            'profile',
            'eventsType',
            'professions',
            'roleCode',
            'mainParams',
            'images',
            'isShowInvitLink',
            'myPublicEvents',
            'invitation',
            'reports',
            'groups',
            'gallery'
        ));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showProfileData()
    {
        $user         = \Auth::user();
        $profile      = $user->profile;
        $tab          = \Session::get('tab');
        $tab          = !empty($tab) ? $tab : '1';
        $role         = $user->getCurrentUserRole();

        $htmlTemplate = $this->getHtmlTemplate($this->profileForm, $user->getMainUserRoleCode());

        if (!$htmlTemplate ||
            (
                (
                    $user->getMainUserRoleCode()==Role::APPLICANT_USER ||
                    $user->getMainUserRoleCode()==Role::APPLICANT_MANAGER_USER
                ) &&
                !($profile instanceof \Acme\Models\Profile\UserProfile)
            ) ||
            !($role)) {
            return \Redirect::to('404', 303);
        }

        $eventsType  = EventsTypeRepository::getEvensTypes();
        $professions = ProfessionsRepository::getAllProfessions();
        $mainParams  = $role->getProfileParamsForCurrentRole();

        $data = array(
            'user'        => $user,
            'msg'         => false,
            'profile'     => $profile,
            'eventsType'  => $eventsType,
            'tab'         => $tab,
            'professions' => $professions,
            'mainParams'  => $mainParams
        );

        return \View::make($htmlTemplate, compact('data'));
    }


    /**
     * @param User $user
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postEditApplicant(User $user)
    {
        if (\Request::isMethod('post'))
        {
            $input          = \Input::all();
            $mainparams     = \Input::get('mainparams');
            $additionparams = \Input::get('additionparams');
            $password       = \Input::get('password');

            \Input::flash();

            if (!empty($mainparams)) {
                $validator = ProfileValidators::getApplicantGeneralFormValidator($input, $user->id);

                if ($validator->passes()) {

                    $saved = $user->saveGeneralInfo($input);

                    if ($saved) {
                        return \Redirect::to('profile')->with('success', \Lang::get('profile.profile_was_successfully_saved'));
                    }
                }

                return \Redirect::to('profile')->with('tab', '1')->withErrors($validator);
            }

            if (!empty($additionparams)) {
                $validator = ProfileValidators::getApplicantAdditionFormValidator($input, $user->id);
            
                if ($validator->passes()) {

                    // attach to User Events Type
                    $user->attachEventTypes(!empty($input['eventtypes']) ? $input['eventtypes'] :[]);

                    // attach to User Professionals Rate
                    $user->attachProfessionsToProfile(
                        !empty($input['professions']) ? $input['professions'] :[],
                        !empty($input['prof_rate_h']) ? $input['prof_rate_h'] :[],
                        !empty($input['prof_rate_d']) ? $input['prof_rate_d'] :[]
                    );

                    // fill User Profile Parameters
                    if (!empty($input['profile_params']) && !empty($input['param_type'])) {
                        $user->fillProfileParams($input['profile_params'], $input['param_type']);
                    }

                    \Session::flash('success',  \Lang::get('profile.profile_was_successfully_saved'));
                    return \Redirect::to('profile')->with('tab', '2');
                }

                return \Redirect::to('profile')->with('tab', '2')->withErrors($validator);
            }

            if (!empty($password)) {
                $validator = ProfileValidators::getPasswordFormValidator($input, $user->id, $user->profile->fb_status);

                if ($validator->passes()) {

                    if($user->profile->fb_status == 1) {
                        $correctPassword = true;
                    } else {
                        $correctPassword = \Hash::check(isset($input['old_password']) ? $input['old_password'] : false, $user->password);
                    }

                    if($correctPassword) {
                        if($user->savePassword($input)) {
                            \Session::flash('success',  \Lang::get('profile.password_was_successfully_saved'));
                            $this->userRepo->setFbStatus($user, 0);
                            return \Redirect::to('profile')->with('tab', '3');
                        } else {
                            \Session::flash('error',  \Lang::get('profile.error_in_password_update'));
                            return \Redirect::to('profile')->with('tab', '3');
                        }
                    } else {
                        \Session::flash('error',  \Lang::get('profile.old_password_not_correct'));
                        return \Redirect::to('profile')->with('tab', '3');
                    }
                }

                return \Redirect::to('profile')->with('tab', '3')->withErrors($validator);
            }
        }
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(User $user)
    {
        $oldUser        = clone $user;
        $user->username = \Input::get('username');
        $user->email    = \Input::get('email');

        $password             = \Input::get('password');
        $passwordConfirmation = \Input::get('password_confirmation');

        if(!empty($password)) {
            if($password === $passwordConfirmation) {
                $user->password              = $password;
                $user->password_confirmation = $passwordConfirmation;
            } else {
                    return \Redirect::to('profile')->with('error', \Lang::get('admin/users/messages.password_does_not_match'));
            }
        }

        if($user->confirmed == null) {
                $user->confirmed = $oldUser->confirmed;
            }

        if (!$user->save()) {
                return \Redirect::to('profile')
                    ->with('error', \Lang::get('admin/users/messages.edit.error'));
        }

        $error = $user->errors()->all();

        if(empty($error)) {
                return \Redirect::to('profile')->with('success', \Lang::get('admin/users/messages.edit.success'));
        } else {
                return \Redirect::to('profile')->with('error', \Lang::get('admin/users/messages.edit.error'));
        }
     }

    /**
     * Edits a user
     * TODO: delete this in futhure
     *
     * @var User
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditOrganizer(User $user)
    {

        $input          = \Input::all();
        $mainparams     = \Input::get('mainparams');
        $password       = \Input::get('password');

        \Input::flash();

        if (!empty($mainparams)) {
            $validator = ProfileValidators::getOrganizerGeneralFormValidator($input, $user->id);

            if ($validator->passes()) {

                $saved = $user->saveGeneralInfo($input);

                if ($saved) {
                    return \Redirect::to('profile')->with('success', \Lang::get('profile.profile_was_successfully_saved'));
                }
            }

            return \Redirect::to('profile')->with('tab', '1')->withErrors($validator);
        }

        if (!empty($password)) {
            $validator = ProfileValidators::getPasswordFormValidator($input, $user->id, $user->profile->fb_status);

            if ($validator->passes()) {

                if($user->profile->fb_status == 1) {
                    $correctPassword = true;
                } else {
                    $correctPassword = \Hash::check(isset($input['old_password']) ? $input['old_password'] : false, $user->password);
                }

                if($correctPassword) {
                    if($user->savePassword($input)) {
                        \Session::flash('success',  \Lang::get('profile.password_was_successfully_saved'));
                        $this->userRepo->setFbStatus($user, 0);
                        return \Redirect::to('profile')->with('tab', '2');
                    } else {
                        \Session::flash('error',  \Lang::get('profile.error_in_password_update'));
                        return \Redirect::to('profile')->with('tab', '2');
                    }
                } else {
                    \Session::flash('error',  \Lang::get('profile.old_password_not_correct'));
                    return \Redirect::to('profile')->with('tab', '2');
                }
            }

            return \Redirect::to('profile')->with('tab', '2')->withErrors($validator);
        }
    }


    /**
     * @param $tmpArr
     * @param $roleId
     *
     * @return bool
     */
    protected function getHtmlTemplate($tmpArr, $roleId)
    {
        if (count($tmpArr) && isset($tmpArr[$roleId])) {

            return $tmpArr[$roleId];
        }

        return $this->defaultTemplate;
    }

    /**
     * @param $user
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getView($user)
    {
        if (!($user instanceof User)) {
            return \Redirect::to('404', 303);
        }

        $gallery     = $user->galleries;
        $profile     = $user->profile;
        $role        = $user->getCurrentUserRole();

        $cur_user       = $this->user;
        $rate_read_only = false;
        $chatCode       = false;
        $applicant_code = true;



        if($cur_user){

            $chatCode      = ChatRepository::getUsersChatCode($cur_user->id, $user->id);
            $cur_role_code = $cur_user->getMainUserRoleCode();
            $user_code     = $user->getMainUserRoleCode();

            if($cur_user->id == $user->id || $cur_role_code == \Acme\Models\Role::ORGANIZER_USER || $cur_role_code == \Acme\Models\Role::AGENT_USER) {
                $rate_read_only = true;
            }
//            if($user_code == \Acme\Models\Role::APPLICANT_USER || $user_code == \Acme\Models\Role::APPLICANT_MANAGER_USER) {
//                $applicant_code = false;
//            }
        }

        $show_agent_invite = (
            $cur_user &&
            ($user_code == Role::APPLICANT_USER || $user_code == Role::APPLICANT_MANAGER_USER) && // код роли пользователя которого приглашаем равен соискателю/менеджеру
            $cur_role_code == Role::AGENT_USER && // текущий пользователь агент
            $cur_user->confirmed == User::CONFIRM_STATUS_ACTIVE && // текущий пользователь подтвержденный агент
            (UsersAgentInvitationsRepository::checkUserInvitationStatus($user, $cur_user) == null) // у пользователя отсутствуют приглашения от этого агента
        );

        $relogin = UserRepository::checkReloginStatus($user, $cur_user);

        $eventsType  = EventsTypeRepository::getEvensTypes();
        $professions = ProfessionsRepository::getAllProfessions();
        $mainParams  = ($role instanceof Role) ? $role->getProfileParamsForCurrentRole() : [];
        $imagesCount = GalleryImagesRepository::getGalleryPreviewCountByUser($user->id);
        $previewImg  = GalleryImagesRepository::getImageForPreview($user->id, (self::PICTURES_IN_PREVIEW-$imagesCount));

        $imagesCount += count($previewImg);

        return \View::make('site/profile/view_user', compact(
                'user',
                'rate_read_only',
                'applicant_code',
                'gallery',
                'previewImg',
                'profile',
                'eventsType',
                'professions',
                'imagesCount',
                'mainParams',
                'show_agent_invite',
                'chatCode',
                'relogin'
            ));
    }

    /**
     * upload photo
     */
    public function postUploadPhoto()
    {
        $data         = \Input::all();
        $user         = \Auth::user();

        $response = [
            'state'   => 404,
            'image'   => '',
            'result'  => null
        ];

        $avatar = $user->saveAvatar($data, true);

        if ($avatar) {

            $response = [
                'state'   => 200,
                'image'   => $avatar,
                'result'  => asset(\Config::get('app.image_dir.profile') . $avatar)
            ];
        }

        echo json_encode($response);
        die();
    }

    public function postRate()
    {
        $user = \Auth::user();

        $object_rate = Ratings::whereRaw('user_id = ? and object_id = ? and type = "' . \Acme\Models\Ratings::RATING_TYPE_USER . '"', array($user->id, \Input::get('id')))->first();


        if($object_rate) {
            $object_rate->user_id = $user->id;
            $object_rate->object_id = \Input::get('id');
            $object_rate->rating = \Input::get('rate');
            $object_rate->type = \Acme\Models\Ratings::RATING_TYPE_USER;

            $object_rate->save();
        } else {
            $rate = new Ratings();

            $rate->user_id = $user->id;
            $rate->object_id = \Input::get('id');
            $rate->rating = \Input::get('rate');
            $rate->type = \Acme\Models\Ratings::RATING_TYPE_USER;

            $rate->save();
        }

        $avg = round(Ratings::whereRaw('object_id = ? and type = "' . \Acme\Models\Ratings::RATING_TYPE_USER . '"', array(\Input::get('id')))->avg('rating'), 1);

        return json_encode(array('avg' => $avg));
    }
}