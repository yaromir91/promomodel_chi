<?php

namespace Acme\Controllers\User;

use Acme\Models\User;

use Acme\Models\Repositories\UserRepository;
use Acme\Models\UserProvider;
use Acme\Models\Repositories\RoleRepository;
use Acme\Models\Role;
use Acme\Validators\Profile\ProfileValidators;

class UserController extends \BaseController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * Inject the models.
     * @param User $user
     * @param UserRepository $userRepo
     */
    public function __construct(User $user, UserRepository $userRepo)
    {
        parent::__construct();
        $this->user = $user;
        $this->userRepo = $userRepo;
    }

    /**
     * Users settings page
     *
     * @return View
     */
    public function getIndex()
    {
        list($user,$redirect) = $this->user->checkAuthAndRedirect('user');
        if($redirect){return $redirect;}

        // Show the page
        return \View::make('site/user/index', compact('user'));
    }

    public function showUsers() {

        $usersList = \DB::table('users')
            ->leftJoin('assigned_roles', function($join)
            {
                $join->on('users.id', '=', 'assigned_roles.user_id');
            })
            ->where('assigned_roles.role_id', '<>', 1)
            ->orWhereNull('assigned_roles.role_id')
            ->get();

        return \View::make('site/user/list', compact('usersList'));
    }

    /**
     *  Отрисовывает форму выбора роли для пользователя
     *
     * @return mixed
     */
    public function chooseRole()
    {
        return \View::make('site/user/fb-login');
    }

    /**
     * TODO Нужно будет переделать
     * @return mixed
     */
    public function postChooseRole()
    {
        return $this->createFbUser(\Input::all());
    }


    /**
     * Авторизация через ФБ
     * @return mixed
     */
    public function loginWithFacebook()
    {
        $code = \Input::get('code');
        $fb   = \OAuth::consumer('Facebook');
        $msg  = false;
        $user = false;

        if (!empty($code)) {

            $fb->requestAccessToken($code);
            $result  = json_decode($fb->request('/me'), true);

            if (!empty($result)) {
                $profile = UserProvider::where(array('provider_id' => $result['id']))->first();

                if ($profile instanceof UserProvider) {
                    $user    = User::find($profile->user_id);
                } else {
                    \Session::put('result', $result);
                    return \Redirect::to('fb/choose-role');
                }
            }

            if ($user instanceof User) {
                \Auth::login($user);
                return \Redirect::to('/');
            } else {
                return \Redirect::to('user/create')
                    ->with('error', \Lang::get('general.user_not_created'));
            }
        }
        else {
            $url = $fb->getAuthorizationUri();
            return \Redirect::to((string)$url);
        }

    }

    public function createFbUser($input)
    {

        $result = \Session::get('result');


        $role          = !empty($input['account_type']) ? $input['account_type'] : RoleRepository::getRoleByCode(Role::APPLICANT_USER);

        $data['username']     = !empty($result['first_name']) ? $result['first_name'] : '';
        $data['email']        = !empty($result['email']) ? $result['email'] : '';

        $data['gender']       = !empty($result['gender']) ? substr($result['gender'], 0, 1) : 'm';
        $data['first_name']   = !empty($result['first_name']) ? $result['first_name'] : '';
        $data['last_name']    = !empty($result['last_name']) ? $result['last_name'] : '';

        $data['account_type'] = $role;

//        $data['password']              = $input['password'];
//        $data['password_confirmation'] = $input['password_confirmation'];

        $password =  \Acme\Helpers\PasswordHelpers::generate_password(8);
        $data['password']              = $password;
        $data['password_confirmation'] = $password;


//        $validator = ProfileValidators::getFbPasswordFormValidator($input);
//
//        if ($validator->passes()) {
            $user = $this->userRepo->signup($data, 1);
//        } else {
//            return Redirect::to('fb/choose-role')->withErrors($validator);
//        }

        if ($user->id) {

            $profileSaved = $this->userRepo->addProfileRoleToUser($user);
            $provider     = $user->addSocialProvider($result['id'], 'Facebook');

            if ($profileSaved && $provider) {

                if($result){
                    $filename = md5(time()) . '.jpg';
                    $img = file_get_contents('https://graph.facebook.com/' . $result['id'] . '/picture?type=large');
                    $uploadDir = 'upload/profile/';
                    $file = $uploadDir . $filename;
                    file_put_contents($file, $img);
                }

                $this->userRepo->addFbAvatar($user, $filename);

                $this->userRepo->setFbStatus($user, 1);

                if (\Config::get('confide::signup_email')) {
                    \Mail::queueOn(
                        \Config::get('confide::signup_email'),
                        'emails.auth.socialreg',
                        ['password' => $data['password']],
                        function($message)  use ($user)
                        {
                            $message->to($user->email, $user->firstName . ' ' . $user->lastName)
                                ->subject(\Lang::get('general.Registration'));
                        });
                }
            } else {

                $user->delete();

                return \Redirect::to('user/create')->with('error', \Lang::get('general.user_not_created'));
            }
        }

        if ($user instanceof User) {
            \Auth::login($user);
            return \Redirect::to('profile');
        } else {
            return \Redirect::to('user/create')
                ->with('error', \Lang::get('general.user_not_created'));
        }
    }

    /**
     * Stores new user
     *
     */
    public function postIndex()
    {
        $user = $this->userRepo->signup(\Input::all());

        if ($user->id) {

            $profileSaved = $this->userRepo->addProfileRoleToUser($user);

            if ($profileSaved) {
                if (\Config::get('confide::signup_email')) {
                    \Mail::queueOn(
                        \Config::get('confide::email_queue'),
                        \Config::get('confide::email_account_confirmation'),
                        compact('user'),
                        function ($message) use ($user) {
                            $message
                                ->to($user->email, $user->username)
                                ->subject(\Lang::get('confide::confide.email.account_confirmation.subject'));
                        }
                    );
                }

                return \Redirect::to('user/login')
                    ->with('success', \Lang::get('user/user.confirm_your_account', array('email' => $user->email)));
            } else {
                $user->delete();

                return \Redirect::to('user/create');
            }
        } else {
//            $error = $user->errors()->all(':message');
            $error = $user->errors()->toArray();

            return \Redirect::to('user/create')
                ->withInput(\Input::except('password'))
                ->with('err', $error);
        }

    }

    // TODO С этим методом нужно что-то делать
    /**
     * Edits a user
     * @var User
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(User $user)
    {
        $oldUser = clone $user;

        $user->username = \Input::get('username');
        $user->email = \Input::get('email');

        $password = \Input::get('password');
        $passwordConfirmation = \Input::get('password_confirmation');

        if (!empty($password)) {
            if ($password != $passwordConfirmation) {
                // Redirect to the new user page
                $error = \Lang::get('admin/users/messages.password_does_not_match');
                return \Redirect::to('user')
                    ->with('error', $error);
            } else {
                $user->password = $password;
                $user->password_confirmation = $passwordConfirmation;
            }
        }

        if ($this->userRepo->save($user)) {
            return \Redirect::to('user')
                ->with( 'success', \Lang::get('user/user.user_account_updated') );
        } else {
            $error = $user->errors()->all(':message');
            return \Redirect::to('user')
                ->withInput(\Input::except('password', 'password_confirmation'))
                ->with('error', $error);
        }

    }

    /**
     * Displays the form for user creation
     *
     */
    public function getCreate()
    {
        return \View::make('site/user/create');
    }


    /**
     * Displays the login form
     *
     */
    public function getLogin()
    {
        $user = \Auth::user();
        if(!empty($user->id)){
            if(\Session::has('loginRedirect')) {
                $redirect_link = \Session::get('loginRedirect');
                \Session::forget('loginRedirect');
            } else {
                $redirect_link = '/';
            }

            return \Redirect::to($redirect_link);
        }

        return \View::make('site/user/login');
    }

    /**
     * Attempt to do login
     *
     */
    public function postLogin()
    {
        \App::make('Acme\Models\Repositories\UserRepository');
        $input = \Input::all();

        if ($this->userRepo->login($input)) {
            if(\Session::has('loginRedirect')) {
                $redirect_link = \Session::get('loginRedirect');
                \Session::forget('loginRedirect');
            } else {
                $redirect_link = '/';
            }
            return \Redirect::intended($redirect_link);
        } else {
            if ($this->userRepo->isThrottled($input)) {
                $err_msg = \Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($this->userRepo->existsButNotConfirmed($input)) {
                $err_msg = \Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = \Lang::get('confide::confide.alerts.wrong_credentials');
            }

            return \Redirect::to('user/login')
                ->withInput(\Input::except('password'))
                ->with('error', $err_msg);
        }

    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getConfirm($code)
    {
        if ( \Confide::confirm( $code ) )
        {
            return \Redirect::to('user/login')
                ->with( 'notice', \Lang::get('confide::confide.alerts.confirmation') );
        }
        else
        {
            return \Redirect::to('user/login')
                ->with( 'error', \Lang::get('confide::confide.alerts.wrong_confirmation') );
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function getForgot()
    {
        return \View::make('site/user/forgot');
    }

    /**
     * Attempt to reset password with given email
     *
     */
    public function postForgotPassword()
    {
        if (\Confide::forgotPassword(\Input::get('email'))) {
            $notice_msg = \Lang::get('confide::confide.alerts.password_forgot');
            return \Redirect::to('user/forgot')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = \Lang::get('confide::confide.alerts.wrong_password_forgot');
            return \Redirect::to('user/forgot')
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function getReset( $token )
    {

        return \View::make('site/user/reset')
            ->with('token',$token);
    }


    /**
     * Attempt change password of the user
     *
     */
    public function postReset()
    {
        
        $input = array(
            'token'                 => \Input::get('token'),
            'password'              => \Input::get('password'),
            'password_confirmation' => \Input::get('password_confirmation'),
        );

        if (\Input::get('password') != \Input::get('password_confirmation')) {
            $error_msg = \Lang::get('confide::confide.alerts.wrong_password_reset');
            return \Redirect::to('user/reset/' . $input['token'])
//                ->withInput()
                ->with('error', $error_msg);
        }
        // By passing an array with the token, password and confirmation
        if ($this->userRepo->resetPassword($input)) {
            $notice_msg = \Lang::get('confide::confide.alerts.password_reset');
            return \Redirect::to('user/login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = \Lang::get('confide::confide.alerts.wrong_password_reset');
            return \Redirect::to('user/reset/' . $input['token'])
//                ->withInput()
                ->with('error', $error_msg);
        }

    }

    /**
     * Log the user out of the application.
     *
     */
    public function getLogout()
    {
        \Confide::logout();

        return \Redirect::to('/');
    }

    /**
     * Get user's profile
     * @param $username
     * @return mixed
     */
    public function getProfile($username)
    {
        $userModel = new User;
        $user = $userModel->getUserByUsername($username);

        // Check if the user exists
        if (is_null($user))
        {
            return \App::abort(404);
        }

        return \View::make('site/user/profile', compact('user'));
    }

    public function getSettings()
    {
        list($user,$redirect) = User::checkAuthAndRedirect('user/settings');
        if($redirect){return $redirect;}

        return \View::make('site/user/profile', compact('user'));
    }

    /**
     * Process a dumb redirect.
     * @param $url1
     * @param $url2
     * @param $url3
     * @return string
     */
    public function processRedirect($url1,$url2,$url3)
    {
        $redirect = '';
        if( ! empty( $url1 ) )
        {
            $redirect = $url1;
            $redirect .= (empty($url2)? '' : '/' . $url2);
            $redirect .= (empty($url3)? '' : '/' . $url3);
        }
        return $redirect;
    }
}
