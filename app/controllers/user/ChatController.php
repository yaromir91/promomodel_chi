<?php

namespace Acme\Controllers\User;

use  Acme\Models\Chat\UserChats;
use  Acme\Models\Chat\UserChatMessages;
use  Acme\Models\Repositories\ChatRepository;
/**
 * Class ChatController
 * @package Acme\Controllers\User
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class ChatController extends \BaseController
{

    /**
     * @return \Illuminate\View\View
     */
    public function getList()
    {
        $this->notAuthRedirect();

        $chats  = ChatRepository::getUserChats($this->user->id);
        $userId = $this->user->id;

        return \View::make('site/chat/list',
            compact(
                'chats',
                'userId'
            )
        );
    }

    /**
     * @param $chat
     *
     * @return mixed
     */
    public function getIndex($chat)
    {
        $this->notAuthRedirect();

        if (!($chat instanceof UserChats)) {

            return \Redirect::to('404', 303);
        }

        if (!$chat->isAllowToTheChat($this->user->id)) {

            return \Redirect::to('404', 303);
        }

        $chatTitle    = $chat->getUserNames($this->user->id);
        $userId       = $this->user->id;
        $currentuser  = $this->user;
        $receiveruser = $chat->getReceiverUserId($this->user->id, true);
        $messages     = $chat->getCurrentMessages();
        $countMsg     = $chat->getCountOfMessages();

        return \View::make('site/chat/index',
            compact(
                'chat',
                'userId',
                'currentuser',
                'receiveruser',
                'messages',
                'countMsg',
                'chatTitle'
            )
        );
    }

    /**
     * @param $chat
     *
     * @return mixed
     */
    public function postMessage($chat)
    {
        $this->notAuthRedirect();

        if (!($chat instanceof UserChats)) {

            return \Redirect::to('404', 303);
        }

        if (!$chat->isAllowToTheChat($this->user->id)) {

            return \Redirect::to('404', 303);
        }

        $data       = \Input::all();
        $user       = $this->user;
        $recUserId  = $chat->getReceiverUserId($this->user->id);

        $result['send_status'] = true;
        $result['theTime']     = false;
        $result['recUserId']   = $recUserId;
        $result['msg_is']      = false;

        if (!empty($data['message'])) {
            $theMsg                = null;
            $result['send_status'] = $chat->postNewMessage($this->user->id, $data['message'], $theMsg);
            $result['theTime']     = date("Y-m-d H:i:s");

            if ($result['send_status']) {
                $result['msg_is'] = $theMsg->id;
            }
        }

        return \Response::json($result);
    }

    /**
     * @param $chat
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getData($chat)
    {
        $user = $this->user;

        $this->notAuthRedirect();

        if (!($chat instanceof UserChats)) {

            return \Redirect::to('404', 303);
        }

        if (!$chat->isAllowToTheChat($this->user->id)) {

            return \Redirect::to('404', 303);
        }

        $data         = \Input::all();
        $userId       = $this->user->id;
        $receiveruser = $chat->getReceiverUserId($this->user->id, true);

        $messages     = $chat->getCurrentMessages(isset($data['currPage']) ? $data['currPage'] : 0);

        $html = \View::make('site/chat/_partials/_chat_messages',
            compact(
                'chat',
                'userId',
                'receiveruser',
                'messages'
            )
        )->render();

        $result['html'] = $html;

        echo json_encode($result);
        die();
    }


    /**
     * @param $chatMes
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setRead($chatMes)
    {
        $this->notAuthRedirect();
        $user                  = $this->user;

        if (!($chatMes instanceof UserChatMessages)) {

            return \Redirect::to('404', 303);
        }

        $chatMes->is_m_read	 = UserChatMessages::READ_ON;
        $chatMes->save();

        $result['changed'] = true;

        echo json_encode($result);
        die();
    }

    /**
     * @param $chatMes
     * @return mixed
     */
    public function postDelete($chatMes)
    {
        $this->notAuthRedirect();
        $user                  = $this->user;
        $result['send_status'] = false;

        if (!($chatMes instanceof UserChatMessages)) {

            return \Redirect::to('404', 303);
        }

        if ($user->id != $chatMes->user_sender_id) {

            return \Redirect::to('404', 303);
        }

        $chatMes->is_deleted = UserChatMessages::DELETED_MSG_ON;
        $chatMes->save();

        $result['send_status'] = true;

        echo json_encode($result);
        die();
    }

}