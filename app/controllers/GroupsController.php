<?php

namespace Acme\Controllers;

use Acme\Models\GroupComments;
use Acme\Models\Groups;
use Acme\Models\GroupsInvites;
use Acme\Models\Repositories\GroupsRepository;
use Acme\Models\Role;
use Acme\Models\User;
use Acme\Models\UsersGroup;
use Acme\Helpers\PictureHelper;
use Acme\Models\Notifications;
use Acme\Models\Repositories\NotificationRepository;

class GroupsController extends \BaseController
{
	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('auth');
		\View::share('title', 'Groups');
	}

	/**
	 *
	 * GET /groups
	 *
	 * @return \Response
	 */
	public function index()
	{
		if(\Route::currentRouteNamed('my_group')){
			$groups = UsersGroup::where(['user_id' => \Auth::id()])->get();
		} else {
			$groups = Groups::orderBy('id', 'DESC')
				->paginate(\Config::get('app.groups_pagination_list'));
		}

		return \View::make('site/groups/index', compact('groups'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /groups/create
	 *
	 * @return \Response
	 */
	public function getCreate()
	{
		$title = \Lang::get('groups.title');

		if(\Request::isMethod('post') && (\Auth::user()->getMainUserRoleCode() == Role::APPLICANT_MANAGER_USER
			|| \Auth::user()->getMainUserRoleCode() == Role::AGENT_USER)
		){
			$validator = \Validator::make(\Input::all(), [
				'title' 	  => 'required',
				'description' => 'required',
				'new_picture' => 'required',
			]);

			if(Groups::where(['user_id' => \Auth::id()])->exists() && \Auth::user()->getMainUserRoleCode() == Role::APPLICANT_MANAGER_USER){
				return \Redirect::back()
                    ->withInput()
					->with('error', \Lang::get('groups.exists_group'));
			}

			if($validator->fails()){
				return \Redirect::back()
					->withInput()
					->withErrors($validator);
			}

			$group = new Groups;
			$group->title = \Input::get('title');
			$group->description = \Input::get('description');
			$group->user_id = \Auth::id();

			if(\Input::get('new_picture')) {
				$group->image       = \Input::get('new_picture');
			}
			$group->save();

            $userGroup = new UsersGroup();
            $userGroup->groups_id = $group->id;
            $userGroup->user_id = \Auth::id();
            $userGroup->save();

			return \Redirect::route('groups')
				->with('success', \Lang::get('groups.group_success_add'));
		}
		return \View::make('site.groups.create', compact('title'));
	}

	/**
	 * * Display the specified resource.
	 * GET /groups/show/{id}
	 *
	 * @param Groups $group
	 * @return \View
	 */
	public function getShow(Groups $group)
	{
		// Reset session
		\Session::put('images');

		$subscribers 		= UsersGroup::where(['groups_id' => $group->id])->limit(\Config::get('app.groups_pagination'))->get();
		$currentSubscriber 	= UsersGroup::where(['groups_id' => $group->id, 'user_id' => \Auth::user()->id])->first();
//		$comments 			= GroupComments::where(['groups_id' => $group->id])->orderBy('created_at', 'desc')->paginate(\Config::get('app.group_comments_pagination'));

        $comments = $group->comments()->orderBy('created_at', 'DESC')->get();

        $is_admin = ($this->user->getCurrentUserRole()->code == Role::ADMIN_USER);
        $user = $this->user;

		return \View::make('site.groups.show', compact('group', 'subscribers', 'currentSubscriber', 'comments', 'user', 'is_admin'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /groups/{id}/edit
	 *
	 * @param  Groups $group
	 * @return \Response
	 */
	public function getEdit(Groups $group)
	{
            if(\Auth::user()->getMainUserRoleCode() == Role::APPLICANT_MANAGER_USER || \Auth::user()->getMainUserRoleCode() == Role::AGENT_USER){
                return \View::make('site.groups.edit', compact('group'));
            } else {
                return \Redirect::route('groups')->with('error', \Lang::get('groups.group_not_access'));
            }
	}

	public function postEdit(Groups $group)
	{
		$data      = \Input::all();
		$validator = \Validator::make($data, [
			'title'       => 'required|min:3|max:70',
			'description' => 'required',
			'user_id'     => 'groupuser:id'
		]);

		if ($validator->passes()) {
			$savedRes = $group->saveGeneralInfo($data);

			if ($savedRes['status']==0) {
				return \Redirect::route('groups')->with('success', \Lang::get('groups.group_success_update'));
			}

			return \Redirect::route('groups')->with('error', \Lang::get('groups.group_error_update'));
		}

		return \Redirect::back()->withInput()->withErrors($validator);
	}

        /**
	 * Remove the specified resource from storage.
	 * POST /groups/{id}
	 *
	 * @param  Groups $group
	 * @param  null|integer $except
	 * @return \Redirect
	 */
	public function destroy(Groups $group, $except = null)
	{
		if(\Auth::user()->id != $group->user_id)
		{
			return \Redirect::route('show_group' , $group->id)->with('error', \Lang::get('groups.group_user_error_delete'));
		}

		$users = UsersGroup::where(['groups_id' => $group->id])->get();
		if($users->count() > 1) {
			\Mail::send('emails.groups.delete-group', [
				'group' => $group->title,
			], function ($message) use ($group, $users, $except) {

			$message->from(\Config::get('mail.from.address'), \Config::get('mail.from.name'))
					->subject(\Lang::get('groups.group_deleted'));

				foreach ($users as $user) {
                    if($user->user_id !== \Auth::id() ){
                        $message = $message->to($user->users->email);

                        NotificationRepository::addNewNotification($group->user_id, $user->user_id, \Lang::get('notifications.msg_13'), Notifications::TYPE_GROUP);
                    }
				}

			});
		}
		$group->delete();

		return \Redirect::route('groups')
			->with('success', \Lang::get('groups.group_success_delete'));
	}

	/**
	 * @param Groups $group
	 * @param null $user_id
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function getUnsubscribe(Groups $group, $user_id = null)
	{
		if($group && $user_id && \Auth::user()) {
			NotificationRepository::addNewNotification($user_id, $group->user_id, \Lang::get('notifications.msg_12'), Notifications::TYPE_GROUP);

			$subscriberInv = GroupsInvites::where(['group_id' => $group->id, 'user_id' => $user_id])->first();
			$subscriberGr = UsersGroup::where(['groups_id' => $group->id, 'user_id' => $user_id])->first();

			$subscriberInv->delete();
			$subscriberGr->delete();
			return \Redirect::back()
				->with('success', \Lang::get('groups.subscribe_unsubscribe_success'));
		} else if(is_null($user_id) && \Auth::user()) {
			NotificationRepository::addNewNotification(\Auth::user()->id, $group->user_id, \Lang::get('notifications.msg_12'), Notifications::TYPE_GROUP);

			$subscriberInv = GroupsInvites::where(['group_id' => $group->id, 'user_id' => \Auth::user()->id])->first();
			$subscriberGr = UsersGroup::where(['groups_id' => $group->id, 'user_id' => \Auth::user()->id])->first();

			$subscriberInv->delete();
			$subscriberGr->delete();
			return \Redirect::back()
				->with('success', \Lang::get('groups.subscribe_unsubscribe_success'));
		} else {
			return \Redirect::back()
				->with('error', \Lang::get('groups.subscribe_role_error'));
		}
	}

	/**
	 * @param Groups $group
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function getSubscribe(Groups $group) // TODO UNUSED method
	{
		if(in_array(\Auth::user()->getMainUserRoleCode(), [Role::APPLICANT_USER, Role::APPLICANT_MANAGER_USER, Role::AGENT_USER])){
			if(\Auth::user()->getMainUserRoleCode() == Role::AGENT_USER){
				$unique = '';
			} else {
				$unique = 'required|unique:' . UsersGroup::getTableName() . ',user_id';
			}
			$validator = \Validator::make([
				'user_id' => \Auth::user()->id,
				'groups_id' => $group->id,
			], [
				// TODO нужно доделать проверку на сушествоваоания пользователя для данной группы
				'user_id' => $unique,
				'groups_id' => 'required',
			],[
				'user_id.unique' => \Lang::get('groups.exists_subscribe')
			]);

			if($validator->fails()){
				return \Redirect::back()
					->with('error', $validator->getMessageBag()->first());
			}


			$userGroup = new UsersGroup;
			$userGroup->user_id = \Auth::user()->id;
			$userGroup->groups_id = $group->id;
			$userGroup->save();

			return \Redirect::back()
				->with('success', \Lang::get('groups.subscribe_success'));
		}

		return \Redirect::back()
			->with('error', \Lang::get('groups.role_error'));
	}

	/**
	 * @param Groups $group
	 * @return \Illuminate\View\View
	 */
	public function showMembers(Groups $group)
	{
		$members = UsersGroup::where(['groups_id' => $group->id])->paginate(\Config::get('app.groups_pagination'));

		return \View::make('site.groups.members', compact('members', 'group'));
	}

    /**
     * @param Groups $group
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getInviteToGroup(Groups $group)
    {
        if(\Request::isMethod('post') && (\Auth::id() == $group->users->id)){

			$search_params = \Input::get('search_params');
            $result = \App::make('InviteToGroupServiceProvider', compact('search_params', 'group'));

            $userInvitation = GroupsRepository::getUserInvitationResult($result, $group);

            return \View::make('site.groups.search_user', compact('result', 'group', 'userInvitation'));
        }

		$role   = Role::getApplicantRole();
		$params = $role->getSearchParamsForCurrentRole();
		$ranges = \App::make('SearchApplicantServiceProvider', compact('params'));
		$event = $group;
		$route = 'invite_to_group_post';
		$postParams = $group->id;
		$title = \Lang::get('groups.search');

		$data   = [
			'params' => $params,
			'ranges' => $ranges,
		];

		return \View::make('site.events.search_form', compact('title', 'event', 'data', 'route' , 'postParams'));
    }

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function getInviteUserToGroup()
    {
        if(\Input::ajax()){
                $data = \Input::except('_token');
                $data['token'] = str_random(50);

                try{
                        $user = new GroupsInvites;
                        $user->group_id = $data['group_id'];
                        $user->user_id = $data['user_id'];
                        $user->token = $data['token'];
                        $user->save();

                        $dataEmail = [
                                'group_id' 	=> $data['group_id'],
                                'group_name' 	=> Groups::find($data['group_id'])->title,
                                'token' 	=> $data['token'],
                        ];

                        \Mail::queue('emails.groups.invite-user', $dataEmail, function ($send) use ($data) {
                                $send->to(User::find($data['user_id'])->email)->subject(\Lang::get('mails.invite_mail_title'));;
                        });

                }catch (\Illuminate\Database\QueryException $e){
                        return \Response::json(\Lang::get('groups.exists_user_invite'), 500);
                }
                return \Response::json(['Done'], 200);
        }else{
                return \Response::json('Not ajax', 403);
        }
    }

    /**
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getInviteSuccess($token)
    {
        /** @var GroupsInvites $user */
        $user = GroupsInvites::where(['token' => $token])->get()->first();
        if(is_null($user)){
                return \Redirect::to('/')->with('error', \Lang::get('groups.invite_mail_token_expires'));
        }

        $refused = GroupsRepository::getUserInvitationRefuse($user->user_id, $user->group_id);

        if($refused) {
            foreach($refused as $ref) {
                //Send email to Applicant Manager about accept
                $dataEmail = [
                    'group_id' => $ref->group_id,
                    'group_name' => Groups::find($ref->group_id)->title,
                    'user_id' => $ref->user_id,
                ];
                \Mail::queue('emails.groups.invite-accept', $dataEmail, function ($send) use ($ref) {
                    $send->to(User::find($ref->user_id)->email)->subject(\Lang::get('mails.invite_mail_title'));
                });

                GroupsInvites::find($ref->id)->delete();
            }
        }

		$user->status = 'active';
		$user->token = '';
		$user->save();

		$members = new UsersGroup;
		$members->user_id = $user->user_id;
		$members->groups_id = $user->group_id;
		$members->save();

        //Send email to Applicant Manager about accept
        $dataEmail = [
            'group_id' => $user->group_id,
            'group_name' => Groups::find($user->group_id)->title,
            'user_id' => $user->user_id,
        ];
        \Mail::queue('emails.groups.invite-accept', $dataEmail, function ($send) use ($user) {
            $send->to(User::find($user->user_id)->email)->subject(\Lang::get('mails.invite_mail_title'));;
        });

        $group = Groups::where(['id' => $user->group_id])->get()->first();

        // регистрация уведомления соискателю
        NotificationRepository::addNewNotification($user->user_id, $group->user_id, \Lang::get('notifications.msg_11') . ' <a href="' . \URL::route('show_group', $user->group_id) . '">' . \Lang::get('notifications.msg_10') . '</a>', Notifications::TYPE_GROUP_INVITE);

        return \Redirect::route('group_my_invitation');
    }

	/**
	 * @param $token
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function getInviteRefuse($token)
	{
		/** @var GroupsInvites $user */
		$user = GroupsInvites::where(['token' => $token, 'status' => GroupsInvites::STATUS_NOT_ACTIVE])->get()->first();
		if(is_null($user)){
			return \Redirect::to('/')->with('error', \Lang::get('groups.invite_mail_token_expires'));
		}

		//Send email to Applicant Manager about refuse
		$dataEmail = [
			'group_id' => $user->group_id,
			'group_name' => Groups::find($user->group_id)->title,
			'user_id' => $user->user_id,
		];
		\Mail::queue('emails.groups.invite-refuse', $dataEmail, function ($send) use ($user) {
			$send->to(User::find($user->user_id)->email)->subject(\Lang::get('mails.invite_mail_title'));;
		});

                $group = Groups::where(['id' => $user->group_id])->get()->first();
                // регистрация уведомления соискателю
                NotificationRepository::addNewNotification($user->user_id, $group->user_id, \Lang::get('notifications.msg_09') . ' <a href="' . \URL::route('show_group', $user->group_id) . '">' . \Lang::get('notifications.msg_10') . '</a>', Notifications::TYPE_GROUP_INVITE);

		//Delete invite
		$user->delete();

		return \Redirect::route('group_my_invitation');
	}

	/**
	 * Check exists directory and add image
	 *
	 * @return string message
	 */
	private function _createImage($name = null)
	{
		$destinationPath = \Config::get('app.image_dir.groups');

		if(!is_dir($destinationPath)){
			mkdir($destinationPath);
			return "Directory $destinationPath was not created and will be created automatically";
		}
		if(!is_null($name)){
			\Input::file('image')->move($destinationPath, $name);
		}
	}


	/**
	 * upload group preview
	 */
	public function postUploadPreview()
	{
		$data         		= \Input::all();
		$user         		= $this->user;
		$fileName     		= "";
		$fileSmallPathName	= "";

		$response = [
			'state'   => 404,
			'image'   => '',
			'result'  => null
		];

		$destinationPath      = \Config::get('app.image_dir.groups');
		$destinationSmallPath = \Config::get('app.image_dir.groups_small');
		$destinationPrevPath  = \Config::get('app.image_dir.groups_preview');

		if (!is_dir($destinationPath)) {
			$oldmask = umask(0);
			\File::makeDirectory($destinationPath, 0777, true);
			umask($oldmask);
		}

		if (!is_dir($destinationSmallPath)) {
			$oldmask = umask(0);
			\File::makeDirectory($destinationSmallPath, 0777, true);
			umask($oldmask);
		}

		if (!is_dir($destinationPrevPath)) {
			$oldmask = umask(0);
			\File::makeDirectory($destinationPrevPath, 0777, true);
			umask($oldmask);
		}

		$file       = isset($data['image']) ? $data['image'] : false;
		$avatarData = isset($data['avatar_data']) ? json_decode(stripslashes($data['avatar_data'])) : null;

		try {
			if($file) {
				$extensionList = \Config::get('app.allowed_extension');
				$extension     = $file->getClientOriginalExtension();

				if (isset($extensionList[$extension]) && ($extensionList[$extension] ==1)) {
					$fileName = \Str::random(20) . '.' . $extension;
					$file->move($destinationPath, $fileName);

					$filePathName        = $destinationPath . '/' . $fileName;
					$fileSmallPathName   = $destinationSmallPath . '/' . $fileName;
					$filePreviewPathName = $destinationPrevPath . '/' . $fileName;

					copy($filePathName, $fileSmallPathName);
					copy($filePathName, $filePreviewPathName);

					$cropResult = PictureHelper::crop($filePathName, $fileSmallPathName, $avatarData, $error, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'), \Config::get('app.quality_picture'));

					$img = \Image::make($filePathName);
					$img->fit(\Config::get('app.preview_width'), \Config::get('app.preview_height'));
					$img->save($filePreviewPathName, \Config::get('app.quality_picture'));
				}
			}

			if ($fileSmallPathName) {

				$response = [
					'state'   => 200,
					'image'   => $fileName,
					'result'  => asset($fileSmallPathName)
				];
			}
		} catch (\Exception $e) {
			$response = [
				'state'   => 404,
				'image'   => '',
				'result'  => null
			];
		}

		echo json_encode($response);
		die();
	}

    public function myInvitation()
    {
		$this->beforeFilter('auth');
        $userInvitations    = [];

        $userCodeRole       = $this->user->getMainUserRoleCode();
        $isShowInvitation   = ($userCodeRole == Role::APPLICANT_MANAGER_USER || $userCodeRole == Role::APPLICANT_USER);

        if ($isShowInvitation) {
            $userInvitations    = GroupsRepository::getUserInvitation($this->user->id);
        }


        return \View::make('site/groups/my_invitation', compact(
            'userInvitations',
            'isShowInvitation'
        ));
    }
}