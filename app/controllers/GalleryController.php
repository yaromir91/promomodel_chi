<?php

namespace Acme\Controllers;

use Acme\Models\Gallery;
use Acme\Models\Repositories\GalleryRepository;
use Acme\Models\Repositories\GalleryImagesRepository;
use Acme\Models\GalleryImages;
use Acme\Models\Tags;
use Acme\Models\User;
use Acme\Models\Repositories\TagsRepository;
use Acme\Models\Ratings;
use \Acme\Models\Repositories\RatingsRepository;
use Acme\Helpers\PictureHelper;


/**
 *
 * Class GalleryController
 *
 *
 * @property
 *
 * @author Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class GalleryController extends \BaseController {

    /**
     * Gallery Model
     * @var Gallery
     */
    protected $gallery;

    /**
     * @var string
     */
    protected $gallery_title_rule   = 'required|min:3|max:70';
    /**
     * @var string
     */
    protected $gallery_tag_rule     = 'min:3';
    /**
     * @var string
     */
    protected $image_file_rule      = 'image';
    /**
     * @var string
     */
    protected $album_rule           = 'required';


    /**
     * Inject the models.
     * @param Gallery $gallery
     */
    public function __construct(Gallery $gallery)
    {
        parent::__construct();
        $this->gallery = $gallery;
    }

	/**
	 * Display a listing of the resource.
	 * GET /gallery
	 *
	 * @return Response
	 */
	public function getPublicGalleriesList()
	{
        \Session::forget('images');
        $user = \Auth::user();
        $galeries = GalleryRepository::getPublicGalleries();

        $data = [
            'user'          => $user,
            'galeries'      => $galeries,
        ];

        return \View::make('site/gallery/publicGalleriesList', compact('data'));
	}

    /**
     * @param $filter
     *
     * @return mixed
     */
    public function getPublicGalleriesListFilter($filter)
    {
        \Session::forget('images');
        $user = \Auth::user();
        $galeries = GalleryRepository::getPublicFilterGalleries($filter);

        $data = [
            'user'          => $user,
            'galeries'      => $galeries,
        ];

        return \View::make('site/gallery/publicGalleriesList', compact('data'));
    }

    /**
     * @return mixed
     */
    public function getPersonalGalleriesList()
    {
        \Session::forget('images');
        $user = \Auth::user();
        $galeries = GalleryRepository::getUserGalleries($user->id);

        $data = [
            'user'          => $user,
            'galeries'      => $galeries,
        ];

        return \View::make('site/gallery/personalGalleriesList', compact('data'));
    }

    /**
     * @return mixed
     */
    public function getPersonalPhotoList()
    {
        \Session::forget('images');
        $user = \Auth::user();
        $photos = GalleryImagesRepository::getImagesOfUser($user->id);

        $data = [
            'user'          => $user,
            'photos'        => $photos,
        ];

        return \View::make('site/gallery/personalPhotosList', compact('data'));
    }

    /**
     * @param User $user
     * @return \Illuminate\View\View
     */
    public function getAlbums(User $user)
    {
        $galeries = GalleryRepository::getUserGalleries($user->id);

        $data = [
            'user'          => $user,
            'galeries'      => $galeries,
        ];

        $data['user_auth'] = $this->user;

        if($this->user) {
            $data['user_auth'] = $this->user;
        }


        return \View::make('site/gallery/user_albums', compact('data'));
    }

    /**
     * @param Gallery $gallery
     *
     * @return mixed
     */
    public function getShowAlbum(Gallery $gallery)
    {
        if(!$gallery) {
            return \Redirect::to('gallery')->with('error', \Lang::get('gallery.no_gallery_found'));
        }

        $albumOwner  = $gallery->user;

        if(!$albumOwner) {
            return \Redirect::to('gallery')->with('error', \Lang::get('gallery.no_gallery_found'));
        }

        $images      = GalleryImagesRepository::getImagesOfAlbum($gallery->id);
        $currentTags = TagsRepository::getCurrentGalleriesTags($gallery->id);
        $ratings     = RatingsRepository::getGalleryRating($gallery->id);

        $data = [
            'gallery'       => $gallery,
            'images'        => $images,
            'currentTags'   => $currentTags,
            'ratings'       => $ratings,
            'albumOwner'    => $albumOwner
        ];

        $data['user_auth'] = false;

        $user = $this->user;
        if($user && $user->id == $gallery->user_id) {
            $data['user_auth'] = $user;
        }


        return \View::make('site/gallery/showAlbum', compact('data'));
    }

    /**
     * @param Gallery $gallery
     *
     * @return mixed
     */
    public function getEditPersonalAlbum(Gallery $gallery)
    {
        if(!$gallery) {
            return \Redirect::to('gallery')->with('error', \Lang::get('gallery.no_gallery_found'));
        }
        $currentTags = TagsRepository::getCurrentGalleriesTags($gallery->id);
        $data = [
            'currentTags'   => implode(',', $currentTags),
            'gallery'       => $gallery
        ];

        $data['user'] = false;
        $user = \Auth::user();
        if($user && $user->id == $gallery->user_id) {
            $data['user'] = $user;
        }

        return \View::make('site/gallery/editPersonalAlbumForm', compact('data'));
    }

    /**
     * @param Gallery $gallery
     *
     * @return mixed
     */
    public function getShowPersonalAlbum(Gallery $gallery)
    {
        \Session::forget('images');
        if(!$gallery) {
            return \Redirect::to('gallery')->with('error', \Lang::get('gallery.no_gallery_found'));
        }
        $images = GalleryImagesRepository::getImagesOfAlbum($gallery->id);
        $currentTags = TagsRepository::getCurrentGalleriesTags($gallery->id);

        $data = [
            'gallery'       => $gallery,
            'images'        => $images,
            'currentTags'   => $currentTags
        ];

        $data['user_auth'] = false;

        $user = $this->user;
        if($user && $user->id == $gallery->user_id) {
            $data['user_auth'] = $user;
        }


        return \View::make('site/gallery/showPersonalAlbum', compact('data'));
    }

    /**
     * @param Gallery $gallery
     *
     * @return mixed
     */
    public function getDeletePersonalAlbum($gallery)
    {
        if(!$gallery) {
            return \Redirect::to('gallery')->with('error', \Lang::get('gallery.no_gallery_found'));
        }
        $user = \Auth::user();
        $destinationPath = \Config::get('app.image_dir.album') . $user->id . '/small/' . $gallery->preview;
        $destSliderPic   = \Config::get('app.image_dir.album_slider') . $gallery->preview;

        if (\File::exists($destinationPath)) {
            \File::delete($destinationPath);
        }

        if (\File::exists($destSliderPic)) {
            \File::delete($destSliderPic);
        }

        TagsRepository::clearGalleriesTags($gallery->id);
        GalleryImagesRepository::deleteImageById($gallery->id);

        $gallery->delete();

        return \Redirect::to('gallery/personal/album')->with('success', \Lang::get('gallery.success_delete_album'));
    }

    /**
     * @param Gallery $gallery
     *
     * @return mixed
     */
    public function getAddPersonalPhoto(Gallery $gallery = null)
    {
        \Session::forget('images');
        $user = \Auth::user();
        if(empty($user->id))
        {
            \Session::put('loginRedirect', \Request::url());
            return \Redirect::to('user/login');
        }

        $galeries = GalleryRepository::getUserGalleries($user->id, true);

        if(!isset($galeries[0])) {
            $galeries[0] = \Lang::get('gallery.create_new_album');
        }

        ksort($galeries);

//        $ratings = RatingsRepository::getGalleryRating($gallery->id);

        $data = [
            'galeries'  => $galeries,
            'user'      => $user,
//            'ratings'   => $ratings
        ];

        if($gallery){
            $data['gallery']    = $gallery;
            $data['back_link']  = 'gallery/personal/album/show/' . $gallery->id;
        } else {
            $data['gallery']    = false;
            $data['back_link']  = 'gallery/personal/photo';
        }

        return \View::make('site/gallery/addPersonalPhotoForm', compact('data', 'user'));
    }

    /**
     * @return mixed
     */
    public function getAddPersonalAlbum()
    {
        $user = \Auth::user();
        if(empty($user->id))
        {
            \Session::put('loginRedirect', \Request::url());
            return \Redirect::to('user/login');
        }

        $data = [
            'user'      => $user
        ];

        return \View::make('site/gallery/addPersonalAlbumForm', compact('data', 'user'));
    }

    /**
     * @return mixed
     */
    public function postCreateAlbum()
    {

        $rules = array(
            'title'     => $this->gallery_title_rule,
        );

        $data      = \Input::all();

        $validator = \Validator::make($data, $rules);

        if ($validator->passes())
        {
            $user                       = $this->user;
            $this->gallery->user_id     = $this->user->id;
            $this->gallery->title       = \Input::get('title');

            if($data['new_picture']) {
                $this->gallery->preview        = $data['new_picture'];
            }

            if($this->gallery->save())
            {
                return \Redirect::to('gallery/personal/album')->with('success', \Lang::get('gallery.success_add_album'));
            }

            return \Redirect::to('gallery/create-album')->with('error', \Lang::get('gallery.error_with_save'));
        }

        return \Redirect::back()
            ->withErrors($validator)
            ->withInput();
    }

    /**
     * @return string
     */
    public function postJSAddImage()
    {
        $file = \Input::file('myfile');

        $destinationPath      = \Config::get('app.image_dir.album') . $this->user->id;
        $destinationSmallPath = \Config::get('app.image_dir.album') . $this->user->id .'/small';
        $destPreviewPath      = \Config::get('app.image_dir.album') . $this->user->id .'/preview';

        $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();

        if (!file_exists($destinationPath)) {
            \File::makeDirectory($destinationPath, 0777, true);
        }

        if (!file_exists($destinationSmallPath)) {
            \File::makeDirectory($destinationSmallPath, 0777, true);
        }

        if (!file_exists($destPreviewPath)) {
            \File::makeDirectory($destPreviewPath, 0777, true);
        }

        \Input::file('myfile')->move($destinationPath, $name);

        $filePathName      = $destinationPath . '/' . $name;
        $fileSmallPathName = $destinationSmallPath . '/' . $name;
        $filePrevPathName  = $destPreviewPath . '/' . $name;

        copy($filePathName, $fileSmallPathName);
        copy($filePathName, $filePrevPathName);

        /*$img = \Image::make($filePrevPathName);
        $img->fit(\Config::get('app.gall_slider_width'), \Config::get('app.gall_slider_height'));
        $img->save($filePrevPathName);*/

        PictureHelper::img_resize($filePathName, $fileSmallPathName, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'), 0xF0F0F0, 60);
        PictureHelper::img_resize($filePrevPathName, $filePrevPathName, \Config::get('app.gall_slider_width'), \Config::get('app.gall_slider_height'), 0xF0F0F0, 60);
        return json_encode(array($name));
    }

    /**
     * @return string
     */
    public function postJSRemoveImage()
    {
        $user = \Auth::user();

        $fileName = \Input::get('name');

        $destinationPath      = \Config::get('app.image_dir.album') . '/' . $user->id . '/' . $fileName;
        $destinationSmallPath = \Config::get('app.image_dir.album') . $this->user->id .'/small/' . $fileName;
        $destPreviewPath      = \Config::get('app.image_dir.album') . $this->user->id .'/preview/' . $fileName;


        if (\File::exists($destinationPath)) {
            \File::delete($destinationPath);
        }

        if (\File::exists($destinationSmallPath)) {
            \File::delete($destinationSmallPath);
        }

        if (\File::exists($destPreviewPath)) {
            \File::delete($destPreviewPath);
        }

        return json_encode($fileName);
    }

    public function postJSDeleteCancel()
    {
        $user = \Auth::user();

        $imageNames = \Input::get('images');

        if($imageNames) {
            foreach($imageNames as $fileName) {

                $destinationPath      = \Config::get('app.image_dir.album') . '/' . $user->id . '/' . $fileName;
                $destinationSmallPath = \Config::get('app.image_dir.album') . $user->id .'/small/' . $fileName;
                $destPreviewPath      = \Config::get('app.image_dir.album') . $this->user->id .'/preview/' . $fileName;


                if (\File::exists($destinationPath)) {
                    \File::delete($destinationPath);
                }

                if (\File::exists($destinationSmallPath)) {
                    \File::delete($destinationSmallPath);
                }

                if (\File::exists($destPreviewPath)) {
                    \File::delete($destPreviewPath);
                }
            }
        }
        return json_encode(array('success'));
    }

    /**
     * @return string
     */
    public function postJSDeleteImages()
    {
        $user = \Auth::user();

        $imageIds = \Input::get('images');

        if($imageIds) {
            foreach($imageIds as $id) {
                if(!$this->deleteImage($id, $user->id)) {
                    return json_encode(array('error'));
                }
            }
        }
        return json_encode(array('success'));
    }

    /**
     * @param $id
     * @param $user_id
     *
     * @return bool
     */
    protected function deleteImage($id, $user_id)
    {
        $image = GalleryImages::find($id);
        if($image) {
            $destinationPath      = \Config::get('app.image_dir.album') . $user_id . '/' . $image->file;
            $destinationSmallPath = \Config::get('app.image_dir.album') . $this->user->id .'/small/' . $image->file;
            $destPreviewPath      = \Config::get('app.image_dir.album') . $this->user->id .'/preview/' . $image->file;
            if (\File::exists($destinationPath)) {
                \File::delete($destinationPath);
                \File::delete($destinationSmallPath);
                \File::delete($destPreviewPath);
                GalleryImagesRepository::deleteImageById($id);
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function postAddImageToAlbum()
    {
        $rules = array(
            'album'     => $this->album_rule
        );

        $album = \Input::get('album');

        if($album == 0) {
            $rules['title'] = $this->gallery_title_rule;
            $rules['tag']   = $this->gallery_tag_rule;
        }

        $validator = \Validator::make(\Input::all(), $rules);

        if(\Input::get('gallery')){
            $redirect_link = 'gallery/personal/photo/add/' . \Input::get('gallery');
            $success_redirect = 'gallery/personal/album/show/' . \Input::get('gallery');
        } else {
            $redirect_link = 'gallery/personal/photo/add';
            $success_redirect = 'gallery/personal/photo';
        }

        if ($validator->passes())
        {
            $user = \Auth::user();

            if($album == 0) {
                $gallery = new Gallery();
                $gallery->user_id     = $user->id;
                $gallery->title       = \Input::get('title');

                $gallery->save();

                if(\Input::get('tag')) {
                    $data_tags = explode(',', \Input::get('tag'));

                    foreach($data_tags as $data_tag)
                    {
                        $check_t = Tags::Name($data_tag)->first();
                        if($check_t)
                        {
                            $this->gallery->tags()->attach($check_t->id);
                        }
                        else
                        {
                            $new_tag = new Tags;
                            $new_tag->name = $data_tag;
                            $new_tag->save();
                            $this->gallery->tags()->attach($new_tag->id);
                        }
                    }
                }

            } else {
                $gallery = Gallery::find($album);
            }

            $images = \Input::get('images');

            if($images) {

                foreach($images as $image){
                    $g_image = new GalleryImages();

                    $g_image->gallery_id     = $gallery->id;
                    $g_image->file           = $image;

                    if($g_image->save())
                    {
                        $result = true;
                    } else {
                        $result = false;
                        break;
                    }
                }

                if($result) {
                    return \Redirect::to($success_redirect)->with('success', \Lang::get('gallery.success_add_photo'));
                } else {
                    return \Redirect::to($redirect_link)->with('error', \Lang::get('gallery.error_with_save'));
                }
            }

            return \Redirect::to($redirect_link)->with('error', \Lang::get('gallery.error_with_save'));
        }

        return \Redirect::to($redirect_link)->withInput()->withErrors($validator);
    }

    /**
     * @param Gallery $gallery
     *
     * @return mixed
     */
    public function postEditAlbum(Gallery $gallery)
    {
        $this->notAuthRedirect();

        $rules = array(
            'title'     => $this->gallery_title_rule,
//            'tag'       => $this->gallery_tag_rule
        );

        $validator = \Validator::make(\Input::all(), $rules);
        $data      = \Input::all();
        $user      = \Auth::user();

        if ($validator->passes())
        {
            $gallery->title  = \Input::get('title');
            $oldPicture      = $gallery->preview;
            $destinationPath = \Config::get('app.image_dir.album') . $user->id;

            if($data['new_picture']) {
                if (trim($data['new_picture']) != $oldPicture) {
                    \File::delete($destinationPath . '/' . $oldPicture);
                }
                $gallery->preview        = $data['new_picture'];
            }

            if($gallery->save())
            {
                return \Redirect::route('show_personal_album_gallery', ['id' => $gallery->id])->with('success', \Lang::get('gallery.success_update_album'));
            }

            return \Redirect::to('gallery/edit/' . $gallery->id)->with('error', \Lang::get('gallery.error_with_save'));
        }

        // Form validation failed
        return \Redirect::back()
            ->withErrors($validator)
            ->withInput();
    }

    /**
     * Creates a unique filename by appending a number
     *
     * i.e. if image.jpg already exists, returns
     * image2.jpg
     */
    public function uniqueFilename($path, $name, $ext) {

        $output = $name;
        $basename = basename($name, '.' . $ext);
        $i = 2;

        while(\File::exists($path . '/' . $output)) {
            $output = $basename . $i . '.' . $ext;
            $i ++;
        }

        return $output;

    }

    /**
     * get search tag
     */
    public function getSearchtag()
    {
        $searched = [];
        $word     = \Input::get('q', '');

        if ($word) {
            $searched = TagsRepository::searchTags($word, true);
        }

        echo (json_encode($searched));
        die();
    }

    public function postAddRating()
    {
        $user = \Auth::user();

        $object_rate = Ratings::whereRaw('user_id = ? and object_id = ? and type = "photo"', array($user->id, \Input::get('id')))->first();

        if($object_rate) {
            $object_rate->user_id = $user->id;
            $object_rate->object_id = \Input::get('id');
            $object_rate->rating = \Input::get('rate');
            $object_rate->type = 'photo';

            $object_rate->save();
        } else {
            $rate = new Ratings();

            $rate->user_id = $user->id;
            $rate->object_id = \Input::get('id');
            $rate->rating = \Input::get('rate');
            $rate->type = 'photo';

            $rate->save();
        }

        $avg = round(Ratings::whereRaw('object_id = ? and type = "photo"', array(\Input::get('id')))->avg('rating'), 1);

        return json_encode(array('avg' => $avg));
    }


    /**
     * upload gallery preview
     */
    public function postUploadPreview()
    {
        $data         = \Input::all();
        $user         = $this->user;
        $fileName     = "";

        $response = [
            'state'   => 404,
            'image'   => '',
            'result'  => null
        ];

        $destinationPath = \Config::get('app.image_dir.album') . (isset($data['user_id']) ? $data['user_id'] : $user->id);
        $destSliderPath  = \Config::get('app.image_dir.album_slider');

        if (!is_dir($destinationPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destinationPath, 0777, true);
            umask($oldmask);
        }

        if (!is_dir($destSliderPath)) {
            $oldmask = umask(0);
            \File::makeDirectory($destSliderPath, 0777, true);
            umask($oldmask);
        }

        $file       = isset($data['preview']) ? $data['preview'] : false;
        $avatarData = isset($data['avatar_data']) ? json_decode(stripslashes($data['avatar_data'])) : null;

        try {
            if($file) {
                $extensionList = \Config::get('app.allowed_extension');
                $extension     = $file->getClientOriginalExtension();

                if (isset($extensionList[$extension]) && ($extensionList[$extension] ==1)) {
                    $fileName      = \Str::random(20) . '.' . $extension;
                    $file->move($destinationPath, $fileName);

                    $filePathName = $destinationPath . '/' . $fileName;
                    $sliderFile   = $destSliderPath . '/' . $fileName;

                    copy($filePathName, $sliderFile);

                    PictureHelper::img_resize($sliderFile, $sliderFile, \Config::get('app.gall_slider_width'), \Config::get('app.gall_slider_height'), 0xF0F0F0, 60);
                    PictureHelper::crop($filePathName, $filePathName, $avatarData, $error, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'));
                }
           }

            if ($fileName) {

                $response = [
                    'state'   => 200,
                    'image'   => $fileName,
                    'result'  => asset($destinationPath . '/' . $fileName)
                ];
            }

        } catch (\Exception $e) {
            $response = [
                'state'   => 404,
                'image'   => '',
                'result'  => null
            ];
        }

        echo json_encode($response);
        die();
    }
}