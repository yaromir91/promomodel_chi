<?php

use Illuminate\Support\Facades\Redis;

use Acme\Models\Role;
use Acme\Models\User;
use Acme\Models\Profile\UserProfile;
use Acme\Models\Repositories\ChatRepository;

class BaseController extends Controller {

    protected $user;
    protected $redisConnection      = null;
    protected $loginRoute           = 'main_site_login';
    protected $user_role_for_header = '';

    private $eventUrlPath = 'events';


    public $unReadMessage = 0;

    /**
     * Initializer.
     *
     * @access   public
     * @return \BaseController
     */
    public function __construct()
    {

        $this->redisConnection = Redis::connection();
        $this->setAuthorized();
        $this->setLastActiveTimeToUser();
        \App::setLocale(Session::get('lang', 'ru'));

        $this->beforeFilter('csrf', array('on' => 'post'));

        if($this->user instanceof Acme\Models\User){
            $this->setUserRoleHeadertitle();
            $this->unReadMessage   = $this->getUnReadMessage($this->user->id);
        }



        \View::share('user_role_for_header', $this->user_role_for_header);
        \View::share('user_un_read_message', $this->unReadMessage);
    }

    /**
     * @param $userId
     *
     * @return int
     */
    private function getUnReadMessage($userId)
    {
        return ChatRepository::getUserNotReadMessage($userId);
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
    protected function setupLayout()
    {
            if ( ! is_null($this->layout))
            {
                    $this->layout = View::make($this->layout);
            }
    }


    /**
     * set auth user
     */
    private function setAuthorized()
    {
        $this->user = Auth::user();
    }

    /**
     * @return bool
     */
    protected function isAuthorized() {

        if ($this->user instanceof Acme\Models\User) {

            return true;
        }

        return false;
    }


    /**
     * Redirect to login page if there is no auth user
     */
    protected function notAuthRedirect()
    {
        if (!$this->isAuthorized()) {
            Session::put('loginRedirect', \Request::url());
            Redirect::route($this->loginRoute)->send();
        }
    }

    /**
     * Set time of last user activity
     */
    protected function setLastActiveTimeToUser()
    {
        $user         = Auth::user();

        if ($user) {

            $userActivityCachedName = 'user_last_activity_' . $user->id;
            $userLastActivityValue  = ['id' => $user->id, 'time' => time(), 'role' => $user->getMainUserRoleCode()];

            $this->redisConnection->set($userActivityCachedName, json_encode($userLastActivityValue));
            $this->redisConnection->expire($userActivityCachedName, 600);
        }

    }


    /**
     * @param array $userRoles
     *
     * @return array
     */
    protected function getOnlineUsers($userRoles = [])
    {
        $cached = $this->redisConnection->keys('user_last_activity_*');
        $result = [];

        if (count($cached)) {
            $matched = preg_grep('/^user_last_activity_([0-9]+)/', $cached);

            if (count($matched)) {
                foreach ($matched as $mcd) {
                    $fromMemory = $this->redisConnection->get($mcd);

                    if ($fromMemory) {
                        $tmpResult = json_decode($fromMemory);

                        if (!empty($tmpResult)) {

                            $isApproveSelect = false;

                            if (!empty($userRoles)) {
                                $isApproveSelect = $this->checkRolesForOnlineUsers($tmpResult->role, $userRoles);
                            } else {
                                $isApproveSelect = true;
                            }

                            if ($isApproveSelect) {
                                $user = User::find($tmpResult->id);

                                if (($user instanceof User) && ($user->profile instanceof UserProfile)) {
                                    $result[] = $user->profile;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $cacheName
     * @param array $roles
     *
     * @return array|mixed
     */
    protected function getOnlineUsersFromCache($cacheName, $roles = [])
    {
        $results = $this->redisConnection->get($cacheName);
        $results = json_decode($results);

        if (empty($results)) {
            $results = $this->getOnlineUsers($roles);
            $this->redisConnection->set($cacheName, json_encode($results));
            $this->redisConnection->expire($cacheName, 600);
        }

        return $results;
    }


    /**
     * @param $role
     * @param array $userRoles
     *
     * @return bool
     */
    private function checkRolesForOnlineUsers($role, $userRoles = [])
    {

        if (!empty($userRoles)) {
           foreach ($userRoles as $ur) {
                if ($ur == $role) {
                    return true;
                }
           }
        }

        return false;
    }


    /**
     * Set header for user/role
     */
    private function setUserRoleHeadertitle()
    {
        $segment1 = (Request::segment(1));
        $segment3 = (Request::segment(3));

        $this->user_role_for_header = \Acme\Models\Role::getRolesTitle($this->user->getCurrentUserRole()->code);
        if($this->user->getCurrentUserRole()->code == Role::AGENT_USER && !$this->user->isConfirmedAgent($this->user->id)) {
            $this->user_role_for_header = '';
        }

        if (!empty($segment1) && !empty($segment3)) {
            if ($segment1 == $this->eventUrlPath) {
                $invitation = null;
                $isSendInv  = $this->user->isSentEventInvited($segment3, $invitation);

                if (!empty($invitation)) {
                    if (isset($invitation->status) && ($invitation->status == Acme\Models\Events\EventInvite::INVITE_PROF_STATUS_ACCEPT)) {
                        $this->user_role_for_header = \Lang::get('general.participant');
                    }
                }
            }
        }
    }

}