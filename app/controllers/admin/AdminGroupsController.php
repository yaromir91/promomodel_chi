<?php

namespace Acme\Controllers\Admin;

use Acme\Models\User;
use Acme\Models\Role;
use Acme\Models\Groups;
use Acme\Models\Repositories\UserRepository;

/**
 * Class AdminGroupsController
 * @package Acme\Controllers\Admin
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class AdminGroupsController extends \AdminController
{

    /**
     * @var array
     */
    protected $groupValidation = [
        'title'       => 'required|min:3|max:70',
        'description' => 'required',
        'user_id'     => 'groupuser:id'
    ];

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $title = \Lang::get('admin/groups/titles.groups_list');

        return \View::make('admin/groups/index', compact('title'));
    }


    /**
     * @return mixed
     */
    public function getData()
    {
        $groups = Groups::leftJoin('users', 'groups.user_id', '=', 'users.id')
            ->select(array(
                'groups.id',
                'groups.image',
                'users.username',
                'groups.user_id',
                'groups.title'
            ));

        return \Datatables::of($groups)

            ->edit_column('username', '<a target="_blank" href="{{ URL::route(\'view_user_profile\' , $user_id) }}">{{$username}}</a>')
            ->edit_column('actions', '<a href="{{ URL::to(\'admin/groups/\' . $id . \'/edit\' ) }}" class="iframe btn btn-xs btn-default">{{ Lang::get(\'button.edit\') }}</a>
                                      <a href="{{ URL::to(\'admin/groups/\' . $id . \'/delete\' ) }}" class="iframe btn btn-xs btn-danger">{{ Lang::get(\'button.delete\') }}</a>
            ')
            ->edit_column('image', '@if($image) <img width="140" alt="{{$title}}" src="{{ asset(\Config::get(\'app.image_dir.groups_small\') . $image) }}"> @endif')
            ->remove_column('user_id')
            ->remove_column('id')

            ->make();
    }

    /**
     * @return mixed
     */
    public function getCreate()
    {
        $title       = \Lang::get('admin/groups/titles.create_group');
        $usersList   = UserRepository::getUsersOfGroupOwners([User::CONFIRM_STATUS_YES, User::CONFIRM_STATUS_ACTIVE], true, [Role::APPLICANT_MANAGER_USER, Role::AGENT_USER]);
        $tab         = 1;

        return \View::make('admin/groups/edit', compact(
            'usersList',
            'tab',
            'title'
        ));
    }


    /**
     * @param $group
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($group)
    {
        $tab         = 1;
        $title       = \Lang::get('admin/groups/titles.update_group');
        $usersList   = UserRepository::getUsersOfGroupOwners([User::CONFIRM_STATUS_YES, User::CONFIRM_STATUS_ACTIVE], true, [Role::APPLICANT_MANAGER_USER, Role::AGENT_USER]);

        return \View::make('admin/groups/edit', compact(
            'tab',
            'usersList',
            'group',
            'title'
        ));
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate()
    {
        $data      = \Input::all();
        $validator = \Validator::make($data, $this->groupValidation);

        if ($validator->passes()) {

            $group    = new Groups();
            $savedRes = $group->saveGeneralInfo($data);

            if ($savedRes['status']==0) {

                return \Redirect::to('admin/groups/' . $group->id . '/edit')->with('error', $savedRes['error']);
            }

            return \Redirect::to('admin/groups/' . $group->id . '/edit')->with('success', \Lang::get('admin/groups/messages.update.success'));

        }

        return \Redirect::back()->withErrors($validator)->withInput();
    }


    /**
     * @param $group
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postEdit($group)
    {
        $data      = \Input::all();
        $validator = \Validator::make($data, $this->groupValidation);

        if ($validator->passes()) {
            $savedRes = $group->saveGeneralInfo($data);

            if ($savedRes['status']==0) {

                return \Redirect::to('admin/groups/' . $group->id . '/edit')->with('error', $savedRes['error']);
            }

            return \Redirect::to('admin/groups/' . $group->id . '/edit')->with('success', \Lang::get('admin/groups/messages.update.success'));
        }

        return \Redirect::back()->withErrors($validator)->withInput();
    }

    /**
     * @param $group
     * @return \Illuminate\View\View
     */
    public function getDelete($group)
    {
        $title = \Lang::get('admin/groups/titles.delete_group');

        return \View::make('admin/groups/delete', compact('group', 'title'));
    }


    /**
     * @param $group
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete($group)
    {
        if($group->delete('\Acme\Models\Repositories\GroupsRepository::sendLettersAboutDeleteGroup')) {
            return \Redirect::to('admin/groups')->with('success', \Lang::get('admin/groups/messages.delete.success'));
        }

        return \Redirect::to('admin/groups')->with('error', \Lang::get('admin/groups/messages.delete.error'));
    }
}