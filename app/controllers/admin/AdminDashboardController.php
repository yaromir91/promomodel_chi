<?php

namespace Acme\Controllers\Admin;

class AdminDashboardController extends \AdminController {

	/**
	 * Admin dashboard
	 *
	 */
	public function getIndex()
	{
        return \View::make('admin/dashboard');
	}

}