<?php

namespace Acme\Controllers\Admin;

use Acme\Models\Gallery;
use Acme\Models\Repositories\TagsRepository;
use Acme\Models\Repositories\UserRepository;
use Acme\Models\Repositories\GalleryImagesRepository;
use Acme\Models\User;
use Acme\Models\GalleryImages;
use Acme\Helpers\PictureHelper;

/**
 * Class AdminGalleryController
 *
 * @package Acme\Models\Controllers\Admin
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class AdminGalleryController extends \AdminController
{

    /**
     * @var array
     */
    protected $albumEditValidation = [
        'title'   => 'required|min:3|max:70',
//        'tag'     => 'min:3'
    ];

    /**
     * @var array
     */
    protected $albumCreateValidation = [
        'title'   => 'required|min:3|max:70',
        'user_id' => 'required',
//        'tag'     => 'min:3'
    ];

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $title = \Lang::get('admin/gallery/titles.gallery_list');

        return \View::make('admin/gallery/index', compact('title'));
    }


    /**
     * @param $gallery
     *
     * @return \Illuminate\View\View
     */
    public function getCreateimg($gallery)
    {
        $title = \Lang::get('admin/gallery/titles.add_images');

        return \View::make('admin/gallery/partials/createimg', compact('title', 'gallery'));
    }

    /**
     * @param $gallery
     *
     * @return string
     */
    public function postJSRemoveImage($gallery)
    {
        $data                   = \Input::all();
        $destinationPath        = \Config::get('app.image_dir.album') . $gallery->user_id . '/';
        $destinationSmallPath   = \Config::get('app.image_dir.album') . $gallery->user_id .'/small/';
        $destPreviewPath        = \Config::get('app.image_dir.album') . $gallery->user_id .'/preview/';
        $file                   = isset($data['name']) ? $data['name'] : '';

        if ($file) {
            if (\File::exists($destinationPath . $file)) {
                \File::delete($destinationPath . $file);
            }
            if (\File::exists($destinationSmallPath . $file)) {
                \File::delete($destinationSmallPath . $file);
            }
            if (\File::exists($destPreviewPath . $file)) {
                \File::delete($destPreviewPath . $file);
            }

            $dataIMG         = array();
            $current_images = \Session::get('images');

            foreach($current_images as $current_image) {
                if($current_image != $file){
                    $dataIMG[] = $current_image;
                }
            }
            \Session::put('images', array_unique($dataIMG));

            GalleryImagesRepository::deleteImageByName($file);
        }

        return json_encode(array($file));
    }

    /**
     * @param $gallery
     *
     * @return string
     */
    public function postJSAddImage($gallery)
    {
        $data                   = \Input::all();
        $destinationPath        = \Config::get('app.image_dir.album') . $gallery->user_id;
        $destinationSmallPath   = \Config::get('app.image_dir.album') . $gallery->user_id .'/small';
        $destPreviewPath        = \Config::get('app.image_dir.album') . $gallery->user_id .'/preview';
        $file                   = isset($data['myfile']) ? $data['myfile'] : '';

        if ($file) {
            $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();

            if (!file_exists($destinationPath)) {
                \File::makeDirectory($destinationPath, 0777, true);
            }

            if (!file_exists($destinationSmallPath)) {
                \File::makeDirectory($destinationSmallPath, 0777, true);
            }

            if (!file_exists($destPreviewPath)) {
                \File::makeDirectory($destPreviewPath, 0777, true);
            }

            \Input::file('myfile')->move($destinationPath, $name);

            $newImage             = new GalleryImages();
            $newImage->gallery_id = $gallery->id;
            $newImage->file       = $name;
            $newImage->save();


            $filePathName      = $destinationPath . '/' . $name;
            $fileSmallPathName = $destinationSmallPath . '/' . $name;
            $filePrevPathName  = $destPreviewPath . '/' . $name;

            copy($filePathName, $fileSmallPathName);
            copy($filePathName, $filePrevPathName);

            PictureHelper::img_resize($filePathName, $fileSmallPathName, \Config::get('app.main_pic_resize_width'), \Config::get('app.main_pic_resize_height'), 0xF0F0F0, 60);
            PictureHelper::img_resize($filePrevPathName, $filePrevPathName, \Config::get('app.gall_slider_width'), \Config::get('app.gall_slider_height'), 0xF0F0F0, 60);

            $current_images   = \Session::get('images', array());
            $current_images[] = $name;
            \Session::put('images', array_unique($current_images));


            return json_encode(array($name));
        }

        return json_encode([]);
    }

    /**
     * @param $gallery
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($gallery)
    {
        $title = \Lang::get('admin/gallery/titles.update_gallery');
        $tab   = 1;

//        $currentTags = TagsRepository::getCurrentGalleriesTags($gallery->id);
//        $currentTags = implode(',', $currentTags);
        $usersList   = UserRepository::getUsersByStatus(User::STATUS_ACTIVE, true);

        $images      = $gallery->images;

        return \View::make('admin/gallery/edit', compact(
                'gallery',
                'tab',
//                'currentTags',
                'usersList',
                'title',
                'images'
            ));
    }

    /**
     * get search tag
     */
    public function getSearchtag()
    {
        $searched = [];
        $word     = \Input::get('q', '');

        if ($word) {
            $searched = TagsRepository::searchTags($word, true);
        }

        echo (json_encode($searched));
        die();
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        $title       = \Lang::get('admin/gallery/titles.add_gallery');
        $tab         = 1;
//        $currentTags = [];
        $images      = [];
        $usersList   = UserRepository::getUsersByStatus(User::STATUS_ACTIVE, true);

        return \View::make('admin/gallery/edit', compact(
            'title',
            'tab',
//            'currentTags',
            'usersList',
            'images'
        ));
    }


    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate()
    {
        $data      = \Input::all();
        $validator = \Validator::make($data, $this->albumCreateValidation);

        if ($validator->passes()) {

            $gallery  = new Gallery();

            $savedRes = $gallery->saveGeneralInfo($data);


            if ($savedRes['status']==0) {

                return \Redirect::to('admin/gallery/' . $gallery->id . '/edit')->with('error', $savedRes['error']);
            }

//            $gallery->saveTags($data['tag']);

            return \Redirect::to('admin/gallery/' . $gallery->id . '/edit')->with('success', \Lang::get('admin/blogs/messages.update.success'));

        }

        return \Redirect::back()->withErrors($validator)->withInput();
    }

    /**
     * @param $gallery
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postEdit($gallery)
    {
        $data      = \Input::all();
        $validator = \Validator::make($data, $this->albumEditValidation);

        if ($validator->passes()) {
            $savedRes = $gallery->saveGeneralInfo($data);
//            $gallery->saveTags($data['tag']);

            if ($savedRes['status']==0) {

                return \Redirect::to('admin/gallery/' . $gallery->id . '/edit')->with('error', $savedRes['error']);
            }

            return \Redirect::to('admin/gallery/' . $gallery->id . '/edit')->with('success', \Lang::get('admin/blogs/messages.update.success'));
        }

        return \Redirect::back()->withErrors($validator)->withInput();
    }


    /**
     * @param $image
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDeleteImg($image)
    {
        $rules = array(
            'id' => 'required|integer'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $image->delete();
        }

        return \Redirect::to('admin/gallery')->with('error', \Lang::get('admin/blogs/messages.delete.error'));
    }

    /**
     * @param $gallery
     *
     * @return \Illuminate\View\View
     */
    public function getDelete($gallery)
    {
        $title = \Lang::get('admin/gallery/titles.delete_gallery');

        return \View::make('admin/gallery/delete', compact('gallery', 'title'));
    }

    public function postDelete($gallery)
    {
        $rules = array(
            'id' => 'required|integer'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            GalleryImagesRepository::deleteAllGalleryImages($gallery);
            $gallery->delete();
        }


        return \Redirect::to('admin/gallery')->with('error', \Lang::get('admin/blogs/messages.delete.error'));
    }

    /**
     * @param $image
     *
     * @return \Illuminate\View\View
     */
    public function getDeleteImg($image)
    {
        $title = \Lang::get('admin/gallery/titles.delete_gallery');

        return \View::make('admin/gallery/partials/deleteimg', compact('image', 'title'));
    }

    /**
     * @param $gallery
     *
     * @return mixed
     */
    public function getDataimg($gallery)
    {
        $images = GalleryImages::select([
                'gallery_images.id',
                'gallery_images.gallery_id',
                'galleries.user_id',
                'gallery_images.file'
            ])
            ->leftJoin('galleries', 'galleries.id', '=', 'gallery_images.gallery_id')
            ->where('gallery_id', '=', $gallery->id)
        ;

        return \Datatables::of($images)

            ->edit_column('file','@if($file)
                            <img src="{{ asset(\Config::get(\'app.image_dir.album\') . $user_id . \'/small/\' .$file) }}">
                        @else
                            '.\Config::get('app.album_no_image').'
                        @endif')

            ->edit_column('actions', '<a href="{{{ URL::to(\'admin/gallery/\' . $id . \'/deleteimg\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')

            ->remove_column('user_id')
            ->remove_column('gallery_id')
            ->remove_column('id')

            ->make();
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $gallery = Gallery::leftJoin('users', 'galleries.user_id', '=', 'users.id')
            ->select(array('galleries.id', 'galleries.preview', 'galleries.user_id', 'users.username',  'galleries.title',  'galleries.created_at'));

        return  \Datatables::of($gallery)

            ->edit_column('username', '<a target="_blank" href="{{ URL::route(\'view_user_profile\' , $user_id) }}">{{$username}}</a>')
            ->edit_column('preview','@if($preview)
                            <img width="100%" src="{{ asset(\Config::get(\'app.image_dir.album\') . $user_id . \'/\' .$preview) }}">
                        @else
                            '.\Config::get('app.album_no_image').'
                        @endif')
            ->edit_column('actions', '<a href="{{ URL::to(\'admin/gallery/\' . $id . \'/edit\' ) }}" class="iframe btn btn-xs btn-default">{{ Lang::get(\'button.edit\') }}</a>
                                      <a href="{{ URL::to(\'admin/gallery/\' . $id . \'/delete\' ) }}" class="iframe btn btn-xs btn-danger">{{ Lang::get(\'button.delete\') }}</a>
            ')
            ->remove_column('user_id')
            ->remove_column('id')

            ->make();
    }
}