<?php

namespace Acme\Controllers\Admin;

use Acme\Models\Events\EventsType;

/**
 * Class AdminEventsController
 *
 * @package Acme\Models\Controllers\Admin
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class AdminEventTypesController extends \AdminController
{

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $title = \Lang::get('admin/events/titles.event_titles_list');

        return \View::make('admin/event_types/types', compact('title'));
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $roles = EventsType::select(array('events_type.id', 'events_type.title', 'events_type.title_en'));

        return \Datatables::of($roles)

            ->edit_column('actions', '<a href="{{{ URL::to(\'admin/events-type/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                      <a href="{{{ URL::to(\'admin/events-type/\' . $id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')
            ->remove_column('id')

            ->make();
    }


    /**
     * @param $eventstype
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($eventstype)
    {
        $title = \Lang::get('admin/events/titles.edit_event_types');

        return \View::make('admin/event_types/edit', compact('eventstype', 'title'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        $title = \Lang::get('admin/events/titles.create_event_types');

        return \View::make('admin/event_types/edit', compact('title'));
    }

    /**
     * @param $eventstype
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postEdit($eventstype)
    {
        $validator = \Validator::make(\Input::all(), ['title' => 'required', 'title_en' => 'required']);

        if ($validator->passes())
        {
            $eventstype->title = \Input::get('title', '');
            $eventstype->title_en = \Input::get('title_en', '');

            if ($eventstype->save()) {
                return \Redirect::to('admin/events-type/' . $eventstype->id . '/edit')->with('success', \Lang::get('admin/events/titles.edit_success'));
            }

        }

        return \Redirect::to('admin/events-type/' . $eventstype->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate()
    {
        $validator = \Validator::make(\Input::all(), ['title' => 'required', 'title_en' => 'required']);

        if ($validator->passes())
        {
            $eventstype        = new EventsType();
            $eventstype->title = \Input::get('title', '');
            $eventstype->title_en = \Input::get('title_en', '');

            if ($eventstype->save()) {
                return \Redirect::to('admin/events-type/' . $eventstype->id . '/edit')->with('success', \Lang::get('admin/events/titles.create_success'));
            }

            return \Redirect::to('admin/events-type/create')->with('errors', $eventstype->errors()->all());

        }

        return \Redirect::to('admin/events-type/create')->withInput()->withErrors($validator);
    }


    /**
     * @param $eventstype
     *
     * @return \Illuminate\View\View
     */
    public function getDelete($eventstype)
    {
        $title = \Lang::get('admin/events/titles.delete_event_type');

        return \View::make('admin/event_types/delete', compact('eventstype', 'title'));
    }

    /**
     * @param $eventstype
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete($eventstype)
    {

        if($eventstype->delete()) {
            return \Redirect::to('admin/events-type')->with('success', \Lang::get('admin/roles/messages.delete.success'));
        }

        return \Redirect::to('admin/events-type')->with('error', \Lang::get('admin/roles/messages.delete.error'));
    }
}