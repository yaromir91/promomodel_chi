<?php

namespace Acme\Controllers\Admin;

use Acme\Models\Events\Events;
use Acme\Models\Payments\UserApplicantsPayments;
use Acme\Models\Notifications;
use Acme\Models\Repositories\EventInviteRepository;
use Acme\Models\Repositories\NotificationRepository;

/**
 *
 * Class AdminPaymentsController
 *
 * @package Acme\Controllers\Admin
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class AdminPaymentsController extends \AdminController {

    /**
     * @return \Illuminate\View\View
     */
    public function getList()
    {
        $title = \Lang::get('admin/payments/titles.payments');

        return \View::make('admin/payments/index', compact('title'));
    }

    public function getUsers($event)
    {
        $title = \Lang::get('admin/payments/titles.users_list');

        $users = UserApplicantsPayments::leftjoin('users', 'user_applicants_payments.user_id', '=', 'users.id')
            ->select(array(
                'user_applicants_payments.id',
                'user_applicants_payments.status',
                'user_applicants_payments.price',
                'users.username',
                'users.payment_count'
            ))
            ->where('user_applicants_payments.event_id', '=', $event->id)->get();



        return \View::make('admin/payments/users_list', compact('title', 'users'));
    }

    public function getConfirmPayment($event) {

        $title = \Lang::get('admin/payments/titles.payments_confirm');
        $date = date('c');

        return \View::make('admin/payments/confirmPayment', compact('event', 'date', 'title'));

    }

    public function getCancelPayment($event) {

        $title = \Lang::get('admin/payments/titles.payments_cancel');
        $date = date('c');

        return \View::make('admin/payments/cancelPayment', compact('event', 'date', 'title'));
    }

    public function postAjaxSetPaid($payment)
    {
        if($payment && $payment instanceof UserApplicantsPayments) {
            $payment->status = UserApplicantsPayments::STATUS_PAID;
            $payment->save();

            return json_encode(['success' => 'Статус изменен успешно']);
        }
        return json_encode(['error' => 'Ошибка при изменении статуса']);
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $events = Events::select(array(
                'events.id',
                'events.title',
                'events.date_end',
                'events.paiment_status',
                'events.total_price'))
                ->whereIn('events.paiment_status', array(
                    Events::PAIMENT_STATUS_HOLD,
                    Events::PAIMENT_HOLD_ACCEPT,
                    Events::PAIMENT_HOLD_CANCEL
                ));

        return \Datatables::of($events)

            ->edit_column('title', '<a target="_blank" href="{{ URL::to(\'events/show/\' . $id) }}">{{ $title }}</a>')
            ->edit_column('date_end', '@if(strtotime($date_end) < time() && \Acme\Models\Repositories\PaymentsRepository::getEventPaymentStatus($id) > 0) <span style="color: {{ \Config::get(\'app.payment_color.end\') }}">{{$date_end}}</span>@elseif(\Acme\Models\Repositories\PaymentsRepository::getEventPaymentStatus($id) == 0)<span style="color: {{ \Config::get(\'app.payment_color.complete\') }}">{{$date_end}}</span>@else{{$date_end}}@endif')
            ->edit_column('paiment_status', '{{ \Acme\Models\Repositories\EventsRepository::getPaimentStatusTitle($paiment_status) }}')
            ->add_column('total_payment_status', '@if(strtotime($date_end) < time()) @if(\Acme\Models\Repositories\PaymentsRepository::getEventPaymentStatus($id) == 0)<span style="color: {{ \Config::get(\'app.payment_color.complete\') }}">Работа участников оплачена</span>@else<span style="color: {{ \Config::get(\'app.payment_color.end\') }}">Работа участников не оплачена</span>@endif @endif')
            ->add_column('actions', '
                <a href="{{ URL::to(\'admin/payments/\' . $id . \'/users\') }}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.show\') }}}</a><br />
                @if($paiment_status == \Acme\Models\Events\Events::PAIMENT_STATUS_HOLD)<a href="{{ URL::to(\'admin/payments/\' . $id . \'/confirm\') }}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.confirm\') }}}</a><br />
                <a href="{{ URL::to(\'admin/payments/\' . $id . \'/cancel\' ) }}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.cancel\') }}}</a>@endif
            ')
            ->remove_column('id')

            ->make();
    }

    public function sendConfirmRequest()
    {
        if(
            isset($_POST['requestDT']) &&
            isset($_POST['orderId']) &&
            isset($_POST['amount']) &&
            isset($_POST['currency'])
        ) {

            $event = Events::find($_POST['orderId']);

            if($event instanceof Events && $event) {
                $event->paiment_status = Events::PAIMENT_HOLD_ACCEPT;
                $event->save();
            }

            $certFile = \Config::get('app.payments._cer_path');
            $keyFile = \Config::get('app.payments._key_path');

            $data = 'requestDT=' . $_POST['requestDT'] . '&orderId=' . $event->Invoiceid . '&amount=' . $_POST['amount'] . '&currency=RUB';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, \Config::get('app.payments.payment_url') . 'confirmPayment');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_VERBOSE, false);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Ymoney CollectMoney');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSLCERT, $certFile);
            curl_setopt($ch, CURLOPT_SSLKEY, $keyFile);
            curl_setopt($ch, CURLOPT_SSLCERTPASSWD, \Config::get('app.payments._pswd'));
            curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);

            return json_encode(['msg' => $response]);

//            header('Content-Type:text/xml');
//            die($response);
        }
    }

    public function sendCancelRequest()
    {
        $response = '';
        if(
            isset($_POST['requestDT']) &&
            isset($_POST['orderId'])
        ) {

            $event = Events::find($_POST['orderId']);

            if($event instanceof Events && $event) {
                $event->paiment_status = Events::PAIMENT_HOLD_CANCEL;
                $event->save();
            }

            $certFile = \Config::get('app.payments._cer_path');
            $keyFile = \Config::get('app.payments._key_path');
            $data = 'requestDT=' . $_POST['requestDT'] . '&orderId=' . $event->Invoiceid;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, \Config::get('app.payments.payment_url') . 'cancelPayment');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_VERBOSE, false);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Ymoney CollectMoney');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSLCERT, $certFile);
            curl_setopt($ch, CURLOPT_SSLKEY, $keyFile);
            curl_setopt($ch, CURLOPT_SSLCERTPASSWD, \Config::get('app.payments._pswd'));
            curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);
        }

        $team_users = EventInviteRepository::getEventTeam($event->id);

        foreach($team_users as $us) {
            NotificationRepository::addNewNotification($event->users_id, $us->users_id, \Lang::get('notifications.msg_16'), Notifications::TYPE_EVENT);
        }

        $event->delete('\Acme\Models\Repositories\EventInviteRepository::sendLettersAboutDeleteEventToMembers');

        return json_encode(['msg' => $response]);
    }
}