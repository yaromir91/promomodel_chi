<?php

namespace Acme\Controllers\Admin;

use Acme\Models\Events\Events;
use Acme\Models\Events\EventInvite;
use Acme\Models\Professions\Professions;
use Acme\Models\Repositories\ProfessionsRepository;
use Acme\Models\Repositories\UserRepository;
use Acme\Models\User;
use Acme\Models\Role;
use Acme\Validators\PromoModelValidators;

/**
 * Class AdminEventsController
 *
 * @package Acme\Controllers\Admin
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class AdminEventsController extends \AdminController
{

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $title = \Lang::get('admin/events/titles.events_list');

        return \View::make('admin/events/index', compact('title'));
    }


    /**
     * @param $event
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($event)
    {
        $title         = \Lang::get('admin/events/titles.edit_event');
        $professions   = Professions::all();
        $profRates     = ProfessionsRepository::getEventsRates($event->id, true);
        $usersList     = UserRepository::getUsersByStatus(User::STATUS_ACTIVE, true, Role::ORGANIZER_USER);
        $tab           = \Session::get('tab');
        $tab           = !empty($tab) ? $tab : '1';
        $isSearch      = false;
        $sessionName   = 'admin_events_search_result_' . $event->id;
        $result        = \Session::get($sessionName, false);

        if ($result) {
            $isSearch      = true;
        }

        $role          = Role::getApplicantRole();
        $params        = $role->getSearchParamsForCurrentRole();
        $ranges        = \App::make('SearchApplicantServiceProvider', compact('params'));

        return \View::make('admin/events/edit', compact(
                'event',
                'professions',
                'profRates',
                'usersList',
                'tab',
                'params',
                'isSearch',
                'result',
                'ranges',
                'title'
            ));
    }


    /**
     * @return mixed
     */
    public function getCreate()
    {
        $title       = \Lang::get('admin/events/titles.event_create');
        $professions = Professions::all();
        $usersList   = UserRepository::getUsersByStatus(User::STATUS_ACTIVE, true, Role::ORGANIZER_USER);
        $tab         = 1;

        return \View::make('admin/events/edit', compact(
            'professions',
            'usersList',
            'tab',
            'title'
        ));
    }

    /**
     * @return mixed
     */
    public function postCreate()
    {
        $beforeStart = date('Y-m-d H:i:s', strtotime(' +1 day'));

        $rules = array(
            'title'                 => 'required|min:3',
            'description'           => 'required|min:3|max:571',
            'new_picture'            => 'required',
            'profession'            => 'required|array|profileprof:prof_rate_h,prof_rate_d,count',
            'date_start'            => 'required|date|after:'.$beforeStart,
            'date_end'              => 'required|date|after:date_start',
            'report_date'           => 'required|date|before:date_start|after:'.date('Y-m-d H:i:s'),
            'location'              => 'required|min:3',
            'type'                  => 'required',
            'permission_view'       => 'required',
//            'permission_invite'     => 'required'
        );

        $data      = \Input::all();
        $validator = \Validator::make($data, $rules);

        if ($validator->passes()) {
            $event = new Events();

            $savedResult = $event->saveGeneralInfo($data);

            if ($savedResult) {

                $event->addProfessionsToEvents($data, true);
                $event->getTotalPrice(true, true);

                return \Redirect::to('admin/events/' . $event->id . '/edit')->with('success', \Lang::get('admin/events/titles.edit_success'));
            } else {

                return \Redirect::to('admin/events/create')->withInput()->withErrors($event->errors);
            }
        }

        return \Redirect::to('admin/events/create')->withInput()->withErrors($validator);
    }

    /**
     * @param $event
     *
     * @return mixed
     */
    public function postEdit($event)
    {
        $rules = array(
            'title'                 => 'required|min:3',
            'description'           => 'required|min:3|max:571'
        );

        if(!count($event->invites)) {
            $beforeStart = date('Y-m-d H:i:s', strtotime(' +1 day'));
            $additional = array(
                'profession'            => 'required|array|profileprof:prof_rate_h,prof_rate_d,count',
                'date_start'            => 'required|date|after:'.$beforeStart,
                'date_end'              => 'required|date|after:date_start',
                'report_date'           => 'required|date|before:date_start|after:'.date('Y-m-d H:i:s'),
                'location'              => 'required|min:3',
                'type'                  => 'required',
                'permission_view'       => 'required',
//                'permission_invite'     => 'required'
            );
            $rules = array_merge($rules, $additional);
        }

        $data          = \Input::all();
        $validator     = \Validator::make($data, $rules);

        $isGeneralInfo = isset($data['events_general_info']) ? true : false;
        $isSearch      = isset($data['events_search']) ? true : false;

        if ($isGeneralInfo) {
            if ($validator->passes()) {
                $savedResult = $event->saveGeneralInfo($data);

                if ($savedResult) {

                    $event->addProfessionsToEvents($data, true);

                    if ($event->paiment_status == Events::PAIMENT_STATUS_NEW) {
                        $event->getTotalPrice(true, true);
                    }

                    return \Redirect::to('admin/events/' . $event->id . '/edit')->with('success', \Lang::get('admin/events/titles.edit_success'));
                } else {

                    return \Redirect::to('admin/events/' . $event->id . '/edit')->withInput()->withErrors($event->errors);
                }
            }
        } elseif ($isSearch) {

            if (isset($data['search_params'])) {
                $search_params = $data['search_params'];
                $sessionName   = 'admin_events_search_result_' . $event->id;
                \Session::put($sessionName, \App::make('PMSearchServiceProvider', compact('search_params', 'event')));
            }

            return \Redirect::to('admin/events/' . $event->id . '/edit')->with('tab', '3');
        }

        return \Redirect::to('admin/events/' . $event->id . '/edit')->withInput()->withErrors($validator);
    }


    public function getTeamData($event)
    {
        $team = EventInvite::leftJoin('users', 'event_invite.users_id', '=', 'users.id')
            ->leftJoin('professions', 'professions.id', '=', 'event_invite.professions_id')
            ->select([
                'event_invite.id',
                'event_invite.users_id',
                'users.username',
                'professions.title',
                'event_invite.in_team',
                'event_invite.manager_in_team',
                'event_invite.is_manager',
                'event_invite.manager_status',
            ])
            ->where('event_invite.status', '=', EventInvite::INVITE_PROF_STATUS_ACCEPT)
            ->where('event_invite.events_id', '=', $event->id)
            ;

        return \Datatables::of($team)

            ->edit_column('username', '<a target="_blank" href="{{ URL::route(\'view_user_profile\' , $users_id) }}">{{$username}}</a>')
            ->edit_column('in_team', '{{Acme\Models\Events\EventInvite::getCurrentTeamStatusTitle($in_team)}}')
            ->edit_column('manager_in_team', '@if($is_manager==1){{Acme\Models\Events\EventInvite::getCurrentTeamStatusTitle($manager_in_team)}}@endif')


            ->remove_column('users_id')
            ->remove_column('is_manager')
            ->remove_column('manager_status')
            ->remove_column('id')

            ->make();
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $events = Events::leftJoin('users', 'events.users_id', '=', 'users.id')
            ->leftJoin('events_type', 'events.type_id', '=', 'events_type.id')
            ->select(array(
                'events.id',
                'events.title',
                'events.image',
                'events_type.title as event_type_title',
                'users.username',
                'events.users_id',
                'events.location',
                'events.date_start',
                'events.date_end',
            ));

        return \Datatables::of($events)

            ->edit_column('username', '<a target="_blank" href="{{ URL::route(\'view_user_profile\' , $users_id) }}">{{$username}}</a>')
            ->edit_column('actions', '<a href="{{ URL::to(\'admin/events/\' . $id . \'/edit\' ) }}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                      <a href="{{ URL::to(\'admin/events/\' . $id . \'/delete\' ) }}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')
            ->edit_column('image', '@if($image) <img width="140" alt="{{$title}}" src="{{ asset(\Config::get(\'app.image_dir.events_small\') . $image) }}"> @endif')
            ->remove_column('users_id')
            ->remove_column('id')

            ->make();
    }

    /**
     * Remove event
     *
     * @param $event
     * @return Response
     */
    public function getDelete($event)
    {
        $title = \Lang::get('admin/events/titles.delete_event');

        return \View::make('admin/events/delete', compact('event', 'title'));
    }


    /**
     * @param $event
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete($event)
    {
        if($event->delete('\Acme\Models\Repositories\EventInviteRepository::sendLettersAboutDeleteEventToMembers')) {
            return \Redirect::to('admin/events')->with('success', \Lang::get('admin/events/messages.delete.success'));
        }

        return \Redirect::to('admin/events')->with('error', \Lang::get('admin/events/messages.delete.error'));
    }
}