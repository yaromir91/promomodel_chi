<?php

namespace Acme\Controllers\Admin;

use Acme\Models\Profile\ProfileParams;
use Acme\Models\Post;


class AdminProfileParamsController extends \AdminController {


    /**
     * Profile Params Model
     * @var Params
     */
    protected $params;

    /**
     * Inject the models.
     * @param ProfileParams $params
     */
    public function __construct(ProfileParams $params)
    {
        parent::__construct();
        $this->params = $params;

    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title  = \Lang::get('admin/users/title.profile_params');
        $params = $this->params;

        return \View::make('admin/params/index', compact('params', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $parent
     *
     * @return \Illuminate\View\View
     */
    public function getCreate($parent = null)
    {
        $title  = \Lang::get('admin/params/titles.new_profile_params');
        $mode   = 'create';
        $tab    = 1;

        return \View::make('admin/params/create_edit', compact('title', 'parent', 'mode', 'tab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        $rules = array(
            'name'    => 'required|min:1',
            'name_en' => 'required|min:1',
        );

        $validator = \Validator::make(\Input::all(), $rules);
        $parentId  = \Input::get('parent_id', null);
        $returnUrl = ($parentId) ? ('admin/params/create/' . $parentId) : 'admin/params/create/';

        if ($validator->passes())
        {
            $params                  = new ProfileParams();
            $params->name            = \Input::get('name');
            $params->name_en         = \Input::get('name_en');
            $params->type            = \Input::get('type');
            $params->parent_id       = $parentId;

            if($params->save())
            {
                return \Redirect::to('admin/params/' . $params->id . '/edit')->with('success', \Lang::get('admin/blogs/messages.update.success'));
            }

            return \Redirect::to('admin/params/' . $params->id . '/edit')->with('error', \Lang::get('admin/blogs/messages.update.error'));
        }

        return \Redirect::to($returnUrl)->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $params
     */
    public function getShow($params)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $param
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($param)
    {
        $title = \Lang::get('admin/params/titles.update_params_title');
        $tab   = 1;

        return \View::make('admin/params/param_edit', compact(
                'param',
                'tab',
                'title'
            ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $params
     * @return Response
     */
    public function postEdit($params)
    {

        // Declare the rules for the form validation
        $rules = array(
            'name'    => 'required|min:1',
            'name_en' => 'required|min:1',
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $params->name            = \Input::get('name');
            $params->name_en         = \Input::get('name_en');
            $params->type            = \Input::get('type');

            if($params->save())
            {
                // Redirect to the new blog post page
                return \Redirect::to('admin/params/' . $params->id . '/edit')->with('success', \Lang::get('admin/blogs/messages.update.success'));
            }

            // Redirect to the blogs post management page
            return \Redirect::to('admin/params/' . $params->id . '/edit')->with('error', \Lang::get('admin/blogs/messages.update.error'));
        }

        // Form validation failed
        return \Redirect::to('admin/params/' . $params->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $param
     * @return Response
     */
    public function getDelete($param)
    {
        $title = \Lang::get('admin/params/titles.param_delete');

        // Show the page
        return \View::make('admin/params/delete', compact('param', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $params
     * @return \Response
     */
    public function postDelete($params)
    {
        $rules = array(
            'id' => 'required|integer'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $id = $params->id;
            $params->delete();
        }

        return \Redirect::to('admin/blogs')->with('error', \Lang::get('admin/blogs/messages.delete.error'));
    }

    /**
     * @param $param
     *
     * @return mixed
     */
    public function getDataChild($param)
    {
        $params = ProfileParams::select([
                'profile_params.id',
                'profile_params.parent_id',
                'profile_params.name',
                'profile_params.name_en',
                'profile_params.type'
            ])
            ->where('parent_id', '=', $param->id)
            ->orderBy('num', 'asc')
        ;

        return \Datatables::of($params)


            ->edit_column('type','{{Acme\Models\Profile\ProfileParams::getTypeNames($type)}}')

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/params/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/params/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
            ')
            ->set_index_column('id')

            ->remove_column('parent_id')

            ->make();
    }

    /**
     * Set order to parameters
     */
    public function getSetorder()
    {
        $posData    = \Input::get('ordData', '');
        $activePage = \Input::get('activePage', 1);
        $perPage    = \Input::get('perPage', 10);

        $i = $perPage * ($activePage - 1);
        if (!empty($posData)) {
           foreach ($posData as $od) {
               $param = ProfileParams::find($od);
               if ($param instanceof ProfileParams) {
                   $param->num = $i;
                   $param->save();
               }
               $i++;
           }
        }
    }

    /**
     * Show a list of all the blog posts formatted for Datatables.
     *
     * @return \Datatables JSON
     */
    public function getData()
    {
        $params = ProfileParams::select([
                'profile_params.id',
                'profile_params.parent_id',
                'profile_params.name',
//                'profile_params.name_en',
                'profile_params.type'
            ])
            ->whereNull('parent_id')
            ->orderBy('num', 'asc')
        ;

        return \Datatables::of($params)

            ->edit_column('type','{{Acme\Models\Profile\ProfileParams::getTypeNames($type)}}')

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/params/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/params/\' . $id . \'/delete\' ) }}}" class="btn btn-xs  btn-danger cboxElement iframe">{{{ Lang::get(\'button.delete\') }}}</a>
            ')

            ->set_index_column('id')

            ->remove_column('parent_id')

            ->make();
    }
}