<?php

namespace Acme\Controllers\Admin;

use Acme\Models\Professions\Professions;
use Mockery\Generator\Parameter;

/**
 * Class AdminProfessionController
 *
 * @property \Acme\Models\Professions\Professions profession
 *
 * @package Acme\Models\Controllers\Admin
 *
 * @author Elena Zhuga <elena.zhuga@chisw.com>
 */
class AdminProfessionController extends \AdminController
{

    /**
     * Professions Model
     * @var Professions
     */
    protected $profession;

    /**
     * Display a listing of the professions.
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $title = \Lang::get('admin/profession/titles.professions');

        return \View::make('admin/profession/index', compact('title'));
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $roles = Professions::select(array('professions.id', 'professions.title', 'professions.title_en'));

        return  \Datatables::of($roles)

            ->edit_column('actions', '<a href="{{{ URL::to(\'admin/profession/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                      <a href="{{{ URL::to(\'admin/profession/\' . $id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')
            ->remove_column('id')

            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        $title = \Lang::get('admin/profession/titles.create_a_new_profession');

        return \View::make('admin/profession/create', compact('title'));
    }


    /**
     * @param $profession
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($profession)
    {
        $title = \Lang::get('admin/profession/titles.edit_profession');

        return \View::make('admin/profession/edit', compact('profession', 'title'));
    }

    /**
     * @param $profession
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postEdit($profession)
    {
        $validator = \Validator::make(\Input::all(), ['title' => 'required', 'title_en' => 'required']);

        if ($validator->passes())
        {
            $profession->title = \Input::get('title');
            $profession->title_en = \Input::get('title_en');

            if ($profession->save()) {
                return \Redirect::to('admin/profession/' . $profession->id . '/edit')->with('success', \Lang::get('admin/roles/messages.create.success'));
            }

        }

        return \Redirect::to('admin/profession/' . $profession->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate()
    {
        $validator = \Validator::make(\Input::all(), ['title' => 'required', 'title_en' => 'required']);

        if ($validator->passes())
        {
            $this->profession        = new Professions();
            $this->profession->title = \Input::get('title');
            $this->profession->title_en = \Input::get('title_en');

            if ($this->profession->save()) {
                return \Redirect::to('admin/profession/' . $this->profession->id . '/edit')->with('success', \Lang::get('admin/roles/messages.create.success'));
            }

            return \Redirect::to('admin/profession/create')->with('errors', $this->profession->errors()->all());

        }

        return \Redirect::to('admin/profession/create')->withInput()->withErrors($validator);
    }

    /**
     * @param $profession
     *
     * @return \Illuminate\View\View
     */
    public function getDelete($profession)
    {
        $title = \Lang::get('admin/profession/titles.delete_profession');

        return \View::make('admin/profession/delete', compact('profession', 'title'));
    }

    /**
     * @param $profession
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete($profession)
    {

        if($profession->delete()) {
            return \Redirect::to('admin/profession')->with('success', \Lang::get('admin/roles/messages.delete.success'));
        }

        return \Redirect::to('admin/profession')->with('error', \Lang::get('admin/roles/messages.delete.error'));
    }
}