<?php

namespace Acme\Controllers\Admin;

use Acme\Models\User;
use Acme\Models\Role;
use Acme\Models\Permission;
use Acme\Models\AssignedRoles;

use Acme\Models\Repositories\EventsTypeRepository;
use Acme\Models\Repositories\ProfessionsRepository;
use Acme\Models\Repositories\UserRepository;

use Acme\Validators\Profile\ProfileValidators;

class AdminUsersController extends \AdminController {


    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @var array
     */
    private $profileForm = [
        Role::ADMIN_USER             => 'admin/users/create_edit',
        Role::AGENT_USER             => 'admin/users/create_edit',
        Role::APPLICANT_USER         => 'admin/users/applicant/form',
        Role::APPLICANT_MANAGER_USER => 'admin/users/applicant/form',
        Role::ORGANIZER_USER         => 'admin/users/organizer/form',
    ];

    /**
     * @var string
     */
    private $defaultTemplate = 'admin/users/create_edit';

    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission)
    {
        parent::__construct();
        $this->user       = $user;
        $this->role       = $role;
        $this->permission = $permission;
        $this->userRepo   = new UserRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = \Lang::get('admin/users/title.user_management');

        // Grab all the users
        $users = $this->user;

        // Show the page
        return \View::make('admin/users/index', compact('users', 'title'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getSettingsList()
    {
        $title = \Lang::get('admin/users/title.user_management');

        return \View::make('admin/users/settings_list', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // All roles
        $roles = $this->role->all();

        // Get all the available permissions
        $permissions = $this->permission->all();

        // Selected groups
        $selectedRoles = \Input::old('roles', array());

        // Selected permissions
        $selectedPermissions = \Input::old('permissions', array());

		// Title
		$title = \Lang::get('admin/users/title.create_a_new_user');

		// Mode
		$mode = 'create';

		// Show the page
		return \View::make('admin/users/new_user', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        $confirmed  = (\Input::get('confirm')) ? \Input::get('confirm') : 0;
        $this->user = $this->userRepo->signup(\Input::all(), $confirmed);

        if ($this->user->id) {
            $profileSaved = $this->userRepo->addProfileRoleToUser($this->user);
            $this->user->saveRole(\Input::get('account_type', ''));

            if ($profileSaved) {
                if (\Config::get('confide::signup_email')) {
                    $user     = $this->user;
                    $password = \Input::get('password', '');

                    \Mail::queueOn(
                        \Config::get('confide::email_queue'),
                        'emails.auth.user_password',
                        compact('user', 'password'),
                        function (\Illuminate\Mail\Message $message) use ($user) {
                            $message
                                ->to($user->email, $user->username)
                                ->subject(\Lang::get('confide::confide.email.account_confirmation.subject'));
                        }
                    );
                }

                return \Redirect::to('admin/users/' . $this->user->id . '/edit')
                    ->with('success', \Lang::get('admin/users/messages.create.success'));
            }
        }  else {
            $error = $this->user->errors()->all();

            return \Redirect::to('admin/users/create')
                ->withInput(\Input::except('password'))
                ->with( 'error', $error );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getShow($user)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($user)
    {
        if ( $user->id )
        {
            $tab         = \Session::get('tab');
            $tab         = !empty($tab) ? $tab : '1';
            $profile     = $user->getOrCreateProfile();
            $role        = $user->getCurrentUserRole();
            $roles       = $this->role->all();
            $permissions = $this->permission->all();

            $htmlTemplate = $this->getHtmlTemplate($this->profileForm, $user->getMainUserRoleCode());

            if (!$htmlTemplate ||
                (
                    (
                        $user->getMainUserRoleCode()==\Acme\Models\Role::APPLICANT_USER ||
                        $user->getMainUserRoleCode()==\Acme\Models\Role::APPLICANT_MANAGER_USER
                    ) &&
                    !($profile instanceof \Acme\Models\Profile\UserProfile)
                )
               ) {
                $htmlTemplate = $this->defaultTemplate;
            }

        	$title       = \Lang::get('admin/users/title.user_update');
        	$mode        = 'edit';
            $eventsType  = EventsTypeRepository::getEvensTypes();
            $professions = ProfessionsRepository::getAllProfessions();
            $mainParams  = (is_object($role)) ? $role->getProfileParamsForCurrentRole() : null;

        	return \View::make($htmlTemplate, compact(
                    'user',
                    'roles',
                    'permissions',
                    'title',
                    'mode',
                    'eventsType',
                    'professions',
                    'mainParams',
                    'tab',
                    'profile'
                ));
        }
        else
        {
            return \Redirect::to('admin/users')->with('error', \Lang::get('admin/users/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param User $user
     * @return Response
     */
    public function postEdit($user)
    {
        $input          = \Input::all();
        $mainparams     = \Input::get('mainparams');
        $additionparams = \Input::get('additionparams');
        $setpassword    = \Input::get('set_password');

        \Input::flash();

        if (!empty($mainparams)) {
            $validator = ProfileValidators::getAdminApplicantGeneralFormValidator($input, $user->id, $user->getMainUserRoleCode());

            if ($validator->passes()) {

                $saved = $user->saveGeneralInfo($input);
                $user->saveRole(\Input::get('roles', ''));

                if ($saved) {
                    return \Redirect::to('admin/users/' . $user->id . '/edit')->with('success', \Lang::get('profile.profile_was_successfully_saved'));
                }
            }

            return \Redirect::to('admin/users/' . $user->id . '/edit')->withErrors($validator);
        }

        if (!empty($setpassword)) {
            $validator = ProfileValidators::getPasswordFormValidator($input, $user->id, $user->profile->fb_status, true);

            if ($validator->passes()) {
                if($user->savePassword($input)) {
                    \Session::flash('success',  \Lang::get('profile.password_was_successfully_saved'));
                } else {
                    \Session::flash('error',  \Lang::get('profile.error_in_password_update'));
                }
            } else {
                \Session::flash('errors',  $validator->errors());
            }

            return \Redirect::to('admin/users/' . $user->id . '/edit')->with('tab', '3');
        }

        if (!empty($additionparams)) {
            $validator = ProfileValidators::getApplicantAdditionFormValidator($input, $user->id);

            if ($validator->passes()) {

                // attach to User Events Type
                $user->attachEventTypes(!empty($input['eventtypes']) ? $input['eventtypes'] :[]);

                // attach to User Professionals Rate
                $user->attachProfessionsToProfile(
                    !empty($input['professions']) ? $input['professions'] :[],
                    !empty($input['prof_rate_d']) ? $input['prof_rate_d'] :[],
                    !empty($input['prof_rate_h']) ? $input['prof_rate_h'] :[]
                );

                // fill User Profile Parameters
                if (!empty($input['profile_params']) && !empty($input['param_type'])) {
                    $user->fillProfileParams($input['profile_params'], $input['param_type']);
                }

                \Session::flash('success',  \Lang::get('profile.profile_was_successfully_saved'));
            } else {
                \Session::flash('errors',  $validator->errors());
            }

            return \Redirect::to('admin/users/' . $user->id . '/edit')->with('tab', '2');
        }

        // Get validation errors (see Ardent package)
        $error = $user->errors()->all();

        if(empty($error)) {
            // Redirect to the new user page
            return \Redirect::to('admin/users/' . $user->id . '/edit')->with('success', \Lang::get('admin/users/messages.edit.success'));
        } else {
            return \Redirect::to('admin/users/' . $user->id . '/edit')->with('error', \Lang::get('admin/users/messages.edit.error'));
        }
    }

    /**
     * Remove user page.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($user)
    {
        // Title
        $title = \Lang::get('admin/users/title.user_delete');

        // Show the page
        return \View::make('admin/users/delete', compact('user', 'title'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete($user)
    {
        // Check if we are not trying to delete ourselves
        if ($user->id === \Confide::user()->id)
        {
            // Redirect to the user management page
            return \Redirect::to('admin/users')->with('error', \Lang::get('admin/users/messages.delete.impossible'));
        }

        AssignedRoles::where('user_id', $user->id)->delete();

        $id = $user->id;
        $user->delete('\Acme\Models\Repositories\UserRepository::sendLettersAboutDeleteUser');

        // Was the comment post deleted?
        $user = User::find($id);
        if ( empty($user) )
        {
            // TODO needs to delete all of that user's content
            return \Redirect::to('admin/users')->with('success', \Lang::get('admin/users/messages.delete.success'));
        }
        else
        {
            // There was a problem deleting the user
            return \Redirect::to('admin/users')->with('error', \Lang::get('admin/users/messages.delete.error'));
        }
    }

    /**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $users = User::leftjoin('assigned_roles', 'assigned_roles.user_id', '=', 'users.id')
                    ->leftjoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
                    ->select(array('users.id', 'users.username','users.email', 'roles.name as rolename', 'users.confirmed', 'users.created_at', 'users.id as actions'));

        return \Datatables::of($users)
        ->edit_column('confirmed','@if($confirmed)
                            Yes
                        @else
                            No
                        @endif')


        ->edit_column('actions', '<a href="{{{ URL::to(\'admin/users/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ \Lang::get(\'button.edit\') }}}</a>
                                @if($rolename == \'admin\')
                                @else
                                    <a href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ \Lang::get(\'button.delete\') }}}</a>
                                @endif
            ')
        ->remove_column('id')

        ->make();
    }

    /**
     * @param $tmpArr
     * @param $roleId
     *
     * @return bool
     */
    protected function getHtmlTemplate($tmpArr, $roleId)
    {
        if (count($tmpArr) && isset($tmpArr[$roleId])) {

            return $tmpArr[$roleId];
        }

        return $this->defaultTemplate;
    }
}
