<?php

namespace Acme\Controllers\Admin;

use Acme\Models\Comment;
use Acme\Models\Events\Events;
use Acme\Models\Gallery;
use Acme\Models\Post;
use Acme\Models\Repositories\EventsRepository;
use Acme\Models\Repositories\TagsRepository;
use Acme\Models\Tags;
use Acme\Models\Repositories\UserRepository;
use Acme\Models\Repositories\GalleryImagesRepository;
use Acme\Models\User;
use Acme\Models\GalleryImages;

/**
 *
 * Class AdminReportsController
 *
 * @package Acme\Controllers\Admin
 *
 * @property
 *
 * @author  Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class AdminReportsController extends \AdminController
{
    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $title = \Lang::get('admin/reports/titles.reports_list');

        return \View::make('admin/reports/index', compact('title'));
    }

    public function getCreate()
    {
        $title = \Lang::get('admin/reports/titles.create_report');

        $events = \DB::table('events')->lists('title', 'id');
        $choose = [0 => \Lang::get('report.choose_event')];
        $events = array_replace($choose, $events);

        return \View::make('admin/reports/create', compact(
            'title',
            'events',
            'choose'
        ));
    }

    public function postCreate()
    {
        $rules = array(
            'events'   => 'required|not_in:0',
            'title'    => 'required|min:3',
//            'tags'     => 'required|min:3',
            'content'  => 'required|min:3'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $name = '';
            $data = \Input::all();

            $destinationPath        = \Config::get('app.image_dir.posts');
            $destinationSmallPath   = \Config::get('app.image_dir.posts_small');

            if(isset($data['image'])) {
                $file = $data['image'];
                $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
            }

            $post = new Post();

            $post->user_id          = $this->user->id;
            $post->event_id         = \Input::get('events');
            $post->title            = \Input::get('title');
            $post->slug             = \Slug::make(\Input::get('title'));
            $post->content          = \Input::get('content');
            $post->image            = $name;
            $post->status          = (\Input::get('status'))?1:0;

            if($data['new_picture']) {
                $post->image       = $data['new_picture'];
            }

            if($post->save())
            {


                if(isset($data['image'])) {
                    if (!file_exists($destinationPath)) {
                        \File::makeDirectory($destinationPath, 0777, true);
                    }
                    $data['image']->move($destinationPath, $name);
                }

//                $data_tags = explode(',', \Input::get('tags'));
//
//                foreach($data_tags as $data_tag)
//                {
//                    $check_t = Tags::Name($data_tag)->first();
//                    if($check_t)
//                    {
//                        $post->tags()->attach($check_t->id);
//                    }
//                    else
//                    {
//                        $new_tag = new Tags;
//                        $new_tag->name = $data_tag;
//                        $new_tag->save();
//                        $post->tags()->attach($new_tag->id);
//                    }
//                }

                return \Redirect::to('admin/reports/edit/' . $post->id)->with('success', \Lang::get('report/messages.create.success'));
            }

            return \Redirect::to('admin/reports/create')->withInput()->with('error', \Lang::get('report/messages.create.error'));
        }

        return \Redirect::to('admin/reports/create')->withInput()->withErrors($validator);
    }

    /**
     * @param $report
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($report)
    {
        $title = \Lang::get('admin/reports/titles.update_reports');

        $title_comments = \Lang::get('admin/reports/titles.comments_list');

//        $currentTags = TagsRepository::getCurrentPostTags($report->id);
//        $currentTags = implode(',', $currentTags);

        $tab   = 1;

        $event = EventsRepository::getEventForReport($report->event_id);

        $comments = $report->comments()->orderBy('created_at', 'ASC')->get();


        return \View::make('admin/reports/edit', compact(
            'report',
            'comments',
            'title_comments',
            'tab',
            'currentTags',
            'title',
            'event'
        ));
    }

    public function postEdit($report)
    {
        $rules = array(
            'title'   => 'required|min:3',
//            'tags'    => 'required|min:3',
            'content' => 'required|min:3'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $data                   = \Input::all();
            $name                   = $report->image;
            $destinationPath        = \Config::get('app.image_dir.posts');
            $destinationSmallPath   = \Config::get('app.image_dir.posts_small');
            $destinationPrevPath    = \Config::get('app.image_dir.posts_preview');

            if(isset($data['image'])) {
                $file = $data['image'];
                $name = \Str::random(20) . '.' . $file->getClientOriginalExtension();
            }
            $old_image = $report->image;

            $report->title            = $data['title'];
            $report->content          = $data['content'];
            $report->image            = $name;
            $report->status          = (null !== \Input::get('status')) ? 1 : 0;

            if($data['new_picture']) {

                if (trim($data['new_picture']) != $old_image) {
                    \File::delete($destinationPath . '/' . $old_image);
                    \File::delete($destinationSmallPath . '/' . $old_image);
                    \File::delete($destinationPrevPath . '/' . $old_image);
                }
                $report->image       = $data['new_picture'];
            }

            if($report->save())
            {

                if(isset($data['image'])) {
                    if (\File::exists($destinationPath)) {
                        \File::delete($destinationPath . $old_image);
                    }

                    if (!file_exists($destinationPath)) {
                        \File::makeDirectory($destinationPath, 0777, true);
                    }
                    $data['image']->move($destinationPath, $name);
                }

//                TagsRepository::clearPostTags($report->id);
//                $data_tags = explode(',', \Input::get('tags'));
//
//                foreach($data_tags as $data_tag)
//                {
//                    $check_t = Tags::Name($data_tag)->first();
//                    if($check_t)
//                    {
//                        $report->tags()->attach($check_t->id);
//                    }
//                    else
//                    {
//                        $new_tag = new Tags;
//                        $new_tag->name = $data_tag;
//                        $new_tag->save();
//                        $report->tags()->attach($new_tag->id);
//                    }
//                }
                return\Redirect::back()->with('success', \Lang::get('admin/blogs/messages.update.success'));
            }

            return \Redirect::to('admin/reports/edit/' . $report->id)->withInput()->with('error', \Lang::get('report/messages.create.error'));
        }

        return \Redirect::to('admin/reports/edit/' . $report->id)->withInput()->withErrors($validator);
    }

    /**
     * @param $report
     *
     * @return \Illuminate\View\View
     */
    public function getDelete($report)
    {
        $title = \Lang::get('admin/reports/titles.delete_reports');

        return \View::make('admin/reports/delete_report', compact('report', 'title'));
    }

    public function postDelete($report)
    {
        $rules = array(
            'id' => 'required|integer'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $report->delete();
        }
    }

    public function getCommentCreate($report)
    {
        $title = \Lang::get('admin/reports/titles.create_comment');

        return \View::make('admin/reports/create_comment', compact(
            'title',
            'report'
        ));
    }

    public function postCommentCreate()
    {
        $rules = array(
            'content'   => 'required',
            'post_id'   => 'required'
        );

        // Validate the inputs
        $validator = \Validator::make(\Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $comment = new Comment();

            $comment->user_id = $this->user->id;
            $comment->post_id = \Input::get('post_id');
            $comment->content = \Input::get('content');

            if($comment->save())
            {
                return \Redirect::to('admin/reports/comments/edit/' . $comment->id)->with('success', \Lang::get('admin/reports/messages.save.success'));
            }
            return \Redirect::back()->with('error', \Lang::get('report.comment.save_error'));
        }
        return \Redirect::back()->with('error', \Lang::get('report.comment.validation_error'));
    }

    public function getCommentEdit($comment)
    {
        $title = \Lang::get('admin/reports/titles.edit_comment');


        return \View::make('admin/reports/edit_comment', compact(
            'title',
            'comment'
        ));
    }

    public function postCommentEdit($comment)
    {
        $rules = array(
            'content' => 'required'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $comment->content = \Input::get('content');
            if($comment->save()) {
                return\Redirect::back()->with('success', \Lang::get('admin/blogs/messages.update.success'));
            }
        }
        return \Redirect::to('admin/reports/comments/edit/' . $comment->id)->withInput()->with('error', \Lang::get('admin/reports/messages.edit_error'));
    }

    public function getCommentDelete($comment)
    {
        $title = \Lang::get('admin/reports/titles.delete_comment');

        return \View::make('admin/reports/delete_comment', compact('comment', 'title'));
    }

    public function postCommentDelete($comment)
    {
        $rules = array(
            'id' => 'required|integer'
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->passes())
        {
            $comment->delete();
        }

        return\Redirect::to('admin/reports/edit/' . $comment->post_id)->with('success', \Lang::get('admin/reports/messages.delete.success'));
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $reports = Post::leftJoin('users', 'posts.user_id', '=', 'users.id')
            ->leftJoin('events', 'posts.event_id', '=', 'events.id')
            ->select(array(
                'posts.id',
                'posts.slug',
                'posts.user_id',
                'posts.title AS post_title',
                'users.username',
                'events.title',
                'posts.image',
                'posts.status',
                'posts.created_at',
                'events.id as events_id',
            ));
        return  \Datatables::of($reports)

            ->edit_column('post_title', '<a target="_blank" href="{{{ URL::to(\'report/show/\' . $slug . \'/\' ) }}}">{{$post_title}}</a>')
            ->edit_column('username', '<a target="_blank" href="{{{ URL::to(\'profile/view/\' . $user_id . \'/\' ) }}}">{{$username}}</a>')
            ->edit_column('title', '<a target="_blank" href="{{{ URL::to(\'events/show/\' . $events_id . \'/\' ) }}}">{{$title}}</a>')
            ->edit_column(
                'status',
                '@if($status) druft @else published @endif'
            )
            ->edit_column('image','@if($image)
                            <img width="100px" src="{{ asset(\Config::get(\'app.image_dir.posts\') .$image) }}">
                        @endif')
            ->edit_column('actions', '<a href="{{ URL::to(\'admin/reports/edit/\' . $id ) }}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                      <a href="{{URL::to(\'admin/reports/delete/\' . $id) }}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')
            ->remove_column('user_id')
            ->remove_column('id')
            ->remove_column('events_id')
            ->remove_column('slug')

            ->make();
    }

    public function getDataComments($report)
    {
        $comments = Comment::select(array(
                'comments.id',
                'comments.user_id',
                'users.username',
                'comments.post_id',
                'comments.content',
                'comments.updated_at',
            ))
            ->leftJoin('users', 'comments.user_id', '=', 'users.id')
            ->where('comments.post_id', '=', $report->id);

        return  \Datatables::of($comments)

            ->edit_column('username', '<a target="_blank" href="{{{ URL::to(\'profile/view/\' . $user_id . \'/\' ) }}}">{{$username}}</a>')
            ->edit_column('actions', '<a href="{{ URL::to(\'admin/reports/comments/edit/\' . $id ) }}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                      <a href="{{URL::to(\'admin/reports/comments/delete/\' . $id) }}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
            ')
            ->remove_column('id')
            ->remove_column('user_id')
            ->remove_column('post_id')

            ->make();
    }
}