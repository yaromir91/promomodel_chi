<?php

namespace Acme\Controllers\Admin;

use Acme\Models\Partners;


class AdminPartnersController extends \AdminController
{

    /**
     * @var array
     */
    protected $partnersValidation = [
        'title' => 'required|min:5|max:76',
        'description' => 'max:570'
    ];

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $title = \Lang::get('admin/partners/title.partners_list');

        return \View::make('admin/partners/index', compact('title'));
    }


    /**
     * @return mixed
     */
    public function getData()
    {
        $partners = Partners::select(array(
                'partners.id',
                'partners.title',
                'partners.url',
                'partners.email',
                'partners.active',
                'partners.updated_at'
            ));

        return \Datatables::of($partners)
            ->edit_column('url', '<a href="{{ $url }}" target="_blank">{{ $url }}</a>')
            ->edit_column('active', '@if($active) {{ Lang::get(\'admin/partners/form.active_yes\') }} @else {{ Lang::get(\'admin/partners/form.active_no\') }} @endif')
            ->edit_column('actions', '<a href="{{ URL::to(\'admin/partners/\' . $id . \'/edit\' ) }}" class="iframe btn btn-xs btn-default">{{ Lang::get(\'button.edit\') }}</a>
                                      <a href="{{ URL::to(\'admin/partners/\' . $id . \'/delete\' ) }}" class="iframe btn btn-xs btn-danger">{{ Lang::get(\'button.delete\') }}</a>
            ')
            ->remove_column('id')

            ->make();
    }

    /**
     * @return mixed
     */
    public function getCreate()
    {
        $title       = \Lang::get('admin/partners/title.create');

        return \View::make('admin/partners/edit', compact(
            'title'
        ));
    }


    /**
     * @param $group
     *
     * @return \Illuminate\View\View
     */
    public function getEdit($partners)
    {
        $title       = \Lang::get('admin/partners/title.update');

        return \View::make('admin/partners/edit', compact(
            'partners',
            'title'
        ));
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate()
    {
        $data      = \Input::all();
        $validator = \Validator::make($data, $this->partnersValidation);

        if ($validator->passes()) {

            $partner    = new Partners();
            $save = $partner->saveInfo($data);

            if ($save['status']==0) {

                return \Redirect::to('admin/partners/' . $partner->id . '/edit')->with('error', $save['error']);
            }

            return \Redirect::to('admin/partners/' . $partner->id . '/edit')->with('success', \Lang::get('admin/partners/messages.update.success'));

        }

        return \Redirect::back()->withErrors($validator)->withInput();
    }


    /**
     * @param $group
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postEdit($partner)
    {
        $data      = \Input::all();
        $validator = \Validator::make($data, $this->partnersValidation);

        if ($validator->passes()) {
            $savedRes = $partner->saveInfo($data);

            if ($savedRes['status']==0) {

                return \Redirect::to('admin/partners/' . $partner->id . '/edit')->with('error', $savedRes['error']);
            }

            return \Redirect::to('admin/partners/' . $partner->id . '/edit')->with('success', \Lang::get('admin/partners/messages.update.success'));
        }

        return \Redirect::back()->withErrors($validator)->withInput();
    }

    /**
     * @param $group
     * @return \Illuminate\View\View
     */
    public function getDelete($partner)
    {
        $title = \Lang::get('admin/partners/title.delete');

        return \View::make('admin/partners/delete', compact('partner', 'title'));
    }


    /**
     * @param $group
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete($partner)
    {
        if($partner->delete()) {
            return \Redirect::to('admin/partners')->with('success', \Lang::get('admin/partners/messages.delete.success'));
        }

        return \Redirect::to('admin/partners')->with('error', \Lang::get('admin/partners/messages.delete.error'));
    }
}