<?php

namespace Acme\Controllers\Admin;

use Acme\Models\User;
use Acme\Models\Role;
use Acme\Models\Permission;
use Acme\Models\AssignedRoles;

use Acme\Models\Repositories\EventsTypeRepository;
use Acme\Models\Repositories\ProfessionsRepository;
use Acme\Models\Repositories\UserRepository;

use Acme\Validators\Profile\ProfileValidators;
use Illuminate\Support\Facades\DB;

class AdminAgentsController extends \AdminController
{


    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @var array
     */
    private $profileForm = [
        Role::ADMIN_USER             => 'admin/users/create_edit',
        Role::AGENT_USER             => 'admin/users/create_edit',
        Role::APPLICANT_USER         => 'admin/users/applicant/form',
        Role::APPLICANT_MANAGER_USER => 'admin/users/applicant/form',
        Role::ORGANIZER_USER         => 'admin/users/organizer/form',
    ];

    /**
     * @var string
     */
    private $defaultTemplate = 'admin/users/create_edit';

    /**
     * Inject the models.
     *
     * @param User       $user
     */
    public function __construct(User $user, Role $role, Permission $permission)
    {
        parent::__construct();
        $this->user       = $user;
        $this->role       = $role;
        $this->permission = $permission;
        $this->userRepo   = new UserRepository();
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $agents = \DB::table('users')
            ->leftjoin('assigned_roles', 'assigned_roles.user_id', '=', 'users.id')
            ->leftjoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
            ->where('users.confirmed', '!=', User::CONFIRM_STATUS_NOT)
            ->where('roles.code', '=', \Acme\Models\Role::AGENT_USER)
            ->select(array('users.id', 'users.username','users.email', 'users.confirmed'));

        return \Datatables::of($agents)
            ->edit_column('confirmed', '@if($confirmed == ' . User::CONFIRM_STATUS_YES . ') Не активен @else Активен @endif')
            ->add_column('actions', ''
                    . '@if($confirmed == ' . User::CONFIRM_STATUS_YES . ')<a href="{{{ URL::to(\'admin/agents/\' . $id . \'/activate\' ) }}}" class="iframe btn btn-default btn-xs">{{{ \Lang::get(\'button.activate\') }}}</a>'
                    . '@elseif($confirmed == ' . User::CONFIRM_STATUS_ACTIVE . ')<a href="{{{ URL::to(\'admin/agents/\' . $id . \'/deactivate\' ) }}}" class="iframe btn btn-default btn-xs">{{{ \Lang::get(\'button.deactivate\') }}}</a>@endif')
            ->remove_column('id')
            ->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = \Lang::get('admin/agents/title.user_management');

        // Grab all the users
        $users = $this->user;

        // Show the page
        return \View::make('admin/agents/index', compact('users', 'title'));
    }

    public function getActivate($user)
    {
        // Title
        $title = \Lang::get('admin/agents/title.agent_activate');

        // Show the page
        return \View::make('admin/agents/activate', compact('user', 'title'));
    }

    public function postActivate($user)
    {
        $user->agentActivate();

        if ( $user->checkAgentStatus() )
        {
            return \Redirect::to('admin/agents')->with('success', \Lang::get('admin/agents/messages.activate.success'));
        }
        else
        {
            // There was a problem deleting the user
            return \Redirect::to('admin/agents')->with('error', \Lang::get('admin/users/messages.activate.error'));
        }
    }
    
    public function getDeactivate($user)
    {
        // Title
        $title = \Lang::get('admin/agents/title.agent_deactivate');

        // Show the page
        return \View::make('admin/agents/deactivate', compact('user', 'title'));
    }
    
    public function postDeactivate($user)
    {
        $result = $user->agentDeactivate();

        if ( $result )
        {
            return \Redirect::to('admin/agents')->with('success', \Lang::get('admin/agents/messages.deactivate.success'));
        }
        else
        {
            // There was a problem deleting the user
            return \Redirect::to('admin/agents')->with('error', \Lang::get('admin/users/messages.deactivate.error'));
        }
    }
}