<?php

namespace Acme\Controllers;


class StaticPageController extends \BaseController {

    /**
     * Display a static page.
     * GET /page
     *
     * @return Response
     */
    public function getIndex($page = null)
    {
        if ($page && \View::exists('site/static/' . $page))
        {
            return \View::make('site/static/' . $page);
        } else {
            return \View::make('site/static/default');
        }
    }
}