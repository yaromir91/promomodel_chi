<?php

namespace Acme\Controllers;

use \Acme\Models\Repositories\EventsRepository;
use Acme\Models\Repositories\RoleRepository;
use Acme\Models\Repositories\UserRepository;
use \Acme\Models\UserReserves;
use \Acme\Models\Repositories\UserReservesRepository;

/**
 *
 * Class CalendarController
 *
 *
 * @property
 *
 * @author Sergey Bespalov <sergey.bespalov@chisw.com>
 */
class CalendarController extends \BaseController {

    function __construct()
    {
        parent::__construct();
    }


    /**
	 * Display a listing of the resource.
	 * GET /calendar
	 *
	 * @return Response
	 */
    public function index()
    {
        $role = $this->user->getMainUserRoleCode();

        return \View::make('site/calendar/index', compact('role'));
    }

    /**
     * Get events list for the calenfar
     * POST
     *
     * @return JSON
     */
    public function getEvents()
    {

        $user = \Auth::user();
        if(!empty($user->id)){
            $result = \App::make('CalendarServiceProvider', compact('user'));
        } else {
            $result = [];
        }

        return json_encode($result);
    }

    /**
     * @return mixed
     */
    public function getAddReserveForm()
    {
        $title = \Lang::get('calendar.add_reserve');
        $data  = [];

        return \View::make('site/calendar/addReserveForm', compact('data', 'title'));
    }

    /**
     * @return mixed
     */
    public function postAddReserve()
    {
        // Declare the rules for the form validation
        $rules = array(
            'title' => 'required|min:3',
            'datetimepickerStart'   => 'required|date',
            'datetimepickerEnd'     => 'required|date|after:datetimepickerStart',
        );

        // Validate the inputs
        $validator = \Validator::make(\Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {

            $reserve = new UserReserves();
            
            if(\Input::get('repeat') == '1') {
                $reserve_input = \Input::get('reserve');
                $repeats = \App::make('ReserveServiceProvider', compact('reserve_input'));
                foreach ($repeats as $key=>$repeat) {
                    $reserve->$key = $repeat;
                }
                $reserve->repeat_status = 1;
                $reserve->repeat_text = \Input::get('show_repeat');
                $reserve->repeat_type = \Input::get('reserve')['repeat_type'];
            } else {
                $reserve->repeat_status = 0;
            }

            $reserve->user_id = $this->user->id;

            $reserve->title = \Input::get('title');
            $reserve->description = \Input::get('description');
            $reserve->reserve_start = date('Y-m-d H:i', strtotime(\Input::get('datetimepickerStart')));
            $reserve->reserve_end = date('Y-m-d H:i', strtotime(\Input::get('datetimepickerEnd')));

            if ($reserve->save()) {
                return \Redirect::to('calendar')->with('success', \Lang::get('calendar.reserve_create_success'));
            }
            return \Redirect::to('calendar/add-reserve')->with('error', \Lang::get('calendar._reserve_create_error'));
        }
        return \Redirect::to('calendar/add-reserve')->withInput()->withErrors($validator);
    }

    public function getEditReserveForm(UserReserves $reserve)
    {
        $title = \Lang::get('calendar.edit_reserve');

        return \View::make('site/calendar/editReserveForm', compact('reserve', 'title'));

    }

    public function postEditReserve(UserReserves $reserve)
    {
        // Declare the rules for the form validation
        $rules = array(
            'title' => 'required|min:3',
            'datetimepickerStart'   => 'required|date',
            'datetimepickerEnd'     => 'required|date|after:datetimepickerStart',
        );

        // Validate the inputs
        $validator = \Validator::make(\Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {
            // Create a new
            $user = \Auth::user();

            $reserve->repeat_end = '0000-00-00 00:00:00';
            $reserve->repeat_years = '';
            $reserve->repeat_month = '';
            $reserve->repeat_weekday = '';
            $reserve->repeat_day = '';
            $reserve->repeat_text = '';
            $reserve->repeat_type = '';

            if(\Input::get('repeat') == '1') {
                $reserve_input = \Input::get('reserve');
                $repeats = \App::make('ReserveServiceProvider', compact('reserve_input'));
                foreach ($repeats as $key=>$repeat) {
                    $reserve->$key = $repeat;
                }
                $reserve->repeat_status = 1;
                $reserve->repeat_text = \Input::get('show_repeat');
                $reserve->repeat_type = \Input::get('reserve')['repeat_type'];
            } else {
                $reserve->repeat_status = 0;
            }

            $reserve->user_id = $user->id;

            $reserve->title = \Input::get('title');
            $reserve->description = \Input::get('description');
            $reserve->reserve_start = date('Y-m-d H:i', strtotime(\Input::get('datetimepickerStart')));
            $reserve->reserve_end = date('Y-m-d H:i', strtotime(\Input::get('datetimepickerEnd')));

            if ($reserve->save()) {
                return \Redirect::to('calendar')->with('success', \Lang::get('calendar.reserve_create_success'));
            }
            return \Redirect::to('calendar/add-reserve')->with('error', \Lang::get('calendar._reserve_create_error'));
        }
        return \Redirect::to('calendar/add-reserve')->withInput()->withErrors($validator);
    }

    public function getDeleteReserve(UserReserves $reserve)
    {
        $reserve->delete();

        return \Redirect::to('calendar')->with('success', \Lang::get('calendar.success_delete_reserv'));
    }
}