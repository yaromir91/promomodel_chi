/**
 * Created by sergey on 19.06.15.
 */
$(document).ready(function() {

    $(".datepicker").each(function() {
        var curDate = $(this).val();
        if (curDate && curDate != '0000-00-00 00:00:00') {
            $(this).datetimepicker({
                value       : curDate,
                format      : 'Y-m-d H:i:s',
                lang        : 'ru'
            });
        } else {
            $(this).datetimepicker({format:'Y-m-d H:i:s', lang: 'ru'});
        }
    });

    $(".datepicker-onlydate").each(function() {
        console.log('datepicker-onlydate');
        if ($(this).val()) {
            $(this).datetimepicker({
                value:$(this).val(),
                format:'Y-m-d',
                timepicker:false,
                timepickerScrollbar:false,
                scrollMonth:false,
                scrollTime:false,
                lang:'ru',
                scrollInput:false
            });
        } else {
            $(this).datetimepicker({
                format:'Y-m-d',
                timepicker:false,
                timepickerScrollbar:false,
                scrollMonth:false,
                scrollTime:false,
                lang:'ru',
                scrollInput:false
            });
        }
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    jQuery('.onlydatepicker').datetimepicker({
        format:'Y-m-d',
        lang: 'ru',
        timepicker:false,
        timepickerScrollbar:false,
        scrollMonth:false,
        scrollTime:false,
        scrollInput:false
    });


    $('.phone').mask('(000) 000-00-00');

    //Init calendar if the click on the icons
    openCalendar();

    //================= Repeat =================
    //==========================================

    //клик по чекбоксу, повторяемое. Если он выбран, отрисовываем попап с данными
    $('#RepeatCheckbox').click(function(){
        if(!$('#datetimepickerStart').val() || !$('#datetimepickerEnd').val()) {
            $('#RepeatCheckbox').attr('checked', false);
            $('.repeat-data-error').show();
        } else  if(timeHandler($('#datetimepickerStart').val()) > timeHandler($('#datetimepickerEnd').val())){
                $('#RepeatCheckbox').attr('checked', false);
                $('.repeat-time-error').show();
        } else {
            $('.repeat-data-error').hide();
            $('.repeat-time-error').hide();
            if(document.getElementById('RepeatCheckbox').checked == true) {
                var range = timeHandler($('#datetimepickerEnd').val()) - timeHandler($('#datetimepickerStart').val());
                if(range > 86400) {
                    $('#WeeklyOption').attr('disabled', true);
                } else {
                    $('#WeeklyOption').removeAttr('disabled');
                }
                $('#RepeatPopup').show();
            } else {
                document.getElementById('RepeatCheckbox').checked = false;
                $('#RepeatPopup').hide();
                $('#EditRepeatLink').hide();
                $('#EditRepeatValueInput').hide();
                reset_repeat(true);
            }
        }
    });

    // кнопка сохранения выбранного повторения в попапе
    $('#AddRepeat').click(function(){
        var error = false;
        var text = '';
        switch ($('#label-repeat').val()){
            case 'daily':
                if($('#datetimepickerDaily').val() && (timeHandler($('input[name=date_end]').val()) < timeHandler($('#datetimepickerDaily').val()))) {
                    var num = parseInt($('.daily_select').val()) - 1;
                    var text = repeat_dictionary.daily[num];
                    if ($('#datetimepickerDaily').val()) {
                        text += ' ' + repeat_dictionary.before + ': ' + $('#datetimepickerDaily').val();
                    }
                    $('#EditRepeatValueInput').val(text);
                } else {
                    alert($('input[name=repeat_after_date_title]').val());
                    return false;
                }
                break;
            case 'weekly':
                if($('#datetimepickerWeekly').val() && (timeHandler($('input[name=date_end]').val()) < timeHandler($('#datetimepickerWeekly').val()))) {
                    var days = '';
                    $('.weekly_repeat').each(function () {
                        if (document.getElementById($(this).attr('id')).checked == true) {
                            var num = parseInt($(this).val()) - 1;
                            days = days + repeat_dictionary.weekly[num] + ', ';
                        }
                    });

                    if (!days) {
                        $('#RepeatWeeklyError').show();
                        error = true;
                    } else {
                        $('#RepeatWeeklyError').hide();
                        error = false;
                    }

                    var text = repeat_dictionary.week + ' ' + days.substring(0, days.length - 2);
                    if ($('#datetimepickerWeekly').val()) {
                        text += ' ' + repeat_dictionary.before + ': ' + $('#datetimepickerWeekly').val();
                    }

                    $('#EditRepeatValueInput').val(text);
                } else {
                    alert($('input[name=repeat_after_date_title]').val());
                    return false;
                }
                break;
            case 'monthly':
                if($('#datetimepickerMonthly').val() && (timeHandler($('input[name=date_end]').val()) < timeHandler($('#datetimepickerMonthly').val()))) {
                    var num = parseInt($('.monthly_select').val()) - 1;
                    var text = repeat_dictionary.monthly[num];
                    if ($('#datetimepickerMonthly').val()) {
                        text += ' ' + repeat_dictionary.before + ': ' + $('#datetimepickerMonthly').val();
                    }
                    $('#EditRepeatValueInput').val(text);
                } else {
                    alert($('input[name=repeat_after_date_title]').val());
                    return false;
                }
                break;
            case 'yearly':
                if($('#datetimepickerYearly').val() && (timeHandler($('input[name=date_end]').val()) < timeHandler($('#datetimepickerYearly').val()))) {
                    var num = parseInt($('.yearly_select').val()) - 1;
                    var text = repeat_dictionary.yearly[num];
                    if ($('#datetimepickerYearly').val()) {
                        text += ' ' + repeat_dictionary.before + ': ' + $('#datetimepickerYearly').val();
                    }
                    $('#EditRepeatValueInput').val(text);
                } else {
                    alert($('input[name=repeat_after_date_title]').val());
                    return false;
                }
                break;
            default:
                $('#EditRepeatValueInput').val('');
                break
        }
        $('#ShowRepeat').val(text);
        if(!error) {
            $('#RepeatPopup').hide();
            $('#EditRepeatLink').show();
            $('#EditRepeatValueInput').show();
        }
    });

    $('#CloseRepeatPopup').click(function(){
        $('#RepeatPopup').hide();
        $('#EditRepeatLink').show();
    });

    // показываем блок с данными в попапе на основе выбранного в селекте типа
    $('#'+$('#label-repeat').val()).show();

    // отслеживаем изменение селекта типа повторения
    $('#label-repeat').change(function() {
        $('.repeat-data').hide(); // прячем все блоки с данными
        reset_repeat(false); // сбрасываем установленные ранее данные
        $('#'+$('#label-repeat').val()).show(); // отображаем нужный блок с данными
    });

    // клик по сылке редактирования мероприятия
    $('#EditRepeatLink').click(function(){
        $('#RepeatPopup').show(); // отображает попап
    });

    // функция сброса параметров
    function reset_repeat(label_repeat)
    {
        if(label_repeat){
            $('#label-repeat').val('daily');
            $('.repeat-data').hide();
            $('#daily').show();
        }
        $('.daily_select').val('1');
        $('.weekly_repeat').each(function(){
            $(this).attr('checked', false);
        });
        $('#RepeatWeeklyError').hide();
        $('.monthly_select').val('1');
        $('.yearly_select').val('1');
    }

    function timeHandler(data)
    {
        var dateString = data,
            dateParts = dateString.split(' '),
            timeParts = dateParts[1].split(':'),
            date;

        dateParts = dateParts[0].split('-');

        date = new Date(dateParts[0], parseInt(dateParts[1], 10) - 1, dateParts[2], timeParts[0], timeParts[1]);

        return date.getTime()/1000;
    }

    function openCalendar() {
        $('span.input-group-addon').on({
            click: function(e) {
                $(this).siblings('input').trigger('open.xdsoft');
            }
        })
    }



});