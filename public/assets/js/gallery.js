$(document).ready( function() {
    $('#album_choose').on('change', function(e) {
        if(this.value != 0) {
            $('.album_container').hide();
        } else {
            $('.album_container').show();
        }
    });

    $('#singleFieldTags').tagit({
        autocomplete: {
            delay: 0,
            minLength: 2,
            source: function( request, response ) {
                $.ajax({
                    url: "/gallery/searchtag",
                    dataType: "json",
                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            }
        },
        singleField: true,
        singleFieldNode: $('#mySingleField')
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    $("a.fancy_images").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600,
        'speedOut'		:	200,
        'overlayShow'	:	false
    });

    // Change this to the location of your server-side upload handler:
    var token = $("#fileuploader").attr('token');

    $('#CancelSaveImage').click(function(){
        var names = [];
        var backUrl = $(this).attr('data-url');
        $('.hiddenImages').each(function() {
            names.push($(this).val());
        });
        console.log('names: ', names);
        console.log('backUrl: ', backUrl);
        $.post(
            base_url+"gallery/personal/js-cancel",
            {
                images: names,
                _token: token
            },
            function (result) {
                window.location = backUrl;
            }
        );
    });

    $("#fileuploader").uploadFile({
        url: base_url+'gallery/personal/photo/add/js-add-photo',
        dragDrop: false,
        fileName: "myfile",
        returnType: "json",
        showDelete: true,
        showDownload:false,
        statusBarWidth:600,
        dragdropWidth:600,
        showPreview:true,
        previewHeight: "100px",
        previewWidth: "100px",
        uploadStr: translate_upload,
        abortStr: translate_reject,
        deletelStr: translate_delete,
        onSubmit:function(files)
        {
            //files : List of files to be uploaded
            //return flase;   to stop upload
            $('#SaveImage').attr('disabled', 'disabled');
            $('#CancelSaveImage').attr('disabled', 'disabled');
        },
        afterUploadAll:function(obj)
        {
            $( ".HiddenImgInputs").html("");
            $(obj.responses).each(function( index ) { console.log(index, this);
                $( ".HiddenImgInputs" ).append( "<input class='hiddenImages' type='hidden' name='images[]' value='" + this + "' />" );
            });
            $('#SaveImage').removeAttr('disabled');
            $('#CancelSaveImage').removeAttr('disabled');
            //You can get data of the plugin using obj
        },
        deleteCallback: function (data, pd) {
            for (var i = 0; i < data.length; i++) {
                $.post(
                    base_url+"gallery/personal/photo/add/js-remove-photo",
                    {
                        op: "delete",
                        name: data[i],
                        _token: token
                    },
                    function (resp,textStatus, jqXHR) {
                        $('input[value=' + resp + ']').remove();
                    }
                );
            }
            pd.statusbar.hide(); //You choice.

        },
        extraHTML:function()
        {
            html = "<input type='hidden' name='_token' value='" + token + "' />";
            return html;
        }
    });

    $('#DeleteImages').click(function(){
        var images = [];
        $('.removeImageCheckbox').each(function(){
            if($(this).prop("checked")){
                images.push($(this).val());
            }
        });

        console.log('images: ', images);

        $.post(
            base_url+"gallery/personal/delete_images",
            {
                images: images,
                _token: $('#_token').attr('token')
            },
            function (result) {
                window.location.reload();
            }
        );
    });

    $('.customImageList').each(function(){
        var checkbox = $(this).children('.checkboxCustom');
        $(this).hover(
            function() {
                checkbox.removeClass('hide');
                checkbox.addClass('show');
            }, function() {
                if(!checkbox.find('#removeCheckbox').prop("checked")) {
                    checkbox.removeClass('show');
                    checkbox.addClass('hide');
                }
            }
        );
    });

    // показываем-прячем кнопку удаления картинок
    $('.removeImageCheckbox').click(function(){
        console.log('removeImageCheckbox: ', $('.removeImageCheckbox:checked'));
        if($('.removeImageCheckbox:checked').length > 0) {
            $('#RemoveImages').css('display', 'inline-block');
        } else {
            $('#RemoveImages').hide();
        }
    });


    $(".g_rating").rating({
        min:0,
        max:5,
        step:1,
        size:'xs',
        showClear: false,
        showCaption: false,
        starCaptions: {
            0.5: 'Пол звезды',
            1: 'одна звезда',
            1.5: 'Полторы звезды',
            2: 'Две звезды',
            2.5: 'Две с половиной звезды',
            3: 'Три звезды',
            3.5: 'Три с половиной звезды',
            4: 'Четыре звезды',
            4.5: 'Четыре с половиной звезды',
            5: 'Пять звезд'
        },
        clearCaption: 'Нет оценки'
    });

    $('.g_rating').on('rating.change', function(event, value, caption) {
        //console.log('this: ', $(this));
        //console.log('value: ', value);
        //console.log('caption: ', caption);

        var obj =  $(this);

        $.ajax({
            url: base_url+"gallery/personal/photo/rate",
            dataType: "json",
            type: "POST",
            data: {
                id: obj.attr('data-id'),
                rate: value,
                _token: $('#_token').attr('token')
            },
            success: function(data) {
                obj.rating('update', data.avg);
            }
        });


    });
});