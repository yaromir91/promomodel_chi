$(document).ready(function() {
    $('.prof_checked').each(function(){
        $(this).click(function(){
            $(this).parent().next().toggle();
        });
    });
    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    $(".slider").each(function(){

        var max = parseInt($(this).attr('data-max'));
        var min = parseInt($(this).attr('data-min'));
        var id = $(this).attr('id');

        $('#'+id).slider({
            min: min,
            max: max,
            range: true,
            step: 1,
            values: [min, max]
        })
            .slider("pips", {
                rest: "label"
            })
            .slider("float")
            .on("slidechange", function(e,ui) {
                $('#hidden1_'+id).val(ui.values[1]);
                $('#hidden0_'+id).val(ui.values[0]);
            });
    });
});