$(document).ready( function() {

    $('#singleFieldTags').tagit({
        autocomplete: {
            delay: 0,
            minLength: 2,
            source: function( request, response ) {
                $.ajax({
                    url: "/admin/gallery/searchtag",
                    dataType: "json",
                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            }
        },
        singleField: true,
        singleFieldNode: $('#mySingleField')
    });

    var token        = $("#fileuploader").attr('token');
    var galleryObj   = $('.gallery-id-hdn');
    var addUrl       = '/admin/gallery/js-add-photo/';
    var deleteImgUrl = '/admin/gallery/js-remove-photo';

    if (galleryObj.length) {
        addUrl      = '/admin/gallery/js-add-photo/'+galleryObj.val();
        deleteImgUrl = '/admin/gallery/js-remove-photo/'+galleryObj.val();
    }


    $("#fileuploader").uploadFile({
        url: addUrl,
        dragDrop: false,
        fileName: "myfile",
        returnType: "json",
        showDelete: true,
        showDownload:false,
        statusBarWidth:600,
        dragdropWidth:600,
        showPreview:true,
        previewHeight: "100px",
        previewWidth: "100px",
        uploadStr: 'Добавить',
        abortStr: 'Отменить',
        deletelStr: 'Удалить',
        deleteCallback: function (data, pd) {
            for (var i = 0; i < data.length; i++) {
                $.post(
                    deleteImgUrl,
                    {
                        op:     "delete",
                        name:   data[i],
                        _token: token
                    },
                    function (resp,textStatus, jqXHR) {
                        alert("File Deleted");
                    }
                );
            }
            pd.statusbar.hide();
        },
        extraHTML:function()
        {
            html = "<input type='hidden' name='_token' value='" + token + "' />";
            return html;
        }
    });

});