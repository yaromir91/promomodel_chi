$(document).ready(function() {

    $( ".tab-pane" ).on( "change", ".params-types", function() {

        var $selectedTypes = $(".params-type-to-select>option").map(function() { return $(this).val(); });
        var isInArray      = jQuery.inArray($(this).val(), $selectedTypes);

        if (isInArray >= 0) {
            $('.childrens-el').removeClass('hidden');
        } else {
            $('.childrens-el').removeClass('hidden');
            $('.childrens-el').addClass('hidden');
        }

    });


});