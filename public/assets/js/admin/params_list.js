$(document).ready(function() {

    $(".table-params-data").sortable({
        connectWith: "tr",
        update: function (event, ui) {
            var ordData = [];
            $(".table-params-data").children().each(function(i) {
                var tr = $(this);
                ordData.push(tr.attr("id"));
            });

            var activePage    = 1;
            var perPage       = 10;
            var activePageObj = $('.dataTables_paginate .active a');
            var perPageObj    = $('.dataTables_length select option:selected');

            if (activePageObj.length > 0) {
                activePage = activePageObj.text();
            }

            if (perPageObj.length > 0) {
                perPage = perPageObj.text();
            }

            $.ajax({
                data: {'ordData':ordData, 'activePage': activePage, 'perPage': perPage},
                type: 'GET',
                url:  '/admin/params/setorder'
            });

        }
    });

    $(".table-params-data").disableSelection();
});