(function() {
    'use strict';
    var windowWidth = window.innerWidth,
        mobileNavigation = $('.mobile-navigation'),
        mobileNavigationBtn = $('.mobile-navigationBtn');
        mobileNavigation.hide();
    if(windowWidth < 768) {
        mobileNavigationBtn.show();
    }
    else {
        mobileNavigationBtn.hide();
    }
    mobileNavigationBtn.on('click', function(event) {
        event.preventDefault();
        $(this).siblings('.mobile-navigation').slideToggle();
    });
    function scrollMobileMenu() {
        if(window.innerWidth < 768) {
            $(window).scroll(function() {
                if ($(window).scrollTop() >= 65 ) {
                    mobileNavigationBtn.show(400);
                } else {
                    mobileNavigationBtn.hide();
                    mobileNavigation.hide();
                }
            });
        }
        else {
            mobileNavigationBtn.hide();
            mobileNavigation.hide();
        }
    }
    scrollMobileMenu();
    $(window).on('resize', function() {
        scrollMobileMenu();
    });
    $('.language-section').on('click', '.active', function(e) {
        e.preventDefault();
        $(this).parent('.inner_language').next().find('a').toggleClass('show-language');
    })
})();


