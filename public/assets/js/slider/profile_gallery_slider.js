$(document).ready(function ($) {

    if ($('#slider1_container').length >0)
    {
        var _SlideshowTransitions = [
            {$Duration: 1200, x: 0.3, $During: { $Left: [0.3, 0.7] }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, x: -0.3, $SlideOut: true, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, x: -0.3, $During: { $Left: [0.3, 0.7] }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, x: 0.3, $SlideOut: true, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }

            , { $Duration: 1200, y: 0.3, $During: { $Top: [0.3, 0.7] }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2, $Outside: true }
            , { $Duration: 1200, y: -0.3, $SlideOut: true, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2, $Outside: true }
            , { $Duration: 1200, y: -0.3, $During: { $Top: [0.3, 0.7] }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, y: 0.3, $SlideOut: true, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }

            , { $Duration: 1200, x: 0.3, $Cols: 2, $During: { $Left: [0.3, 0.7] }, $ChessMode: { $Column: 3 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2, $Outside: true }
            , { $Duration: 1200, x: 0.3, $Cols: 2, $SlideOut: true, $ChessMode: { $Column: 3 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2, $Outside: true }
            , { $Duration: 1200, y: 0.3, $Rows: 2, $During: { $Top: [0.3, 0.7] }, $ChessMode: { $Row: 12 }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, y: 0.3, $Rows: 2, $SlideOut: true, $ChessMode: { $Row: 12 }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }

            , { $Duration: 1200, y: 0.3, $Cols: 2, $During: { $Top: [0.3, 0.7] }, $ChessMode: { $Column: 12 }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2, $Outside: true }
            , { $Duration: 1200, y: -0.3, $Cols: 2, $SlideOut: true, $ChessMode: { $Column: 12 }, $Easing: { $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, x: 0.3, $Rows: 2, $During: { $Left: [0.3, 0.7] }, $ChessMode: { $Row: 3 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2, $Outside: true }
            , { $Duration: 1200, x: -0.3, $Rows: 2, $SlideOut: true, $ChessMode: { $Row: 3 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }

            , { $Duration: 1200, x: 0.3, y: 0.3, $Cols: 2, $Rows: 2, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $ChessMode: { $Column: 3, $Row: 12 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2, $Outside: true }
            , { $Duration: 1200, x: 0.3, y: 0.3, $Cols: 2, $Rows: 2, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $SlideOut: true, $ChessMode: { $Column: 3, $Row: 12 }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2, $Outside: true }

            , { $Duration: 1200, $Delay: 20, $Clip: 3, $Assembly: 260, $Easing: { $Clip: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, $Delay: 20, $Clip: 3, $SlideOut: true, $Assembly: 260, $Easing: { $Clip: $JssorEasing$.$EaseOutCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, $Delay: 20, $Clip: 12, $Assembly: 260, $Easing: { $Clip: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
            , { $Duration: 1200, $Delay: 20, $Clip: 12, $SlideOut: true, $Assembly: 260, $Easing: { $Clip: $JssorEasing$.$EaseOutCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
        ];

        var options = {
            $AutoPlay: false,
            $AutoPlayInterval: 1500,
            $PauseOnHover: 1,

            $DragOrientation: 3,
            $ArrowKeyNavigation: true,
            $SlideDuration: 800,

            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: _SlideshowTransitions,
                $TransitionsOrder: 1,
                $ShowLink: true
            },

            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $ChanceToShow: 1
            },

            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $ChanceToShow: 2,

                $ActionMode: 1,
                $SpacingX: 8,
                $DisplayPieces: 10,
                $ParkingPosition: 360
            }
        };

        var jssor_slider1 = new $JssorSlider$("slider1_container", options);

        function ScaleSliderOld() {
            var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
            if (parentWidth)
                jssor_slider1.$ScaleWidth(Math.max(Math.min(parentWidth, 1000), 1000));
            else
                window.setTimeout(ScaleSlider, 30);
        }

        /*function ScaleSlider() {

            var paddingWidth    = 20;
            var minReserveWidth = 150;
            var parentElement   = jssor_slider1.$Elmt.parentNode;
            var parentWidth     = parentElement.clientWidth;

            if (parentWidth) {
                var availableWidth = parentWidth - paddingWidth;
                var sliderWidth = availableWidth * 0.9;

                sliderWidth = Math.min(sliderWidth, 1000);
                sliderWidth = Math.max(sliderWidth, 200);
                var clearFix = "none";

                if (availableWidth - sliderWidth < minReserveWidth) {
                    sliderWidth = availableWidth;
                    sliderWidth = Math.max(sliderWidth, 200);
                    clearFix = "both";
                }
                var toClearElment = $Jssor$.$GetElement("clearFixDiv");
                toClearElment && $Jssor$.$Css(toClearElment, "clear", clearFix);

                jssor_slider1.$ScaleWidth(sliderWidth);
            }
            else
                $Jssor$.$Delay(ScaleSlider, 30);
        }*/
        function ScaleSlider() {
            var paddingWidth = 20;
            var minReserveWidth = 150;
            var parentWidth = $("#slider1_container").parent().width();
            if (parentWidth) {
                if(window.innerWidth <480 && window.innerWidth > 400) {
                    jssor_slider1.$ScaleWidth(Math.min(parentWidth, 400));
                }
                else if(window.innerWidth < 400) {
                    jssor_slider1.$ScaleWidth(Math.min(parentWidth, 300));
                }
                else if(window.innerWidth > 480 && window.innerWidth <767) {
                    jssor_slider1.$ScaleWidth(Math.min(parentWidth, 640));
                }
                else if(window.innerWidth > 767) {
                    var availableWidth = parentWidth - paddingWidth;
                    var sliderWidth = availableWidth * 0.9;
                    sliderWidth = Math.min(sliderWidth, 1000);
                    sliderWidth = Math.max(sliderWidth, 200);
                    var clearFix = "none";

                    if (availableWidth - sliderWidth < minReserveWidth) {
                        sliderWidth = availableWidth;
                        sliderWidth = Math.max(sliderWidth, 200);
                        clearFix = "both";
                    }
                    var toClearElment = $Jssor$.$GetElement("clearFixDiv");
                    toClearElment && $Jssor$.$Css(toClearElment, "clear", clearFix);

                    jssor_slider1.$ScaleWidth(sliderWidth*0.9);
                }
            }
        }

        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", $Jssor$.$WindowResizeFilter(window, ScaleSlider));
        $(window).bind("orientationchange", ScaleSlider);
    }


});
