(function ($) {
    console.log(1);
    var Faq = {
        $accord: $('#faq-accordion'),
        init: function () {
            var firstQ = this.$accord.find('li li a').first();
            this.selectQuestion(firstQ);
            this.bindEvents();
        },
        bindEvents: function () {
            var self = this;
            this.$accord.find('li li a').on('click', function () {
                self.selectQuestion($(this));
                return false;
            });
        },
        selectQuestion: function (firstQ) {
            $(firstQ.attr('href')).addClass('show').siblings().removeClass('show');

            this.$accord.find('.selected').removeClass('selected');
            firstQ.parent().addClass('selected');
        }
    };

    $(function () {
        Faq.init();
    });

})(jQuery);