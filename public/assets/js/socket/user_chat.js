$(function(){

    var inProgress = false;
    var currPage   = 1;
    var theToken   = $('#_token').attr('token');

    var user         = $('.current-user');
    var chat         = $('.current-chat');
    var recUser      = $('.receiver-user');
    var recUserAva   = $('.receiver-user-avatar');
    var curUserAva   = $('.current-user-avatar');
    var recUserName  = $('.receiver-user-name');
    var totalCount   = $('.count2').val();
    var messageText  = $('#chat-message');
    var delete_title = $('input[name=delete_title]').val();

    var user_id       = false;
    var user_ava      = false;
    var res_user_id   = false;
    var res_user_ava  = false;
    var chat_code     = false;
    var res_user_name = false;
    var me_msg        = 'Me';
    var delete_msg    = 'Delete';

    if (user.length && chat.length && recUserAva.length && curUserAva.length) {
        user_id       = user.val();
        me_msg        = user.attr('data-msg-me');
        delete_msg    = user.attr('data-msg-delete');
        chat_code     = chat.val();
        res_user_id   = recUser.val();
        user_ava      = curUserAva.val();
        res_user_ava  = recUserAva.val();
        res_user_name = recUserName.val();
    }

    var listenUrl     = 'chat-' + chat_code;

    $('.messenger').on( "click", ".show-else-messages", function() {
        var getDataURL = "/chat/"+chat_code+"/get";
        var theShowBtn = $(this);

        $.ajax({
            url: getDataURL,
            method: 'POST',
            data: {"currPage" : currPage, _token: theToken },
            dataType: "json",
            beforeSend: function() {
                inProgress = true;}
        }).done(function(data){
            if (data.html.length > 0) {
                $(".chat-messages").append(data.html);
                inProgress = false;
                currPage += 1;
            } else {
                theShowBtn.hide();
            }
        });
    });


    $('.messenger').on( "click", ".del-user-msg a", function() {

        var deleteURL = $(this).attr('href');
        var r         = confirm($(this).data('msg'));
        var thisA     = $(this);

        if (r == true) {
            $.ajax({
                method: "POST",
                url: deleteURL,
                dataType: "json",
                data: {_token: theToken }
            })
            .done(function( data ) {
                    thisA.closest("li").remove();
            });
        }

        return false;
    });

    var socket = io(':3030');

    socket.on('connect', function (user) {
        socket.on('getMessage_'+user_id, function(msg){
            var  theInfo = '<li><div data-received-user="'+user_id+'" data-msg-id="'+msg.msg_is+'" class="alert not-read-message" data-received-user="'+res_user_id+'">';
            if (user_ava) {
                theInfo += '<img style="width:50px;" src="'+res_user_ava+'" /><a href="/profile/view/'+res_user_id+'" target="_blank"> '+res_user_name+'</a> : '
            }
            theInfo += msg.message;
            theInfo += '<div class="msg-time">'+msg.the_time+'</div></div></li>';

            $('#chat-log').prepend(theInfo);
        });
    });


    $('.messenger').on( "mouseover", ".not-read-message", function() {
        var $userRecId = $(this).attr("data-received-user");
        var $msgId     = $(this).attr("data-msg-id");
        var $msgBlock  = $(this);

        if (user_id == $userRecId) {
            var postURL  = "/chat/"+$msgId+"/set-read-msg";

            $.ajax({
                method: "POST",
                url: postURL,
                dataType: "json",
                data: { _token: theToken }
            })
                .done(function( data ) {
                    if (data.changed) {
                        $msgBlock.removeClass("not-read-message");
                        $msgBlock.addClass("alert-info");
                    }
                });
        }
    });

    function sendMessage(msg)
    {
        var postURL  = "/chat/"+chat_code+"/send-message";

        if (msg) {
            $.ajax({
                method: "POST",
                url: postURL,
                dataType: "json",
                data: { userrId: user_id, message: msg, _token: theToken }
            })
                .done(function( data ) {
                    if (data.send_status && (data.recUserId == res_user_id)) {
                        socket.emit('sendMessage', {
                            'message'       :msg,
                            'the_time'      :data.theTime,
                            'msg_is'        :data.msg_is,
                            'user_id'       :user_id,
                            'res_user_id'   :res_user_id
                        });
                        var theInfo  = '<li><div class="alert alert-success">';
                        theInfo += '<div class="del-user-msg"><a href="/chat/'+data.msg_is+'/delete" data-msg="'+ delete_title +'">' + delete_msg + '</a></div>'
                        if (user_ava) {
                            theInfo += '<img style="width:50px;" src="'+user_ava+'" /> ' + me_msg + '  : '
                        }
                        theInfo += msg;
                        theInfo += '<div class="msg-time">'+data.theTime+'</div></div></li>';

                        $('#chat-log').prepend(theInfo);
                    }
                });
        }

    }


    $('.messenger').on( "click", ".chat-send-message", function() {
        if (messageText.length) {
            var theMsg = messageText.val();

            if(theMsg){
                sendMessage(theMsg);
                messageText.val('');
            }
        }
    });

    $('#chat-message').keypress(function(event) {

            var theMsg = $(this).val();

            if(event.keyCode == 13 && theMsg){
                sendMessage(theMsg);
                $(this).val('');
            }

            return event.keyCode != 13;
        }
    );
});