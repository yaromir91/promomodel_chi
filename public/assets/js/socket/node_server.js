var https = require('https'),
    fs =    require('fs');

var options = {
    key:    fs.readFileSync('/etc/ssl/certs/www_promogang_ru.key'),
    cert:   fs.readFileSync('/etc/ssl/certs/www_promogang_ru.crt'),
    ca:     fs.readFileSync('/etc/ssl/certs/www_promogang_ru.ca-bundle')
};
var app = https.Server(options);
//var app = https.Server();
var io = require('/usr/local/lib/node_modules/npm/node_modules/socket.io').listen(app);     //socket.io server listens to https connections
app.listen(3030, function(){
    console.log('listening on *:3030');
});




//var https = require('http').Server();
//var io = require('socket.io')(https);

io.on('connection', function(socket){
    socket.on('sendMessage', function(data){
        io.sockets.emit('getMessage_'+data.res_user_id, data);
        //console.log('data: ', data);
    });
    console.log('a user connected');
});

//https.listen(3000, function(){
//    console.log('listening on *:3000');
//});