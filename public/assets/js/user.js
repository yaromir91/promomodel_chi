$(document).ready(function() {

    resetErrors();

    /**
     * Reset error in the forget
     */
    function resetErrors() {
        if(($('.container').find('.alert').length)){
            $('.container input').one({
                focus : function (e) {
                    e.preventDefault();
                    $('.alert').slideUp(500)
                }
            });
        }
    }
});