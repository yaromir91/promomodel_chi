$(document).ready(function() {

    viewImage();

    $('#confirm-delete-report').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    $('#confirm-delete-comment').on('show.bs.modal', function(e) {
        $('#btn_delete_comment').click(function(){
            console.log('data relatedTarget: ', $(e.relatedTarget).attr('data-href'));
            var url = $(e.relatedTarget).attr('data-href');
            var id = $(e.relatedTarget).attr('data-id');
            var success_block = '<div class="alert alert-success alert-block"><button data-dismiss="alert" class="close" type="button">×</button><h4>Коментарий успешно удален.</h4></div>';
            //var error_block = '<div class="alert alert-danger alert-block"><button data-dismiss="alert" class="close" type="button">×</button><h4>Error</h4></div>';

            $.ajax({
                url: url,
                dataType: "json",
                type: "GET",
                success: function(data) {
                    console.log('ajax data: ', data);
                    //location.reload(true);
                    $('#confirm-delete-comment').modal('hide');
                    $('#comment_' + id).remove();
                    $('.page-header').before(success_block);
                    window.scrollBy(0,-1000);
                }
            });
        });
    });

    $('.btn_update_comment').each(function(){
        $(this).click(function(e){
            console.log('data this: ', $(this).attr('data-href'));
            var url = $(this).attr('data-href');
            var id = $(this).attr('data-id');
            var success_block = '<div class="alert alert-success alert-block"><button data-dismiss="alert" class="close" type="button">×</button><h4>Коментарий успешно обновлен.</h4></div>';
            var content = $('#comment_content_' + id).val();

            $.ajax({
                url: url,
                dataType: "json",
                type: "POST",
                data: {
                    content: content,
                    _token: $('#_token').attr('token')
                },
                success: function(data) {
                    console.log('ajax data: ', data);
                    $('#comment_content_block_'+id).html(content);
                    $('#comment-edit-form_'+id).modal('hide');
                    $('.page-header').before(success_block);
                    window.scrollBy(0,-1000);
                }
            });
        });
    });

    // Change this to the location of your server-side upload handler:
    var token = $("#fileuploader").attr('token');

    $(".g_rating").rating({
        min:0,
        max:5,
        step:1,
        size:'xs',
        showClear: false,
        showCaption: false,
        starCaptions: {
            0.5: 'Пол звезды',
            1: 'одна звезда',
            1.5: 'Полторы звезды',
            2: 'Две звезды',
            2.5: 'Две с половиной звезды',
            3: 'Три звезды',
            3.5: 'Три с половиной звезды',
            4: 'Четыре звезды',
            4.5: 'Четыре с половиной звезды',
            5: 'Пять звезд'
        },
        clearCaption: 'Нет оценки'
    });

    $('.g_rating').on('rating.change', function(event, value, caption) {

        var obj =  $(this);

        $.ajax({
            url: base_url+"report/rate",
            dataType: "json",
            type: "POST",
            data: {
                id: obj.attr('data-id'),
                rate: value,
                _token: $('#_token').attr('token')
            },
            success: function(data) {
                obj.rating('update', data.avg);
            }
        });

    });

    function viewImage() {
        function imageLoad(files) {


            var reader = new FileReader();
            var image = new Image();


            reader.readAsDataURL(files);
            reader.onload = function (_file) {
                image.src = _file.target.result // url.createObjectURL(file);
                image.onload = function () {
                    var w = this.width,
                        h = this.height,
                        t = files.type, // ext only: // file.type.split('/')[1],
                        n = files.name,
                        s = ~~(files.size / 1024) + 'KB',
                        $img = $('input[type="file"]');

                    $img.siblings('img.img-thumbnail').remove();
                    $img.parent().append('<img class="img-thumbnail" src="' + this.src + '" style="margin-top:10px; width: 300px; height: 200px">');

                };
                image.onerror = function () {
                    alert('Не верный тип файла: ' + _file.type);
                };
            };
        }


        $('[type="file"]').on({
            change: function () {
                imageLoad(this.files[0]);
            }
        });
    }
});