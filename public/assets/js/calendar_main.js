$(document).ready(function() {
    //$('#calendar').fullCalendar({
    //    header: {
    //        left: 'prev,next today',
    //        center: 'title',
    //        right: 'month,basicWeek,basicDay'
    //    },
    //    defaultDate: '2015-06-12',
    //    editable: true,
    //    eventLimit: true // allow "more" link when too many events
    //});

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay',
        },
        lang: current_lang,
        eventLimit: true, // allow "more" link when too many events
        events: function(start, end, timezone, callback)
        {
            getEvents(start, end, timezone, callback);
        },
        timeFormat: 'hh:mm A'
    });


    //Data sample
    //
    //    start: '2015-02-09T16:00:00'
    //

    function getEvents(start, end, timezone, callback)
    {
        var events = [];
        $.ajax(
            {
                url: 'calendar/getEvents',
                dataType: 'json',
                data: {
                    _token: $('#Token').val()
                },
                cache: false,
                type: 'POST',
                success: function (result)
                {
                    for(var index in result) {
                        var attr = result[index];

                        var startDateRangeString = "";
                        var endDateRangeString = "";

                        var reserve_start = new Date((attr.start + 3600) * 1000);

                        var reserve_end = new Date((attr.end + 3600) * 1000);
                        var reserve_range =  new Date((attr.end - attr.start) * 1000);


                        if (attr.repeat) {
                            startDateRangeString = attr.start;
                            endDateRangeString = attr.repeat_end;
                        }

                        if (endDateRangeString) {
                            var end_date = new Date(attr.repeat_end * 1000);
                        } else {
                            var end_date = new Date(end);
                        }


                        if(attr.repeat_day) {
                            while (reserve_start < end_date) {

                                events.push({
                                    title:      attr.title,
                                    url:        attr.url,
                                    start:      new Date(reserve_start),
                                    end:        new Date(reserve_end),
                                    textColor:  attr.textColor,
                                    color:      attr.color
                                });

                                reserve_start.setDate(reserve_start.getDate() + reserve_range.getDate() + parseInt(attr.repeat_day) - 1);
                                reserve_end.setDate(reserve_end.getDate() + reserve_range.getDate()  + parseInt(attr.repeat_day) - 1);
                            }
                        } else if(attr.repeat_weekday) {
                            if((attr.end - attr.start) > 86400) {

                            } else {
                                var str = attr.repeat_weekday;
                                var res = str.split(",");
                                while (reserve_start < end_date) {
                                    $(res).each(function(){
                                        if (reserve_start.getDay() == this) {
                                            events.push({
                                                title:      attr.title,
                                                url:        attr.url,
                                                start:      new Date(reserve_start),
                                                end:        new Date(reserve_end),
                                                textColor:  attr.textColor,
                                                color:      attr.color
                                            });
                                        }
                                    });
                                    reserve_start.setDate(reserve_start.getDate() + 1);
                                    reserve_end.setDate(reserve_end.getDate() + 1);
                                }
                            }
                        } else if(attr.repeat_month) {
                            while (reserve_start < end_date) {
                                events.push({
                                    title:      attr.title,
                                    url:        attr.url,
                                    start:      new Date(reserve_start),
                                    end:        new Date(reserve_end),
                                    textColor:  attr.textColor,
                                    color:      attr.color
                                });
                                reserve_start.setMonth(reserve_start.getMonth() + parseInt(attr.repeat_month));
                                reserve_end.setMonth(reserve_end.getMonth() + parseInt(attr.repeat_month));
                            }
                        } else if(attr.repeat_years) {
                            while (reserve_start < end_date) {
                                events.push({
                                    title:      attr.title,
                                    url:        attr.url,
                                    start:      new Date(reserve_start),
                                    end:        new Date(reserve_end),
                                    textColor:  attr.textColor,
                                    color:      attr.color
                                });
                                console.log('y: ', reserve_start.getFullYear());
                                reserve_start.setYear(reserve_start.getFullYear() + parseInt(attr.repeat_years));
                                reserve_end.setYear(reserve_end.getFullYear() + parseInt(attr.repeat_years));
                            }
                        } else {
                            events.push({
                                title:      attr.title,
                                url:        attr.url,
                                start:      new Date(reserve_start),
                                end:        new Date(reserve_end),
                                textColor:  attr.textColor,
                                color:      attr.color
                            });
                        }
                    }
                    return callback(events);
                }

            });
    }
});