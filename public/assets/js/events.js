/**
 * Created by sergey on 19.06.15.
 */
$(document).ready(function() {

    $('.prof_checked').each(function(){
        $(this).click(function(){
            $(this).parent().next().toggle();
        });
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    $('#confirm-cancel').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    $('#confirm-move-team-member-main').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    $('.numbersOnly').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });

    $(".slider").each(function(){

        var max = parseInt($(this).attr('data-max'));
        var min = parseInt($(this).attr('data-min'));
        var step = parseInt($(this).attr('data-step'));
        var pip_step = parseInt($(this).attr('data-pip_step'));
        var id  = $(this).attr('id');

        $('#'+id).slider({
            min: min,
            max: max,
            range: true,
            step: step,
            values: [min, max]
        })
        .slider("pips", {
            rest: "label",
            step: pip_step
        })
        .slider("float")
        .on("slidechange", function(e,ui) {
            $('#hidden1_'+id).val(ui.values[1]);
            $('#hidden0_'+id).val(ui.values[0]);
        });
    });

    $( ".send-invite" ).submit(function( event ) {

        event.preventDefault();

        var theForm     = $(this);
        var theSbmBtn   = theForm.find('.btn-submit');
        var inviteBlock = theForm.find('.invite-the-user');

        $.ajax({
            type: theForm.attr('method'),
            url:  theForm.attr('action'),
            data: theForm.serialize(),
            dataType: 'json',
            success: function(data)
            {
                $modal      = $('#ajaxResponse');

                if ($modal.length) {
                    $modalTitle = $modal.find('.modal-title');

                    if ($modalTitle.length) {
                        $modalTitle.text('');
                        $modalTitle.text(data.title);
                    }

                    $modalBody = $modal.find('.modal-body p');

                    if ($modalBody.length) {
                        $modalBody.text('');
                        $modalBody.text(data.message);
                    }

                    $("#ajaxResponse").modal('show');
                }

                if (inviteBlock.length && data.status==1) {
                    var prof_msg = '<p class="bg-warning"><span>Статус</span>: '+data.info+'</p>';
                    var manage_msg = '';

                    if(data.manager){
                        manage_msg = '<p class="bg-warning"><span>Статус менеджера</span>: '+data.info+'</p>';
                    }

                    inviteBlock.html(manage_msg + prof_msg);
                    theSbmBtn.hide();
                }
            }
        });

        return false;
    });

    $( ".tab-user-invitation, .event-team-users-block" ).on( "click", ".btn-send-invitation-answer", function() {

        var dataTarget  = $(this).attr('data-target');
        var modalW      = $(dataTarget);
        var theUrl      = $(this).attr('data-href');
        var eventTarget = $(this).attr('data-event-target');


        if (modalW.length) {
            var sendAcceptBtn = modalW.find('.send-accept');
            var sendAcceptReloadBtn = modalW.find('.send-accept-reload');

            if (sendAcceptBtn.length) {
                sendAcceptBtn.attr('href', theUrl);
                sendAcceptBtn.attr('data-event-target', eventTarget);

                if ($(this).hasClass('doNotDelete')) {
                    sendAcceptBtn.addClass('notDel');
                } else {
                    sendAcceptBtn.removeClass('notDel');
                }

                if ($(this).hasClass('send-just')) {
                    modalW.find('.send-accept').click();
                } else {
                    modalW.modal('show');
                }
            } else if(sendAcceptReloadBtn.length) {
                sendAcceptReloadBtn.attr('href', theUrl);
                sendAcceptReloadBtn.attr('data-event-target', eventTarget);

                if ($(this).hasClass('doNotDelete')) {
                    sendAcceptReloadBtn.addClass('notDel');
                } else {
                    sendAcceptReloadBtn.removeClass('notDel');
                }

                if ($(this).hasClass('send-just')) {
                    modalW.find('.send-accept-reload').click();
                } else {
                    modalW.modal('show');
                }
            }

        }
    });

    $( ".tab-user-invitation, .event-team-users-block" ).on( "click", ".send-accept", function() {
        var url            = $(this).attr('href');
        var answerSelector = $(this).attr('data-target-answer');
        var eventSelector  = $(this).attr('data-event-target');
        var parentModel    = $(this).closest(".modal");
        var answer         = $(answerSelector);
        var eventTarget    = $(eventSelector);
        var isDelete       = (!$(this).hasClass('notDel'));

        //setTimeout(function(){
        //    $.get(url, function(data, status){
        //        if (answer.length) {
        //            answer.find('.modal-body').html(data.message);
        //            answer.modal('show');
        //        }
        //
        //        if(data.manager) {
        //            $('.manager_buttons').remove();
        //        }
        //
        //        if (eventTarget.length) {
        //            var deletedElem    = eventTarget.find('.delAfter');
        //
        //            if (deletedElem.length) {
        //                deletedElem.each(function(){
        //                    deletedElem.remove();
        //                });
        //            }
        //
        //            if (isDelete) {
        //                eventTarget.remove();
        //            }
        //        }
        //
        //        if (parentModel.length) {
        //            parentModel.modal('hide');
        //        }
        //    }, "json");
        //}, 2000);

        //
        $.ajax({
            url: url,
            method: "GET",
            dataType: "json",
            success: function(data) {
                console.log('success data: ', data);
                if (answer.length) {
                    answer.find('.modal-body').html(data.message);
                    answer.modal('show');
                }

                if(data.manager) {
                    $('.manager_buttons').remove();
                }

                if (eventTarget.length) {
                    var deletedElem    = eventTarget.find('.delAfter');

                    if (deletedElem.length) {
                        deletedElem.each(function(){
                            deletedElem.remove();
                        });
                    }

                    if (isDelete) {
                        eventTarget.remove();
                    }
                }

                if (parentModel.length) {
                    parentModel.modal('hide');
                }
            },
            error: function(data) {
                console.log('error data: ', data);
            },
            complete: function(data, status) {
                console.log('complete data: ', data);
                console.log('complete status: ', status);
            }
        });

        return false;
    });

    $( ".tab-user-invitation, .event-team-users-block" ).on( "click", ".send-accept-reload", function() {
        var url            = $(this).attr('href');
        var eventTeam      = $(this).attr('data-event-target');

        $.ajax({
            url: url,
            method: "GET",
            dataType: "json",
            success: function(data) {
                window.location.href = eventTeam;
            },
            error: function(data) {
                console.log('error data: ', data);
            },
            complete: function(data, status) {
                console.log('complete data: ', data);
                console.log('complete status: ', status);
            }
        });

        return false;
    });

    $(".g_rating, .g_rating_team").rating({
        min:0,
        max:5,
        step:1,
        size:'xs',
        showClear: false,
        showCaption: false,
        starCaptions: {
            0.5: 'Пол звезды',
            1: 'одна звезда',
            1.5: 'Полторы звезды',
            2: 'Две звезды',
            2.5: 'Две с половиной звезды',
            3: 'Три звезды',
            3.5: 'Три с половиной звезды',
            4: 'Четыре звезды',
            4.5: 'Четыре с половиной звезды',
            5: 'Пять звезд'
        },
        clearCaption: 'Нет оценки'
    });

    $('.g_rating').on('rating.change', function(event, value, caption) {

        var obj =  $(this);

        $.ajax({
            url: base_url+"events/rate",
            dataType: "json",
            type: "POST",
            data: {
                id          : obj.attr('data-id'),
                rate        : value,
                type_rate   : obj.attr('data-rating-type'),
                event_id    : obj.attr('data-event-id'),
                _token      : $('#_token').attr('token')
            },
            success: function(data) {
                obj.rating('update', data.avg);
                $('.g_rating_team').rating('update', data.avg_team);
            }
        });
    });

    $('.g_rating_team').on('rating.change', function(event, value, caption) {

        var obj =  $(this);

        $.ajax({
            url: base_url+"events/rate_team",
            dataType: "json",
            type: "POST",
            data: {
                id: obj.attr('data-id'),
                rate: value,
                type_rate: obj.attr('data-rating-type'),
                _token: $('#_token').attr('token')
            },
            success: function(data) {
                $('.g_rating').each(function(){
                    $(this).rating('update', data.avg);
                });

            }
        });
    });

});