$(document).ready( function() {

    $('.prof_check').click(function () {

        var id = $(this).attr('id');

        if($(this).is(':checked')) {
            $('#prh_' + id).removeAttr('disabled');
            $('#prd_' + id).removeAttr('disabled');
        } else {
            $('#prh_' + id).val('');
            $('#prd_' + id).val('');
            $('#prh_' + id).attr('disabled', 'disabled');
            $('#prd_' + id).attr('disabled', 'disabled');
        }
    });

    $(".g_rating").rating({
        min:0,
        max:5,
        step:1,
        size:'xs',
        showCaption: false,
        showClear: false,
        starCaptions: {
            0.5: 'Пол звезды',
            1: 'одна звезда',
            1.5: 'Полторы звезды',
            2: 'Две звезды',
            2.5: 'Две с половиной звезды',
            3: 'Три звезды',
            3.5: 'Три с половиной звезды',
            4: 'Четыре звезды',
            4.5: 'Четыре с половиной звезды',
            5: 'Пять звезд'
        },
        clearCaption: 'Нет оценки'
    });

    $('.g_rating').on('rating.change', function(event, value, caption) {

        var obj =  $(this);

        $.ajax({
            url: base_url+"profile/rate",
            dataType: "json",
            type: "POST",
            data: {
                id: obj.attr('data-id'),
                rate: value,
                _token: $('#_token').attr('token')
            },
            success: function(data) {
                obj.rating('update', data.avg);
            }
        });
    });

});