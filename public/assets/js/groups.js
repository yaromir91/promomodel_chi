$(document).ready(function() {


    viewImage();
    ajaxLoadImage();
    submit();
    unsubscribeUser();
    sendInvite();


    $('#confirm-delete-comment').on('show.bs.modal', function(e) {
        $('#btn_delete_comment').click(function(){
            console.log('data relatedTarget: ', $(e.relatedTarget).attr('data-href'));
            var url = $(e.relatedTarget).attr('data-href');
            var id = $(e.relatedTarget).attr('data-id');
            var success_block = '<div class="alert alert-success alert-block"><button data-dismiss="alert" class="close" type="button">×</button><h4>Коментарий успешно удален.</h4></div>';
            //var error_block = '<div class="alert alert-danger alert-block"><button data-dismiss="alert" class="close" type="button">×</button><h4>Error</h4></div>';

            $.ajax({
                url: url,
                dataType: "json",
                type: "GET",
                success: function(data) {
                    //location.reload(true);
                    $('#confirm-delete-comment').modal('hide');
                    $('#comment_' + id).remove();
                    $('.page-header').before(success_block);
                    window.scrollBy(0,-1000);
                }
            });
        });
    });

    //$('.btn_update_comment').each(function(){
    //    $(this).click(function(e){
    //        console.log('data this: ', $(this).attr('data-href'));
    //        var url = $(this).attr('data-href');
    //        var id = $(this).attr('data-id');
    //        var success_block = '<div class="alert alert-success alert-block"><button data-dismiss="alert" class="close" type="button">×</button><h4>Коментарий успешно обновлен.</h4></div>';
    //        var content = $('#comment_content_' + id).val();
    //
    //        $.ajax({
    //            url: url,
    //            dataType: "json",
    //            type: "POST",
    //            data: {
    //                content: content,
    //                _token: $('#_token').attr('token')
    //            },
    //            success: function(data) {
    //                console.log('ajax data: ', data);
    //                $('#comment_content_block_'+id).html(content);
    //                $('#comment-edit-form_'+id).modal('hide');
    //                $('.page-header').before(success_block);
    //                window.scrollBy(0,-1000);
    //            }
    //        });
    //    });
    //});


    function viewImage() {
        function imageLoad(files) {
            var reader = new FileReader();
            var image = new Image();

            reader.readAsDataURL(files);
            reader.onload = function (_file) {
                image.src = _file.target.result // url.createObjectURL(file);
                image.onload = function () {
                    var w = this.width,
                        h = this.height,
                        t = files.type, // ext only: // file.type.split('/')[1],
                        n = files.name,
                        s = ~~(files.size / 1024) + 'KB',
                        $img = $('input[id="MainCommentImage"]');

                    $img.siblings('img.img-thumbnail').remove();
                    $img.parent().append('<img class="img-thumbnail" src="' + this.src + '" style="margin-top:10px; width: 300px; height: 200px">');

                };
                image.onerror = function () {
                    alert('Invalid file type: ' + file.type);
                };
            };
        }

        $('[id="MainCommentImage"]').on({
            change: function () {
                imageLoad(this.files[0]);
            }
        });
    }

    function ajaxLoadImage(){
        var token = $('#image_load').attr('token');
        $("#image_load").uploadFile({
            url: $('input[type=hidden][name=add_image]').val(),
            dragDrop: false,
            fileName: "myfile",
            returnType: "json",
            showDelete: true,
            showDownload:false,
            statusBar : false,
            statusBarWidth:600,
            dragdropWidth:600,
            showPreview:false,
            previewHeight: "100px",
            previewWidth: "100px",
            maxFileCount: 4,
            uploadStr: translate_upload,
            abortStr: translate_reject,
            deletelStr: translate_delete,
            /*showError: false,*/
            afterUploadAll:function(obj)
            {
                $('input[type=hidden][name="images[]"]').val(JSON.stringify(obj.responses));
                //You can get data of the plugin using obj
            },
            deleteCallback: function (data, pd) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    $.post(
                        $('input[type=hidden][name=delete_image]').val(),
                        {
                            op: "delete",
                            name: data[i],
                            _token: token
                        },
                        function (resp,textStatus, jqXHR) {
                            //Show Message
                            //var res = [],
                            //    $images = $('input[type=hidden][name="images[]"]');
                            //$(JSON.parse($images.val())).each(function(){
                            //    if(this[0] !== JSON.parse(resp)[0]){
                            //        res.push(this[0])
                            //    }
                            //});
                            //if($('.ajax-file-upload-statusbar').length == 0){
                            //    $images.val([]);
                            //    console.log('reset');
                            //} else {
                            //    $images.val(JSON.stringify(res));
                            //}

                            console.log("File Deleted", resp);
                        }
                    );
                }
                pd.statusbar.hide(); //You choice.

            },
            extraHTML:function()
            {
                html = "<input type='hidden' name='_token' value='" + token + "' />";
                return html;
            }
        });

    }

    function submit(){
        $('textarea[name="comment"]').on('focus', function(){
            $(this).on('keypress', function(e){
                if(e.keyCode == 13 && !e.shiftKey){
                    e.preventDefault();
                    $(this).parents('form').submit();
                }
            });

        });
    }

    function unsubscribeUser(){
        $('#confirm-unsubscribe').on('shown.bs.modal', function (e) {
            $(this).find('a.btn-ok').attr('href', e.relatedTarget.getAttribute('data-href'));
        })
    }

    function sendInvite() {
        var user_id = 0;
        $('[data-user]').on({
            click : function (e) {
               user_id = e.target.attributes.getNamedItem('data-user').value;
            }
        });
        $('#ajaxResponse').on('show.bs.modal', function () {
            var modal = this;
            console.log(modal);
            $(this).find('button').off('click');
            $(this).find('button').on('click', function (e) {
                if($(this).data('res') == 'yes'){
                    var group_id    = $('[name="group_id"]').val(),
                        token      = $('#_token').val();

                    $.post(
                        $('input[type=hidden][name=invite_user]').val(),
                        {
                            user_id: user_id,
                            group_id: group_id,
                            _token: token
                        }
                    )
                    .done(function (resp) {
                        $('[data-user="'+ user_id+'"]').fadeOut(500, function (e) {
                            $(this).remove();
                        });
                        $(modal).modal('hide'); //close modal
                        $('#' + user_id).removeClass('hidden');
                    })
                    .error(function (e) {
                        if (e.status == 500) {
                            alert(e.responseJSON)
                        }
                    });

                }
            })
        });
    }

    $( ".tab-user-invitation" ).on( "click", ".btn-send-invitation-answer", function() {

        var dataTarget  = $(this).attr('data-target');
        var modalW      = $(dataTarget);
        var theUrl      = $(this).attr('data-href');
        var eventTarget = $(this).attr('data-event-target');

        if (modalW.length) {
            var sendAcceptBtn = modalW.find('.send-accept');

            if (sendAcceptBtn.length) {
                sendAcceptBtn.attr('href', theUrl);
                sendAcceptBtn.attr('data-event-target', eventTarget);
                modalW.modal('show');
            }
        }
    });
});