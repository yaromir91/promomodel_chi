(function() {
   var expandWidth = 767,mobileWidth = 517;
    function changeClassByWidth() {
        if(window.innerWidth < expandWidth && window.innerWidth > mobileWidth) {
            $('.main-info-menu').removeClass('col-xs-12');
            $('.main-info-menu').addClass('col-xs-6');
        }
        else {
            $('.main-info-menu').removeClass('col-xs-6');
            $('.main-info-menu').addClass('col-xs-12');
        }
    }
    changeClassByWidth();
    $(window).on('resize',function() {
        changeClassByWidth();
    });
}());
