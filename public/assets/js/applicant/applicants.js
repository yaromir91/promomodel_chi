$(document).ready(function() {
    $(".applicants-list").on('click', '.one-applicant', function() {
        var apId = $(this).attr('data-id');
        document.location.href = "/profile/view/" + apId;
    });
    $(".slider").each(function(){

        var max = parseInt($(this).attr('data-max'));
        var min = parseInt($(this).attr('data-min'));
        var step = parseInt($(this).attr('data-step'));
        var pip_step = parseInt($(this).attr('data-pip_step'));
        var id  = $(this).attr('id');

        $('#'+id).slider({
            min: min,
            max: max,
            range: true,
            step: step,
            values: [min, max]
        })
        .slider("pips", {
            rest: "label",
            step: pip_step
        })
        .slider("float")
        .on("slidechange", function(e,ui) {
            $('#hidden1_'+id).val(ui.values[1]);
            $('#hidden0_'+id).val(ui.values[0]);
        });
    });

});